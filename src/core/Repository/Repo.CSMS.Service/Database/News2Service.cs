﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface INews2Service
    {
        News2 Get(Expression<Func<News2, bool>> where, List<Expression<Func<News2, object>>> includes);
        List<News2> GetMany(int? top, Expression<Func<News2, bool>> where, Func<IQueryable<News2>, IOrderedQueryable<News2>> orderBy, List<Expression<Func<News2, object>>> includes);
        List<News2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<News2, bool>> where, Func<IQueryable<News2>, IOrderedQueryable<News2>> orderBy, List<Expression<Func<News2, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<News2, bool>> where, List<Expression<Func<News2, object>>> includes);
        void Insert(News2 item, bool saveChanges = true);
        void Update(News2 item, bool saveChanges = true);
        void Delete(News2 item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class News2Service : INews2Service
    {
        private readonly IRepository<News2> _repository;

        public News2Service(IRepository<News2> repository)
        {
            this._repository = repository;
        }

        public News2 Get(Expression<Func<News2, bool>> where, List<Expression<Func<News2, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<News2> GetMany(int? top, Expression<Func<News2, bool>> where, Func<IQueryable<News2>, IOrderedQueryable<News2>> orderBy, List<Expression<Func<News2, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<News2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<News2, bool>> where, Func<IQueryable<News2>, IOrderedQueryable<News2>> orderBy, List<Expression<Func<News2, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<News2, bool>> where, List<Expression<Func<News2, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(News2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("News2");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(News2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("News2");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(News2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("News2");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
