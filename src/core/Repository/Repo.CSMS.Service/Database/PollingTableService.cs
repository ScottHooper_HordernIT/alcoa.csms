﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IPollingTableService
    {
        PollingTable Get(Expression<Func<PollingTable, bool>> where, List<Expression<Func<PollingTable, object>>> includes);
        List<PollingTable> GetMany(int? top, Expression<Func<PollingTable, bool>> where, Func<IQueryable<PollingTable>, IOrderedQueryable<PollingTable>> orderBy, List<Expression<Func<PollingTable, object>>> includes);
        List<PollingTable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<PollingTable, bool>> where, Func<IQueryable<PollingTable>, IOrderedQueryable<PollingTable>> orderBy, List<Expression<Func<PollingTable, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<PollingTable, bool>> where, List<Expression<Func<PollingTable, object>>> includes);
        void Insert(PollingTable item, bool saveChanges = true);
        void Update(PollingTable item, bool saveChanges = true);
        void Delete(PollingTable item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class PollingTableService : IPollingTableService
    {
        private readonly IRepository<PollingTable> _repository;

        public PollingTableService(IRepository<PollingTable> repository)
        {
            this._repository = repository;
        }

        public PollingTable Get(Expression<Func<PollingTable, bool>> where, List<Expression<Func<PollingTable, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<PollingTable> GetMany(int? top, Expression<Func<PollingTable, bool>> where, Func<IQueryable<PollingTable>, IOrderedQueryable<PollingTable>> orderBy, List<Expression<Func<PollingTable, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<PollingTable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<PollingTable, bool>> where, Func<IQueryable<PollingTable>, IOrderedQueryable<PollingTable>> orderBy, List<Expression<Func<PollingTable, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<PollingTable, bool>> where, List<Expression<Func<PollingTable, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(PollingTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("PollingTable");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(PollingTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("PollingTable");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(PollingTable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("PollingTable");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
