﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryStandardAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanySiteCategoryStandardAuditDataSourceDesigner))]
	public class CompanySiteCategoryStandardAuditDataSource : ProviderDataSource<CompanySiteCategoryStandardAudit, CompanySiteCategoryStandardAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditDataSource class.
		/// </summary>
		public CompanySiteCategoryStandardAuditDataSource() : base(new CompanySiteCategoryStandardAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryStandardAuditDataSourceView used by the CompanySiteCategoryStandardAuditDataSource.
		/// </summary>
		protected CompanySiteCategoryStandardAuditDataSourceView CompanySiteCategoryStandardAuditView
		{
			get { return ( View as CompanySiteCategoryStandardAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanySiteCategoryStandardAuditDataSource control invokes to retrieve data.
		/// </summary>
		public CompanySiteCategoryStandardAuditSelectMethod SelectMethod
		{
			get
			{
				CompanySiteCategoryStandardAuditSelectMethod selectMethod = CompanySiteCategoryStandardAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanySiteCategoryStandardAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryStandardAuditDataSourceView class that is to be
		/// used by the CompanySiteCategoryStandardAuditDataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryStandardAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategoryStandardAudit, CompanySiteCategoryStandardAuditKey> GetNewDataSourceView()
		{
			return new CompanySiteCategoryStandardAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryStandardAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryStandardAuditDataSourceView : ProviderDataSourceView<CompanySiteCategoryStandardAudit, CompanySiteCategoryStandardAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryStandardAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryStandardAuditDataSourceView(CompanySiteCategoryStandardAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryStandardAuditDataSource CompanySiteCategoryStandardAuditOwner
		{
			get { return Owner as CompanySiteCategoryStandardAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanySiteCategoryStandardAuditSelectMethod SelectMethod
		{
			get { return CompanySiteCategoryStandardAuditOwner.SelectMethod; }
			set { CompanySiteCategoryStandardAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryStandardAuditService CompanySiteCategoryStandardAuditProvider
		{
			get { return Provider as CompanySiteCategoryStandardAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanySiteCategoryStandardAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanySiteCategoryStandardAudit> results = null;
			CompanySiteCategoryStandardAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case CompanySiteCategoryStandardAuditSelectMethod.Get:
					CompanySiteCategoryStandardAuditKey entityKey  = new CompanySiteCategoryStandardAuditKey();
					entityKey.Load(values);
					item = CompanySiteCategoryStandardAuditProvider.Get(entityKey);
					results = new TList<CompanySiteCategoryStandardAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryStandardAuditSelectMethod.GetAll:
                    results = CompanySiteCategoryStandardAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanySiteCategoryStandardAuditSelectMethod.GetPaged:
					results = CompanySiteCategoryStandardAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanySiteCategoryStandardAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanySiteCategoryStandardAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanySiteCategoryStandardAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanySiteCategoryStandardAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryStandardAuditProvider.GetByAuditId(_auditId);
					results = new TList<CompanySiteCategoryStandardAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanySiteCategoryStandardAuditSelectMethod.Get || SelectMethod == CompanySiteCategoryStandardAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanySiteCategoryStandardAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanySiteCategoryStandardAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanySiteCategoryStandardAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanySiteCategoryStandardAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanySiteCategoryStandardAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryStandardAuditDataSource class.
	/// </summary>
	public class CompanySiteCategoryStandardAuditDataSourceDesigner : ProviderDataSourceDesigner<CompanySiteCategoryStandardAudit, CompanySiteCategoryStandardAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditDataSourceDesigner class.
		/// </summary>
		public CompanySiteCategoryStandardAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryStandardAuditSelectMethod SelectMethod
		{
			get { return ((CompanySiteCategoryStandardAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanySiteCategoryStandardAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanySiteCategoryStandardAuditDataSourceActionList

	/// <summary>
	/// Supports the CompanySiteCategoryStandardAuditDataSourceDesigner class.
	/// </summary>
	internal class CompanySiteCategoryStandardAuditDataSourceActionList : DesignerActionList
	{
		private CompanySiteCategoryStandardAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanySiteCategoryStandardAuditDataSourceActionList(CompanySiteCategoryStandardAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryStandardAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanySiteCategoryStandardAuditDataSourceActionList
	
	#endregion CompanySiteCategoryStandardAuditDataSourceDesigner
	
	#region CompanySiteCategoryStandardAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanySiteCategoryStandardAuditDataSource.SelectMethod property.
	/// </summary>
	public enum CompanySiteCategoryStandardAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion CompanySiteCategoryStandardAuditSelectMethod

	#region CompanySiteCategoryStandardAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditFilter : SqlFilter<CompanySiteCategoryStandardAuditColumn>
	{
	}
	
	#endregion CompanySiteCategoryStandardAuditFilter

	#region CompanySiteCategoryStandardAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryStandardAuditColumn>
	{
	}
	
	#endregion CompanySiteCategoryStandardAuditExpressionBuilder	

	#region CompanySiteCategoryStandardAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanySiteCategoryStandardAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditProperty : ChildEntityProperty<CompanySiteCategoryStandardAuditChildEntityTypes>
	{
	}
	
	#endregion CompanySiteCategoryStandardAuditProperty
}

