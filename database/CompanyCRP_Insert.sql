USE [ALCOA_WAO_CSMWP_HORD]
GO

/****** Object:  StoredProcedure [dbo].[CompanyCRP_Insert]    Script Date: 8/07/2015 4:20:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE PROCEDURE [dbo].[CompanyCRP_Insert]      
AS       
DECLARE @CompanyId INT      
      
BEGIN TRY      
 DECLARE curCRP CURSOR FOR       
 SELECT DISTINCT D.COMPANYID FROM HR..XXHR_ARP_CRP_ALL_V AS A              
 LEFT OUTER JOIN HR..XXHR_PEOPLE_DETAILS_V AS B              
 ON A.PERSON_ID = B.PERSON_ID              
 LEFT OUTER JOIN CompanyVendor AS C              
 ON C.VENDOR_NUMBER = B.CWK_VENDOR_NO             
 LEFT OUTER JOIN Companies AS D              
 ON D.CompanyAbn = C.TAX_REGISTRATION_NUMBER              
 LEFT OUTER JOIN SITES AS E              
 ON E.SITENAMEHR=B.LOCATION             
 WHERE A.PER_TYPE_ID = 'C'              
 AND DATEADD(yy,2, A.COURSE_END_DATE)> getdate()      
 AND CompanyId IS NOT NULL      
      
 --TRUNCATE TABLE [dbo].[CompanyCRPMap]      
      
 OPEN curCRP       
 FETCH NEXT FROM curCRP INTO @CompanyId      
  WHILE @@FETCH_STATUS = 0      
  BEGIN   
  
 IF EXISTS (SELECT * FROM CompanyCRPMap WHERE CRP_CompanyId=@CompanyId)   
   
 UPDATE CompanyCRPMap SET CRP_Names=dbo.GetCRPByCompanyId(@CompanyId) WHERE CRP_CompanyId = @CompanyId  
   
        
   ELSE  
  
  INSERT INTO [dbo].[CompanyCRPMap]      
      ([CRP_CompanyId]      
      ,[CRP_Names])      
   VALUES      
      (@CompanyId      
      ,dbo.GetCRPByCompanyId(@CompanyId))      
  FETCH NEXT FROM curCRP INTO @CompanyId      
  END      
        
  CLOSE curCRP      
  DEALLOCATE curCRP      
END TRY      
BEGIN CATCH      
 CLOSE curCRP      
 DEALLOCATE curCRP      
END CATCH     
----------------------------------------

GO


