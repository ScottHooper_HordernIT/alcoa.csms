using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxGridView.Export;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;

using DevExpress.Web.ASPxGridView;
using DevExpress.XtraGrid;
using System.Data.SqlClient;



public partial class UserControls_Admin_ebi : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {

        //gridCompanies.FilterExpression = "Deactivated <> True";
        if (!Page.IsPostBack)
        {
            ConfigService cnfgService = new ConfigService();
            Config cfg = cnfgService.GetByKey("EbiAutomaticEmail");
            if (cfg != null)
            {
                if (cfg.Value == "Yes")
                    cbAutoEmail.Checked = true;
                else
                    cbAutoEmail.Checked = false;
            }

            deByDay.Date = DateTime.Now.AddDays(-1);

            cbByMonth_Year.Items.Add("< Please Select... >", -1);
            cbByMonth_Year.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3));
            cbByMonth_Year.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2));
            cbByMonth_Year.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1));
            cbByMonth_Year.Items.Add(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString());
            cbByMonth_Year.SelectedIndex = 0;


            cbRawData_Year.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3));
            cbRawData_Year.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2));
            cbRawData_Year.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1));
            cbRawData_Year.Items.Add(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString());
            cbRawData_Year.Value = DateTime.Now.Year;

            cmbHourWorkedYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3));
            cmbHourWorkedYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2));
            cmbHourWorkedYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1));
            cmbHourWorkedYear.Items.Add(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString());
            cmbHourWorkedYear.Value = DateTime.Now.Year;


            cmbKPEYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3));
            cmbKPEYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2));
            cmbKPEYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1));
            cmbKPEYear.Items.Add(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString());
            cmbKPEYear.Value = DateTime.Now.Year;



            //ASPxGridViewExporter ex0 = (ASPxGridViewExporter)ExportButtons0.FindControl("gridExporter");
            //ex0.GridViewID = "gridCompanies";
            //ex0.FileName = "Companies EBI-CSMS Match Up";

            ASPxGridViewExporter ex1 = (ASPxGridViewExporter)ExportButtons1.FindControl("gridExporter");
            ex1.GridViewID = "gridByDay"; //Company By Day
            ex1.FileName = "Contractors On Site (EBI) By Day";

            //Add by By Bishwajit Sahoo
            ASPxGridViewExporter ex3 = (ASPxGridViewExporter)ExportButtons3.FindControl("gridExporter");
            //ex3.GridViewID = "gridRawData_ByMonthYear";
            ex3.GridViewID = "grid2";  //Raw Data
            ex3.FileName = "EBI Raw Data (for Year and Month)";
            Session["RawDataGrid"] = "gridRawData_ByMonthYear";

            ASPxGridViewExporter ex4 = (ASPxGridViewExporter)ExportButtons4.FindControl("gridExporter");
            ex4.GridViewID = "gridHourWorked_ByMonthYear";
            //ex4.GridViewID = "grid";
            ex4.FileName = "EBI Hour Worked (for Year and Month)";

            ASPxGridViewExporter ex5 = (ASPxGridViewExporter)ExportButtons5.FindControl("gridExporter");
            ex5.GridViewID = "gridKPE_ByMonthYear"; //Company KPI vs EBI Hours
            ex5.FileName = "Company KPI vs EBI hours (for Year and Month)";

            ASPxGridViewExporter ex2 = (ASPxGridViewExporter)ExportButtons2.FindControl("gridExporter");
            ex2.GridViewID = "gridByMonth"; //Company By Month
            ex2.FileName = "Contractors On Site (EBI) By Month";

            FillCompanyDropdown(CmbRawDataCompany);
            FillCompanyDropdown(cmbKPECompany);
            FillCompanyDropdown(cmbHourWorkedCompany);

            FillMonth(cmbRawDataMonth);
            FillMonth(cmbHourWorkedMonth);
            FillMonth(cmbKPEMonth);
            FillMonth(cbByMonth_Month);

            int startIndex = 0;
            CmbRawDataCompany.Enabled = true;
            FillSitesCombo("-1", CmbRawDataSite);
            startIndex = CmbRawDataSite.Items.IndexOfText("Australia");
            CmbRawDataSite.SelectedIndex = startIndex;

            cmbHourWorkedCompany.Enabled = true;
            FillSitesCombo("-1", cmbHourWorkedSite);
            startIndex = cmbHourWorkedSite.Items.IndexOfText("Australia");
            cmbHourWorkedSite.SelectedIndex = startIndex;

            cmbKPECompany.Enabled = true;
            FillSitesCombo("-1", cmbKPESite);
            startIndex = cmbKPESite.Items.IndexOfText("Australia");
            cmbKPESite.SelectedIndex = startIndex;

        }
        if (ViewState["Year"] != null && ViewState["Month"] != null)
        {
            DataTable dt = new DataTable();
            string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

            int lastYear = DateTime.Now.Year;
            int FirstYear = 2011;

            using (SqlConnection cnDel = new SqlConnection(sqldb))
            {
                cnDel.Open();
                //Console.WriteLine("..Creating table: WorkFormPlanSummary" + "...");
                SqlCommand cmdDel = new SqlCommand("_Ebi_CompaniesResidential_OnSite_ByYearMonth", cnDel);
                cmdDel.CommandTimeout = 7000;
                cmdDel.CommandType = CommandType.StoredProcedure;
                cmdDel.Parameters.AddWithValue("@Year", Convert.ToInt32(cbByMonth_Year.Value));
                cmdDel.Parameters.AddWithValue("@Month", Convert.ToInt32(cbByMonth_Month.Value));
                SqlDataAdapter sqlda = new SqlDataAdapter(cmdDel);
                sqlda.Fill(dt);
                cnDel.Close();

                gridByMonth.DataSource = dt;
                gridByMonth.DataBind();
            }
        }
    }

    protected void btnKPE_Click(object sender, EventArgs e)
    {
        //KpiVsEbiDS.DataBind();

        gridKPE_ByMonthYear.DataBind();
    }
    protected void btnByMonth_GoFilter_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(cbByMonth_Year.SelectedItem.Value) >= 0)
        {
            DataTable dt = new DataTable();
            string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;
            //Ebi_YearMonthDayCompanies_ByYearMonth.DataBind();
            using (SqlConnection cnDel = new SqlConnection(sqldb))
            {
                cnDel.Open();
                //Console.WriteLine("..Creating table: WorkFormPlanSummary" + "...");
                SqlCommand cmdDel = new SqlCommand("_Ebi_CompaniesResidential_OnSite_ByYearMonth", cnDel);
                cmdDel.CommandTimeout = 7000;
                cmdDel.CommandType = CommandType.StoredProcedure;

                cmdDel.Parameters.AddWithValue("@Year", Convert.ToInt32(cbByMonth_Year.Value));
                cmdDel.Parameters.AddWithValue("@Month", Convert.ToInt32(cbByMonth_Month.Value));

                SqlDataAdapter sqlda = new SqlDataAdapter(cmdDel);
                sqlda.Fill(dt);
                cnDel.Close();
                gridByMonth.DataSource = dt;
                gridByMonth.DataBind();

                ViewState["Year"] = Convert.ToInt32(cbByMonth_Year.Value);
                ViewState["Month"] = Convert.ToInt32(cbByMonth_Month.Value);
            }
            //gridByMonth.DataBind();
        }
        else
            throw new Exception("Please select an Year");
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)  //Raw Data Button
    {
        if (Convert.ToInt32(cmbHourWorkedSite.SelectedItem.Value) != 0 || Convert.ToInt32(cmbHourWorkedCompany.SelectedItem.Value) != 0)
        {


            //dsEbi_RawData_ByYearMonthCompanySite.DataBind();
            //gridRawData_ByMonthYear.DataBind();
            grid2.DataBind();
        }
        else
            throw new Exception("Please select a valid Company/Site");
    }
    protected void btnAutoEmail_Click(object sender, EventArgs e)
    {
        ConfigService cfgService = new ConfigService();
        Config cfg = cfgService.GetByKey("EbiAutomaticEmail");
        if (cbAutoEmail.Checked == true)
        {
            cfg.Value = "Yes";
        }
        else
            cfg.Value = "No";
        cfg.EntityState = EntityState.Changed;
        cfgService.Save(cfg);
    }

    protected void btnHoursWorked_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(cmbHourWorkedSite.SelectedItem.Value) != 0 || Convert.ToInt32(cmbHourWorkedCompany.SelectedItem.Value) != 0)
        {
            //EbiHoursWorkedDS.DataBind();

            gridHourWorked_ByMonthYear.DataBind();
            //grid.DataBind();
            
        }
        else
            throw new Exception("Please select a valid Company/Site");

    }

    protected void cmbKPECompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(cmbKPECompany.SelectedItem.Value.ToString(), cmbKPESite);

    }

    protected void CmbRawDataCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(CmbRawDataCompany.SelectedItem.Value.ToString(), CmbRawDataSite);
    }

    protected void cmbHourWorkedCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(cmbHourWorkedCompany.SelectedItem.Value.ToString(), cmbHourWorkedSite);

    }



    protected void FillSitesCombo(string companyId, ASPxComboBox CmbSite)
    {
        int _i = 0;
        if (string.IsNullOrEmpty(companyId)) return;

        int _companyId;
        bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
        if (!companyId_isInt) return;



        if (_companyId > 0) //Specific Company
        {

            SitesService sService = new SitesService();
            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_companyId);

            CmbSite.Items.Clear();

            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (cscs.CompanySiteCategoryId != null)
                            {
                                if (s.SiteId == cscs.SiteId)
                                {
                                    CmbSite.Items.Add(s.SiteName, cscs.SiteId);
                                }
                            }
                        }
                    }

                    CmbSite.Items.Add("----------------", 0);

                    //Add Regions
                    RegionsService rService = new RegionsService();
                    DataSet dsRegion = rService.GetByCompanyId(_companyId);
                    if (dsRegion.Tables[0] != null)
                    {
                        if (dsRegion.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsRegion.Tables[0].Rows)
                            {
                                if (dr[2].ToString() != "AU") CmbSite.Items.Add(dr[0].ToString(), (-1 * Convert.ToInt32(dr[1].ToString())));
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (_companyId == -1) // All Companies
            {
                CmbSite.Items.Clear();
                SitesFilters sitesFilters = new SitesFilters();
                sitesFilters.Append(SitesColumn.IsVisible, "1");
                TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                foreach (Sites s in sitesList)
                {
                    CmbSite.Items.Add(s.SiteName, s.SiteId);
                }
                CmbSite.Items.Add("----------------", 0);

                RegionsFilters regionsFilters = new RegionsFilters();
                regionsFilters.Append(RegionsColumn.IsVisible, "1");
                TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                foreach (Regions r in regionsList)
                {
                    CmbSite.Items.Add(r.RegionName, (-1 * r.RegionId));
                }
            }
            else if (_companyId == -2) //Selected "----" invalid option, so lets hide all sites.
            {
                CmbSite.Items.Clear();
                CmbSite.DataSourceID = "";
                CmbSite.DataBind();
            }
        }

    }

    protected void cmbSites_Callback(object source, CallbackEventArgsBase e)
    {
        CmbRawDataSite.SelectedIndex = 0;
        FillSitesCombo(e.Parameter, CmbRawDataSite);
    }

    protected void cmbSites1_Callback(object source, CallbackEventArgsBase e)
    {
        cmbHourWorkedSite.SelectedIndex = 0;
        FillSitesCombo(e.Parameter, cmbHourWorkedSite);
    }

    protected void cmbSites2_Callback(object source, CallbackEventArgsBase e)
    {
        cmbHourWorkedYear.SelectedIndex = 0;
        FillSitesCombo(e.Parameter, cmbKPESite);
    }


    protected void FillCompanyDropdown(ASPxComboBox CmbCompany)
    {
        CmbCompany.DataSourceID = "CompaniesDataSource3";
        CmbCompany.TextField = "CompanyName";
        CmbCompany.ValueField = "CompanyId";
        CmbCompany.DataBind();
        CmbCompany.Items.Add("-----------------------------", 0);
        CmbCompany.Items.Add("All", -1);
        CmbCompany.Value = -1;
    }

    protected void FillMonth(ASPxComboBox cmbMonth)
    {


        cmbMonth.Items.Add("1");
        cmbMonth.Items.Add("2");
        cmbMonth.Items.Add("3");
        cmbMonth.Items.Add("4");
        cmbMonth.Items.Add("5");
        cmbMonth.Items.Add("6");
        cmbMonth.Items.Add("7");
        cmbMonth.Items.Add("8");
        cmbMonth.Items.Add("9");
        cmbMonth.Items.Add("10");
        cmbMonth.Items.Add("11");
        cmbMonth.Items.Add("12");
        cmbMonth.Value = DateTime.Now.Month;

    }

    protected void grid_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "DoubtPer")
        {
            decimal doubts = Convert.ToDecimal(e.GetListSourceFieldValue("Doubts"));
            decimal totalSwipes = Convert.ToInt32(e.GetListSourceFieldValue("TotalSwipes"));
            
            if (totalSwipes == 0)
                e.Value = Convert.ToDecimal(0.00);
            else
            {
                //string mxValue = ((doubts / totalSwipes) * 100).ToString();
                //if (mxValue == "0")
                //    e.Value = 0.00;
                //else if (mxValue == "100")
                //    e.Value = 100.00;
                //else
                //{
                //    mxValue = mxValue.Substring(0, 4);
                //    e.Value = Convert.ToDecimal(mxValue);
                //}
                //e.Value=Convert.ToDecimal((doubts / totalSwipes) * 100);
                double dc = Convert.ToDouble((doubts / totalSwipes) * 100);
                Decimal dc1 = Convert.ToDecimal(RoundUp(dc, 2));
                e.Value = dc1;
                
            }}
            if (e.Column.FieldName == "Variance")
            {
                int kpiHours = (int)e.GetListSourceFieldValue("KpiHours");
                int ebiHours = Convert.ToInt32(e.GetListSourceFieldValue("EbiHours"));
                e.Value = kpiHours - ebiHours;
            }
            if (e.Column.FieldName == "VariancePer")
            {

                decimal kpiHours = Convert.ToDecimal(e.GetListSourceFieldValue("KpiHours"));
                decimal ebiHours = Convert.ToDecimal(e.GetListSourceFieldValue("EbiHours"));

                if (ebiHours == 0 && kpiHours == 0)
                    e.Value = Convert.ToDecimal(0.00);
                else if (kpiHours == 0 && ebiHours > 0)
                    e.Value = Convert.ToDecimal(100.0);
                else
                {
                    //string mxValue2 = ((kpiHours - ebiHours) / ebiHours).ToString();

                    //if (mxValue2 == "0")
                    //    e.Value = 0.00;
                    //else if (mxValue2 == "100")
                    //    e.Value = 100.00;
                    //else
                    //{
                    //    mxValue2 = mxValue2.Substring(0, 4);
                    //    e.Value = Convert.ToDecimal(mxValue2);
                    //}
                    double dc = Convert.ToDouble(((ebiHours - kpiHours) / kpiHours) * 100);
                    Decimal dc1 = Convert.ToDecimal(RoundUp(dc, 2));
                    e.Value = dc1;
                }
            }
        }
        public static double RoundUp(double input, int places)
        {
            double multiplier = Math.Pow(10, Convert.ToDouble(places));
            return Math.Ceiling(input * multiplier) / multiplier;
        }

    
}
