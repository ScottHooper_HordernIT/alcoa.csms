USE [HR]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[XXHR_SQL_CWK_Update]    
(    
    
 @PERSON_ID numeric(10,0),     
 @EMPLID varchar(50),    
 @PER_TYPE_ID varchar(50),    
 @FULL_NAME varchar(max),    
 @SGNAME varchar(max),    
 @LAST_NAME_SRCH varchar(max),    
 @FIRST_NAME_SRCH varchar(max),    
 @SUPERVISOR_ID varchar(50),    
 @POS_DESCR varchar(max),    
 @LOCATION varchar(max),    
 @SEX varchar(50),    
 @LBC varchar(50),    
 @DEPT_CD varchar(50),    
 @ACCOUNT varchar(50),    
 @SERVICE_DT datetime,    
 @TERMINATION_DT datetime,    
 @AOA_PREF_NAME varchar(50),    
 @LOGINID varchar(50),    
 @ORG_CODE varchar(50),    
 @DEPT_ORG_CODE char(10),    
 @OP_CENTRE varchar(max),    
 @CWK_VENDOR_NO varchar(max),    
 @CWK_VENDOR_NAME varchar(max),    
 @CWK_NON_PREFERRED_SUPP_NAME varchar(max),    
 @CWK_SPONSOR_ID varchar(max),    
 @CWK_SUPP_SUPERVISOR_ID varchar(max),    
 @CWL_PRIVILEGE_FLAG varchar(max)    
 )    
AS    
    
    
 IF EXISTS(Select * from HR..XXHR_PEOPLE_DETAILS     
 where Person_id=@PERSON_ID )    
 BEGIN     
        
    -- Modify the updatable columns    
    UPDATE    
     HR..XXHR_PEOPLE_DETAILS    
    SET    
          
     EMPLID=@EMPLID,    
     PER_TYPE_ID=@PER_TYPE_ID,    
     FULL_NAME=@FULL_NAME,    
     SGNAME = @SGNAME,    
     LAST_NAME_SRCH=@LAST_NAME_SRCH,    
     FIRST_NAME_SRCH=@FIRST_NAME_SRCH,    
     SUPERVISOR_ID=@SUPERVISOR_ID,    
     POS_DESCR=@POS_DESCR,    
     LOCATION = @LOCATION,    
     SEX = @SEX,      
     LBC=@LBC,    
     DEPT_CD=@DEPT_CD,    
     ACCOUNT=@ACCOUNT,     
     SERVICE_DT=@SERVICE_DT,    
     TERMINATION_DT=@TERMINATION_DT,    
     AOA_PREF_NAME=@AOA_PREF_NAME,    
     LOGINID=@LOGINID,    
     ORG_CODE=@ORG_CODE,    
     DEPT_ORG_CODE=@DEPT_ORG_CODE,    
     OP_CENTRE=@OP_CENTRE,    
     CWK_VENDOR_NO = @CWK_VENDOR_NO,    
     CWK_VENDOR_NAME = @CWK_VENDOR_NAME,    
     CWK_NON_PREFERRED_SUPP_NAME = @CWK_NON_PREFERRED_SUPP_NAME,    
     CWK_SPONSOR_ID = @CWK_SPONSOR_ID,    
     CWK_SUPP_SUPERVISOR_ID = @CWK_SUPP_SUPERVISOR_ID,    
     CWL_PRIVILEGE_FLAG = @CWL_PRIVILEGE_FLAG,     
     LAST_UPDATE_DATE=GETdATE()    
    WHERE    
 Person_id=@PERSON_ID     
     
 END    
 ELSE    
 BEGIN    
 INSERT INTO HR..XXHR_PEOPLE_DETAILS values    
 (    
 @PERSON_ID,     
 @EMPLID,    
 @PER_TYPE_ID,    
 @FULL_NAME,    
 @SGNAME,    
 @LAST_NAME_SRCH,    
 @FIRST_NAME_SRCH,    
 @SUPERVISOR_ID,    
 @POS_DESCR,    
 NULL,    
 @LOCATION,    
 NULL ,    
 @SEX ,    
 @LBC ,    
 @DEPT_CD,    
 @ACCOUNT,    
 @SERVICE_DT ,    
 @TERMINATION_DT ,    
 @AOA_PREF_NAME ,    
 @LOGINID ,    
 @ORG_CODE ,    
 @DEPT_ORG_CODE ,    
 @OP_CENTRE ,    
 @CWK_VENDOR_NO,    
 @CWK_VENDOR_NAME,    
 @CWK_NON_PREFERRED_SUPP_NAME,    
 @CWK_SPONSOR_ID,    
 @CWK_SUPP_SUPERVISOR_ID,    
 @CWL_PRIVILEGE_FLAG,    
 GETdATE()    
    
    
    
    
 )    
 END    
    
  ------------------------------------------------------------


GO


