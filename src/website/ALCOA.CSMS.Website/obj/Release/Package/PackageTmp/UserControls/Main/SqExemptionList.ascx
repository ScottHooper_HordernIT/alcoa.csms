﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SqExemptionList.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Main.SqExemptionList" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Width="900px">
    <TabPages>
        <dx:TabPage Name="tpCurrent" Text="Current">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxGridView ID="gvCurrent" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ClientInstanceName="gvCurrent" CssPostfix="Office2003Blue" AutoGenerateColumns="False"
                        DataSourceID="SqExemptionFriendlyCurrentDataSource1" Width="870px" OnHtmlRowCreated="gvCurrent_HtmlRowCreated">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption=" " FieldName="SqExemptionId" ReadOnly="True"
                                VisibleIndex="0" Visible="false">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption=" " FieldName="CompanyId" ReadOnly="True" VisibleIndex="0"
                                Visible="false">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CompanyName" ShowInCustomizationForm="True"
                                VisibleIndex="1" SortIndex="1" SortOrder="Ascending">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hlView" runat="server" Text="(View)">
                                    </dx:ASPxHyperLink>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="RequestingCompanyName" ShowInCustomizationForm="True"
                                VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SiteName" ShowInCustomizationForm="True" VisibleIndex="0"
                                GroupIndex="0" SortIndex="0" SortOrder="Ascending">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="DateApplied" ShowInCustomizationForm="True"
                                VisibleIndex="2">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="ValidFrom" ShowInCustomizationForm="True" VisibleIndex="3">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="ValidTo" ShowInCustomizationForm="True" VisibleIndex="4">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn Caption="Company Type" FieldName="CompanyStatusDesc" ShowInCustomizationForm="True"
                                VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsPager Mode="ShowAllRecords" NumericButtonCount="100" PageSize="100">
                        </SettingsPager>
                        <Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" />
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                    <table width="100%">
                        <tr align="right">
                            <td colspan="3" style="height: 35px; text-align: right; text-align: -moz-right;"
                                align="right">
                                <div align="right">
                                    <uc1:ExportButtons ID="ExportButtons1" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Name="tpExpired" Text="Expired">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <dx:ASPxGridView ID="gvExpired" runat="server" ClientInstanceName="gvExpired" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" AutoGenerateColumns="False" DataSourceID="SqExemptionFriendlyExpiredDataSource1"
                        Width="870px" OnHtmlRowCreated="gvExpired_HtmlRowCreated">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption=" " FieldName="SqExemptionId" ReadOnly="True"
                                VisibleIndex="0" Visible="false">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption=" " FieldName="CompanyId" ReadOnly="True" VisibleIndex="0"
                                Visible="false">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CompanyName" ShowInCustomizationForm="True"
                                VisibleIndex="1">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hlView" runat="server" Text="(View)">
                                    </dx:ASPxHyperLink>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="RequestingCompanyName" ShowInCustomizationForm="True"
                                VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SiteName" GroupIndex="0" ShowInCustomizationForm="True"
                                SortIndex="0" SortOrder="Ascending" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="DateApplied" ShowInCustomizationForm="True"
                                VisibleIndex="2">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="ValidFrom" ShowInCustomizationForm="True" VisibleIndex="3">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="ValidTo" ShowInCustomizationForm="True" SortIndex="1"
                                SortOrder="Ascending" VisibleIndex="4">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn Caption="Company Type" FieldName="CompanyStatusDesc" ShowInCustomizationForm="True"
                                VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" />
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                    <table width="100%">
                        <tr align="right">
                            <td colspan="3" style="height: 35px; text-align: right; text-align: -moz-right;"
                                align="right">
                                <div align="right" style="text-align: right">
                                    <uc1:ExportButtons ID="ExportButtons2" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>
<data:SqExemptionFriendlyExpiredDataSource ID="SqExemptionFriendlyExpiredDataSource1"
    runat="server">
</data:SqExemptionFriendlyExpiredDataSource>
<data:SqExemptionFriendlyCurrentDataSource ID="SqExemptionFriendlyCurrentDataSource1"
    runat="server">
</data:SqExemptionFriendlyCurrentDataSource>
