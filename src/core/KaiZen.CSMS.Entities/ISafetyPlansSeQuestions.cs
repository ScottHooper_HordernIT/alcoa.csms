﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'SafetyPlans_SE_Questions' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ISafetyPlansSeQuestions 
	{
		/// <summary>			
		/// QuestionId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "SafetyPlans_SE_Questions"</remarks>
		System.Int32 QuestionId { get; set; }
				
		/// <summary>
		/// keep a copy of the original so it can be used for editable primary keys.
		/// </summary>
		System.Int32 OriginalQuestionId { get; set; }
			
		
		
		/// <summary>
		/// Question : 
		/// </summary>
		System.String  Question  { get; set; }
		
		/// <summary>
		/// QuestionText : 
		/// </summary>
		System.Byte[]  QuestionText  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _safetyPlansSeAnswersQuestionId
		/// </summary>	
		TList<SafetyPlansSeAnswers> SafetyPlansSeAnswersCollection {  get;  set;}	

		#endregion Data Properties

	}
}


