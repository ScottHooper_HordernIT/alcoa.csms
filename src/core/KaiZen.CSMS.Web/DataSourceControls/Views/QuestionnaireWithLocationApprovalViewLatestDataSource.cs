﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireWithLocationApprovalViewLatestProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireWithLocationApprovalViewLatestDataSourceDesigner))]
	public class QuestionnaireWithLocationApprovalViewLatestDataSource : ReadOnlyDataSource<QuestionnaireWithLocationApprovalViewLatest>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestDataSource class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewLatestDataSource() : base(new QuestionnaireWithLocationApprovalViewLatestService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireWithLocationApprovalViewLatestDataSourceView used by the QuestionnaireWithLocationApprovalViewLatestDataSource.
		/// </summary>
		protected QuestionnaireWithLocationApprovalViewLatestDataSourceView QuestionnaireWithLocationApprovalViewLatestView
		{
			get { return ( View as QuestionnaireWithLocationApprovalViewLatestDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireWithLocationApprovalViewLatestDataSourceView class that is to be
		/// used by the QuestionnaireWithLocationApprovalViewLatestDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireWithLocationApprovalViewLatestDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireWithLocationApprovalViewLatest, Object> GetNewDataSourceView()
		{
			return new QuestionnaireWithLocationApprovalViewLatestDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireWithLocationApprovalViewLatestDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireWithLocationApprovalViewLatestDataSourceView : ReadOnlyDataSourceView<QuestionnaireWithLocationApprovalViewLatest>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireWithLocationApprovalViewLatestDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireWithLocationApprovalViewLatestDataSourceView(QuestionnaireWithLocationApprovalViewLatestDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireWithLocationApprovalViewLatestDataSource QuestionnaireWithLocationApprovalViewLatestOwner
		{
			get { return Owner as QuestionnaireWithLocationApprovalViewLatestDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireWithLocationApprovalViewLatestService QuestionnaireWithLocationApprovalViewLatestProvider
		{
			get { return Provider as QuestionnaireWithLocationApprovalViewLatestService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireWithLocationApprovalViewLatestDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireWithLocationApprovalViewLatestDataSource class.
	/// </summary>
	public class QuestionnaireWithLocationApprovalViewLatestDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireWithLocationApprovalViewLatest>
	{
	}

	#endregion QuestionnaireWithLocationApprovalViewLatestDataSourceDesigner

	#region QuestionnaireWithLocationApprovalViewLatestFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewLatestFilter : SqlFilter<QuestionnaireWithLocationApprovalViewLatestColumn>
	{
	}

	#endregion QuestionnaireWithLocationApprovalViewLatestFilter

	#region QuestionnaireWithLocationApprovalViewLatestExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewLatestExpressionBuilder : SqlExpressionBuilder<QuestionnaireWithLocationApprovalViewLatestColumn>
	{
	}
	
	#endregion QuestionnaireWithLocationApprovalViewLatestExpressionBuilder		
}

