﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit06Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit06DataSourceDesigner))]
	public class TwentyOnePointAudit06DataSource : ProviderDataSource<TwentyOnePointAudit06, TwentyOnePointAudit06Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06DataSource class.
		/// </summary>
		public TwentyOnePointAudit06DataSource() : base(new TwentyOnePointAudit06Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit06DataSourceView used by the TwentyOnePointAudit06DataSource.
		/// </summary>
		protected TwentyOnePointAudit06DataSourceView TwentyOnePointAudit06View
		{
			get { return ( View as TwentyOnePointAudit06DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit06DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit06SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit06SelectMethod selectMethod = TwentyOnePointAudit06SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit06SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit06DataSourceView class that is to be
		/// used by the TwentyOnePointAudit06DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit06DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit06, TwentyOnePointAudit06Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit06DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit06DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit06DataSourceView : ProviderDataSourceView<TwentyOnePointAudit06, TwentyOnePointAudit06Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit06DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit06DataSourceView(TwentyOnePointAudit06DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit06DataSource TwentyOnePointAudit06Owner
		{
			get { return Owner as TwentyOnePointAudit06DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit06SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit06Owner.SelectMethod; }
			set { TwentyOnePointAudit06Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit06Service TwentyOnePointAudit06Provider
		{
			get { return Provider as TwentyOnePointAudit06Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit06> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit06> results = null;
			TwentyOnePointAudit06 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit06SelectMethod.Get:
					TwentyOnePointAudit06Key entityKey  = new TwentyOnePointAudit06Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit06Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit06>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit06SelectMethod.GetAll:
                    results = TwentyOnePointAudit06Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit06SelectMethod.GetPaged:
					results = TwentyOnePointAudit06Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit06SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit06Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit06Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit06SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit06Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit06>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit06SelectMethod.Get || SelectMethod == TwentyOnePointAudit06SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit06 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit06Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit06> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit06Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit06DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit06DataSource class.
	/// </summary>
	public class TwentyOnePointAudit06DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit06, TwentyOnePointAudit06Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit06DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit06SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit06DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit06DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit06DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit06DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit06DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit06DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit06DataSourceActionList(TwentyOnePointAudit06DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit06SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit06DataSourceActionList
	
	#endregion TwentyOnePointAudit06DataSourceDesigner
	
	#region TwentyOnePointAudit06SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit06DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit06SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit06SelectMethod

	#region TwentyOnePointAudit06Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06Filter : SqlFilter<TwentyOnePointAudit06Column>
	{
	}
	
	#endregion TwentyOnePointAudit06Filter

	#region TwentyOnePointAudit06ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit06Column>
	{
	}
	
	#endregion TwentyOnePointAudit06ExpressionBuilder	

	#region TwentyOnePointAudit06Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit06ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06Property : ChildEntityProperty<TwentyOnePointAudit06ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit06Property
}

