﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireCommentService
    {
        QuestionnaireComment Get(Expression<Func<QuestionnaireComment, bool>> where, List<Expression<Func<QuestionnaireComment, object>>> includes);
        List<QuestionnaireComment> GetMany(int? top, Expression<Func<QuestionnaireComment, bool>> where, Func<IQueryable<QuestionnaireComment>, IOrderedQueryable<QuestionnaireComment>> orderBy, List<Expression<Func<QuestionnaireComment, object>>> includes);
        List<QuestionnaireComment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireComment, bool>> where, Func<IQueryable<QuestionnaireComment>, IOrderedQueryable<QuestionnaireComment>> orderBy, List<Expression<Func<QuestionnaireComment, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireComment, bool>> where, List<Expression<Func<QuestionnaireComment, object>>> includes);
        void Insert(QuestionnaireComment item, bool saveChanges = true);
        void Update(QuestionnaireComment item, bool saveChanges = true);
        void Delete(QuestionnaireComment item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireCommentService : IQuestionnaireCommentService
    {
        private readonly IRepository<QuestionnaireComment> _repository;

        public QuestionnaireCommentService(IRepository<QuestionnaireComment> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireComment Get(Expression<Func<QuestionnaireComment, bool>> where, List<Expression<Func<QuestionnaireComment, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireComment> GetMany(int? top, Expression<Func<QuestionnaireComment, bool>> where, Func<IQueryable<QuestionnaireComment>, IOrderedQueryable<QuestionnaireComment>> orderBy, List<Expression<Func<QuestionnaireComment, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireComment> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireComment, bool>> where, Func<IQueryable<QuestionnaireComment>, IOrderedQueryable<QuestionnaireComment>> orderBy, List<Expression<Func<QuestionnaireComment, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireComment, bool>> where, List<Expression<Func<QuestionnaireComment, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireComment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireComment");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireComment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireComment");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireComment item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireComment");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
