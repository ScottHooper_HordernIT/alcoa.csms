﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsFileTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsmsFileTypeDataSourceDesigner))]
	public class CsmsFileTypeDataSource : ProviderDataSource<CsmsFileType, CsmsFileTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeDataSource class.
		/// </summary>
		public CsmsFileTypeDataSource() : base(new CsmsFileTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsFileTypeDataSourceView used by the CsmsFileTypeDataSource.
		/// </summary>
		protected CsmsFileTypeDataSourceView CsmsFileTypeView
		{
			get { return ( View as CsmsFileTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsmsFileTypeDataSource control invokes to retrieve data.
		/// </summary>
		public CsmsFileTypeSelectMethod SelectMethod
		{
			get
			{
				CsmsFileTypeSelectMethod selectMethod = CsmsFileTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsmsFileTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsFileTypeDataSourceView class that is to be
		/// used by the CsmsFileTypeDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsFileTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsFileType, CsmsFileTypeKey> GetNewDataSourceView()
		{
			return new CsmsFileTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsFileTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsFileTypeDataSourceView : ProviderDataSourceView<CsmsFileType, CsmsFileTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsFileTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsFileTypeDataSourceView(CsmsFileTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsFileTypeDataSource CsmsFileTypeOwner
		{
			get { return Owner as CsmsFileTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsmsFileTypeSelectMethod SelectMethod
		{
			get { return CsmsFileTypeOwner.SelectMethod; }
			set { CsmsFileTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsFileTypeService CsmsFileTypeProvider
		{
			get { return Provider as CsmsFileTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsmsFileType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsmsFileType> results = null;
			CsmsFileType item;
			count = 0;
			
			System.Int32 _csmsFileTypeId;
			System.String _csmsFileTypeName;

			switch ( SelectMethod )
			{
				case CsmsFileTypeSelectMethod.Get:
					CsmsFileTypeKey entityKey  = new CsmsFileTypeKey();
					entityKey.Load(values);
					item = CsmsFileTypeProvider.Get(entityKey);
					results = new TList<CsmsFileType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsmsFileTypeSelectMethod.GetAll:
                    results = CsmsFileTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsmsFileTypeSelectMethod.GetPaged:
					results = CsmsFileTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsmsFileTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsmsFileTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsmsFileTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsmsFileTypeSelectMethod.GetByCsmsFileTypeId:
					_csmsFileTypeId = ( values["CsmsFileTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsFileTypeId"], typeof(System.Int32)) : (int)0;
					item = CsmsFileTypeProvider.GetByCsmsFileTypeId(_csmsFileTypeId);
					results = new TList<CsmsFileType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CsmsFileTypeSelectMethod.GetByCsmsFileTypeName:
					_csmsFileTypeName = ( values["CsmsFileTypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["CsmsFileTypeName"], typeof(System.String)) : string.Empty;
					item = CsmsFileTypeProvider.GetByCsmsFileTypeName(_csmsFileTypeName);
					results = new TList<CsmsFileType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsmsFileTypeSelectMethod.Get || SelectMethod == CsmsFileTypeSelectMethod.GetByCsmsFileTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsmsFileType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsmsFileTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsmsFileType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsmsFileTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsmsFileTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsFileTypeDataSource class.
	/// </summary>
	public class CsmsFileTypeDataSourceDesigner : ProviderDataSourceDesigner<CsmsFileType, CsmsFileTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeDataSourceDesigner class.
		/// </summary>
		public CsmsFileTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsFileTypeSelectMethod SelectMethod
		{
			get { return ((CsmsFileTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsmsFileTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsmsFileTypeDataSourceActionList

	/// <summary>
	/// Supports the CsmsFileTypeDataSourceDesigner class.
	/// </summary>
	internal class CsmsFileTypeDataSourceActionList : DesignerActionList
	{
		private CsmsFileTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsmsFileTypeDataSourceActionList(CsmsFileTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsFileTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsmsFileTypeDataSourceActionList
	
	#endregion CsmsFileTypeDataSourceDesigner
	
	#region CsmsFileTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsmsFileTypeDataSource.SelectMethod property.
	/// </summary>
	public enum CsmsFileTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsmsFileTypeId method.
		/// </summary>
		GetByCsmsFileTypeId,
		/// <summary>
		/// Represents the GetByCsmsFileTypeName method.
		/// </summary>
		GetByCsmsFileTypeName
	}
	
	#endregion CsmsFileTypeSelectMethod

	#region CsmsFileTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFileType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileTypeFilter : SqlFilter<CsmsFileTypeColumn>
	{
	}
	
	#endregion CsmsFileTypeFilter

	#region CsmsFileTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFileType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileTypeExpressionBuilder : SqlExpressionBuilder<CsmsFileTypeColumn>
	{
	}
	
	#endregion CsmsFileTypeExpressionBuilder	

	#region CsmsFileTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsmsFileTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFileType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileTypeProperty : ChildEntityProperty<CsmsFileTypeChildEntityTypes>
	{
	}
	
	#endregion CsmsFileTypeProperty
}

