﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileDbUsersCompaniesSitesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(FileDbUsersCompaniesSitesDataSourceDesigner))]
	public class FileDbUsersCompaniesSitesDataSource : ReadOnlyDataSource<FileDbUsersCompaniesSites>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesDataSource class.
		/// </summary>
		public FileDbUsersCompaniesSitesDataSource() : base(new FileDbUsersCompaniesSitesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileDbUsersCompaniesSitesDataSourceView used by the FileDbUsersCompaniesSitesDataSource.
		/// </summary>
		protected FileDbUsersCompaniesSitesDataSourceView FileDbUsersCompaniesSitesView
		{
			get { return ( View as FileDbUsersCompaniesSitesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileDbUsersCompaniesSitesDataSourceView class that is to be
		/// used by the FileDbUsersCompaniesSitesDataSource.
		/// </summary>
		/// <returns>An instance of the FileDbUsersCompaniesSitesDataSourceView class.</returns>
		protected override BaseDataSourceView<FileDbUsersCompaniesSites, Object> GetNewDataSourceView()
		{
			return new FileDbUsersCompaniesSitesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileDbUsersCompaniesSitesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileDbUsersCompaniesSitesDataSourceView : ReadOnlyDataSourceView<FileDbUsersCompaniesSites>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileDbUsersCompaniesSitesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileDbUsersCompaniesSitesDataSourceView(FileDbUsersCompaniesSitesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileDbUsersCompaniesSitesDataSource FileDbUsersCompaniesSitesOwner
		{
			get { return Owner as FileDbUsersCompaniesSitesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileDbUsersCompaniesSitesService FileDbUsersCompaniesSitesProvider
		{
			get { return Provider as FileDbUsersCompaniesSitesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region FileDbUsersCompaniesSitesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileDbUsersCompaniesSitesDataSource class.
	/// </summary>
	public class FileDbUsersCompaniesSitesDataSourceDesigner : ReadOnlyDataSourceDesigner<FileDbUsersCompaniesSites>
	{
	}

	#endregion FileDbUsersCompaniesSitesDataSourceDesigner

	#region FileDbUsersCompaniesSitesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbUsersCompaniesSitesFilter : SqlFilter<FileDbUsersCompaniesSitesColumn>
	{
	}

	#endregion FileDbUsersCompaniesSitesFilter

	#region FileDbUsersCompaniesSitesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbUsersCompaniesSitesExpressionBuilder : SqlExpressionBuilder<FileDbUsersCompaniesSitesColumn>
	{
	}
	
	#endregion FileDbUsersCompaniesSitesExpressionBuilder		
}

