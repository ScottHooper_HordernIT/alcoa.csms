﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'FileDbMedicalTraining' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class FileDbMedicalTraining : FileDbMedicalTrainingBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="FileDbMedicalTraining"/> instance.
		///</summary>
		public FileDbMedicalTraining():base(){}	
		
		#endregion
	}
}
