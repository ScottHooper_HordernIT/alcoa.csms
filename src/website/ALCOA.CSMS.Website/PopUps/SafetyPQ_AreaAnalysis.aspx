﻿<%@ Page Title="Safety Questionnaire - Area Analysis" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_SafetyPQ_AreaAnalysis" Codebehind="SafetyPQ_AreaAnalysis.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ScriptManager ID="scriptmanager1" runat="Server" EnablePageMethods="true" EnablePartialRendering="true" AsyncPostBackTimeOut="600" Loadscriptsbeforeui="true">
</asp:ScriptManager>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px; text-align: left;">
        <span class="bodycopy"><span class="title">Safety Qualification</span><br />
        <span class="date">Questionnaire > Assess/Review > Supplier Questionnaire > Score Analysis</span><br />
            <img src="../images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
                </td>
    </tr>
    <tr>
        <td class="pageName" colspan="3" style="height: 17px; text-align: center">
            <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" 
                Text="Close" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dx:ASPxButton>
            <br />
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="3" style="height: 17px; text-align: center">
            <dx:ASPxLabel ID="ASPxLabel1" runat="server" ForeColor="Red">
            </dx:ASPxLabel>
    <dx:aspxgridview id="grid" runat="server" clientinstancename="grid" 
                AutoGenerateColumns="False" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" OnHtmlRowPrepared="grid_HtmlRowPrepared">
<Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowFooter="True" ShowGroupFooter="VisibleAlways"></Settings>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Area" SortIndex="0">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="#" FieldName="QuestionNo" SortIndex="0" SortOrder="Ascending"
                VisibleIndex="1">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Question" VisibleIndex="2">
                <Settings AllowHeaderFilter="False" />
                <CellStyle HorizontalAlign="Left">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Answer" VisibleIndex="3">
                <Settings AllowHeaderFilter="False" />
                <CellStyle HorizontalAlign="Left">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="FileUploaded" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Comments" VisibleIndex="5">
                <Settings AllowHeaderFilter="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Score" VisibleIndex="6">
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ForcedHigh" Visible="False" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
        </Columns>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <AlternatingRow Enabled="True">
            </AlternatingRow>
            <LoadingPanel ImageSpacing="10px">
            </LoadingPanel>
        </Styles>
        <SettingsPager Mode="ShowAllRecords">
            <AllButton Visible="True">
            </AllButton>
        </SettingsPager>
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <StylesEditors>
            <ProgressBar Height="25px">
            </ProgressBar>
        </StylesEditors>
        <GroupSummary>
            <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="Score" SummaryType="Sum"
                        ShowInGroupFooterColumn="Score" />
        </GroupSummary>
        <TotalSummary>
            <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Score"
                         SummaryType="Sum" />
        </TotalSummary>
</dx:aspxgridview>
        </td>
    </tr>
        <tr>
        <td class="pageName" colspan="7" style="height: 30px; text-align: right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
        </tr>
</table>
</asp:Content>

