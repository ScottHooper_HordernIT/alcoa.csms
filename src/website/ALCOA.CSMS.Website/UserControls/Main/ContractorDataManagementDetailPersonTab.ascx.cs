﻿using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.Data;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using OfficeOpenXml;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using dal = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
using log = Repo.CSMS.Common.Log;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContractorDataManagementDetailPersonTab : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        bool isAdmin;
        repo.Hr.IXxhrPeopleDetailsService peopleSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrPeopleDetailsService>();
        repo.Hr.IXxhrCwkHistoryService historySvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrCwkHistoryService>();
        repo.Hr.IXxhrAddTrngService addTrainingSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrAddTrngService>();
        repo.IStoredProcedureService storedProcSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.IStoredProcedureService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);
            moduleInit(Page.IsPostBack);

            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.btnSubmit);
        }

        private void moduleInit(bool postBack)
        {
            isAdmin = auth.RoleId == (int)RoleList.Administrator;
            string CWK_NBR = this.Request.QueryString["CWK_NBR"];

            if (!postBack)
            {
                switch (auth.RoleId)
                {
                    case (int)RoleList.CWKMaintainer:
                    case (int)RoleList.Administrator:
                        //Can access
                        break;

                    case (int)RoleList.CWKEnquiry:
                        //Can access limited
                        this.btnSubmit.Visible = false;
                        this.lblDateOfBirth.Visible = false;
                        this.dtDateOfBirth.Visible = false;
                        this.lblAddressType.Visible = false;
                        this.cbAddressType.Visible = false;
                        this.lblAddress1.Visible = false;
                        this.tbAddress1.Visible = false;
                        this.tbAddress2.Visible = false;
                        this.tbAddress3.Visible = false;
                        this.lblCity.Visible = false;
                        this.tbCity.Visible = false;
                        this.lblState.Visible = false;
                        this.tbState.Visible = false;
                        this.lblPostalCode.Visible = false;
                        this.tbPostalCode.Visible = false;
                        this.lblCountry.Visible = false;
                        this.cbCountry.Visible = false;

                        //Disable all controls (readonly)
                        foreach (Control childControl in this.Controls)
                        {
                            if (childControl.GetType() == typeof(ASPxTextBox) || childControl.GetType() == typeof(ASPxComboBox) || childControl.GetType() == typeof(ASPxDateEdit) || childControl.GetType() == typeof(ASPxRadioButtonList))
                                ((ASPxWebControl)childControl).Enabled = false;
                        }
                        break;

                    default:
                        Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised user")));
                        break;
                }

                if (!string.IsNullOrEmpty(CWK_NBR))
                {
                    //First load
                    if (CWK_NBR.ToLower() == "new")
                    {
                        //Add contractor
                        this.tbContractorNumber.Text = "(auto-generated)";
                    }
                    else
                    {
                        //Update contractor
                        if (!string.IsNullOrEmpty(this.Request.QueryString["EFFECTIVE_START_DATE"]))
                        {
                            try
                            {
                                DateTime EFFECTIVE_START_DATE = System.Convert.ToDateTime(this.Request.QueryString["EFFECTIVE_START_DATE"]).Date;

                                var cwkHistory = this.historySvc.Get(e => e.CWK_NBR == CWK_NBR && e.EFFECTIVE_START_DATE == EFFECTIVE_START_DATE, null);

                                if (cwkHistory != null)
                                {
                                    this.cbTitle.Value = cwkHistory.TITLE;
                                    this.tbFirstName.Text = cwkHistory.FIRST_NAME;
                                    this.tbMiddleName.Text = cwkHistory.MIDDLE_NAMES;
                                    this.tbLastName.Text = cwkHistory.LAST_NAME;
                                    this.dtDateOfBirth.Value = cwkHistory.DOB;
                                    this.tbHomePhone.Text = cwkHistory.PHONE_NO;
                                    this.tbMobilePhone.Text = cwkHistory.MOBILE;
                                    this.tbEmailAddress.Text = cwkHistory.EMAIL;
                                    this.dtOriginalHireDate.Value = cwkHistory.ORIGINAL_DATE_OF_HIRE;
                                    this.tbLoginID.Text = cwkHistory.LOGINID;
                                    this.rbGender.Value = cwkHistory.GENDER;

                                    this.cbAddressType.Value = cwkHistory.ADDR_TYPE;
                                    this.tbAddress1.Text = cwkHistory.ADDR_LINE_1;
                                    this.tbAddress2.Text = cwkHistory.ADDR_LINE_2;
                                    this.tbAddress3.Text = cwkHistory.ADDR_LINE_3;
                                    this.tbCity.Text = cwkHistory.CITY;
                                    this.tbState.Text = cwkHistory.STATE;
                                    this.tbPostalCode.Text = cwkHistory.POSTCODE;
                                    this.cbCountry.Value = cwkHistory.COUNTRY;

                                    this.tbContractorNumber.Text = cwkHistory.CWK_NBR;
                                    this.cbAssignment.Value = cwkHistory.ASSIGNMENT;
                                    this.dtPlacementStartDate.Value = cwkHistory.PLACEMENT_START_DATE;
                                    this.tbAssignmentStatus.Text = cwkHistory.ASSIGNMENT_STATUS;
                                    this.rbAlcoaPrivileges.Value = cwkHistory.ALCOA_PRIV;
                                    this.cbLocation.Value = cwkHistory.LOCATION_CODE;
                                    this.cbAlcoaJob.Value = cwkHistory.POS_DESCR;

                                    this.cbLabourClassification.Value = cwkHistory.LABOUR_CLASS;
                                    this.cbLabourClassification2.Value = cwkHistory.LABOUR_CLASS_2;
                                    this.cbLabourClassification3.Value = cwkHistory.LABOUR_CLASS_3;

                                    this.cbName.Value = cwkHistory.NAME;

                                    this.cbPersonType.Value = cwkHistory.PER_TYPE_ID;
                                    this.tbAssignmentNumber.Text = cwkHistory.ASSIGNMENT_NO;
                                    this.dtAssignmentEffectiveStartDate.Value = cwkHistory.EFFECTIVE_START_DATE;
                                    this.dtAssignmentEffectiveStartDate.Enabled = false;
                                    this.dtAssignmentEffectiveEndDate.Value = cwkHistory.EFFECTIVE_END_DATE;
                                    this.tbAssignmentCategory.Text = cwkHistory.ASSIGNMENT_CAT;

                                    this.tbOldContractorNumber.Text = cwkHistory.OLD_CWK_NBR;
                                    this.tbOrganization.Text = cwkHistory.ORG_CODE;
                                    this.cbAlcoaSupervisor.Value = cwkHistory.SUPERVISOR_ID;
                                    this.tbAlcoaSponsor.Text = cwkHistory.SPONSOR_ID;

                                    this.tbSupplier.Text = cwkHistory.SUPPLY_SUP;
                                    this.tbNonPrefSupplier.Text = cwkHistory.NON_PREFF_SUPP;
                                    this.tbPeopleGroup.Text = cwkHistory.PEOPLE_GROUP;
                                    this.dtProjectedContractEndDate.Value = cwkHistory.PROJ_CONTRACT_END_DATE;
                                    this.cbSupplierSupervisorOnSite.Value = cwkHistory.SUPPLIER_SUP_ON_SITE;
                                }
                                else
                                {
                                    Elmah.ErrorSignal.FromContext(Context).Raise(new Exception("moduleInit.no_history"));
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                            }
                        }
                    }
                }
            }
            else
            {
                //Postback
            }
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //Saving
            try
            {
                string CWK_NBR = this.Request.QueryString["CWK_NBR"];
                if (CWK_NBR.ToLower() == "new")
                {
                    //Add contractor
                    var people = new XXHR_PEOPLE_DETAILS();
                    FillCommonPeople(people);

                    var cwkHistory = new XXHR_CWK_HISTORY();
                    FillCommonHistory(cwkHistory);

                    //Generate contractor number
                    int NEW_CWK_NBR = (this.storedProcSvc.SP_XXHR_PEOPLE_DETAILS_SelectMaxCwkNbr() ?? 0) + 1;
                    people.EMPLID = NEW_CWK_NBR.ToString().PadLeft(6, '0');
                    cwkHistory.CWK_NBR = NEW_CWK_NBR.ToString().PadLeft(6, '0');

                    this.peopleSvc.Insert(people, false);
                    this.historySvc.Insert(cwkHistory);  //Nb. SaveChanges only called once here

                    Response.Redirect("~/ContractorDataManagement.aspx");
                }
                else
                {
                    //Update contractor
                    if (!string.IsNullOrEmpty(this.Request.QueryString["EFFECTIVE_START_DATE"]))
                    {
                        DateTime EFFECTIVE_START_DATE = System.Convert.ToDateTime(this.Request.QueryString["EFFECTIVE_START_DATE"]).Date;

                        var people = this.peopleSvc.Get(i => i.EMPLID == CWK_NBR && i.SERVICE_DT == EFFECTIVE_START_DATE, null);
                        var cwkHistory = this.historySvc.Get(i => i.CWK_NBR == CWK_NBR && i.EFFECTIVE_START_DATE == EFFECTIVE_START_DATE, null);

                        if (people != null && cwkHistory != null)
                        {
                            FillCommonPeople(people);
                            FillCommonHistory(cwkHistory);

                            this.peopleSvc.Update(people, false);
                            this.historySvc.Update(cwkHistory);  //Nb. SaveChanges only called once here

                            Response.Redirect("~/ContractorDataManagement.aspx");
                        }
                        else
                        {
                            PopUpErrorMessage("Cannot find person or contractor in database");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);

                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = ex.InnerException.Message;
                }

                PopUpErrorMessage(msg);
            }
        }

        private void FillCommonPeople(XXHR_PEOPLE_DETAILS cwkPeople) 
        {
            cwkPeople.EMPLID = this.tbContractorNumber.Text;
            cwkPeople.PER_TYPE_ID = "C";
            cwkPeople.FULL_NAME = string.Format("{0}, {1}", this.tbLastName.Text, this.tbFirstName.Text);
            //cwkPeople.SGNAME = "";
            cwkPeople.LAST_NAME_SRCH = this.tbLastName.Text;
            cwkPeople.FIRST_NAME_SRCH = this.tbFirstName.Text;
            cwkPeople.SUPERVISOR_ID = (this.cbAlcoaSupervisor.Value != null ? this.cbAlcoaSupervisor.Value.ToString() : null);
            cwkPeople.POS_DESCR = (this.cbAlcoaJob.Value != null ? this.cbAlcoaJob.Value.ToString() : null);
            //cwkPeople.PAYGROUP = "";
            cwkPeople.LOCATION = (this.cbLocation.Value != null ? this.cbLocation.Value.ToString() : null);
            //cwkPeople.COMPANY = "";
            cwkPeople.SEX = (this.rbGender.Value != null ? this.rbGender.Value.ToString() : null);
            //cwkPeople.LBC = "";
            //cwkPeople.DEPT_CD = "";
            //cwkPeople.ACCOUNT = "";
            cwkPeople.SERVICE_DT = Convert.ToDateTime(this.dtAssignmentEffectiveStartDate.Value).Date;
            cwkPeople.TERMINATION_DT = Convert.ToDateTime(this.dtAssignmentEffectiveEndDate.Value).Date;
            cwkPeople.AOA_PREF_NAME = this.tbFirstName.Text;
            cwkPeople.LOGINID = this.tbLoginID.Text;
            cwkPeople.ORG_CODE = this.tbOrganization.Text;
            //cwkPeople.DEPT_ORG_CODE = "";
            //cwkPeople.OP_CENTRE = "";
            //cwkPeople.CWK_VENDOR_NO = "";
            //cwkPeople.CWK_VENDOR_NAME = "";
            cwkPeople.CWK_NON_PREFERRED_SUPP_NAME = this.tbNonPrefSupplier.Text;
            cwkPeople.CWK_SPONSOR_ID = this.tbAlcoaSponsor.Text;
            cwkPeople.CWK_SUPP_SUPERVISOR_ID = this.tbSupplier.Text;
            //cwkPeople.CWL_PRIVILEGE_FLAG = "";
            cwkPeople.LAST_UPDATE_DATE = DateTime.Now;
        }

        private void FillCommonHistory(XXHR_CWK_HISTORY cwkHistory)
        {
            cwkHistory.TITLE = (this.cbTitle.Value != null ? this.cbTitle.Value.ToString() : null);
            cwkHistory.FIRST_NAME = this.tbFirstName.Text;
            cwkHistory.MIDDLE_NAMES = this.tbMiddleName.Text;
            cwkHistory.LAST_NAME = this.tbLastName.Text;
            cwkHistory.FULL_NAME = string.Format("{0}, {1}", this.tbLastName.Text, this.tbFirstName.Text);

            if (this.dtDateOfBirth.Value != null)
            {
                cwkHistory.DOB = Convert.ToDateTime(this.dtDateOfBirth.Value).Date;
            }
            else
            {
                cwkHistory.DOB = null;
            }

            cwkHistory.PHONE_NO = this.tbHomePhone.Text;
            cwkHistory.MOBILE = this.tbMobilePhone.Text;
            cwkHistory.EMAIL = this.tbEmailAddress.Text;

            if (this.dtOriginalHireDate.Value != null)
            {
                cwkHistory.ORIGINAL_DATE_OF_HIRE = Convert.ToDateTime(this.dtOriginalHireDate.Value).Date;
            }
            else
            {
                cwkHistory.ORIGINAL_DATE_OF_HIRE = null;
            }

            cwkHistory.LOGINID = this.tbLoginID.Text;
            cwkHistory.GENDER = (this.rbGender.Value != null ? this.rbGender.Value.ToString() : null);

            cwkHistory.ADDR_TYPE = (this.cbAddressType.Value != null ? this.cbAddressType.Value.ToString() : null);
            cwkHistory.ADDR_LINE_1 = this.tbAddress1.Text;
            cwkHistory.ADDR_LINE_2 = this.tbAddress2.Text;
            cwkHistory.ADDR_LINE_3 = this.tbAddress3.Text;
            cwkHistory.CITY = this.tbCity.Text;
            cwkHistory.STATE = this.tbState.Text;
            cwkHistory.POSTCODE = this.tbPostalCode.Text;
            cwkHistory.COUNTRY = (this.cbCountry.Value != null ? this.cbCountry.Value.ToString() : null);

            cwkHistory.CWK_NBR = this.tbContractorNumber.Text;
            cwkHistory.ASSIGNMENT = (this.cbAssignment.Value != null ? this.cbAssignment.Value.ToString() : null);

            if (this.dtPlacementStartDate.Value != null)
            {
                cwkHistory.PLACEMENT_START_DATE = Convert.ToDateTime(this.dtPlacementStartDate.Value).Date;
            }
            else
            {
                cwkHistory.PLACEMENT_START_DATE = null;
            }

            cwkHistory.ASSIGNMENT_STATUS = this.tbAssignmentStatus.Text;
            cwkHistory.ALCOA_PRIV = (this.rbAlcoaPrivileges.Value != null ? this.rbAlcoaPrivileges.Value.ToString() : null);
            cwkHistory.LOCATION_CODE = (this.cbLocation.Value != null ? this.cbLocation.Value.ToString() : null);
            cwkHistory.POS_DESCR = (this.cbAlcoaJob.Value != null ? this.cbAlcoaJob.Value.ToString() : null);

            cwkHistory.LABOUR_CLASS = (this.cbLabourClassification.Value != null ? this.cbLabourClassification.Value.ToString() : null);
            cwkHistory.LABOUR_CLASS_2 = (this.cbLabourClassification2.Value != null ? this.cbLabourClassification2.Value.ToString() : null);
            cwkHistory.LABOUR_CLASS_3 = (this.cbLabourClassification3.Value != null ? this.cbLabourClassification3.Value.ToString() : null);

            cwkHistory.NAME = (this.cbName.Value != null ? this.cbName.Value.ToString() : null);

            cwkHistory.PER_TYPE_ID = (this.cbPersonType.Value != null ? this.cbPersonType.Value.ToString() : null);
            cwkHistory.ASSIGNMENT_NO = this.tbAssignmentNumber.Text;

            cwkHistory.SERVICE_DATE = Convert.ToDateTime(this.dtAssignmentEffectiveStartDate.Value).Date;
            cwkHistory.EFFECTIVE_START_DATE = Convert.ToDateTime(this.dtAssignmentEffectiveStartDate.Value).Date;
            cwkHistory.EFFECTIVE_END_DATE = Convert.ToDateTime(this.dtAssignmentEffectiveEndDate.Value).Date;
            cwkHistory.ASSIGNMENT_CAT = this.tbAssignmentCategory.Text;

            cwkHistory.OLD_CWK_NBR = this.tbOldContractorNumber.Text;
            cwkHistory.ORG_CODE = this.tbOrganization.Text;
            cwkHistory.SUPERVISOR_ID = (this.cbAlcoaSupervisor.Value != null ? this.cbAlcoaSupervisor.Value.ToString() : null);
            cwkHistory.SPONSOR_ID = this.tbAlcoaSponsor.Text;

            cwkHistory.SUPPLY_SUP = this.tbSupplier.Text;
            cwkHistory.NON_PREFF_SUPP = this.tbNonPrefSupplier.Text;
            cwkHistory.PEOPLE_GROUP = this.tbPeopleGroup.Text;

            if (this.dtProjectedContractEndDate.Value != null)
            {
                cwkHistory.PROJ_CONTRACT_END_DATE = Convert.ToDateTime(this.dtProjectedContractEndDate.Value).Date;
            }
            else
            {
                cwkHistory.PROJ_CONTRACT_END_DATE = null;
            }

            cwkHistory.SUPPLIER_SUP_ON_SITE = (this.cbSupplierSupervisorOnSite.Value != null ? this.cbSupplierSupervisorOnSite.Value.ToString() : null);

            cwkHistory.LAST_UPDATE_DATE = DateTime.Now;
            cwkHistory.LAST_UPDATED_BY = auth.UserLogon;
        }

        protected void PopUpErrorMessage(string errormsg)
        {
            //ASPxGridView1.Visible = false;
            PopupWindow pcWindow = new PopupWindow(errormsg);
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
            ASPxPopupControl1.Windows.Add(pcWindow);
            //WebChartControl1.Visible = false;
        }

        protected void btnTestData_Click(object sender, EventArgs e)
        {
            this.cbTitle.Value = "Mr";
            this.tbFirstName.Text = "Firth";
            this.tbMiddleName.Text = "E";
            this.tbLastName.Text = "Longren";
            this.dtDateOfBirth.Value = DateTime.Now.AddYears(-38);
            this.tbHomePhone.Text = "0352221672";
            this.tbMobilePhone.Text = "0400000000";
            this.tbEmailAddress.Text = "test@test.com";
            this.dtOriginalHireDate.Value = DateTime.Now;
            this.tbLoginID.Text = "phx88fl";
            this.rbGender.Value = "M";

            this.cbAddressType.Value = "Work";
            this.tbAddress1.Text = "21 Jump Street";
            this.tbAddress2.Text = "";
            this.tbAddress3.Text = "";
            this.tbCity.Text = "Geelong";
            this.tbState.Text = "Vic";
            this.tbPostalCode.Text = "3220";
            this.cbCountry.Value = "Australia";

            this.cbAssignment.Value = "CONTRACTOR.GENERAL";
            this.dtPlacementStartDate.Value = DateTime.Now;
            this.tbAssignmentStatus.Text = "Ok";
            this.rbAlcoaPrivileges.Value = "No";
            this.cbLocation.Value = "AU KWI Kwinana Refinery";
            this.cbAlcoaJob.Value = "ADMIN.BU-INACTIVE.Associate Director.";

            this.cbLabourClassification.Value = "Admin Support";
            this.cbLabourClassification2.Value = "";
            this.cbLabourClassification3.Value = "";

            this.cbName.Value = "03531-29601 Sales";

            this.tbAssignmentNumber.Text = "123";
            this.dtAssignmentEffectiveStartDate.Value = DateTime.Now;
            this.dtAssignmentEffectiveEndDate.Value = DateTime.Now.AddDays(7);
            this.tbAssignmentCategory.Text = "123";

            this.tbOldContractorNumber.Text = "";
            this.tbOrganization.Text = "AOA";
            this.cbAlcoaSupervisor.Value = "000004";
            this.tbAlcoaSponsor.Text = "123";

            this.tbSupplier.Text = "123";
            this.tbNonPrefSupplier.Text = "123";
            this.tbPeopleGroup.Text = "123";
            this.dtProjectedContractEndDate.Value = DateTime.Now.AddDays(7);
            this.cbSupplierSupervisorOnSite.Value = "000004";
        }
    }
}
