﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsaCompanySiteCategoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CsaCompanySiteCategoryDataSourceDesigner))]
	public class CsaCompanySiteCategoryDataSource : ReadOnlyDataSource<CsaCompanySiteCategory>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryDataSource class.
		/// </summary>
		public CsaCompanySiteCategoryDataSource() : base(new CsaCompanySiteCategoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsaCompanySiteCategoryDataSourceView used by the CsaCompanySiteCategoryDataSource.
		/// </summary>
		protected CsaCompanySiteCategoryDataSourceView CsaCompanySiteCategoryView
		{
			get { return ( View as CsaCompanySiteCategoryDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsaCompanySiteCategoryDataSourceView class that is to be
		/// used by the CsaCompanySiteCategoryDataSource.
		/// </summary>
		/// <returns>An instance of the CsaCompanySiteCategoryDataSourceView class.</returns>
		protected override BaseDataSourceView<CsaCompanySiteCategory, Object> GetNewDataSourceView()
		{
			return new CsaCompanySiteCategoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsaCompanySiteCategoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsaCompanySiteCategoryDataSourceView : ReadOnlyDataSourceView<CsaCompanySiteCategory>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsaCompanySiteCategoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsaCompanySiteCategoryDataSourceView(CsaCompanySiteCategoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsaCompanySiteCategoryDataSource CsaCompanySiteCategoryOwner
		{
			get { return Owner as CsaCompanySiteCategoryDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsaCompanySiteCategoryService CsaCompanySiteCategoryProvider
		{
			get { return Provider as CsaCompanySiteCategoryService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CsaCompanySiteCategoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsaCompanySiteCategoryDataSource class.
	/// </summary>
	public class CsaCompanySiteCategoryDataSourceDesigner : ReadOnlyDataSourceDesigner<CsaCompanySiteCategory>
	{
	}

	#endregion CsaCompanySiteCategoryDataSourceDesigner

	#region CsaCompanySiteCategoryFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaCompanySiteCategoryFilter : SqlFilter<CsaCompanySiteCategoryColumn>
	{
	}

	#endregion CsaCompanySiteCategoryFilter

	#region CsaCompanySiteCategoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaCompanySiteCategoryExpressionBuilder : SqlExpressionBuilder<CsaCompanySiteCategoryColumn>
	{
	}
	
	#endregion CsaCompanySiteCategoryExpressionBuilder		
}

