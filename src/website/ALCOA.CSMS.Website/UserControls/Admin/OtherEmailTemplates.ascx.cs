﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.Text;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class OtherEmailTemplates : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
        repo.IEmailTemplateVariableService emailTemplateVariableService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateVariableService>();
        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                List<model.EmailTemplate> et = emailTemplateService.GetMany(null, null,null, null);
                cbTemplate.DataSource = et;
                cbTemplate.DataBind();
                Hide(true);
            }
            //BindRecipients();
        }

        protected void Load()
        {
            int emailTemplateId = (int)cbTemplate.SelectedItem.Value;
            model.EmailTemplate t = emailTemplateService.Get(i => i.EmailTemplateId == emailTemplateId, null);
            if (t != null)
            {
                heBody.Html = "";
                if (t.EmailBody != null)
                {
                    heBody.Html = Encoding.ASCII.GetString(t.EmailBody);
                }
                tbSubject.Text = t.EmailTemplateSubject;
            }
            List<model.EmailTemplateVariable> tv = emailTemplateVariableService.GetMany(null, id => id.EmailTemplateId == emailTemplateId,null,null);
            grdVariables.DataSource = tv;
            grdVariables.DataBind();
            Hide(false);
        }
        protected void Save(bool close)
        {
            int emailTemplateId = (int)cbTemplate.SelectedItem.Value;

            model.EmailTemplate t = emailTemplateService.Get(i => i.EmailTemplateId == emailTemplateId, null);
            ASCIIEncoding encoding = new ASCIIEncoding();
            t.EmailBody = (byte[])encoding.GetBytes(heBody.Html);
            t.EmailTemplateSubject = tbSubject.Text;
            emailTemplateService.Update(t);

            if (close) Close();
        }
        protected void Close()
        {
            heBody.Html = "";
            btnLoad.Enabled = true;
            cbTemplate.Enabled = true;

            Hide(true);
        }
        protected void Hide(bool hide)
        {
            if (hide)
            {
                btnSave.Enabled = false;
                btnSaveClose.Enabled = false;

                heBody.Visible = false;

                cbTemplate.Enabled = true;
                btnLoad.Enabled = true;
                Panel1.Visible = false;
            }
            else
            {
                btnSave.Enabled = true;
                btnSaveClose.Enabled = true;

                heBody.Visible = true;

                cbTemplate.Enabled = false;
                btnLoad.Enabled = false;

                Panel1.Visible = true;
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            Load();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save(false);
        }
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            Save(true);
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}