﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileDbAuditService
    {
        FileDbAudit Get(Expression<Func<FileDbAudit, bool>> where, List<Expression<Func<FileDbAudit, object>>> includes);
        List<FileDbAudit> GetMany(int? top, Expression<Func<FileDbAudit, bool>> where, Func<IQueryable<FileDbAudit>, IOrderedQueryable<FileDbAudit>> orderBy, List<Expression<Func<FileDbAudit, object>>> includes);
        List<FileDbAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileDbAudit, bool>> where, Func<IQueryable<FileDbAudit>, IOrderedQueryable<FileDbAudit>> orderBy, List<Expression<Func<FileDbAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileDbAudit, bool>> where, List<Expression<Func<FileDbAudit, object>>> includes);
        void Insert(FileDbAudit item, bool saveChanges = true);
        void Update(FileDbAudit item, bool saveChanges = true);
        void Delete(FileDbAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileDbAuditService : IFileDbAuditService
    {
        private readonly IRepository<FileDbAudit> _repository;

        public FileDbAuditService(IRepository<FileDbAudit> repository)
        {
            this._repository = repository;
        }

        public FileDbAudit Get(Expression<Func<FileDbAudit, bool>> where, List<Expression<Func<FileDbAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileDbAudit> GetMany(int? top, Expression<Func<FileDbAudit, bool>> where, Func<IQueryable<FileDbAudit>, IOrderedQueryable<FileDbAudit>> orderBy, List<Expression<Func<FileDbAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileDbAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileDbAudit, bool>> where, Func<IQueryable<FileDbAudit>, IOrderedQueryable<FileDbAudit>> orderBy, List<Expression<Func<FileDbAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileDbAudit, bool>> where, List<Expression<Func<FileDbAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileDbAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileDbAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileDbAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileDbAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileDbAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileDbAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
