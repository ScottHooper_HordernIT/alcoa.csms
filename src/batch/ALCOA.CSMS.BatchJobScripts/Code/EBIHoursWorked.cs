﻿using ALCOA.CSMS.Batch.Common.Log;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ALCOA.CSMS.BatchJobScripts
{
    public class EBIHoursWorked
    {
        static string sqldb = "";
        static ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
        public static void UpdateHoursWorkedTable(int EbiId,bool live)
        {
            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            sqldb = conString.ConnectionString;
            string sqlHoursWorked = string.Empty;
            if (live == true)
                sqlHoursWorked = "SELECT DISTINCT CLOCKID FROM EBI WHERE SOURCE IS NOT NULL AND CLOCKID IS NOT NULL AND EBIID >" + EbiId;
            else
                sqlHoursWorked = "SELECT DISTINCT CLOCKID FROM EBI WHERE SOURCE IS NOT NULL AND CLOCKID IS NOT NULL";
            try
            {
                using (SqlConnection conn = new SqlConnection(sqldb))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sqlHoursWorked, conn);

                    Log4NetService.LogInfo("Reading Ebi Data..");

                    cmd.CommandTimeout = 60000;
                    SqlDataReader r = cmd.ExecuteReader();
                    Log4NetService.LogInfo("Read...");

                    while (r.Read())
                    {
                        try
                        {
                            int Clockid = Convert.ToInt32(r["ClockId"]);
                            if(Clockid!=null)
                                CalculateData(Clockid);
                        }
                        catch (Exception ex)
                        {
                            Log4NetService.LogInfo("Error In Row : " + ex.Message);
                        }
                    }
                    Log4NetService.LogInfo("Finished.\n\n");
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo("Error occurred while reading from CSMS Ebi data " + ex.Message);
            }
        }

        public static void CalculateData(int Person)
        {
            long lastClock = 0;
            DateTime lastEntryDate;
            DateTime lastEntry;
            DateTime lastExitDate;
            DateTime lastExit;
            DateTime lastRecord;
            string tmpGate;

            //' Find the number of records to process
            //int iTestClock = 241889;
            //string iTestDate = "#1/19/2012#";
            //' Reset the flags
            lastEntryDate = DateTime.Now.AddYears(-1000);
            lastExitDate = DateTime.Now.AddYears(-1000);
            double lastDiff = 0;
            int iPeriods = 0;
            int rows = 0;
            string lastDirection = string.Empty;
            DateTime lastDate;

            lastDate = DateTime.Now.AddYears(-1000);
            lastExit = DateTime.Now.AddYears(-1000);
            lastEntry = DateTime.Now.AddYears(-1000);

            DateTime[] aEntryDate;
            DateTime[] aExitDate;
            double[] aDiff;
            int[] aClock;
            bool[] aException;
            string[] aSite;
            string[] aFullName;
            string[] aVendorNo;

            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            sqldb = conString.ConnectionString;

            using (SqlConnection conn = new SqlConnection(sqldb))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(String.Format("Delete from EbiHoursWorked where ClockId={0}",Person), conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            using (SqlConnection conn2 = new SqlConnection(sqldb))
            {
                conn2.Open();
                SqlCommand cmd2 = new SqlCommand(String.Format("Select Count(*) As ClockRows from Ebi where ClockId={0} and Source Is Not Null",Person), conn2);
                SqlDataReader r2 = cmd2.ExecuteReader();
                while (r2.Read())
                {
                    rows = (int)r2["ClockRows"];
                }
                conn2.Close();
            }

            try
            {
                using (SqlConnection conn1 = new SqlConnection(sqldb))
                {
                    conn1.Open();
                    SqlCommand cmd1 = new SqlCommand(String.Format("Select ClockId,SwipeDateTime,Source,SwipeSite,FullName,Vendor_Number from Ebi where Clockid={0} and Source Is Not Null order by SwipeDateTime ASC",Person), conn1);
                    SqlDataReader r1 = cmd1.ExecuteReader();
                    Log4NetService.LogInfo("Updating data for the ClockId= " + Person);
                            aEntryDate  =new DateTime[rows+3];
                            aExitDate = new DateTime[rows+3];
                            aDiff = new double[rows+3];
                            aClock = new int[rows+3];
                            aException = new bool[rows+3];
                            aSite = new string[rows + 3];
                            aFullName = new string[rows + 3];
                            aVendorNo = new string[rows + 3];

                    while (r1.Read())
                    {
                        try
                        {
                            DateTime tmpDate = (DateTime)r1["SwipedateTime"];
                            tmpGate = (string)r1["Source"];
                            int tmpClock = Convert.ToInt32(r1["ClockId"]);
                            string tmpSite = Convert.ToString(r1["SwipeSite"]);
                            string tmpFullName = Convert.ToString(r1["FullName"]);
                            string tmpVendorNo = Convert.ToString(r1["Vendor_Number"]);

                            DateTime tmpDateTime = tmpDate;

                            if(iPeriods==0 || tmpClock!=lastClock)
                            {
                                iPeriods = iPeriods + 1;
                                aEntryDate[iPeriods] = tmpDate;
                                aExitDate[iPeriods] = tmpDate;

                                aDiff[iPeriods] = 0;
                                aClock[iPeriods] = tmpClock;

                                aSite[iPeriods] = tmpSite;
                                aFullName[iPeriods] = tmpFullName;
                                aVendorNo[iPeriods] = tmpVendorNo;

                                lastEntryDate = tmpDate;
                                lastExitDate = tmpDate;
                                lastEntry = lastEntryDate;
                                lastExit = lastExitDate;

                                lastDate = tmpDate;

                                lastClock = tmpClock;
                                lastDiff = 0;
                                lastDirection = "In";
                            }
                            else if(IsExit(tmpGate))
                            {
                                //' How much time has elapsed from the last exit
                                double txtElapsed=0;
                                int hourOfLastEntry = lastEntryDate.Hour;

                                txtElapsed = (tmpDateTime - lastEntry).TotalHours;

                                if ((lastDirection == "Out" && tmpDate != lastDate && txtElapsed > 14.1) || (lastDirection == "In" && txtElapsed > 20))
                                {
                                    iPeriods = iPeriods + 1;
                                    aEntryDate[iPeriods] = tmpDate;

                                    aExitDate[iPeriods] = tmpDate;

                                    aDiff[iPeriods] = 0;
                                    aClock[iPeriods] = tmpClock;
                                    lastEntryDate = tmpDate;

                                    aSite[iPeriods] = tmpSite;
                                    aFullName[iPeriods] = tmpFullName;
                                    aVendorNo[iPeriods] = tmpVendorNo;

                                    lastExitDate = tmpDate;
                                    lastDate = tmpDate;
                                    lastEntry = lastEntryDate;
                                    lastExit = lastExitDate;
                                    lastClock = tmpClock;
                                    lastDiff = 0;

                                    if(tmpDate.Hour < 18)
                                    {
                                        lastDirection = "Out";
                                        //lastEntryTime = #7:00:00 AM#;
                                        //lastEntry = lastEntryDate + lastEntryTime;
                                        lastEntry=new DateTime(lastEntryDate.Year,lastEntryDate.Month,lastEntryDate.Day,7,0,0);
                                        lastEntryDate = lastEntry;
                                        aException[iPeriods] = true;
                                    }
                                    else if(tmpDate.Hour >= 18 && txtElapsed > 15)
                                    {
                                        lastDirection = "Out";
                                        //lastEntryTime = #7:00:00 AM#;
                                        //lastEntry = lastEntryDate + lastEntryTime;
                                        lastEntry = new DateTime(lastEntryDate.Year, lastEntryDate.Month, lastEntryDate.Day, 7, 0, 0);
                                        lastEntryDate = lastEntry;
                                        aException[iPeriods] = true;
                                    }
                                    else
                                    {
                                        lastDirection = "In";
                                        aException[iPeriods] = true;

                                        iPeriods = iPeriods + 1;
                                        aEntryDate[iPeriods] = tmpDate;

                                        aExitDate[iPeriods] = tmpDate;

                                        aDiff[iPeriods] = 0;
                                        aClock[iPeriods] = tmpClock;

                                        aSite[iPeriods] = tmpSite;
                                        aFullName[iPeriods] = tmpFullName;
                                        aVendorNo[iPeriods] = tmpVendorNo;
                                    }
                                }
                                else
                                {
                                    aExitDate[iPeriods] = tmpDate;
                                    aDiff[iPeriods] = (tmpDateTime - lastEntry).TotalHours;

                                    lastDiff = (tmpDateTime - lastEntryDate).TotalHours;
                                    lastDate = tmpDate;
                                    lastExitDate = tmpDate;
                                    lastExit = lastExitDate;
                                    lastDirection = "Out";

                                    aSite[iPeriods] = tmpSite;
                                    aFullName[iPeriods] = tmpFullName;
                                    aVendorNo[iPeriods] = tmpVendorNo;
                                }
                            }
                            else if(isEntry(tmpGate))
                            {
                                //' Either create a new record or ignore
                                //' How much time has elapsed from the last exit
                                double txtElapsed = 0;
                                txtElapsed = (tmpDateTime - lastEntry).TotalHours;
                                int hourOfLastEntry = lastEntryDate.Hour;

                                if (((lastDirection == "Out") && ((tmpDate == lastDate) && (txtElapsed > 14))) ||
                                    (lastDirection == "In" && hourOfLastEntry < 11 && tmpDate != lastDate) ||
                                        (lastDirection == "In" && txtElapsed > 14.1 && tmpDate != lastDate) ||
                                            (lastDirection == "Out" && ((tmpDate != lastDate) && (txtElapsed > 14.1))) )
                                {
                                    if (lastDirection == "In" && txtElapsed > 14.1 && tmpDate != lastDate)
                                    {
                                        if(txtElapsed > 20 && hourOfLastEntry > 11)
                                        {
                                            aException[iPeriods] = true;
                                            iPeriods = iPeriods + 1;
                                            aEntryDate[iPeriods] = aEntryDate[iPeriods - 1];
                                            aExitDate[iPeriods] = Convert.ToDateTime(aExitDate[iPeriods - 1]).AddDays(1);
                                            aExitDate[iPeriods] = aExitDate[iPeriods].AddHours(-12);
                                            aDiff[iPeriods] = 0;
                                            aClock[iPeriods] = tmpClock;
                                            aException[iPeriods] = true;

                                            aSite[iPeriods] = tmpSite;
                                            aFullName[iPeriods] = tmpFullName;
                                            aVendorNo[iPeriods] = tmpVendorNo;
                                        }
                                    }
                                    iPeriods = iPeriods + 1;
                                    aEntryDate[iPeriods] = tmpDate;
                                    aExitDate[iPeriods] = tmpDate;

                                    aDiff[iPeriods] = 0;
                                    aClock[iPeriods] = tmpClock;
                                    lastDate = tmpDate;
                                    lastEntryDate = tmpDate;
                                    lastExitDate = tmpDate;

                                    aSite[iPeriods] = tmpSite;
                                    aFullName[iPeriods] = tmpFullName;
                                    aVendorNo[iPeriods] = tmpVendorNo;

                                    lastEntry = lastEntryDate;
                                    lastExit = lastExitDate;
                                    lastClock = tmpClock;
                                    lastDiff = 0;
                                    lastDirection = "In";
                                }
                                else
                                {
                                    //' check the last record processed
                                    if(lastExit > lastEntry)
                                    {
                                        lastRecord = lastExit;
                                    }
                                    else
                                    {
                                        lastRecord = lastEntry;
                                    }
                                    double lastElapsed = 0;

                                    lastElapsed = (tmpDate - lastRecord).TotalHours;
                                    if(lastDirection == "Out" && txtElapsed > 11 && lastElapsed > 8 && tmpDate.Hour >= 18)
                                    {
                                            iPeriods = iPeriods + 1;
                                            aEntryDate[iPeriods] = tmpDate;
                                            DateTime tmpTime = tmpDate.AddDays(1);
                                            tmpTime = tmpTime.AddHours(-12);
                                            aExitDate[iPeriods] = tmpTime;

                                            aSite[iPeriods] = tmpSite;
                                            aFullName[iPeriods] = tmpFullName;
                                            aVendorNo[iPeriods] = tmpVendorNo;

                                            aDiff[iPeriods] = 0;
                                            aClock[iPeriods] = tmpClock;
                                            aException[iPeriods] = true;
                                            lastDirection = "In";
                                    }
                                    else
                                    {
                                            aExitDate[iPeriods] = tmpDate;
                                            aDiff[iPeriods] = (tmpDateTime - lastEntry).TotalHours;
                                            lastDate = tmpDate;
                                            lastExitDate = tmpDate;
                                            lastDiff = (tmpDateTime - lastEntry).TotalHours;

                                            aSite[iPeriods] = tmpSite;
                                            aFullName[iPeriods] = tmpFullName;
                                            aVendorNo[iPeriods] = tmpVendorNo;

                                            lastExit = lastExitDate;
                                            lastDirection = "Out";
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Log4NetService.LogInfo(ex.Message);
                        }
                    }

                    if (iPeriods >= 1)
                    {
                        for (int i = 1; i <= iPeriods; i++)
                        {
                            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                            {
                                using (SqlCommand cmd = new SqlCommand("EbiHoursWorked_Insert", conn))
                                {
                                    int EbiYear = Convert.ToDateTime(aEntryDate[i]).Year;
                                    int EbiMonth = Convert.ToDateTime(aEntryDate[i]).Month;
                                    double TotalTime = (Convert.ToDateTime(aExitDate[i]) - Convert.ToDateTime(aEntryDate[i])).TotalHours;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add(new SqlParameter("@ClockId", Convert.ToInt32(aClock[i])));
                                    cmd.Parameters.Add(new SqlParameter("@EntryDateTime", Convert.ToDateTime(aEntryDate[i])));
                                    cmd.Parameters.Add(new SqlParameter("@ExitDateTime", Convert.ToDateTime(aExitDate[i])));
                                    cmd.Parameters.Add(new SqlParameter("@TotalTime", TotalTime));
                                    cmd.Parameters.Add(new SqlParameter("@Year", EbiYear));
                                    cmd.Parameters.Add(new SqlParameter("@Month", EbiMonth));
                                    cmd.Parameters.Add(new SqlParameter("@Doubt", Convert.ToBoolean(aException[i])));
                                    cmd.Parameters.Add(new SqlParameter("@Site", Convert.ToString(aSite[i])));
                                    cmd.Parameters.Add(new SqlParameter("@Fullname", Convert.ToString(aFullName[i])));
                                    cmd.Parameters.Add(new SqlParameter("@Vendor_Number", Convert.ToString(aVendorNo[i])));

                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                }
                            }
                        }
                    }
                    Log4NetService.LogInfo(" Updated...");
                    conn1.Close();
                }
            }
            catch (Exception ex)
            {
                Log4NetService.LogInfo(ex.Message);
            }
        }

        public static bool isEntry(string Source)
        {
            string isEntrystr = string.Empty;
            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select Top 1 IsEntry from EbiGateDetails where Source='"+Source+"'", conn))
                {
                    conn.Open();
                    Object isEntryobj = cmd.ExecuteScalar();
                    isEntrystr = Convert.ToString(isEntryobj);
                }
            }
            if (isEntrystr.ToLower() == "true")
            {
                return true;
            }
            else
                return false;
        }
        
        public static bool IsExit(string Source)
        {
            string isExitstr = string.Empty;
            using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Select Top 1 IsExit from EbiGateDetails where Source='" + Source+"'", conn))
                {
                    conn.Open();
                    Object isExitobj = cmd.ExecuteScalar();

                    isExitstr = Convert.ToString(isExitobj);

                }
            }
            if (isExitstr.ToLower() == "true")
            {
                return true;
            }
            else
                return false;
        }
    }
}
