

/****** Object:  StoredProcedure [dbo].[_CSA_DashBoard]    Script Date: 3/08/2015 1:33:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_CSA_DashBoard]                      
(                      
 @SiteId int,                      
 @Month int,                      
 @Year int                      
)                      
AS            
DECLARE @CASEmbeddedRequired INT, @CSAEmbeddedRequiredToDate INT, @CSANonEmbedded1TotalRequired INT,    
@CSANonEmbedded1RequiredToDate INT,@EmbeddedActualCompleted INT, @NonEmbedded1ActualCompleted INT    
  
IF (@SiteId > 0)                     
 BEGIN          
  -----          
  (SELECT @CASEmbeddedRequired = COUNT(DISTINCT(B.CompanyId))          
  FROM CompanySiteCategoryStandard B          
  WHERE B.SiteId = @SiteId    
  AND B.CompanySiteCategoryId = 1)           
  ------   
              
  (Select @CSAEmbeddedRequiredToDate = dbo.GetCSAEmbededReqToDateDB(@SiteId, @Month, @Year))          
            
  ----          
            
  (SELECT @CSANonEmbedded1TotalRequired = COUNT(DISTINCT(B.CompanyId))          
  FROM CompanySiteCategoryStandard B          
  WHERE B.SiteId = @SiteId    
  AND B.CompanySiteCategoryId = 2)            
            
  ----          
            
  (Select @CSANonEmbedded1RequiredToDate = dbo.GetCSANonEmbeded1ReqToDateDB(@SiteId, @Month, @Year))          
            
  ----          
            
  (SELECT @EmbeddedActualCompleted = COUNT(DISTINCT(A.CompanyId))          
  FROM CSA A LEFT OUTER JOIN CompanySiteCategoryStandard B          
  ON A.CompanyId = B.CompanyId AND A.SiteId = B.SiteId      
  WHERE B.SiteId = @SiteId    
  AND A.[Year] = @Year           
  --AND A.QtrId = @VarQtr           
  AND B.CompanySiteCategoryId = 1          
  AND A.TotalRating > 0)          
            
  ----          
            
  (SELECT @NonEmbedded1ActualCompleted = COUNT(DISTINCT(A.CompanyId))          
  FROM CSA A LEFT OUTER JOIN CompanySiteCategoryStandard B          
  ON A.CompanyId = B.CompanyId AND A.SiteId = B.SiteId      
  WHERE B.SiteId = @SiteId    
  AND A.[Year] = @Year           
  --AND A.QtrId = @VarQtr    
  AND B.CompanySiteCategoryId = 2          
  AND A.TotalRating > 0)          
            
  SELECT @CASEmbeddedRequired AS [CAS Embedded �Required],     
  @CSAEmbeddedRequiredToDate AS [CSA Embedded � Required to date],     
  @CSANonEmbedded1TotalRequired AS [CSA Non-Embedded 1 � Total Required],    
  @CSANonEmbedded1RequiredToDate AS [CSA � Non-Embedded 1 � Required to date],    
  @EmbeddedActualCompleted AS [Embedded � Actual completed],     
  @NonEmbedded1ActualCompleted AS [Non-Embedded 1 � Actual completed]    
          
 END              
ELSE    
 BEGIN          
  -----          
  (SELECT @CASEmbeddedRequired = COUNT(DISTINCT(B.CompanyId))          
  FROM CompanySiteCategoryStandard B          
  WHERE ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = B.SiteId) > 0)  
  AND B.CompanySiteCategoryId = 1)           
  ------   
              
  (Select @CSAEmbeddedRequiredToDate = dbo.GetCSAEmbededReqToDateDB(@SiteId, @Month, @Year))          
            
  ----          
            
  (SELECT @CSANonEmbedded1TotalRequired = COUNT(DISTINCT(B.CompanyId))          
  FROM CompanySiteCategoryStandard B          
  WHERE ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = B.SiteId) > 0)  
  AND B.CompanySiteCategoryId = 2)            
            
  ----          
            
  (Select @CSANonEmbedded1RequiredToDate = dbo.GetCSANonEmbeded1ReqToDateDB(@SiteId, @Month, @Year))          
            
  ----          
            
  (SELECT @EmbeddedActualCompleted = COUNT(DISTINCT(A.CompanyId))          
  FROM CSA A LEFT OUTER JOIN CompanySiteCategoryStandard B          
  ON A.CompanyId = B.CompanyId AND A.SiteId = B.SiteId      
  WHERE ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = B.SiteId) > 0)  
  AND A.[Year] = @Year           
  --AND A.QtrId = @VarQtr           
  AND B.CompanySiteCategoryId = 1          
  AND A.TotalRating > 0)          
            
  ----          
            
  (SELECT @NonEmbedded1ActualCompleted = COUNT(DISTINCT(A.CompanyId))          
  FROM CSA A LEFT OUTER JOIN CompanySiteCategoryStandard B          
  ON A.CompanyId = B.CompanyId AND A.SiteId = B.SiteId      
  WHERE ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = B.SiteId) > 0)  
  AND A.[Year] = @Year           
  --AND A.QtrId = @VarQtr    
  AND B.CompanySiteCategoryId = 2          
  AND A.TotalRating > 0)          
            
  SELECT @CASEmbeddedRequired AS [CAS Embedded �Required],     
  @CSAEmbeddedRequiredToDate AS [CSA Embedded � Required to date],     
  @CSANonEmbedded1TotalRequired AS [CSA Non-Embedded 1 � Total Required],    
  @CSANonEmbedded1RequiredToDate AS [CSA � Non-Embedded 1 � Required to date],    
  @EmbeddedActualCompleted AS [Embedded � Actual completed],     
  @NonEmbedded1ActualCompleted AS [Non-Embedded 1 � Actual completed]    
  
 END
----------------------------------------

GO


