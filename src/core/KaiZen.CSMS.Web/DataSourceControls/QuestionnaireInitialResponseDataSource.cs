﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialResponseProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialResponseDataSourceDesigner))]
	public class QuestionnaireInitialResponseDataSource : ProviderDataSource<QuestionnaireInitialResponse, QuestionnaireInitialResponseKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseDataSource class.
		/// </summary>
		public QuestionnaireInitialResponseDataSource() : base(new QuestionnaireInitialResponseService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialResponseDataSourceView used by the QuestionnaireInitialResponseDataSource.
		/// </summary>
		protected QuestionnaireInitialResponseDataSourceView QuestionnaireInitialResponseView
		{
			get { return ( View as QuestionnaireInitialResponseDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialResponseDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialResponseSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialResponseSelectMethod selectMethod = QuestionnaireInitialResponseSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialResponseSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialResponseDataSourceView class that is to be
		/// used by the QuestionnaireInitialResponseDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialResponseDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialResponse, QuestionnaireInitialResponseKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialResponseDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialResponseDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialResponseDataSourceView : ProviderDataSourceView<QuestionnaireInitialResponse, QuestionnaireInitialResponseKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialResponseDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialResponseDataSourceView(QuestionnaireInitialResponseDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialResponseDataSource QuestionnaireInitialResponseOwner
		{
			get { return Owner as QuestionnaireInitialResponseDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialResponseSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialResponseOwner.SelectMethod; }
			set { QuestionnaireInitialResponseOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialResponseService QuestionnaireInitialResponseProvider
		{
			get { return Provider as QuestionnaireInitialResponseService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialResponse> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialResponse> results = null;
			QuestionnaireInitialResponse item;
			count = 0;
			
			System.Int32 _responseId;
			System.Int32 _questionnaireId;
			System.String _questionId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialResponseSelectMethod.Get:
					QuestionnaireInitialResponseKey entityKey  = new QuestionnaireInitialResponseKey();
					entityKey.Load(values);
					item = QuestionnaireInitialResponseProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialResponse>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialResponseSelectMethod.GetAll:
                    results = QuestionnaireInitialResponseProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialResponseSelectMethod.GetPaged:
					results = QuestionnaireInitialResponseProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialResponseSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialResponseProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialResponseProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialResponseSelectMethod.GetByResponseId:
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialResponseProvider.GetByResponseId(_responseId);
					results = new TList<QuestionnaireInitialResponse>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialResponseSelectMethod.GetByQuestionnaireIdQuestionId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_questionId = ( values["QuestionId"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionId"], typeof(System.String)) : string.Empty;
					item = QuestionnaireInitialResponseProvider.GetByQuestionnaireIdQuestionId(_questionnaireId, _questionId);
					results = new TList<QuestionnaireInitialResponse>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialResponseSelectMethod.GetByQuestionId:
					_questionId = ( values["QuestionId"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionId"], typeof(System.String)) : string.Empty;
					results = QuestionnaireInitialResponseProvider.GetByQuestionId(_questionId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireInitialResponseSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialResponseProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialResponseSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialResponseProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialResponseSelectMethod.Get || SelectMethod == QuestionnaireInitialResponseSelectMethod.GetByResponseId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialResponse entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialResponseProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialResponse> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialResponseProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialResponseDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialResponseDataSource class.
	/// </summary>
	public class QuestionnaireInitialResponseDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialResponse, QuestionnaireInitialResponseKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialResponseDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialResponseSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialResponseDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialResponseDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialResponseDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialResponseDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialResponseDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialResponseDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialResponseDataSourceActionList(QuestionnaireInitialResponseDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialResponseSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialResponseDataSourceActionList
	
	#endregion QuestionnaireInitialResponseDataSourceDesigner
	
	#region QuestionnaireInitialResponseSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialResponseDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialResponseSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdQuestionId method.
		/// </summary>
		GetByQuestionnaireIdQuestionId,
		/// <summary>
		/// Represents the GetByQuestionId method.
		/// </summary>
		GetByQuestionId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireInitialResponseSelectMethod

	#region QuestionnaireInitialResponseFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseFilter : SqlFilter<QuestionnaireInitialResponseColumn>
	{
	}
	
	#endregion QuestionnaireInitialResponseFilter

	#region QuestionnaireInitialResponseExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialResponseColumn>
	{
	}
	
	#endregion QuestionnaireInitialResponseExpressionBuilder	

	#region QuestionnaireInitialResponseProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialResponseChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseProperty : ChildEntityProperty<QuestionnaireInitialResponseChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialResponseProperty
}

