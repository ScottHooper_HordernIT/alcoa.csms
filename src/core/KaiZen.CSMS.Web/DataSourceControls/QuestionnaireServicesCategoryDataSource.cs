﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireServicesCategoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireServicesCategoryDataSourceDesigner))]
	public class QuestionnaireServicesCategoryDataSource : ProviderDataSource<QuestionnaireServicesCategory, QuestionnaireServicesCategoryKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryDataSource class.
		/// </summary>
		public QuestionnaireServicesCategoryDataSource() : base(new QuestionnaireServicesCategoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireServicesCategoryDataSourceView used by the QuestionnaireServicesCategoryDataSource.
		/// </summary>
		protected QuestionnaireServicesCategoryDataSourceView QuestionnaireServicesCategoryView
		{
			get { return ( View as QuestionnaireServicesCategoryDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireServicesCategoryDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireServicesCategorySelectMethod SelectMethod
		{
			get
			{
				QuestionnaireServicesCategorySelectMethod selectMethod = QuestionnaireServicesCategorySelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireServicesCategorySelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireServicesCategoryDataSourceView class that is to be
		/// used by the QuestionnaireServicesCategoryDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireServicesCategoryDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireServicesCategory, QuestionnaireServicesCategoryKey> GetNewDataSourceView()
		{
			return new QuestionnaireServicesCategoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireServicesCategoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireServicesCategoryDataSourceView : ProviderDataSourceView<QuestionnaireServicesCategory, QuestionnaireServicesCategoryKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireServicesCategoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireServicesCategoryDataSourceView(QuestionnaireServicesCategoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireServicesCategoryDataSource QuestionnaireServicesCategoryOwner
		{
			get { return Owner as QuestionnaireServicesCategoryDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireServicesCategorySelectMethod SelectMethod
		{
			get { return QuestionnaireServicesCategoryOwner.SelectMethod; }
			set { QuestionnaireServicesCategoryOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireServicesCategoryService QuestionnaireServicesCategoryProvider
		{
			get { return Provider as QuestionnaireServicesCategoryService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireServicesCategory> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireServicesCategory> results = null;
			QuestionnaireServicesCategory item;
			count = 0;
			
			System.Int32 _categoryId;
			System.Boolean? _highRisk_nullable;
			System.Boolean _visible;
			System.String _categoryText;

			switch ( SelectMethod )
			{
				case QuestionnaireServicesCategorySelectMethod.Get:
					QuestionnaireServicesCategoryKey entityKey  = new QuestionnaireServicesCategoryKey();
					entityKey.Load(values);
					item = QuestionnaireServicesCategoryProvider.Get(entityKey);
					results = new TList<QuestionnaireServicesCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireServicesCategorySelectMethod.GetAll:
                    results = QuestionnaireServicesCategoryProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireServicesCategorySelectMethod.GetPaged:
					results = QuestionnaireServicesCategoryProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireServicesCategorySelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireServicesCategoryProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireServicesCategoryProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireServicesCategorySelectMethod.GetByCategoryId:
					_categoryId = ( values["CategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CategoryId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireServicesCategoryProvider.GetByCategoryId(_categoryId);
					results = new TList<QuestionnaireServicesCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireServicesCategorySelectMethod.GetByHighRisk:
					_highRisk_nullable = (System.Boolean?) EntityUtil.ChangeType(values["HighRisk"], typeof(System.Boolean?));
					results = QuestionnaireServicesCategoryProvider.GetByHighRisk(_highRisk_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireServicesCategorySelectMethod.GetByVisible:
					_visible = ( values["Visible"] != null ) ? (System.Boolean) EntityUtil.ChangeType(values["Visible"], typeof(System.Boolean)) : false;
					results = QuestionnaireServicesCategoryProvider.GetByVisible(_visible, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireServicesCategorySelectMethod.GetByCategoryText:
					_categoryText = ( values["CategoryText"] != null ) ? (System.String) EntityUtil.ChangeType(values["CategoryText"], typeof(System.String)) : string.Empty;
					item = QuestionnaireServicesCategoryProvider.GetByCategoryText(_categoryText);
					results = new TList<QuestionnaireServicesCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireServicesCategorySelectMethod.Get || SelectMethod == QuestionnaireServicesCategorySelectMethod.GetByCategoryId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireServicesCategory entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireServicesCategoryProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireServicesCategory> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireServicesCategoryProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireServicesCategoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireServicesCategoryDataSource class.
	/// </summary>
	public class QuestionnaireServicesCategoryDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireServicesCategory, QuestionnaireServicesCategoryKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryDataSourceDesigner class.
		/// </summary>
		public QuestionnaireServicesCategoryDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireServicesCategorySelectMethod SelectMethod
		{
			get { return ((QuestionnaireServicesCategoryDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireServicesCategoryDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireServicesCategoryDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireServicesCategoryDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireServicesCategoryDataSourceActionList : DesignerActionList
	{
		private QuestionnaireServicesCategoryDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireServicesCategoryDataSourceActionList(QuestionnaireServicesCategoryDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireServicesCategorySelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireServicesCategoryDataSourceActionList
	
	#endregion QuestionnaireServicesCategoryDataSourceDesigner
	
	#region QuestionnaireServicesCategorySelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireServicesCategoryDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireServicesCategorySelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCategoryId method.
		/// </summary>
		GetByCategoryId,
		/// <summary>
		/// Represents the GetByHighRisk method.
		/// </summary>
		GetByHighRisk,
		/// <summary>
		/// Represents the GetByVisible method.
		/// </summary>
		GetByVisible,
		/// <summary>
		/// Represents the GetByCategoryText method.
		/// </summary>
		GetByCategoryText
	}
	
	#endregion QuestionnaireServicesCategorySelectMethod

	#region QuestionnaireServicesCategoryFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesCategoryFilter : SqlFilter<QuestionnaireServicesCategoryColumn>
	{
	}
	
	#endregion QuestionnaireServicesCategoryFilter

	#region QuestionnaireServicesCategoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesCategoryExpressionBuilder : SqlExpressionBuilder<QuestionnaireServicesCategoryColumn>
	{
	}
	
	#endregion QuestionnaireServicesCategoryExpressionBuilder	

	#region QuestionnaireServicesCategoryProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireServicesCategoryChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesCategoryProperty : ChildEntityProperty<QuestionnaireServicesCategoryChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireServicesCategoryProperty
}

