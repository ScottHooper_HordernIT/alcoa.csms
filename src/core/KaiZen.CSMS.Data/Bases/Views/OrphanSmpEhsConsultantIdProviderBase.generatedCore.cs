﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrphanSmpEhsConsultantIdProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class OrphanSmpEhsConsultantIdProviderBaseCore : EntityViewProviderBase<OrphanSmpEhsConsultantId>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;OrphanSmpEhsConsultantId&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;OrphanSmpEhsConsultantId&gt;"/></returns>
		protected static VList&lt;OrphanSmpEhsConsultantId&gt; Fill(DataSet dataSet, VList<OrphanSmpEhsConsultantId> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<OrphanSmpEhsConsultantId>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;OrphanSmpEhsConsultantId&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<OrphanSmpEhsConsultantId>"/></returns>
		protected static VList&lt;OrphanSmpEhsConsultantId&gt; Fill(DataTable dataTable, VList<OrphanSmpEhsConsultantId> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					OrphanSmpEhsConsultantId c = new OrphanSmpEhsConsultantId();
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;OrphanSmpEhsConsultantId&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;OrphanSmpEhsConsultantId&gt;"/></returns>
		protected VList<OrphanSmpEhsConsultantId> Fill(IDataReader reader, VList<OrphanSmpEhsConsultantId> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					OrphanSmpEhsConsultantId entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<OrphanSmpEhsConsultantId>("OrphanSmpEhsConsultantId",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new OrphanSmpEhsConsultantId();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsConsultantId = (reader.IsDBNull(((int)OrphanSmpEhsConsultantIdColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)OrphanSmpEhsConsultantIdColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="OrphanSmpEhsConsultantId"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanSmpEhsConsultantId"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, OrphanSmpEhsConsultantId entity)
		{
			reader.Read();
			entity.EhsConsultantId = (reader.IsDBNull(((int)OrphanSmpEhsConsultantIdColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)OrphanSmpEhsConsultantIdColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="OrphanSmpEhsConsultantId"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanSmpEhsConsultantId"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, OrphanSmpEhsConsultantId entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region OrphanSmpEhsConsultantIdFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanSmpEhsConsultantIdFilterBuilder : SqlFilterBuilder<OrphanSmpEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdFilterBuilder class.
		/// </summary>
		public OrphanSmpEhsConsultantIdFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanSmpEhsConsultantIdFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanSmpEhsConsultantIdFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanSmpEhsConsultantIdFilterBuilder

	#region OrphanSmpEhsConsultantIdParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanSmpEhsConsultantIdParameterBuilder : ParameterizedSqlFilterBuilder<OrphanSmpEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdParameterBuilder class.
		/// </summary>
		public OrphanSmpEhsConsultantIdParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanSmpEhsConsultantIdParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanSmpEhsConsultantIdParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanSmpEhsConsultantIdParameterBuilder
	
	#region OrphanSmpEhsConsultantIdSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrphanSmpEhsConsultantIdSortBuilder : SqlSortBuilder<OrphanSmpEhsConsultantIdColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdSqlSortBuilder class.
		/// </summary>
		public OrphanSmpEhsConsultantIdSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrphanSmpEhsConsultantIdSortBuilder

} // end namespace
