﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultTableProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileVaultTableDataSourceDesigner))]
	public class FileVaultTableDataSource : ProviderDataSource<FileVaultTable, FileVaultTableKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableDataSource class.
		/// </summary>
		public FileVaultTableDataSource() : base(new FileVaultTableService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultTableDataSourceView used by the FileVaultTableDataSource.
		/// </summary>
		protected FileVaultTableDataSourceView FileVaultTableView
		{
			get { return ( View as FileVaultTableDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileVaultTableDataSource control invokes to retrieve data.
		/// </summary>
		public FileVaultTableSelectMethod SelectMethod
		{
			get
			{
				FileVaultTableSelectMethod selectMethod = FileVaultTableSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileVaultTableSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultTableDataSourceView class that is to be
		/// used by the FileVaultTableDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultTableDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultTable, FileVaultTableKey> GetNewDataSourceView()
		{
			return new FileVaultTableDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultTableDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultTableDataSourceView : ProviderDataSourceView<FileVaultTable, FileVaultTableKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultTableDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultTableDataSourceView(FileVaultTableDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultTableDataSource FileVaultTableOwner
		{
			get { return Owner as FileVaultTableDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileVaultTableSelectMethod SelectMethod
		{
			get { return FileVaultTableOwner.SelectMethod; }
			set { FileVaultTableOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultTableService FileVaultTableProvider
		{
			get { return Provider as FileVaultTableService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileVaultTable> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileVaultTable> results = null;
			FileVaultTable item;
			count = 0;
			
			System.Int32 _fileVaultTableId;
			System.Int32 _fileVaultId;
			System.Int32 _fileVaultSubCategoryId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case FileVaultTableSelectMethod.Get:
					FileVaultTableKey entityKey  = new FileVaultTableKey();
					entityKey.Load(values);
					item = FileVaultTableProvider.Get(entityKey);
					results = new TList<FileVaultTable>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileVaultTableSelectMethod.GetAll:
                    results = FileVaultTableProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileVaultTableSelectMethod.GetPaged:
					results = FileVaultTableProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileVaultTableSelectMethod.Find:
					if ( FilterParameters != null )
						results = FileVaultTableProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileVaultTableProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileVaultTableSelectMethod.GetByFileVaultTableId:
					_fileVaultTableId = ( values["FileVaultTableId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultTableId"], typeof(System.Int32)) : (int)0;
					item = FileVaultTableProvider.GetByFileVaultTableId(_fileVaultTableId);
					results = new TList<FileVaultTable>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case FileVaultTableSelectMethod.GetByFileVaultIdFileVaultSubCategoryId:
					_fileVaultId = ( values["FileVaultId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultId"], typeof(System.Int32)) : (int)0;
					_fileVaultSubCategoryId = ( values["FileVaultSubCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultSubCategoryId"], typeof(System.Int32)) : (int)0;
					item = FileVaultTableProvider.GetByFileVaultIdFileVaultSubCategoryId(_fileVaultId, _fileVaultSubCategoryId);
					results = new TList<FileVaultTable>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case FileVaultTableSelectMethod.GetByFileVaultSubCategoryId:
					_fileVaultSubCategoryId = ( values["FileVaultSubCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultSubCategoryId"], typeof(System.Int32)) : (int)0;
					results = FileVaultTableProvider.GetByFileVaultSubCategoryId(_fileVaultSubCategoryId, this.StartIndex, this.PageSize, out count);
					break;
				case FileVaultTableSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = FileVaultTableProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case FileVaultTableSelectMethod.GetByFileVaultId:
					_fileVaultId = ( values["FileVaultId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultId"], typeof(System.Int32)) : (int)0;
					results = FileVaultTableProvider.GetByFileVaultId(_fileVaultId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileVaultTableSelectMethod.Get || SelectMethod == FileVaultTableSelectMethod.GetByFileVaultTableId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileVaultTable entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileVaultTableProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileVaultTable> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileVaultTableProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileVaultTableDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultTableDataSource class.
	/// </summary>
	public class FileVaultTableDataSourceDesigner : ProviderDataSourceDesigner<FileVaultTable, FileVaultTableKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileVaultTableDataSourceDesigner class.
		/// </summary>
		public FileVaultTableDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultTableSelectMethod SelectMethod
		{
			get { return ((FileVaultTableDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileVaultTableDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileVaultTableDataSourceActionList

	/// <summary>
	/// Supports the FileVaultTableDataSourceDesigner class.
	/// </summary>
	internal class FileVaultTableDataSourceActionList : DesignerActionList
	{
		private FileVaultTableDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileVaultTableDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileVaultTableDataSourceActionList(FileVaultTableDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultTableSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileVaultTableDataSourceActionList
	
	#endregion FileVaultTableDataSourceDesigner
	
	#region FileVaultTableSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileVaultTableDataSource.SelectMethod property.
	/// </summary>
	public enum FileVaultTableSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileVaultTableId method.
		/// </summary>
		GetByFileVaultTableId,
		/// <summary>
		/// Represents the GetByFileVaultIdFileVaultSubCategoryId method.
		/// </summary>
		GetByFileVaultIdFileVaultSubCategoryId,
		/// <summary>
		/// Represents the GetByFileVaultSubCategoryId method.
		/// </summary>
		GetByFileVaultSubCategoryId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByFileVaultId method.
		/// </summary>
		GetByFileVaultId
	}
	
	#endregion FileVaultTableSelectMethod

	#region FileVaultTableFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableFilter : SqlFilter<FileVaultTableColumn>
	{
	}
	
	#endregion FileVaultTableFilter

	#region FileVaultTableExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableExpressionBuilder : SqlExpressionBuilder<FileVaultTableColumn>
	{
	}
	
	#endregion FileVaultTableExpressionBuilder	

	#region FileVaultTableProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileVaultTableChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableProperty : ChildEntityProperty<FileVaultTableChildEntityTypes>
	{
	}
	
	#endregion FileVaultTableProperty
}

