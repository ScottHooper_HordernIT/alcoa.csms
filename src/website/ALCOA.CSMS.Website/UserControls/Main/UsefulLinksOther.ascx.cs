﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;

using System.Text;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class UsefulLinksOther : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                TemplateService tService = new TemplateService();
                Template t = tService.GetByTemplateId((int)TemplateTypeList.UsefulLinksOther);
                if (t != null)
                    aheTemplate.Html = Encoding.ASCII.GetString(t.TemplateContent);
                else
                    aheTemplate.Html = "";

                aheTemplate.ActiveView = DevExpress.Web.ASPxHtmlEditor.HtmlEditorView.Preview;
                aheTemplate.Settings.AllowDesignView = false;
                btnEdit.Visible = false;

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        break;
                    case ((int)RoleList.Contractor):
                        break;
                    case ((int)RoleList.Administrator):
                        btnEdit.Visible = true;
                        break;
                    default:
                        // do something
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (btnSave.Visible == true)
            {
                lblSave.Visible = false;
                btnSave.Visible = false;
                btnEdit.Text = "(Edit)";
                aheTemplate.Settings.AllowDesignView = false;
                aheTemplate.ActiveView = DevExpress.Web.ASPxHtmlEditor.HtmlEditorView.Preview;
            }
            else
            {
                lblSave.Visible = true;
                btnSave.Visible = true;
                btnEdit.Text = "(View)";
                aheTemplate.Settings.AllowDesignView = true;
                aheTemplate.ActiveView = DevExpress.Web.ASPxHtmlEditor.HtmlEditorView.Design;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();

                TemplateService tService = new TemplateService();
                Template t = tService.GetByTemplateId((int)TemplateTypeList.UsefulLinksOther);
                if (t == null) t = new Template();
                t.TemplateContent = (byte[])encoding.GetBytes(aheTemplate.Html);
                tService.Save(t);

                lblSave.Text = "Saved Successfully";
            }
            catch (Exception ex)
            {
                lblSave.Text = "Error Saving: " + ex.Message;
                Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
            }
        }
    }
}