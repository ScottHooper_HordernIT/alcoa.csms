//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdHoc_Radar_Items2
    {
        public int AdHoc_Radar_Item2Id { get; set; }
        public int AdHoc_RadarId { get; set; }
        public int Year { get; set; }
        public int SiteId { get; set; }
        public int CompanyId { get; set; }
        public Nullable<int> KpiComplianceScore { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual AdHoc_Radar AdHoc_Radar { get; set; }
        public virtual Company Company { get; set; }
        public virtual Site Site { get; set; }
    }
}
