﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailTemplateRecipientProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskEmailTemplateRecipientDataSourceDesigner))]
	public class AdminTaskEmailTemplateRecipientDataSource : ProviderDataSource<AdminTaskEmailTemplateRecipient, AdminTaskEmailTemplateRecipientKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientDataSource class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientDataSource() : base(new AdminTaskEmailTemplateRecipientService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailTemplateRecipientDataSourceView used by the AdminTaskEmailTemplateRecipientDataSource.
		/// </summary>
		protected AdminTaskEmailTemplateRecipientDataSourceView AdminTaskEmailTemplateRecipientView
		{
			get { return ( View as AdminTaskEmailTemplateRecipientDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskEmailTemplateRecipientDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskEmailTemplateRecipientSelectMethod SelectMethod
		{
			get
			{
				AdminTaskEmailTemplateRecipientSelectMethod selectMethod = AdminTaskEmailTemplateRecipientSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskEmailTemplateRecipientSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailTemplateRecipientDataSourceView class that is to be
		/// used by the AdminTaskEmailTemplateRecipientDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailTemplateRecipientDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailTemplateRecipient, AdminTaskEmailTemplateRecipientKey> GetNewDataSourceView()
		{
			return new AdminTaskEmailTemplateRecipientDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailTemplateRecipientDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailTemplateRecipientDataSourceView : ProviderDataSourceView<AdminTaskEmailTemplateRecipient, AdminTaskEmailTemplateRecipientKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailTemplateRecipientDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailTemplateRecipientDataSourceView(AdminTaskEmailTemplateRecipientDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailTemplateRecipientDataSource AdminTaskEmailTemplateRecipientOwner
		{
			get { return Owner as AdminTaskEmailTemplateRecipientDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskEmailTemplateRecipientSelectMethod SelectMethod
		{
			get { return AdminTaskEmailTemplateRecipientOwner.SelectMethod; }
			set { AdminTaskEmailTemplateRecipientOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailTemplateRecipientService AdminTaskEmailTemplateRecipientProvider
		{
			get { return Provider as AdminTaskEmailTemplateRecipientService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskEmailTemplateRecipient> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskEmailTemplateRecipient> results = null;
			AdminTaskEmailTemplateRecipient item;
			count = 0;
			
			System.Int32 _adminTaskEmailTemplateRecipientId;
			System.Int32 _adminTaskEmailTemplateId;
			System.Int32 _adminTaskEmailRecipientId;
			System.Int32 _csmsEmailRecipientTypeId;

			switch ( SelectMethod )
			{
				case AdminTaskEmailTemplateRecipientSelectMethod.Get:
					AdminTaskEmailTemplateRecipientKey entityKey  = new AdminTaskEmailTemplateRecipientKey();
					entityKey.Load(values);
					item = AdminTaskEmailTemplateRecipientProvider.Get(entityKey);
					results = new TList<AdminTaskEmailTemplateRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskEmailTemplateRecipientSelectMethod.GetAll:
                    results = AdminTaskEmailTemplateRecipientProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskEmailTemplateRecipientSelectMethod.GetPaged:
					results = AdminTaskEmailTemplateRecipientProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskEmailTemplateRecipientSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskEmailTemplateRecipientProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskEmailTemplateRecipientProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskEmailTemplateRecipientSelectMethod.GetByAdminTaskEmailTemplateRecipientId:
					_adminTaskEmailTemplateRecipientId = ( values["AdminTaskEmailTemplateRecipientId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailTemplateRecipientId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailTemplateRecipientId(_adminTaskEmailTemplateRecipientId);
					results = new TList<AdminTaskEmailTemplateRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskEmailTemplateRecipientSelectMethod.GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId:
					_adminTaskEmailTemplateId = ( values["AdminTaskEmailTemplateId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailTemplateId"], typeof(System.Int32)) : (int)0;
					_adminTaskEmailRecipientId = ( values["AdminTaskEmailRecipientId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailRecipientId"], typeof(System.Int32)) : (int)0;
					_csmsEmailRecipientTypeId = ( values["CsmsEmailRecipientTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailRecipientTypeId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(_adminTaskEmailTemplateId, _adminTaskEmailRecipientId, _csmsEmailRecipientTypeId);
					results = new TList<AdminTaskEmailTemplateRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskEmailTemplateRecipientSelectMethod.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId:
					_adminTaskEmailTemplateId = ( values["AdminTaskEmailTemplateId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailTemplateId"], typeof(System.Int32)) : (int)0;
					_csmsEmailRecipientTypeId = ( values["CsmsEmailRecipientTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailRecipientTypeId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(_adminTaskEmailTemplateId, _csmsEmailRecipientTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case AdminTaskEmailTemplateRecipientSelectMethod.GetByAdminTaskEmailTemplateId:
					_adminTaskEmailTemplateId = ( values["AdminTaskEmailTemplateId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailTemplateId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailTemplateId(_adminTaskEmailTemplateId, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskEmailTemplateRecipientSelectMethod.GetByAdminTaskEmailRecipientId:
					_adminTaskEmailRecipientId = ( values["AdminTaskEmailRecipientId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailRecipientId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailRecipientId(_adminTaskEmailRecipientId, this.StartIndex, this.PageSize, out count);
					break;
				case AdminTaskEmailTemplateRecipientSelectMethod.GetByCsmsEmailRecipientTypeId:
					_csmsEmailRecipientTypeId = ( values["CsmsEmailRecipientTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailRecipientTypeId"], typeof(System.Int32)) : (int)0;
					results = AdminTaskEmailTemplateRecipientProvider.GetByCsmsEmailRecipientTypeId(_csmsEmailRecipientTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskEmailTemplateRecipientSelectMethod.Get || SelectMethod == AdminTaskEmailTemplateRecipientSelectMethod.GetByAdminTaskEmailTemplateRecipientId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskEmailTemplateRecipient entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskEmailTemplateRecipientProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskEmailTemplateRecipient> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskEmailTemplateRecipientProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskEmailTemplateRecipientDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailTemplateRecipientDataSource class.
	/// </summary>
	public class AdminTaskEmailTemplateRecipientDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskEmailTemplateRecipient, AdminTaskEmailTemplateRecipientKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientDataSourceDesigner class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailTemplateRecipientSelectMethod SelectMethod
		{
			get { return ((AdminTaskEmailTemplateRecipientDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskEmailTemplateRecipientDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskEmailTemplateRecipientDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskEmailTemplateRecipientDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskEmailTemplateRecipientDataSourceActionList : DesignerActionList
	{
		private AdminTaskEmailTemplateRecipientDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskEmailTemplateRecipientDataSourceActionList(AdminTaskEmailTemplateRecipientDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailTemplateRecipientSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskEmailTemplateRecipientDataSourceActionList
	
	#endregion AdminTaskEmailTemplateRecipientDataSourceDesigner
	
	#region AdminTaskEmailTemplateRecipientSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskEmailTemplateRecipientDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskEmailTemplateRecipientSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskEmailTemplateRecipientId method.
		/// </summary>
		GetByAdminTaskEmailTemplateRecipientId,
		/// <summary>
		/// Represents the GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId method.
		/// </summary>
		GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId,
		/// <summary>
		/// Represents the GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId method.
		/// </summary>
		GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId,
		/// <summary>
		/// Represents the GetByAdminTaskEmailTemplateId method.
		/// </summary>
		GetByAdminTaskEmailTemplateId,
		/// <summary>
		/// Represents the GetByAdminTaskEmailRecipientId method.
		/// </summary>
		GetByAdminTaskEmailRecipientId,
		/// <summary>
		/// Represents the GetByCsmsEmailRecipientTypeId method.
		/// </summary>
		GetByCsmsEmailRecipientTypeId
	}
	
	#endregion AdminTaskEmailTemplateRecipientSelectMethod

	#region AdminTaskEmailTemplateRecipientFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientFilter : SqlFilter<AdminTaskEmailTemplateRecipientColumn>
	{
	}
	
	#endregion AdminTaskEmailTemplateRecipientFilter

	#region AdminTaskEmailTemplateRecipientExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailTemplateRecipientColumn>
	{
	}
	
	#endregion AdminTaskEmailTemplateRecipientExpressionBuilder	

	#region AdminTaskEmailTemplateRecipientProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskEmailTemplateRecipientChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientProperty : ChildEntityProperty<AdminTaskEmailTemplateRecipientChildEntityTypes>
	{
	}
	
	#endregion AdminTaskEmailTemplateRecipientProperty
}

