﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SqExemptionAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SqExemptionAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.SqExemptionAudit, KaiZen.CSMS.Entities.SqExemptionAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemptionAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.SqExemptionAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemptionAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SqExemptionAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemptionAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemptionAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemptionAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemptionAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemptionAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemptionAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemptionAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemptionAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.SqExemptionAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SqExemptionAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SqExemptionAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SqExemptionAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SqExemptionAudit&gt;"/></returns>
		public static TList<SqExemptionAudit> Fill(IDataReader reader, TList<SqExemptionAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.SqExemptionAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SqExemptionAudit")
					.Append("|").Append((System.Int32)reader[((int)SqExemptionAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SqExemptionAudit>(
					key.ToString(), // EntityTrackingKey
					"SqExemptionAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.SqExemptionAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)SqExemptionAuditColumn.AuditId - 1)];
					c.SqExemptionId = (reader.IsDBNull(((int)SqExemptionAuditColumn.SqExemptionId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.SqExemptionId - 1)];
					c.CompanyId = (reader.IsDBNull(((int)SqExemptionAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.CompanyId - 1)];
					c.SiteId = (reader.IsDBNull(((int)SqExemptionAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.SiteId - 1)];
					c.DateApplied = (reader.IsDBNull(((int)SqExemptionAuditColumn.DateApplied - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.DateApplied - 1)];
					c.ValidFrom = (reader.IsDBNull(((int)SqExemptionAuditColumn.ValidFrom - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.ValidFrom - 1)];
					c.ValidTo = (reader.IsDBNull(((int)SqExemptionAuditColumn.ValidTo - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.ValidTo - 1)];
					c.CompanyStatus2Id = (reader.IsDBNull(((int)SqExemptionAuditColumn.CompanyStatus2Id - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.CompanyStatus2Id - 1)];
					c.RequestedByCompanyId = (reader.IsDBNull(((int)SqExemptionAuditColumn.RequestedByCompanyId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.RequestedByCompanyId - 1)];
					c.FileVaultId = (reader.IsDBNull(((int)SqExemptionAuditColumn.FileVaultId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.FileVaultId - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)SqExemptionAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)SqExemptionAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.ModifiedDate - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)SqExemptionAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)SqExemptionAuditColumn.AuditEventId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.SqExemptionAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)SqExemptionAuditColumn.AuditId - 1)];
			entity.SqExemptionId = (reader.IsDBNull(((int)SqExemptionAuditColumn.SqExemptionId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.SqExemptionId - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)SqExemptionAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.CompanyId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)SqExemptionAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.SiteId - 1)];
			entity.DateApplied = (reader.IsDBNull(((int)SqExemptionAuditColumn.DateApplied - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.DateApplied - 1)];
			entity.ValidFrom = (reader.IsDBNull(((int)SqExemptionAuditColumn.ValidFrom - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.ValidFrom - 1)];
			entity.ValidTo = (reader.IsDBNull(((int)SqExemptionAuditColumn.ValidTo - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.ValidTo - 1)];
			entity.CompanyStatus2Id = (reader.IsDBNull(((int)SqExemptionAuditColumn.CompanyStatus2Id - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.CompanyStatus2Id - 1)];
			entity.RequestedByCompanyId = (reader.IsDBNull(((int)SqExemptionAuditColumn.RequestedByCompanyId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.RequestedByCompanyId - 1)];
			entity.FileVaultId = (reader.IsDBNull(((int)SqExemptionAuditColumn.FileVaultId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.FileVaultId - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)SqExemptionAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)SqExemptionAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)SqExemptionAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)SqExemptionAuditColumn.ModifiedDate - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)SqExemptionAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)SqExemptionAuditColumn.AuditEventId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.SqExemptionAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.SqExemptionId = Convert.IsDBNull(dataRow["SqExemptionId"]) ? null : (System.Int32?)dataRow["SqExemptionId"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.DateApplied = Convert.IsDBNull(dataRow["DateApplied"]) ? null : (System.DateTime?)dataRow["DateApplied"];
			entity.ValidFrom = Convert.IsDBNull(dataRow["ValidFrom"]) ? null : (System.DateTime?)dataRow["ValidFrom"];
			entity.ValidTo = Convert.IsDBNull(dataRow["ValidTo"]) ? null : (System.DateTime?)dataRow["ValidTo"];
			entity.CompanyStatus2Id = Convert.IsDBNull(dataRow["CompanyStatus2Id"]) ? null : (System.Int32?)dataRow["CompanyStatus2Id"];
			entity.RequestedByCompanyId = Convert.IsDBNull(dataRow["RequestedByCompanyId"]) ? null : (System.Int32?)dataRow["RequestedByCompanyId"];
			entity.FileVaultId = Convert.IsDBNull(dataRow["FileVaultId"]) ? null : (System.Int32?)dataRow["FileVaultId"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SqExemptionAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SqExemptionAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemptionAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SqExemptionAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.SqExemptionAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SqExemptionAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.SqExemptionAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SqExemptionAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.SqExemptionAudit</c>
	///</summary>
	public enum SqExemptionAuditChildEntityTypes
	{
	}
	
	#endregion SqExemptionAuditChildEntityTypes
	
	#region SqExemptionAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SqExemptionAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditFilterBuilder : SqlFilterBuilder<SqExemptionAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditFilterBuilder class.
		/// </summary>
		public SqExemptionAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionAuditFilterBuilder
	
	#region SqExemptionAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SqExemptionAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditParameterBuilder : ParameterizedSqlFilterBuilder<SqExemptionAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditParameterBuilder class.
		/// </summary>
		public SqExemptionAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionAuditParameterBuilder
	
	#region SqExemptionAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SqExemptionAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SqExemptionAuditSortBuilder : SqlSortBuilder<SqExemptionAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditSqlSortBuilder class.
		/// </summary>
		public SqExemptionAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SqExemptionAuditSortBuilder
	
} // end namespace
