<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Reports_iProc" Codebehind="Reports_iProc.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" style="text-align: left;">
                        <span class="title">Reports</span><br />
                            <span class="date">iProc - Contractors that have lodged an iProc claim without submitting KPI data for that month</span><br />
                            <img height="1" src="images/grfc_dottedline.gif" width="24" />
            </td>
    </tr>
    <tr>
        <td>
<em><span style="color: navy">
    <strong>NB. This report is only updated on the 15th of the month for the previous month.</strong></span><br />
    <br />
</span></em>
<dxwgv:aspxgridview id="grid" runat="server" autogeneratecolumns="False" 
    ClientInstanceName="grid" Width="100%"
    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
    >
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Width="11px"></CollapsedButton>

<ExpandedButton Width="11px"></ExpandedButton>

<DetailCollapsedButton Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Width="11px"></DetailExpandedButton>

<FilterRowButton Width="13px"></FilterRowButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--Change by Debashis--%> 
    </LoadingPanel>
</Images>


<Columns>
<dxwgv:GridViewDataTextColumn FieldName="CompanyName" VisibleIndex="0" SortIndex="0"></dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="LocCode" Caption="Site Name" VisibleIndex="1" SortIndex="1" Width="200px"></dxwgv:GridViewDataTextColumn>

<dxwgv:GridViewDataComboBoxColumn Name="Month" Caption="Month" FieldName="Month" Width="100px"
                        SortIndex="4" SortOrder="Ascending" UnboundType="String" VisibleIndex="2">
    <PropertiesComboBox DataSourceID="dsMonths" TextField="MonthName"
        ValueField="MonthId" ValueType="System.Int32">
    </PropertiesComboBox>
</dxwgv:GridViewDataComboBoxColumn>

<dxwgv:GridViewDataTextColumn FieldName="Year" ReadOnly="True" UnboundType="Integer" VisibleIndex="3" SortIndex="3" Width="50px"></dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="No of Claims" Caption="# Claims" UnboundType="Integer" VisibleIndex="4" Width="100px">
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="KPI Lodged" Caption="KPI Lodged" UnboundType="Integer" VisibleIndex="4" Visible="False">
</dxwgv:GridViewDataTextColumn>
</Columns>
<Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowFooter="False" ShowPreview="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="225"></Settings>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <ClientSideEvents ContextMenu="function(s, e) {
                if (expanded == true)
                {
                    grid.CollapseAll();
                    expanded = false;
                }
                else
                {
	                grid.ExpandAll();
	                expanded = true;
	            }
}" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:aspxgridview>
<tr align="right">
            <td colspan="3" style="padding-top:6px; text-align: right; text-align:-moz-right; width:100%" align="right">
                <div align="right">
                    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                </div>    
            </td>
        </tr>
</table>
<asp:SqlDataSource ID="dsMonths" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT [MonthId], [MonthName] FROM [Months] ORDER BY [MonthId]">
</asp:SqlDataSource>
<asp:SqlDataSource ID="GetABNbyVendor" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="CompanyVendor_GetABNByVendorNumber" SelectCommandType="StoredProcedure">
    <SelectParameters>
    <asp:Parameter Name="Vendor_Number" DbType="String" />
    </SelectParameters>
</asp:SqlDataSource>