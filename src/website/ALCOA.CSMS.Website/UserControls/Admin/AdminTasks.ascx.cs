﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

using System.Drawing;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class AdminTasks : System.Web.UI.UserControl
    {
        //User Customisable Colors
        const string CellColourBad = "Red"; //"#ffb8b8";
        const string CellColourGood = "Lime"; //"#def3ca";
        const string CellColourUnsure = "";

        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");
                grid.ExpandAll();
            }
        }

        protected void btnAddNewTask_Click(object sender, EventArgs e)
        {
            try
            {
                AdminTaskService atService = new AdminTaskService();

                using (KaiZen.CSMS.Entities.AdminTask at = new KaiZen.CSMS.Entities.AdminTask())
                {
                    at.AdminTaskStatusId = (int)AdminTaskStatusList.Open;
                    at.AdminTaskSourceId = (int)AdminTaskSourceList.Manual;
                    at.CsmsAccessId = (int)cbAddNewTaskAccess.SelectedItem.Value;
                    at.AdminTaskTypeId = (int)cbAddNewTaskType.SelectedItem.Value;
                    at.DateOpened = DateTime.Now;
                    at.OpenedByUserId = auth.UserId;
                    at.Login = tbAddNewTaskLogin.Text;
                    at.EmailAddress = tbAddNewTaskEmail.Text;
                    at.FirstName = tbAddNewTaskFirstName.Text;
                    at.LastName = tbAddNewTaskLastName.Text;
                    at.JobTitle = tbAddNewTaskJobTitle.Text;
                    at.CompanyId = (int)cbAddNewTaskCompany.SelectedItem.Value;
                    at.TelephoneNo = tbAddNewTaskTelephoneNumber.Text;
                    at.MobileNo = tbAddNewTaskMobileNumber.Text;
                    at.FaxNo = tbAddNewTaskFaxNumber.Text;
                    at.AdminTaskComments = mAddNewTaskMemoComments.Text;

                    atService.Insert(at);
                }
                Response.Redirect("AdminTasks.aspx", true); 
            }
            catch (Exception ex)
            {
               
            }

            //grid.DataBind();
        }

        protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

            int AdminTaskStatusId = (int)grid.GetRowValues(e.VisibleIndex, "AdminTaskStatusId");
            
            int AdminTaskId = (int)grid.GetRowValues(e.VisibleIndex, "AdminTaskId");

            HyperLink hlPerformTask = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlPerformTask") as HyperLink;
            if (hlPerformTask != null)
            {
                if (AdminTaskStatusId == (int)AdminTaskStatusList.Closed)
                {
                    hlPerformTask.Text = "View Task";
                }
                else
                {
                    hlPerformTask.Text = "Perform Task";
                }

                hlPerformTask.NavigateUrl = String.Format("../../AdminTask.aspx{0}",
                        QueryStringModule.Encrypt("q=" + AdminTaskId));

            }

            HyperLink hlChangeStatus = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlChangeStatus") as HyperLink;
            if (hlChangeStatus != null)
            {
                if (AdminTaskStatusId == (int)AdminTaskStatusList.Open)
                {
                    hlChangeStatus.NavigateUrl = String.Format("../../admin.aspx{0}",
                        QueryStringModule.Encrypt("s=" + "AdminTaskClose&q=" + AdminTaskId));
                }
                else
                {
                    hlChangeStatus.Visible = false;
                }
            }

            ASPxLabel lblStatus = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblStatus") as ASPxLabel;
            if (lblStatus != null)
            {
                switch (AdminTaskStatusId)
                {
                    case (int)AdminTaskStatusList.Closed:
                        //colour = CellColourBad;
                        lblStatus.Text = "Closed";
                        lblStatus.ForeColor = Color.Red;
                        break;
                    case (int)AdminTaskStatusList.Open:
                        //colour = CellColourGood;
                        lblStatus.Text = "Open";
                        lblStatus.ForeColor = Color.Green;
                        break;
                    default:
                        break;
                }
            }

            //change colour of 'Status' column
            //int colIndex = 0;
            //int xtremeColCount = 1;
            //for (int i = 0; i < (sender as ASPxGridView).Columns.Count; i++)
            //{
            //    if ((sender as ASPxGridView).Columns[i].Name == "Status")
            //    {
            //        colIndex = (sender as ASPxGridView).Columns[i].VisibleIndex;
            //        break;
            //    }
            //}
            //if ((sender as ASPxGridView).SettingsDetail.ShowDetailButtons)
            //{
            //    colIndex += 1;
            //    xtremeColCount += 1;
            //}
            //if (e.Row.Cells.Count > xtremeColCount)
            //{
            //    if (e.Row.Cells[colIndex + 2] != null)
            //        e.Row.Cells[colIndex + 2].Style.Add("background-color", colour);
            //} 
            
        }

    }
}