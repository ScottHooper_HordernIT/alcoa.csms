﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.PermissionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(PermissionDataSourceDesigner))]
	public class PermissionDataSource : ProviderDataSource<Permission, PermissionKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PermissionDataSource class.
		/// </summary>
		public PermissionDataSource() : base(new PermissionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the PermissionDataSourceView used by the PermissionDataSource.
		/// </summary>
		protected PermissionDataSourceView PermissionView
		{
			get { return ( View as PermissionDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the PermissionDataSource control invokes to retrieve data.
		/// </summary>
		public PermissionSelectMethod SelectMethod
		{
			get
			{
				PermissionSelectMethod selectMethod = PermissionSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (PermissionSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the PermissionDataSourceView class that is to be
		/// used by the PermissionDataSource.
		/// </summary>
		/// <returns>An instance of the PermissionDataSourceView class.</returns>
		protected override BaseDataSourceView<Permission, PermissionKey> GetNewDataSourceView()
		{
			return new PermissionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the PermissionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class PermissionDataSourceView : ProviderDataSourceView<Permission, PermissionKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PermissionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the PermissionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public PermissionDataSourceView(PermissionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal PermissionDataSource PermissionOwner
		{
			get { return Owner as PermissionDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal PermissionSelectMethod SelectMethod
		{
			get { return PermissionOwner.SelectMethod; }
			set { PermissionOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal PermissionService PermissionProvider
		{
			get { return Provider as PermissionService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Permission> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Permission> results = null;
			Permission item;
			count = 0;
			
			System.Int32 _permissionId;
			System.String _permission;

			switch ( SelectMethod )
			{
				case PermissionSelectMethod.Get:
					PermissionKey entityKey  = new PermissionKey();
					entityKey.Load(values);
					item = PermissionProvider.Get(entityKey);
					results = new TList<Permission>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case PermissionSelectMethod.GetAll:
                    results = PermissionProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case PermissionSelectMethod.GetPaged:
					results = PermissionProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case PermissionSelectMethod.Find:
					if ( FilterParameters != null )
						results = PermissionProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = PermissionProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case PermissionSelectMethod.GetByPermissionId:
					_permissionId = ( values["PermissionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["PermissionId"], typeof(System.Int32)) : (int)0;
					item = PermissionProvider.GetByPermissionId(_permissionId);
					results = new TList<Permission>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case PermissionSelectMethod.GetByPermission:
					_permission = ( values["Permission"] != null ) ? (System.String) EntityUtil.ChangeType(values["Permission"], typeof(System.String)) : string.Empty;
					item = PermissionProvider.GetByPermission(_permission);
					results = new TList<Permission>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == PermissionSelectMethod.Get || SelectMethod == PermissionSelectMethod.GetByPermissionId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Permission entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					PermissionProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Permission> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			PermissionProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region PermissionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the PermissionDataSource class.
	/// </summary>
	public class PermissionDataSourceDesigner : ProviderDataSourceDesigner<Permission, PermissionKey>
	{
		/// <summary>
		/// Initializes a new instance of the PermissionDataSourceDesigner class.
		/// </summary>
		public PermissionDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PermissionSelectMethod SelectMethod
		{
			get { return ((PermissionDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new PermissionDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region PermissionDataSourceActionList

	/// <summary>
	/// Supports the PermissionDataSourceDesigner class.
	/// </summary>
	internal class PermissionDataSourceActionList : DesignerActionList
	{
		private PermissionDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the PermissionDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public PermissionDataSourceActionList(PermissionDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PermissionSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion PermissionDataSourceActionList
	
	#endregion PermissionDataSourceDesigner
	
	#region PermissionSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the PermissionDataSource.SelectMethod property.
	/// </summary>
	public enum PermissionSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByPermissionId method.
		/// </summary>
		GetByPermissionId,
		/// <summary>
		/// Represents the GetByPermission method.
		/// </summary>
		GetByPermission
	}
	
	#endregion PermissionSelectMethod

	#region PermissionFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Permission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionFilter : SqlFilter<PermissionColumn>
	{
	}
	
	#endregion PermissionFilter

	#region PermissionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Permission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionExpressionBuilder : SqlExpressionBuilder<PermissionColumn>
	{
	}
	
	#endregion PermissionExpressionBuilder	

	#region PermissionProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;PermissionChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Permission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionProperty : ChildEntityProperty<PermissionChildEntityTypes>
	{
	}
	
	#endregion PermissionProperty
}

