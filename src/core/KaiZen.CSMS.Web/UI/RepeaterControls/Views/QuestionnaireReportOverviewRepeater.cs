﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireReportOverviewRepeater</c>
    /// </summary>
	public class QuestionnaireReportOverviewRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewRepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireReportOverviewRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireReportOverviewRepeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireReportOverviewRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireReportOverviewRepeater z = (QuestionnaireReportOverviewRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireReportOverviewRepeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireReportOverviewRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireReportOverviewRepeater runat=\"server\"></{0}:QuestionnaireReportOverviewRepeater>")]
	public class QuestionnaireReportOverviewRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewRepeater"/> class.
        /// </summary>
		public QuestionnaireReportOverviewRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireReportOverviewItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireReportOverview entity = o as KaiZen.CSMS.Entities.QuestionnaireReportOverview;
						QuestionnaireReportOverviewItem container = new QuestionnaireReportOverviewItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireReportOverviewItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireReportOverview _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewItem"/> class.
        /// </summary>
		public QuestionnaireReportOverviewItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewItem"/> class.
        /// </summary>
		public QuestionnaireReportOverviewItem(KaiZen.CSMS.Entities.QuestionnaireReportOverview entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the CompanyAbn
        /// </summary>
        /// <value>The CompanyAbn.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyAbn
		{
			get { return _entity.CompanyAbn; }
		}
        /// <summary>
        /// Gets the Recommended
        /// </summary>
        /// <value>The Recommended.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Recommended
		{
			get { return _entity.Recommended; }
		}
        /// <summary>
        /// Gets the InitialRiskAssessment
        /// </summary>
        /// <value>The InitialRiskAssessment.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String InitialRiskAssessment
		{
			get { return _entity.InitialRiskAssessment; }
		}
        /// <summary>
        /// Gets the MainAssessmentRiskRating
        /// </summary>
        /// <value>The MainAssessmentRiskRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MainAssessmentRiskRating
		{
			get { return _entity.MainAssessmentRiskRating; }
		}
        /// <summary>
        /// Gets the FinalRiskRating
        /// </summary>
        /// <value>The FinalRiskRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FinalRiskRating
		{
			get { return _entity.FinalRiskRating; }
		}
        /// <summary>
        /// Gets the Status
        /// </summary>
        /// <value>The Status.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Status
		{
			get { return _entity.Status; }
		}
        /// <summary>
        /// Gets the Type
        /// </summary>
        /// <value>The Type.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Type
		{
			get { return _entity.Type; }
		}
        /// <summary>
        /// Gets the DescriptionOfWork
        /// </summary>
        /// <value>The DescriptionOfWork.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String DescriptionOfWork
		{
			get { return _entity.DescriptionOfWork; }
		}
        /// <summary>
        /// Gets the LevelOfSupervision
        /// </summary>
        /// <value>The LevelOfSupervision.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String LevelOfSupervision
		{
			get { return _entity.LevelOfSupervision; }
		}
        /// <summary>
        /// Gets the PrimaryContractor
        /// </summary>
        /// <value>The PrimaryContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PrimaryContractor
		{
			get { return _entity.PrimaryContractor; }
		}
        /// <summary>
        /// Gets the ProcurementContactUserId
        /// </summary>
        /// <value>The ProcurementContactUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ProcurementContactUserId
		{
			get { return _entity.ProcurementContactUserId; }
		}
        /// <summary>
        /// Gets the ContractManagerUserId
        /// </summary>
        /// <value>The ContractManagerUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ContractManagerUserId
		{
			get { return _entity.ContractManagerUserId; }
		}
        /// <summary>
        /// Gets the ProcurementContactUser
        /// </summary>
        /// <value>The ProcurementContactUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProcurementContactUser
		{
			get { return _entity.ProcurementContactUser; }
		}
        /// <summary>
        /// Gets the ContractManagerUser
        /// </summary>
        /// <value>The ContractManagerUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ContractManagerUser
		{
			get { return _entity.ContractManagerUser; }
		}
        /// <summary>
        /// Gets the TypeOfService
        /// </summary>
        /// <value>The TypeOfService.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TypeOfService
		{
			get { return _entity.TypeOfService; }
		}
        /// <summary>
        /// Gets the MainAssessmentValidTo
        /// </summary>
        /// <value>The MainAssessmentValidTo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainAssessmentValidTo
		{
			get { return _entity.MainAssessmentValidTo; }
		}
        /// <summary>
        /// Gets the ApprovedByUserId
        /// </summary>
        /// <value>The ApprovedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ApprovedByUserId
		{
			get { return _entity.ApprovedByUserId; }
		}
        /// <summary>
        /// Gets the ApprovedByUser
        /// </summary>
        /// <value>The ApprovedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ApprovedByUser
		{
			get { return _entity.ApprovedByUser; }
		}
        /// <summary>
        /// Gets the IsMainRequired
        /// </summary>
        /// <value>The IsMainRequired.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsMainRequired
		{
			get { return _entity.IsMainRequired; }
		}
        /// <summary>
        /// Gets the IsVerificationRequired
        /// </summary>
        /// <value>The IsVerificationRequired.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsVerificationRequired
		{
			get { return _entity.IsVerificationRequired; }
		}
        /// <summary>
        /// Gets the QuestionnaireId
        /// </summary>
        /// <value>The QuestionnaireId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QuestionnaireId
		{
			get { return _entity.QuestionnaireId; }
		}
        /// <summary>
        /// Gets the ApprovedDate
        /// </summary>
        /// <value>The ApprovedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? ApprovedDate
		{
			get { return _entity.ApprovedDate; }
		}
        /// <summary>
        /// Gets the NumberOfPeople
        /// </summary>
        /// <value>The NumberOfPeople.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String NumberOfPeople
		{
			get { return _entity.NumberOfPeople; }
		}
        /// <summary>
        /// Gets the LastSpqDate
        /// </summary>
        /// <value>The LastSpqDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? LastSpqDate
		{
			get { return _entity.LastSpqDate; }
		}
        /// <summary>
        /// Gets the InitialSpqDate
        /// </summary>
        /// <value>The InitialSpqDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? InitialSpqDate
		{
			get { return _entity.InitialSpqDate; }
		}
        /// <summary>
        /// Gets the Active
        /// </summary>
        /// <value>The Active.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Active
		{
			get { return _entity.Active; }
		}
        /// <summary>
        /// Gets the MainScorePoverall
        /// </summary>
        /// <value>The MainScorePoverall.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePoverall
		{
			get { return _entity.MainScorePoverall; }
		}
        /// <summary>
        /// Gets the CompanyStatusDesc
        /// </summary>
        /// <value>The CompanyStatusDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyStatusDesc
		{
			get { return _entity.CompanyStatusDesc; }
		}
        /// <summary>
        /// Gets the EhsConsultantId
        /// </summary>
        /// <value>The EhsConsultantId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? EhsConsultantId
		{
			get { return _entity.EhsConsultantId; }
		}
        /// <summary>
        /// Gets the SafetyAssessor
        /// </summary>
        /// <value>The SafetyAssessor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SafetyAssessor
		{
			get { return _entity.SafetyAssessor; }
		}
        /// <summary>
        /// Gets the CreatedByUserId
        /// </summary>
        /// <value>The CreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CreatedByUserId
		{
			get { return _entity.CreatedByUserId; }
		}
        /// <summary>
        /// Gets the CreatedByUser
        /// </summary>
        /// <value>The CreatedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CreatedByUser
		{
			get { return _entity.CreatedByUser; }
		}
        /// <summary>
        /// Gets the CreatedDate
        /// </summary>
        /// <value>The CreatedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime CreatedDate
		{
			get { return _entity.CreatedDate; }
		}
        /// <summary>
        /// Gets the ModifiedByUserId
        /// </summary>
        /// <value>The ModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedByUserId
		{
			get { return _entity.ModifiedByUserId; }
		}
        /// <summary>
        /// Gets the ModifiedByUser
        /// </summary>
        /// <value>The ModifiedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ModifiedByUser
		{
			get { return _entity.ModifiedByUser; }
		}
        /// <summary>
        /// Gets the ModifiedDate
        /// </summary>
        /// <value>The ModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime ModifiedDate
		{
			get { return _entity.ModifiedDate; }
		}
        /// <summary>
        /// Gets the SubContractor
        /// </summary>
        /// <value>The SubContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? SubContractor
		{
			get { return _entity.SubContractor; }
		}
        /// <summary>
        /// Gets the Validity
        /// </summary>
        /// <value>The Validity.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Validity
		{
			get { return _entity.Validity; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithActionId
        /// </summary>
        /// <value>The QuestionnairePresentlyWithActionId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithActionId
		{
			get { return _entity.QuestionnairePresentlyWithActionId; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSince
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSince.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QuestionnairePresentlyWithSince
		{
			get { return _entity.QuestionnairePresentlyWithSince; }
		}
        /// <summary>
        /// Gets the ProcessNo
        /// </summary>
        /// <value>The ProcessNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ProcessNo
		{
			get { return _entity.ProcessNo; }
		}
        /// <summary>
        /// Gets the UserName
        /// </summary>
        /// <value>The UserName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserName
		{
			get { return _entity.UserName; }
		}
        /// <summary>
        /// Gets the ActionName
        /// </summary>
        /// <value>The ActionName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionName
		{
			get { return _entity.ActionName; }
		}
        /// <summary>
        /// Gets the UserDescription
        /// </summary>
        /// <value>The UserDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserDescription
		{
			get { return _entity.UserDescription; }
		}
        /// <summary>
        /// Gets the ActionDescription
        /// </summary>
        /// <value>The ActionDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionDescription
		{
			get { return _entity.ActionDescription; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSinceDaysCount
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSinceDaysCount.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithSinceDaysCount
		{
			get { return _entity.QuestionnairePresentlyWithSinceDaysCount; }
		}
        /// <summary>
        /// Gets the DaysTillExpiry
        /// </summary>
        /// <value>The DaysTillExpiry.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? DaysTillExpiry
		{
			get { return _entity.DaysTillExpiry; }
		}
        /// <summary>
        /// Gets the TrafficLight
        /// </summary>
        /// <value>The TrafficLight.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String TrafficLight
		{
			get { return _entity.TrafficLight; }
		}
        /// <summary>
        /// Gets the CompanyId1
        /// </summary>
        /// <value>The CompanyId1.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId1
		{
			get { return _entity.CompanyId1; }
		}
        /// <summary>
        /// Gets the Kwi
        /// </summary>
        /// <value>The Kwi.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Kwi
		{
			get { return _entity.Kwi; }
		}
        /// <summary>
        /// Gets the KwiSpaName
        /// </summary>
        /// <value>The KwiSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiSpaName
		{
			get { return _entity.KwiSpaName; }
		}
        /// <summary>
        /// Gets the KwiSpa
        /// </summary>
        /// <value>The KwiSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? KwiSpa
		{
			get { return _entity.KwiSpa; }
		}
        /// <summary>
        /// Gets the KwiSponsorName
        /// </summary>
        /// <value>The KwiSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiSponsorName
		{
			get { return _entity.KwiSponsorName; }
		}
        /// <summary>
        /// Gets the KwiSponsor
        /// </summary>
        /// <value>The KwiSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? KwiSponsor
		{
			get { return _entity.KwiSponsor; }
		}
        /// <summary>
        /// Gets the KwiCompanySiteCategory
        /// </summary>
        /// <value>The KwiCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiCompanySiteCategory
		{
			get { return _entity.KwiCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Pin
        /// </summary>
        /// <value>The Pin.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pin
		{
			get { return _entity.Pin; }
		}
        /// <summary>
        /// Gets the PinSpaName
        /// </summary>
        /// <value>The PinSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinSpaName
		{
			get { return _entity.PinSpaName; }
		}
        /// <summary>
        /// Gets the PinSpa
        /// </summary>
        /// <value>The PinSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PinSpa
		{
			get { return _entity.PinSpa; }
		}
        /// <summary>
        /// Gets the PinSponsorName
        /// </summary>
        /// <value>The PinSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinSponsorName
		{
			get { return _entity.PinSponsorName; }
		}
        /// <summary>
        /// Gets the PinSponsor
        /// </summary>
        /// <value>The PinSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PinSponsor
		{
			get { return _entity.PinSponsor; }
		}
        /// <summary>
        /// Gets the PinCompanySiteCategory
        /// </summary>
        /// <value>The PinCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinCompanySiteCategory
		{
			get { return _entity.PinCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Wgp
        /// </summary>
        /// <value>The Wgp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Wgp
		{
			get { return _entity.Wgp; }
		}
        /// <summary>
        /// Gets the WgpSpaName
        /// </summary>
        /// <value>The WgpSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpSpaName
		{
			get { return _entity.WgpSpaName; }
		}
        /// <summary>
        /// Gets the WgpSpa
        /// </summary>
        /// <value>The WgpSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WgpSpa
		{
			get { return _entity.WgpSpa; }
		}
        /// <summary>
        /// Gets the WgpSponsorName
        /// </summary>
        /// <value>The WgpSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpSponsorName
		{
			get { return _entity.WgpSponsorName; }
		}
        /// <summary>
        /// Gets the WgpSponsor
        /// </summary>
        /// <value>The WgpSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WgpSponsor
		{
			get { return _entity.WgpSponsor; }
		}
        /// <summary>
        /// Gets the WgpCompanySiteCategory
        /// </summary>
        /// <value>The WgpCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpCompanySiteCategory
		{
			get { return _entity.WgpCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Hun
        /// </summary>
        /// <value>The Hun.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Hun
		{
			get { return _entity.Hun; }
		}
        /// <summary>
        /// Gets the HunSpaName
        /// </summary>
        /// <value>The HunSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunSpaName
		{
			get { return _entity.HunSpaName; }
		}
        /// <summary>
        /// Gets the HunSpa
        /// </summary>
        /// <value>The HunSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HunSpa
		{
			get { return _entity.HunSpa; }
		}
        /// <summary>
        /// Gets the HunSponsorName
        /// </summary>
        /// <value>The HunSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunSponsorName
		{
			get { return _entity.HunSponsorName; }
		}
        /// <summary>
        /// Gets the HunSponsor
        /// </summary>
        /// <value>The HunSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HunSponsor
		{
			get { return _entity.HunSponsor; }
		}
        /// <summary>
        /// Gets the HunCompanySiteCategory
        /// </summary>
        /// <value>The HunCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunCompanySiteCategory
		{
			get { return _entity.HunCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Wdl
        /// </summary>
        /// <value>The Wdl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Wdl
		{
			get { return _entity.Wdl; }
		}
        /// <summary>
        /// Gets the WdlSpaName
        /// </summary>
        /// <value>The WdlSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlSpaName
		{
			get { return _entity.WdlSpaName; }
		}
        /// <summary>
        /// Gets the WdlSpa
        /// </summary>
        /// <value>The WdlSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WdlSpa
		{
			get { return _entity.WdlSpa; }
		}
        /// <summary>
        /// Gets the WdlSponsorName
        /// </summary>
        /// <value>The WdlSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlSponsorName
		{
			get { return _entity.WdlSponsorName; }
		}
        /// <summary>
        /// Gets the WdlSponsor
        /// </summary>
        /// <value>The WdlSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? WdlSponsor
		{
			get { return _entity.WdlSponsor; }
		}
        /// <summary>
        /// Gets the WdlCompanySiteCategory
        /// </summary>
        /// <value>The WdlCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlCompanySiteCategory
		{
			get { return _entity.WdlCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Bun
        /// </summary>
        /// <value>The Bun.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Bun
		{
			get { return _entity.Bun; }
		}
        /// <summary>
        /// Gets the BunSpaName
        /// </summary>
        /// <value>The BunSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunSpaName
		{
			get { return _entity.BunSpaName; }
		}
        /// <summary>
        /// Gets the BunSpa
        /// </summary>
        /// <value>The BunSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BunSpa
		{
			get { return _entity.BunSpa; }
		}
        /// <summary>
        /// Gets the BunSponsorName
        /// </summary>
        /// <value>The BunSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunSponsorName
		{
			get { return _entity.BunSponsorName; }
		}
        /// <summary>
        /// Gets the BunSponsor
        /// </summary>
        /// <value>The BunSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BunSponsor
		{
			get { return _entity.BunSponsor; }
		}
        /// <summary>
        /// Gets the BunCompanySiteCategory
        /// </summary>
        /// <value>The BunCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunCompanySiteCategory
		{
			get { return _entity.BunCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Fml
        /// </summary>
        /// <value>The Fml.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Fml
		{
			get { return _entity.Fml; }
		}
        /// <summary>
        /// Gets the FmlSpaName
        /// </summary>
        /// <value>The FmlSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlSpaName
		{
			get { return _entity.FmlSpaName; }
		}
        /// <summary>
        /// Gets the FmlSpa
        /// </summary>
        /// <value>The FmlSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? FmlSpa
		{
			get { return _entity.FmlSpa; }
		}
        /// <summary>
        /// Gets the FmlSponsorName
        /// </summary>
        /// <value>The FmlSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlSponsorName
		{
			get { return _entity.FmlSponsorName; }
		}
        /// <summary>
        /// Gets the FmlSponsor
        /// </summary>
        /// <value>The FmlSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? FmlSponsor
		{
			get { return _entity.FmlSponsor; }
		}
        /// <summary>
        /// Gets the FmlCompanySiteCategory
        /// </summary>
        /// <value>The FmlCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlCompanySiteCategory
		{
			get { return _entity.FmlCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Bgn
        /// </summary>
        /// <value>The Bgn.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Bgn
		{
			get { return _entity.Bgn; }
		}
        /// <summary>
        /// Gets the BgnSpaName
        /// </summary>
        /// <value>The BgnSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnSpaName
		{
			get { return _entity.BgnSpaName; }
		}
        /// <summary>
        /// Gets the BgnSpa
        /// </summary>
        /// <value>The BgnSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BgnSpa
		{
			get { return _entity.BgnSpa; }
		}
        /// <summary>
        /// Gets the BgnSponsorName
        /// </summary>
        /// <value>The BgnSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnSponsorName
		{
			get { return _entity.BgnSponsorName; }
		}
        /// <summary>
        /// Gets the BgnSponsor
        /// </summary>
        /// <value>The BgnSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BgnSponsor
		{
			get { return _entity.BgnSponsor; }
		}
        /// <summary>
        /// Gets the BgnCompanySiteCategory
        /// </summary>
        /// <value>The BgnCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnCompanySiteCategory
		{
			get { return _entity.BgnCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Ce
        /// </summary>
        /// <value>The Ce.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ce
		{
			get { return _entity.Ce; }
		}
        /// <summary>
        /// Gets the CeSpaName
        /// </summary>
        /// <value>The CeSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeSpaName
		{
			get { return _entity.CeSpaName; }
		}
        /// <summary>
        /// Gets the CeSpa
        /// </summary>
        /// <value>The CeSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CeSpa
		{
			get { return _entity.CeSpa; }
		}
        /// <summary>
        /// Gets the CeSponsorName
        /// </summary>
        /// <value>The CeSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeSponsorName
		{
			get { return _entity.CeSponsorName; }
		}
        /// <summary>
        /// Gets the CeSponsor
        /// </summary>
        /// <value>The CeSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CeSponsor
		{
			get { return _entity.CeSponsor; }
		}
        /// <summary>
        /// Gets the CeCompanySiteCategory
        /// </summary>
        /// <value>The CeCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeCompanySiteCategory
		{
			get { return _entity.CeCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Ang
        /// </summary>
        /// <value>The Ang.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ang
		{
			get { return _entity.Ang; }
		}
        /// <summary>
        /// Gets the AngSpaName
        /// </summary>
        /// <value>The AngSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngSpaName
		{
			get { return _entity.AngSpaName; }
		}
        /// <summary>
        /// Gets the AngSpa
        /// </summary>
        /// <value>The AngSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AngSpa
		{
			get { return _entity.AngSpa; }
		}
        /// <summary>
        /// Gets the AngSponsorName
        /// </summary>
        /// <value>The AngSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngSponsorName
		{
			get { return _entity.AngSponsorName; }
		}
        /// <summary>
        /// Gets the AngSponsor
        /// </summary>
        /// <value>The AngSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AngSponsor
		{
			get { return _entity.AngSponsor; }
		}
        /// <summary>
        /// Gets the AngCompanySiteCategory
        /// </summary>
        /// <value>The AngCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngCompanySiteCategory
		{
			get { return _entity.AngCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Ptl
        /// </summary>
        /// <value>The Ptl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ptl
		{
			get { return _entity.Ptl; }
		}
        /// <summary>
        /// Gets the PtlSpaName
        /// </summary>
        /// <value>The PtlSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlSpaName
		{
			get { return _entity.PtlSpaName; }
		}
        /// <summary>
        /// Gets the PtlSpa
        /// </summary>
        /// <value>The PtlSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PtlSpa
		{
			get { return _entity.PtlSpa; }
		}
        /// <summary>
        /// Gets the PtlSponsorName
        /// </summary>
        /// <value>The PtlSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlSponsorName
		{
			get { return _entity.PtlSponsorName; }
		}
        /// <summary>
        /// Gets the PtlSponsor
        /// </summary>
        /// <value>The PtlSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PtlSponsor
		{
			get { return _entity.PtlSponsor; }
		}
        /// <summary>
        /// Gets the PtlCompanySiteCategory
        /// </summary>
        /// <value>The PtlCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlCompanySiteCategory
		{
			get { return _entity.PtlCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Pth
        /// </summary>
        /// <value>The Pth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pth
		{
			get { return _entity.Pth; }
		}
        /// <summary>
        /// Gets the PthSpaName
        /// </summary>
        /// <value>The PthSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthSpaName
		{
			get { return _entity.PthSpaName; }
		}
        /// <summary>
        /// Gets the PthSpa
        /// </summary>
        /// <value>The PthSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PthSpa
		{
			get { return _entity.PthSpa; }
		}
        /// <summary>
        /// Gets the PthSponsorName
        /// </summary>
        /// <value>The PthSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthSponsorName
		{
			get { return _entity.PthSponsorName; }
		}
        /// <summary>
        /// Gets the PthSponsor
        /// </summary>
        /// <value>The PthSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PthSponsor
		{
			get { return _entity.PthSponsor; }
		}
        /// <summary>
        /// Gets the PthCompanySiteCategory
        /// </summary>
        /// <value>The PthCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthCompanySiteCategory
		{
			get { return _entity.PthCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Pel
        /// </summary>
        /// <value>The Pel.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pel
		{
			get { return _entity.Pel; }
		}
        /// <summary>
        /// Gets the PelSpaName
        /// </summary>
        /// <value>The PelSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelSpaName
		{
			get { return _entity.PelSpaName; }
		}
        /// <summary>
        /// Gets the PelSpa
        /// </summary>
        /// <value>The PelSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PelSpa
		{
			get { return _entity.PelSpa; }
		}
        /// <summary>
        /// Gets the PelSponsorName
        /// </summary>
        /// <value>The PelSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelSponsorName
		{
			get { return _entity.PelSponsorName; }
		}
        /// <summary>
        /// Gets the PelSponsor
        /// </summary>
        /// <value>The PelSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? PelSponsor
		{
			get { return _entity.PelSponsor; }
		}
        /// <summary>
        /// Gets the PelCompanySiteCategory
        /// </summary>
        /// <value>The PelCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelCompanySiteCategory
		{
			get { return _entity.PelCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Arp
        /// </summary>
        /// <value>The Arp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Arp
		{
			get { return _entity.Arp; }
		}
        /// <summary>
        /// Gets the ArpSpaName
        /// </summary>
        /// <value>The ArpSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpSpaName
		{
			get { return _entity.ArpSpaName; }
		}
        /// <summary>
        /// Gets the ArpSpa
        /// </summary>
        /// <value>The ArpSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ArpSpa
		{
			get { return _entity.ArpSpa; }
		}
        /// <summary>
        /// Gets the ArpSponsorName
        /// </summary>
        /// <value>The ArpSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpSponsorName
		{
			get { return _entity.ArpSponsorName; }
		}
        /// <summary>
        /// Gets the ArpSponsor
        /// </summary>
        /// <value>The ArpSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ArpSponsor
		{
			get { return _entity.ArpSponsor; }
		}
        /// <summary>
        /// Gets the ArpCompanySiteCategory
        /// </summary>
        /// <value>The ArpCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpCompanySiteCategory
		{
			get { return _entity.ArpCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the Yen
        /// </summary>
        /// <value>The Yen.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Yen
		{
			get { return _entity.Yen; }
		}
        /// <summary>
        /// Gets the YenSpaName
        /// </summary>
        /// <value>The YenSpaName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenSpaName
		{
			get { return _entity.YenSpaName; }
		}
        /// <summary>
        /// Gets the YenSpa
        /// </summary>
        /// <value>The YenSpa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? YenSpa
		{
			get { return _entity.YenSpa; }
		}
        /// <summary>
        /// Gets the YenSponsorName
        /// </summary>
        /// <value>The YenSponsorName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenSponsorName
		{
			get { return _entity.YenSponsorName; }
		}
        /// <summary>
        /// Gets the YenSponsor
        /// </summary>
        /// <value>The YenSponsor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? YenSponsor
		{
			get { return _entity.YenSponsor; }
		}
        /// <summary>
        /// Gets the YenCompanySiteCategory
        /// </summary>
        /// <value>The YenCompanySiteCategory.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenCompanySiteCategory
		{
			get { return _entity.YenCompanySiteCategory; }
		}
        /// <summary>
        /// Gets the CompanyId2
        /// </summary>
        /// <value>The CompanyId2.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId2
		{
			get { return _entity.CompanyId2; }
		}
        /// <summary>
        /// Gets the KwiArpName
        /// </summary>
        /// <value>The KwiArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiArpName
		{
			get { return _entity.KwiArpName; }
		}
        /// <summary>
        /// Gets the KwiCrpName
        /// </summary>
        /// <value>The KwiCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiCrpName
		{
			get { return _entity.KwiCrpName; }
		}
        /// <summary>
        /// Gets the PinArpName
        /// </summary>
        /// <value>The PinArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinArpName
		{
			get { return _entity.PinArpName; }
		}
        /// <summary>
        /// Gets the PinCrpName
        /// </summary>
        /// <value>The PinCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinCrpName
		{
			get { return _entity.PinCrpName; }
		}
        /// <summary>
        /// Gets the WgpArpName
        /// </summary>
        /// <value>The WgpArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpArpName
		{
			get { return _entity.WgpArpName; }
		}
        /// <summary>
        /// Gets the WgpCrpName
        /// </summary>
        /// <value>The WgpCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpCrpName
		{
			get { return _entity.WgpCrpName; }
		}
        /// <summary>
        /// Gets the HunArpName
        /// </summary>
        /// <value>The HunArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunArpName
		{
			get { return _entity.HunArpName; }
		}
        /// <summary>
        /// Gets the HunCrpName
        /// </summary>
        /// <value>The HunCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunCrpName
		{
			get { return _entity.HunCrpName; }
		}
        /// <summary>
        /// Gets the WdlArpName
        /// </summary>
        /// <value>The WdlArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlArpName
		{
			get { return _entity.WdlArpName; }
		}
        /// <summary>
        /// Gets the WdlCrpName
        /// </summary>
        /// <value>The WdlCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlCrpName
		{
			get { return _entity.WdlCrpName; }
		}
        /// <summary>
        /// Gets the BunArpName
        /// </summary>
        /// <value>The BunArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunArpName
		{
			get { return _entity.BunArpName; }
		}
        /// <summary>
        /// Gets the BunCrpName
        /// </summary>
        /// <value>The BunCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunCrpName
		{
			get { return _entity.BunCrpName; }
		}
        /// <summary>
        /// Gets the FmlArpName
        /// </summary>
        /// <value>The FmlArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlArpName
		{
			get { return _entity.FmlArpName; }
		}
        /// <summary>
        /// Gets the FmlCrpName
        /// </summary>
        /// <value>The FmlCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlCrpName
		{
			get { return _entity.FmlCrpName; }
		}
        /// <summary>
        /// Gets the BgnArpName
        /// </summary>
        /// <value>The BgnArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnArpName
		{
			get { return _entity.BgnArpName; }
		}
        /// <summary>
        /// Gets the BgnCrpName
        /// </summary>
        /// <value>The BgnCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnCrpName
		{
			get { return _entity.BgnCrpName; }
		}
        /// <summary>
        /// Gets the CeArpName
        /// </summary>
        /// <value>The CeArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeArpName
		{
			get { return _entity.CeArpName; }
		}
        /// <summary>
        /// Gets the CeCrpName
        /// </summary>
        /// <value>The CeCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeCrpName
		{
			get { return _entity.CeCrpName; }
		}
        /// <summary>
        /// Gets the AngArpName
        /// </summary>
        /// <value>The AngArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngArpName
		{
			get { return _entity.AngArpName; }
		}
        /// <summary>
        /// Gets the AngCrpName
        /// </summary>
        /// <value>The AngCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngCrpName
		{
			get { return _entity.AngCrpName; }
		}
        /// <summary>
        /// Gets the PtlArpName
        /// </summary>
        /// <value>The PtlArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlArpName
		{
			get { return _entity.PtlArpName; }
		}
        /// <summary>
        /// Gets the PtlCrpName
        /// </summary>
        /// <value>The PtlCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlCrpName
		{
			get { return _entity.PtlCrpName; }
		}
        /// <summary>
        /// Gets the PthArpName
        /// </summary>
        /// <value>The PthArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthArpName
		{
			get { return _entity.PthArpName; }
		}
        /// <summary>
        /// Gets the PthCrpName
        /// </summary>
        /// <value>The PthCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthCrpName
		{
			get { return _entity.PthCrpName; }
		}
        /// <summary>
        /// Gets the PelArpName
        /// </summary>
        /// <value>The PelArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelArpName
		{
			get { return _entity.PelArpName; }
		}
        /// <summary>
        /// Gets the PelCrpName
        /// </summary>
        /// <value>The PelCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelCrpName
		{
			get { return _entity.PelCrpName; }
		}
        /// <summary>
        /// Gets the ArpArpName
        /// </summary>
        /// <value>The ArpArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpArpName
		{
			get { return _entity.ArpArpName; }
		}
        /// <summary>
        /// Gets the ArpCrpName
        /// </summary>
        /// <value>The ArpCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpCrpName
		{
			get { return _entity.ArpCrpName; }
		}
        /// <summary>
        /// Gets the YenArpName
        /// </summary>
        /// <value>The YenArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenArpName
		{
			get { return _entity.YenArpName; }
		}
        /// <summary>
        /// Gets the YenCrpName
        /// </summary>
        /// <value>The YenCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenCrpName
		{
			get { return _entity.YenCrpName; }
		}
        /// <summary>
        /// Gets the RequestingCompanyId
        /// </summary>
        /// <value>The RequestingCompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? RequestingCompanyId
		{
			get { return _entity.RequestingCompanyId; }
		}
        /// <summary>
        /// Gets the RequestingCompanyName
        /// </summary>
        /// <value>The RequestingCompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String RequestingCompanyName
		{
			get { return _entity.RequestingCompanyName; }
		}
        /// <summary>
        /// Gets the ReasonContractor
        /// </summary>
        /// <value>The ReasonContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ReasonContractor
		{
			get { return _entity.ReasonContractor; }
		}

	}
}
