﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiService
    {
        Kpi Get(Expression<Func<Kpi, bool>> where, List<Expression<Func<Kpi, object>>> includes);
        List<Kpi> GetMany(int? top, Expression<Func<Kpi, bool>> where, Func<IQueryable<Kpi>, IOrderedQueryable<Kpi>> orderBy, List<Expression<Func<Kpi, object>>> includes);
        List<Kpi> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Kpi, bool>> where, Func<IQueryable<Kpi>, IOrderedQueryable<Kpi>> orderBy, List<Expression<Func<Kpi, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Kpi, bool>> where, List<Expression<Func<Kpi, object>>> includes);
        void Insert(Kpi item, bool saveChanges = true);
        void Update(Kpi item, bool saveChanges = true);
        void Delete(Kpi item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiService : IKpiService
    {
        private readonly IRepository<Kpi> _repository;

        public KpiService(IRepository<Kpi> repository)
        {
            this._repository = repository;
        }

        public Kpi Get(Expression<Func<Kpi, bool>> where, List<Expression<Func<Kpi, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Kpi> GetMany(int? top, Expression<Func<Kpi, bool>> where, Func<IQueryable<Kpi>, IOrderedQueryable<Kpi>> orderBy, List<Expression<Func<Kpi, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Kpi> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Kpi, bool>> where, Func<IQueryable<Kpi>, IOrderedQueryable<Kpi>> orderBy, List<Expression<Func<Kpi, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Kpi, bool>> where, List<Expression<Func<Kpi, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Kpi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Kpi");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Kpi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Kpi");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Kpi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Kpi");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
