﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileDbProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileDbDataSourceDesigner))]
	public class FileDbDataSource : ProviderDataSource<FileDb, FileDbKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbDataSource class.
		/// </summary>
		public FileDbDataSource() : base(new FileDbService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileDbDataSourceView used by the FileDbDataSource.
		/// </summary>
		protected FileDbDataSourceView FileDbView
		{
			get { return ( View as FileDbDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileDbDataSource control invokes to retrieve data.
		/// </summary>
		public FileDbSelectMethod SelectMethod
		{
			get
			{
				FileDbSelectMethod selectMethod = FileDbSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileDbSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileDbDataSourceView class that is to be
		/// used by the FileDbDataSource.
		/// </summary>
		/// <returns>An instance of the FileDbDataSourceView class.</returns>
		protected override BaseDataSourceView<FileDb, FileDbKey> GetNewDataSourceView()
		{
			return new FileDbDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileDbDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileDbDataSourceView : ProviderDataSourceView<FileDb, FileDbKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileDbDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileDbDataSourceView(FileDbDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileDbDataSource FileDbOwner
		{
			get { return Owner as FileDbDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileDbSelectMethod SelectMethod
		{
			get { return FileDbOwner.SelectMethod; }
			set { FileDbOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileDbService FileDbProvider
		{
			get { return Provider as FileDbService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileDb> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileDb> results = null;
			FileDb item;
			count = 0;
			
			System.Int32 _fileId;
			System.Int32 _companyId;
			System.Int32? _selfAssessmentResponseId_nullable;
			System.Int32 _modifiedByUserId;
			System.Int32? _statusModifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case FileDbSelectMethod.Get:
					FileDbKey entityKey  = new FileDbKey();
					entityKey.Load(values);
					item = FileDbProvider.Get(entityKey);
					results = new TList<FileDb>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileDbSelectMethod.GetAll:
                    results = FileDbProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileDbSelectMethod.GetPaged:
					results = FileDbProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileDbSelectMethod.Find:
					if ( FilterParameters != null )
						results = FileDbProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileDbProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileDbSelectMethod.GetByFileId:
					_fileId = ( values["FileId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileId"], typeof(System.Int32)) : (int)0;
					item = FileDbProvider.GetByFileId(_fileId);
					results = new TList<FileDb>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case FileDbSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = FileDbProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case FileDbSelectMethod.GetBySelfAssessmentResponseId:
					_selfAssessmentResponseId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SelfAssessmentResponseId"], typeof(System.Int32?));
					results = FileDbProvider.GetBySelfAssessmentResponseId(_selfAssessmentResponseId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case FileDbSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = FileDbProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case FileDbSelectMethod.GetByStatusModifiedByUserId:
					_statusModifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["StatusModifiedByUserId"], typeof(System.Int32?));
					results = FileDbProvider.GetByStatusModifiedByUserId(_statusModifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileDbSelectMethod.Get || SelectMethod == FileDbSelectMethod.GetByFileId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileDb entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileDbProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileDb> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileDbProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileDbDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileDbDataSource class.
	/// </summary>
	public class FileDbDataSourceDesigner : ProviderDataSourceDesigner<FileDb, FileDbKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileDbDataSourceDesigner class.
		/// </summary>
		public FileDbDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbSelectMethod SelectMethod
		{
			get { return ((FileDbDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileDbDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileDbDataSourceActionList

	/// <summary>
	/// Supports the FileDbDataSourceDesigner class.
	/// </summary>
	internal class FileDbDataSourceActionList : DesignerActionList
	{
		private FileDbDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileDbDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileDbDataSourceActionList(FileDbDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileDbDataSourceActionList
	
	#endregion FileDbDataSourceDesigner
	
	#region FileDbSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileDbDataSource.SelectMethod property.
	/// </summary>
	public enum FileDbSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileId method.
		/// </summary>
		GetByFileId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetBySelfAssessmentResponseId method.
		/// </summary>
		GetBySelfAssessmentResponseId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByStatusModifiedByUserId method.
		/// </summary>
		GetByStatusModifiedByUserId
	}
	
	#endregion FileDbSelectMethod

	#region FileDbFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbFilter : SqlFilter<FileDbColumn>
	{
	}
	
	#endregion FileDbFilter

	#region FileDbExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbExpressionBuilder : SqlExpressionBuilder<FileDbColumn>
	{
	}
	
	#endregion FileDbExpressionBuilder	

	#region FileDbProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileDbChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbProperty : ChildEntityProperty<FileDbChildEntityTypes>
	{
	}
	
	#endregion FileDbProperty
}

