﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.UserControls.Main.CompanyContractorReview" Codebehind="CompanyContractorReview.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>


<table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
    <tr>
        <td class="pageName" colspan="100">
            <span class="bodycopy"><span class="title">Company Contractor Review</span><br><span class=date>
                <dxe:ASPxLabel ID="ASPxCompany" runat="server" Text="">
                </dxe:ASPxLabel></span>
                <br />
                <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
        </td>
    </tr>
    <tr>
        <td colspan="100"><dxe:ASPxLabel ID="ASPxlblInfo1" runat="server" Text=""/>
        </td>
    </tr>
    <tr>
        <td colspan="100">
            <dxwgv:ASPxGridView ID="grid" runat="server" ClientInstanceName="grid" AutoGenerateColumns="False" Width="100%"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                OnCommandButtonInitialize="grid_CommandButtonInitialize"
                OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize" OnCellEditorInitialize="grid_CellEditorInitialize"
                OnRowValidating="grid_RowValidating" OnRowUpdating="grid_RowUpdating" OnCustomColumnDisplayText="grid_CustomColumnDisplayText" 
                KeyFieldName="CompanyAbn;CWKNumber">
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px" Width="11px" />
                    <ExpandedButton Height="12px" Width="11px" />
                    <DetailCollapsedButton Height="12px" Width="11px" />
                    <DetailExpandedButton Height="12px" Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue">
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                </Styles>
                <Styles Header-Wrap="True" />
                <Columns>
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" AllowDragDrop="False" Caption="Action" VisibleIndex="0" Width="75" FixedStyle="Left">
                        <EditButton Visible="True">
                        </EditButton>
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                        <HeaderTemplate>
                            <span>Action&nbsp;
                            <input type="checkbox" onclick="grid.SelectAllRowsOnPage(this.checked);" style="vertical-align: middle;"
                                runat="server" OnInit="HdrCheckBox_Init" title="Select/Unselect all rows on the page" />
                            </span>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Width="150" FixedStyle="Left">
                        <PropertiesComboBox IncrementalFilteringMode="StartsWith" TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="CWKNumber" VisibleIndex="2" Width="75" FixedStyle="Left">
                        <Settings SortMode="Value" />
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="FullName" VisibleIndex="3" SortIndex="1" SortOrder="Ascending" Width="150" FixedStyle="Left">
                        <Settings SortMode="DisplayText" />
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataCheckColumn Caption="Alcoa record Active?" FieldName="AlcoaRecordActive" VisibleIndex="4" Width="75" FixedStyle="Left">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataCheckColumn>
                    <dxwgv:GridViewDataCheckColumn Caption="Active Induction?" FieldName="ActiveInduction" VisibleIndex="5" Width="75">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataCheckColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Labour Classification" FieldName="LabourClassTypeId" VisibleIndex="6" Width="100">
                        <PropertiesComboBox IncrementalFilteringMode="StartsWith" TextField="LabourClassTypeDesc" ValueField="LabourClassTypeId" ValueType="System.Int32">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <EditFormSettings Visible="True" VisibleIndex="0" ColumnSpan="2" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataCheckColumn Caption="With Company Still?" FieldName="StillAtCompany" VisibleIndex="7" Width="75">
                        <EditFormSettings Visible="True" VisibleIndex="1" ColumnSpan="1" />
                    </dxwgv:GridViewDataCheckColumn>
                    <dxwgv:GridViewDataCheckColumn Caption="Active At Alcoa?" FieldName="ActiveAtAlcoa" VisibleIndex="8" Width="75">
                        <EditFormSettings Visible="True" VisibleIndex="2" ColumnSpan="1" />
                    </dxwgv:GridViewDataCheckColumn>
                    <dxwgv:GridViewDataCheckColumn Caption="Changes made?" FieldName="Changed" VisibleIndex="9" Width="75">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataCheckColumn>
                    <dxwgv:GridViewDataCheckColumn Caption="Processed?" FieldName="Processed" VisibleIndex="10" Width="75">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataCheckColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Comments" VisibleIndex="11" Width="150">
                        <EditFormSettings Visible="True" VisibleIndex="3" ColumnSpan="4" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Templates>
                  <EditForm>
                    <div style="width:700px">
                        <dxwgv:ASPxGridViewTemplateReplacement ReplacementType="EditFormEditors" ID="Editors" runat="server">
                        </dxwgv:ASPxGridViewTemplateReplacement>
                        <dxwgv:ASPxGridViewTemplateReplacement ReplacementType="EditFormUpdateButton" runat="server" ID="UpdateButton">
                        </dxwgv:ASPxGridViewTemplateReplacement>
                        &nbsp;&nbsp;&nbsp;
                        <dxwgv:ASPxGridViewTemplateReplacement ReplacementType="EditFormCancelButton" runat="server" ID="CancelButton">
                        </dxwgv:ASPxGridViewTemplateReplacement>
                    </div>
                  </EditForm>
                </Templates>
                <SettingsEditing EditFormColumnCount="4" Mode="EditFormAndDisplayRow">
                </SettingsEditing>
                <Settings ShowGroupPanel="True"  ShowFilterRow="True" ShowFilterBar="Visible" HorizontalScrollBarMode="Auto" />
                <SettingsBehavior ColumnResizeMode="Control" />
                <SettingsPager>
                    <AllButton Visible="True">
                    </AllButton>
                    <PageSizeItemSettings ShowAllItem="True" Visible="True">
                    </PageSizeItemSettings>
                </SettingsPager>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">
            <dxe:ASPxButton ID="RetrieveRefreshBtn" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" OnClick="RetrieveRefreshBtn_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                Text="Retrieve/Refresh Contractor List" Width="100px">
            </dxe:ASPxButton>
            <dxe:ASPxButton ID="UpdateProcessedBtn" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" OnClick="UpdateProcessedBtn_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                Text="Update Processed Records" Width="100px">
                <ClientSideEvents Click="function(s, e) { if(grid.GetSelectedRowCount() <= 0) { alert('Nothing selected to update'); e.processOnServer = false; } else { e.processOnServer = true; } }" />
            </dxe:ASPxButton>
        </td>
        <td style="width: 60%; text-align: right; text-align:-moz-right;">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server"/>
            </div>
        </td>
    </tr>
</table>
