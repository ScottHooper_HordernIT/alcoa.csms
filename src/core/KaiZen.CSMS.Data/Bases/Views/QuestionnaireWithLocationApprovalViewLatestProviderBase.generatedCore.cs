﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireWithLocationApprovalViewLatestProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireWithLocationApprovalViewLatestProviderBaseCore : EntityViewProviderBase<QuestionnaireWithLocationApprovalViewLatest>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireWithLocationApprovalViewLatest_ListCompaniesApprovedForSubContractors
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListCompaniesApprovedForSubContractors' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListCompaniesApprovedForSubContractors()
		{
			return ListCompaniesApprovedForSubContractors(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListCompaniesApprovedForSubContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListCompaniesApprovedForSubContractors(int start, int pageLength)
		{
			return ListCompaniesApprovedForSubContractors(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListCompaniesApprovedForSubContractors' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListCompaniesApprovedForSubContractors(TransactionManager transactionManager)
		{
			return ListCompaniesApprovedForSubContractors(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListCompaniesApprovedForSubContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListCompaniesApprovedForSubContractors(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalViewLatest_GetProcurementContactHsAssessor_ByCompanyId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementContactHsAssessor_ByCompanyId(System.Int32? companyId)
		{
			return GetProcurementContactHsAssessor_ByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementContactHsAssessor_ByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetProcurementContactHsAssessor_ByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementContactHsAssessor_ByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetProcurementContactHsAssessor_ByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetProcurementContactHsAssessor_ByCompanyId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalViewLatest_NotAssessmentComplete_ByUserId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NotAssessmentComplete_ByUserId(System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return NotAssessmentComplete_ByUserId(null, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NotAssessmentComplete_ByUserId(int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return NotAssessmentComplete_ByUserId(null, start, pageLength , statusId, userId, proc);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NotAssessmentComplete_ByUserId(TransactionManager transactionManager, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return NotAssessmentComplete_ByUserId(transactionManager, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet NotAssessmentComplete_ByUserId(TransactionManager transactionManager, int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalViewLatest_ListContractManagerUsers
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListContractManagerUsers' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListContractManagerUsers()
		{
			return ListContractManagerUsers(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListContractManagerUsers' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListContractManagerUsers(int start, int pageLength)
		{
			return ListContractManagerUsers(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListContractManagerUsers' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListContractManagerUsers(TransactionManager transactionManager)
		{
			return ListContractManagerUsers(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListContractManagerUsers' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListContractManagerUsers(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalViewLatest_AssessmentComplete_ByUserId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet AssessmentComplete_ByUserId(System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return AssessmentComplete_ByUserId(null, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet AssessmentComplete_ByUserId(int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return AssessmentComplete_ByUserId(null, start, pageLength , statusId, userId, proc);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet AssessmentComplete_ByUserId(TransactionManager transactionManager, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return AssessmentComplete_ByUserId(transactionManager, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet AssessmentComplete_ByUserId(TransactionManager transactionManager, int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalViewLatest_ListProcurementContacts
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListProcurementContacts' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListProcurementContacts()
		{
			return ListProcurementContacts(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListProcurementContacts' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListProcurementContacts(int start, int pageLength)
		{
			return ListProcurementContacts(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListProcurementContacts' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListProcurementContacts(TransactionManager transactionManager)
		{
			return ListProcurementContacts(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalViewLatest_ListProcurementContacts' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListProcurementContacts(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt;"/></returns>
		protected static VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt; Fill(DataSet dataSet, VList<QuestionnaireWithLocationApprovalViewLatest> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireWithLocationApprovalViewLatest>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireWithLocationApprovalViewLatest>"/></returns>
		protected static VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt; Fill(DataTable dataTable, VList<QuestionnaireWithLocationApprovalViewLatest> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireWithLocationApprovalViewLatest c = new QuestionnaireWithLocationApprovalViewLatest();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.CreatedByUser = (Convert.IsDBNull(row["CreatedByUser"]))?string.Empty:(System.String)row["CreatedByUser"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedByUser = (Convert.IsDBNull(row["ModifiedByUser"]))?string.Empty:(System.String)row["ModifiedByUser"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.DaysSinceLastActivity = (Convert.IsDBNull(row["DaysSinceLastActivity"]))?(int)0:(System.Int32?)row["DaysSinceLastActivity"];
					c.QmaLastModified = (Convert.IsDBNull(row["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)row["QMALastModified"];
					c.QvaLastModified = (Convert.IsDBNull(row["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)row["QVALastModified"];
					c.AssessedDate = (Convert.IsDBNull(row["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)row["AssessedDate"];
					c.AssessedByUserId = (Convert.IsDBNull(row["AssessedByUserId"]))?(int)0:(System.Int32?)row["AssessedByUserId"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.InitialCreatedByUserId = (Convert.IsDBNull(row["InitialCreatedByUserId"]))?(int)0:(System.Int32?)row["InitialCreatedByUserId"];
					c.InitialCreatedDate = (Convert.IsDBNull(row["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialCreatedDate"];
					c.InitialSubmittedByUserId = (Convert.IsDBNull(row["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)row["InitialSubmittedByUserId"];
					c.InitialSubmittedDate = (Convert.IsDBNull(row["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialSubmittedDate"];
					c.MainCreatedByUserId = (Convert.IsDBNull(row["MainCreatedByUserId"]))?(int)0:(System.Int32?)row["MainCreatedByUserId"];
					c.MainCreatedDate = (Convert.IsDBNull(row["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["MainCreatedDate"];
					c.VerificationCreatedByUserId = (Convert.IsDBNull(row["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)row["VerificationCreatedByUserId"];
					c.VerificationCreatedDate = (Convert.IsDBNull(row["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["VerificationCreatedDate"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.IsMainRequired = (Convert.IsDBNull(row["IsMainRequired"]))?false:(System.Boolean)row["IsMainRequired"];
					c.IsVerificationRequired = (Convert.IsDBNull(row["IsVerificationRequired"]))?false:(System.Boolean)row["IsVerificationRequired"];
					c.InitialCategoryHigh = (Convert.IsDBNull(row["InitialCategoryHigh"]))?false:(System.Boolean?)row["InitialCategoryHigh"];
					c.InitialModifiedByUserId = (Convert.IsDBNull(row["InitialModifiedByUserId"]))?(int)0:(System.Int32?)row["InitialModifiedByUserId"];
					c.InitialModifiedDate = (Convert.IsDBNull(row["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialModifiedDate"];
					c.InitialStatus = (Convert.IsDBNull(row["InitialStatus"]))?(int)0:(System.Int32)row["InitialStatus"];
					c.InitialRiskAssessment = (Convert.IsDBNull(row["InitialRiskAssessment"]))?string.Empty:(System.String)row["InitialRiskAssessment"];
					c.MainModifiedByUserId = (Convert.IsDBNull(row["MainModifiedByUserId"]))?(int)0:(System.Int32?)row["MainModifiedByUserId"];
					c.MainModifiedDate = (Convert.IsDBNull(row["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["MainModifiedDate"];
					c.MainScoreExpectations = (Convert.IsDBNull(row["MainScoreExpectations"]))?(int)0:(System.Int32?)row["MainScoreExpectations"];
					c.MainScoreOverall = (Convert.IsDBNull(row["MainScoreOverall"]))?(int)0:(System.Int32?)row["MainScoreOverall"];
					c.MainScoreResults = (Convert.IsDBNull(row["MainScoreResults"]))?(int)0:(System.Int32?)row["MainScoreResults"];
					c.MainScoreStaffing = (Convert.IsDBNull(row["MainScoreStaffing"]))?(int)0:(System.Int32?)row["MainScoreStaffing"];
					c.MainScoreSystems = (Convert.IsDBNull(row["MainScoreSystems"]))?(int)0:(System.Int32?)row["MainScoreSystems"];
					c.MainStatus = (Convert.IsDBNull(row["MainStatus"]))?(int)0:(System.Int32)row["MainStatus"];
					c.MainAssessmentByUserId = (Convert.IsDBNull(row["MainAssessmentByUserId"]))?(int)0:(System.Int32?)row["MainAssessmentByUserId"];
					c.MainAssessmentComments = (Convert.IsDBNull(row["MainAssessmentComments"]))?string.Empty:(System.String)row["MainAssessmentComments"];
					c.MainAssessmentDate = (Convert.IsDBNull(row["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentDate"];
					c.MainAssessmentRiskRating = (Convert.IsDBNull(row["MainAssessmentRiskRating"]))?string.Empty:(System.String)row["MainAssessmentRiskRating"];
					c.MainAssessmentStatus = (Convert.IsDBNull(row["MainAssessmentStatus"]))?string.Empty:(System.String)row["MainAssessmentStatus"];
					c.MainAssessmentValidTo = (Convert.IsDBNull(row["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentValidTo"];
					c.MainScorePexpectations = (Convert.IsDBNull(row["MainScorePExpectations"]))?(int)0:(System.Int32?)row["MainScorePExpectations"];
					c.MainScorePoverall = (Convert.IsDBNull(row["MainScorePOverall"]))?(int)0:(System.Int32?)row["MainScorePOverall"];
					c.MainScorePresults = (Convert.IsDBNull(row["MainScorePResults"]))?(int)0:(System.Int32?)row["MainScorePResults"];
					c.MainScorePstaffing = (Convert.IsDBNull(row["MainScorePStaffing"]))?(int)0:(System.Int32?)row["MainScorePStaffing"];
					c.MainScorePsystems = (Convert.IsDBNull(row["MainScorePSystems"]))?(int)0:(System.Int32?)row["MainScorePSystems"];
					c.VerificationModifiedByUserId = (Convert.IsDBNull(row["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)row["VerificationModifiedByUserId"];
					c.VerificationModifiedDate = (Convert.IsDBNull(row["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["VerificationModifiedDate"];
					c.VerificationRiskRating = (Convert.IsDBNull(row["VerificationRiskRating"]))?string.Empty:(System.String)row["VerificationRiskRating"];
					c.VerificationStatus = (Convert.IsDBNull(row["VerificationStatus"]))?(int)0:(System.Int32)row["VerificationStatus"];
					c.SupplierContactFirstName = (Convert.IsDBNull(row["SupplierContactFirstName"]))?string.Empty:(System.String)row["SupplierContactFirstName"];
					c.SupplierContactLastName = (Convert.IsDBNull(row["SupplierContactLastName"]))?string.Empty:(System.String)row["SupplierContactLastName"];
					c.SupplierContactEmail = (Convert.IsDBNull(row["SupplierContactEmail"]))?string.Empty:(System.String)row["SupplierContactEmail"];
					c.ProcurementContactUserId = (Convert.IsDBNull(row["ProcurementContactUserId"]))?(int)0:(System.Int32?)row["ProcurementContactUserId"];
					c.ContractManagerUserId = (Convert.IsDBNull(row["ContractManagerUserId"]))?(int)0:(System.Int32?)row["ContractManagerUserId"];
					c.Kwi = (Convert.IsDBNull(row["KWI"]))?string.Empty:(System.String)row["KWI"];
					c.Pin = (Convert.IsDBNull(row["PIN"]))?string.Empty:(System.String)row["PIN"];
					c.Wgp = (Convert.IsDBNull(row["WGP"]))?string.Empty:(System.String)row["WGP"];
					c.Hun = (Convert.IsDBNull(row["HUN"]))?string.Empty:(System.String)row["HUN"];
					c.Wdl = (Convert.IsDBNull(row["WDL"]))?string.Empty:(System.String)row["WDL"];
					c.Bun = (Convert.IsDBNull(row["BUN"]))?string.Empty:(System.String)row["BUN"];
					c.Fml = (Convert.IsDBNull(row["FML"]))?string.Empty:(System.String)row["FML"];
					c.Bgn = (Convert.IsDBNull(row["BGN"]))?string.Empty:(System.String)row["BGN"];
					c.Ang = (Convert.IsDBNull(row["ANG"]))?string.Empty:(System.String)row["ANG"];
					c.Ptl = (Convert.IsDBNull(row["PTL"]))?string.Empty:(System.String)row["PTL"];
					c.Pth = (Convert.IsDBNull(row["PTH"]))?string.Empty:(System.String)row["PTH"];
					c.Pel = (Convert.IsDBNull(row["PEL"]))?string.Empty:(System.String)row["PEL"];
					c.Arp = (Convert.IsDBNull(row["ARP"]))?string.Empty:(System.String)row["ARP"];
					c.Yen = (Convert.IsDBNull(row["YEN"]))?string.Empty:(System.String)row["YEN"];
					c.ProcurementModified = (Convert.IsDBNull(row["ProcurementModified"]))?false:(System.Boolean)row["ProcurementModified"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.SafetyAssessorUserId = (Convert.IsDBNull(row["SafetyAssessorUserId"]))?(int)0:(System.Int32?)row["SafetyAssessorUserId"];
					c.SubContractor = (Convert.IsDBNull(row["SubContractor"]))?false:(System.Boolean?)row["SubContractor"];
					c.CompanyStatusId = (Convert.IsDBNull(row["CompanyStatusId"]))?(int)0:(System.Int32)row["CompanyStatusId"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.Deactivated = (Convert.IsDBNull(row["Deactivated"]))?false:(System.Boolean?)row["Deactivated"];
					c.ProcurementContactUser = (Convert.IsDBNull(row["ProcurementContactUser"]))?string.Empty:(System.String)row["ProcurementContactUser"];
					c.ContractManagerUser = (Convert.IsDBNull(row["ContractManagerUser"]))?string.Empty:(System.String)row["ContractManagerUser"];
					c.NoSqExpiryEmailsSent = (Convert.IsDBNull(row["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)row["NoSqExpiryEmailsSent"];
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(row["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithUserId"];
					c.ProcessNo = (Convert.IsDBNull(row["ProcessNo"]))?(int)0:(System.Int32?)row["ProcessNo"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.ActionName = (Convert.IsDBNull(row["ActionName"]))?string.Empty:(System.String)row["ActionName"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(row["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithSinceDaysCount"];
					c.LevelOfSupervision = (Convert.IsDBNull(row["LevelOfSupervision"]))?string.Empty:(System.String)row["LevelOfSupervision"];
					c.RecommendedComments = (Convert.IsDBNull(row["RecommendedComments"]))?string.Empty:(System.String)row["RecommendedComments"];
					c.IsReQualification = (Convert.IsDBNull(row["IsReQualification"]))?false:(System.Boolean)row["IsReQualification"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.LastReminderEmailSentOn = (Convert.IsDBNull(row["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)row["LastReminderEmailSentOn"];
					c.SupplierContactVerifiedOn = (Convert.IsDBNull(row["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)row["SupplierContactVerifiedOn"];
					c.NoReminderEmailsSent = (Convert.IsDBNull(row["NoReminderEmailsSent"]))?(int)0:(System.Int32?)row["NoReminderEmailsSent"];
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireWithLocationApprovalViewLatest&gt;"/></returns>
		protected VList<QuestionnaireWithLocationApprovalViewLatest> Fill(IDataReader reader, VList<QuestionnaireWithLocationApprovalViewLatest> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireWithLocationApprovalViewLatest entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireWithLocationApprovalViewLatest>("QuestionnaireWithLocationApprovalViewLatest",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireWithLocationApprovalViewLatest();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedByUser)];
					//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedByUser)];
					//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
					entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.DaysSinceLastActivity = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.DaysSinceLastActivity)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.DaysSinceLastActivity)];
					//entity.DaysSinceLastActivity = (Convert.IsDBNull(reader["DaysSinceLastActivity"]))?(int)0:(System.Int32?)reader["DaysSinceLastActivity"];
					entity.QmaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QmaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QmaLastModified)];
					//entity.QmaLastModified = (Convert.IsDBNull(reader["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QMALastModified"];
					entity.QvaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QvaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QvaLastModified)];
					//entity.QvaLastModified = (Convert.IsDBNull(reader["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QVALastModified"];
					entity.AssessedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedDate)];
					//entity.AssessedDate = (Convert.IsDBNull(reader["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)reader["AssessedDate"];
					entity.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedByUserId)];
					//entity.AssessedByUserId = (Convert.IsDBNull(reader["AssessedByUserId"]))?(int)0:(System.Int32?)reader["AssessedByUserId"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedByUserId)];
					//entity.InitialCreatedByUserId = (Convert.IsDBNull(reader["InitialCreatedByUserId"]))?(int)0:(System.Int32?)reader["InitialCreatedByUserId"];
					entity.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedDate)];
					//entity.InitialCreatedDate = (Convert.IsDBNull(reader["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialCreatedDate"];
					entity.InitialSubmittedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedByUserId)];
					//entity.InitialSubmittedByUserId = (Convert.IsDBNull(reader["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)reader["InitialSubmittedByUserId"];
					entity.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedDate)];
					//entity.InitialSubmittedDate = (Convert.IsDBNull(reader["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialSubmittedDate"];
					entity.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedByUserId)];
					//entity.MainCreatedByUserId = (Convert.IsDBNull(reader["MainCreatedByUserId"]))?(int)0:(System.Int32?)reader["MainCreatedByUserId"];
					entity.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedDate)];
					//entity.MainCreatedDate = (Convert.IsDBNull(reader["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainCreatedDate"];
					entity.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedByUserId)];
					//entity.VerificationCreatedByUserId = (Convert.IsDBNull(reader["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)reader["VerificationCreatedByUserId"];
					entity.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedDate)];
					//entity.VerificationCreatedDate = (Convert.IsDBNull(reader["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationCreatedDate"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.IsMainRequired)];
					//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
					entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.IsVerificationRequired)];
					//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
					entity.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCategoryHigh)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCategoryHigh)];
					//entity.InitialCategoryHigh = (Convert.IsDBNull(reader["InitialCategoryHigh"]))?false:(System.Boolean?)reader["InitialCategoryHigh"];
					entity.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedByUserId)];
					//entity.InitialModifiedByUserId = (Convert.IsDBNull(reader["InitialModifiedByUserId"]))?(int)0:(System.Int32?)reader["InitialModifiedByUserId"];
					entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedDate)];
					//entity.InitialModifiedDate = (Convert.IsDBNull(reader["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialModifiedDate"];
					entity.InitialStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialStatus)];
					//entity.InitialStatus = (Convert.IsDBNull(reader["InitialStatus"]))?(int)0:(System.Int32)reader["InitialStatus"];
					entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialRiskAssessment)];
					//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
					entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedByUserId)];
					//entity.MainModifiedByUserId = (Convert.IsDBNull(reader["MainModifiedByUserId"]))?(int)0:(System.Int32?)reader["MainModifiedByUserId"];
					entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedDate)];
					//entity.MainModifiedDate = (Convert.IsDBNull(reader["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainModifiedDate"];
					entity.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreExpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreExpectations)];
					//entity.MainScoreExpectations = (Convert.IsDBNull(reader["MainScoreExpectations"]))?(int)0:(System.Int32?)reader["MainScoreExpectations"];
					entity.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreOverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreOverall)];
					//entity.MainScoreOverall = (Convert.IsDBNull(reader["MainScoreOverall"]))?(int)0:(System.Int32?)reader["MainScoreOverall"];
					entity.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreResults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreResults)];
					//entity.MainScoreResults = (Convert.IsDBNull(reader["MainScoreResults"]))?(int)0:(System.Int32?)reader["MainScoreResults"];
					entity.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreStaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreStaffing)];
					//entity.MainScoreStaffing = (Convert.IsDBNull(reader["MainScoreStaffing"]))?(int)0:(System.Int32?)reader["MainScoreStaffing"];
					entity.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreSystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreSystems)];
					//entity.MainScoreSystems = (Convert.IsDBNull(reader["MainScoreSystems"]))?(int)0:(System.Int32?)reader["MainScoreSystems"];
					entity.MainStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainStatus)];
					//entity.MainStatus = (Convert.IsDBNull(reader["MainStatus"]))?(int)0:(System.Int32)reader["MainStatus"];
					entity.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentByUserId)];
					//entity.MainAssessmentByUserId = (Convert.IsDBNull(reader["MainAssessmentByUserId"]))?(int)0:(System.Int32?)reader["MainAssessmentByUserId"];
					entity.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentComments)];
					//entity.MainAssessmentComments = (Convert.IsDBNull(reader["MainAssessmentComments"]))?string.Empty:(System.String)reader["MainAssessmentComments"];
					entity.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentDate)];
					//entity.MainAssessmentDate = (Convert.IsDBNull(reader["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentDate"];
					entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentRiskRating)];
					//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
					entity.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentStatus)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentStatus)];
					//entity.MainAssessmentStatus = (Convert.IsDBNull(reader["MainAssessmentStatus"]))?string.Empty:(System.String)reader["MainAssessmentStatus"];
					entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentValidTo)];
					//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
					entity.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePexpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePexpectations)];
					//entity.MainScorePexpectations = (Convert.IsDBNull(reader["MainScorePExpectations"]))?(int)0:(System.Int32?)reader["MainScorePExpectations"];
					entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePoverall)];
					//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
					entity.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePresults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePresults)];
					//entity.MainScorePresults = (Convert.IsDBNull(reader["MainScorePResults"]))?(int)0:(System.Int32?)reader["MainScorePResults"];
					entity.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePstaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePstaffing)];
					//entity.MainScorePstaffing = (Convert.IsDBNull(reader["MainScorePStaffing"]))?(int)0:(System.Int32?)reader["MainScorePStaffing"];
					entity.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePsystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePsystems)];
					//entity.MainScorePsystems = (Convert.IsDBNull(reader["MainScorePSystems"]))?(int)0:(System.Int32?)reader["MainScorePSystems"];
					entity.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedByUserId)];
					//entity.VerificationModifiedByUserId = (Convert.IsDBNull(reader["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)reader["VerificationModifiedByUserId"];
					entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedDate)];
					//entity.VerificationModifiedDate = (Convert.IsDBNull(reader["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationModifiedDate"];
					entity.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationRiskRating)];
					//entity.VerificationRiskRating = (Convert.IsDBNull(reader["VerificationRiskRating"]))?string.Empty:(System.String)reader["VerificationRiskRating"];
					entity.VerificationStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationStatus)];
					//entity.VerificationStatus = (Convert.IsDBNull(reader["VerificationStatus"]))?(int)0:(System.Int32)reader["VerificationStatus"];
					entity.SupplierContactFirstName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactFirstName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactFirstName)];
					//entity.SupplierContactFirstName = (Convert.IsDBNull(reader["SupplierContactFirstName"]))?string.Empty:(System.String)reader["SupplierContactFirstName"];
					entity.SupplierContactLastName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactLastName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactLastName)];
					//entity.SupplierContactLastName = (Convert.IsDBNull(reader["SupplierContactLastName"]))?string.Empty:(System.String)reader["SupplierContactLastName"];
					entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactEmail)];
					//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
					entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUserId)];
					//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
					entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUserId)];
					//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?(int)0:(System.Int32?)reader["ContractManagerUserId"];
					entity.Kwi = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Kwi)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Kwi)];
					//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
					entity.Pin = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pin)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pin)];
					//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
					entity.Wgp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wgp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wgp)];
					//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
					entity.Hun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Hun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Hun)];
					//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
					entity.Wdl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wdl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wdl)];
					//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
					entity.Bun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bun)];
					//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
					entity.Fml = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Fml)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Fml)];
					//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
					entity.Bgn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bgn)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bgn)];
					//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
					entity.Ang = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ang)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ang)];
					//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
					entity.Ptl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ptl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ptl)];
					//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
					entity.Pth = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pth)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pth)];
					//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
					entity.Pel = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pel)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pel)];
					//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
					entity.Arp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Arp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Arp)];
					//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
					entity.Yen = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Yen)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Yen)];
					//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
					entity.ProcurementModified = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementModified)];
					//entity.ProcurementModified = (Convert.IsDBNull(reader["ProcurementModified"]))?false:(System.Boolean)reader["ProcurementModified"];
					entity.CompanyName = (System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.SafetyAssessorUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessorUserId)];
					//entity.SafetyAssessorUserId = (Convert.IsDBNull(reader["SafetyAssessorUserId"]))?(int)0:(System.Int32?)reader["SafetyAssessorUserId"];
					entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SubContractor)];
					//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
					entity.CompanyStatusId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyStatusId)];
					//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.Deactivated = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Deactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Deactivated)];
					//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
					entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUser)];
					//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
					entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUser)];
					//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
					entity.NoSqExpiryEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoSqExpiryEmailsSent)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoSqExpiryEmailsSent)];
					//entity.NoSqExpiryEmailsSent = (Convert.IsDBNull(reader["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)reader["NoSqExpiryEmailsSent"];
					entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithUserId)];
					//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
					entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcessNo)];
					//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
					entity.UserName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.ActionName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionName)];
					//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSinceDaysCount)];
					//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
					entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.LevelOfSupervision)];
					//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
					entity.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.RecommendedComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.RecommendedComments)];
					//entity.RecommendedComments = (Convert.IsDBNull(reader["RecommendedComments"]))?string.Empty:(System.String)reader["RecommendedComments"];
					entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.IsReQualification)];
					//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.LastReminderEmailSentOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.LastReminderEmailSentOn)];
					//entity.LastReminderEmailSentOn = (Convert.IsDBNull(reader["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)reader["LastReminderEmailSentOn"];
					entity.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactVerifiedOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactVerifiedOn)];
					//entity.SupplierContactVerifiedOn = (Convert.IsDBNull(reader["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)reader["SupplierContactVerifiedOn"];
					entity.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoReminderEmailsSent)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoReminderEmailsSent)];
					//entity.NoReminderEmailsSent = (Convert.IsDBNull(reader["NoReminderEmailsSent"]))?(int)0:(System.Int32?)reader["NoReminderEmailsSent"];
					entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireWithLocationApprovalViewLatest entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedByUser)];
			//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedByUser)];
			//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.DaysSinceLastActivity = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.DaysSinceLastActivity)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.DaysSinceLastActivity)];
			//entity.DaysSinceLastActivity = (Convert.IsDBNull(reader["DaysSinceLastActivity"]))?(int)0:(System.Int32?)reader["DaysSinceLastActivity"];
			entity.QmaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QmaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QmaLastModified)];
			//entity.QmaLastModified = (Convert.IsDBNull(reader["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QMALastModified"];
			entity.QvaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QvaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QvaLastModified)];
			//entity.QvaLastModified = (Convert.IsDBNull(reader["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QVALastModified"];
			entity.AssessedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedDate)];
			//entity.AssessedDate = (Convert.IsDBNull(reader["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)reader["AssessedDate"];
			entity.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.AssessedByUserId)];
			//entity.AssessedByUserId = (Convert.IsDBNull(reader["AssessedByUserId"]))?(int)0:(System.Int32?)reader["AssessedByUserId"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedByUserId)];
			//entity.InitialCreatedByUserId = (Convert.IsDBNull(reader["InitialCreatedByUserId"]))?(int)0:(System.Int32?)reader["InitialCreatedByUserId"];
			entity.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCreatedDate)];
			//entity.InitialCreatedDate = (Convert.IsDBNull(reader["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialCreatedDate"];
			entity.InitialSubmittedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedByUserId)];
			//entity.InitialSubmittedByUserId = (Convert.IsDBNull(reader["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)reader["InitialSubmittedByUserId"];
			entity.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialSubmittedDate)];
			//entity.InitialSubmittedDate = (Convert.IsDBNull(reader["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialSubmittedDate"];
			entity.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedByUserId)];
			//entity.MainCreatedByUserId = (Convert.IsDBNull(reader["MainCreatedByUserId"]))?(int)0:(System.Int32?)reader["MainCreatedByUserId"];
			entity.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainCreatedDate)];
			//entity.MainCreatedDate = (Convert.IsDBNull(reader["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainCreatedDate"];
			entity.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedByUserId)];
			//entity.VerificationCreatedByUserId = (Convert.IsDBNull(reader["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)reader["VerificationCreatedByUserId"];
			entity.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationCreatedDate)];
			//entity.VerificationCreatedDate = (Convert.IsDBNull(reader["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationCreatedDate"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.IsMainRequired)];
			//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
			entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.IsVerificationRequired)];
			//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
			entity.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCategoryHigh)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialCategoryHigh)];
			//entity.InitialCategoryHigh = (Convert.IsDBNull(reader["InitialCategoryHigh"]))?false:(System.Boolean?)reader["InitialCategoryHigh"];
			entity.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedByUserId)];
			//entity.InitialModifiedByUserId = (Convert.IsDBNull(reader["InitialModifiedByUserId"]))?(int)0:(System.Int32?)reader["InitialModifiedByUserId"];
			entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialModifiedDate)];
			//entity.InitialModifiedDate = (Convert.IsDBNull(reader["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialModifiedDate"];
			entity.InitialStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialStatus)];
			//entity.InitialStatus = (Convert.IsDBNull(reader["InitialStatus"]))?(int)0:(System.Int32)reader["InitialStatus"];
			entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.InitialRiskAssessment)];
			//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
			entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedByUserId)];
			//entity.MainModifiedByUserId = (Convert.IsDBNull(reader["MainModifiedByUserId"]))?(int)0:(System.Int32?)reader["MainModifiedByUserId"];
			entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainModifiedDate)];
			//entity.MainModifiedDate = (Convert.IsDBNull(reader["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainModifiedDate"];
			entity.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreExpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreExpectations)];
			//entity.MainScoreExpectations = (Convert.IsDBNull(reader["MainScoreExpectations"]))?(int)0:(System.Int32?)reader["MainScoreExpectations"];
			entity.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreOverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreOverall)];
			//entity.MainScoreOverall = (Convert.IsDBNull(reader["MainScoreOverall"]))?(int)0:(System.Int32?)reader["MainScoreOverall"];
			entity.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreResults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreResults)];
			//entity.MainScoreResults = (Convert.IsDBNull(reader["MainScoreResults"]))?(int)0:(System.Int32?)reader["MainScoreResults"];
			entity.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreStaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreStaffing)];
			//entity.MainScoreStaffing = (Convert.IsDBNull(reader["MainScoreStaffing"]))?(int)0:(System.Int32?)reader["MainScoreStaffing"];
			entity.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreSystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScoreSystems)];
			//entity.MainScoreSystems = (Convert.IsDBNull(reader["MainScoreSystems"]))?(int)0:(System.Int32?)reader["MainScoreSystems"];
			entity.MainStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainStatus)];
			//entity.MainStatus = (Convert.IsDBNull(reader["MainStatus"]))?(int)0:(System.Int32)reader["MainStatus"];
			entity.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentByUserId)];
			//entity.MainAssessmentByUserId = (Convert.IsDBNull(reader["MainAssessmentByUserId"]))?(int)0:(System.Int32?)reader["MainAssessmentByUserId"];
			entity.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentComments)];
			//entity.MainAssessmentComments = (Convert.IsDBNull(reader["MainAssessmentComments"]))?string.Empty:(System.String)reader["MainAssessmentComments"];
			entity.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentDate)];
			//entity.MainAssessmentDate = (Convert.IsDBNull(reader["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentDate"];
			entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentRiskRating)];
			//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
			entity.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentStatus)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentStatus)];
			//entity.MainAssessmentStatus = (Convert.IsDBNull(reader["MainAssessmentStatus"]))?string.Empty:(System.String)reader["MainAssessmentStatus"];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainAssessmentValidTo)];
			//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
			entity.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePexpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePexpectations)];
			//entity.MainScorePexpectations = (Convert.IsDBNull(reader["MainScorePExpectations"]))?(int)0:(System.Int32?)reader["MainScorePExpectations"];
			entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePoverall)];
			//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
			entity.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePresults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePresults)];
			//entity.MainScorePresults = (Convert.IsDBNull(reader["MainScorePResults"]))?(int)0:(System.Int32?)reader["MainScorePResults"];
			entity.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePstaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePstaffing)];
			//entity.MainScorePstaffing = (Convert.IsDBNull(reader["MainScorePStaffing"]))?(int)0:(System.Int32?)reader["MainScorePStaffing"];
			entity.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePsystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.MainScorePsystems)];
			//entity.MainScorePsystems = (Convert.IsDBNull(reader["MainScorePSystems"]))?(int)0:(System.Int32?)reader["MainScorePSystems"];
			entity.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedByUserId)];
			//entity.VerificationModifiedByUserId = (Convert.IsDBNull(reader["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)reader["VerificationModifiedByUserId"];
			entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationModifiedDate)];
			//entity.VerificationModifiedDate = (Convert.IsDBNull(reader["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationModifiedDate"];
			entity.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationRiskRating)];
			//entity.VerificationRiskRating = (Convert.IsDBNull(reader["VerificationRiskRating"]))?string.Empty:(System.String)reader["VerificationRiskRating"];
			entity.VerificationStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.VerificationStatus)];
			//entity.VerificationStatus = (Convert.IsDBNull(reader["VerificationStatus"]))?(int)0:(System.Int32)reader["VerificationStatus"];
			entity.SupplierContactFirstName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactFirstName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactFirstName)];
			//entity.SupplierContactFirstName = (Convert.IsDBNull(reader["SupplierContactFirstName"]))?string.Empty:(System.String)reader["SupplierContactFirstName"];
			entity.SupplierContactLastName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactLastName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactLastName)];
			//entity.SupplierContactLastName = (Convert.IsDBNull(reader["SupplierContactLastName"]))?string.Empty:(System.String)reader["SupplierContactLastName"];
			entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactEmail)];
			//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
			entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUserId)];
			//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
			entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUserId)];
			//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?(int)0:(System.Int32?)reader["ContractManagerUserId"];
			entity.Kwi = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Kwi)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Kwi)];
			//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
			entity.Pin = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pin)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pin)];
			//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
			entity.Wgp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wgp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wgp)];
			//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
			entity.Hun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Hun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Hun)];
			//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
			entity.Wdl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wdl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Wdl)];
			//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
			entity.Bun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bun)];
			//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
			entity.Fml = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Fml)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Fml)];
			//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
			entity.Bgn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bgn)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Bgn)];
			//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
			entity.Ang = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ang)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ang)];
			//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
			entity.Ptl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ptl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Ptl)];
			//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
			entity.Pth = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pth)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pth)];
			//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
			entity.Pel = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pel)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Pel)];
			//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
			entity.Arp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Arp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Arp)];
			//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
			entity.Yen = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Yen)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Yen)];
			//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
			entity.ProcurementModified = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementModified)];
			//entity.ProcurementModified = (Convert.IsDBNull(reader["ProcurementModified"]))?false:(System.Boolean)reader["ProcurementModified"];
			entity.CompanyName = (System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.SafetyAssessorUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SafetyAssessorUserId)];
			//entity.SafetyAssessorUserId = (Convert.IsDBNull(reader["SafetyAssessorUserId"]))?(int)0:(System.Int32?)reader["SafetyAssessorUserId"];
			entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SubContractor)];
			//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
			entity.CompanyStatusId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyStatusId)];
			//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.Deactivated = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.Deactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.Deactivated)];
			//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
			entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcurementContactUser)];
			//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
			entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ContractManagerUser)];
			//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
			entity.NoSqExpiryEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoSqExpiryEmailsSent)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoSqExpiryEmailsSent)];
			//entity.NoSqExpiryEmailsSent = (Convert.IsDBNull(reader["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)reader["NoSqExpiryEmailsSent"];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithUserId)];
			//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ProcessNo)];
			//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
			entity.UserName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.ActionName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionName)];
			//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.QuestionnairePresentlyWithSinceDaysCount)];
			//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.LevelOfSupervision)];
			//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
			entity.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.RecommendedComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.RecommendedComments)];
			//entity.RecommendedComments = (Convert.IsDBNull(reader["RecommendedComments"]))?string.Empty:(System.String)reader["RecommendedComments"];
			entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.IsReQualification)];
			//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.LastReminderEmailSentOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.LastReminderEmailSentOn)];
			//entity.LastReminderEmailSentOn = (Convert.IsDBNull(reader["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)reader["LastReminderEmailSentOn"];
			entity.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactVerifiedOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.SupplierContactVerifiedOn)];
			//entity.SupplierContactVerifiedOn = (Convert.IsDBNull(reader["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)reader["SupplierContactVerifiedOn"];
			entity.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoReminderEmailsSent)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.NoReminderEmailsSent)];
			//entity.NoReminderEmailsSent = (Convert.IsDBNull(reader["NoReminderEmailsSent"]))?(int)0:(System.Int32?)reader["NoReminderEmailsSent"];
			entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewLatestColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewLatestColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireWithLocationApprovalViewLatest entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedByUser = (Convert.IsDBNull(dataRow["CreatedByUser"]))?string.Empty:(System.String)dataRow["CreatedByUser"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedByUser = (Convert.IsDBNull(dataRow["ModifiedByUser"]))?string.Empty:(System.String)dataRow["ModifiedByUser"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.DaysSinceLastActivity = (Convert.IsDBNull(dataRow["DaysSinceLastActivity"]))?(int)0:(System.Int32?)dataRow["DaysSinceLastActivity"];
			entity.QmaLastModified = (Convert.IsDBNull(dataRow["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)dataRow["QMALastModified"];
			entity.QvaLastModified = (Convert.IsDBNull(dataRow["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)dataRow["QVALastModified"];
			entity.AssessedDate = (Convert.IsDBNull(dataRow["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AssessedDate"];
			entity.AssessedByUserId = (Convert.IsDBNull(dataRow["AssessedByUserId"]))?(int)0:(System.Int32?)dataRow["AssessedByUserId"];
			entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
			entity.InitialCreatedByUserId = (Convert.IsDBNull(dataRow["InitialCreatedByUserId"]))?(int)0:(System.Int32?)dataRow["InitialCreatedByUserId"];
			entity.InitialCreatedDate = (Convert.IsDBNull(dataRow["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialCreatedDate"];
			entity.InitialSubmittedByUserId = (Convert.IsDBNull(dataRow["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)dataRow["InitialSubmittedByUserId"];
			entity.InitialSubmittedDate = (Convert.IsDBNull(dataRow["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialSubmittedDate"];
			entity.MainCreatedByUserId = (Convert.IsDBNull(dataRow["MainCreatedByUserId"]))?(int)0:(System.Int32?)dataRow["MainCreatedByUserId"];
			entity.MainCreatedDate = (Convert.IsDBNull(dataRow["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainCreatedDate"];
			entity.VerificationCreatedByUserId = (Convert.IsDBNull(dataRow["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)dataRow["VerificationCreatedByUserId"];
			entity.VerificationCreatedDate = (Convert.IsDBNull(dataRow["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["VerificationCreatedDate"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.IsMainRequired = (Convert.IsDBNull(dataRow["IsMainRequired"]))?false:(System.Boolean)dataRow["IsMainRequired"];
			entity.IsVerificationRequired = (Convert.IsDBNull(dataRow["IsVerificationRequired"]))?false:(System.Boolean)dataRow["IsVerificationRequired"];
			entity.InitialCategoryHigh = (Convert.IsDBNull(dataRow["InitialCategoryHigh"]))?false:(System.Boolean?)dataRow["InitialCategoryHigh"];
			entity.InitialModifiedByUserId = (Convert.IsDBNull(dataRow["InitialModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["InitialModifiedByUserId"];
			entity.InitialModifiedDate = (Convert.IsDBNull(dataRow["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialModifiedDate"];
			entity.InitialStatus = (Convert.IsDBNull(dataRow["InitialStatus"]))?(int)0:(System.Int32)dataRow["InitialStatus"];
			entity.InitialRiskAssessment = (Convert.IsDBNull(dataRow["InitialRiskAssessment"]))?string.Empty:(System.String)dataRow["InitialRiskAssessment"];
			entity.MainModifiedByUserId = (Convert.IsDBNull(dataRow["MainModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["MainModifiedByUserId"];
			entity.MainModifiedDate = (Convert.IsDBNull(dataRow["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainModifiedDate"];
			entity.MainScoreExpectations = (Convert.IsDBNull(dataRow["MainScoreExpectations"]))?(int)0:(System.Int32?)dataRow["MainScoreExpectations"];
			entity.MainScoreOverall = (Convert.IsDBNull(dataRow["MainScoreOverall"]))?(int)0:(System.Int32?)dataRow["MainScoreOverall"];
			entity.MainScoreResults = (Convert.IsDBNull(dataRow["MainScoreResults"]))?(int)0:(System.Int32?)dataRow["MainScoreResults"];
			entity.MainScoreStaffing = (Convert.IsDBNull(dataRow["MainScoreStaffing"]))?(int)0:(System.Int32?)dataRow["MainScoreStaffing"];
			entity.MainScoreSystems = (Convert.IsDBNull(dataRow["MainScoreSystems"]))?(int)0:(System.Int32?)dataRow["MainScoreSystems"];
			entity.MainStatus = (Convert.IsDBNull(dataRow["MainStatus"]))?(int)0:(System.Int32)dataRow["MainStatus"];
			entity.MainAssessmentByUserId = (Convert.IsDBNull(dataRow["MainAssessmentByUserId"]))?(int)0:(System.Int32?)dataRow["MainAssessmentByUserId"];
			entity.MainAssessmentComments = (Convert.IsDBNull(dataRow["MainAssessmentComments"]))?string.Empty:(System.String)dataRow["MainAssessmentComments"];
			entity.MainAssessmentDate = (Convert.IsDBNull(dataRow["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentDate"];
			entity.MainAssessmentRiskRating = (Convert.IsDBNull(dataRow["MainAssessmentRiskRating"]))?string.Empty:(System.String)dataRow["MainAssessmentRiskRating"];
			entity.MainAssessmentStatus = (Convert.IsDBNull(dataRow["MainAssessmentStatus"]))?string.Empty:(System.String)dataRow["MainAssessmentStatus"];
			entity.MainAssessmentValidTo = (Convert.IsDBNull(dataRow["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentValidTo"];
			entity.MainScorePexpectations = (Convert.IsDBNull(dataRow["MainScorePExpectations"]))?(int)0:(System.Int32?)dataRow["MainScorePExpectations"];
			entity.MainScorePoverall = (Convert.IsDBNull(dataRow["MainScorePOverall"]))?(int)0:(System.Int32?)dataRow["MainScorePOverall"];
			entity.MainScorePresults = (Convert.IsDBNull(dataRow["MainScorePResults"]))?(int)0:(System.Int32?)dataRow["MainScorePResults"];
			entity.MainScorePstaffing = (Convert.IsDBNull(dataRow["MainScorePStaffing"]))?(int)0:(System.Int32?)dataRow["MainScorePStaffing"];
			entity.MainScorePsystems = (Convert.IsDBNull(dataRow["MainScorePSystems"]))?(int)0:(System.Int32?)dataRow["MainScorePSystems"];
			entity.VerificationModifiedByUserId = (Convert.IsDBNull(dataRow["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["VerificationModifiedByUserId"];
			entity.VerificationModifiedDate = (Convert.IsDBNull(dataRow["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["VerificationModifiedDate"];
			entity.VerificationRiskRating = (Convert.IsDBNull(dataRow["VerificationRiskRating"]))?string.Empty:(System.String)dataRow["VerificationRiskRating"];
			entity.VerificationStatus = (Convert.IsDBNull(dataRow["VerificationStatus"]))?(int)0:(System.Int32)dataRow["VerificationStatus"];
			entity.SupplierContactFirstName = (Convert.IsDBNull(dataRow["SupplierContactFirstName"]))?string.Empty:(System.String)dataRow["SupplierContactFirstName"];
			entity.SupplierContactLastName = (Convert.IsDBNull(dataRow["SupplierContactLastName"]))?string.Empty:(System.String)dataRow["SupplierContactLastName"];
			entity.SupplierContactEmail = (Convert.IsDBNull(dataRow["SupplierContactEmail"]))?string.Empty:(System.String)dataRow["SupplierContactEmail"];
			entity.ProcurementContactUserId = (Convert.IsDBNull(dataRow["ProcurementContactUserId"]))?(int)0:(System.Int32?)dataRow["ProcurementContactUserId"];
			entity.ContractManagerUserId = (Convert.IsDBNull(dataRow["ContractManagerUserId"]))?(int)0:(System.Int32?)dataRow["ContractManagerUserId"];
			entity.Kwi = (Convert.IsDBNull(dataRow["KWI"]))?string.Empty:(System.String)dataRow["KWI"];
			entity.Pin = (Convert.IsDBNull(dataRow["PIN"]))?string.Empty:(System.String)dataRow["PIN"];
			entity.Wgp = (Convert.IsDBNull(dataRow["WGP"]))?string.Empty:(System.String)dataRow["WGP"];
			entity.Hun = (Convert.IsDBNull(dataRow["HUN"]))?string.Empty:(System.String)dataRow["HUN"];
			entity.Wdl = (Convert.IsDBNull(dataRow["WDL"]))?string.Empty:(System.String)dataRow["WDL"];
			entity.Bun = (Convert.IsDBNull(dataRow["BUN"]))?string.Empty:(System.String)dataRow["BUN"];
			entity.Fml = (Convert.IsDBNull(dataRow["FML"]))?string.Empty:(System.String)dataRow["FML"];
			entity.Bgn = (Convert.IsDBNull(dataRow["BGN"]))?string.Empty:(System.String)dataRow["BGN"];
			entity.Ang = (Convert.IsDBNull(dataRow["ANG"]))?string.Empty:(System.String)dataRow["ANG"];
			entity.Ptl = (Convert.IsDBNull(dataRow["PTL"]))?string.Empty:(System.String)dataRow["PTL"];
			entity.Pth = (Convert.IsDBNull(dataRow["PTH"]))?string.Empty:(System.String)dataRow["PTH"];
			entity.Pel = (Convert.IsDBNull(dataRow["PEL"]))?string.Empty:(System.String)dataRow["PEL"];
			entity.Arp = (Convert.IsDBNull(dataRow["ARP"]))?string.Empty:(System.String)dataRow["ARP"];
			entity.Yen = (Convert.IsDBNull(dataRow["YEN"]))?string.Empty:(System.String)dataRow["YEN"];
			entity.ProcurementModified = (Convert.IsDBNull(dataRow["ProcurementModified"]))?false:(System.Boolean)dataRow["ProcurementModified"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.SafetyAssessorUserId = (Convert.IsDBNull(dataRow["SafetyAssessorUserId"]))?(int)0:(System.Int32?)dataRow["SafetyAssessorUserId"];
			entity.SubContractor = (Convert.IsDBNull(dataRow["SubContractor"]))?false:(System.Boolean?)dataRow["SubContractor"];
			entity.CompanyStatusId = (Convert.IsDBNull(dataRow["CompanyStatusId"]))?(int)0:(System.Int32)dataRow["CompanyStatusId"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.Deactivated = (Convert.IsDBNull(dataRow["Deactivated"]))?false:(System.Boolean?)dataRow["Deactivated"];
			entity.ProcurementContactUser = (Convert.IsDBNull(dataRow["ProcurementContactUser"]))?string.Empty:(System.String)dataRow["ProcurementContactUser"];
			entity.ContractManagerUser = (Convert.IsDBNull(dataRow["ContractManagerUser"]))?string.Empty:(System.String)dataRow["ContractManagerUser"];
			entity.NoSqExpiryEmailsSent = (Convert.IsDBNull(dataRow["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)dataRow["NoSqExpiryEmailsSent"];
			entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (Convert.IsDBNull(dataRow["ProcessNo"]))?(int)0:(System.Int32?)dataRow["ProcessNo"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.ActionName = (Convert.IsDBNull(dataRow["ActionName"]))?string.Empty:(System.String)dataRow["ActionName"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithSinceDaysCount"];
			entity.LevelOfSupervision = (Convert.IsDBNull(dataRow["LevelOfSupervision"]))?string.Empty:(System.String)dataRow["LevelOfSupervision"];
			entity.RecommendedComments = (Convert.IsDBNull(dataRow["RecommendedComments"]))?string.Empty:(System.String)dataRow["RecommendedComments"];
			entity.IsReQualification = (Convert.IsDBNull(dataRow["IsReQualification"]))?false:(System.Boolean)dataRow["IsReQualification"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.LastReminderEmailSentOn = (Convert.IsDBNull(dataRow["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastReminderEmailSentOn"];
			entity.SupplierContactVerifiedOn = (Convert.IsDBNull(dataRow["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)dataRow["SupplierContactVerifiedOn"];
			entity.NoReminderEmailsSent = (Convert.IsDBNull(dataRow["NoReminderEmailsSent"]))?(int)0:(System.Int32?)dataRow["NoReminderEmailsSent"];
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireWithLocationApprovalViewLatestFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewLatestFilterBuilder : SqlFilterBuilder<QuestionnaireWithLocationApprovalViewLatestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestFilterBuilder class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewLatestFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewLatestFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewLatestFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewLatestFilterBuilder

	#region QuestionnaireWithLocationApprovalViewLatestParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewLatestParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireWithLocationApprovalViewLatestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestParameterBuilder class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewLatestParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewLatestParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewLatestParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewLatestParameterBuilder
	
	#region QuestionnaireWithLocationApprovalViewLatestSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireWithLocationApprovalViewLatestSortBuilder : SqlSortBuilder<QuestionnaireWithLocationApprovalViewLatestColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestSqlSortBuilder class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewLatestSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireWithLocationApprovalViewLatestSortBuilder

} // end namespace
