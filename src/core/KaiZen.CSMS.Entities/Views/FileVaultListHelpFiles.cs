﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'FileVault_List_HelpFiles' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class FileVaultListHelpFiles : FileVaultListHelpFilesBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="FileVaultListHelpFiles"/> instance.
		///</summary>
		public FileVaultListHelpFiles():base(){}	
		
		#endregion
	}
}
