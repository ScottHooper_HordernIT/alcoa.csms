﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit06ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit06ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit06, KaiZen.CSMS.Entities.TwentyOnePointAudit06Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit06Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit06 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit06Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_06 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit06 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_06 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit06 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_06 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit06 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_06 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit06 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_06 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit06 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_06 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit06 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit06&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit06&gt;"/></returns>
		public static TList<TwentyOnePointAudit06> Fill(IDataReader reader, TList<TwentyOnePointAudit06> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit06 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit06")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit06Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit06>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit06",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit06();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit06Column.Id - 1)];
					c.Achieved6a = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6a - 1)];
					c.Achieved6b = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6b - 1)];
					c.Achieved6c = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6c - 1)];
					c.Achieved6d = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6d - 1)];
					c.Achieved6e = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6e - 1)];
					c.Achieved6f = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6f - 1)];
					c.Achieved6g = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6g - 1)];
					c.Achieved6h = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6h - 1)];
					c.Achieved6i = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6i - 1)];
					c.Achieved6j = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6j - 1)];
					c.Achieved6k = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6k - 1)];
					c.Achieved6l = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6l - 1)];
					c.Observation6a = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6a - 1)];
					c.Observation6b = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6b - 1)];
					c.Observation6c = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6c - 1)];
					c.Observation6d = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6d - 1)];
					c.Observation6e = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6e - 1)];
					c.Observation6f = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6f - 1)];
					c.Observation6g = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6g - 1)];
					c.Observation6h = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6h - 1)];
					c.Observation6i = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6i - 1)];
					c.Observation6j = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6j - 1)];
					c.Observation6k = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6k - 1)];
					c.Observation6l = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6l - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit06 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit06Column.Id - 1)];
			entity.Achieved6a = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6a - 1)];
			entity.Achieved6b = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6b - 1)];
			entity.Achieved6c = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6c - 1)];
			entity.Achieved6d = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6d - 1)];
			entity.Achieved6e = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6e - 1)];
			entity.Achieved6f = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6f - 1)];
			entity.Achieved6g = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6g - 1)];
			entity.Achieved6h = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6h - 1)];
			entity.Achieved6i = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6i - 1)];
			entity.Achieved6j = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6j - 1)];
			entity.Achieved6k = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6k - 1)];
			entity.Achieved6l = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Achieved6l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.Achieved6l - 1)];
			entity.Observation6a = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6a - 1)];
			entity.Observation6b = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6b - 1)];
			entity.Observation6c = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6c - 1)];
			entity.Observation6d = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6d - 1)];
			entity.Observation6e = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6e - 1)];
			entity.Observation6f = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6f - 1)];
			entity.Observation6g = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6g - 1)];
			entity.Observation6h = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6h - 1)];
			entity.Observation6i = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6i - 1)];
			entity.Observation6j = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6j - 1)];
			entity.Observation6k = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6k - 1)];
			entity.Observation6l = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.Observation6l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit06Column.Observation6l - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit06Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit06Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit06 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved6a = Convert.IsDBNull(dataRow["Achieved6a"]) ? null : (System.Int32?)dataRow["Achieved6a"];
			entity.Achieved6b = Convert.IsDBNull(dataRow["Achieved6b"]) ? null : (System.Int32?)dataRow["Achieved6b"];
			entity.Achieved6c = Convert.IsDBNull(dataRow["Achieved6c"]) ? null : (System.Int32?)dataRow["Achieved6c"];
			entity.Achieved6d = Convert.IsDBNull(dataRow["Achieved6d"]) ? null : (System.Int32?)dataRow["Achieved6d"];
			entity.Achieved6e = Convert.IsDBNull(dataRow["Achieved6e"]) ? null : (System.Int32?)dataRow["Achieved6e"];
			entity.Achieved6f = Convert.IsDBNull(dataRow["Achieved6f"]) ? null : (System.Int32?)dataRow["Achieved6f"];
			entity.Achieved6g = Convert.IsDBNull(dataRow["Achieved6g"]) ? null : (System.Int32?)dataRow["Achieved6g"];
			entity.Achieved6h = Convert.IsDBNull(dataRow["Achieved6h"]) ? null : (System.Int32?)dataRow["Achieved6h"];
			entity.Achieved6i = Convert.IsDBNull(dataRow["Achieved6i"]) ? null : (System.Int32?)dataRow["Achieved6i"];
			entity.Achieved6j = Convert.IsDBNull(dataRow["Achieved6j"]) ? null : (System.Int32?)dataRow["Achieved6j"];
			entity.Achieved6k = Convert.IsDBNull(dataRow["Achieved6k"]) ? null : (System.Int32?)dataRow["Achieved6k"];
			entity.Achieved6l = Convert.IsDBNull(dataRow["Achieved6l"]) ? null : (System.Int32?)dataRow["Achieved6l"];
			entity.Observation6a = Convert.IsDBNull(dataRow["Observation6a"]) ? null : (System.String)dataRow["Observation6a"];
			entity.Observation6b = Convert.IsDBNull(dataRow["Observation6b"]) ? null : (System.String)dataRow["Observation6b"];
			entity.Observation6c = Convert.IsDBNull(dataRow["Observation6c"]) ? null : (System.String)dataRow["Observation6c"];
			entity.Observation6d = Convert.IsDBNull(dataRow["Observation6d"]) ? null : (System.String)dataRow["Observation6d"];
			entity.Observation6e = Convert.IsDBNull(dataRow["Observation6e"]) ? null : (System.String)dataRow["Observation6e"];
			entity.Observation6f = Convert.IsDBNull(dataRow["Observation6f"]) ? null : (System.String)dataRow["Observation6f"];
			entity.Observation6g = Convert.IsDBNull(dataRow["Observation6g"]) ? null : (System.String)dataRow["Observation6g"];
			entity.Observation6h = Convert.IsDBNull(dataRow["Observation6h"]) ? null : (System.String)dataRow["Observation6h"];
			entity.Observation6i = Convert.IsDBNull(dataRow["Observation6i"]) ? null : (System.String)dataRow["Observation6i"];
			entity.Observation6j = Convert.IsDBNull(dataRow["Observation6j"]) ? null : (System.String)dataRow["Observation6j"];
			entity.Observation6k = Convert.IsDBNull(dataRow["Observation6k"]) ? null : (System.String)dataRow["Observation6k"];
			entity.Observation6l = Convert.IsDBNull(dataRow["Observation6l"]) ? null : (System.String)dataRow["Observation6l"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit06"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit06 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit06 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint06Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit06 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit06 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit06 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit06 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point06IdSource != null)
						{
							child.Point06Id = child.Point06IdSource.Id;
						}
						else
						{
							child.Point06Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit06ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit06</c>
	///</summary>
	public enum TwentyOnePointAudit06ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit06</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit06ChildEntityTypes
	
	#region TwentyOnePointAudit06FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit06Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit06Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit06FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit06FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit06FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit06FilterBuilder
	
	#region TwentyOnePointAudit06ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit06Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit06Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit06ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit06ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit06ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit06ParameterBuilder
	
	#region TwentyOnePointAudit06SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit06Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit06SortBuilder : SqlSortBuilder<TwentyOnePointAudit06Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit06SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit06SortBuilder
	
} // end namespace
