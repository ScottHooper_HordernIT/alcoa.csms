﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>KpiCompanySiteCategoryRepeater</c>
    /// </summary>
	public class KpiCompanySiteCategoryRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiCompanySiteCategoryRepeaterDesigner"/> class.
        /// </summary>
		public KpiCompanySiteCategoryRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is KpiCompanySiteCategoryRepeater))
			{ 
				throw new ArgumentException("Component is not a KpiCompanySiteCategoryRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			KpiCompanySiteCategoryRepeater z = (KpiCompanySiteCategoryRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="KpiCompanySiteCategoryRepeater"/> Type.
    /// </summary>
	[Designer(typeof(KpiCompanySiteCategoryRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:KpiCompanySiteCategoryRepeater runat=\"server\"></{0}:KpiCompanySiteCategoryRepeater>")]
	public class KpiCompanySiteCategoryRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiCompanySiteCategoryRepeater"/> class.
        /// </summary>
		public KpiCompanySiteCategoryRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(KpiCompanySiteCategoryItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(KpiCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.KpiCompanySiteCategory entity = o as KaiZen.CSMS.Entities.KpiCompanySiteCategory;
						KpiCompanySiteCategoryItem container = new KpiCompanySiteCategoryItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class KpiCompanySiteCategoryItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.KpiCompanySiteCategory _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiCompanySiteCategoryItem"/> class.
        /// </summary>
		public KpiCompanySiteCategoryItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:KpiCompanySiteCategoryItem"/> class.
        /// </summary>
		public KpiCompanySiteCategoryItem(KaiZen.CSMS.Entities.KpiCompanySiteCategory entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the KpiId
        /// </summary>
        /// <value>The KpiId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 KpiId
		{
			get { return _entity.KpiId; }
		}
        /// <summary>
        /// Gets the SiteId
        /// </summary>
        /// <value>The SiteId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 SiteId
		{
			get { return _entity.SiteId; }
		}
        /// <summary>
        /// Gets the CreatedbyUserId
        /// </summary>
        /// <value>The CreatedbyUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CreatedbyUserId
		{
			get { return _entity.CreatedbyUserId; }
		}
        /// <summary>
        /// Gets the ModifiedbyUserId
        /// </summary>
        /// <value>The ModifiedbyUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedbyUserId
		{
			get { return _entity.ModifiedbyUserId; }
		}
        /// <summary>
        /// Gets the DateAdded
        /// </summary>
        /// <value>The DateAdded.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateAdded
		{
			get { return _entity.DateAdded; }
		}
        /// <summary>
        /// Gets the Datemodified
        /// </summary>
        /// <value>The Datemodified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime Datemodified
		{
			get { return _entity.Datemodified; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the KpiDateTime
        /// </summary>
        /// <value>The KpiDateTime.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime KpiDateTime
		{
			get { return _entity.KpiDateTime; }
		}
        /// <summary>
        /// Gets the KpiGeneral
        /// </summary>
        /// <value>The KpiGeneral.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal KpiGeneral
		{
			get { return _entity.KpiGeneral; }
		}
        /// <summary>
        /// Gets the KpiCalcinerExpense
        /// </summary>
        /// <value>The KpiCalcinerExpense.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal KpiCalcinerExpense
		{
			get { return _entity.KpiCalcinerExpense; }
		}
        /// <summary>
        /// Gets the KpiCalcinerCapital
        /// </summary>
        /// <value>The KpiCalcinerCapital.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal KpiCalcinerCapital
		{
			get { return _entity.KpiCalcinerCapital; }
		}
        /// <summary>
        /// Gets the KpiResidue
        /// </summary>
        /// <value>The KpiResidue.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal KpiResidue
		{
			get { return _entity.KpiResidue; }
		}
        /// <summary>
        /// Gets the ProjectCapital1Title
        /// </summary>
        /// <value>The ProjectCapital1Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital1Title
		{
			get { return _entity.ProjectCapital1Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital1Hours
        /// </summary>
        /// <value>The ProjectCapital1Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital1Hours
		{
			get { return _entity.ProjectCapital1Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital2Title
        /// </summary>
        /// <value>The ProjectCapital2Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital2Title
		{
			get { return _entity.ProjectCapital2Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital2Hours
        /// </summary>
        /// <value>The ProjectCapital2Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital2Hours
		{
			get { return _entity.ProjectCapital2Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital3Title
        /// </summary>
        /// <value>The ProjectCapital3Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital3Title
		{
			get { return _entity.ProjectCapital3Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital3Hours
        /// </summary>
        /// <value>The ProjectCapital3Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital3Hours
		{
			get { return _entity.ProjectCapital3Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital4Title
        /// </summary>
        /// <value>The ProjectCapital4Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital4Title
		{
			get { return _entity.ProjectCapital4Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital4Hours
        /// </summary>
        /// <value>The ProjectCapital4Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital4Hours
		{
			get { return _entity.ProjectCapital4Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital5Title
        /// </summary>
        /// <value>The ProjectCapital5Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital5Title
		{
			get { return _entity.ProjectCapital5Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital5Hours
        /// </summary>
        /// <value>The ProjectCapital5Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital5Hours
		{
			get { return _entity.ProjectCapital5Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital6Title
        /// </summary>
        /// <value>The ProjectCapital6Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital6Title
		{
			get { return _entity.ProjectCapital6Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital6Hours
        /// </summary>
        /// <value>The ProjectCapital6Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital6Hours
		{
			get { return _entity.ProjectCapital6Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital7Title
        /// </summary>
        /// <value>The ProjectCapital7Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital7Title
		{
			get { return _entity.ProjectCapital7Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital7Hours
        /// </summary>
        /// <value>The ProjectCapital7Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital7Hours
		{
			get { return _entity.ProjectCapital7Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital8Title
        /// </summary>
        /// <value>The ProjectCapital8Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital8Title
		{
			get { return _entity.ProjectCapital8Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital8Hours
        /// </summary>
        /// <value>The ProjectCapital8Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital8Hours
		{
			get { return _entity.ProjectCapital8Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital9Title
        /// </summary>
        /// <value>The ProjectCapital9Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital9Title
		{
			get { return _entity.ProjectCapital9Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital9Hours
        /// </summary>
        /// <value>The ProjectCapital9Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital9Hours
		{
			get { return _entity.ProjectCapital9Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital10Title
        /// </summary>
        /// <value>The ProjectCapital10Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital10Title
		{
			get { return _entity.ProjectCapital10Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital10Hours
        /// </summary>
        /// <value>The ProjectCapital10Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital10Hours
		{
			get { return _entity.ProjectCapital10Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital11Title
        /// </summary>
        /// <value>The ProjectCapital11Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital11Title
		{
			get { return _entity.ProjectCapital11Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital11Hours
        /// </summary>
        /// <value>The ProjectCapital11Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital11Hours
		{
			get { return _entity.ProjectCapital11Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital12Title
        /// </summary>
        /// <value>The ProjectCapital12Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital12Title
		{
			get { return _entity.ProjectCapital12Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital12Hours
        /// </summary>
        /// <value>The ProjectCapital12Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital12Hours
		{
			get { return _entity.ProjectCapital12Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital13Title
        /// </summary>
        /// <value>The ProjectCapital13Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital13Title
		{
			get { return _entity.ProjectCapital13Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital13Hours
        /// </summary>
        /// <value>The ProjectCapital13Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital13Hours
		{
			get { return _entity.ProjectCapital13Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital14Title
        /// </summary>
        /// <value>The ProjectCapital14Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital14Title
		{
			get { return _entity.ProjectCapital14Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital14Hours
        /// </summary>
        /// <value>The ProjectCapital14Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital14Hours
		{
			get { return _entity.ProjectCapital14Hours; }
		}
        /// <summary>
        /// Gets the ProjectCapital15Title
        /// </summary>
        /// <value>The ProjectCapital15Title.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProjectCapital15Title
		{
			get { return _entity.ProjectCapital15Title; }
		}
        /// <summary>
        /// Gets the ProjectCapital15Hours
        /// </summary>
        /// <value>The ProjectCapital15Hours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? ProjectCapital15Hours
		{
			get { return _entity.ProjectCapital15Hours; }
		}
        /// <summary>
        /// Gets the AheaRefineryWork
        /// </summary>
        /// <value>The AheaRefineryWork.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal AheaRefineryWork
		{
			get { return _entity.AheaRefineryWork; }
		}
        /// <summary>
        /// Gets the AheaResidueWork
        /// </summary>
        /// <value>The AheaResidueWork.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal AheaResidueWork
		{
			get { return _entity.AheaResidueWork; }
		}
        /// <summary>
        /// Gets the AheaSmeltingWork
        /// </summary>
        /// <value>The AheaSmeltingWork.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? AheaSmeltingWork
		{
			get { return _entity.AheaSmeltingWork; }
		}
        /// <summary>
        /// Gets the AheaPowerGenerationWork
        /// </summary>
        /// <value>The AheaPowerGenerationWork.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? AheaPowerGenerationWork
		{
			get { return _entity.AheaPowerGenerationWork; }
		}
        /// <summary>
        /// Gets the AheaTotalManHours
        /// </summary>
        /// <value>The AheaTotalManHours.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal AheaTotalManHours
		{
			get { return _entity.AheaTotalManHours; }
		}
        /// <summary>
        /// Gets the AheaPeakNopplSiteWeek
        /// </summary>
        /// <value>The AheaPeakNopplSiteWeek.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AheaPeakNopplSiteWeek
		{
			get { return _entity.AheaPeakNopplSiteWeek; }
		}
        /// <summary>
        /// Gets the AheaAvgNopplSiteMonth
        /// </summary>
        /// <value>The AheaAvgNopplSiteMonth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AheaAvgNopplSiteMonth
		{
			get { return _entity.AheaAvgNopplSiteMonth; }
		}
        /// <summary>
        /// Gets the AheaNoEmpExcessMonth
        /// </summary>
        /// <value>The AheaNoEmpExcessMonth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AheaNoEmpExcessMonth
		{
			get { return _entity.AheaNoEmpExcessMonth; }
		}
        /// <summary>
        /// Gets the IpFati
        /// </summary>
        /// <value>The IpFati.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? IpFati
		{
			get { return _entity.IpFati; }
		}
        /// <summary>
        /// Gets the IpMti
        /// </summary>
        /// <value>The IpMti.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? IpMti
		{
			get { return _entity.IpMti; }
		}
        /// <summary>
        /// Gets the IpRdi
        /// </summary>
        /// <value>The IpRdi.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? IpRdi
		{
			get { return _entity.IpRdi; }
		}
        /// <summary>
        /// Gets the IpLti
        /// </summary>
        /// <value>The IpLti.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? IpLti
		{
			get { return _entity.IpLti; }
		}
        /// <summary>
        /// Gets the IpIfe
        /// </summary>
        /// <value>The IpIfe.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? IpIfe
		{
			get { return _entity.IpIfe; }
		}
        /// <summary>
        /// Gets the EhspNoLwd
        /// </summary>
        /// <value>The EhspNoLwd.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? EhspNoLwd
		{
			get { return _entity.EhspNoLwd; }
		}
        /// <summary>
        /// Gets the EhspNoRd
        /// </summary>
        /// <value>The EhspNoRd.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? EhspNoRd
		{
			get { return _entity.EhspNoRd; }
		}
        /// <summary>
        /// Gets the EhsCorrective
        /// </summary>
        /// <value>The EhsCorrective.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? EhsCorrective
		{
			get { return _entity.EhsCorrective; }
		}
        /// <summary>
        /// Gets the JsaAudits
        /// </summary>
        /// <value>The JsaAudits.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? JsaAudits
		{
			get { return _entity.JsaAudits; }
		}
        /// <summary>
        /// Gets the IWsc
        /// </summary>
        /// <value>The IWsc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? IWsc
		{
			get { return _entity.IWsc; }
		}
        /// <summary>
        /// Gets the ONoHswc
        /// </summary>
        /// <value>The ONoHswc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ONoHswc
		{
			get { return _entity.ONoHswc; }
		}
        /// <summary>
        /// Gets the ONoBsp
        /// </summary>
        /// <value>The ONoBsp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ONoBsp
		{
			get { return _entity.ONoBsp; }
		}
        /// <summary>
        /// Gets the QQas
        /// </summary>
        /// <value>The QQas.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Decimal? QQas
		{
			get { return _entity.QQas; }
		}
        /// <summary>
        /// Gets the QNoNci
        /// </summary>
        /// <value>The QNoNci.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QNoNci
		{
			get { return _entity.QNoNci; }
		}
        /// <summary>
        /// Gets the MTbmpm
        /// </summary>
        /// <value>The MTbmpm.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MTbmpm
		{
			get { return _entity.MTbmpm; }
		}
        /// <summary>
        /// Gets the MAwcm
        /// </summary>
        /// <value>The MAwcm.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MAwcm
		{
			get { return _entity.MAwcm; }
		}
        /// <summary>
        /// Gets the MAmcm
        /// </summary>
        /// <value>The MAmcm.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MAmcm
		{
			get { return _entity.MAmcm; }
		}
        /// <summary>
        /// Gets the MFatality
        /// </summary>
        /// <value>The MFatality.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MFatality
		{
			get { return _entity.MFatality; }
		}
        /// <summary>
        /// Gets the Training
        /// </summary>
        /// <value>The Training.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Training
		{
			get { return _entity.Training; }
		}
        /// <summary>
        /// Gets the MtTolo
        /// </summary>
        /// <value>The MtTolo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtTolo
		{
			get { return _entity.MtTolo; }
		}
        /// <summary>
        /// Gets the MtFp
        /// </summary>
        /// <value>The MtFp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtFp
		{
			get { return _entity.MtFp; }
		}
        /// <summary>
        /// Gets the MtElec
        /// </summary>
        /// <value>The MtElec.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtElec
		{
			get { return _entity.MtElec; }
		}
        /// <summary>
        /// Gets the MtMe
        /// </summary>
        /// <value>The MtMe.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtMe
		{
			get { return _entity.MtMe; }
		}
        /// <summary>
        /// Gets the MtCs
        /// </summary>
        /// <value>The MtCs.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtCs
		{
			get { return _entity.MtCs; }
		}
        /// <summary>
        /// Gets the MtCb
        /// </summary>
        /// <value>The MtCb.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtCb
		{
			get { return _entity.MtCb; }
		}
        /// <summary>
        /// Gets the MtErgo
        /// </summary>
        /// <value>The MtErgo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtErgo
		{
			get { return _entity.MtErgo; }
		}
        /// <summary>
        /// Gets the MtRa
        /// </summary>
        /// <value>The MtRa.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtRa
		{
			get { return _entity.MtRa; }
		}
        /// <summary>
        /// Gets the MtHs
        /// </summary>
        /// <value>The MtHs.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtHs
		{
			get { return _entity.MtHs; }
		}
        /// <summary>
        /// Gets the MtSp
        /// </summary>
        /// <value>The MtSp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtSp
		{
			get { return _entity.MtSp; }
		}
        /// <summary>
        /// Gets the MtIf
        /// </summary>
        /// <value>The MtIf.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtIf
		{
			get { return _entity.MtIf; }
		}
        /// <summary>
        /// Gets the MtHp
        /// </summary>
        /// <value>The MtHp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtHp
		{
			get { return _entity.MtHp; }
		}
        /// <summary>
        /// Gets the MtRp
        /// </summary>
        /// <value>The MtRp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtRp
		{
			get { return _entity.MtRp; }
		}
        /// <summary>
        /// Gets the MtEnginfo81t
        /// </summary>
        /// <value>The MtEnginfo81t.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MtEnginfo81t
		{
			get { return _entity.MtEnginfo81t; }
		}
        /// <summary>
        /// Gets the MtOthers
        /// </summary>
        /// <value>The MtOthers.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MtOthers
		{
			get { return _entity.MtOthers; }
		}
        /// <summary>
        /// Gets the SafetyPlansSubmitted
        /// </summary>
        /// <value>The SafetyPlansSubmitted.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean SafetyPlansSubmitted
		{
			get { return _entity.SafetyPlansSubmitted; }
		}
        /// <summary>
        /// Gets the EbiOnSite
        /// </summary>
        /// <value>The EbiOnSite.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? EbiOnSite
		{
			get { return _entity.EbiOnSite; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryId
        /// </summary>
        /// <value>The CompanySiteCategoryId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CompanySiteCategoryId
		{
			get { return _entity.CompanySiteCategoryId; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryName
        /// </summary>
        /// <value>The CompanySiteCategoryName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanySiteCategoryName
		{
			get { return _entity.CompanySiteCategoryName; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryDesc
        /// </summary>
        /// <value>The CompanySiteCategoryDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanySiteCategoryDesc
		{
			get { return _entity.CompanySiteCategoryDesc; }
		}

	}
}
