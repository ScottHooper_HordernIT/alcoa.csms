﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersAuditService
    {
        UsersAudit Get(Expression<Func<UsersAudit, bool>> where, List<Expression<Func<UsersAudit, object>>> includes);
        List<UsersAudit> GetMany(int? top, Expression<Func<UsersAudit, bool>> where, Func<IQueryable<UsersAudit>, IOrderedQueryable<UsersAudit>> orderBy, List<Expression<Func<UsersAudit, object>>> includes);
        List<UsersAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersAudit, bool>> where, Func<IQueryable<UsersAudit>, IOrderedQueryable<UsersAudit>> orderBy, List<Expression<Func<UsersAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsersAudit, bool>> where, List<Expression<Func<UsersAudit, object>>> includes);
        void Insert(UsersAudit item, bool saveChanges = true);
        void Update(UsersAudit item, bool saveChanges = true);
        void Delete(UsersAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersAuditService : IUsersAuditService
    {
        private readonly IRepository<UsersAudit> _repository;

        public UsersAuditService(IRepository<UsersAudit> repository)
        {
            this._repository = repository;
        }

        public UsersAudit Get(Expression<Func<UsersAudit, bool>> where, List<Expression<Func<UsersAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersAudit> GetMany(int? top, Expression<Func<UsersAudit, bool>> where, Func<IQueryable<UsersAudit>, IOrderedQueryable<UsersAudit>> orderBy, List<Expression<Func<UsersAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersAudit, bool>> where, Func<IQueryable<UsersAudit>, IOrderedQueryable<UsersAudit>> orderBy, List<Expression<Func<UsersAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersAudit, bool>> where, List<Expression<Func<UsersAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
