﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireVerificationResponseAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireVerificationResponseAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByQuestionnaireIdSectionIdQuestionId(System.Int32? _questionnaireId, System.Int32? _sectionId, System.Int32? _questionId)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(null,_questionnaireId, _sectionId, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByQuestionnaireIdSectionIdQuestionId(System.Int32? _questionnaireId, System.Int32? _sectionId, System.Int32? _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(null, _questionnaireId, _sectionId, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByQuestionnaireIdSectionIdQuestionId(TransactionManager transactionManager, System.Int32? _questionnaireId, System.Int32? _sectionId, System.Int32? _questionId)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(transactionManager, _questionnaireId, _sectionId, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByQuestionnaireIdSectionIdQuestionId(TransactionManager transactionManager, System.Int32? _questionnaireId, System.Int32? _sectionId, System.Int32? _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdSectionIdQuestionId(transactionManager, _questionnaireId, _sectionId, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByQuestionnaireIdSectionIdQuestionId(System.Int32? _questionnaireId, System.Int32? _sectionId, System.Int32? _questionId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdSectionIdQuestionId(null, _questionnaireId, _sectionId, _questionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationResponseAudit> GetByQuestionnaireIdSectionIdQuestionId(TransactionManager transactionManager, System.Int32? _questionnaireId, System.Int32? _sectionId, System.Int32? _questionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationResponseAudit_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByModifiedByUserId(System.Int32? _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(null,_modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByModifiedByUserId(System.Int32? _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByModifiedByUserId(System.Int32? _modifiedByUserId, int start, int pageLength, out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationResponseAudit> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationResponseAudit_2 index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByResponseId(System.Int32? _responseId)
		{
			int count = -1;
			return GetByResponseId(null,_responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_2 index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByResponseId(System.Int32? _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(null, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByResponseId(TransactionManager transactionManager, System.Int32? _responseId)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByResponseId(TransactionManager transactionManager, System.Int32? _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_2 index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public TList<QuestionnaireVerificationResponseAudit> GetByResponseId(System.Int32? _responseId, int start, int pageLength, out int count)
		{
			return GetByResponseId(null, _responseId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationResponseAudit_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/> class.</returns>
		public abstract TList<QuestionnaireVerificationResponseAudit> GetByResponseId(TransactionManager transactionManager, System.Int32? _responseId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireVerificationResponseAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireVerificationResponseAudit&gt;"/></returns>
		public static TList<QuestionnaireVerificationResponseAudit> Fill(IDataReader reader, TList<QuestionnaireVerificationResponseAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireVerificationResponseAudit")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireVerificationResponseAudit>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireVerificationResponseAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditEventId - 1)];
					c.ResponseId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.ResponseId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.ResponseId - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.QuestionnaireId - 1)];
					c.SectionId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.SectionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.SectionId - 1)];
					c.QuestionId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.QuestionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.QuestionId - 1)];
					c.AnswerText = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.AnswerText - 1)))?null:(System.String)reader[((int)QuestionnaireVerificationResponseAuditColumn.AnswerText - 1)];
					c.AdditionalEvidence = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.AdditionalEvidence - 1)))?null:(System.Byte[])reader[((int)QuestionnaireVerificationResponseAuditColumn.AdditionalEvidence - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireVerificationResponseAuditColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)QuestionnaireVerificationResponseAuditColumn.AuditEventId - 1)];
			entity.ResponseId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.ResponseId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.ResponseId - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.QuestionnaireId - 1)];
			entity.SectionId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.SectionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.SectionId - 1)];
			entity.QuestionId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.QuestionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.QuestionId - 1)];
			entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.AnswerText - 1)))?null:(System.String)reader[((int)QuestionnaireVerificationResponseAuditColumn.AnswerText - 1)];
			entity.AdditionalEvidence = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.AdditionalEvidence - 1)))?null:(System.Byte[])reader[((int)QuestionnaireVerificationResponseAuditColumn.AdditionalEvidence - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationResponseAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireVerificationResponseAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireVerificationResponseAuditColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.ResponseId = Convert.IsDBNull(dataRow["ResponseId"]) ? null : (System.Int32?)dataRow["ResponseId"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.Int32?)dataRow["QuestionnaireId"];
			entity.SectionId = Convert.IsDBNull(dataRow["SectionId"]) ? null : (System.Int32?)dataRow["SectionId"];
			entity.QuestionId = Convert.IsDBNull(dataRow["QuestionId"]) ? null : (System.Int32?)dataRow["QuestionId"];
			entity.AnswerText = Convert.IsDBNull(dataRow["AnswerText"]) ? null : (System.String)dataRow["AnswerText"];
			entity.AdditionalEvidence = Convert.IsDBNull(dataRow["AdditionalEvidence"]) ? null : (System.Byte[])dataRow["AdditionalEvidence"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireVerificationResponseAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireVerificationResponseAudit</c>
	///</summary>
	public enum QuestionnaireVerificationResponseAuditChildEntityTypes
	{
	}
	
	#endregion QuestionnaireVerificationResponseAuditChildEntityTypes
	
	#region QuestionnaireVerificationResponseAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireVerificationResponseAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditFilterBuilder : SqlFilterBuilder<QuestionnaireVerificationResponseAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditFilterBuilder class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationResponseAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationResponseAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationResponseAuditFilterBuilder
	
	#region QuestionnaireVerificationResponseAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireVerificationResponseAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireVerificationResponseAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditParameterBuilder class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationResponseAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationResponseAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationResponseAuditParameterBuilder
	
	#region QuestionnaireVerificationResponseAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireVerificationResponseAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireVerificationResponseAuditSortBuilder : SqlSortBuilder<QuestionnaireVerificationResponseAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditSqlSortBuilder class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireVerificationResponseAuditSortBuilder
	
} // end namespace
