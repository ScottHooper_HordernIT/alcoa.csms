﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'FileVaultCategory' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table FileVaultCategory</remark>
	[Serializable, Flags]
	public enum FileVaultCategoryList
	{
		/// <summary> 
		/// Help Files
		/// </summary>
		[EnumTextValue(@"Help Files")]
		HelpFiles = 1, 

		/// <summary> 
		/// Safety Qualification Exemption Form
		/// </summary>
		[EnumTextValue(@"Safety Qualification Exemption Form")]
		SqExemption = 2

	}
}
