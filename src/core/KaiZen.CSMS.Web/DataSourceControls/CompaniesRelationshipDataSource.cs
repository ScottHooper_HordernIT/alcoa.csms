﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesRelationshipProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompaniesRelationshipDataSourceDesigner))]
	public class CompaniesRelationshipDataSource : ProviderDataSource<CompaniesRelationship, CompaniesRelationshipKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipDataSource class.
		/// </summary>
		public CompaniesRelationshipDataSource() : base(new CompaniesRelationshipService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesRelationshipDataSourceView used by the CompaniesRelationshipDataSource.
		/// </summary>
		protected CompaniesRelationshipDataSourceView CompaniesRelationshipView
		{
			get { return ( View as CompaniesRelationshipDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompaniesRelationshipDataSource control invokes to retrieve data.
		/// </summary>
		public CompaniesRelationshipSelectMethod SelectMethod
		{
			get
			{
				CompaniesRelationshipSelectMethod selectMethod = CompaniesRelationshipSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompaniesRelationshipSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesRelationshipDataSourceView class that is to be
		/// used by the CompaniesRelationshipDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesRelationshipDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesRelationship, CompaniesRelationshipKey> GetNewDataSourceView()
		{
			return new CompaniesRelationshipDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesRelationshipDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesRelationshipDataSourceView : ProviderDataSourceView<CompaniesRelationship, CompaniesRelationshipKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesRelationshipDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesRelationshipDataSourceView(CompaniesRelationshipDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesRelationshipDataSource CompaniesRelationshipOwner
		{
			get { return Owner as CompaniesRelationshipDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompaniesRelationshipSelectMethod SelectMethod
		{
			get { return CompaniesRelationshipOwner.SelectMethod; }
			set { CompaniesRelationshipOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesRelationshipService CompaniesRelationshipProvider
		{
			get { return Provider as CompaniesRelationshipService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompaniesRelationship> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompaniesRelationship> results = null;
			CompaniesRelationship item;
			count = 0;
			
			System.Int32 _parentId;
			System.Int32 _childId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case CompaniesRelationshipSelectMethod.Get:
					CompaniesRelationshipKey entityKey  = new CompaniesRelationshipKey();
					entityKey.Load(values);
					item = CompaniesRelationshipProvider.Get(entityKey);
					results = new TList<CompaniesRelationship>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesRelationshipSelectMethod.GetAll:
                    results = CompaniesRelationshipProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompaniesRelationshipSelectMethod.GetPaged:
					results = CompaniesRelationshipProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompaniesRelationshipSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompaniesRelationshipProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompaniesRelationshipProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompaniesRelationshipSelectMethod.GetByParentIdChildId:
					_parentId = ( values["ParentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ParentId"], typeof(System.Int32)) : (int)0;
					_childId = ( values["ChildId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ChildId"], typeof(System.Int32)) : (int)0;
					item = CompaniesRelationshipProvider.GetByParentIdChildId(_parentId, _childId);
					results = new TList<CompaniesRelationship>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case CompaniesRelationshipSelectMethod.GetByChildId:
					_childId = ( values["ChildId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ChildId"], typeof(System.Int32)) : (int)0;
					results = CompaniesRelationshipProvider.GetByChildId(_childId, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesRelationshipSelectMethod.GetByParentId:
					_parentId = ( values["ParentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ParentId"], typeof(System.Int32)) : (int)0;
					results = CompaniesRelationshipProvider.GetByParentId(_parentId, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesRelationshipSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = CompaniesRelationshipProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompaniesRelationshipSelectMethod.Get || SelectMethod == CompaniesRelationshipSelectMethod.GetByParentIdChildId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompaniesRelationship entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompaniesRelationshipProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompaniesRelationship> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompaniesRelationshipProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompaniesRelationshipDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesRelationshipDataSource class.
	/// </summary>
	public class CompaniesRelationshipDataSourceDesigner : ProviderDataSourceDesigner<CompaniesRelationship, CompaniesRelationshipKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipDataSourceDesigner class.
		/// </summary>
		public CompaniesRelationshipDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesRelationshipSelectMethod SelectMethod
		{
			get { return ((CompaniesRelationshipDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompaniesRelationshipDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompaniesRelationshipDataSourceActionList

	/// <summary>
	/// Supports the CompaniesRelationshipDataSourceDesigner class.
	/// </summary>
	internal class CompaniesRelationshipDataSourceActionList : DesignerActionList
	{
		private CompaniesRelationshipDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompaniesRelationshipDataSourceActionList(CompaniesRelationshipDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesRelationshipSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompaniesRelationshipDataSourceActionList
	
	#endregion CompaniesRelationshipDataSourceDesigner
	
	#region CompaniesRelationshipSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompaniesRelationshipDataSource.SelectMethod property.
	/// </summary>
	public enum CompaniesRelationshipSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByParentIdChildId method.
		/// </summary>
		GetByParentIdChildId,
		/// <summary>
		/// Represents the GetByChildId method.
		/// </summary>
		GetByChildId,
		/// <summary>
		/// Represents the GetByParentId method.
		/// </summary>
		GetByParentId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion CompaniesRelationshipSelectMethod

	#region CompaniesRelationshipFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipFilter : SqlFilter<CompaniesRelationshipColumn>
	{
	}
	
	#endregion CompaniesRelationshipFilter

	#region CompaniesRelationshipExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipExpressionBuilder : SqlExpressionBuilder<CompaniesRelationshipColumn>
	{
	}
	
	#endregion CompaniesRelationshipExpressionBuilder	

	#region CompaniesRelationshipProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompaniesRelationshipChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipProperty : ChildEntityProperty<CompaniesRelationshipChildEntityTypes>
	{
	}
	
	#endregion CompaniesRelationshipProperty
}

