﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'CompanySiteCategoryException2' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CompanySiteCategoryException2 : CompanySiteCategoryException2Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CompanySiteCategoryException2"/> instance.
		///</summary>
		public CompanySiteCategoryException2():base(){}	
		
		#endregion
	}
}
