﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ConfigSa812SitesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class ConfigSa812SitesProviderBaseCore : EntityViewProviderBase<ConfigSa812Sites>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;ConfigSa812Sites&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;ConfigSa812Sites&gt;"/></returns>
		protected static VList&lt;ConfigSa812Sites&gt; Fill(DataSet dataSet, VList<ConfigSa812Sites> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<ConfigSa812Sites>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;ConfigSa812Sites&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<ConfigSa812Sites>"/></returns>
		protected static VList&lt;ConfigSa812Sites&gt; Fill(DataTable dataTable, VList<ConfigSa812Sites> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					ConfigSa812Sites c = new ConfigSa812Sites();
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.IsVisible = (Convert.IsDBNull(row["IsVisible"]))?false:(System.Boolean)row["IsVisible"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;ConfigSa812Sites&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;ConfigSa812Sites&gt;"/></returns>
		protected VList<ConfigSa812Sites> Fill(IDataReader reader, VList<ConfigSa812Sites> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					ConfigSa812Sites entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<ConfigSa812Sites>("ConfigSa812Sites",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new ConfigSa812Sites();
					}
					
					entity.SuppressEntityEvents = true;

					entity.SiteId = (System.Int32)reader[((int)ConfigSa812SitesColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.SiteName = (System.String)reader[((int)ConfigSa812SitesColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.IsVisible = (System.Boolean)reader[((int)ConfigSa812SitesColumn.IsVisible)];
					//entity.IsVisible = (Convert.IsDBNull(reader["IsVisible"]))?false:(System.Boolean)reader["IsVisible"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="ConfigSa812Sites"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ConfigSa812Sites"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, ConfigSa812Sites entity)
		{
			reader.Read();
			entity.SiteId = (System.Int32)reader[((int)ConfigSa812SitesColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.SiteName = (System.String)reader[((int)ConfigSa812SitesColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.IsVisible = (System.Boolean)reader[((int)ConfigSa812SitesColumn.IsVisible)];
			//entity.IsVisible = (Convert.IsDBNull(reader["IsVisible"]))?false:(System.Boolean)reader["IsVisible"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="ConfigSa812Sites"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ConfigSa812Sites"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, ConfigSa812Sites entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.IsVisible = (Convert.IsDBNull(dataRow["IsVisible"]))?false:(System.Boolean)dataRow["IsVisible"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region ConfigSa812SitesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812SitesFilterBuilder : SqlFilterBuilder<ConfigSa812SitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesFilterBuilder class.
		/// </summary>
		public ConfigSa812SitesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812SitesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812SitesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812SitesFilterBuilder

	#region ConfigSa812SitesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812SitesParameterBuilder : ParameterizedSqlFilterBuilder<ConfigSa812SitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesParameterBuilder class.
		/// </summary>
		public ConfigSa812SitesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812SitesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812SitesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812SitesParameterBuilder
	
	#region ConfigSa812SitesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ConfigSa812SitesSortBuilder : SqlSortBuilder<ConfigSa812SitesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesSqlSortBuilder class.
		/// </summary>
		public ConfigSa812SitesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ConfigSa812SitesSortBuilder

} // end namespace
