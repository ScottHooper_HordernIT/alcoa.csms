﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SafetyPlansSeAnswersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SafetyPlansSeAnswersProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.SafetyPlansSeAnswers, KaiZen.CSMS.Entities.SafetyPlansSeAnswersKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeAnswersKey key)
		{
			return Delete(transactionManager, key.AnswerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_answerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _answerId)
		{
			return Delete(null, _answerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _answerId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions Description: 
		/// </summary>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByQuestionId(System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(_questionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		/// <remarks></remarks>
		public TList<SafetyPlansSeAnswers> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionId(transactionManager, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions key.
		///		fkSafetyPlansSeAnswersSafetyPlansSeQuestions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByQuestionId(System.Int32 _questionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionId(null, _questionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions key.
		///		fkSafetyPlansSeAnswersSafetyPlansSeQuestions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByQuestionId(System.Int32 _questionId, int start, int pageLength,out int count)
		{
			return GetByQuestionId(null, _questionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Questions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public abstract TList<SafetyPlansSeAnswers> GetByQuestionId(TransactionManager transactionManager, System.Int32 _questionId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses Description: 
		/// </summary>
		/// <param name="_responseId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByResponseId(System.Int32 _responseId)
		{
			int count = -1;
			return GetByResponseId(_responseId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		/// <remarks></remarks>
		public TList<SafetyPlansSeAnswers> GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses key.
		///		fkSafetyPlansSeAnswersSafetyPlansSeResponses Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_responseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByResponseId(System.Int32 _responseId, int start, int pageLength)
		{
			int count =  -1;
			return GetByResponseId(null, _responseId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses key.
		///		fkSafetyPlansSeAnswersSafetyPlansSeResponses Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_responseId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public TList<SafetyPlansSeAnswers> GetByResponseId(System.Int32 _responseId, int start, int pageLength,out int count)
		{
			return GetByResponseId(null, _responseId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses key.
		///		FK_SafetyPlans_SE_Answers_SafetyPlans_SE_Responses Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSeAnswers objects.</returns>
		public abstract TList<SafetyPlansSeAnswers> GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.SafetyPlansSeAnswers Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeAnswersKey key, int start, int pageLength)
		{
			return GetByAnswerId(transactionManager, key.AnswerId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByAnswerId(System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(null,_answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByAnswerId(System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(null, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByAnswerId(System.Int32 _answerId, int start, int pageLength, out int count)
		{
			return GetByAnswerId(null, _answerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="_responseId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByQuestionIdResponseId(System.Int32 _questionId, System.Int32 _responseId)
		{
			int count = -1;
			return GetByQuestionIdResponseId(null,_questionId, _responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByQuestionIdResponseId(System.Int32 _questionId, System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionIdResponseId(null, _questionId, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="_responseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByQuestionIdResponseId(TransactionManager transactionManager, System.Int32 _questionId, System.Int32 _responseId)
		{
			int count = -1;
			return GetByQuestionIdResponseId(transactionManager, _questionId, _responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByQuestionIdResponseId(TransactionManager transactionManager, System.Int32 _questionId, System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionIdResponseId(transactionManager, _questionId, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="_questionId"></param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByQuestionIdResponseId(System.Int32 _questionId, System.Int32 _responseId, int start, int pageLength, out int count)
		{
			return GetByQuestionIdResponseId(null, _questionId, _responseId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlans_SE_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionId"></param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SafetyPlansSeAnswers GetByQuestionIdResponseId(TransactionManager transactionManager, System.Int32 _questionId, System.Int32 _responseId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _SafetyPlans_SE_Answers_Printable_GetByResponseId 
		
		/// <summary>
		///	This method wrap the '_SafetyPlans_SE_Answers_Printable_GetByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Printable_GetByResponseId(System.Int32? responseId)
		{
			return Printable_GetByResponseId(null, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_SafetyPlans_SE_Answers_Printable_GetByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Printable_GetByResponseId(int start, int pageLength, System.Int32? responseId)
		{
			return Printable_GetByResponseId(null, start, pageLength , responseId);
		}
				
		/// <summary>
		///	This method wrap the '_SafetyPlans_SE_Answers_Printable_GetByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Printable_GetByResponseId(TransactionManager transactionManager, System.Int32? responseId)
		{
			return Printable_GetByResponseId(transactionManager, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_SafetyPlans_SE_Answers_Printable_GetByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Printable_GetByResponseId(TransactionManager transactionManager, int start, int pageLength , System.Int32? responseId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SafetyPlansSeAnswers&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SafetyPlansSeAnswers&gt;"/></returns>
		public static TList<SafetyPlansSeAnswers> Fill(IDataReader reader, TList<SafetyPlansSeAnswers> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.SafetyPlansSeAnswers c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SafetyPlansSeAnswers")
					.Append("|").Append((System.Int32)reader[((int)SafetyPlansSeAnswersColumn.AnswerId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SafetyPlansSeAnswers>(
					key.ToString(), // EntityTrackingKey
					"SafetyPlansSeAnswers",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.SafetyPlansSeAnswers();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AnswerId = (System.Int32)reader[((int)SafetyPlansSeAnswersColumn.AnswerId - 1)];
					c.ResponseId = (System.Int32)reader[((int)SafetyPlansSeAnswersColumn.ResponseId - 1)];
					c.QuestionId = (System.Int32)reader[((int)SafetyPlansSeAnswersColumn.QuestionId - 1)];
					c.Answer = (System.Boolean)reader[((int)SafetyPlansSeAnswersColumn.Answer - 1)];
					c.Comment = (reader.IsDBNull(((int)SafetyPlansSeAnswersColumn.Comment - 1)))?null:(System.Byte[])reader[((int)SafetyPlansSeAnswersColumn.Comment - 1)];
					c.AssessorApproval = (reader.IsDBNull(((int)SafetyPlansSeAnswersColumn.AssessorApproval - 1)))?null:(System.Boolean?)reader[((int)SafetyPlansSeAnswersColumn.AssessorApproval - 1)];
					c.AssessorComment = (reader.IsDBNull(((int)SafetyPlansSeAnswersColumn.AssessorComment - 1)))?null:(System.Byte[])reader[((int)SafetyPlansSeAnswersColumn.AssessorComment - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.SafetyPlansSeAnswers entity)
		{
			if (!reader.Read()) return;
			
			entity.AnswerId = (System.Int32)reader[((int)SafetyPlansSeAnswersColumn.AnswerId - 1)];
			entity.ResponseId = (System.Int32)reader[((int)SafetyPlansSeAnswersColumn.ResponseId - 1)];
			entity.QuestionId = (System.Int32)reader[((int)SafetyPlansSeAnswersColumn.QuestionId - 1)];
			entity.Answer = (System.Boolean)reader[((int)SafetyPlansSeAnswersColumn.Answer - 1)];
			entity.Comment = (reader.IsDBNull(((int)SafetyPlansSeAnswersColumn.Comment - 1)))?null:(System.Byte[])reader[((int)SafetyPlansSeAnswersColumn.Comment - 1)];
			entity.AssessorApproval = (reader.IsDBNull(((int)SafetyPlansSeAnswersColumn.AssessorApproval - 1)))?null:(System.Boolean?)reader[((int)SafetyPlansSeAnswersColumn.AssessorApproval - 1)];
			entity.AssessorComment = (reader.IsDBNull(((int)SafetyPlansSeAnswersColumn.AssessorComment - 1)))?null:(System.Byte[])reader[((int)SafetyPlansSeAnswersColumn.AssessorComment - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.SafetyPlansSeAnswers entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerId = (System.Int32)dataRow["AnswerId"];
			entity.ResponseId = (System.Int32)dataRow["ResponseId"];
			entity.QuestionId = (System.Int32)dataRow["QuestionId"];
			entity.Answer = (System.Boolean)dataRow["Answer"];
			entity.Comment = Convert.IsDBNull(dataRow["Comment"]) ? null : (System.Byte[])dataRow["Comment"];
			entity.AssessorApproval = Convert.IsDBNull(dataRow["AssessorApproval"]) ? null : (System.Boolean?)dataRow["AssessorApproval"];
			entity.AssessorComment = Convert.IsDBNull(dataRow["AssessorComment"]) ? null : (System.Byte[])dataRow["AssessorComment"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSeAnswers"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSeAnswers Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeAnswers entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionIdSource	
			if (CanDeepLoad(entity, "SafetyPlansSeQuestions|QuestionIdSource", deepLoadType, innerList) 
				&& entity.QuestionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionId;
				SafetyPlansSeQuestions tmpEntity = EntityManager.LocateEntity<SafetyPlansSeQuestions>(EntityLocator.ConstructKeyFromPkItems(typeof(SafetyPlansSeQuestions), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionIdSource = tmpEntity;
				else
					entity.QuestionIdSource = DataRepository.SafetyPlansSeQuestionsProvider.GetByQuestionId(transactionManager, entity.QuestionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SafetyPlansSeQuestionsProvider.DeepLoad(transactionManager, entity.QuestionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionIdSource

			#region ResponseIdSource	
			if (CanDeepLoad(entity, "SafetyPlansSeResponses|ResponseIdSource", deepLoadType, innerList) 
				&& entity.ResponseIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ResponseId;
				SafetyPlansSeResponses tmpEntity = EntityManager.LocateEntity<SafetyPlansSeResponses>(EntityLocator.ConstructKeyFromPkItems(typeof(SafetyPlansSeResponses), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ResponseIdSource = tmpEntity;
				else
					entity.ResponseIdSource = DataRepository.SafetyPlansSeResponsesProvider.GetByResponseId(transactionManager, entity.ResponseId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ResponseIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ResponseIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SafetyPlansSeResponsesProvider.DeepLoad(transactionManager, entity.ResponseIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ResponseIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SafetyPlansSeAnswers object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.SafetyPlansSeAnswers instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSeAnswers Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlansSeAnswers entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionIdSource
			if (CanDeepSave(entity, "SafetyPlansSeQuestions|QuestionIdSource", deepSaveType, innerList) 
				&& entity.QuestionIdSource != null)
			{
				DataRepository.SafetyPlansSeQuestionsProvider.Save(transactionManager, entity.QuestionIdSource);
				entity.QuestionId = entity.QuestionIdSource.QuestionId;
			}
			#endregion 
			
			#region ResponseIdSource
			if (CanDeepSave(entity, "SafetyPlansSeResponses|ResponseIdSource", deepSaveType, innerList) 
				&& entity.ResponseIdSource != null)
			{
				DataRepository.SafetyPlansSeResponsesProvider.Save(transactionManager, entity.ResponseIdSource);
				entity.ResponseId = entity.ResponseIdSource.ResponseId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SafetyPlansSeAnswersChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.SafetyPlansSeAnswers</c>
	///</summary>
	public enum SafetyPlansSeAnswersChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>SafetyPlansSeQuestions</c> at QuestionIdSource
		///</summary>
		[ChildEntityType(typeof(SafetyPlansSeQuestions))]
		SafetyPlansSeQuestions,
			
		///<summary>
		/// Composite Property for <c>SafetyPlansSeResponses</c> at ResponseIdSource
		///</summary>
		[ChildEntityType(typeof(SafetyPlansSeResponses))]
		SafetyPlansSeResponses,
		}
	
	#endregion SafetyPlansSeAnswersChildEntityTypes
	
	#region SafetyPlansSeAnswersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SafetyPlansSeAnswersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersFilterBuilder : SqlFilterBuilder<SafetyPlansSeAnswersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersFilterBuilder class.
		/// </summary>
		public SafetyPlansSeAnswersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeAnswersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeAnswersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeAnswersFilterBuilder
	
	#region SafetyPlansSeAnswersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SafetyPlansSeAnswersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersParameterBuilder : ParameterizedSqlFilterBuilder<SafetyPlansSeAnswersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersParameterBuilder class.
		/// </summary>
		public SafetyPlansSeAnswersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeAnswersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeAnswersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeAnswersParameterBuilder
	
	#region SafetyPlansSeAnswersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SafetyPlansSeAnswersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SafetyPlansSeAnswersSortBuilder : SqlSortBuilder<SafetyPlansSeAnswersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersSqlSortBuilder class.
		/// </summary>
		public SafetyPlansSeAnswersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SafetyPlansSeAnswersSortBuilder
	
} // end namespace
