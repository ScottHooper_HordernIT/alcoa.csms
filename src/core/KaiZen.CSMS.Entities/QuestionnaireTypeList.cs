﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnaireType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnaireType</remark>
	[Serializable]
	public enum QuestionnaireTypeList
	{
		/// <summary> 
		/// Procurement Questionnaire
		/// </summary>
		[EnumTextValue(@"Procurement Questionnaire")]
		Initial_1 = 1, 

		/// <summary> 
		/// Procurement Questionnaire - Secondary Service(s)
		/// </summary>
		[EnumTextValue(@"Procurement Questionnaire - Secondary Service(s)")]
		Initial_2 = 2, 

		/// <summary> 
		/// Supplier Questionnaire
		/// </summary>
		[EnumTextValue(@"Supplier Questionnaire")]
		Main_1 = 3, 

		/// <summary> 
		/// Supplier Questionnaire - Secondary Service(s)
		/// </summary>
		[EnumTextValue(@"Supplier Questionnaire - Secondary Service(s)")]
		Main_2 = 5

	}
}
