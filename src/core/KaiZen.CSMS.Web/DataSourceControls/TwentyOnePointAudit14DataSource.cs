﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit14Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit14DataSourceDesigner))]
	public class TwentyOnePointAudit14DataSource : ProviderDataSource<TwentyOnePointAudit14, TwentyOnePointAudit14Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14DataSource class.
		/// </summary>
		public TwentyOnePointAudit14DataSource() : base(new TwentyOnePointAudit14Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit14DataSourceView used by the TwentyOnePointAudit14DataSource.
		/// </summary>
		protected TwentyOnePointAudit14DataSourceView TwentyOnePointAudit14View
		{
			get { return ( View as TwentyOnePointAudit14DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit14DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit14SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit14SelectMethod selectMethod = TwentyOnePointAudit14SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit14SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit14DataSourceView class that is to be
		/// used by the TwentyOnePointAudit14DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit14DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit14, TwentyOnePointAudit14Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit14DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit14DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit14DataSourceView : ProviderDataSourceView<TwentyOnePointAudit14, TwentyOnePointAudit14Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit14DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit14DataSourceView(TwentyOnePointAudit14DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit14DataSource TwentyOnePointAudit14Owner
		{
			get { return Owner as TwentyOnePointAudit14DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit14SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit14Owner.SelectMethod; }
			set { TwentyOnePointAudit14Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit14Service TwentyOnePointAudit14Provider
		{
			get { return Provider as TwentyOnePointAudit14Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit14> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit14> results = null;
			TwentyOnePointAudit14 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit14SelectMethod.Get:
					TwentyOnePointAudit14Key entityKey  = new TwentyOnePointAudit14Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit14Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit14>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit14SelectMethod.GetAll:
                    results = TwentyOnePointAudit14Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit14SelectMethod.GetPaged:
					results = TwentyOnePointAudit14Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit14SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit14Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit14Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit14SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit14Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit14>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit14SelectMethod.Get || SelectMethod == TwentyOnePointAudit14SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit14 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit14Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit14> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit14Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit14DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit14DataSource class.
	/// </summary>
	public class TwentyOnePointAudit14DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit14, TwentyOnePointAudit14Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit14DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit14SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit14DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit14DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit14DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit14DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit14DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit14DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit14DataSourceActionList(TwentyOnePointAudit14DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit14SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit14DataSourceActionList
	
	#endregion TwentyOnePointAudit14DataSourceDesigner
	
	#region TwentyOnePointAudit14SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit14DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit14SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit14SelectMethod

	#region TwentyOnePointAudit14Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit14"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit14Filter : SqlFilter<TwentyOnePointAudit14Column>
	{
	}
	
	#endregion TwentyOnePointAudit14Filter

	#region TwentyOnePointAudit14ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit14"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit14ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit14Column>
	{
	}
	
	#endregion TwentyOnePointAudit14ExpressionBuilder	

	#region TwentyOnePointAudit14Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit14ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit14"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit14Property : ChildEntityProperty<TwentyOnePointAudit14ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit14Property
}

