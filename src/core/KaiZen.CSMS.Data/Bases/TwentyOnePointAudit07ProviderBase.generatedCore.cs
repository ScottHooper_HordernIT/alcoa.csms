﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit07ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit07ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit07, KaiZen.CSMS.Entities.TwentyOnePointAudit07Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit07Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit07 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit07Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_07 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit07 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_07 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit07 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_07 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit07 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_07 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit07 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_07 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit07 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_07 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit07 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit07&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit07&gt;"/></returns>
		public static TList<TwentyOnePointAudit07> Fill(IDataReader reader, TList<TwentyOnePointAudit07> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit07 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit07")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit07Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit07>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit07",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit07();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit07Column.Id - 1)];
					c.Achieved7a = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7a - 1)];
					c.Achieved7b = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7b - 1)];
					c.Achieved7c = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7c - 1)];
					c.Achieved7d = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7d - 1)];
					c.Achieved7e = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7e - 1)];
					c.Achieved7f = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7f - 1)];
					c.Observation7a = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7a - 1)];
					c.Observation7b = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7b - 1)];
					c.Observation7c = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7c - 1)];
					c.Observation7d = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7d - 1)];
					c.Observation7e = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7e - 1)];
					c.Observation7f = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7f - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit07 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit07Column.Id - 1)];
			entity.Achieved7a = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7a - 1)];
			entity.Achieved7b = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7b - 1)];
			entity.Achieved7c = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7c - 1)];
			entity.Achieved7d = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7d - 1)];
			entity.Achieved7e = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7e - 1)];
			entity.Achieved7f = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Achieved7f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.Achieved7f - 1)];
			entity.Observation7a = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7a - 1)];
			entity.Observation7b = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7b - 1)];
			entity.Observation7c = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7c - 1)];
			entity.Observation7d = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7d - 1)];
			entity.Observation7e = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7e - 1)];
			entity.Observation7f = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.Observation7f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit07Column.Observation7f - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit07Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit07Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit07 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved7a = Convert.IsDBNull(dataRow["Achieved7a"]) ? null : (System.Int32?)dataRow["Achieved7a"];
			entity.Achieved7b = Convert.IsDBNull(dataRow["Achieved7b"]) ? null : (System.Int32?)dataRow["Achieved7b"];
			entity.Achieved7c = Convert.IsDBNull(dataRow["Achieved7c"]) ? null : (System.Int32?)dataRow["Achieved7c"];
			entity.Achieved7d = Convert.IsDBNull(dataRow["Achieved7d"]) ? null : (System.Int32?)dataRow["Achieved7d"];
			entity.Achieved7e = Convert.IsDBNull(dataRow["Achieved7e"]) ? null : (System.Int32?)dataRow["Achieved7e"];
			entity.Achieved7f = Convert.IsDBNull(dataRow["Achieved7f"]) ? null : (System.Int32?)dataRow["Achieved7f"];
			entity.Observation7a = Convert.IsDBNull(dataRow["Observation7a"]) ? null : (System.String)dataRow["Observation7a"];
			entity.Observation7b = Convert.IsDBNull(dataRow["Observation7b"]) ? null : (System.String)dataRow["Observation7b"];
			entity.Observation7c = Convert.IsDBNull(dataRow["Observation7c"]) ? null : (System.String)dataRow["Observation7c"];
			entity.Observation7d = Convert.IsDBNull(dataRow["Observation7d"]) ? null : (System.String)dataRow["Observation7d"];
			entity.Observation7e = Convert.IsDBNull(dataRow["Observation7e"]) ? null : (System.String)dataRow["Observation7e"];
			entity.Observation7f = Convert.IsDBNull(dataRow["Observation7f"]) ? null : (System.String)dataRow["Observation7f"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit07"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit07 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit07 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint07Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit07 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit07 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit07 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit07 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point07IdSource != null)
						{
							child.Point07Id = child.Point07IdSource.Id;
						}
						else
						{
							child.Point07Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit07ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit07</c>
	///</summary>
	public enum TwentyOnePointAudit07ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit07</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit07ChildEntityTypes
	
	#region TwentyOnePointAudit07FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit07Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit07Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit07FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit07FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit07FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit07FilterBuilder
	
	#region TwentyOnePointAudit07ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit07Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit07Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit07ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit07ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit07ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit07ParameterBuilder
	
	#region TwentyOnePointAudit07SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit07Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit07SortBuilder : SqlSortBuilder<TwentyOnePointAudit07Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit07SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit07SortBuilder
	
} // end namespace
