﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEbiDailyReportService
    {
        EbiDailyReport Get(Expression<Func<EbiDailyReport, bool>> where, List<Expression<Func<EbiDailyReport, object>>> includes);
        List<EbiDailyReport> GetMany(int? top, Expression<Func<EbiDailyReport, bool>> where, Func<IQueryable<EbiDailyReport>, IOrderedQueryable<EbiDailyReport>> orderBy, List<Expression<Func<EbiDailyReport, object>>> includes);
        List<EbiDailyReport> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiDailyReport, bool>> where, Func<IQueryable<EbiDailyReport>, IOrderedQueryable<EbiDailyReport>> orderBy, List<Expression<Func<EbiDailyReport, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EbiDailyReport, bool>> where, List<Expression<Func<EbiDailyReport, object>>> includes);
        void Insert(EbiDailyReport item, bool saveChanges = true);
        void Update(EbiDailyReport item, bool saveChanges = true);
        void Delete(EbiDailyReport item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EbiDailyReportService : IEbiDailyReportService
    {
        private readonly IRepository<EbiDailyReport> _repository;

        public EbiDailyReportService(IRepository<EbiDailyReport> repository)
        {
            this._repository = repository;
        }

        public EbiDailyReport Get(Expression<Func<EbiDailyReport, bool>> where, List<Expression<Func<EbiDailyReport, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EbiDailyReport> GetMany(int? top, Expression<Func<EbiDailyReport, bool>> where, Func<IQueryable<EbiDailyReport>, IOrderedQueryable<EbiDailyReport>> orderBy, List<Expression<Func<EbiDailyReport, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EbiDailyReport> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EbiDailyReport, bool>> where, Func<IQueryable<EbiDailyReport>, IOrderedQueryable<EbiDailyReport>> orderBy, List<Expression<Func<EbiDailyReport, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EbiDailyReport, bool>> where, List<Expression<Func<EbiDailyReport, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EbiDailyReport item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiDailyReport");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EbiDailyReport item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiDailyReport");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EbiDailyReport item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EbiDailyReport");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
