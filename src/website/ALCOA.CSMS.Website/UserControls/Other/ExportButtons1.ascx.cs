using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;
using System.Net.Mime;

using System.IO;
using DevExpress.Web.ASPxGridView.Export;
using DevExpress.Web.ASPxPopupControl;

/// <summary>
/// Last Updated: 11/09/2009
/// Description: User control with 3 buttons to export an aspxgridview to rtf(doc)/pdf/xls
/// </summary>
public partial class UserControls_Tiny_ExportButtons1 : System.Web.UI.UserControl
{
    bool forceDownload = true;

    string e_rtd = string.Empty;
    string ne_rtd = string.Empty;
    int e_counter = 0;
    int ne_counter = 0;

    protected void imgBtnWord_Click(object sender, ImageClickEventArgs e) //export to word document format 
    {
        try
        {
            if (gridExporter.GridView.VisibleRowCount < 65000)
            {
                gridExporter.GridView.Columns[2].Visible = false;
                Response.Clear();
                RtfExportOptions opt = new RtfExportOptions();
                opt.ExportWatermarks = true;
                gridExporter.WriteRtfToResponse(opt);
                Response.Close();
            } 
            else
            {
                PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once." +
           "\nThere is currently over 65,000 rows being displayed." +
           "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
            }
        }
        catch (Exception ex)
        {
            PopUpErrorMessage(ex.Message);
        }
    }
    protected void imgBtnPdf_Click(object sender, ImageClickEventArgs e) //export to adobe acrobat pdf format 
    {
        //try
        //{
        //    if (gridExporter.GridView.VisibleRowCount < 65000)
        //    {
        //        gridExporter.GridView.Columns[2].Visible = false;
        //        Response.Clear();
        //        PdfExportOptions opt = new PdfExportOptions();
        //        opt.Compressed = true;
        //        opt.DocumentOptions.Title = gridExporter.FileName;
        //        opt.DocumentOptions.Author = "Alcoa Inc.";
        //        gridExporter.WritePdfToResponse(opt);
        //        Response.Close();
        //    }
        //    else
        //    {
        //        PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once." +
        //   "\nThere is currently over 65,000 rows being displayed." +
        //   "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
        //    }
        //}
        //catch (Exception ex)
        //{

        //    PopUpErrorMessage(ex.Message);
        //}
        try
        {
            if (gridExporter.GridView.VisibleRowCount < 65000)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    gridExporter.GridView.Columns[2].Visible = false;
                    PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
                    pcl.Component = gridExporter;
                    pcl.Margins.Left = pcl.Margins.Right = 50;
                    pcl.Landscape = true;
                    pcl.CreateDocument(false);
                    pcl.PrintingSystem.Document.AutoFitToPagesWidth = 1;
                    pcl.ExportToPdf(ms);
                    WriteResponse(this.Response, ms.ToArray(), System.Net.Mime.DispositionTypeNames.Inline.ToString());
                }
            }
            else
            {
                PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once." +
           "\nThere is currently over 65,000 rows being displayed." +
           "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
            }
        }
        catch (Exception ex)
        {

            PopUpErrorMessage(ex.Message);
        }

    }

    public static void WriteResponse(HttpResponse response, byte[] filearray, string type)
    {
        response.ClearContent();
        response.Buffer = true;
        response.Cache.SetCacheability(HttpCacheability.Private);
        response.ContentType = "application/pdf";
        ContentDisposition contentDisposition = new ContentDisposition();
        contentDisposition.FileName = "ALCOA CSMS - Non Compliance Report - SQ Validation against KPI data.pdf";
        contentDisposition.DispositionType = type;
        response.AddHeader("Content-Disposition", contentDisposition.ToString());
        response.BinaryWrite(filearray);
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        try
        {
            response.End();
        }
        catch (System.Threading.ThreadAbortException)
        {
        }

    }


    //End
    protected void response_write(MemoryStream iStream, string FileName)
    {
        // Buffer to read 10K bytes in chunk:
        byte[] buffer = new Byte[10000];

        // Length of the file:
        int length;

        // Total bytes to read:
        long dataToRead;

        try
        {
            dataToRead = iStream.Length;
            if (forceDownload)
            {
                Response.ContentType = "application/download";
            }
            else
            {
                Response.ContentType = "application/octet-stream";
            }

            string ContentDisposition = String.Format(@"attachment; filename={0}", Server.UrlEncode(FileName));
            String userAgent = Request.Headers.Get("User-Agent");
            if (userAgent.Contains("MSIE 7.0"))
            {
                ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
            }
            else
            {
                ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
            }
            Response.AppendHeader("Content-Disposition", ContentDisposition);

            // Read the bytes.
            while (dataToRead > 0)
            {
                // Verify that the client is connected.
                if (Response.IsClientConnected)
                {
                    // Read the data in buffer.
                    length = iStream.Read(buffer, 0, 10000);

                    // Write the data to the current output stream.
                    Response.OutputStream.Write(buffer, 0, length);

                    // Flush the data to the HTML output.
                    Response.Flush();

                    buffer = new Byte[10000];
                    dataToRead = dataToRead - length;
                }
                else
                {
                    //prevent infinite loop if user disconnects
                    dataToRead = -1;
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Error: " + ex.Message);
        }
        finally
        {
            iStream.Close();
            Response.Close();
        }
    }
    protected void imgBtnExcel_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
    {
        try
        {
            if (gridExporter.GridView.VisibleRowCount < 65000)
            {
                gridExporter.GridView.Columns[2].Visible = false;
                Response.Clear();
                
                    XlsxExportOptions opt = new XlsxExportOptions();
                    opt.SheetName = gridExporter.FileName;
                    opt.ShowGridLines = true;
                    opt.TextExportMode = TextExportMode.Value;
                    
                    gridExporter.WriteXlsxToResponse(opt);
                

                Response.Close();
            }
            else
            {
                PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once."+
            "\nThere is currently over 65,000 rows being displayed."+
            "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
            }
        }
        catch (Exception ex)
        {
            PopUpErrorMessage(ex.Message);
        }
      
    }

    //protected void OnButtonClick(object sender, EventArgs e)
    //{
    //    Response.Clear();
    //    XlsExportOptions opt = new XlsExportOptions();
    //    opt.SheetName = gridExporter.FileName;
    //    opt.ShowGridLines = true;
    //    opt.TextExportMode = TextExportMode.Value;
    //    //opt.UseNativeFormat = true;
    //    gridExporter.WriteXlsToResponse(opt);
    //    Response.Close();
    //}

    protected void PopUpErrorMessage(string errormsg)
    {
        //ASPxGridView1.Visible = false;
        PopupWindow pcWindow = new PopupWindow(errormsg);
        pcWindow.FooterText = "";
        pcWindow.ShowOnPageLoad = true;
        pcWindow.Modal = true;
        //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
        ASPxPopupControl1.Windows.Add(pcWindow);
        //WebChartControl1.Visible = false;
    }

    protected void exporter_RenderBrick(object sender, DevExpress.Web.ASPxGridView.Export.ASPxGridViewExportRenderingEventArgs e)
    {
        try
        {
            if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Header)
            {
                e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
            } 
            if (e.Value != null)
            {
                

                if (e.Column.Name == "Row1" || e.Column.Name == "Row2" || e.Column.Name == "FieldName")
                    return;
                
                if (e.VisibleIndex == 0 || e.VisibleIndex == 3)
                {
                    if (Convert.ToDecimal(e.Value) == 0)
                    {

                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                        

                        
                    }
                    else if (Convert.ToDecimal(e.Value) > 0)
                        e.BrickStyle.BackColor = System.Drawing.Color.Red;
                    e.TextValue = Convert.ToDecimal(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    
                }
                if (e.VisibleIndex == 1)
                {
                    if (Convert.ToDecimal(e.Value) < 1)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToDecimal(e.Value) >= 1)
                        e.BrickStyle.BackColor = System.Drawing.Color.Red;
                    e.TextValue = Convert.ToDecimal(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if (e.VisibleIndex == 2)
                {
                    if (Convert.ToDecimal(e.Value) < 10)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToDecimal(e.Value) >= 10)
                        e.BrickStyle.BackColor = System.Drawing.Color.Red;
                    e.TextValue = Convert.ToDecimal(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if (e.VisibleIndex >=4 && e.VisibleIndex <= 9)
                {
                    //if (e.Column.Name == "SalesRep") return;
                    if (Convert.ToInt32(e.Value) == 0)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToInt32(e.Value) > 0)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    e.TextValue = Convert.ToInt32(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                   
                }
                if (e.VisibleIndex == 10 || e.VisibleIndex == 11)
                {
                    if (Convert.ToDecimal(e.Value) > 5)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToDecimal(e.Value) <= 5)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    e.TextValue = Convert.ToDecimal(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if (e.VisibleIndex == 12 || e.VisibleIndex == 13 || e.VisibleIndex == 20 || (e.VisibleIndex>=25 && e.VisibleIndex<=30))
                {
                    if (Convert.ToString(e.Value) == "")
                        e.BrickStyle.BackColor = System.Drawing.Color.Orange;
                    else if (Convert.ToInt32(e.Value) == 0)
                    {
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                        e.TextValue = Convert.ToInt32(e.Value);
                        e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    }
                    else if (Convert.ToInt32(e.Value) > 0)
                    {
                        e.BrickStyle.BackColor = System.Drawing.Color.Red;
                        e.TextValue = Convert.ToInt32(e.Value);
                        e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    }
                    
                }
                if ((e.VisibleIndex >= 14 && e.VisibleIndex <= 19) || (e.VisibleIndex >= 21 && e.VisibleIndex <= 24))
                {
                    if (Convert.ToString(e.Value) == "")
                        e.BrickStyle.BackColor = System.Drawing.Color.Orange;
                    else if (Convert.ToInt32(e.Value) == 0)
                    {
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                        e.TextValue = Convert.ToInt32(e.Value);
                        e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    }
                    else if (Convert.ToInt32(e.Value) > 0)
                    {
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                        e.TextValue = Convert.ToInt32(e.Value);
                        e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    }
                    
                }
                if (e.VisibleIndex == 31)
                {
                    if (Convert.ToInt32(e.Value) >73)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToInt32(e.Value) <=73)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    e.TextValue = Convert.ToInt32(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if (e.VisibleIndex == 32)
                {
                    if (Convert.ToInt32(e.Value) > 27)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToInt32(e.Value) <= 27)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    e.TextValue = Convert.ToInt32(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if (e.VisibleIndex == 33)
                {
                    if (Convert.ToInt32(e.Value) > 46)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToInt32(e.Value) <= 46)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    e.TextValue = Convert.ToInt32(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if (e.VisibleIndex == 34)
                {
                    if (Convert.ToInt32(e.Value) > 13)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if (Convert.ToInt32(e.Value) <= 13)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    e.TextValue = Convert.ToInt32(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                if ((e.VisibleIndex >= 38 && e.VisibleIndex <= 47) || (e.VisibleIndex >= 50 && e.VisibleIndex <= 57))
                {
                    if (Convert.ToInt32(e.Value) == 100)
                        e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                    else if ((Convert.ToInt32(e.Value)) < 100 && (Convert.ToInt32(e.Value))>=80)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                    else if(Convert.ToInt32(e.Value) < 80)
                        e.BrickStyle.BackColor = System.Drawing.Color.Red;
                    e.Text = e.Value.ToString() + "%";
                    e.TextValueFormatString = "Percentage";
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    
                }
                if (e.VisibleIndex == 35)
                {
                    string[] str = Convert.ToString(e.Value).Split('.');
                     if (Convert.ToInt32(str[0]) == 100)
                         e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                     else if (Convert.ToInt32(str[0]) < 100)
                        e.BrickStyle.BackColor = System.Drawing.Color.Yellow;
                     e.Text = str[0] + "%";
                     e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                }
                
                if (e.VisibleIndex == 59)
                {
                    e_rtd += Convert.ToString(e.Value)+"~";
                }
                if (e.VisibleIndex == 62)
                {
                    ne_rtd +=  Convert.ToString(e.Value)+"~";
                }
                if (e.VisibleIndex == 60)
                {
                    string[] e_rtdsplit = e_rtd.Split('~');
                    try
                    {
                        if (Convert.ToInt32(e_rtdsplit[e_counter]) <= Convert.ToInt32(e.Value))
                            e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                        else if (Convert.ToInt32(e_rtdsplit[e_counter]) > Convert.ToInt32(e.Value))
                            e.BrickStyle.BackColor = System.Drawing.Color.Red;
                        e_counter++;
                        e.TextValue = Convert.ToInt32(e.Value);
                        e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    }
                    catch (Exception ex)
                    {
                        e_counter++;
                        throw;
                    }
                }
                if (e.VisibleIndex == 63)
                {
                    string[] ne_rtdsplit = ne_rtd.Split('~');
                    try
                    {
                        if (Convert.ToInt32(ne_rtdsplit[ne_counter]) <= Convert.ToInt32(e.Value))
                            e.BrickStyle.BackColor = System.Drawing.Color.FromArgb(0, 255, 0);
                        else if (Convert.ToInt32(ne_rtdsplit[ne_counter]) > Convert.ToInt32(e.Value))
                            e.BrickStyle.BackColor = System.Drawing.Color.Red;
                        
                        ne_counter++;
                        e.TextValue = Convert.ToInt32(e.Value);
                        e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    }
                    catch (Exception ex)
                    {
                        ne_counter++;
                        throw;
                    }
                }
                if (e.VisibleIndex == 36 || e.VisibleIndex == 37 || e.VisibleIndex == 61 || e.VisibleIndex == 62 || e.VisibleIndex == 58 || e.VisibleIndex == 59 || e.VisibleIndex == 48)
                {
                    e.TextValue = Convert.ToInt32(e.Value);
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);

                }
                if (e.VisibleIndex == 64 || e.VisibleIndex==49)
                {
                    e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
                    e.TextValue = Convert.ToString(e.Value);
                }

            }
            
           
            //if (Convert.ToString(e.Text) == "Barlas")
            //    e.BrickStyle.BackColor = System.Drawing.Color.Purple;
            //if (Convert.ToString(e.Text) == "Alexandra")
            //    e.BrickStyle.BackColor = System.Drawing.Color.Orange;
            //if (Convert.ToString(e.Text) == "Paris")
            //    e.BrickStyle.BackColor = System.Drawing.Color.PaleGreen;
            //if (Convert.ToString(e.Text) == "Vincent")
            //    e.BrickStyle.BackColor = System.Drawing.Color.LightBlue;
        }
        catch(Exception ex)
        {
          e.BrickStyle.BackColor = System.Drawing.Color.White;
          e.BrickStyle.SetAlignment(DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center);
        }
    }

    

}
