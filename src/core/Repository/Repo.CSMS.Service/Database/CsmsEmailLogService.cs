﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICsmsEmailLogService
    {
        CsmsEmailLog Get(Expression<Func<CsmsEmailLog, bool>> where, List<Expression<Func<CsmsEmailLog, object>>> includes);
        List<CsmsEmailLog> GetMany(int? top, Expression<Func<CsmsEmailLog, bool>> where, Func<IQueryable<CsmsEmailLog>, IOrderedQueryable<CsmsEmailLog>> orderBy, List<Expression<Func<CsmsEmailLog, object>>> includes);
        List<CsmsEmailLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsEmailLog, bool>> where, Func<IQueryable<CsmsEmailLog>, IOrderedQueryable<CsmsEmailLog>> orderBy, List<Expression<Func<CsmsEmailLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CsmsEmailLog, bool>> where, List<Expression<Func<CsmsEmailLog, object>>> includes);
        void Insert(CsmsEmailLog item, bool saveChanges = true);
        void Update(CsmsEmailLog item, bool saveChanges = true);
        void Delete(CsmsEmailLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CsmsEmailLogService : ICsmsEmailLogService
    {
        private readonly IRepository<CsmsEmailLog> _repository;

        public CsmsEmailLogService(IRepository<CsmsEmailLog> repository)
        {
            this._repository = repository;
        }

        public CsmsEmailLog Get(Expression<Func<CsmsEmailLog, bool>> where, List<Expression<Func<CsmsEmailLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CsmsEmailLog> GetMany(int? top, Expression<Func<CsmsEmailLog, bool>> where, Func<IQueryable<CsmsEmailLog>, IOrderedQueryable<CsmsEmailLog>> orderBy, List<Expression<Func<CsmsEmailLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CsmsEmailLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsEmailLog, bool>> where, Func<IQueryable<CsmsEmailLog>, IOrderedQueryable<CsmsEmailLog>> orderBy, List<Expression<Func<CsmsEmailLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CsmsEmailLog, bool>> where, List<Expression<Func<CsmsEmailLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CsmsEmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsEmailLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CsmsEmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsEmailLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CsmsEmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsEmailLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
