﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdHocRadarItems2ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdHocRadarItems2ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdHocRadarItems2, KaiZen.CSMS.Entities.AdHocRadarItems2Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItems2Key key)
		{
			return Delete(transactionManager, key.AdHocRadarItem2Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adHocRadarItem2Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adHocRadarItem2Id)
		{
			return Delete(null, _adHocRadarItem2Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItem2Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adHocRadarItem2Id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items2_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByAdHocRadarId(System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByAdHocRadarId(_adHocRadarId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items2_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		/// <remarks></remarks>
		public TList<AdHocRadarItems2> GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByAdHocRadarId(transactionManager, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items2_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarId(transactionManager, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_AdHoc_Radar key.
		///		fkAdHocRadarItems2AdHocRadar Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adHocRadarId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByAdHocRadarId(System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdHocRadarId(null, _adHocRadarId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_AdHoc_Radar key.
		///		fkAdHocRadarItems2AdHocRadar Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByAdHocRadarId(System.Int32 _adHocRadarId, int start, int pageLength,out int count)
		{
			return GetByAdHocRadarId(null, _adHocRadarId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items2_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public abstract TList<AdHocRadarItems2> GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Companies key.
		///		FK_AdHoc_Radar_Items2_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Companies key.
		///		FK_AdHoc_Radar_Items2_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		/// <remarks></remarks>
		public TList<AdHocRadarItems2> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Companies key.
		///		FK_AdHoc_Radar_Items2_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Companies key.
		///		fkAdHocRadarItems2Companies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Companies key.
		///		fkAdHocRadarItems2Companies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Companies key.
		///		FK_AdHoc_Radar_Items2_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public abstract TList<AdHocRadarItems2> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Sites key.
		///		FK_AdHoc_Radar_Items2_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Sites key.
		///		FK_AdHoc_Radar_Items2_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		/// <remarks></remarks>
		public TList<AdHocRadarItems2> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Sites key.
		///		FK_AdHoc_Radar_Items2_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Sites key.
		///		fkAdHocRadarItems2Sites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Sites key.
		///		fkAdHocRadarItems2Sites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public TList<AdHocRadarItems2> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items2_Sites key.
		///		FK_AdHoc_Radar_Items2_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems2 objects.</returns>
		public abstract TList<AdHocRadarItems2> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdHocRadarItems2 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItems2Key key, int start, int pageLength)
		{
			return GetByAdHocRadarItem2Id(transactionManager, key.AdHocRadarItem2Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="_adHocRadarItem2Id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByAdHocRadarItem2Id(System.Int32 _adHocRadarItem2Id)
		{
			int count = -1;
			return GetByAdHocRadarItem2Id(null,_adHocRadarItem2Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="_adHocRadarItem2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByAdHocRadarItem2Id(System.Int32 _adHocRadarItem2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarItem2Id(null, _adHocRadarItem2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItem2Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByAdHocRadarItem2Id(TransactionManager transactionManager, System.Int32 _adHocRadarItem2Id)
		{
			int count = -1;
			return GetByAdHocRadarItem2Id(transactionManager, _adHocRadarItem2Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItem2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByAdHocRadarItem2Id(TransactionManager transactionManager, System.Int32 _adHocRadarItem2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarItem2Id(transactionManager, _adHocRadarItem2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="_adHocRadarItem2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByAdHocRadarItem2Id(System.Int32 _adHocRadarItem2Id, int start, int pageLength, out int count)
		{
			return GetByAdHocRadarItem2Id(null, _adHocRadarItem2Id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItem2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdHocRadarItems2 GetByAdHocRadarItem2Id(TransactionManager transactionManager, System.Int32 _adHocRadarItem2Id, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByYearAdHocRadarIdCompanyIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyIdSiteId(null,_year, _adHocRadarId, _companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByYearAdHocRadarIdCompanyIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyIdSiteId(null, _year, _adHocRadarId, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByYearAdHocRadarIdCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyIdSiteId(transactionManager, _year, _adHocRadarId, _companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByYearAdHocRadarIdCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyIdSiteId(transactionManager, _year, _adHocRadarId, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems2 GetByYearAdHocRadarIdCompanyIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByYearAdHocRadarIdCompanyIdSiteId(null, _year, _adHocRadarId, _companyId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdHocRadarItems2 GetByYearAdHocRadarIdCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar_Items2_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarId(System.Int32 _year, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByYearAdHocRadarId(null,_year, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarId(System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarId(null, _year, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByYearAdHocRadarId(transactionManager, _year, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarId(transactionManager, _year, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarId(System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength, out int count)
		{
			return GetByYearAdHocRadarId(null, _year, _adHocRadarId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public abstract TList<AdHocRadarItems2> GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar_Items2_2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdCompanyId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyId(null,_year, _adHocRadarId, _companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdCompanyId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyId(null, _year, _adHocRadarId, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdCompanyId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyId(transactionManager, _year, _adHocRadarId, _companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdCompanyId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdCompanyId(transactionManager, _year, _adHocRadarId, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdCompanyId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, int start, int pageLength, out int count)
		{
			return GetByYearAdHocRadarIdCompanyId(null, _year, _adHocRadarId, _companyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public abstract TList<AdHocRadarItems2> GetByYearAdHocRadarIdCompanyId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _companyId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar_Items2_3 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(null,_year, _adHocRadarId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_3 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(null, _year, _adHocRadarId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(transactionManager, _year, _adHocRadarId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(transactionManager, _year, _adHocRadarId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_3 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public TList<AdHocRadarItems2> GetByYearAdHocRadarIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByYearAdHocRadarIdSiteId(null, _year, _adHocRadarId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items2_3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems2&gt;"/> class.</returns>
		public abstract TList<AdHocRadarItems2> GetByYearAdHocRadarIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdHocRadarItems2&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdHocRadarItems2&gt;"/></returns>
		public static TList<AdHocRadarItems2> Fill(IDataReader reader, TList<AdHocRadarItems2> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdHocRadarItems2 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdHocRadarItems2")
					.Append("|").Append((System.Int32)reader[((int)AdHocRadarItems2Column.AdHocRadarItem2Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdHocRadarItems2>(
					key.ToString(), // EntityTrackingKey
					"AdHocRadarItems2",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdHocRadarItems2();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdHocRadarItem2Id = (System.Int32)reader[((int)AdHocRadarItems2Column.AdHocRadarItem2Id - 1)];
					c.AdHocRadarId = (System.Int32)reader[((int)AdHocRadarItems2Column.AdHocRadarId - 1)];
					c.Year = (System.Int32)reader[((int)AdHocRadarItems2Column.Year - 1)];
					c.SiteId = (System.Int32)reader[((int)AdHocRadarItems2Column.SiteId - 1)];
					c.CompanyId = (System.Int32)reader[((int)AdHocRadarItems2Column.CompanyId - 1)];
					c.KpiComplianceScore = (reader.IsDBNull(((int)AdHocRadarItems2Column.KpiComplianceScore - 1)))?null:(System.Int32?)reader[((int)AdHocRadarItems2Column.KpiComplianceScore - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)AdHocRadarItems2Column.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdHocRadarItems2 entity)
		{
			if (!reader.Read()) return;
			
			entity.AdHocRadarItem2Id = (System.Int32)reader[((int)AdHocRadarItems2Column.AdHocRadarItem2Id - 1)];
			entity.AdHocRadarId = (System.Int32)reader[((int)AdHocRadarItems2Column.AdHocRadarId - 1)];
			entity.Year = (System.Int32)reader[((int)AdHocRadarItems2Column.Year - 1)];
			entity.SiteId = (System.Int32)reader[((int)AdHocRadarItems2Column.SiteId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)AdHocRadarItems2Column.CompanyId - 1)];
			entity.KpiComplianceScore = (reader.IsDBNull(((int)AdHocRadarItems2Column.KpiComplianceScore - 1)))?null:(System.Int32?)reader[((int)AdHocRadarItems2Column.KpiComplianceScore - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)AdHocRadarItems2Column.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdHocRadarItems2 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdHocRadarItem2Id = (System.Int32)dataRow["AdHoc_Radar_Item2Id"];
			entity.AdHocRadarId = (System.Int32)dataRow["AdHoc_RadarId"];
			entity.Year = (System.Int32)dataRow["Year"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.KpiComplianceScore = Convert.IsDBNull(dataRow["KpiComplianceScore"]) ? null : (System.Int32?)dataRow["KpiComplianceScore"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadarItems2"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdHocRadarItems2 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItems2 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdHocRadarIdSource	
			if (CanDeepLoad(entity, "AdHocRadar|AdHocRadarIdSource", deepLoadType, innerList) 
				&& entity.AdHocRadarIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdHocRadarId;
				AdHocRadar tmpEntity = EntityManager.LocateEntity<AdHocRadar>(EntityLocator.ConstructKeyFromPkItems(typeof(AdHocRadar), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdHocRadarIdSource = tmpEntity;
				else
					entity.AdHocRadarIdSource = DataRepository.AdHocRadarProvider.GetByAdHocRadarId(transactionManager, entity.AdHocRadarId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdHocRadarIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdHocRadarProvider.DeepLoad(transactionManager, entity.AdHocRadarIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdHocRadarIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdHocRadarItems2 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdHocRadarItems2 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdHocRadarItems2 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItems2 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdHocRadarIdSource
			if (CanDeepSave(entity, "AdHocRadar|AdHocRadarIdSource", deepSaveType, innerList) 
				&& entity.AdHocRadarIdSource != null)
			{
				DataRepository.AdHocRadarProvider.Save(transactionManager, entity.AdHocRadarIdSource);
				entity.AdHocRadarId = entity.AdHocRadarIdSource.AdHocRadarId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdHocRadarItems2ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdHocRadarItems2</c>
	///</summary>
	public enum AdHocRadarItems2ChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdHocRadar</c> at AdHocRadarIdSource
		///</summary>
		[ChildEntityType(typeof(AdHocRadar))]
		AdHocRadar,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion AdHocRadarItems2ChildEntityTypes
	
	#region AdHocRadarItems2FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdHocRadarItems2Column&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2FilterBuilder : SqlFilterBuilder<AdHocRadarItems2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2FilterBuilder class.
		/// </summary>
		public AdHocRadarItems2FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItems2FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItems2FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItems2FilterBuilder
	
	#region AdHocRadarItems2ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdHocRadarItems2Column&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2ParameterBuilder : ParameterizedSqlFilterBuilder<AdHocRadarItems2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2ParameterBuilder class.
		/// </summary>
		public AdHocRadarItems2ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItems2ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItems2ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItems2ParameterBuilder
	
	#region AdHocRadarItems2SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdHocRadarItems2Column&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdHocRadarItems2SortBuilder : SqlSortBuilder<AdHocRadarItems2Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2SqlSortBuilder class.
		/// </summary>
		public AdHocRadarItems2SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdHocRadarItems2SortBuilder
	
} // end namespace
