USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_SMP_HS_Assessor_Insert]    Script Date: 3/08/2015 1:20:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Dashboard_SMP_HS_Assessor_Insert]        
(        
 @Site int        
 ,@Month int        
 ,@Year int        
 ,@HSAssessorsRequiredCompanies decimal(18,2)        
 ,@HSAssessorsBeingAssessedCompanies decimal(18,2)        
 ,@HSAssessorsAwaitingAssignment decimal(18,2)        
 ,@HSAssessorsApproved decimal(18,2)        
 ,@HSAssessorsApprovedPer decimal(18,2)        
        
)        
AS        
if exists(Select * from  Dashboard_SMP_HS_Assessor where SiteId=@Site and [Month]=@Month  
 and [Year]=@Year)  
 begin   
 Update Dashboard_SMP_HS_Assessor Set  
   [HS Assessors � Required (# Companies)] = @HSAssessorsRequiredCompanies      
     ,[HS Assessors � Being Assessed (# Companies)]  =@HSAssessorsBeingAssessedCompanies      
     ,[HS Assessors � Awaiting Assignment]=@HSAssessorsAwaitingAssignment        
     ,[HS Assessors � Approved] = @HSAssessorsApproved       
     ,[HS Assessors � Approved %] =@HSAssessorsApprovedPer 
     ,[LastModifiedDate] = GETDATE() 
    where SiteId=@Site AND [Month]=@Month AND [Year]=@Year  
   end  
        
     else  
 begin       
    INSERT INTO [dbo].[Dashboard_SMP_HS_Assessor]        
     (        
     [SiteId]        
     ,[Month]        
     ,[Year]        
     ,[HS Assessors � Required (# Companies)]        
     ,[HS Assessors � Being Assessed (# Companies)]        
     ,[HS Assessors � Awaiting Assignment]        
     ,[HS Assessors � Approved]        
     ,[HS Assessors � Approved %]  
     ,[LastModifiedDate]      
     )        
    VALUES        
     (        
     @Site        
     ,@Month        
     ,@Year        
     ,@HSAssessorsRequiredCompanies        
     ,@HSAssessorsBeingAssessedCompanies        
     ,@HSAssessorsAwaitingAssignment        
     ,@HSAssessorsApproved        
     ,@HSAssessorsApprovedPer 
     ,GETDATE()       
     )   
end       
----------------------------------------

GO


