USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_KpiCompanySiteCategory_Compliance_DashBoard]    Script Date: 3/08/2015 1:12:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_KpiCompanySiteCategory_Compliance_DashBoard]                              
(                              
 @SiteId int,                            
 @Month int,                            
 @Year int                              
)                              
AS                              
                
IF (@SiteId > 0)                             
 BEGIN                  
  SELECT                              
  ISNULL(AVG(aheaAvgNopplSiteMonth),0) As [Average No. Of People On Site],                              
  ISNULL(SUM(COALESCE(aheaTotalManHours,0)),0) As [Total Man Hours],                              
  ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) As [LWDFR],                              
  ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) As [TRIFR],     
  
    
      
        
          
              
            
  ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18
  
    
      
        
          
,2            
  )))),0) As [AIFR], SUM(ISNULL(CAST(EbiOnSite As Int),0)) As [EbiOnSite],               
                             
  --added by Sayani for Task# 1             
                              
  (SELECT ISNULL(CAST(CASE                               
   WHEN (                              
     COALESCE(ISNULL(SUM(dbo.KpiCompanySiteCategory.aheaTotalManHours),0),0) = 0                              
     )                              
     THEN (0)                              
   ELSE (                              
     (((ISNULL(SUM(dbo.KpiCompanySiteCategory.ipLTI),0) + (ISNULL(SUM(dbo.KpiCompanySiteCategory.ipRDI),0))))                              
     * 200000) / (SUM(dbo.KpiCompanySiteCategory.aheaTotalManHours))                              
     )                               
   END As Decimal(18,2)),0)) As [DART],                            
                        
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipFATI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipFATI,0)))                          
   END) AS int))),0) as [FA],                 
                            
                  
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipMTI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipMTI,0)))                          
   END) AS int))),0) as [MT],                 
                  
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipRDI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipRDI,0)))                          
   END) AS int))),0) as [RST],                 
                  
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipLTI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipLTI,0)))                          
   END) AS int))),0) as [LWD],                 
                  
  --SUM(COALESCE(ipIFE,0)) as [IFECount],                            
                        
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipIFE,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipIFE,0)))                          
   END) AS int))),0) as [IFE],                       
                        
  --SUM(COALESCE(ipRN,0)) as [RNCount],                       
   ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipRN,0)) = 0)                          
   THEN 0    
   ELSE (SUM(COALESCE(ipRN,0)))                          
   END) AS int))),0)  as [RN],                         
                          
  --SUM(COALESCE(ipRN,0)) as [Risk Notifications],                         
                        
    ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipRN,0)) = 0)                     
   THEN 0                          
   ELSE (SUM(COALESCE(ipRN,0)))                          
END) AS int))),0) as [Risk Notifications],                       
                           
  --(SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) as [IFE+RN Count],                            
                           
  ISNULL(((CAST((CASE                          
    WHEN ((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) = 0.0)                          
   THEN 0                          
   ELSE ((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))))             
   END) AS int))),0)as [IFE+RN Count],                          
                       
  ISNULL(CAST((CASE                            
  WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                             
  THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,2))                             
  ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2)))            
  END) AS DECIMAL(18,2)),0) As [RN Injury Ratio],                            
            
  ISNULL(CAST((CASE                            
  WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                             
  THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2))                             
  ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2)))            
  END) AS DECIMAL(18,2)),0) As [IFE Injury Ratio],                           
            
  ISNULL(CAST((CASE                            
  WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                             
  THEN CAST((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) AS DECIMAL(18,2))                             
  ELSE (CAST((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2)))            
  END) AS DECIMAL(18,2)),0) As [IFERN Injury Ratio]                           
                              
  FROM KpiCompanySiteCategory                 
  WHERE YEAR(kpiDateTime) = @Year      
  AND CompanySiteCategoryId like '%'                 
  --AND MONTH(kpiDateTime) = @Month                
  AND SiteId = @SiteId             
 END                      
ELSE                      
 BEGIN                  
  SELECT                              
  ISNULL(AVG(aheaAvgNopplSiteMonth),0) As [Average No. Of People On Site],                              
  ISNULL(SUM(COALESCE(aheaTotalManHours,0)),0) As [Total Man Hours],                              
  ISNULL(CAST((ISNULL((SUM(COALESCE(ipLTI,0)) * 200000),0) / NULLIF(ISNULL(COALESCE((SUM(COALESCE(aheaTotalManHours,0))),0),0),0)) AS Decimal(18,2)),0) As [LWDFR],                              
  ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18,2)))),0) As [TRIFR],    
   
   
       
        
         
               
            
  ISNULL(((CAST((CASE WHEN (SUM(COALESCE(aheaTotalManHours,0)) = 0.0) THEN 0.0 ELSE (((SUM(COALESCE(ipFATI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipLTI,0))) * 200000) / (SUM(COALESCE(aheaTotalManHours,0)))) END) AS DECIMAL(18
  
    
      
        
          
,2            
  )))),0) As [AIFR], SUM(ISNULL(CAST(EbiOnSite As Int),0)) As [EbiOnSite],               
                             
  --added by Sayani for Task# 1             
                              
  (SELECT ISNULL(CAST(CASE                     
   WHEN (                              
     COALESCE(ISNULL(SUM(dbo.KpiCompanySiteCategory.aheaTotalManHours),0),0) = 0                              
     )                              
     THEN (0)                              
   ELSE (                              
     (((ISNULL(SUM(dbo.KpiCompanySiteCategory.ipLTI),0) + (ISNULL(SUM(dbo.KpiCompanySiteCategory.ipRDI),0))))                              
     * 200000) / (SUM(dbo.KpiCompanySiteCategory.aheaTotalManHours))                              
     )                               
   END As Decimal(18,2)),0)) As [DART],                           
                    
                        
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipFATI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipFATI,0)))                          
   END) AS int))),0) as [FA],              
                            
                  
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipMTI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipMTI,0)))                          
   END) AS int))),0) as [MT],                 
                  
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipRDI,0)) = 0)                          
   THEN 0                     
   ELSE (SUM(COALESCE(ipRDI,0)))                          
   END) AS int))),0) as [RST],                 
                  
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipLTI,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipLTI,0)))                          
   END) AS int))),0) as [LWD],                 
                  
  --SUM(COALESCE(ipIFE,0)) as [IFECount],                            
                        
  ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipIFE,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipIFE,0)))                          
   END) AS int))),0) as [IFE],                       
                        
  --SUM(COALESCE(ipRN,0)) as [RNCount],                       
   ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipRN,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipRN,0)))                          
   END) AS int))),0)  as [RN],                         
                          
  --SUM(COALESCE(ipRN,0)) as [Risk Notifications],                         
                        
    ISNULL(((CAST((CASE                          
    WHEN (SUM(COALESCE(ipRN,0)) = 0)                          
   THEN 0                          
   ELSE (SUM(COALESCE(ipRN,0)))                          
   END) AS int))),0) as [Risk Notifications],                       
                           
  --(SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) as [IFE+RN Count],                            
                            
  ISNULL(((CAST((CASE                          
    WHEN ((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) = 0.0)                          
   THEN 0.0                          
   ELSE ((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))))                          
   END) AS int))),0)as [IFE+RN Count],                          
                       
    ISNULL(CAST((CASE                            
  WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                             
  THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,2))                             
  ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2)))            
  END) AS DECIMAL(18,2)),0) As [RN Injury Ratio],                            
            
  ISNULL(CAST((CASE                            
  WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                             
  THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2))                           
  ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2)))            
  END) AS DECIMAL(18,2)),0) As [IFE Injury Ratio],                           
            
  ISNULL(CAST((CASE                            
  WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                             
  THEN CAST((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) AS DECIMAL(18,2))                             
  ELSE (CAST((SUM(COALESCE(ipIFE,0)) + SUM(COALESCE(ipRN,0))) AS DECIMAL(18,2)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,2)))            
  END) AS DECIMAL(18,2)),0) As [IFERN Injury Ratio]                           
            
                              
  FROM KpiCompanySiteCategory                 
  WHERE YEAR(kpiDateTime) = @Year            
  AND CompanySiteCategoryId like '%'               
  --AND MONTH(kpiDateTime) = @Month                
  AND ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = dbo.KpiCompanySiteCategory.SiteId) > 0)                       
 END              
----------------------------------------

GO


