﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using System.Configuration;

using System.Data;
using System.Data.SqlClient;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;

using System.IO;
using System.Web.Configuration;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class query : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        static string sqldb = "";

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");
            ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
            sqldb = conString.ConnectionString;

            if (Session["adminquery"] != null)
            {
                grid.DataSource = (DataTable)Session["adminquery"];
            }

            if (!postBack)
            {
                //Configuration config = new Configuration();
                ////if (config.GetValue(ConfigList.Debug))
                //lblQuery.Text = "Query: " + conString.ToString();

                if (Request.QueryString["q"] != null)
                {
                    if (Request.QueryString["debug"].ToString() == "1")
                    {
                        //ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        Configuration config = new Configuration();
                        //if (config.GetValue(ConfigList.Debug))
                        Response.Write(String.Format("Connecting via:{0}/{1} [{2}]<br /><br />Using: {3}",
                                                        WebConfigurationManager.AppSettings.Get("WAOCSM_domain"),
                                                        WebConfigurationManager.AppSettings.Get("WAOCSM_user"),
                                                        WebConfigurationManager.AppSettings.Get("WAOCSM_pass"),
                                                        conString.ToString()));
                    }
                }
            }
        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            try
            {
                
                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();
                    SqlCommand cmd = new SqlCommand(mQuery.Text, cn);

                    try
                    {
                        switch ((string)cbResultType.SelectedItem.Value)
                        {
                            case "grid":
                                using(SqlDataReader reader = cmd.ExecuteReader())
                                {
                                    DataTable dt = new DataTable();
                                    dt.Load(reader);

                                    Session["adminquery"] = dt;
                                }

                                grid.DataSource = (DataTable)Session["adminquery"];
                                grid.DataBind();
                                lblResults.Text = "Query Returned grid";
                                break;
                            case "string":
                                lblResults.Text = "Query Result: " + (string)cmd.ExecuteScalar();
                                break;
                            case "int":
                                lblResults.Text = "Query Result: " + ((int)cmd.ExecuteScalar()).ToString();
                                break;
                            case "n/a":
                                cmd.ExecuteNonQuery();
                                lblResults.Text = "Query Executed Successfully";
                                break;
                            default:
                                throw new Exception("No Result Type Selected");
                        }
                        cmd.Dispose();
                        cn.Close();
                    }
                    catch (Exception ex)
                    {
                        cmd.Dispose();
                        cn.Close();
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                lblResults.Text = "Query Failed: " + ex.Message.ToString();
            }
        }
    }
}