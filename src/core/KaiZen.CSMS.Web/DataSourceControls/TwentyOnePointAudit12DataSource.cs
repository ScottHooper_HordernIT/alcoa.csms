﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit12Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit12DataSourceDesigner))]
	public class TwentyOnePointAudit12DataSource : ProviderDataSource<TwentyOnePointAudit12, TwentyOnePointAudit12Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12DataSource class.
		/// </summary>
		public TwentyOnePointAudit12DataSource() : base(new TwentyOnePointAudit12Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit12DataSourceView used by the TwentyOnePointAudit12DataSource.
		/// </summary>
		protected TwentyOnePointAudit12DataSourceView TwentyOnePointAudit12View
		{
			get { return ( View as TwentyOnePointAudit12DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit12DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit12SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit12SelectMethod selectMethod = TwentyOnePointAudit12SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit12SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit12DataSourceView class that is to be
		/// used by the TwentyOnePointAudit12DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit12DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit12, TwentyOnePointAudit12Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit12DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit12DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit12DataSourceView : ProviderDataSourceView<TwentyOnePointAudit12, TwentyOnePointAudit12Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit12DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit12DataSourceView(TwentyOnePointAudit12DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit12DataSource TwentyOnePointAudit12Owner
		{
			get { return Owner as TwentyOnePointAudit12DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit12SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit12Owner.SelectMethod; }
			set { TwentyOnePointAudit12Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit12Service TwentyOnePointAudit12Provider
		{
			get { return Provider as TwentyOnePointAudit12Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit12> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit12> results = null;
			TwentyOnePointAudit12 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit12SelectMethod.Get:
					TwentyOnePointAudit12Key entityKey  = new TwentyOnePointAudit12Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit12Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit12>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit12SelectMethod.GetAll:
                    results = TwentyOnePointAudit12Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit12SelectMethod.GetPaged:
					results = TwentyOnePointAudit12Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit12SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit12Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit12Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit12SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit12Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit12>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit12SelectMethod.Get || SelectMethod == TwentyOnePointAudit12SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit12 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit12Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit12> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit12Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit12DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit12DataSource class.
	/// </summary>
	public class TwentyOnePointAudit12DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit12, TwentyOnePointAudit12Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit12DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit12SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit12DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit12DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit12DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit12DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit12DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit12DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit12DataSourceActionList(TwentyOnePointAudit12DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit12SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit12DataSourceActionList
	
	#endregion TwentyOnePointAudit12DataSourceDesigner
	
	#region TwentyOnePointAudit12SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit12DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit12SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit12SelectMethod

	#region TwentyOnePointAudit12Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit12"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit12Filter : SqlFilter<TwentyOnePointAudit12Column>
	{
	}
	
	#endregion TwentyOnePointAudit12Filter

	#region TwentyOnePointAudit12ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit12"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit12ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit12Column>
	{
	}
	
	#endregion TwentyOnePointAudit12ExpressionBuilder	

	#region TwentyOnePointAudit12Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit12ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit12"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit12Property : ChildEntityProperty<TwentyOnePointAudit12ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit12Property
}

