﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiCompanySiteCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class KpiCompanySiteCategoryProviderBaseCore : EntityViewProviderBase<KpiCompanySiteCategory>
	{
		#region Custom Methods
		
		
		#region _KpiCompanySiteCategory_GetByCompanySiteCategoryId
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanySiteCategoryId(System.Int32? companySiteCategoryId)
		{
			return GetByCompanySiteCategoryId(null, 0, int.MaxValue , companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanySiteCategoryId(int start, int pageLength, System.Int32? companySiteCategoryId)
		{
			return GetByCompanySiteCategoryId(null, start, pageLength , companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? companySiteCategoryId)
		{
			return GetByCompanySiteCategoryId(transactionManager, 0, int.MaxValue , companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> GetByCompanySiteCategoryId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Compliance_Table_AS_CSA
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS_CSA' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AS_CSA(System.Int32? siteId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId)
		{
			return Compliance_Table_AS_CSA(null, 0, int.MaxValue , siteId, year, qtr, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS_CSA' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AS_CSA(int start, int pageLength, System.Int32? siteId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId)
		{
			return Compliance_Table_AS_CSA(null, start, pageLength , siteId, year, qtr, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS_CSA' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AS_CSA(TransactionManager transactionManager, System.Int32? siteId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId)
		{
			return Compliance_Table_AS_CSA(transactionManager, 0, int.MaxValue , siteId, year, qtr, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS_CSA' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Compliance_Table_AS_CSA(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Compliance_Table_AS
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AS(System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_AS(null, 0, int.MaxValue , siteId, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AS(int start, int pageLength, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_AS(null, start, pageLength , siteId, year, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AS(TransactionManager transactionManager, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_AS(transactionManager, 0, int.MaxValue , siteId, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AS' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Compliance_Table_AS(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_GetByCompanyId
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyId(System.Int32? companyId)
		{
			return GetByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> GetByCompanyId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_GetByCompanyIdSiteId
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteId(System.Int32? companyId, System.Int32? siteId)
		{
			return GetByCompanyIdSiteId(null, 0, int.MaxValue , companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteId(int start, int pageLength, System.Int32? companyId, System.Int32? siteId)
		{
			return GetByCompanyIdSiteId(null, start, pageLength , companyId, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32? companyId, System.Int32? siteId)
		{
			return GetByCompanyIdSiteId(transactionManager, 0, int.MaxValue , companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> GetByCompanyIdSiteId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? siteId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_GetByCompanyIdSiteIdYearMonth
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYearMonth' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYearMonth(System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.Int32? month)
		{
			return GetByCompanyIdSiteIdYearMonth(null, 0, int.MaxValue , companyId, siteId, year, month);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYearMonth' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYearMonth(int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.Int32? month)
		{
			return GetByCompanyIdSiteIdYearMonth(null, start, pageLength , companyId, siteId, year, month);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYearMonth' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYearMonth(TransactionManager transactionManager, System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.Int32? month)
		{
			return GetByCompanyIdSiteIdYearMonth(transactionManager, 0, int.MaxValue , companyId, siteId, year, month);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYearMonth' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYearMonth(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.Int32? month);
		
		#endregion

		
		#region _KpiCompanySiteCategory_ByMonthYear_CompanySiteCategoryId
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear_CompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> ByMonthYear_CompanySiteCategoryId(System.Int32? year, System.Int32? month, System.Int32? companySiteCategoryId)
		{
			return ByMonthYear_CompanySiteCategoryId(null, 0, int.MaxValue , year, month, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear_CompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> ByMonthYear_CompanySiteCategoryId(int start, int pageLength, System.Int32? year, System.Int32? month, System.Int32? companySiteCategoryId)
		{
			return ByMonthYear_CompanySiteCategoryId(null, start, pageLength , year, month, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear_CompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> ByMonthYear_CompanySiteCategoryId(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.Int32? companySiteCategoryId)
		{
			return ByMonthYear_CompanySiteCategoryId(transactionManager, 0, int.MaxValue , year, month, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear_CompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> ByMonthYear_CompanySiteCategoryId(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32? month, System.Int32? companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Compliance_Table_AA
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AA(System.Int32? regionId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_AA(null, 0, int.MaxValue , regionId, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AA(int start, int pageLength, System.Int32? regionId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_AA(null, start, pageLength , regionId, year, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AA(TransactionManager transactionManager, System.Int32? regionId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_AA(transactionManager, 0, int.MaxValue , regionId, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Compliance_Table_AA(TransactionManager transactionManager, int start, int pageLength, System.Int32? regionId, System.Int32? year, System.String companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Compliance_Table_SS_Month
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS_Month' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_SS_Month(System.Int32? companyId, System.Int32? siteId, System.Int32? month, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_SS_Month(null, 0, int.MaxValue , companyId, siteId, month, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS_Month' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_SS_Month(int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? month, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_SS_Month(null, start, pageLength , companyId, siteId, month, year, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS_Month' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_SS_Month(TransactionManager transactionManager, System.Int32? companyId, System.Int32? siteId, System.Int32? month, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_SS_Month(transactionManager, 0, int.MaxValue , companyId, siteId, month, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS_Month' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Compliance_Table_SS_Month(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? month, System.Int32? year, System.String companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_ByMonthYear
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> ByMonthYear(System.Int32? year, System.Int32? month)
		{
			return ByMonthYear(null, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> ByMonthYear(int start, int pageLength, System.Int32? year, System.Int32? month)
		{
			return ByMonthYear(null, start, pageLength , year, month);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> ByMonthYear(TransactionManager transactionManager, System.Int32? year, System.Int32? month)
		{
			return ByMonthYear(transactionManager, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_ByMonthYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> ByMonthYear(TransactionManager transactionManager, int start, int pageLength, System.Int32? year, System.Int32? month);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId_ByCompanyId
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId_ByCompanyId' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Exceptions_NoCompanySiteCategoryId_ByCompanyId()
		{
			return Exceptions_NoCompanySiteCategoryId_ByCompanyId(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Exceptions_NoCompanySiteCategoryId_ByCompanyId(int start, int pageLength)
		{
			return Exceptions_NoCompanySiteCategoryId_ByCompanyId(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId_ByCompanyId' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Exceptions_NoCompanySiteCategoryId_ByCompanyId(TransactionManager transactionManager)
		{
			return Exceptions_NoCompanySiteCategoryId_ByCompanyId(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Exceptions_NoCompanySiteCategoryId_ByCompanyId(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> Exceptions_NoCompanySiteCategoryId()
		{
			return Exceptions_NoCompanySiteCategoryId(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> Exceptions_NoCompanySiteCategoryId(int start, int pageLength)
		{
			return Exceptions_NoCompanySiteCategoryId(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> Exceptions_NoCompanySiteCategoryId(TransactionManager transactionManager)
		{
			return Exceptions_NoCompanySiteCategoryId(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Exceptions_NoCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> Exceptions_NoCompanySiteCategoryId(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Compliance_Table_SS
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_SS(System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_SS(null, 0, int.MaxValue , companyId, siteId, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_SS(int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_SS(null, start, pageLength , companyId, siteId, year, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_SS(TransactionManager transactionManager, System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId)
		{
			return Compliance_Table_SS(transactionManager, 0, int.MaxValue , companyId, siteId, year, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_SS' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Compliance_Table_SS(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? year, System.String companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_Compliance_Table_AA_CSA
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA_CSA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AA_CSA(System.Int32? regionId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId)
		{
			return Compliance_Table_AA_CSA(null, 0, int.MaxValue , regionId, year, qtr, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA_CSA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AA_CSA(int start, int pageLength, System.Int32? regionId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId)
		{
			return Compliance_Table_AA_CSA(null, start, pageLength , regionId, year, qtr, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA_CSA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Compliance_Table_AA_CSA(TransactionManager transactionManager, System.Int32? regionId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId)
		{
			return Compliance_Table_AA_CSA(transactionManager, 0, int.MaxValue , regionId, year, qtr, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_Compliance_Table_AA_CSA' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="qtr"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Compliance_Table_AA_CSA(TransactionManager transactionManager, int start, int pageLength, System.Int32? regionId, System.Int32? year, System.Int32? qtr, System.String companySiteCategoryId);
		
		#endregion

		
		#region _KpiCompanySiteCategory_GetByCompanyIdSiteIdYear
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYear(System.Int32? companyId, System.Int32? siteId, System.Int32? year)
		{
			return GetByCompanyIdSiteIdYear(null, 0, int.MaxValue , companyId, siteId, year);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYear(int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? year)
		{
			return GetByCompanyIdSiteIdYear(null, start, pageLength , companyId, siteId, year);
		}
				
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYear(TransactionManager transactionManager, System.Int32? companyId, System.Int32? siteId, System.Int32? year)
		{
			return GetByCompanyIdSiteIdYear(transactionManager, 0, int.MaxValue , companyId, siteId, year);
		}
		
		/// <summary>
		///	This method wrap the '_KpiCompanySiteCategory_GetByCompanyIdSiteIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> instance.</returns>
		public abstract VList<KpiCompanySiteCategory> GetByCompanyIdSiteIdYear(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? siteId, System.Int32? year);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;KpiCompanySiteCategory&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;KpiCompanySiteCategory&gt;"/></returns>
		protected static VList&lt;KpiCompanySiteCategory&gt; Fill(DataSet dataSet, VList<KpiCompanySiteCategory> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<KpiCompanySiteCategory>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;KpiCompanySiteCategory&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<KpiCompanySiteCategory>"/></returns>
		protected static VList&lt;KpiCompanySiteCategory&gt; Fill(DataTable dataTable, VList<KpiCompanySiteCategory> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					KpiCompanySiteCategory c = new KpiCompanySiteCategory();
					c.KpiId = (Convert.IsDBNull(row["KpiId"]))?(int)0:(System.Int32)row["KpiId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.CreatedbyUserId = (Convert.IsDBNull(row["CreatedbyUserId"]))?(int)0:(System.Int32)row["CreatedbyUserId"];
					c.ModifiedbyUserId = (Convert.IsDBNull(row["ModifiedbyUserId"]))?(int)0:(System.Int32)row["ModifiedbyUserId"];
					c.DateAdded = (Convert.IsDBNull(row["DateAdded"]))?DateTime.MinValue:(System.DateTime)row["DateAdded"];
					c.Datemodified = (Convert.IsDBNull(row["Datemodified"]))?DateTime.MinValue:(System.DateTime)row["Datemodified"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.KpiDateTime = (Convert.IsDBNull(row["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)row["kpiDateTime"];
					c.KpiGeneral = (Convert.IsDBNull(row["kpiGeneral"]))?0.0m:(System.Decimal)row["kpiGeneral"];
					c.KpiCalcinerExpense = (Convert.IsDBNull(row["kpiCalcinerExpense"]))?0.0m:(System.Decimal)row["kpiCalcinerExpense"];
					c.KpiCalcinerCapital = (Convert.IsDBNull(row["kpiCalcinerCapital"]))?0.0m:(System.Decimal)row["kpiCalcinerCapital"];
					c.KpiResidue = (Convert.IsDBNull(row["kpiResidue"]))?0.0m:(System.Decimal)row["kpiResidue"];
					c.ProjectCapital1Title = (Convert.IsDBNull(row["projectCapital1Title"]))?string.Empty:(System.String)row["projectCapital1Title"];
					c.ProjectCapital1Hours = (Convert.IsDBNull(row["projectCapital1Hours"]))?0.0m:(System.Decimal?)row["projectCapital1Hours"];
					c.ProjectCapital2Title = (Convert.IsDBNull(row["projectCapital2Title"]))?string.Empty:(System.String)row["projectCapital2Title"];
					c.ProjectCapital2Hours = (Convert.IsDBNull(row["projectCapital2Hours"]))?0.0m:(System.Decimal?)row["projectCapital2Hours"];
					c.ProjectCapital3Title = (Convert.IsDBNull(row["projectCapital3Title"]))?string.Empty:(System.String)row["projectCapital3Title"];
					c.ProjectCapital3Hours = (Convert.IsDBNull(row["projectCapital3Hours"]))?0.0m:(System.Decimal?)row["projectCapital3Hours"];
					c.ProjectCapital4Title = (Convert.IsDBNull(row["projectCapital4Title"]))?string.Empty:(System.String)row["projectCapital4Title"];
					c.ProjectCapital4Hours = (Convert.IsDBNull(row["projectCapital4Hours"]))?0.0m:(System.Decimal?)row["projectCapital4Hours"];
					c.ProjectCapital5Title = (Convert.IsDBNull(row["projectCapital5Title"]))?string.Empty:(System.String)row["projectCapital5Title"];
					c.ProjectCapital5Hours = (Convert.IsDBNull(row["projectCapital5Hours"]))?0.0m:(System.Decimal?)row["projectCapital5Hours"];
					c.ProjectCapital6Title = (Convert.IsDBNull(row["projectCapital6Title"]))?string.Empty:(System.String)row["projectCapital6Title"];
					c.ProjectCapital6Hours = (Convert.IsDBNull(row["projectCapital6Hours"]))?0.0m:(System.Decimal?)row["projectCapital6Hours"];
					c.ProjectCapital7Title = (Convert.IsDBNull(row["projectCapital7Title"]))?string.Empty:(System.String)row["projectCapital7Title"];
					c.ProjectCapital7Hours = (Convert.IsDBNull(row["projectCapital7Hours"]))?0.0m:(System.Decimal?)row["projectCapital7Hours"];
					c.ProjectCapital8Title = (Convert.IsDBNull(row["projectCapital8Title"]))?string.Empty:(System.String)row["projectCapital8Title"];
					c.ProjectCapital8Hours = (Convert.IsDBNull(row["projectCapital8Hours"]))?0.0m:(System.Decimal?)row["projectCapital8Hours"];
					c.ProjectCapital9Title = (Convert.IsDBNull(row["projectCapital9Title"]))?string.Empty:(System.String)row["projectCapital9Title"];
					c.ProjectCapital9Hours = (Convert.IsDBNull(row["projectCapital9Hours"]))?0.0m:(System.Decimal?)row["projectCapital9Hours"];
					c.ProjectCapital10Title = (Convert.IsDBNull(row["projectCapital10Title"]))?string.Empty:(System.String)row["projectCapital10Title"];
					c.ProjectCapital10Hours = (Convert.IsDBNull(row["projectCapital10Hours"]))?0.0m:(System.Decimal?)row["projectCapital10Hours"];
					c.ProjectCapital11Title = (Convert.IsDBNull(row["projectCapital11Title"]))?string.Empty:(System.String)row["projectCapital11Title"];
					c.ProjectCapital11Hours = (Convert.IsDBNull(row["projectCapital11Hours"]))?0.0m:(System.Decimal?)row["projectCapital11Hours"];
					c.ProjectCapital12Title = (Convert.IsDBNull(row["projectCapital12Title"]))?string.Empty:(System.String)row["projectCapital12Title"];
					c.ProjectCapital12Hours = (Convert.IsDBNull(row["projectCapital12Hours"]))?0.0m:(System.Decimal?)row["projectCapital12Hours"];
					c.ProjectCapital13Title = (Convert.IsDBNull(row["projectCapital13Title"]))?string.Empty:(System.String)row["projectCapital13Title"];
					c.ProjectCapital13Hours = (Convert.IsDBNull(row["projectCapital13Hours"]))?0.0m:(System.Decimal?)row["projectCapital13Hours"];
					c.ProjectCapital14Title = (Convert.IsDBNull(row["projectCapital14Title"]))?string.Empty:(System.String)row["projectCapital14Title"];
					c.ProjectCapital14Hours = (Convert.IsDBNull(row["projectCapital14Hours"]))?0.0m:(System.Decimal?)row["projectCapital14Hours"];
					c.ProjectCapital15Title = (Convert.IsDBNull(row["projectCapital15Title"]))?string.Empty:(System.String)row["projectCapital15Title"];
					c.ProjectCapital15Hours = (Convert.IsDBNull(row["projectCapital15Hours"]))?0.0m:(System.Decimal?)row["projectCapital15Hours"];
					c.AheaRefineryWork = (Convert.IsDBNull(row["aheaRefineryWork"]))?0.0m:(System.Decimal)row["aheaRefineryWork"];
					c.AheaResidueWork = (Convert.IsDBNull(row["aheaResidueWork"]))?0.0m:(System.Decimal)row["aheaResidueWork"];
					c.AheaSmeltingWork = (Convert.IsDBNull(row["aheaSmeltingWork"]))?0.0m:(System.Decimal?)row["aheaSmeltingWork"];
					c.AheaPowerGenerationWork = (Convert.IsDBNull(row["aheaPowerGenerationWork"]))?0.0m:(System.Decimal?)row["aheaPowerGenerationWork"];
					c.AheaTotalManHours = (Convert.IsDBNull(row["aheaTotalManHours"]))?0.0m:(System.Decimal)row["aheaTotalManHours"];
					c.AheaPeakNopplSiteWeek = (Convert.IsDBNull(row["aheaPeakNopplSiteWeek"]))?(int)0:(System.Int32)row["aheaPeakNopplSiteWeek"];
					c.AheaAvgNopplSiteMonth = (Convert.IsDBNull(row["aheaAvgNopplSiteMonth"]))?(int)0:(System.Int32)row["aheaAvgNopplSiteMonth"];
					c.AheaNoEmpExcessMonth = (Convert.IsDBNull(row["aheaNoEmpExcessMonth"]))?(int)0:(System.Int32)row["aheaNoEmpExcessMonth"];
					c.IpFati = (Convert.IsDBNull(row["ipFATI"]))?(int)0:(System.Int32?)row["ipFATI"];
					c.IpMti = (Convert.IsDBNull(row["ipMTI"]))?(int)0:(System.Int32?)row["ipMTI"];
					c.IpRdi = (Convert.IsDBNull(row["ipRDI"]))?(int)0:(System.Int32?)row["ipRDI"];
					c.IpLti = (Convert.IsDBNull(row["ipLTI"]))?(int)0:(System.Int32?)row["ipLTI"];
					c.IpIfe = (Convert.IsDBNull(row["ipIFE"]))?(int)0:(System.Int32?)row["ipIFE"];
					c.EhspNoLwd = (Convert.IsDBNull(row["ehspNoLWD"]))?0.0m:(System.Decimal?)row["ehspNoLWD"];
					c.EhspNoRd = (Convert.IsDBNull(row["ehspNoRD"]))?0.0m:(System.Decimal?)row["ehspNoRD"];
					c.EhsCorrective = (Convert.IsDBNull(row["ehsCorrective"]))?(int)0:(System.Int32?)row["ehsCorrective"];
					c.JsaAudits = (Convert.IsDBNull(row["JSAAudits"]))?(int)0:(System.Int32?)row["JSAAudits"];
					c.IWsc = (Convert.IsDBNull(row["iWSC"]))?(int)0:(System.Int32?)row["iWSC"];
					c.ONoHswc = (Convert.IsDBNull(row["oNoHSWC"]))?(int)0:(System.Int32?)row["oNoHSWC"];
					c.ONoBsp = (Convert.IsDBNull(row["oNoBSP"]))?(int)0:(System.Int32?)row["oNoBSP"];
					c.QQas = (Convert.IsDBNull(row["qQAS"]))?0.0m:(System.Decimal?)row["qQAS"];
					c.QNoNci = (Convert.IsDBNull(row["qNoNCI"]))?(int)0:(System.Int32?)row["qNoNCI"];
					c.MTbmpm = (Convert.IsDBNull(row["mTbmpm"]))?(int)0:(System.Int32?)row["mTbmpm"];
					c.MAwcm = (Convert.IsDBNull(row["mAwcm"]))?(int)0:(System.Int32?)row["mAwcm"];
					c.MAmcm = (Convert.IsDBNull(row["mAmcm"]))?(int)0:(System.Int32?)row["mAmcm"];
					c.MFatality = (Convert.IsDBNull(row["mFatality"]))?(int)0:(System.Int32?)row["mFatality"];
					c.Training = (Convert.IsDBNull(row["Training"]))?(int)0:(System.Int32?)row["Training"];
					c.MtTolo = (Convert.IsDBNull(row["mtTolo"]))?(int)0:(System.Int32?)row["mtTolo"];
					c.MtFp = (Convert.IsDBNull(row["mtFp"]))?(int)0:(System.Int32?)row["mtFp"];
					c.MtElec = (Convert.IsDBNull(row["mtElec"]))?(int)0:(System.Int32?)row["mtElec"];
					c.MtMe = (Convert.IsDBNull(row["mtMe"]))?(int)0:(System.Int32?)row["mtMe"];
					c.MtCs = (Convert.IsDBNull(row["mtCs"]))?(int)0:(System.Int32?)row["mtCs"];
					c.MtCb = (Convert.IsDBNull(row["mtCb"]))?(int)0:(System.Int32?)row["mtCb"];
					c.MtErgo = (Convert.IsDBNull(row["mtErgo"]))?(int)0:(System.Int32?)row["mtErgo"];
					c.MtRa = (Convert.IsDBNull(row["mtRa"]))?(int)0:(System.Int32?)row["mtRa"];
					c.MtHs = (Convert.IsDBNull(row["mtHs"]))?(int)0:(System.Int32?)row["mtHs"];
					c.MtSp = (Convert.IsDBNull(row["mtSp"]))?(int)0:(System.Int32?)row["mtSp"];
					c.MtIf = (Convert.IsDBNull(row["mtIf"]))?(int)0:(System.Int32?)row["mtIf"];
					c.MtHp = (Convert.IsDBNull(row["mtHp"]))?(int)0:(System.Int32?)row["mtHp"];
					c.MtRp = (Convert.IsDBNull(row["mtRp"]))?(int)0:(System.Int32?)row["mtRp"];
					c.MtEnginfo81t = (Convert.IsDBNull(row["mtENGINFO81t"]))?(int)0:(System.Int32?)row["mtENGINFO81t"];
					c.MtOthers = (Convert.IsDBNull(row["mtOthers"]))?string.Empty:(System.String)row["mtOthers"];
					c.SafetyPlansSubmitted = (Convert.IsDBNull(row["SafetyPlansSubmitted"]))?false:(System.Boolean)row["SafetyPlansSubmitted"];
					c.EbiOnSite = (Convert.IsDBNull(row["EbiOnSite"]))?false:(System.Boolean?)row["EbiOnSite"];
					c.CompanySiteCategoryId = (Convert.IsDBNull(row["CompanySiteCategoryId"]))?(int)0:(System.Int32?)row["CompanySiteCategoryId"];
					c.CompanySiteCategoryName = (Convert.IsDBNull(row["CompanySiteCategoryName"]))?string.Empty:(System.String)row["CompanySiteCategoryName"];
					c.CompanySiteCategoryDesc = (Convert.IsDBNull(row["CompanySiteCategoryDesc"]))?string.Empty:(System.String)row["CompanySiteCategoryDesc"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;KpiCompanySiteCategory&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;KpiCompanySiteCategory&gt;"/></returns>
		protected VList<KpiCompanySiteCategory> Fill(IDataReader reader, VList<KpiCompanySiteCategory> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					KpiCompanySiteCategory entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<KpiCompanySiteCategory>("KpiCompanySiteCategory",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new KpiCompanySiteCategory();
					}
					
					entity.SuppressEntityEvents = true;

					entity.KpiId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.KpiId)];
					//entity.KpiId = (Convert.IsDBNull(reader["KpiId"]))?(int)0:(System.Int32)reader["KpiId"];
					entity.SiteId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.CreatedbyUserId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.CreatedbyUserId)];
					//entity.CreatedbyUserId = (Convert.IsDBNull(reader["CreatedbyUserId"]))?(int)0:(System.Int32)reader["CreatedbyUserId"];
					entity.ModifiedbyUserId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.ModifiedbyUserId)];
					//entity.ModifiedbyUserId = (Convert.IsDBNull(reader["ModifiedbyUserId"]))?(int)0:(System.Int32)reader["ModifiedbyUserId"];
					entity.DateAdded = (System.DateTime)reader[((int)KpiCompanySiteCategoryColumn.DateAdded)];
					//entity.DateAdded = (Convert.IsDBNull(reader["DateAdded"]))?DateTime.MinValue:(System.DateTime)reader["DateAdded"];
					entity.Datemodified = (System.DateTime)reader[((int)KpiCompanySiteCategoryColumn.Datemodified)];
					//entity.Datemodified = (Convert.IsDBNull(reader["Datemodified"]))?DateTime.MinValue:(System.DateTime)reader["Datemodified"];
					entity.CompanyId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.KpiDateTime = (System.DateTime)reader[((int)KpiCompanySiteCategoryColumn.KpiDateTime)];
					//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)reader["kpiDateTime"];
					entity.KpiGeneral = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiGeneral)];
					//entity.KpiGeneral = (Convert.IsDBNull(reader["kpiGeneral"]))?0.0m:(System.Decimal)reader["kpiGeneral"];
					entity.KpiCalcinerExpense = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiCalcinerExpense)];
					//entity.KpiCalcinerExpense = (Convert.IsDBNull(reader["kpiCalcinerExpense"]))?0.0m:(System.Decimal)reader["kpiCalcinerExpense"];
					entity.KpiCalcinerCapital = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiCalcinerCapital)];
					//entity.KpiCalcinerCapital = (Convert.IsDBNull(reader["kpiCalcinerCapital"]))?0.0m:(System.Decimal)reader["kpiCalcinerCapital"];
					entity.KpiResidue = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiResidue)];
					//entity.KpiResidue = (Convert.IsDBNull(reader["kpiResidue"]))?0.0m:(System.Decimal)reader["kpiResidue"];
					entity.ProjectCapital1Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital1Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital1Title)];
					//entity.ProjectCapital1Title = (Convert.IsDBNull(reader["projectCapital1Title"]))?string.Empty:(System.String)reader["projectCapital1Title"];
					entity.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital1Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital1Hours)];
					//entity.ProjectCapital1Hours = (Convert.IsDBNull(reader["projectCapital1Hours"]))?0.0m:(System.Decimal?)reader["projectCapital1Hours"];
					entity.ProjectCapital2Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital2Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital2Title)];
					//entity.ProjectCapital2Title = (Convert.IsDBNull(reader["projectCapital2Title"]))?string.Empty:(System.String)reader["projectCapital2Title"];
					entity.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital2Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital2Hours)];
					//entity.ProjectCapital2Hours = (Convert.IsDBNull(reader["projectCapital2Hours"]))?0.0m:(System.Decimal?)reader["projectCapital2Hours"];
					entity.ProjectCapital3Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital3Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital3Title)];
					//entity.ProjectCapital3Title = (Convert.IsDBNull(reader["projectCapital3Title"]))?string.Empty:(System.String)reader["projectCapital3Title"];
					entity.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital3Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital3Hours)];
					//entity.ProjectCapital3Hours = (Convert.IsDBNull(reader["projectCapital3Hours"]))?0.0m:(System.Decimal?)reader["projectCapital3Hours"];
					entity.ProjectCapital4Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital4Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital4Title)];
					//entity.ProjectCapital4Title = (Convert.IsDBNull(reader["projectCapital4Title"]))?string.Empty:(System.String)reader["projectCapital4Title"];
					entity.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital4Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital4Hours)];
					//entity.ProjectCapital4Hours = (Convert.IsDBNull(reader["projectCapital4Hours"]))?0.0m:(System.Decimal?)reader["projectCapital4Hours"];
					entity.ProjectCapital5Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital5Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital5Title)];
					//entity.ProjectCapital5Title = (Convert.IsDBNull(reader["projectCapital5Title"]))?string.Empty:(System.String)reader["projectCapital5Title"];
					entity.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital5Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital5Hours)];
					//entity.ProjectCapital5Hours = (Convert.IsDBNull(reader["projectCapital5Hours"]))?0.0m:(System.Decimal?)reader["projectCapital5Hours"];
					entity.ProjectCapital6Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital6Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital6Title)];
					//entity.ProjectCapital6Title = (Convert.IsDBNull(reader["projectCapital6Title"]))?string.Empty:(System.String)reader["projectCapital6Title"];
					entity.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital6Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital6Hours)];
					//entity.ProjectCapital6Hours = (Convert.IsDBNull(reader["projectCapital6Hours"]))?0.0m:(System.Decimal?)reader["projectCapital6Hours"];
					entity.ProjectCapital7Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital7Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital7Title)];
					//entity.ProjectCapital7Title = (Convert.IsDBNull(reader["projectCapital7Title"]))?string.Empty:(System.String)reader["projectCapital7Title"];
					entity.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital7Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital7Hours)];
					//entity.ProjectCapital7Hours = (Convert.IsDBNull(reader["projectCapital7Hours"]))?0.0m:(System.Decimal?)reader["projectCapital7Hours"];
					entity.ProjectCapital8Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital8Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital8Title)];
					//entity.ProjectCapital8Title = (Convert.IsDBNull(reader["projectCapital8Title"]))?string.Empty:(System.String)reader["projectCapital8Title"];
					entity.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital8Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital8Hours)];
					//entity.ProjectCapital8Hours = (Convert.IsDBNull(reader["projectCapital8Hours"]))?0.0m:(System.Decimal?)reader["projectCapital8Hours"];
					entity.ProjectCapital9Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital9Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital9Title)];
					//entity.ProjectCapital9Title = (Convert.IsDBNull(reader["projectCapital9Title"]))?string.Empty:(System.String)reader["projectCapital9Title"];
					entity.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital9Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital9Hours)];
					//entity.ProjectCapital9Hours = (Convert.IsDBNull(reader["projectCapital9Hours"]))?0.0m:(System.Decimal?)reader["projectCapital9Hours"];
					entity.ProjectCapital10Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital10Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital10Title)];
					//entity.ProjectCapital10Title = (Convert.IsDBNull(reader["projectCapital10Title"]))?string.Empty:(System.String)reader["projectCapital10Title"];
					entity.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital10Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital10Hours)];
					//entity.ProjectCapital10Hours = (Convert.IsDBNull(reader["projectCapital10Hours"]))?0.0m:(System.Decimal?)reader["projectCapital10Hours"];
					entity.ProjectCapital11Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital11Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital11Title)];
					//entity.ProjectCapital11Title = (Convert.IsDBNull(reader["projectCapital11Title"]))?string.Empty:(System.String)reader["projectCapital11Title"];
					entity.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital11Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital11Hours)];
					//entity.ProjectCapital11Hours = (Convert.IsDBNull(reader["projectCapital11Hours"]))?0.0m:(System.Decimal?)reader["projectCapital11Hours"];
					entity.ProjectCapital12Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital12Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital12Title)];
					//entity.ProjectCapital12Title = (Convert.IsDBNull(reader["projectCapital12Title"]))?string.Empty:(System.String)reader["projectCapital12Title"];
					entity.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital12Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital12Hours)];
					//entity.ProjectCapital12Hours = (Convert.IsDBNull(reader["projectCapital12Hours"]))?0.0m:(System.Decimal?)reader["projectCapital12Hours"];
					entity.ProjectCapital13Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital13Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital13Title)];
					//entity.ProjectCapital13Title = (Convert.IsDBNull(reader["projectCapital13Title"]))?string.Empty:(System.String)reader["projectCapital13Title"];
					entity.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital13Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital13Hours)];
					//entity.ProjectCapital13Hours = (Convert.IsDBNull(reader["projectCapital13Hours"]))?0.0m:(System.Decimal?)reader["projectCapital13Hours"];
					entity.ProjectCapital14Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital14Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital14Title)];
					//entity.ProjectCapital14Title = (Convert.IsDBNull(reader["projectCapital14Title"]))?string.Empty:(System.String)reader["projectCapital14Title"];
					entity.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital14Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital14Hours)];
					//entity.ProjectCapital14Hours = (Convert.IsDBNull(reader["projectCapital14Hours"]))?0.0m:(System.Decimal?)reader["projectCapital14Hours"];
					entity.ProjectCapital15Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital15Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital15Title)];
					//entity.ProjectCapital15Title = (Convert.IsDBNull(reader["projectCapital15Title"]))?string.Empty:(System.String)reader["projectCapital15Title"];
					entity.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital15Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital15Hours)];
					//entity.ProjectCapital15Hours = (Convert.IsDBNull(reader["projectCapital15Hours"]))?0.0m:(System.Decimal?)reader["projectCapital15Hours"];
					entity.AheaRefineryWork = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.AheaRefineryWork)];
					//entity.AheaRefineryWork = (Convert.IsDBNull(reader["aheaRefineryWork"]))?0.0m:(System.Decimal)reader["aheaRefineryWork"];
					entity.AheaResidueWork = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.AheaResidueWork)];
					//entity.AheaResidueWork = (Convert.IsDBNull(reader["aheaResidueWork"]))?0.0m:(System.Decimal)reader["aheaResidueWork"];
					entity.AheaSmeltingWork = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.AheaSmeltingWork)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.AheaSmeltingWork)];
					//entity.AheaSmeltingWork = (Convert.IsDBNull(reader["aheaSmeltingWork"]))?0.0m:(System.Decimal?)reader["aheaSmeltingWork"];
					entity.AheaPowerGenerationWork = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.AheaPowerGenerationWork)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.AheaPowerGenerationWork)];
					//entity.AheaPowerGenerationWork = (Convert.IsDBNull(reader["aheaPowerGenerationWork"]))?0.0m:(System.Decimal?)reader["aheaPowerGenerationWork"];
					entity.AheaTotalManHours = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.AheaTotalManHours)];
					//entity.AheaTotalManHours = (Convert.IsDBNull(reader["aheaTotalManHours"]))?0.0m:(System.Decimal)reader["aheaTotalManHours"];
					entity.AheaPeakNopplSiteWeek = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.AheaPeakNopplSiteWeek)];
					//entity.AheaPeakNopplSiteWeek = (Convert.IsDBNull(reader["aheaPeakNopplSiteWeek"]))?(int)0:(System.Int32)reader["aheaPeakNopplSiteWeek"];
					entity.AheaAvgNopplSiteMonth = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.AheaAvgNopplSiteMonth)];
					//entity.AheaAvgNopplSiteMonth = (Convert.IsDBNull(reader["aheaAvgNopplSiteMonth"]))?(int)0:(System.Int32)reader["aheaAvgNopplSiteMonth"];
					entity.AheaNoEmpExcessMonth = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.AheaNoEmpExcessMonth)];
					//entity.AheaNoEmpExcessMonth = (Convert.IsDBNull(reader["aheaNoEmpExcessMonth"]))?(int)0:(System.Int32)reader["aheaNoEmpExcessMonth"];
					entity.IpFati = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpFati)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpFati)];
					//entity.IpFati = (Convert.IsDBNull(reader["ipFATI"]))?(int)0:(System.Int32?)reader["ipFATI"];
					entity.IpMti = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpMti)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpMti)];
					//entity.IpMti = (Convert.IsDBNull(reader["ipMTI"]))?(int)0:(System.Int32?)reader["ipMTI"];
					entity.IpRdi = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpRdi)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpRdi)];
					//entity.IpRdi = (Convert.IsDBNull(reader["ipRDI"]))?(int)0:(System.Int32?)reader["ipRDI"];
					entity.IpLti = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpLti)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpLti)];
					//entity.IpLti = (Convert.IsDBNull(reader["ipLTI"]))?(int)0:(System.Int32?)reader["ipLTI"];
					entity.IpIfe = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpIfe)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpIfe)];
					//entity.IpIfe = (Convert.IsDBNull(reader["ipIFE"]))?(int)0:(System.Int32?)reader["ipIFE"];
					entity.EhspNoLwd = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EhspNoLwd)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.EhspNoLwd)];
					//entity.EhspNoLwd = (Convert.IsDBNull(reader["ehspNoLWD"]))?0.0m:(System.Decimal?)reader["ehspNoLWD"];
					entity.EhspNoRd = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EhspNoRd)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.EhspNoRd)];
					//entity.EhspNoRd = (Convert.IsDBNull(reader["ehspNoRD"]))?0.0m:(System.Decimal?)reader["ehspNoRD"];
					entity.EhsCorrective = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EhsCorrective)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.EhsCorrective)];
					//entity.EhsCorrective = (Convert.IsDBNull(reader["ehsCorrective"]))?(int)0:(System.Int32?)reader["ehsCorrective"];
					entity.JsaAudits = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.JsaAudits)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.JsaAudits)];
					//entity.JsaAudits = (Convert.IsDBNull(reader["JSAAudits"]))?(int)0:(System.Int32?)reader["JSAAudits"];
					entity.IWsc = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IWsc)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IWsc)];
					//entity.IWsc = (Convert.IsDBNull(reader["iWSC"]))?(int)0:(System.Int32?)reader["iWSC"];
					entity.ONoHswc = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ONoHswc)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.ONoHswc)];
					//entity.ONoHswc = (Convert.IsDBNull(reader["oNoHSWC"]))?(int)0:(System.Int32?)reader["oNoHSWC"];
					entity.ONoBsp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ONoBsp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.ONoBsp)];
					//entity.ONoBsp = (Convert.IsDBNull(reader["oNoBSP"]))?(int)0:(System.Int32?)reader["oNoBSP"];
					entity.QQas = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.QQas)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.QQas)];
					//entity.QQas = (Convert.IsDBNull(reader["qQAS"]))?0.0m:(System.Decimal?)reader["qQAS"];
					entity.QNoNci = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.QNoNci)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.QNoNci)];
					//entity.QNoNci = (Convert.IsDBNull(reader["qNoNCI"]))?(int)0:(System.Int32?)reader["qNoNCI"];
					entity.MTbmpm = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MTbmpm)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MTbmpm)];
					//entity.MTbmpm = (Convert.IsDBNull(reader["mTbmpm"]))?(int)0:(System.Int32?)reader["mTbmpm"];
					entity.MAwcm = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MAwcm)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MAwcm)];
					//entity.MAwcm = (Convert.IsDBNull(reader["mAwcm"]))?(int)0:(System.Int32?)reader["mAwcm"];
					entity.MAmcm = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MAmcm)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MAmcm)];
					//entity.MAmcm = (Convert.IsDBNull(reader["mAmcm"]))?(int)0:(System.Int32?)reader["mAmcm"];
					entity.MFatality = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MFatality)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MFatality)];
					//entity.MFatality = (Convert.IsDBNull(reader["mFatality"]))?(int)0:(System.Int32?)reader["mFatality"];
					entity.Training = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.Training)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.Training)];
					//entity.Training = (Convert.IsDBNull(reader["Training"]))?(int)0:(System.Int32?)reader["Training"];
					entity.MtTolo = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtTolo)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtTolo)];
					//entity.MtTolo = (Convert.IsDBNull(reader["mtTolo"]))?(int)0:(System.Int32?)reader["mtTolo"];
					entity.MtFp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtFp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtFp)];
					//entity.MtFp = (Convert.IsDBNull(reader["mtFp"]))?(int)0:(System.Int32?)reader["mtFp"];
					entity.MtElec = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtElec)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtElec)];
					//entity.MtElec = (Convert.IsDBNull(reader["mtElec"]))?(int)0:(System.Int32?)reader["mtElec"];
					entity.MtMe = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtMe)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtMe)];
					//entity.MtMe = (Convert.IsDBNull(reader["mtMe"]))?(int)0:(System.Int32?)reader["mtMe"];
					entity.MtCs = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtCs)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtCs)];
					//entity.MtCs = (Convert.IsDBNull(reader["mtCs"]))?(int)0:(System.Int32?)reader["mtCs"];
					entity.MtCb = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtCb)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtCb)];
					//entity.MtCb = (Convert.IsDBNull(reader["mtCb"]))?(int)0:(System.Int32?)reader["mtCb"];
					entity.MtErgo = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtErgo)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtErgo)];
					//entity.MtErgo = (Convert.IsDBNull(reader["mtErgo"]))?(int)0:(System.Int32?)reader["mtErgo"];
					entity.MtRa = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtRa)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtRa)];
					//entity.MtRa = (Convert.IsDBNull(reader["mtRa"]))?(int)0:(System.Int32?)reader["mtRa"];
					entity.MtHs = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtHs)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtHs)];
					//entity.MtHs = (Convert.IsDBNull(reader["mtHs"]))?(int)0:(System.Int32?)reader["mtHs"];
					entity.MtSp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtSp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtSp)];
					//entity.MtSp = (Convert.IsDBNull(reader["mtSp"]))?(int)0:(System.Int32?)reader["mtSp"];
					entity.MtIf = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtIf)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtIf)];
					//entity.MtIf = (Convert.IsDBNull(reader["mtIf"]))?(int)0:(System.Int32?)reader["mtIf"];
					entity.MtHp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtHp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtHp)];
					//entity.MtHp = (Convert.IsDBNull(reader["mtHp"]))?(int)0:(System.Int32?)reader["mtHp"];
					entity.MtRp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtRp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtRp)];
					//entity.MtRp = (Convert.IsDBNull(reader["mtRp"]))?(int)0:(System.Int32?)reader["mtRp"];
					entity.MtEnginfo81t = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtEnginfo81t)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtEnginfo81t)];
					//entity.MtEnginfo81t = (Convert.IsDBNull(reader["mtENGINFO81t"]))?(int)0:(System.Int32?)reader["mtENGINFO81t"];
					entity.MtOthers = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtOthers)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.MtOthers)];
					//entity.MtOthers = (Convert.IsDBNull(reader["mtOthers"]))?string.Empty:(System.String)reader["mtOthers"];
					entity.SafetyPlansSubmitted = (System.Boolean)reader[((int)KpiCompanySiteCategoryColumn.SafetyPlansSubmitted)];
					//entity.SafetyPlansSubmitted = (Convert.IsDBNull(reader["SafetyPlansSubmitted"]))?false:(System.Boolean)reader["SafetyPlansSubmitted"];
					entity.EbiOnSite = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EbiOnSite)))?null:(System.Boolean?)reader[((int)KpiCompanySiteCategoryColumn.EbiOnSite)];
					//entity.EbiOnSite = (Convert.IsDBNull(reader["EbiOnSite"]))?false:(System.Boolean?)reader["EbiOnSite"];
					entity.CompanySiteCategoryId = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryId)];
					//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
					//Error in casting change by Sayani 
                    entity.CompanySiteCategoryName = (System.String)reader[((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryName)];
                    //End
					//entity.CompanySiteCategoryName = (Convert.IsDBNull(reader["CompanySiteCategoryName"]))?string.Empty:(System.String)reader["CompanySiteCategoryName"];
					entity.CompanySiteCategoryDesc = (System.String)reader[((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryDesc)];
					//entity.CompanySiteCategoryDesc = (Convert.IsDBNull(reader["CompanySiteCategoryDesc"]))?string.Empty:(System.String)reader["CompanySiteCategoryDesc"];
                    entity.IpRN = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpRN))) ? null : (System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpRN)];

					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="KpiCompanySiteCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiCompanySiteCategory"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, KpiCompanySiteCategory entity)
		{
			reader.Read();
			entity.KpiId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.KpiId)];
			//entity.KpiId = (Convert.IsDBNull(reader["KpiId"]))?(int)0:(System.Int32)reader["KpiId"];
			entity.SiteId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.CreatedbyUserId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.CreatedbyUserId)];
			//entity.CreatedbyUserId = (Convert.IsDBNull(reader["CreatedbyUserId"]))?(int)0:(System.Int32)reader["CreatedbyUserId"];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.ModifiedbyUserId)];
			//entity.ModifiedbyUserId = (Convert.IsDBNull(reader["ModifiedbyUserId"]))?(int)0:(System.Int32)reader["ModifiedbyUserId"];
			entity.DateAdded = (System.DateTime)reader[((int)KpiCompanySiteCategoryColumn.DateAdded)];
			//entity.DateAdded = (Convert.IsDBNull(reader["DateAdded"]))?DateTime.MinValue:(System.DateTime)reader["DateAdded"];
			entity.Datemodified = (System.DateTime)reader[((int)KpiCompanySiteCategoryColumn.Datemodified)];
			//entity.Datemodified = (Convert.IsDBNull(reader["Datemodified"]))?DateTime.MinValue:(System.DateTime)reader["Datemodified"];
			entity.CompanyId = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.KpiDateTime = (System.DateTime)reader[((int)KpiCompanySiteCategoryColumn.KpiDateTime)];
			//entity.KpiDateTime = (Convert.IsDBNull(reader["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)reader["kpiDateTime"];
			entity.KpiGeneral = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiGeneral)];
			//entity.KpiGeneral = (Convert.IsDBNull(reader["kpiGeneral"]))?0.0m:(System.Decimal)reader["kpiGeneral"];
			entity.KpiCalcinerExpense = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiCalcinerExpense)];
			//entity.KpiCalcinerExpense = (Convert.IsDBNull(reader["kpiCalcinerExpense"]))?0.0m:(System.Decimal)reader["kpiCalcinerExpense"];
			entity.KpiCalcinerCapital = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiCalcinerCapital)];
			//entity.KpiCalcinerCapital = (Convert.IsDBNull(reader["kpiCalcinerCapital"]))?0.0m:(System.Decimal)reader["kpiCalcinerCapital"];
			entity.KpiResidue = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.KpiResidue)];
			//entity.KpiResidue = (Convert.IsDBNull(reader["kpiResidue"]))?0.0m:(System.Decimal)reader["kpiResidue"];
			entity.ProjectCapital1Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital1Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital1Title)];
			//entity.ProjectCapital1Title = (Convert.IsDBNull(reader["projectCapital1Title"]))?string.Empty:(System.String)reader["projectCapital1Title"];
			entity.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital1Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital1Hours)];
			//entity.ProjectCapital1Hours = (Convert.IsDBNull(reader["projectCapital1Hours"]))?0.0m:(System.Decimal?)reader["projectCapital1Hours"];
			entity.ProjectCapital2Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital2Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital2Title)];
			//entity.ProjectCapital2Title = (Convert.IsDBNull(reader["projectCapital2Title"]))?string.Empty:(System.String)reader["projectCapital2Title"];
			entity.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital2Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital2Hours)];
			//entity.ProjectCapital2Hours = (Convert.IsDBNull(reader["projectCapital2Hours"]))?0.0m:(System.Decimal?)reader["projectCapital2Hours"];
			entity.ProjectCapital3Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital3Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital3Title)];
			//entity.ProjectCapital3Title = (Convert.IsDBNull(reader["projectCapital3Title"]))?string.Empty:(System.String)reader["projectCapital3Title"];
			entity.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital3Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital3Hours)];
			//entity.ProjectCapital3Hours = (Convert.IsDBNull(reader["projectCapital3Hours"]))?0.0m:(System.Decimal?)reader["projectCapital3Hours"];
			entity.ProjectCapital4Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital4Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital4Title)];
			//entity.ProjectCapital4Title = (Convert.IsDBNull(reader["projectCapital4Title"]))?string.Empty:(System.String)reader["projectCapital4Title"];
			entity.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital4Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital4Hours)];
			//entity.ProjectCapital4Hours = (Convert.IsDBNull(reader["projectCapital4Hours"]))?0.0m:(System.Decimal?)reader["projectCapital4Hours"];
			entity.ProjectCapital5Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital5Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital5Title)];
			//entity.ProjectCapital5Title = (Convert.IsDBNull(reader["projectCapital5Title"]))?string.Empty:(System.String)reader["projectCapital5Title"];
			entity.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital5Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital5Hours)];
			//entity.ProjectCapital5Hours = (Convert.IsDBNull(reader["projectCapital5Hours"]))?0.0m:(System.Decimal?)reader["projectCapital5Hours"];
			entity.ProjectCapital6Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital6Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital6Title)];
			//entity.ProjectCapital6Title = (Convert.IsDBNull(reader["projectCapital6Title"]))?string.Empty:(System.String)reader["projectCapital6Title"];
			entity.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital6Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital6Hours)];
			//entity.ProjectCapital6Hours = (Convert.IsDBNull(reader["projectCapital6Hours"]))?0.0m:(System.Decimal?)reader["projectCapital6Hours"];
			entity.ProjectCapital7Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital7Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital7Title)];
			//entity.ProjectCapital7Title = (Convert.IsDBNull(reader["projectCapital7Title"]))?string.Empty:(System.String)reader["projectCapital7Title"];
			entity.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital7Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital7Hours)];
			//entity.ProjectCapital7Hours = (Convert.IsDBNull(reader["projectCapital7Hours"]))?0.0m:(System.Decimal?)reader["projectCapital7Hours"];
			entity.ProjectCapital8Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital8Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital8Title)];
			//entity.ProjectCapital8Title = (Convert.IsDBNull(reader["projectCapital8Title"]))?string.Empty:(System.String)reader["projectCapital8Title"];
			entity.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital8Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital8Hours)];
			//entity.ProjectCapital8Hours = (Convert.IsDBNull(reader["projectCapital8Hours"]))?0.0m:(System.Decimal?)reader["projectCapital8Hours"];
			entity.ProjectCapital9Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital9Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital9Title)];
			//entity.ProjectCapital9Title = (Convert.IsDBNull(reader["projectCapital9Title"]))?string.Empty:(System.String)reader["projectCapital9Title"];
			entity.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital9Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital9Hours)];
			//entity.ProjectCapital9Hours = (Convert.IsDBNull(reader["projectCapital9Hours"]))?0.0m:(System.Decimal?)reader["projectCapital9Hours"];
			entity.ProjectCapital10Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital10Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital10Title)];
			//entity.ProjectCapital10Title = (Convert.IsDBNull(reader["projectCapital10Title"]))?string.Empty:(System.String)reader["projectCapital10Title"];
			entity.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital10Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital10Hours)];
			//entity.ProjectCapital10Hours = (Convert.IsDBNull(reader["projectCapital10Hours"]))?0.0m:(System.Decimal?)reader["projectCapital10Hours"];
			entity.ProjectCapital11Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital11Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital11Title)];
			//entity.ProjectCapital11Title = (Convert.IsDBNull(reader["projectCapital11Title"]))?string.Empty:(System.String)reader["projectCapital11Title"];
			entity.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital11Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital11Hours)];
			//entity.ProjectCapital11Hours = (Convert.IsDBNull(reader["projectCapital11Hours"]))?0.0m:(System.Decimal?)reader["projectCapital11Hours"];
			entity.ProjectCapital12Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital12Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital12Title)];
			//entity.ProjectCapital12Title = (Convert.IsDBNull(reader["projectCapital12Title"]))?string.Empty:(System.String)reader["projectCapital12Title"];
			entity.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital12Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital12Hours)];
			//entity.ProjectCapital12Hours = (Convert.IsDBNull(reader["projectCapital12Hours"]))?0.0m:(System.Decimal?)reader["projectCapital12Hours"];
			entity.ProjectCapital13Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital13Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital13Title)];
			//entity.ProjectCapital13Title = (Convert.IsDBNull(reader["projectCapital13Title"]))?string.Empty:(System.String)reader["projectCapital13Title"];
			entity.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital13Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital13Hours)];
			//entity.ProjectCapital13Hours = (Convert.IsDBNull(reader["projectCapital13Hours"]))?0.0m:(System.Decimal?)reader["projectCapital13Hours"];
			entity.ProjectCapital14Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital14Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital14Title)];
			//entity.ProjectCapital14Title = (Convert.IsDBNull(reader["projectCapital14Title"]))?string.Empty:(System.String)reader["projectCapital14Title"];
			entity.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital14Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital14Hours)];
			//entity.ProjectCapital14Hours = (Convert.IsDBNull(reader["projectCapital14Hours"]))?0.0m:(System.Decimal?)reader["projectCapital14Hours"];
			entity.ProjectCapital15Title = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital15Title)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital15Title)];
			//entity.ProjectCapital15Title = (Convert.IsDBNull(reader["projectCapital15Title"]))?string.Empty:(System.String)reader["projectCapital15Title"];
			entity.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ProjectCapital15Hours)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.ProjectCapital15Hours)];
			//entity.ProjectCapital15Hours = (Convert.IsDBNull(reader["projectCapital15Hours"]))?0.0m:(System.Decimal?)reader["projectCapital15Hours"];
			entity.AheaRefineryWork = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.AheaRefineryWork)];
			//entity.AheaRefineryWork = (Convert.IsDBNull(reader["aheaRefineryWork"]))?0.0m:(System.Decimal)reader["aheaRefineryWork"];
			entity.AheaResidueWork = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.AheaResidueWork)];
			//entity.AheaResidueWork = (Convert.IsDBNull(reader["aheaResidueWork"]))?0.0m:(System.Decimal)reader["aheaResidueWork"];
			entity.AheaSmeltingWork = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.AheaSmeltingWork)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.AheaSmeltingWork)];
			//entity.AheaSmeltingWork = (Convert.IsDBNull(reader["aheaSmeltingWork"]))?0.0m:(System.Decimal?)reader["aheaSmeltingWork"];
			entity.AheaPowerGenerationWork = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.AheaPowerGenerationWork)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.AheaPowerGenerationWork)];
			//entity.AheaPowerGenerationWork = (Convert.IsDBNull(reader["aheaPowerGenerationWork"]))?0.0m:(System.Decimal?)reader["aheaPowerGenerationWork"];
			entity.AheaTotalManHours = (System.Decimal)reader[((int)KpiCompanySiteCategoryColumn.AheaTotalManHours)];
			//entity.AheaTotalManHours = (Convert.IsDBNull(reader["aheaTotalManHours"]))?0.0m:(System.Decimal)reader["aheaTotalManHours"];
			entity.AheaPeakNopplSiteWeek = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.AheaPeakNopplSiteWeek)];
			//entity.AheaPeakNopplSiteWeek = (Convert.IsDBNull(reader["aheaPeakNopplSiteWeek"]))?(int)0:(System.Int32)reader["aheaPeakNopplSiteWeek"];
			entity.AheaAvgNopplSiteMonth = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.AheaAvgNopplSiteMonth)];
			//entity.AheaAvgNopplSiteMonth = (Convert.IsDBNull(reader["aheaAvgNopplSiteMonth"]))?(int)0:(System.Int32)reader["aheaAvgNopplSiteMonth"];
			entity.AheaNoEmpExcessMonth = (System.Int32)reader[((int)KpiCompanySiteCategoryColumn.AheaNoEmpExcessMonth)];
			//entity.AheaNoEmpExcessMonth = (Convert.IsDBNull(reader["aheaNoEmpExcessMonth"]))?(int)0:(System.Int32)reader["aheaNoEmpExcessMonth"];
			entity.IpFati = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpFati)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpFati)];
			//entity.IpFati = (Convert.IsDBNull(reader["ipFATI"]))?(int)0:(System.Int32?)reader["ipFATI"];
			entity.IpMti = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpMti)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpMti)];
			//entity.IpMti = (Convert.IsDBNull(reader["ipMTI"]))?(int)0:(System.Int32?)reader["ipMTI"];
			entity.IpRdi = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpRdi)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpRdi)];
			//entity.IpRdi = (Convert.IsDBNull(reader["ipRDI"]))?(int)0:(System.Int32?)reader["ipRDI"];
			entity.IpLti = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpLti)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpLti)];
			//entity.IpLti = (Convert.IsDBNull(reader["ipLTI"]))?(int)0:(System.Int32?)reader["ipLTI"];
			entity.IpIfe = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpIfe)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpIfe)];
			//entity.IpIfe = (Convert.IsDBNull(reader["ipIFE"]))?(int)0:(System.Int32?)reader["ipIFE"];
			entity.EhspNoLwd = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EhspNoLwd)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.EhspNoLwd)];
			//entity.EhspNoLwd = (Convert.IsDBNull(reader["ehspNoLWD"]))?0.0m:(System.Decimal?)reader["ehspNoLWD"];
			entity.EhspNoRd = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EhspNoRd)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.EhspNoRd)];
			//entity.EhspNoRd = (Convert.IsDBNull(reader["ehspNoRD"]))?0.0m:(System.Decimal?)reader["ehspNoRD"];
			entity.EhsCorrective = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EhsCorrective)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.EhsCorrective)];
			//entity.EhsCorrective = (Convert.IsDBNull(reader["ehsCorrective"]))?(int)0:(System.Int32?)reader["ehsCorrective"];
			entity.JsaAudits = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.JsaAudits)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.JsaAudits)];
			//entity.JsaAudits = (Convert.IsDBNull(reader["JSAAudits"]))?(int)0:(System.Int32?)reader["JSAAudits"];
			entity.IWsc = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IWsc)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IWsc)];
			//entity.IWsc = (Convert.IsDBNull(reader["iWSC"]))?(int)0:(System.Int32?)reader["iWSC"];
			entity.ONoHswc = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ONoHswc)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.ONoHswc)];
			//entity.ONoHswc = (Convert.IsDBNull(reader["oNoHSWC"]))?(int)0:(System.Int32?)reader["oNoHSWC"];
			entity.ONoBsp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.ONoBsp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.ONoBsp)];
			//entity.ONoBsp = (Convert.IsDBNull(reader["oNoBSP"]))?(int)0:(System.Int32?)reader["oNoBSP"];
			entity.QQas = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.QQas)))?null:(System.Decimal?)reader[((int)KpiCompanySiteCategoryColumn.QQas)];
			//entity.QQas = (Convert.IsDBNull(reader["qQAS"]))?0.0m:(System.Decimal?)reader["qQAS"];
			entity.QNoNci = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.QNoNci)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.QNoNci)];
			//entity.QNoNci = (Convert.IsDBNull(reader["qNoNCI"]))?(int)0:(System.Int32?)reader["qNoNCI"];
			entity.MTbmpm = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MTbmpm)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MTbmpm)];
			//entity.MTbmpm = (Convert.IsDBNull(reader["mTbmpm"]))?(int)0:(System.Int32?)reader["mTbmpm"];
			entity.MAwcm = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MAwcm)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MAwcm)];
			//entity.MAwcm = (Convert.IsDBNull(reader["mAwcm"]))?(int)0:(System.Int32?)reader["mAwcm"];
			entity.MAmcm = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MAmcm)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MAmcm)];
			//entity.MAmcm = (Convert.IsDBNull(reader["mAmcm"]))?(int)0:(System.Int32?)reader["mAmcm"];
			entity.MFatality = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MFatality)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MFatality)];
			//entity.MFatality = (Convert.IsDBNull(reader["mFatality"]))?(int)0:(System.Int32?)reader["mFatality"];
			entity.Training = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.Training)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.Training)];
			//entity.Training = (Convert.IsDBNull(reader["Training"]))?(int)0:(System.Int32?)reader["Training"];
			entity.MtTolo = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtTolo)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtTolo)];
			//entity.MtTolo = (Convert.IsDBNull(reader["mtTolo"]))?(int)0:(System.Int32?)reader["mtTolo"];
			entity.MtFp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtFp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtFp)];
			//entity.MtFp = (Convert.IsDBNull(reader["mtFp"]))?(int)0:(System.Int32?)reader["mtFp"];
			entity.MtElec = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtElec)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtElec)];
			//entity.MtElec = (Convert.IsDBNull(reader["mtElec"]))?(int)0:(System.Int32?)reader["mtElec"];
			entity.MtMe = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtMe)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtMe)];
			//entity.MtMe = (Convert.IsDBNull(reader["mtMe"]))?(int)0:(System.Int32?)reader["mtMe"];
			entity.MtCs = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtCs)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtCs)];
			//entity.MtCs = (Convert.IsDBNull(reader["mtCs"]))?(int)0:(System.Int32?)reader["mtCs"];
			entity.MtCb = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtCb)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtCb)];
			//entity.MtCb = (Convert.IsDBNull(reader["mtCb"]))?(int)0:(System.Int32?)reader["mtCb"];
			entity.MtErgo = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtErgo)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtErgo)];
			//entity.MtErgo = (Convert.IsDBNull(reader["mtErgo"]))?(int)0:(System.Int32?)reader["mtErgo"];
			entity.MtRa = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtRa)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtRa)];
			//entity.MtRa = (Convert.IsDBNull(reader["mtRa"]))?(int)0:(System.Int32?)reader["mtRa"];
			entity.MtHs = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtHs)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtHs)];
			//entity.MtHs = (Convert.IsDBNull(reader["mtHs"]))?(int)0:(System.Int32?)reader["mtHs"];
			entity.MtSp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtSp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtSp)];
			//entity.MtSp = (Convert.IsDBNull(reader["mtSp"]))?(int)0:(System.Int32?)reader["mtSp"];
			entity.MtIf = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtIf)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtIf)];
			//entity.MtIf = (Convert.IsDBNull(reader["mtIf"]))?(int)0:(System.Int32?)reader["mtIf"];
			entity.MtHp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtHp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtHp)];
			//entity.MtHp = (Convert.IsDBNull(reader["mtHp"]))?(int)0:(System.Int32?)reader["mtHp"];
			entity.MtRp = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtRp)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtRp)];
			//entity.MtRp = (Convert.IsDBNull(reader["mtRp"]))?(int)0:(System.Int32?)reader["mtRp"];
			entity.MtEnginfo81t = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtEnginfo81t)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.MtEnginfo81t)];
			//entity.MtEnginfo81t = (Convert.IsDBNull(reader["mtENGINFO81t"]))?(int)0:(System.Int32?)reader["mtENGINFO81t"];
			entity.MtOthers = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.MtOthers)))?null:(System.String)reader[((int)KpiCompanySiteCategoryColumn.MtOthers)];
			//entity.MtOthers = (Convert.IsDBNull(reader["mtOthers"]))?string.Empty:(System.String)reader["mtOthers"];
			entity.SafetyPlansSubmitted = (System.Boolean)reader[((int)KpiCompanySiteCategoryColumn.SafetyPlansSubmitted)];
			//entity.SafetyPlansSubmitted = (Convert.IsDBNull(reader["SafetyPlansSubmitted"]))?false:(System.Boolean)reader["SafetyPlansSubmitted"];
			entity.EbiOnSite = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.EbiOnSite)))?null:(System.Boolean?)reader[((int)KpiCompanySiteCategoryColumn.EbiOnSite)];
			//entity.EbiOnSite = (Convert.IsDBNull(reader["EbiOnSite"]))?false:(System.Boolean?)reader["EbiOnSite"];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryId)];
			//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
			entity.CompanySiteCategoryName = (System.String)reader[((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryName)];
			//entity.CompanySiteCategoryName = (Convert.IsDBNull(reader["CompanySiteCategoryName"]))?string.Empty:(System.String)reader["CompanySiteCategoryName"];
			entity.CompanySiteCategoryDesc = (System.String)reader[((int)KpiCompanySiteCategoryColumn.CompanySiteCategoryDesc)];
			//entity.CompanySiteCategoryDesc = (Convert.IsDBNull(reader["CompanySiteCategoryDesc"]))?string.Empty:(System.String)reader["CompanySiteCategoryDesc"];
            entity.IpRN = (reader.IsDBNull(((int)KpiCompanySiteCategoryColumn.IpRN))) ? null : (System.Int32?)reader[((int)KpiCompanySiteCategoryColumn.IpRN)];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="KpiCompanySiteCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiCompanySiteCategory"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, KpiCompanySiteCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.KpiId = (Convert.IsDBNull(dataRow["KpiId"]))?(int)0:(System.Int32)dataRow["KpiId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.CreatedbyUserId = (Convert.IsDBNull(dataRow["CreatedbyUserId"]))?(int)0:(System.Int32)dataRow["CreatedbyUserId"];
			entity.ModifiedbyUserId = (Convert.IsDBNull(dataRow["ModifiedbyUserId"]))?(int)0:(System.Int32)dataRow["ModifiedbyUserId"];
			entity.DateAdded = (Convert.IsDBNull(dataRow["DateAdded"]))?DateTime.MinValue:(System.DateTime)dataRow["DateAdded"];
			entity.Datemodified = (Convert.IsDBNull(dataRow["Datemodified"]))?DateTime.MinValue:(System.DateTime)dataRow["Datemodified"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.KpiDateTime = (Convert.IsDBNull(dataRow["kpiDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["kpiDateTime"];
			entity.KpiGeneral = (Convert.IsDBNull(dataRow["kpiGeneral"]))?0.0m:(System.Decimal)dataRow["kpiGeneral"];
			entity.KpiCalcinerExpense = (Convert.IsDBNull(dataRow["kpiCalcinerExpense"]))?0.0m:(System.Decimal)dataRow["kpiCalcinerExpense"];
			entity.KpiCalcinerCapital = (Convert.IsDBNull(dataRow["kpiCalcinerCapital"]))?0.0m:(System.Decimal)dataRow["kpiCalcinerCapital"];
			entity.KpiResidue = (Convert.IsDBNull(dataRow["kpiResidue"]))?0.0m:(System.Decimal)dataRow["kpiResidue"];
			entity.ProjectCapital1Title = (Convert.IsDBNull(dataRow["projectCapital1Title"]))?string.Empty:(System.String)dataRow["projectCapital1Title"];
			entity.ProjectCapital1Hours = (Convert.IsDBNull(dataRow["projectCapital1Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital1Hours"];
			entity.ProjectCapital2Title = (Convert.IsDBNull(dataRow["projectCapital2Title"]))?string.Empty:(System.String)dataRow["projectCapital2Title"];
			entity.ProjectCapital2Hours = (Convert.IsDBNull(dataRow["projectCapital2Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital2Hours"];
			entity.ProjectCapital3Title = (Convert.IsDBNull(dataRow["projectCapital3Title"]))?string.Empty:(System.String)dataRow["projectCapital3Title"];
			entity.ProjectCapital3Hours = (Convert.IsDBNull(dataRow["projectCapital3Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital3Hours"];
			entity.ProjectCapital4Title = (Convert.IsDBNull(dataRow["projectCapital4Title"]))?string.Empty:(System.String)dataRow["projectCapital4Title"];
			entity.ProjectCapital4Hours = (Convert.IsDBNull(dataRow["projectCapital4Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital4Hours"];
			entity.ProjectCapital5Title = (Convert.IsDBNull(dataRow["projectCapital5Title"]))?string.Empty:(System.String)dataRow["projectCapital5Title"];
			entity.ProjectCapital5Hours = (Convert.IsDBNull(dataRow["projectCapital5Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital5Hours"];
			entity.ProjectCapital6Title = (Convert.IsDBNull(dataRow["projectCapital6Title"]))?string.Empty:(System.String)dataRow["projectCapital6Title"];
			entity.ProjectCapital6Hours = (Convert.IsDBNull(dataRow["projectCapital6Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital6Hours"];
			entity.ProjectCapital7Title = (Convert.IsDBNull(dataRow["projectCapital7Title"]))?string.Empty:(System.String)dataRow["projectCapital7Title"];
			entity.ProjectCapital7Hours = (Convert.IsDBNull(dataRow["projectCapital7Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital7Hours"];
			entity.ProjectCapital8Title = (Convert.IsDBNull(dataRow["projectCapital8Title"]))?string.Empty:(System.String)dataRow["projectCapital8Title"];
			entity.ProjectCapital8Hours = (Convert.IsDBNull(dataRow["projectCapital8Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital8Hours"];
			entity.ProjectCapital9Title = (Convert.IsDBNull(dataRow["projectCapital9Title"]))?string.Empty:(System.String)dataRow["projectCapital9Title"];
			entity.ProjectCapital9Hours = (Convert.IsDBNull(dataRow["projectCapital9Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital9Hours"];
			entity.ProjectCapital10Title = (Convert.IsDBNull(dataRow["projectCapital10Title"]))?string.Empty:(System.String)dataRow["projectCapital10Title"];
			entity.ProjectCapital10Hours = (Convert.IsDBNull(dataRow["projectCapital10Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital10Hours"];
			entity.ProjectCapital11Title = (Convert.IsDBNull(dataRow["projectCapital11Title"]))?string.Empty:(System.String)dataRow["projectCapital11Title"];
			entity.ProjectCapital11Hours = (Convert.IsDBNull(dataRow["projectCapital11Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital11Hours"];
			entity.ProjectCapital12Title = (Convert.IsDBNull(dataRow["projectCapital12Title"]))?string.Empty:(System.String)dataRow["projectCapital12Title"];
			entity.ProjectCapital12Hours = (Convert.IsDBNull(dataRow["projectCapital12Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital12Hours"];
			entity.ProjectCapital13Title = (Convert.IsDBNull(dataRow["projectCapital13Title"]))?string.Empty:(System.String)dataRow["projectCapital13Title"];
			entity.ProjectCapital13Hours = (Convert.IsDBNull(dataRow["projectCapital13Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital13Hours"];
			entity.ProjectCapital14Title = (Convert.IsDBNull(dataRow["projectCapital14Title"]))?string.Empty:(System.String)dataRow["projectCapital14Title"];
			entity.ProjectCapital14Hours = (Convert.IsDBNull(dataRow["projectCapital14Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital14Hours"];
			entity.ProjectCapital15Title = (Convert.IsDBNull(dataRow["projectCapital15Title"]))?string.Empty:(System.String)dataRow["projectCapital15Title"];
			entity.ProjectCapital15Hours = (Convert.IsDBNull(dataRow["projectCapital15Hours"]))?0.0m:(System.Decimal?)dataRow["projectCapital15Hours"];
			entity.AheaRefineryWork = (Convert.IsDBNull(dataRow["aheaRefineryWork"]))?0.0m:(System.Decimal)dataRow["aheaRefineryWork"];
			entity.AheaResidueWork = (Convert.IsDBNull(dataRow["aheaResidueWork"]))?0.0m:(System.Decimal)dataRow["aheaResidueWork"];
			entity.AheaSmeltingWork = (Convert.IsDBNull(dataRow["aheaSmeltingWork"]))?0.0m:(System.Decimal?)dataRow["aheaSmeltingWork"];
			entity.AheaPowerGenerationWork = (Convert.IsDBNull(dataRow["aheaPowerGenerationWork"]))?0.0m:(System.Decimal?)dataRow["aheaPowerGenerationWork"];
			entity.AheaTotalManHours = (Convert.IsDBNull(dataRow["aheaTotalManHours"]))?0.0m:(System.Decimal)dataRow["aheaTotalManHours"];
			entity.AheaPeakNopplSiteWeek = (Convert.IsDBNull(dataRow["aheaPeakNopplSiteWeek"]))?(int)0:(System.Int32)dataRow["aheaPeakNopplSiteWeek"];
			entity.AheaAvgNopplSiteMonth = (Convert.IsDBNull(dataRow["aheaAvgNopplSiteMonth"]))?(int)0:(System.Int32)dataRow["aheaAvgNopplSiteMonth"];
			entity.AheaNoEmpExcessMonth = (Convert.IsDBNull(dataRow["aheaNoEmpExcessMonth"]))?(int)0:(System.Int32)dataRow["aheaNoEmpExcessMonth"];
			entity.IpFati = (Convert.IsDBNull(dataRow["ipFATI"]))?(int)0:(System.Int32?)dataRow["ipFATI"];
			entity.IpMti = (Convert.IsDBNull(dataRow["ipMTI"]))?(int)0:(System.Int32?)dataRow["ipMTI"];
			entity.IpRdi = (Convert.IsDBNull(dataRow["ipRDI"]))?(int)0:(System.Int32?)dataRow["ipRDI"];
			entity.IpLti = (Convert.IsDBNull(dataRow["ipLTI"]))?(int)0:(System.Int32?)dataRow["ipLTI"];
			entity.IpIfe = (Convert.IsDBNull(dataRow["ipIFE"]))?(int)0:(System.Int32?)dataRow["ipIFE"];
			entity.EhspNoLwd = (Convert.IsDBNull(dataRow["ehspNoLWD"]))?0.0m:(System.Decimal?)dataRow["ehspNoLWD"];
			entity.EhspNoRd = (Convert.IsDBNull(dataRow["ehspNoRD"]))?0.0m:(System.Decimal?)dataRow["ehspNoRD"];
			entity.EhsCorrective = (Convert.IsDBNull(dataRow["ehsCorrective"]))?(int)0:(System.Int32?)dataRow["ehsCorrective"];
			entity.JsaAudits = (Convert.IsDBNull(dataRow["JSAAudits"]))?(int)0:(System.Int32?)dataRow["JSAAudits"];
			entity.IWsc = (Convert.IsDBNull(dataRow["iWSC"]))?(int)0:(System.Int32?)dataRow["iWSC"];
			entity.ONoHswc = (Convert.IsDBNull(dataRow["oNoHSWC"]))?(int)0:(System.Int32?)dataRow["oNoHSWC"];
			entity.ONoBsp = (Convert.IsDBNull(dataRow["oNoBSP"]))?(int)0:(System.Int32?)dataRow["oNoBSP"];
			entity.QQas = (Convert.IsDBNull(dataRow["qQAS"]))?0.0m:(System.Decimal?)dataRow["qQAS"];
			entity.QNoNci = (Convert.IsDBNull(dataRow["qNoNCI"]))?(int)0:(System.Int32?)dataRow["qNoNCI"];
			entity.MTbmpm = (Convert.IsDBNull(dataRow["mTbmpm"]))?(int)0:(System.Int32?)dataRow["mTbmpm"];
			entity.MAwcm = (Convert.IsDBNull(dataRow["mAwcm"]))?(int)0:(System.Int32?)dataRow["mAwcm"];
			entity.MAmcm = (Convert.IsDBNull(dataRow["mAmcm"]))?(int)0:(System.Int32?)dataRow["mAmcm"];
			entity.MFatality = (Convert.IsDBNull(dataRow["mFatality"]))?(int)0:(System.Int32?)dataRow["mFatality"];
			entity.Training = (Convert.IsDBNull(dataRow["Training"]))?(int)0:(System.Int32?)dataRow["Training"];
			entity.MtTolo = (Convert.IsDBNull(dataRow["mtTolo"]))?(int)0:(System.Int32?)dataRow["mtTolo"];
			entity.MtFp = (Convert.IsDBNull(dataRow["mtFp"]))?(int)0:(System.Int32?)dataRow["mtFp"];
			entity.MtElec = (Convert.IsDBNull(dataRow["mtElec"]))?(int)0:(System.Int32?)dataRow["mtElec"];
			entity.MtMe = (Convert.IsDBNull(dataRow["mtMe"]))?(int)0:(System.Int32?)dataRow["mtMe"];
			entity.MtCs = (Convert.IsDBNull(dataRow["mtCs"]))?(int)0:(System.Int32?)dataRow["mtCs"];
			entity.MtCb = (Convert.IsDBNull(dataRow["mtCb"]))?(int)0:(System.Int32?)dataRow["mtCb"];
			entity.MtErgo = (Convert.IsDBNull(dataRow["mtErgo"]))?(int)0:(System.Int32?)dataRow["mtErgo"];
			entity.MtRa = (Convert.IsDBNull(dataRow["mtRa"]))?(int)0:(System.Int32?)dataRow["mtRa"];
			entity.MtHs = (Convert.IsDBNull(dataRow["mtHs"]))?(int)0:(System.Int32?)dataRow["mtHs"];
			entity.MtSp = (Convert.IsDBNull(dataRow["mtSp"]))?(int)0:(System.Int32?)dataRow["mtSp"];
			entity.MtIf = (Convert.IsDBNull(dataRow["mtIf"]))?(int)0:(System.Int32?)dataRow["mtIf"];
			entity.MtHp = (Convert.IsDBNull(dataRow["mtHp"]))?(int)0:(System.Int32?)dataRow["mtHp"];
			entity.MtRp = (Convert.IsDBNull(dataRow["mtRp"]))?(int)0:(System.Int32?)dataRow["mtRp"];
			entity.MtEnginfo81t = (Convert.IsDBNull(dataRow["mtENGINFO81t"]))?(int)0:(System.Int32?)dataRow["mtENGINFO81t"];
			entity.MtOthers = (Convert.IsDBNull(dataRow["mtOthers"]))?string.Empty:(System.String)dataRow["mtOthers"];
			entity.SafetyPlansSubmitted = (Convert.IsDBNull(dataRow["SafetyPlansSubmitted"]))?false:(System.Boolean)dataRow["SafetyPlansSubmitted"];
			entity.EbiOnSite = (Convert.IsDBNull(dataRow["EbiOnSite"]))?false:(System.Boolean?)dataRow["EbiOnSite"];
			entity.CompanySiteCategoryId = (Convert.IsDBNull(dataRow["CompanySiteCategoryId"]))?(int)0:(System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.CompanySiteCategoryName = (Convert.IsDBNull(dataRow["CompanySiteCategoryName"]))?string.Empty:(System.String)dataRow["CompanySiteCategoryName"];
			entity.CompanySiteCategoryDesc = (Convert.IsDBNull(dataRow["CompanySiteCategoryDesc"]))?string.Empty:(System.String)dataRow["CompanySiteCategoryDesc"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region KpiCompanySiteCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiCompanySiteCategoryFilterBuilder : SqlFilterBuilder<KpiCompanySiteCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryFilterBuilder class.
		/// </summary>
		public KpiCompanySiteCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiCompanySiteCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiCompanySiteCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiCompanySiteCategoryFilterBuilder

	#region KpiCompanySiteCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiCompanySiteCategoryParameterBuilder : ParameterizedSqlFilterBuilder<KpiCompanySiteCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryParameterBuilder class.
		/// </summary>
		public KpiCompanySiteCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiCompanySiteCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiCompanySiteCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiCompanySiteCategoryParameterBuilder
	
	#region KpiCompanySiteCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiCompanySiteCategorySortBuilder : SqlSortBuilder<KpiCompanySiteCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategorySqlSortBuilder class.
		/// </summary>
		public KpiCompanySiteCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiCompanySiteCategorySortBuilder

} // end namespace
