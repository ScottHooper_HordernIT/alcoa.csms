﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFatigueManagementDetailService
    {
        FatigueManagementDetail Get(Expression<Func<FatigueManagementDetail, bool>> where, List<Expression<Func<FatigueManagementDetail, object>>> includes);
        List<FatigueManagementDetail> GetMany(int? top, Expression<Func<FatigueManagementDetail, bool>> where, Func<IQueryable<FatigueManagementDetail>, IOrderedQueryable<FatigueManagementDetail>> orderBy, List<Expression<Func<FatigueManagementDetail, object>>> includes);
        List<FatigueManagementDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FatigueManagementDetail, bool>> where, Func<IQueryable<FatigueManagementDetail>, IOrderedQueryable<FatigueManagementDetail>> orderBy, List<Expression<Func<FatigueManagementDetail, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FatigueManagementDetail, bool>> where, List<Expression<Func<FatigueManagementDetail, object>>> includes);
        DateTime? GetMaxDateTime(Expression<Func<FatigueManagementDetail, bool>> where, Expression<Func<FatigueManagementDetail, DateTime?>> selector, List<Expression<Func<FatigueManagementDetail, object>>> includes);
        void Insert(FatigueManagementDetail item, bool saveChanges = true);
        void Update(FatigueManagementDetail item, bool saveChanges = true);
        void Delete(FatigueManagementDetail item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FatigueManagementDetailService : IFatigueManagementDetailService
    {
        private readonly IRepository<FatigueManagementDetail> _repository;

        public FatigueManagementDetailService(IRepository<FatigueManagementDetail> repository)
        {
            this._repository = repository;
        }

        public FatigueManagementDetail Get(Expression<Func<FatigueManagementDetail, bool>> where, List<Expression<Func<FatigueManagementDetail, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FatigueManagementDetail> GetMany(int? top, Expression<Func<FatigueManagementDetail, bool>> where, Func<IQueryable<FatigueManagementDetail>, IOrderedQueryable<FatigueManagementDetail>> orderBy, List<Expression<Func<FatigueManagementDetail, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FatigueManagementDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FatigueManagementDetail, bool>> where, Func<IQueryable<FatigueManagementDetail>, IOrderedQueryable<FatigueManagementDetail>> orderBy, List<Expression<Func<FatigueManagementDetail, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FatigueManagementDetail, bool>> where, List<Expression<Func<FatigueManagementDetail, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public DateTime? GetMaxDateTime(Expression<Func<FatigueManagementDetail, bool>> where, Expression<Func<FatigueManagementDetail, DateTime?>> selector, List<Expression<Func<FatigueManagementDetail, object>>> includes)
        {
            return _repository.GetMax(where, selector, includes);
        }

        public void Insert(FatigueManagementDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FatigueManagementDetail");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FatigueManagementDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FatigueManagementDetail");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FatigueManagementDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FatigueManagementDetail");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
