﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanyNoteProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanyNoteDataSourceDesigner))]
	public class CompanyNoteDataSource : ProviderDataSource<CompanyNote, CompanyNoteKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyNoteDataSource class.
		/// </summary>
		public CompanyNoteDataSource() : base(new CompanyNoteService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanyNoteDataSourceView used by the CompanyNoteDataSource.
		/// </summary>
		protected CompanyNoteDataSourceView CompanyNoteView
		{
			get { return ( View as CompanyNoteDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanyNoteDataSource control invokes to retrieve data.
		/// </summary>
		public CompanyNoteSelectMethod SelectMethod
		{
			get
			{
				CompanyNoteSelectMethod selectMethod = CompanyNoteSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanyNoteSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanyNoteDataSourceView class that is to be
		/// used by the CompanyNoteDataSource.
		/// </summary>
		/// <returns>An instance of the CompanyNoteDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanyNote, CompanyNoteKey> GetNewDataSourceView()
		{
			return new CompanyNoteDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanyNoteDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanyNoteDataSourceView : ProviderDataSourceView<CompanyNote, CompanyNoteKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyNoteDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanyNoteDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanyNoteDataSourceView(CompanyNoteDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanyNoteDataSource CompanyNoteOwner
		{
			get { return Owner as CompanyNoteDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanyNoteSelectMethod SelectMethod
		{
			get { return CompanyNoteOwner.SelectMethod; }
			set { CompanyNoteOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanyNoteService CompanyNoteProvider
		{
			get { return Provider as CompanyNoteService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanyNote> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanyNote> results = null;
			CompanyNote item;
			count = 0;
			
			System.Int32 _companyNoteId;
			System.Int32 _createdByRoleId;
			System.Int32 _createdByUserId;
            System.Int32 _createdByCompanyId;
            System.String _visibility;

			switch ( SelectMethod )
			{
				case CompanyNoteSelectMethod.Get:
					CompanyNoteKey entityKey  = new CompanyNoteKey();
					entityKey.Load(values);
					item = CompanyNoteProvider.Get(entityKey);
					results = new TList<CompanyNote>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanyNoteSelectMethod.GetAll:
                    results = CompanyNoteProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanyNoteSelectMethod.GetPaged:
					results = CompanyNoteProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanyNoteSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanyNoteProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanyNoteProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanyNoteSelectMethod.GetByCompanyNoteId:
					_companyNoteId = ( values["CompanyNoteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyNoteId"], typeof(System.Int32)) : (int)0;
					item = CompanyNoteProvider.GetByCompanyNoteId(_companyNoteId);
					results = new TList<CompanyNote>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case CompanyNoteSelectMethod.GetByCreatedByRoleId:
					_createdByRoleId = ( values["CreatedByRoleId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByRoleId"], typeof(System.Int32)) : (int)0;
					results = CompanyNoteProvider.GetByCreatedByRoleId(_createdByRoleId, this.StartIndex, this.PageSize, out count);
					break;
                case CompanyNoteSelectMethod.GetByCreatedByUserId:
                    _createdByUserId = (values["CreatedByUserId"] != null) ? (System.Int32)EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
                    results = CompanyNoteProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
                    break;
                case CompanyNoteSelectMethod.GetByCreatedByCompanyId:
                    _createdByCompanyId = (values["CreatedByCompanyId"] != null) ? (System.Int32)EntityUtil.ChangeType(values["CreatedByCompanyId"], typeof(System.Int32)) : (int)0;
                    results = CompanyNoteProvider.GetByCreatedByCompanyId(_createdByCompanyId, this.StartIndex, this.PageSize, out count);
                    break;
                // Column
                case CompanyNoteSelectMethod.GetByVisibility:
                    _visibility = (values["Visibility"] != null) ? (System.String)EntityUtil.ChangeType(values["Visibility"], typeof(System.String)) : string.Empty;
                    results = CompanyNoteProvider.GetByVisibility(_visibility, this.StartIndex, this.PageSize, out count);
                    break;
                case CompanyNoteSelectMethod.GetByCreatedByCompanyIdVisibility:
                    _createdByCompanyId = (values["CreatedByCompanyId"] != null) ? (System.Int32)EntityUtil.ChangeType(values["CreatedByCompanyId"], typeof(System.Int32)) : (int)0;
                    _visibility = (values["Visibility"] != null) ? (System.String)EntityUtil.ChangeType(values["Visibility"], typeof(System.String)) : string.Empty;
                    results = CompanyNoteProvider.GetByCreatedByCompanyIdVisibility(_createdByCompanyId, _visibility, this.StartIndex, this.PageSize, out count);
                    break;
                // M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanyNoteSelectMethod.Get || SelectMethod == CompanyNoteSelectMethod.GetByCompanyNoteId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanyNote entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanyNoteProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanyNote> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanyNoteProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanyNoteDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanyNoteDataSource class.
	/// </summary>
	public class CompanyNoteDataSourceDesigner : ProviderDataSourceDesigner<CompanyNote, CompanyNoteKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompanyNoteDataSourceDesigner class.
		/// </summary>
		public CompanyNoteDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanyNoteSelectMethod SelectMethod
		{
			get { return ((CompanyNoteDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanyNoteDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanyNoteDataSourceActionList

	/// <summary>
	/// Supports the CompanyNoteDataSourceDesigner class.
	/// </summary>
	internal class CompanyNoteDataSourceActionList : DesignerActionList
	{
		private CompanyNoteDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanyNoteDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanyNoteDataSourceActionList(CompanyNoteDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanyNoteSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanyNoteDataSourceActionList
	
	#endregion CompanyNoteDataSourceDesigner
	
	#region CompanyNoteSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanyNoteDataSource.SelectMethod property.
	/// </summary>
	public enum CompanyNoteSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanyNoteId method.
		/// </summary>
		GetByCompanyNoteId,
		/// <summary>
		/// Represents the GetByCreatedByRoleId method.
		/// </summary>
		GetByCreatedByRoleId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
        /// <summary>
        /// Represents the GetByCreatedByCompanyId method.
        /// </summary>
        GetByCreatedByCompanyId,
        /// <summary>
        /// Represents the GetByVisibility method.
        /// </summary>
        GetByVisibility,
        /// <summary>
        /// Represents the GetByCreatedByCompanyIdVisibility method.
        /// </summary>
        GetByCreatedByCompanyIdVisibility
    }
	
	#endregion CompanyNoteSelectMethod

	#region CompanyNoteFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyNote"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyNoteFilter : SqlFilter<CompanyNoteColumn>
	{
	}
	
	#endregion CompanyNoteFilter

	#region CompanyNoteExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyNote"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyNoteExpressionBuilder : SqlExpressionBuilder<CompanyNoteColumn>
	{
	}
	
	#endregion CompanyNoteExpressionBuilder	

	#region CompanyNoteProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanyNoteChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyNote"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyNoteProperty : ChildEntityProperty<CompanyNoteChildEntityTypes>
	{
	}
	
	#endregion CompanyNoteProperty
}

