﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EhsimsExceptionsCompaniesSitesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EhsimsExceptionsCompaniesSitesDataSourceDesigner))]
	public class EhsimsExceptionsCompaniesSitesDataSource : ReadOnlyDataSource<EhsimsExceptionsCompaniesSites>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesDataSource class.
		/// </summary>
		public EhsimsExceptionsCompaniesSitesDataSource() : base(new EhsimsExceptionsCompaniesSitesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EhsimsExceptionsCompaniesSitesDataSourceView used by the EhsimsExceptionsCompaniesSitesDataSource.
		/// </summary>
		protected EhsimsExceptionsCompaniesSitesDataSourceView EhsimsExceptionsCompaniesSitesView
		{
			get { return ( View as EhsimsExceptionsCompaniesSitesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EhsimsExceptionsCompaniesSitesDataSourceView class that is to be
		/// used by the EhsimsExceptionsCompaniesSitesDataSource.
		/// </summary>
		/// <returns>An instance of the EhsimsExceptionsCompaniesSitesDataSourceView class.</returns>
		protected override BaseDataSourceView<EhsimsExceptionsCompaniesSites, Object> GetNewDataSourceView()
		{
			return new EhsimsExceptionsCompaniesSitesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EhsimsExceptionsCompaniesSitesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EhsimsExceptionsCompaniesSitesDataSourceView : ReadOnlyDataSourceView<EhsimsExceptionsCompaniesSites>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EhsimsExceptionsCompaniesSitesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EhsimsExceptionsCompaniesSitesDataSourceView(EhsimsExceptionsCompaniesSitesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EhsimsExceptionsCompaniesSitesDataSource EhsimsExceptionsCompaniesSitesOwner
		{
			get { return Owner as EhsimsExceptionsCompaniesSitesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EhsimsExceptionsCompaniesSitesService EhsimsExceptionsCompaniesSitesProvider
		{
			get { return Provider as EhsimsExceptionsCompaniesSitesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EhsimsExceptionsCompaniesSitesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EhsimsExceptionsCompaniesSitesDataSource class.
	/// </summary>
	public class EhsimsExceptionsCompaniesSitesDataSourceDesigner : ReadOnlyDataSourceDesigner<EhsimsExceptionsCompaniesSites>
	{
	}

	#endregion EhsimsExceptionsCompaniesSitesDataSourceDesigner

	#region EhsimsExceptionsCompaniesSitesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsCompaniesSitesFilter : SqlFilter<EhsimsExceptionsCompaniesSitesColumn>
	{
	}

	#endregion EhsimsExceptionsCompaniesSitesFilter

	#region EhsimsExceptionsCompaniesSitesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsCompaniesSitesExpressionBuilder : SqlExpressionBuilder<EhsimsExceptionsCompaniesSitesColumn>
	{
	}
	
	#endregion EhsimsExceptionsCompaniesSitesExpressionBuilder		
}

