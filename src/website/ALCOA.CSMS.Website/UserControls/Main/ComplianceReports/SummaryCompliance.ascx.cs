using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data.SqlClient;
using DevExpress.XtraCharts;
using System.Threading;
using System.Reflection;
//Added By Bishwajit for Item#31
using System.IO;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.xml;
using System.Xml;
using System.Drawing;
using System.Text;

using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Web.Configuration;
//End Added By Bishwajit for Item#31


// excellent use of oo and programming techniques below... lol
// TODO: write properly when time allows = never? lol
public partial class UserControls_SummaryCompliance : System.Web.UI.UserControl
{
    Auth auth = new Auth();
	//Added By Bishwajit for Item#31
    string pageContent = string.Empty;
	//End Added By Bishwajit for Item#31

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {

       // Thread webBrowseThread = new Thread(new ThreadStart(GenerateScreenshot));
       // webBrowseThread.SetApartmentState(ApartmentState.STA);
       // webBrowseThread.Start(); 

        //To Check if Value Exists
		//Added By Bishwajit for Item#31
        Session["IsDataExist"] = false;
		//End Added By Bishwajit for Item#31



        Reports_DropDownListPicker1.btnHandler += new  UserControls_Tiny_Reports_DropDownListPicker2a.OnButtonClick(DropDownListPicker_btnHandler);

        SessionHandler.spVar_Page = "ComplianceReport";

        ASPxGridView.RegisterBaseScript(this.Page);

        if (Request.QueryString["id"] != null)
        {

            if (!String.IsNullOrEmpty(Request.QueryString["id"].ToString()))
            {
                if (auth.RoleId == (int)RoleList.Contractor &&
    Request.QueryString["id"].ToString() == "go")
                {
                    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
                    { SessionHandler.spVar_SiteId = null; };
                    if (String.IsNullOrEmpty(SessionHandler.spVar_CategoryId))
                    { SessionHandler.spVar_CategoryId = "3"; };
                    if (String.IsNullOrEmpty(SessionHandler.spVar_CategoryId2))
                    { SessionHandler.spVar_CategoryId2 = "%"; };
                    if (String.IsNullOrEmpty(SessionHandler.spVar_Year))
                    {
                        int iYear = DateTime.Now.Year;
                        if (DateTime.Now.Month == 1)
                        {
                            iYear = DateTime.Now.Year - 1;
                        }
                        SessionHandler.spVar_Year = iYear.ToString();
                    }
                }

                int i = 0;
                if (String.IsNullOrEmpty(SessionHandler.spVar_CompanyId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_Year)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_CategoryId)) { i++; };
                if (i == 0)
                {
                    if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0)
                    {
                        bool cont = true;
                        if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0)
                        {
                            int noResi = 0;
                            CompanySiteCategoryStandardFilters cscsFilters = new CompanySiteCategoryStandardFilters();
                            cscsFilters.Append(CompanySiteCategoryStandardColumn.CompanyId, SessionHandler.spVar_CompanyId);
                            cscsFilters.Append(CompanySiteCategoryStandardColumn.SiteId, SessionHandler.spVar_SiteId);
                            cscsFilters.Append(CompanySiteCategoryStandardColumn.CompanySiteCategoryId, SessionHandler.spVar_CategoryId2);

                            TList<CompanySiteCategoryStandard> cscsList = DataRepository.CompanySiteCategoryStandardProvider.GetPaged(cscsFilters.ToString(), null, 0, 100, out noResi);

                            if (noResi <= 0)
                            {
                                PopUpErrorMessage("No Data Exists. Hint: Try again without an Embedded/Non-Embedded Status.");
                                cont = false;
                            }
                        }
                        else
                        {
                            SessionHandler.spVar_RegionId = (SessionHandler.spVar_SiteId).Replace("-", "");
                            int noResi2 = 0;

                            int noResi = 0;
                            CompanySiteCategoryStandardFilters cscsFilters = new CompanySiteCategoryStandardFilters();
                            cscsFilters.Append(CompanySiteCategoryStandardColumn.CompanyId, SessionHandler.spVar_CompanyId);
                            cscsFilters.Append(CompanySiteCategoryStandardColumn.CompanySiteCategoryId, SessionHandler.spVar_CategoryId2);

                            TList<CompanySiteCategoryStandard> cscsList = DataRepository.CompanySiteCategoryStandardProvider.GetPaged(cscsFilters.ToString(), null, 0, 100, out noResi2);

                            if (noResi2 == 0) { cont = false; PopUpErrorMessage("No Data Exists"); };
                        }
                        if (cont == true)
                        {
						//Added By Bishwajit for Item#31
                            //To Check if Value Exists
                            Session["IsDataExist"] = true;
						//End Added By Bishwajit for Item#31
                            DrawPage();

                        }
                    }
                    else
                    {
                        PopUpErrorMessage("Please Select All Search Fields.");
                        //error msg :)
                    }
                }
            }
        }
        else
        {
            //SessionHandler.spVar_Page = "ComplianceChart_A_A";
            SessionHandler.spVar_CompanyId = "-1";
            if (auth.RoleId == (int)RoleList.Contractor)
            {
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            }
            SessionHandler.spVar_SiteId = "";
            SessionHandler.spVar_CategoryId = "-1";
            SessionHandler.spVar_CategoryId2 = "%";

            int iYear = DateTime.Now.Year;
            if (DateTime.Now.Month == 1)
            {
                iYear = DateTime.Now.Year - 1;
            }
            SessionHandler.spVar_Year = iYear.ToString();
            //DrawPage();
        }

        if (!postBack)
        {
            Label1.Text = "As at: " + DateTime.Now.ToString("dd/MM/yyyy");

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader): //Reader
                    break;
                case ((int)RoleList.Contractor): //Contributor
                    break;
                case ((int)RoleList.Administrator): //Administrator
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    public void DrawPage()
    {
        if (String.IsNullOrEmpty(SessionHandler.spVar_ComplianceCount))
        {
            SessionHandler.spVar_ComplianceCount = "0";
        }
        Panel1.Visible = true;

        int _i = 0;

        UserControls_Tiny_ComplianceReports_ComplianceReport_MT uc_MT =
               (UserControls_Tiny_ComplianceReports_ComplianceReport_MT)
               (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_MT.ascx", SessionHandler.spVar_Page)));
        uc_MT.ID = "uc_MT";
        ph_MT.Controls.Add(uc_MT);

        UserControls_Tiny_ComplianceReports_ComplianceReport_SQ uc_SQ =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_SQ)
                (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_SQ.ascx", SessionHandler.spVar_Page)));
        uc_SQ.ID = "uc_SQ";
        ph_SQ.Controls.Add(uc_SQ);

        UserControls_Tiny_ComplianceReports_ComplianceReport_SFR uc_SFR =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_SFR)
                    (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_SFR.ascx", SessionHandler.spVar_Page)));
        uc_SFR.ID = "uc_SFR";
        ph_SFR.Controls.Add(uc_SFR);

        UserControls_Tiny_ComplianceReports_ComplianceReport_KPI uc_KPI =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_KPI)
                    (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_KPI.ascx", SessionHandler.spVar_Page)));
        uc_KPI.ID = "uc_KPI";
        ph_KPI.Controls.Add(uc_KPI);
        _i = _i + uc_KPI.getExpected();

        UserControls_Tiny_ComplianceReports_ComplianceReport_CSA uc_CSA =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_CSA)
                    (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_CSA.ascx", SessionHandler.spVar_Page)));
        uc_CSA.ID = "uc_CSA";
        ph_CSA.Controls.Add(uc_CSA);
        _i = _i + uc_CSA.getExpected();

        UserControls_Tiny_ComplianceReports_ComplianceReport_SP uc_SP = (UserControls_Tiny_ComplianceReports_ComplianceReport_SP)
                (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_SP.ascx", SessionHandler.spVar_Page)));
        uc_SP.ID = "uc_SP";

        ph_SP.Controls.Add(uc_SP);
        _i = _i + uc_SP.getExpected();


        if (uc_SP.FindControl("grid").Visible == false)
            SafetyPlan.Visible = false;
        else { SafetyPlan.Visible = true; }

        //if (auth.RoleId == (int)RoleList.Administrator)
        //{
        UserControls_Tiny_ComplianceReports_ComplianceReport_MS uc_MS = (UserControls_Tiny_ComplianceReports_ComplianceReport_MS)
                (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_MS.ascx", SessionHandler.spVar_Page)));
        uc_MS.ID = "uc_MS";
        ph_MS.Controls.Add(uc_MS);
        _i = _i + uc_MS.getExpected();

        //Training Schedule not applicable in Vic Ops.
        int SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
        if (SiteId > 0)
        {
            RegionsService rService = new RegionsService();
            Regions r = rService.GetByRegionInternalName("VICOPS");
            RegionsSitesService rsService = new RegionsSitesService();
            TList<RegionsSites> rsList = rsService.GetBySiteId(SiteId);
            foreach (RegionsSites rs in rsList)
            {
                if (rs.RegionId == r.RegionId)
                {
                    lblTrainingSchedules.Text = "Training Schedules - n/a";
                }
            }
        }
        if (SiteId == -4)
        {
            lblTrainingSchedules.Text = "Training Schedules - n/a";
        }

        if (lblTrainingSchedules.Text != "Training Schedules - n/a")
        {
            UserControls_Tiny_ComplianceReports_ComplianceReport_TS uc_TS = (UserControls_Tiny_ComplianceReports_ComplianceReport_TS)
        (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_TS.ascx", SessionHandler.spVar_Page)));
            uc_TS.ID = "uc_TS";
            ph_TS.Controls.Add(uc_TS);
            _i = _i + uc_TS.getExpected();
        }

        UserControls_Tiny_ComplianceReports_ComplianceReport_CC uc_CC = (UserControls_Tiny_ComplianceReports_ComplianceReport_CC)
               (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_CC.ascx", SessionHandler.spVar_Page)));
        uc_CC.ID = "uc_CC";
        ph_CC.Controls.Add(uc_CC);
        _i = _i + uc_CC.getExpected();

        UserControls_Tiny_ComplianceReports_ComplianceReport_MR uc_MR = (UserControls_Tiny_ComplianceReports_ComplianceReport_MR)
                (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_MR.ascx", SessionHandler.spVar_Page)));
        uc_MR.ID = "uc_MR";
        ph_MR.Controls.Add(uc_MR);
        _i = _i + uc_MR.getExpected();

        //Thread.Sleep(5000);
        uc_KPI.getExpected();
        lblGood.Text = _i.ToString();

        decimal good = Convert.ToDecimal(_i);
        const decimal total = 7;
        decimal calc = (good / total) * 100;
        int calc2 = Convert.ToInt32(calc);
        lblPerc.Text = String.Format("{0}%", calc2);
		//Added By Bishwajit for Item#31
        btnOpenPdf.Visible = true;
		//End Added By Bishwajit for Item#31
        //SessionHandler.spVar_ComplianceCount = "0";
    }

    void DropDownListPicker_btnHandler(String strvalue)
    {
        //((ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCompanies")).Value = 5;
        //load stuff
        try
        {
            

            ph_KPI.Controls.Clear();
            ph_CSA.Controls.Clear();

            ASPxComboBox ddlCompanies = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCompanies");
            ASPxComboBox ddlCategory = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCategory");
            ASPxComboBox ddlSites = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlSites");
            ASPxComboBox ddlYear = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlYear");
            

            string sCompany = ddlCompanies.SelectedItem.Value.ToString();
            string sCategory = ddlCategory.SelectedItem.Value.ToString();
            string sSite = ddlSites.SelectedItem.Value.ToString();
            string sYear = ddlYear.SelectedItem.Value.ToString();
            

            int i = 0;
            if (String.IsNullOrEmpty(sCompany)) { i++; };
            if (String.IsNullOrEmpty(sCategory)) { i++; };
            if (String.IsNullOrEmpty(sSite)) { i++; };
            if (String.IsNullOrEmpty(sYear)) { i++; };
            
            if (i == 0)
            {
                SessionHandler.spVar_CompanyId = sCompany;
                SessionHandler.spVar_SiteId = sSite;
                SessionHandler.spVar_SiteName = ddlSites.SelectedItem.Text;
                SessionHandler.spVar_Year = sYear;
                SessionHandler.spVar_CategoryId = sCategory;
                //temp
                switch (sCategory)
                {
                    case "0":
                        SessionHandler.spVar_CategoryId2 = "0";
                        break;
                    case "-1":
                        SessionHandler.spVar_CategoryId2 = "%";
                        break;
                    default:
                        SessionHandler.spVar_CategoryId2 = sCategory;
                        break;
                }

                if (sCompany != "-1")
                {
                    Response.Redirect("Reports_SummaryCompliance.aspx?id=go", true);
                }
                else
                {
                    PopUpErrorMessage("Please Select All Search Fields.");
                }

            }

        }
        catch (Exception ex)
        {
           
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            PopUpErrorMessage("Error Occurred: " + ex.Message);
        }
    }

    protected void PopUpErrorMessage(string errormsg)
    {
        Panel1.Visible = false;
        PopupWindow pcWindow = new PopupWindow(errormsg);
        pcWindow.FooterText = "";
        pcWindow.ShowOnPageLoad = true;
        pcWindow.Modal = true;
        //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
        ASPxPopupControl1.Windows.Add(pcWindow);
    }
	//Added By Bishwajit for Item#31
    protected void btnOpenPdf_Click(object sender, EventArgs e)
    {
        try
        {

        if (Session["IsDataExist"] != null)
        {
        if (Convert.ToBoolean(Session["IsDataExist"]))
        {

            btnOpenPdf.Visible = true;   

            int month = 0;
            int expected = 0;
            string headerBgColor = "#D1DBF4";
            string companyName = string.Empty;
            string siteName = string.Empty;
            string CategoryName = string.Empty;
            string strYear = string.Empty;
            int colspan = 0;

            string qtrColor1 = string.Empty;
            string qtrColor2 = string.Empty;
            string qtrColor3 = string.Empty;
            string qtrColor4 = string.Empty;

            string qtrValue1 = string.Empty;
            string qtrValue2 = string.Empty;
            string qtrValue3 = string.Empty;
            string qtrValue4 = string.Empty;

            string imgURLPath = string.Empty;
            string[] arr = Request.Url.ToString().Split('/');
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (i > 0) imgURLPath = imgURLPath + "/";
                imgURLPath = imgURLPath + arr[i];
            }

            string imgPathRedCross = imgURLPath + "/" + @"Images/redcross.gif";
            string imgPathGreenTick = imgURLPath + "/" + @"Images/greentick.gif";

            string imgPathTLRed = imgURLPath + "/" + @"Images/TrafficLights/tlRed.gif";
            string imgPathTLYellow = imgURLPath + "/" + @"Images/TrafficLights/tlYellow.gif";
            string imgPathTLGreen = imgURLPath + "/" + @"Images/TrafficLights/tlGreen.gif";

            string imgPathArticleIcon = imgURLPath + "/" + @"Images/ArticleIcon.gif";

        string imgRedCross = "<img align='center' height='10px' width='10px'  src='" + imgPathRedCross + "' alt='Cross'/>";
        string imgGreenTick = "<img align='center' height='10px' width='10px'  src='" + imgPathGreenTick + "' alt='Cross'/>";
        string imgYellow = "<img align='center' height='10px' width='10px' src='" + imgPathTLYellow + "' alt='Cross'/>";


            if (SessionHandler.spVar_Year == DateTime.Now.Year.ToString())
            {
                month = DateTime.Now.Month;
            }
            else
            {
                month = 12;
                expected = 4;
            }



            string headerFormat = "<h4 style='color:#110c84'>[Header]</h4><br/>";

            bool isCompany = false;
           
            string contents = File.ReadAllText(Server.MapPath(@"pdfTemplate1.htm"));

            if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
            {
                isCompany = true;
            }
            else
            {
                isCompany = false;
            }


        #region Mandated Training (%)

            StringBuilder sbGridsMt = new StringBuilder();


            StringBuilder sbGrids = new StringBuilder();
            if (Session["spVar_GridMT"] != null)
            {
                string headerMT = headerFormat.Replace("[Header]", "Mandated Training (%)");
                sbGridsMt.Append(headerMT);
                DataSet dsGridMt1 = (DataSet)Session["spVar_GridMT"];
                DataTable dtGridMt1=dsGridMt1.Tables[0];

                DataView dvMT = new DataView(dtGridMt1);
                dvMT.Sort = "CompanySiteCategoryId,SiteName ASC";
                DataTable dtGridMt = dvMT.ToTable();

                sbGridsMt.Append("<table width='100%' border='1' align='left' valign='top' style='solid gray; font-size:7px; color:Black;'>");
                sbGridsMt.Append("<tr>");
                sbGridsMt.Append("<td border='1' width='25%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Residential Category</td>");
                sbGridsMt.Append("<td border='1' width='15%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Site Name</td>");

                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Jan</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Feb</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Mar</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Apr</td>");

                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>May</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Jun</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Jul</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Aug</td>");

                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Sep</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Oct</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Nov</td>");
                sbGridsMt.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold; border:1px solid gray;'>Dec</td>");
                sbGridsMt.Append("</tr>");

                if (dtGridMt.Rows.Count > 0)
                {

                    for (int j = 0; j < dtGridMt.Rows.Count; j++)
                    {
                        sbGridsMt.Append("<tr>");
                        sbGridsMt.Append("<td border='1' width='25%' valign='top' align='left'>");
                        if (dtGridMt.Rows[j]["CompanySiteCategoryId"] != null)
                        {
                            CompanySiteCategoryService compSiSer = new CompanySiteCategoryService();
                            CompanySiteCategory compSi = compSiSer.GetByCompanySiteCategoryId(Convert.ToInt32(dtGridMt.Rows[j]["CompanySiteCategoryId"]));

                            sbGridsMt.Append(compSi.CategoryDesc);
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='15%' valign='top' align='left'>");
                        if (dtGridMt.Rows[j]["SiteName"] != null)
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["SiteName"]));
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Jan"] != null && dtGridMt.Rows[j]["Jan"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Jan"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Feb"] != null && dtGridMt.Rows[j]["Feb"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Feb"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Mar"] != null && dtGridMt.Rows[j]["Mar"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Mar"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Apr"] != null && dtGridMt.Rows[j]["Apr"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Apr"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["May"] != null && dtGridMt.Rows[j]["May"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["May"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Jun"] != null && dtGridMt.Rows[j]["Jun"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Jun"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Jul"] != null && dtGridMt.Rows[j]["Jul"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Jul"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Aug"] != null && dtGridMt.Rows[j]["Aug"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Aug"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1'width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Sep"] != null && dtGridMt.Rows[j]["Sep"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Sep"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Oct"] != null && dtGridMt.Rows[j]["Oct"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Oct"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Nov"] != null && dtGridMt.Rows[j]["Nov"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Nov"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("<td border='1' width='5%' valign='top' align='Center'>");
                        if (dtGridMt.Rows[j]["Dec"] != null && dtGridMt.Rows[j]["Dec"].ToString() != "")
                        {
                            sbGridsMt.Append(Convert.ToString(dtGridMt.Rows[j]["Dec"]));
                        }
                        else
                        {
                            sbGridsMt.Append("-");
                        }
                        sbGridsMt.Append("</td>");

                        sbGridsMt.Append("</tr>");
                    }
                }
                else
                {
                    sbGridsMt.Append("<td border='1' width='100%' colspan='14' valign='top' align='Center'>");                    
                    sbGridsMt.Append("No Safety Qualification submitted ");
                    sbGridsMt.Append("</td>");
                    sbGridsMt.Append("</tr>");
                }
                sbGridsMt.Append("</table>");
                sbGridsMt.Append("<br/>"); 
            }
            
            #endregion


        #region Safety Qualification
            if (Session["SQValue"] != null)
            {
                if (Convert.ToBoolean(Session["SQValue"]))
                {
                    //TO Create Tabke for QuestionnaireWithLocationApprovalViewLatest
                    QuestionnaireWithLocationApprovalViewLatestService qLocApprSer = new QuestionnaireWithLocationApprovalViewLatestService();

                    string whereCondition = @"CompanyId='" + Convert.ToInt32(SessionHandler.spVar_CompanyId) + "'";
                    VList<QuestionnaireWithLocationApprovalViewLatest> qLocApprList = qLocApprSer.Get(whereCondition, null);

                    sbGrids.Append(headerFormat.Replace("[Header]", "Safety Qualification"));

                    sbGrids.Append("<table width='100%' align='Center' valign='top' border='1' style='solid gray; font-size:7px; color:Black;'>");
                    sbGrids.Append("<tr>");
                    sbGrids.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold; border:1px solid gray;'>Created Date</td>");
                    sbGrids.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;border:1px solid gray;'>Modified Date</td>");
                    sbGrids.Append("<td border='1' width='17%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;border:1px solid gray;'>Status</td>");
                    sbGrids.Append("<td border='1' width='12%' bgcolor='" + headerBgColor + "' align='Center' valign='top' style='font-weight:bold;border:1px solid gray;'>Final Risk Rating</td>");
                    sbGrids.Append("<td border='1' width='17%' bgcolor='" + headerBgColor + "' align='Center' valign='top' style='font-weight:bold;border:1px solid gray;'>Assessment Valid Till</td>");
                    sbGrids.Append("<td border='1' width='17%' bgcolor='" + headerBgColor + "' align='Center' valign='top' style='font-weight:bold;border:1px solid gray;'>Recommended</td>");
                    sbGrids.Append("<td border='1' width='17%' bgcolor='" + headerBgColor + "' align='Center' valign='top' style='font-weight:bold;border:1px solid gray;'>Company Status</td>");
                    sbGrids.Append("</tr>");

                    DataSet ds = new DataSet();
                    if (qLocApprList != null)
                    {
                        foreach (QuestionnaireWithLocationApprovalViewLatest qerList in qLocApprList)
                        {

                            string mainAssessmentStatus = "";
                            string MainAssessmentValidTo = "";
                            string finalRiskRating = "";

                            if (qerList.MainAssessmentRiskRating != null)
                            {

                                mainAssessmentStatus = (string)qerList.MainAssessmentRiskRating;

                            }

                            if (qerList.MainAssessmentValidTo != null)
                            {
                                DateTime dtExpiry = ((DateTime)qerList.MainAssessmentValidTo).Date;
                                MainAssessmentValidTo = dtExpiry.ToShortDateString();

                            }

                            if (String.IsNullOrEmpty(MainAssessmentValidTo))
                            {
                                //check previous questionnaire
                                if (qerList.CompanyId != null)
                                {

                                    QuestionnaireFilters qFilters = new QuestionnaireFilters();
                                    qFilters.Append(QuestionnaireColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
                                    qFilters.Append(QuestionnaireColumn.CompanyId, ((int)qerList.CompanyId).ToString());
                                    int count = 0;
                                    TList<Questionnaire> qList =
                                        DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);
                                    if (qList != null && count > 0)
                                    {
                                        if (qList[0].MainAssessmentValidTo != null)
                                        {
                                            MainAssessmentValidTo = ((DateTime)qList[0].MainAssessmentValidTo).Date.ToShortDateString() + " (Previous Questionnaire)";
                                        }
                                    }
                                }
                            }



                            if (!String.IsNullOrEmpty(mainAssessmentStatus))
                            {

                                finalRiskRating = "Incomplete"; //default value.

                                int status = (int)qerList.Status;
                                if (status == (int)QuestionnaireStatusList.AssessmentComplete)
                                {
                                    string ProcurementRiskRating = (string)qerList.InitialRiskAssessment;
                                    string SupplierRiskRating = mainAssessmentStatus;
                                    if (!String.IsNullOrEmpty(ProcurementRiskRating))
                                    {
                                        finalRiskRating = ProcurementRiskRating;
                                    }
                                    if (!String.IsNullOrEmpty(SupplierRiskRating))
                                    {
                                        int _ProcurementRiskRating = Helper.Questionnaire.getRiskLevel(ProcurementRiskRating);
                                        int _SupplierRiskRating = Helper.Questionnaire.getRiskLevel(SupplierRiskRating);

                                        if (_SupplierRiskRating > _ProcurementRiskRating)
                                        {
                                            finalRiskRating = SupplierRiskRating;
                                        }
                                    }
                                }
                            }




                            sbGrids.Append("<tr>");
                            sbGrids.Append("<td border='1' width='10%' valign='top' align='left'>");
                            if (qerList.CreatedDate != null)
                            {
                                sbGrids.Append(qerList.CreatedDate.ToShortDateString());
                            }
                            sbGrids.Append("</td>");
                            sbGrids.Append("<td border='1' width='10%' valign='top' align='left'>");
                            if (qerList.ModifiedDate != null)
                            {
                                sbGrids.Append(((DateTime)qerList.ModifiedDate).Date.ToShortDateString());
                            }
                            sbGrids.Append("</td>");
                            sbGrids.Append("<td border='1' width='17%' valign='top' align='left'>");
                            if (qerList.Status != null)
                            {
                                int statusId = qerList.Status;
                                QuestionnaireStatusService qSer = new QuestionnaireStatusService();
                                QuestionnaireStatus q = qSer.GetByQuestionnaireStatusId(statusId);

                                sbGrids.Append(q.QuestionnaireStatusDesc);
                            }
                            sbGrids.Append("</td>");
                            sbGrids.Append("<td border='1' width='12%' valign='top' align='left'>");

                            sbGrids.Append(finalRiskRating);

                            sbGrids.Append("</td>");
                            sbGrids.Append("<td border='1' width='17%' valign='top' align='left'>");

                            sbGrids.Append(MainAssessmentValidTo);

                            sbGrids.Append("</td>");
                            sbGrids.Append("<td border='1' valign='top' width='17%' align='left'>");
                            if (qerList.Recommended != null)
                            {
                                if (Convert.ToBoolean(qerList.Recommended))
                                {
                                    sbGrids.Append("Yes");
                                }
                                else
                                {
                                    sbGrids.Append("No");
                                }

                            }
                            sbGrids.Append("</td>");
                            sbGrids.Append("<td border='1' valign='top' width='17%' align='left'>");
                            if (qerList.CompanyStatusDesc != null)
                            {
                                sbGrids.Append(qerList.CompanyStatusDesc.ToString());
                            }
                            sbGrids.Append("</td>");

                            sbGrids.Append("</tr>");

                        }
                    }
                    else
                    {
                        sbGrids.Append("<tr>");
                        sbGrids.Append("<td border='1' width='100%' colspan='7' valign='top' align='center'>");
                        sbGrids.Append("No Safety Qualification document(s) submitted.");
                        sbGrids.Append("</td>");
                        sbGrids.Append("</tr>");
                    }

                    sbGrids.Append("</table>");
                    sbGrids.Append("<br/>");
                }
            }
            #endregion
            #region Safety Plan
            StringBuilder sbSP = new StringBuilder();
            if (Session["ComplianceReportSP"] != null)
            {
                bool companyVisible = false;
                string CompSafetyCat = string.Empty;
                bool tableShow = false;
                string foreColor = string.Empty;
                string SubmittedValue = string.Empty;
                string imgPath = string.Empty;
               colspan = 0;

                if (SessionHandler.spVar_Page == "ComplianceReport")
                {
                    if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                    {
                        companyVisible = false;
                    }
                    else
                    {
                        companyVisible = true;
                    }
                    DataSet dsSite = DataRepository.FileDbProvider.ComplianceReport_ByCompany_Site(
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                          Convert.ToInt32(SessionHandler.spVar_Year),
                          Convert.ToInt32(SessionHandler.spVar_SiteId));

                    if (dsSite.Tables[0].Rows.Count > 0)
                    {
                        CompSafetyCat = dsSite.Tables[0].Rows[0][0].ToString();
                    }
                    if (CompSafetyCat == "Embedded" || CompSafetyCat == "Non-Embedded 1")
                    {
                        tableShow = true;
                    }
                    else
                    {
                        tableShow = false;
                    }
                }
                else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
                {
                    if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                    {

                    }
                    else//Regions
                    {
                        //Code for Regions
                    }
                    tableShow = true;
                    companyVisible = true;
                }
                if (tableShow)
                {

                    DataTable dtSP1 = (DataTable)Session["ComplianceReportSP"];
                    //CategoryDesc
                    DataView dvSP = new DataView(dtSP1);
                    dvSP.Sort = "CategoryDesc ASC";
                    DataTable dtSP = dvSP.ToTable();
                   

                    sbSP.Append(headerFormat.Replace("[Header]", "Safety Management Plans"));
                    sbSP.Append("<table width='100%' border='1' align='left' valign='top' style='font-size:7px; color:Black;'>");
                    sbSP.Append("<tr>");
                    if (companyVisible)
                    {
                        sbSP.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Company Name</td>");
                        colspan = 5;
                    }
                    else
                    {
                        colspan = 4;
                    }

                    sbSP.Append("<td border='1' width='40%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Residential Category</td>");//Changed by AG from Safety Compliance Category
                    sbSP.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Submitted</td>");
                    sbSP.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Latest Plan Current Status</td>");
                    sbSP.Append("</tr>");
                    if (dtSP.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtSP.Rows.Count; i++)
                        {

                            if (Convert.ToInt32(dtSP.Rows[i]["Submitted"]) != 0)
                            {
                                foreColor = "Green";
                                SubmittedValue = dtSP.Rows[i]["Submitted"].ToString();


                                switch (dtSP.Rows[i]["StatusName"].ToString())
                                {
                                    case "Being Assessed":
                                        //green++;
                                        imgPath = "<img width='10px' align='right' height='10px' src='" + imgPathTLGreen + "' alt='img3'/>";
                                        break;
                                    case "Not Approved":

                                        imgPath = "<img width='10px' align='right' height='10px' src='" + imgPathRedCross + "' alt='img3'/>";
                                        break;
                                    case "Approved":
                                        // green++;                                           
                                        imgPath = "<img width='10px' align='right' height='10px' src='" + imgPathGreenTick + "' alt='img3'/>";
                                        break;
                                    default:
                                        //  green++;                                            
                                        imgPath = "<img width='10px' align='right' height='10px' src='" + imgPathTLGreen + "' alt='img3'/>";
                                        break;
                                }
                            }
                            else
                            {
                                foreColor = "Red";
                                imgPath = "<img align='center' width='10px' height='10px' src='" + imgPathTLRed + "' alt='img3'/>";
                            }

                            sbSP.Append("<tr>");
                            if (companyVisible)
                            {
                                sbSP.Append("<td border='1' width='20%' valign='top' align='left'>");
                                sbSP.Append(dtSP.Rows[i]["CompanyName"].ToString());
                                sbSP.Append("</td>");
                            }
                            else
                            {

                            }
                            sbSP.Append("<td border='1' width='40%' valign='top' align='left'>");
                            sbSP.Append(dtSP.Rows[i]["CategoryDesc"].ToString());
                            sbSP.Append("</td>");
                            sbSP.Append("<td border='1' width='20%' valign='top' align='left'>");
                            if (SubmittedValue != "")
                            {
                               
                                //sbSP.Append(imgPath + "&nbsp;&nbsp;(" + SubmittedValue+ ")");
                                sbSP.Append("<table align='left' border='0' width='100%'><tr><td align='right' width='50%'>" + imgPath + "</td>");
                                sbSP.Append("<td align='left' width='50%'>" + "(" + SubmittedValue + ")" + "</td><tr></table>");

                            }
                            else
                            {
                                sbSP.Append("<table border='0' width='100%'><tr><td>" + imgPath + "</td></tr></table>");
                            }
                            sbSP.Append("</td>");

                            sbSP.Append("<td border='1' width='20%' valign='top' align='left'>");
                            sbSP.Append(dtSP.Rows[i]["StatusName"].ToString());
                            sbSP.Append("</td>");
                            sbSP.Append("</tr>");
                        }


                    }
                    else
                    {
                        sbSP.Append("<tr>");
                        sbSP.Append("<td border='1' colspan='" + colspan + "' width='100%' valign='top' align='Center'>");
                        sbSP.Append("No Safety Plan submitted.");
                        sbSP.Append("</td>");
                        sbSP.Append("</tr>");
                    }
                    sbSP.Append("</table>");
                }
            }


            
            #endregion


        #region Safety Frequency Rates


            StringBuilder sbSFR = new StringBuilder();



            if (Session["CompanyReportSFR"] != null)
            {
                DataTable dtSFR1 = (DataTable)Session["CompanyReportSFR"];
                DataView dvSFR = new DataView(dtSFR1);
                dvSFR.Sort = "CompanySiteCategoryId,SiteName ASC";
                DataTable dtSFR = dvSFR.ToTable();

                sbSFR.Append(headerFormat.Replace("[Header]", "Safety Frequency Rates"));
                sbSFR.Append("<table width='100%' border='1' align='left' valign='top' style='font-size:7px; color:Black;'>");
                sbSFR.Append("<tr>");
                sbSFR.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Residential Category</td>");
                sbSFR.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Site Name</td>");

                sbSFR.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>LWDFR</td>");
                sbSFR.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>TRIFR</td>");
                sbSFR.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>AIFR</td>");
                sbSFR.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>IFE: Injury Ratio</td>");

                sbSFR.Append("</tr>");

                if (dtSFR.Rows.Count > 0)
                {

                    for (int k = 0; k < dtSFR.Rows.Count; k++)
                    {
                        sbSFR.Append("<tr>");

                        sbSFR.Append("<td border='1' valign='top' align='left'>");
                        if (dtSFR.Rows[k]["CompanySiteCategoryId"] != null)
                        {
                            CompanySiteCategoryService compSiSer = new CompanySiteCategoryService();
                            CompanySiteCategory compSi = compSiSer.GetByCompanySiteCategoryId(Convert.ToInt32(dtSFR.Rows[k]["CompanySiteCategoryId"]));

                            sbSFR.Append(compSi.CategoryDesc);
                        }
                        sbSFR.Append("</td>");


                        sbSFR.Append("<td border='1' valign='top' align='left'>");
                        if (dtSFR.Rows[k]["SiteName"] != null)
                        {
                            sbSFR.Append(Convert.ToString(dtSFR.Rows[k]["SiteName"]));
                        }
                        sbSFR.Append("</td>");

                        sbSFR.Append("<td border='1' valign='top' align='Center'>");
                        if (dtSFR.Rows[k]["LWDFR"] != null)
                        {
                            sbSFR.Append(Convert.ToString(dtSFR.Rows[k]["LWDFR"]));
                        }
                        sbSFR.Append("</td>");

                        sbSFR.Append("<td border='1' valign='top' align='Center'>");
                        if (dtSFR.Rows[k]["TRIFR"] != null)
                        {
                            sbSFR.Append(Convert.ToString(dtSFR.Rows[k]["TRIFR"]));
                        }
                        sbSFR.Append("</td>");

                        sbSFR.Append("<td border='1' valign='top' align='Center'>");
                        if (dtSFR.Rows[k]["AIFR"] != null)
                        {
                            sbSFR.Append(Convert.ToString(dtSFR.Rows[k]["AIFR"]));
                        }
                        sbSFR.Append("</td>");

                        sbSFR.Append("<td border='1' valign='top' align='Center'>");
                        if (dtSFR.Rows[k]["IFEIR"] != null)
                        {
                            sbSFR.Append(Convert.ToString(dtSFR.Rows[k]["IFEIR"]));
                        }
                        sbSFR.Append("</td>");
                    }
                    sbSFR.Append("</tr>");
                    
                }
                else
                {
                    sbSFR.Append("<tr>");
                    sbSFR.Append("<td border='1' width='100%' colspan='6' valign='top' align='Center'>");
                    sbSFR.Append("No Documents for Safety Frequency Rates submitted.");
                    sbSFR.Append("</td>");
                    sbSFR.Append("</tr>");
                }
                sbSFR.Append("</table>");
                string SFR = sbSFR.ToString();
            } 
            #endregion



        #region Medical Schedules


        StringBuilder sbMS = new StringBuilder();
       

        //string imgTlYellow = "<img src='" + imgPathTLYellow + "' alt='Cross'/>";
        //string imgTlRed = "<img src='" + imgPathTLRed + "' alt='Cross'/>";
        //string imgTLGreen = "<img src='" + imgPathTLGreen + "' alt='Cross'/>";
        bool isSite=false;
        colspan = 0;
        if (Session["CompanyReportMS"] != null)
        {
            DataTable dtTableMS1 = (DataTable)Session["CompanyReportMS"];
            DataView dvTableMS = new DataView(dtTableMS1);
            dvTableMS.Sort = "CompanySiteCategoryId,SiteName ASC";
            DataTable dtTableMS = dvTableMS.ToTable();

            if (SessionHandler.spVar_Page == "ComplianceReport")
            {

                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                {
                    isCompany = true;
                }
                else
                {
                    isCompany = false;
                    colspan++;
                }
            }
            else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
            {
                if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                {
                    isSite = true;
                }
                else
                {
                    isSite = false;
                    colspan++;
                }
            }



            //Medical Schedules
            //sbMS.Append("<h3>Medical Schedules</h3><br/>");
            sbMS.Append(headerFormat.Replace("[Header]", "Medical Schedules"));

            sbMS.Append("<table width='100%' align='left' valign='top' border='1' style='font-size:7px; color:Black;'>");
            sbMS.Append("<tr>");
            if (!isCompany)
            {
                sbMS.Append("<td border='1'  align='left' valign='top' width='20%' bgcolor='" + headerBgColor + "' style='font-weight:bold;'>Company Name</td>");
                colspan++;
            }
            sbMS.Append("<td border='1' width='30%' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Residential Category</td>");

            if (!isSite)
            {
                sbMS.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Site Name</td>");
                colspan++;
            }
            sbMS.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 1</td>");
            sbMS.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 2</td>");
            sbMS.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 3</td>");
            sbMS.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 4</td>");
            sbMS.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Year</td>");
            colspan = colspan + 6;

            sbMS.Append("</tr>");

            if (dtTableMS.Rows.Count > 0)
            {
                for (int n = 0; n < dtTableMS.Rows.Count; n++)
                {
                    sbMS.Append("<tr>");


                    if (!isCompany)
                    {
                        CompaniesService compSer = new CompaniesService();
                        Companies comp = compSer.GetByCompanyId(Convert.ToInt32(dtTableMS.Rows[n]["CompanyId"]));


                        sbMS.Append("<td width='20%' border='1' valign='top' align='left'>");
                        if (comp != null)
                        {
                            sbMS.Append(comp.CompanyName);
                        }
                        sbMS.Append("</td>");
                    }

                    sbMS.Append("<td border='1' align='left' width='30%' valign='top'>");
                    if (dtTableMS.Rows[n]["CompanySiteCategoryId"] != null)
                    {
                        CompanySiteCategoryService compSiSer = new CompanySiteCategoryService();
                        CompanySiteCategory compSi = compSiSer.GetByCompanySiteCategoryId(Convert.ToInt32(dtTableMS.Rows[n]["CompanySiteCategoryId"]));

                        sbMS.Append(compSi.CategoryDesc);
                    }
                    sbMS.Append("</td>");

                    if (!isSite)
                    {
                        sbMS.Append("<td border='1' align='left' width='20%' valign='top'>");
                        if (dtTableMS.Rows[n]["SiteName"] != null)
                        {
                            sbMS.Append(Convert.ToString(dtTableMS.Rows[n]["SiteName"]));
                        }
                        sbMS.Append("</td>");
                    }



                   

                    if (month >= 0)
                    {
                        if (Convert.ToInt32(dtTableMS.Rows[n]["Qtr1"]) != 0)
                        {
                            qtrColor1 = "Green";
                        }
                        else
                        {
                            qtrColor1 = "Red";
                        }
                        qtrValue1 = dtTableMS.Rows[n]["Qtr1"].ToString();
                        if (month >= 4)
                        {
                            if (Convert.ToInt32(dtTableMS.Rows[n]["Qtr2"]) != 0)
                            {
                                qtrColor2 = "Green";
                            }
                            else
                            {
                                qtrColor2 = "Red";
                            }
                            qtrValue2 = dtTableMS.Rows[n]["Qtr2"].ToString();
                            if (month >= 7)
                            {
                                if (Convert.ToInt32(dtTableMS.Rows[n]["Qtr3"]) != 0)
                                {
                                    qtrColor3 = "Green";
                                }
                                else
                                {
                                    qtrColor3 = "Red";
                                }
                                qtrValue3 = dtTableMS.Rows[n]["Qtr3"].ToString();
                                if (month >= 10)
                                {
                                    if (Convert.ToInt32(dtTableMS.Rows[n]["Qtr4"]) != 0)
                                    {
                                        qtrColor4 = "Green";
                                    }
                                    else
                                    {
                                        qtrColor4 = "Red";
                                    }
                                    qtrValue4 = dtTableMS.Rows[n]["Qtr4"].ToString();
                                }
                            }
                        }


                    }



                    sbMS.Append("<td border='1' align='center' width='10%' valign='top'>");
                    if (qtrColor1 == "Green")
                    {                        
                        sbMS.Append(imgGreenTick);
                        //sbMS.Append("&nbsp;");
                       // sbMS.Append(qtrValue1);
                    }
                    else if (qtrColor1 == "Red")
                    {
                        //sbMS.Append("&nbsp;&nbsp;&nbsp;");
                        sbMS.Append(imgRedCross);
                    }
                    sbMS.Append("</td>");


                    sbMS.Append("<td border='1' width='10%' valign='top' align='center'>");
                    if (qtrColor2 == "Green")
                    {                       
                        sbMS.Append(imgGreenTick);
                       // sbMS.Append("&nbsp;");
                        //sbMS.Append(qtrValue2);
                    }
                    else if (qtrColor2 == "Red")
                    {                        
                        sbMS.Append(imgRedCross);
                    }
                    sbMS.Append("</td>");

                    sbMS.Append("<td border='1' width='10%' valign='top' align='center'>");
                    if (qtrColor3 == "Green")
                    {                        
                        sbMS.Append(imgGreenTick);
                        //sbMS.Append("&nbsp;");
                        //sbMS.Append(qtrValue3);
                    }
                    else if (qtrColor3 == "Red")
                    {                       
                        sbMS.Append(imgRedCross);
                    }
                    sbMS.Append("</td>");

                    sbMS.Append("<td border='1' align='center' width='10%' valign='top'>");
                    if (qtrColor4 == "Green")
                    {                       
                        sbMS.Append(imgGreenTick);
                       // sbMS.Append("&nbsp;");
                       // sbMS.Append(qtrValue4);
                    }
                    else if (qtrColor4 == "Red")
                    {                       
                        sbMS.Append(imgRedCross);
                    }
                    sbMS.Append("</td>");


                    
                    if (dtTableMS.Rows[n]["Year"] != null)
                    {
                        if (Convert.ToInt32(dtTableMS.Rows[n]["Year"]) > 0)
                        {
                            sbMS.Append("<td border='1' align='Center' width='10%' valign='top' style='color:Green'>");
                        }
                        else
                        {
                            if (Convert.ToInt32(dtTableMS.Rows[n]["Year"]) == -1)
                            {
                                sbMS.Append("<td border='1' align='Center' width='10%' valign='top' style='color:Black'>");
                            }
                            else
                            {
                                sbMS.Append("<td border='1' align='Center' width='10%' valign='top' style='color:Red'>");
                            }
                        }
                        sbMS.Append(Convert.ToString(dtTableMS.Rows[n]["Year"]));
                    }
                    sbMS.Append("</td>");
                    sbMS.Append("</tr>");
                }
            }
            else
            {
                sbMS.Append("<tr>");
                sbMS.Append("<td border='1' colspan='" + colspan + "' align='Center' width='100%' valign='top' style='color:Black'>");
                sbMS.Append("No Medical Schedule(s) submitted.");
                sbMS.Append("</td>");
                sbMS.Append("</tr>");
            }
                
            sbMS.Append("</table>");
           
        }

        string strsbMS = sbMS.ToString();
        
        #endregion



        #region KPI

        StringBuilder sbKPI = new StringBuilder();

        if (Session["ComplianceReport_KPI"] != null)
        {
            DataTable dtKPI1 = (DataTable)Session["ComplianceReport_KPI"];
            DataView dvKPI = new DataView(dtKPI1);
            dvKPI.Sort = "CompanySiteCategoryId,SiteName ASC";
            DataTable dtKPI = dvKPI.ToTable();

            int tdCount = 0;
           
            sbKPI.Append(headerFormat.Replace("[Header]", "KPI"));

            sbKPI.Append("<table width='100%' border='1' align='left' valign='top' style='font-size:7px; color:Black;'>");
            sbKPI.Append("<tr>");

            if (!isCompany)
            {
                sbKPI.Append("<td border='1' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold; border:1px solid gray;'>Company Name</td>");
                tdCount = 16;
            }
            else
            {
                tdCount = 15;
            }

            sbKPI.Append("<td border='1' width='20%' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Residential Category</td>");
            sbKPI.Append("<td border='1' width='15%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Site Name</td>");

            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jan</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Feb</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Mar</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Apr</td>");

            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>May</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jun</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Jul</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Aug</td>");

            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Sep</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Oct</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Nov</td>");
            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Dec</td>");

            sbKPI.Append("<td border='1' width='5%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Year</td>");
            sbKPI.Append("</tr>");


            if (dtKPI.Rows.Count > 0)
            {
                for (int m = 0; m < dtKPI.Rows.Count; m++)
                {
                    sbKPI.Append("<tr>");

                    if (!isCompany)
                    {

                        CompaniesService compSer = new CompaniesService();
                        Companies comp = compSer.GetByCompanyId(Convert.ToInt32(dtKPI.Rows[m]["CompanyId"]));


                        sbKPI.Append("<td border='1' align='left' valign='top'>");
                        if (comp != null)
                        {
                            sbKPI.Append(comp.CompanyName);
                        }
                        sbKPI.Append("</td>");
                    }

                    bool embedded = false;
                    int intCat = Convert.ToInt32(dtKPI.Rows[m]["CompanySiteCategoryId"]);
                    if (intCat == 1) embedded = true;


                    sbKPI.Append("<td border='1' width='20%' align='left' valign='top' align='left'>");
                    if (dtKPI.Rows[m]["CompanySiteCategoryId"] != null)
                    {
                        CompanySiteCategoryService compSiSer = new CompanySiteCategoryService();
                        CompanySiteCategory compSi = compSiSer.GetByCompanySiteCategoryId(Convert.ToInt32(dtKPI.Rows[m]["CompanySiteCategoryId"]));

                        sbKPI.Append(compSi.CategoryDesc);
                    }
                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='15%' valign='top' align='left'>");
                    TList<Sites> sList;
                    int i = 0;
                    
                    SitesFilters sFilters = new SitesFilters();
                    sFilters.AppendIsNotNull(SitesColumn.SiteNameEbi);
                    sFilters.Junction = "AND";
                    sFilters.AppendNotEquals(SitesColumn.SiteNameEbi, "");
                    sList = DataRepository.SitesProvider.GetPaged(sFilters.ToString(), null, 0, 999999, out i);
                    int j = 0;

                    foreach (Sites s in sList)
                    {
                        if (Convert.ToString(dtKPI.Rows[m]["SiteName"]) == s.SiteName)
                        {
                            j++;
                        }
                    }
                    


                    if (dtKPI.Rows[m]["SiteName"] != null && j == 0)
                    {
                        sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["SiteName"]) + "*");
                    }
                    else if (dtKPI.Rows[m]["SiteName"] != null)
                    {
                        sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["SiteName"]));
                    }
                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Jan"] != null && dtKPI.Rows[m]["Jan"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Jan"]) != -1)
                    {
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Jan"]), imgPathRedCross,imgPathGreenTick, imgPathTLYellow));
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Feb"] != null && dtKPI.Rows[m]["Feb"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Feb"]) != -1)
                    {
                        //sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Feb"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Feb"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Mar"] != null && dtKPI.Rows[m]["Mar"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Mar"]) != -1)
                    {
                        //sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Mar"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Mar"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1'  width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Apr"] != null && dtKPI.Rows[m]["Apr"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Apr"]) != -1)
                    {
                        //sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Apr"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Apr"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1'  width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["May"] != null && dtKPI.Rows[m]["May"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["May"]) != -1)
                    {
                       // sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["May"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["May"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1'  width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Jun"] != null && dtKPI.Rows[m]["Jun"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Jun"]) != -1)
                    {
                        //sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Jun"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Jun"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1'  width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Jul"] != null && dtKPI.Rows[m]["Jul"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Jul"]) != -1)
                    {
                       // sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Jul"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Jul"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1'  width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Aug"] != null && dtKPI.Rows[m]["Aug"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Aug"]) != -1)
                    {
                       // sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Aug"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Aug"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Sep"] != null && dtKPI.Rows[m]["Sep"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Sep"]) != -1)
                    {
                       // sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Sep"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Sep"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Oct"] != null && dtKPI.Rows[m]["Oct"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Oct"]) != -1)
                    {
                        //sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Oct"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Oct"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Nov"] != null && dtKPI.Rows[m]["Nov"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Nov"]) != -1)
                    {
                       // sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Nov"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Nov"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    sbKPI.Append("<td border='1' width='5%' valign='top' align='Center'>");
                    if (dtKPI.Rows[m]["Dec"] != null && dtKPI.Rows[m]["Dec"].ToString() != "" && Convert.ToInt32(dtKPI.Rows[m]["Dec"]) != -1)
                    {
                       // sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Dec"]));
                        sbKPI.Append(ReturnKPIValues(embedded, Convert.ToInt32(dtKPI.Rows[m]["Dec"]), imgPathRedCross, imgPathGreenTick, imgPathTLYellow));
                    
                    }

                    sbKPI.Append("</td>");

                    
                    if (dtKPI.Rows[m]["Year"] != null && dtKPI.Rows[m]["Year"].ToString() != "")
                    {
                        if (Convert.ToInt32(dtKPI.Rows[m]["Year"]) > 0)
                        {
                            sbKPI.Append("<td border='1' color='green' width='5%' valign='top' align='Center'>");
                        }
                        else
                        {
                            sbKPI.Append("<td border='1' color='black' width='5%' valign='top' align='Center'>");
                        }
        
                        sbKPI.Append(Convert.ToString(dtKPI.Rows[m]["Year"]));
                    }

                    sbKPI.Append("</td>");
                     sbKPI.Append("</tr>");
                }               
            }
            else
            {
                sbKPI.Append("<tr>");
                sbKPI.Append("<td border='1' colspan='" + tdCount + "' width='100%' valign='top' align='Center'>");
                sbKPI.Append("No KPI documetns submitted.");
                sbKPI.Append("</td>");
                sbKPI.Append("</tr>");
            }
            sbKPI.Append("</table>");
            sbKPI.Append("<br/>");


        }

        string strKPI = sbKPI.ToString();
        
        #endregion


       
        #region Training Schedules


       
       // string imgPathRedCross = WebConfigurationManager.AppSettings["PDFImageRedCross"].ToString();
       //string imgRedCross = "<img src='" + imgPathRedCross + "' alt='Cross'/>";
        StringBuilder sbTS = new StringBuilder();

        if (lblTrainingSchedules.Text == "Training Schedules - n/a")
        {
            sbTS.Append("<h4 style='color:#110c84'>Training Schedules - n/a</h4><br/>");
        }
        else
        {

            if (Session["ComplianceReportTS"] != null)
            {
                DataTable dtTableTS1 = (DataTable)Session["ComplianceReportTS"];
                DataView dvTableTS = new DataView(dtTableTS1);
                dvTableTS.Sort = "CompanySiteCategoryId,SiteName ASC";
                DataTable dtTableTS = dvTableTS.ToTable();
                colspan = 0;

                //Training Schedules
                // sbTS.Append("<h3>Training Schedules</h3><br/>");
                sbTS.Append(headerFormat.Replace("[Header]", "Training Schedules"));

                sbTS.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
                sbTS.Append("<tr>");

                if (!isCompany)
                {
                    sbTS.Append("<td border='1' width='20%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Company Name</td>");
                    colspan++;
                }

                sbTS.Append("<td border='1' width='15%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Residential Category</td>");

                if (!isSite)
                {
                    sbTS.Append("<td border='1' width='30%' bgcolor='" + headerBgColor + "' align='left' valign='top' style='font-weight:bold;'>Site Name</td>");
                    colspan++;
                }
                sbTS.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr 1</td>");
                sbTS.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr 2</td>");
                sbTS.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr 3</td>");
                sbTS.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Qtr 4</td>");
                sbTS.Append("<td border='1' width='10%' bgcolor='" + headerBgColor + "' align='center' valign='top' style='font-weight:bold;'>Year</td>");
                colspan = colspan + 6;
                sbTS.Append("</tr>");

                if (dtTableTS.Rows.Count > 0)
                {
                    for (int p = 0; p < dtTableTS.Rows.Count; p++)
                    {
                        sbTS.Append("<tr>");


                        if (!isCompany)
                        {

                            CompaniesService compSer = new CompaniesService();
                            Companies comp = compSer.GetByCompanyId(Convert.ToInt32(dtTableTS.Rows[p]["CompanyId"]));


                            sbTS.Append("<td border='1' width='20%' align='left' valign='top'>");
                            if (comp != null)
                            {
                                sbTS.Append(comp.CompanyName);
                            }
                            sbTS.Append("</td>");
                        }


                        sbTS.Append("<td border='1' width='30%' align='left' valign='top'>");
                        if (dtTableTS.Rows[p]["CompanySiteCategoryId"] != null)
                        {
                            CompanySiteCategoryService compSiSer = new CompanySiteCategoryService();
                            CompanySiteCategory compSi = compSiSer.GetByCompanySiteCategoryId(Convert.ToInt32(dtTableTS.Rows[p]["CompanySiteCategoryId"]));

                            sbTS.Append(compSi.CategoryDesc);
                        }
                        sbTS.Append("</td>");

                        if (!isSite)
                        {
                            sbTS.Append("<td border='1' width='20%' align='left' valign='top'>");
                        }
                        if (dtTableTS.Rows[p]["SiteName"] != null)
                        {
                            sbTS.Append(Convert.ToString(dtTableTS.Rows[p]["SiteName"]));
                        }
                        sbTS.Append("</td>");



                        if (month >= 0)
                        {
                            if (Convert.ToInt32(dtTableTS.Rows[p]["Qtr1"]) != 0)
                            {
                                qtrColor1 = "Green";
                            }
                            else
                            {
                                qtrColor1 = "Red";
                            }
                            qtrValue1 = dtTableTS.Rows[p]["Qtr1"].ToString();
                            if (month >= 4)
                            {
                                if (Convert.ToInt32(dtTableTS.Rows[p]["Qtr2"]) != 0)
                                {
                                    qtrColor2 = "Green";
                                }
                                else
                                {
                                    qtrColor2 = "Red";
                                }
                                qtrValue2 = dtTableTS.Rows[p]["Qtr2"].ToString();
                                if (month >= 7)
                                {
                                    if (Convert.ToInt32(dtTableTS.Rows[p]["Qtr3"]) != 0)
                                    {
                                        qtrColor3 = "Green";
                                    }
                                    else
                                    {
                                        qtrColor3 = "Red";
                                    }
                                    qtrValue3 = dtTableTS.Rows[p]["Qtr3"].ToString();
                                    if (month >= 10)
                                    {
                                        if (Convert.ToInt32(dtTableTS.Rows[p]["Qtr4"]) != 0)
                                        {
                                            qtrColor4 = "Green";
                                        }
                                        else
                                        {
                                            qtrColor4 = "Red";
                                        }
                                        qtrValue4 = dtTableTS.Rows[p]["Qtr4"].ToString();
                                    }
                                }
                            }


                        }


                        sbTS.Append("<td border='1' align='Center' width='10%' valign='top'>");
                        if (qtrColor1 == "Green")
                        {
                            sbTS.Append(imgGreenTick);
                            //sbTS.Append("&nbsp;");
                            //sbTS.Append(qtrValue1);
                        }
                        else if (qtrColor1 == "Red")
                        {
                            sbTS.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                        }
                        sbTS.Append("</td>");


                        sbTS.Append("<td border='1' align='Center' width='10%' valign='top'>");
                        if (qtrColor2 == "Green")
                        {
                            sbTS.Append(imgGreenTick);
                            //sbTS.Append("&nbsp;");
                            // sbTS.Append(qtrValue2);
                        }
                        else if (qtrColor2 == "Red")
                        {
                            //sbTS.Append(imgRedCross);
                            sbTS.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                        }
                        sbTS.Append("</td>");

                        sbTS.Append("<td border='1' align='Center' width='10%' valign='top'>");
                        if (qtrColor3 == "Green")
                        {
                            sbTS.Append(imgGreenTick);
                            // sbTS.Append("&nbsp;");
                            // sbTS.Append(qtrValue3);
                        }
                        else if (qtrColor3 == "Red")
                        {
                            sbTS.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                        }
                        sbTS.Append("</td>");



                        sbTS.Append("<td border='1' align='Center' width='10%' valign='top'>");
                        if (qtrColor4 == "Green")
                        {
                            sbTS.Append(imgGreenTick);
                            // sbTS.Append("&nbsp;");
                            // sbTS.Append(qtrValue4);
                        }
                        else if (qtrColor4 == "Red")
                        {
                            sbTS.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                        }
                        sbTS.Append("</td>");


                        if (dtTableTS.Rows[p]["Year"] != null)
                        {
                            if (Convert.ToInt32(dtTableTS.Rows[p]["Year"]) > 0)
                            {
                                sbTS.Append("<td border='1' align='Center' width='10%' valign='top' style='color:Green'>");
                            }
                            else
                            {
                                if (Convert.ToInt32(dtTableTS.Rows[p]["Year"]) == -1)
                                {
                                    sbTS.Append("<td border='1' align='Center' width='10%' valign='top' style='color:Black'>");
                                }
                                else
                                {
                                    sbTS.Append("<td border='1' align='Center' width='10%' valign='top' style='color:Red'>");
                                }
                            }
                            sbTS.Append(Convert.ToString(dtTableTS.Rows[p]["Year"]));
                        }
                        sbTS.Append("</td>");
                        sbTS.Append("</tr>");
                    }


                }
                else
                {
                    sbTS.Append("</tr>");
                    sbTS.Append("<td border='1' colspan='" + colspan + "' align='Center' width='100%' valign='top'>");
                    sbTS.Append("No Training Plan(s) submitted.");
                    sbTS.Append("</td>");
                    sbTS.Append("</tr>");
                }
                sbTS.Append("</table>");

            }
        }

        string strsbTS = sbTS.ToString();

        #endregion



        #region Contractor Service Audit Embedded
        bool isQtr = true;
        isSite = true;
        colspan = 0;

        if (SessionHandler.spVar_Page == "ComplianceReport")
        {
            if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
            {
                //isCompany = true;

                if (Convert.ToInt32(SessionHandler.spVar_CategoryId) == 1 || Convert.ToInt32(SessionHandler.spVar_CategoryId) == -1)
                {
                    isCompany = true;
                }
                if (Convert.ToInt32(SessionHandler.spVar_CategoryId) == 2 || Convert.ToInt32(SessionHandler.spVar_CategoryId) == -1)
                {
                    isQtr = false;
                }
            }
            else
            {
                isCompany = false;
            }

        }
        else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
        {
            if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
            {
                isSite = false;
            }
            else //Region
            {
                isSite = true;
            }
        }


       
       expected=0;
        qtrColor1 = string.Empty;
        qtrColor2 = string.Empty;
        qtrColor3 = string.Empty;
        qtrColor4 = string.Empty;

        qtrValue1 = string.Empty;
        qtrValue2 = string.Empty;
        qtrValue3 = string.Empty;
        qtrValue4 = string.Empty;

        

        string hlfValue1 = string.Empty;
        string hlfValue2 = string.Empty;

        string hlfcolor1 = string.Empty;
        string hlfcolor2 = string.Empty;

        string AlcoaAudit1 = string.Empty;
        string AlcoaAudit2 = string.Empty;

        string AlcoaAuditColor1 = string.Empty;
        string AlcoaAuditColor2 = string.Empty;
        string AlcoaAuditImg = string.Empty;

        if (SessionHandler.spVar_Year == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
        }
        else
        {
            month = 12;
            expected = 4;
        }

        StringBuilder sbCSA = new StringBuilder();
        if (Session["ComplianceReportCSA1"] != null)
        {

            DataTable dtDSA1 = (DataTable)Session["ComplianceReportCSA1"];


            if (dtDSA1 != null)
            {
                sbCSA.Append(headerFormat.Replace("[Header]", "Contractor Services Audit - Embedded"));
                //sbCSA.Append("<h3>Contractor Services Audit - Embedded</h3><br/>");

                sbCSA.Append("<table width='100%' align='left' valign='top' border='1' style='font-size:7px; color:Black;'>");
                sbCSA.Append("<tr>");

                if (!isCompany)
                {
                    sbCSA.Append("<td border='1' width='20%' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Company Name</td>");
                    colspan++;
                }

                sbCSA.Append("<td border='1' width='30%' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Residential Category</td>");

                if (isSite)
                {
                    sbCSA.Append("<td border='1' width='20%' bgcolor='"+ headerBgColor +"' align='left' valign='top' style='font-weight:bold;'>Site Name</td>");
                    colspan++;
                }

                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 1</td>");
                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 2</td>");
                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 3</td>");
                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 4</td>");
                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Alcoa Audit</td>");
                colspan = colspan + 6;
                
                sbCSA.Append("</tr>");
               
                if (dtDSA1.Rows.Count > 0)
                {
                   
                      for (int p = 0; p < dtDSA1.Rows.Count; p++)
                        {
                            hlfValue1 = string.Empty;
                           hlfValue2 = string.Empty;

                            hlfcolor1 = string.Empty;
                           hlfcolor2 = string.Empty;

                            sbCSA.Append("<tr>");
                            if (!isCompany)
                            {

                                CompaniesService compSer = new CompaniesService();
                                Companies comp = compSer.GetByCompanyId(Convert.ToInt32(dtDSA1.Rows[p]["CompanyId"]));


                                sbCSA.Append("<td border='1' width='20%'  align='left' valign='top'>");
                                if (comp != null)
                                {
                                    sbCSA.Append(comp.CompanyName);
                                }
                                sbCSA.Append("</td>");
                            }


                            sbCSA.Append("<td border='1' width='30%'  align='left' valign='top'>");
                            sbCSA.Append(dtDSA1.Rows[p]["CategoryDesc"].ToString());
                            sbCSA.Append("</td>");

                            if (isSite)
                            {
                                sbCSA.Append("<td border='1' width='20%'  align='left' valign='top'>");
                                if (dtDSA1.Rows[p]["SiteId"] != null)
                                {
                                    SitesService Siser = new SitesService();
                                    Sites si = Siser.GetBySiteId(Convert.ToInt32(dtDSA1.Rows[p]["SiteId"]));
                                    sbCSA.Append(Convert.ToString(si.SiteName));
                                }
                                sbCSA.Append("</td>");
                            }

                            if (month >= 0)
                            {
                                if (Convert.ToInt32(dtDSA1.Rows[p]["Qtr 1"]) != 0)
                                {
                                    qtrColor1 = "Green";
                                }
                                else
                                {
                                    qtrColor1 = "Red";
                                }
                                qtrValue1 = dtDSA1.Rows[p]["Qtr 1"].ToString();
                                if (month >= 4)
                                {
                                    if (Convert.ToInt32(dtDSA1.Rows[p]["Qtr 2"]) != 0)
                                    {
                                        qtrColor2 = "Green";
                                    }
                                    else
                                    {
                                        qtrColor2 = "Red";
                                    }
                                    qtrValue2 = dtDSA1.Rows[p]["Qtr 2"].ToString();
                                    if (month >= 7)
                                    {
                                        if (Convert.ToInt32(dtDSA1.Rows[p]["Qtr 3"]) != 0)
                                        {
                                            qtrColor3 = "Green";
                                        }
                                        else
                                        {
                                            qtrColor3 = "Red";
                                        }
                                        qtrValue3 = dtDSA1.Rows[p]["Qtr 3"].ToString();
                                        if (month >= 10)
                                        {
                                            if (Convert.ToInt32(dtDSA1.Rows[p]["Qtr 4"]) != 0)
                                            {
                                                qtrColor4 = "Green";
                                            }
                                            else
                                            {
                                                qtrColor4 = "Red";
                                            }
                                            qtrValue4 = dtDSA1.Rows[p]["Qtr 4"].ToString();
                                        }
                                    }
                                }


                            }



                            sbCSA.Append("<td border='1' width='10%' valign='top' style='color:" + qtrColor1 + "' align='center'>");
                            if (qtrColor1 == "Green")
                            {
                                sbCSA.Append("<table border='0' width='100%'><tr><td width='50%'>" + imgGreenTick + "</td>");                                
                                sbCSA.Append("<td width='50%'>" + "(" + qtrValue1 + ")" + "</td><tr></table>");
                            }
                            else if (qtrColor1 == "Red")
                            {
                                sbCSA.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                            }
                            sbCSA.Append("</td>");


                            sbCSA.Append("<td border='1'  width='10%' valign='top' style='color:" + qtrColor2 + "' align='center'>");
                            if (qtrColor2 == "Green")
                            {
                                //sbCSA.Append(imgGreenTick);
                                //sbCSA.Append("&nbsp;");
                               //sbCSA.Append("("+qtrValue2+")");
                                sbCSA.Append("<table border='0' width='100%'><tr><td align='right' width='50%'>" + imgGreenTick + "</td>");
                                sbCSA.Append("<td align='left' width='50%'>" + "(" + qtrValue2 + ")" + "</td><tr></table>");
                            }
                            else if (qtrColor2 == "Red")
                            {
                                sbCSA.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                            }
                            sbCSA.Append("</td>");

                            sbCSA.Append("<td border='1'  width='10%' valign='top' style='color:" + qtrColor3 + "' align='center'>");
                            if (qtrColor2 == "Green")
                            {
                                //sbCSA.Append(imgGreenTick);
                                //sbCSA.Append("&nbsp;");
                               // sbCSA.Append("(" + qtrValue3+")");

                                sbCSA.Append("<table border='0' width='100%'><tr><td align='right' width='50%'>" + imgGreenTick + "</td>");
                                sbCSA.Append("<td align='left' width='50%'>" + "(" + qtrValue3 + ")" + "</td><tr></table>");
                            }
                            else if (qtrColor3 == "Red")
                            {
                                sbCSA.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                            }
                            sbCSA.Append("</td>");

                            sbCSA.Append("<td border='1'  width='10%' valign='top' style='color:" + qtrColor4 + "' align='center'>");
                            if (qtrColor4 == "Green")
                            {
                                //sbCSA.Append(imgGreenTick);
                                //sbCSA.Append("&nbsp;");
                               // sbCSA.Append("(" + qtrValue4 + ")");


                                sbCSA.Append("<table border='0' width='100%'><tr><td align='right' width='50%'>" + imgGreenTick + "</td>");
                                sbCSA.Append("<td align='left' width='50%'>" + "(" + qtrValue4 + ")" + "</td><tr></table>");
                            }
                            else if (qtrColor4 == "Red")
                            {
                                sbCSA.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                            }
                            sbCSA.Append("</td>");

                            if (dtDSA1.Rows[p]["Alcoa Audit"] != null)
                            {
                                if (Convert.ToInt32(dtDSA1.Rows[p]["Alcoa Audit"]) != 0)
                                {
                                    AlcoaAuditColor1 = "Green";
                                    AlcoaAudit1 = "(" + dtDSA1.Rows[p]["Alcoa Audit"].ToString() + ")";
                                    AlcoaAuditImg = imgGreenTick;

                                    
                                }
                                else
                                {
                                    AlcoaAuditColor1 = "Red";
                                    AlcoaAuditImg = imgRedCross;
                                }
                            }


                            sbCSA.Append("<td border='1'  width='10%' valign='top' style='color:" + AlcoaAuditColor1 + "' align='center'>");
                            //sbCSA.Append(AlcoaAuditImg);
                            //sbCSA.Append(AlcoaAudit1);
                            sbCSA.Append("<table border='0' width='100%'><tr><td align='right' width='50%'>" + AlcoaAuditImg + "</td>");
                            sbCSA.Append("<td align='left' width='50%'>" + AlcoaAudit1 + "</td><tr></table>");
                            sbCSA.Append("</td>");
                            sbCSA.Append("</tr>");
                    }
                   
                }
                else
                {
                    sbCSA.Append("<tr>");
                    sbCSA.Append("<td valign='top' width='100%' align='center' colspan='" + colspan + "'>");
                    sbCSA.Append("No Contractor Services Audit(s) submitted");
                    sbCSA.Append("</td>");
                    sbCSA.Append("</tr>");
                }

                sbCSA.Append("</table>");
                sbCSA.Append("<br/>");
            }
            string strSBCSA = sbCSA.ToString();
        } 
        #endregion

        #region Contractor Service Audit Non Embeded1
        if (Session["ComplianceReportCSA2"] != null)
        {
            DataTable dtDSA2 = (DataTable)Session["ComplianceReportCSA2"];
            if (dtDSA2 != null)
            {
                //sbCSA.Append("<h3>Contractor Services Audit - Non Embedded 1</h3><br/>");
                sbCSA.Append(headerFormat.Replace("[Header]", "Contractor Services Audit - Non Embedded 1"));

                sbCSA.Append("<table width='100%' align='center' valign='top' border='1' style='font-size:7px; color:Black;'>");
                sbCSA.Append("<tr>");

                //if (!isCompany)
                //{
                //    sbCSA.Append("<td border='1' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Company Name</td>");
                //}

                sbCSA.Append("<td border='1' width='15%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Residential Category</td>");


                sbCSA.Append("<td border='1' width='45%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Site Name</td>");


                //sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 1</td>");
                //sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Qtr 2</td>");
                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Half 1</td>");
                sbCSA.Append("<td border='1' width='10%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Half 2</td>");
                sbCSA.Append("<td border='1' width='20%' bgcolor='"+ headerBgColor +"' align='center' valign='top' style='font-weight:bold;'>Alcoa Audit</td>");

                sbCSA.Append("</tr>");


                if (dtDSA2.Rows.Count > 0)
                {
                    #region forLoop
                    for (int p = 0; p < dtDSA2.Rows.Count; p++)
                    {
                        hlfValue1 = string.Empty;
                        hlfValue2 = string.Empty;

                        hlfcolor1 = string.Empty;
                        hlfcolor2 = string.Empty;

                        sbCSA.Append("<tr>");


                        if (!isCompany)
                        {

                            CompaniesService compSer = new CompaniesService();
                            Companies comp = compSer.GetByCompanyId(Convert.ToInt32(dtDSA2.Rows[p]["CompanyId"]));


                            sbCSA.Append("<td width='15%' border='1' align='left' valign='top'>");
                            if (comp != null)
                            {
                                sbCSA.Append(comp.CompanyName);
                            }
                            sbCSA.Append("</td>");
                        }


                        sbCSA.Append("<td width='35%' border='1' align='left' valign='top'>");
                       
                            sbCSA.Append(dtDSA2.Rows[p]["CategoryDesc"].ToString());
                        
                        sbCSA.Append("</td>");

                        if (isSite)
                        {
                            sbCSA.Append("<td width='15%' valign='top' border='1' align='left'>");
                            if (dtDSA2.Rows[p]["SiteId"] != null)
                            {
                                SitesService Siser = new SitesService();
                                Sites si = Siser.GetBySiteId(Convert.ToInt32(dtDSA2.Rows[p]["SiteId"]));
                                sbCSA.Append(Convert.ToString(si.SiteName));
                            }
                            sbCSA.Append("</td>");
                        }



                        if (month >= 0 || month >= 4)
                        {
                            if (Convert.ToInt32(dtDSA2.Rows[p]["Qtr 1"]) > 0)
                            {
                                hlfValue1 = dtDSA2.Rows[p]["Qtr 1"].ToString();
                            }
                            else if (Convert.ToInt32(dtDSA2.Rows[p]["Qtr 2"]) > 0)
                            {
                                hlfValue1 = dtDSA2.Rows[p]["Qtr 2"].ToString();
                            }
                            if (hlfValue1 != string.Empty)
                            {
                                if (Convert.ToInt32(hlfValue1) != 0)
                                {
                                    hlfcolor1 = "Green";
                                }
                                else
                                {
                                    hlfcolor1 = "Red";
                                }
                            }
                            else
                            {
                                hlfcolor1 = "Red";
                            }
                            if (month >= 7 || month >= 10)
                            {
                                
                                    if (Convert.ToInt32(dtDSA2.Rows[p]["Qtr 3"]) > 0)
                                    {
                                        hlfValue1 = dtDSA2.Rows[p]["Qtr 3"].ToString();
                                    }
                                    else if (Convert.ToInt32(dtDSA2.Rows[p]["Qtr 4"]) > 0)
                                    {
                                        hlfValue1 = dtDSA2.Rows[p]["Qtr 4"].ToString();
                                    }
                            }
                                
                            if (hlfValue1 != string.Empty)
                            {
                                if (Convert.ToInt32(hlfValue1) != 0)
                                {
                                    hlfcolor2 = "Green";
                                }
                                else
                                {
                                    hlfcolor2 = "Red";
                                }
                            }
                            else
                            {
                                hlfcolor2 = "Red";
                            }                         


                        }

                        sbCSA.Append("<td border='1' align='center' width='10%' valign='top'>");
                        if (hlfcolor1 == "Green")
                        {
                            //sbCSA.Append(imgGreenTick);
                           // sbCSA.Append(hlfValue1);

                            sbCSA.Append("<table border='0' width='100%'><tr><td align='right' width='50%'>" + imgGreenTick + "</td>");
                            sbCSA.Append("<td align='left' width='50%'>" + "(" + hlfValue1 + ")" + "</td><tr></table>");

                        }
                        else if (hlfcolor1 == "Red")
                        {
                            sbCSA.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                        }
                        sbCSA.Append("</td>");


                        sbCSA.Append("<td border='1' align='center' width='10%' valign='top'>");
                        if (hlfcolor2 == "Green")
                        {
                            //sbCSA.Append(imgGreenTick);
                            //sbCSA.Append(hlfValue2);

                            sbCSA.Append("<table border='0' width='100%'><tr><td align='right' width='50%'>" + imgGreenTick + "</td>");
                            sbCSA.Append("<td align='left' width='50%'>" + "(" + hlfValue2 + ")" + "</td><tr></table>");

                        }
                        else if (hlfcolor2 == "Red")
                        {
                            sbCSA.Append("<table border='0' width='100%'><tr><td>" + imgRedCross + "</td></tr></table>");
                        }
                        sbCSA.Append("</td>");


                        if (dtDSA2.Rows[p]["Alcoa Audit"] != null)
                        {
                            if (Convert.ToInt32(dtDSA2.Rows[p]["Alcoa Audit"]) != 0)
                            {
                                AlcoaAuditColor2 = "Green";
                                AlcoaAudit2 = dtDSA2.Rows[p]["Alcoa Audit"].ToString();
                                AlcoaAuditImg = imgGreenTick;
                            }
                            else
                            {
                                AlcoaAuditColor2 = "Red";
                                AlcoaAuditImg = imgRedCross;
                            }
                        }


                        sbCSA.Append("<td border='1' width='20%' valign='top' style='color:" + AlcoaAuditColor2 + "' align='center'>");
                        //sbCSA.Append(AlcoaAuditImg);
                        //sbCSA.Append(AlcoaAudit2);

                        sbCSA.Append("</td>");


                        sbCSA.Append("</tr>");

                    }
                    #endregion

                    
                }

                else
                {
                    sbCSA.Append("<tr>");
                    sbCSA.Append("<td colspan='5' valign='top' align='center' width='100%'>");
                    sbCSA.Append("No Contractor Services Audit(s) submitted");
                    sbCSA.Append("</td>");
                    sbCSA.Append("</tr>");
                }

                sbCSA.Append("</table>");
                sbCSA.Append("<br/>");
            }
            string strSBCSA = sbCSA.ToString();

        } 
        #endregion
            

        #region Contractor Contracts
		StringBuilder sbCC = new StringBuilder();

        if (Session["ComplianceReportCC_imgURL"] != null)
        {
            //sbCC.Append("<h3>Contractor Contacts</h3><br/>");

            sbCC.Append(headerFormat.Replace("[Header]", "Contractor Contacts"));


            string imgAlt = Session["ComplianceReportCC_imgAlt"].ToString();
            string imgUrlPath = Session["ComplianceReportCC_imgURL"].ToString();
            if (imgUrlPath.Contains("Yellow"))
            {
                sbCC.Append("<img style='margin-left:10px' height='10px' width='10px' src='" + imgPathTLYellow + "' alt='" + imgAlt + "'/>");
            }
            else if (imgUrlPath.Contains("Green"))
            {
                sbCC.Append("<img style='margin-left:10px' height='10px' width='10px' src='" + imgPathTLGreen + "' alt='" + imgAlt + "'/>");
            }
            else if (imgUrlPath.Contains("Red"))
            {
                sbCC.Append("<img style='margin-left:10px' height='10px' width='10px' src='" + imgPathTLRed + "' alt='" + imgAlt + "'/>");
            }

            sbCC.Append("&nbsp;");
            sbCC.Append("&nbsp;");
            sbCC.Append(Session["ComplianceReportCC_lblText"].ToString());
            sbCC.Append("<br/>");
            sbCC.Append("<br/>");
        } 
	#endregion
        
            
         #region Documents

        StringBuilder sbDocuments = new StringBuilder();

        if (Session["DocumentsList"] != null)
        {
            TList<Documents> DocumentsList = (TList<Documents>)Session["DocumentsList"];

            DataSet ds = DocumentsList.ToDataSet(true);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "DocumentName ASC, RegionId ASC";

            DataTable dtDocuments = dv.ToTable();
           

            if (DocumentsList != null)
            {


                //sbDocuments.Append("<h3>Must Read Library Documents</h3><br/>");

                sbDocuments.Append(headerFormat.Replace("[Header]","Must Read Library Documents"));

                sbDocuments.Append("<table width='100%' align='Left' valign='top' border='1' style='font-size:7px; color:Black;'>");
                sbDocuments.Append("<tr>");

                sbDocuments.Append("<td border='1' align='Center' valign='top' width='5%' bgcolor='" + headerBgColor + "' style='font-weight:bold; border:1px solid gray;'></td>");
                sbDocuments.Append("<td border='1' align='Center' width='15%' bgcolor='" + headerBgColor + "' valign='top' style='font-weight:bold; border:1px solid gray;'>File Name</td>");


                sbDocuments.Append("<td border='1' align='Center' width='50%' bgcolor='" + headerBgColor + "' valign='top' style='font-weight:bold; border:1px solid gray;'>Document Name</td>");


                sbDocuments.Append("<td border='1' align='Center' width='10%' bgcolor='" + headerBgColor + "' valign='top' style='font-weight:bold; border:1px solid gray;'>Read</td>");
                sbDocuments.Append("<td border='1' align='Center' width='20%' bgcolor='" + headerBgColor + "' valign='top' style='font-weight:bold; border:1px solid gray;'>Relevant To</td>");


                sbDocuments.Append("</tr>");

                if (DocumentsList.Count > 0)
                {
                    for( int i=0; i<dtDocuments.Rows.Count; i++)
                    {
                        sbDocuments.Append("<tr>");

                        sbDocuments.Append("<td border='1' width='5%' align='Center'>");
                        sbDocuments.Append("<img style='margin-left:10px' height='10px' width='10px' src='" + imgPathArticleIcon + "' alt='article Icon'/>");
                        sbDocuments.Append("</td>");


                        sbDocuments.Append("<td border='1' width='15%' align='left'>");
                        //sbDocuments.Append(documents.DocumentFileName);
                        sbDocuments.Append(dtDocuments.Rows[i]["DocumentFileName"].ToString());
                        sbDocuments.Append("</td>");

                        sbDocuments.Append("<td border='1' width='50%' align='left'>");
                        sbDocuments.Append(dtDocuments.Rows[i]["DocumentName"].ToString());
                        sbDocuments.Append("</td>");

                        Hashtable htReadFiles = (Hashtable)Session["HashTableReadDownLoaded"];

                        if (htReadFiles.ContainsKey(dtDocuments.Rows[i]["DocumentFileName"]))
                        {
                            sbDocuments.Append("<td border='1' width='10%' style='color:Green' align='Center'>");
                            sbDocuments.Append("Read");
                            sbDocuments.Append("</td>");
                        }
                        else
                        {
                            sbDocuments.Append("<td border='1' width='10%' style='color:Red' align='Center'>");
                            sbDocuments.Append("Unread");
                            sbDocuments.Append("</td>");
                        }

                        sbDocuments.Append("<td border='1' width='20%' align='left'>");

                        RegionsService regSer = new RegionsService();
                        Regions reg = regSer.GetByRegionId(Convert.ToInt32(dtDocuments.Rows[i]["RegionId"]));

                        sbDocuments.Append(reg.RegionName.ToString());
                        sbDocuments.Append("</td>");

                        sbDocuments.Append("</tr>");
                    }
                }

                else
                {
                    sbDocuments.Append("<tr>");
                    sbDocuments.Append("<td colspan='4' align='Center' border='1' style='color:Red'>");
                    sbDocuments.Append("No Documents for this region");
                    sbDocuments.Append("</td>");
                    sbDocuments.Append("</tr>");
                }
                sbDocuments.Append("</table>");
            }
        } 
        #endregion




        #region Lebels
        if (!String.IsNullOrEmpty(SessionHandler.spVar_CompanyId))
        {

            CompaniesService compSer = new CompaniesService();
            Companies comp = compSer.GetByCompanyId(Convert.ToInt32(SessionHandler.spVar_CompanyId));
            if (comp != null)
            {
                companyName = comp.CompanyName;
            }
        }

        if (!String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
        {

            SitesService siteSer = new SitesService();
            Sites site = siteSer.GetBySiteId(Convert.ToInt32(SessionHandler.spVar_SiteId));
            if (site != null)
            {
                siteName = site.SiteName;
            }
            else 
            {
                RegionsService rSer=new RegionsService();
                Regions r = rSer.GetByRegionId(-1 * Convert.ToInt32(SessionHandler.spVar_SiteId));
                if (r != null)
                {
                    siteName = r.RegionName;
                }
            }
        }

        if (!String.IsNullOrEmpty(SessionHandler.spVar_CategoryId))
        {
            if (Convert.ToInt32(SessionHandler.spVar_CategoryId) != -1)
            {
                CompanySiteCategoryService compSiteSer = new CompanySiteCategoryService();
                CompanySiteCategory compSite = compSiteSer.GetByCompanySiteCategoryId(Convert.ToInt32(SessionHandler.spVar_CategoryId));
             
                if (compSite != null)
                {
                    CategoryName = compSite.CategoryDesc;
                }
            }
            else
            {
                CategoryName = "All";
            }

        }

        if (!String.IsNullOrEmpty(SessionHandler.spVar_Year))
        {
            strYear = Convert.ToInt32(SessionHandler.spVar_Year).ToString();
        } 
        #endregion


            contents = contents.Replace("[CurrentDate]", DateTime.Now.Date.ToShortDateString());

            contents = contents.Replace("[CompanyName]", companyName);

            contents = contents.Replace("[SiteName]",siteName);

            contents = contents.Replace("[SiteCategoryName]", CategoryName);

            contents = contents.Replace("[YearName]", strYear);



            contents = contents.Replace("[Table]", sbGrids.ToString());

            contents = contents.Replace("[TableSP]", sbSP.ToString());

            contents = contents.Replace("[TableMT]", sbGridsMt.ToString());

            contents = contents.Replace("[TableSFR]", sbSFR.ToString());

            contents = contents.Replace("[TableMS]", sbMS.ToString());

            contents = contents.Replace("[TableKPI]", sbKPI.ToString());

            contents = contents.Replace("[TableTS]", sbTS.ToString());

            contents = contents.Replace("[TableCSA]", sbCSA.ToString());

            contents = contents.Replace("[ContractorContacts]", sbCC.ToString());

            contents = contents.Replace("[Documents]", sbDocuments.ToString());


            Document document = new Document(PageSize.A4, 50, 50, 25, 25);
            //PdfWriter.GetInstance(document, new FileStream(@"C:\Bishwajit\parsetest.pdf", FileMode.Create));

            MemoryStream output = new MemoryStream();
            Object writer = PdfWriter.GetInstance(document, output);


            document.Open();

            List<IElement> parsedHtmlElements = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
            foreach (object htmlElement in parsedHtmlElements)
            {
                document.Add((IElement)htmlElement);

            }
            document.Close();

            Response.ClearContent();
            Response.Clear();

            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", "Summary Compliance Report By Company"));
            Response.BinaryWrite(output.ToArray());

            
        }
          }
       }
    
        catch (Exception ex)
        {

            Response.Write(ex.Message);
        }
    }

    protected string ReturnKPIValues(bool embedded, int change, string imgPathRedCross, string imgPathGreenTick, string imgPathTLYellow)
    {
       

        
        string imgRedCross = "<img height='10px' width='10px'  src='" + imgPathRedCross + "' alt='Cross'/>";
        string imgGreenTick = "<img height='10px' width='10px'  src='" + imgPathGreenTick + "' alt='Cross'/>";
        string imgYellow = "<img height='10px' width='10px' src='" + imgPathTLYellow + "' alt='Cross'/>";

        StringBuilder sb = new StringBuilder();
        string color = "black";
        string star = string.Empty;
        bool imgVisible = false;
        string imgPath = string.Empty;
        if (change != 0)
        {
            if (change != -1)
            {                
                imgVisible = true;
                color = "Green";

                if (change == 2)
                {
                    color="Black";
                    star = "*";
                }
                if (change == 3)
                    imgPath = imgYellow;
                else
                    imgPath = imgGreenTick;
            }
            else
            {
                imgVisible = false;
            }
        }
        else
        {
            imgPath = imgRedCross;
            color="Red";
            if (!embedded)
                imgVisible = false;
            else
                imgVisible = true;
        }
        string strLine = "";
        if (imgVisible)
        {
            strLine = imgPath + "&nbsp;"+star;
        }
        else
        {
            strLine =  "&nbsp;" + star; 
        }
        return strLine;
    }

    
    

}

