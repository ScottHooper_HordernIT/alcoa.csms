using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxCallbackPanel;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class UserControls_Tiny_Reports_DropDownListPicker2a : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    // Delegate declaration
    public delegate void OnButtonClick(string strValue);

    // Event declaration
    public event OnButtonClick btnHandler;

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            //ddlSites.DataSourceID = "dsSitesFilter";
            //ddlSites.TextField = "SiteName";
            //ddlSites.ValueField = "SiteId";
            //ddlSites.DataBind();

            ddlCompanies.DataSourceID = "sqldsActiveCompaniesList";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();

            ddlCategory.DataSourceID = "CompanySiteCategoryDataSource";
            ddlCategory.TextField = "CategoryDesc";
            ddlCategory.ValueField = "CompanySiteCategoryId";
            ddlCategory.DataBind();
            ddlCategory.Items.Add("-----------------------------", 0);
            ddlCategory.Items.Add("All", -1);
            ddlCategory.Value = -1;

            ddlYear.DataSourceID = "sqldsKpi_GetAllYearsSubmitted";
            ddlYear.TextField = "Year";
            ddlYear.ValueField = "Year";
            ddlYear.DataBind();

            DataTable dt = ((DataView)sqldsKpi_GetAllYearsSubmitted.Select(DataSourceSelectArguments.Empty)).ToTable();
            int r = dt.Rows.Count - 1;
            int latestYear = 0;
            Int32.TryParse(dt.Rows[r].ItemArray[0].ToString(), out latestYear);

            if (latestYear != 0)
            {
                ddlYear.Value = latestYear;
            }
            else
            {
                ddlYear.Value = DateTime.Now.Year;
            }

            LoadPage();
            ddlCompanies.Enabled = false;

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    ddlCompanies.Enabled = true;
                    //FillSitesCombo("-1");
                    break;
                case ((int)RoleList.Contractor):
                    ddlCompanies.Value = auth.CompanyId;
                    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    FillSitesCombo(auth.CompanyId.ToString(), "-1");
                    break;
                case ((int)RoleList.Administrator):
                    ddlCompanies.Enabled = true;
                    //FillSitesCombo("-1");
                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    protected void LoadPage()
    {
        if (String.IsNullOrEmpty(SessionHandler.spVar_MonthId)) SessionHandler.spVar_MonthId = "0";
        if (String.IsNullOrEmpty(SessionHandler.spVar_CategoryId)) SessionHandler.spVar_CategoryId = "-1";

        if (String.IsNullOrEmpty(SessionHandler.spVar_CompanyId))
        {
            ddlCompanies.Text = "< Select Company >";
        }
        else
        {
            if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0)
            {
                ddlCompanies.Value = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                ddlCategory.Value = Convert.ToInt32(SessionHandler.spVar_CategoryId);
                FillSitesCombo(ddlCompanies.Value.ToString(), ddlCategory.Value.ToString());
            }
            else
            {
                ddlCompanies.Text = "< Select Company >";
            }
        }
        if (!String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
        {
            ddlSites.Value = Convert.ToInt32(SessionHandler.spVar_SiteId);
        }

        if (!String.IsNullOrEmpty(SessionHandler.spVar_CategoryId))
        {
            ddlCategory.Value = Convert.ToInt32(SessionHandler.spVar_CategoryId);
        }

        if (!String.IsNullOrEmpty(SessionHandler.spVar_Year))
        {
            ddlYear.Value = Convert.ToInt32(SessionHandler.spVar_Year);
        }
    }

    protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString(), ddlCategory.SelectedItem.Value.ToString());
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompanies.SelectedItem != null && ddlCategory.SelectedItem != null)
        {
            FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString(), ddlCategory.SelectedItem.Value.ToString());
        }
    }

    protected void cmbCompanies_Callback(object source, CallbackEventArgsBase e)
    {
        //ddlCompanies.SelectedIndex = 0;
        //FillCompaniesCombo(e.Parameter);
    }


    protected void FillSitesCombo(string companyId, string CategoryId)
    {
        int _i = 0;
        if (string.IsNullOrEmpty(companyId)) return;
        if (string.IsNullOrEmpty(CategoryId)) return;

        int _companyId;
        bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
        if (!companyId_isInt) return;

        if (_companyId > 0) //Specific Company
        {
            SessionHandler.spVar_CompanyId = companyId;

            SitesService sService = new SitesService();
            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_companyId);

            ddlSites.Items.Clear();

            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (cscs.CompanySiteCategoryId != null)
                            {
                                if (s.SiteId == cscs.SiteId)
                                {
                                    if (Convert.ToInt32(CategoryId) > 0)
                                    {
                                        if (cscs.CompanySiteCategoryId.ToString() == CategoryId)
                                        {
                                            ddlSites.Items.Add(s.SiteName, cscs.SiteId);
                                        }
                                    }
                                    else
                                    {
                                        ddlSites.Items.Add(s.SiteName, cscs.SiteId);
                                    }
                                }
                            }
                        }
                    }

                    ddlSites.Items.Add("----------------", 0);

                    //Add Regions
                    RegionsService rService = new RegionsService();
                    DataSet dsRegion = rService.GetByCompanyId(_companyId);
                    if (dsRegion.Tables[0] != null)
                    {
                        if (dsRegion.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsRegion.Tables[0].Rows)
                            {
                                if (dr[2].ToString() != "AU") ddlSites.Items.Add(dr[0].ToString(), (-1 * Convert.ToInt32(dr[1].ToString())));
                            }
                        }
                    }
                }
            }
        }
    }

    protected void btnSearchGo_Click(object sender, EventArgs e)
    {
        if (btnHandler != null)
            btnHandler(string.Empty);
    }

}
