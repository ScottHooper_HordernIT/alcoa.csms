﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailTemplateAttachmentProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskEmailTemplateAttachmentProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachmentKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachmentKey key)
		{
			return Delete(transactionManager, key.AdminTaskEmailTemplateAttachmentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateAttachmentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskEmailTemplateAttachmentId)
		{
			return Delete(null, _adminTaskEmailTemplateAttachmentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateAttachmentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateAttachmentId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(_adminTaskEmailTemplateId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailTemplateAttachment> GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(transactionManager, _adminTaskEmailTemplateId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(transactionManager, _adminTaskEmailTemplateId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate key.
		///		fkAdminTaskEmailTemplateAttachmentAdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskEmailTemplateId(null, _adminTaskEmailTemplateId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate key.
		///		fkAdminTaskEmailTemplateAttachmentAdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskEmailTemplateId(null, _adminTaskEmailTemplateId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateAttachment_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public abstract TList<AdminTaskEmailTemplateAttachment> GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_CsmsFile key.
		///		FK_AdminTaskEmailTemplateAttachment_CsmsFile Description: 
		/// </summary>
		/// <param name="_csmsFileId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByCsmsFileId(System.Int32 _csmsFileId)
		{
			int count = -1;
			return GetByCsmsFileId(_csmsFileId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_CsmsFile key.
		///		FK_AdminTaskEmailTemplateAttachment_CsmsFile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailTemplateAttachment> GetByCsmsFileId(TransactionManager transactionManager, System.Int32 _csmsFileId)
		{
			int count = -1;
			return GetByCsmsFileId(transactionManager, _csmsFileId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_CsmsFile key.
		///		FK_AdminTaskEmailTemplateAttachment_CsmsFile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByCsmsFileId(TransactionManager transactionManager, System.Int32 _csmsFileId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsFileId(transactionManager, _csmsFileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_CsmsFile key.
		///		fkAdminTaskEmailTemplateAttachmentCsmsFile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsFileId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByCsmsFileId(System.Int32 _csmsFileId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsFileId(null, _csmsFileId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_CsmsFile key.
		///		fkAdminTaskEmailTemplateAttachmentCsmsFile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsFileId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public TList<AdminTaskEmailTemplateAttachment> GetByCsmsFileId(System.Int32 _csmsFileId, int start, int pageLength,out int count)
		{
			return GetByCsmsFileId(null, _csmsFileId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateAttachment_CsmsFile key.
		///		FK_AdminTaskEmailTemplateAttachment_CsmsFile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment objects.</returns>
		public abstract TList<AdminTaskEmailTemplateAttachment> GetByCsmsFileId(TransactionManager transactionManager, System.Int32 _csmsFileId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachmentKey key, int start, int pageLength)
		{
			return GetByAdminTaskEmailTemplateAttachmentId(transactionManager, key.AdminTaskEmailTemplateAttachmentId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateAttachmentId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateAttachmentId(System.Int32 _adminTaskEmailTemplateAttachmentId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateAttachmentId(null,_adminTaskEmailTemplateAttachmentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateAttachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateAttachmentId(System.Int32 _adminTaskEmailTemplateAttachmentId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateAttachmentId(null, _adminTaskEmailTemplateAttachmentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateAttachmentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateAttachmentId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateAttachmentId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateAttachmentId(transactionManager, _adminTaskEmailTemplateAttachmentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateAttachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateAttachmentId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateAttachmentId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateAttachmentId(transactionManager, _adminTaskEmailTemplateAttachmentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateAttachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateAttachmentId(System.Int32 _adminTaskEmailTemplateAttachmentId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailTemplateAttachmentId(null, _adminTaskEmailTemplateAttachmentId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateAttachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateAttachmentId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateAttachmentId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsFileId"></param>
		/// <param name="_csmsAccessId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsFileId, System.Int32 _csmsAccessId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(null,_adminTaskEmailTemplateId, _csmsFileId, _csmsAccessId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsFileId"></param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsFileId, System.Int32 _csmsAccessId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(null, _adminTaskEmailTemplateId, _csmsFileId, _csmsAccessId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsFileId"></param>
		/// <param name="_csmsAccessId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsFileId, System.Int32 _csmsAccessId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(transactionManager, _adminTaskEmailTemplateId, _csmsFileId, _csmsAccessId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsFileId"></param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsFileId, System.Int32 _csmsAccessId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(transactionManager, _adminTaskEmailTemplateId, _csmsFileId, _csmsAccessId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsFileId"></param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsFileId, System.Int32 _csmsAccessId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(null, _adminTaskEmailTemplateId, _csmsFileId, _csmsAccessId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplateAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsFileId"></param>
		/// <param name="_csmsAccessId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment GetByAdminTaskEmailTemplateIdCsmsFileIdCsmsAccessId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsFileId, System.Int32 _csmsAccessId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskEmailTemplateAttachment&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskEmailTemplateAttachment&gt;"/></returns>
		public static TList<AdminTaskEmailTemplateAttachment> Fill(IDataReader reader, TList<AdminTaskEmailTemplateAttachment> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskEmailTemplateAttachment")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.AdminTaskEmailTemplateAttachmentId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskEmailTemplateAttachment>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskEmailTemplateAttachment",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskEmailTemplateAttachmentId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.AdminTaskEmailTemplateAttachmentId - 1)];
					c.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.AdminTaskEmailTemplateId - 1)];
					c.CsmsFileId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.CsmsFileId - 1)];
					c.CsmsAccessId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.CsmsAccessId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskEmailTemplateAttachmentId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.AdminTaskEmailTemplateAttachmentId - 1)];
			entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.AdminTaskEmailTemplateId - 1)];
			entity.CsmsFileId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.CsmsFileId - 1)];
			entity.CsmsAccessId = (System.Int32)reader[((int)AdminTaskEmailTemplateAttachmentColumn.CsmsAccessId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailTemplateAttachmentId = (System.Int32)dataRow["AdminTaskEmailTemplateAttachmentId"];
			entity.AdminTaskEmailTemplateId = (System.Int32)dataRow["AdminTaskEmailTemplateId"];
			entity.CsmsFileId = (System.Int32)dataRow["CsmsFileId"];
			entity.CsmsAccessId = (System.Int32)dataRow["CsmsAccessId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdminTaskEmailTemplateIdSource	
			if (CanDeepLoad(entity, "AdminTaskEmailTemplate|AdminTaskEmailTemplateIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskEmailTemplateIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskEmailTemplateId;
				AdminTaskEmailTemplate tmpEntity = EntityManager.LocateEntity<AdminTaskEmailTemplate>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskEmailTemplate), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskEmailTemplateIdSource = tmpEntity;
				else
					entity.AdminTaskEmailTemplateIdSource = DataRepository.AdminTaskEmailTemplateProvider.GetByAdminTaskEmailTemplateId(transactionManager, entity.AdminTaskEmailTemplateId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskEmailTemplateIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskEmailTemplateProvider.DeepLoad(transactionManager, entity.AdminTaskEmailTemplateIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskEmailTemplateIdSource

			#region CsmsFileIdSource	
			if (CanDeepLoad(entity, "CsmsFile|CsmsFileIdSource", deepLoadType, innerList) 
				&& entity.CsmsFileIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsFileId;
				CsmsFile tmpEntity = EntityManager.LocateEntity<CsmsFile>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsFile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsFileIdSource = tmpEntity;
				else
					entity.CsmsFileIdSource = DataRepository.CsmsFileProvider.GetByCsmsFileId(transactionManager, entity.CsmsFileId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsFileIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsFileIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsFileProvider.DeepLoad(transactionManager, entity.CsmsFileIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsFileIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdminTaskEmailTemplateIdSource
			if (CanDeepSave(entity, "AdminTaskEmailTemplate|AdminTaskEmailTemplateIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskEmailTemplateIdSource != null)
			{
				DataRepository.AdminTaskEmailTemplateProvider.Save(transactionManager, entity.AdminTaskEmailTemplateIdSource);
				entity.AdminTaskEmailTemplateId = entity.AdminTaskEmailTemplateIdSource.AdminTaskEmailTemplateId;
			}
			#endregion 
			
			#region CsmsFileIdSource
			if (CanDeepSave(entity, "CsmsFile|CsmsFileIdSource", deepSaveType, innerList) 
				&& entity.CsmsFileIdSource != null)
			{
				DataRepository.CsmsFileProvider.Save(transactionManager, entity.CsmsFileIdSource);
				entity.CsmsFileId = entity.CsmsFileIdSource.CsmsFileId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskEmailTemplateAttachmentChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskEmailTemplateAttachment</c>
	///</summary>
	public enum AdminTaskEmailTemplateAttachmentChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdminTaskEmailTemplate</c> at AdminTaskEmailTemplateIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskEmailTemplate))]
		AdminTaskEmailTemplate,
			
		///<summary>
		/// Composite Property for <c>CsmsFile</c> at CsmsFileIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsFile))]
		CsmsFile,
		}
	
	#endregion AdminTaskEmailTemplateAttachmentChildEntityTypes
	
	#region AdminTaskEmailTemplateAttachmentFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskEmailTemplateAttachmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateAttachmentFilterBuilder : SqlFilterBuilder<AdminTaskEmailTemplateAttachmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentFilterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateAttachmentFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateAttachmentFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateAttachmentFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateAttachmentFilterBuilder
	
	#region AdminTaskEmailTemplateAttachmentParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskEmailTemplateAttachmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateAttachmentParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailTemplateAttachmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentParameterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateAttachmentParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateAttachmentParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateAttachmentParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateAttachmentParameterBuilder
	
	#region AdminTaskEmailTemplateAttachmentSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskEmailTemplateAttachmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailTemplateAttachmentSortBuilder : SqlSortBuilder<AdminTaskEmailTemplateAttachmentColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateAttachmentSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailTemplateAttachmentSortBuilder
	
} // end namespace
