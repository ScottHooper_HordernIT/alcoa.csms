﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesEhsimsMapSitesEhsimsIhsListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompaniesEhsimsMapSitesEhsimsIhsListProviderBaseCore : EntityViewProviderBase<CompaniesEhsimsMapSitesEhsimsIhsList>
	{
		#region Custom Methods
		
		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteAbbrev
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteAbbrev(System.String contCompanyCode, System.String siteAbbrev)
		{
			return GetByContCompanyCodeSiteAbbrev(null, 0, int.MaxValue , contCompanyCode, siteAbbrev);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteAbbrev(int start, int pageLength, System.String contCompanyCode, System.String siteAbbrev)
		{
			return GetByContCompanyCodeSiteAbbrev(null, start, pageLength , contCompanyCode, siteAbbrev);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteAbbrev(TransactionManager transactionManager, System.String contCompanyCode, System.String siteAbbrev)
		{
			return GetByContCompanyCodeSiteAbbrev(transactionManager, 0, int.MaxValue , contCompanyCode, siteAbbrev);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByContCompanyCodeSiteAbbrev(TransactionManager transactionManager, int start, int pageLength, System.String contCompanyCode, System.String siteAbbrev);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteId
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteId' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteId(System.String contCompanyCode, System.Int32? siteId)
		{
			return GetByContCompanyCodeSiteId(null, 0, int.MaxValue , contCompanyCode, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteId' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteId(int start, int pageLength, System.String contCompanyCode, System.Int32? siteId)
		{
			return GetByContCompanyCodeSiteId(null, start, pageLength , contCompanyCode, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteId' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteId(TransactionManager transactionManager, System.String contCompanyCode, System.Int32? siteId)
		{
			return GetByContCompanyCodeSiteId(transactionManager, 0, int.MaxValue , contCompanyCode, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteId' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByContCompanyCodeSiteId(TransactionManager transactionManager, int start, int pageLength, System.String contCompanyCode, System.Int32? siteId);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteId
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteId(System.Int32? siteId)
		{
			return GetBySiteId(null, 0, int.MaxValue , siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteId(int start, int pageLength, System.Int32? siteId)
		{
			return GetBySiteId(null, start, pageLength , siteId);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteId(TransactionManager transactionManager, System.Int32? siteId)
		{
			return GetBySiteId(transactionManager, 0, int.MaxValue , siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetBySiteId(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteId);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteAbbrev
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteAbbrev(System.String siteAbbrev)
		{
			return GetBySiteAbbrev(null, 0, int.MaxValue , siteAbbrev);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteAbbrev(int start, int pageLength, System.String siteAbbrev)
		{
			return GetBySiteAbbrev(null, start, pageLength , siteAbbrev);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteAbbrev(TransactionManager transactionManager, System.String siteAbbrev)
		{
			return GetBySiteAbbrev(transactionManager, 0, int.MaxValue , siteAbbrev);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetBySiteAbbrev' stored procedure. 
		/// </summary>
		/// <param name="siteAbbrev"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetBySiteAbbrev(TransactionManager transactionManager, int start, int pageLength, System.String siteAbbrev);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyId
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(System.Int32? companyId)
		{
			return GetByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByEhsimsLocCode
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="ehsimsLocCode"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByEhsimsLocCode(System.Int32? ehsimsLocCode)
		{
			return GetByEhsimsLocCode(null, 0, int.MaxValue , ehsimsLocCode);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="ehsimsLocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByEhsimsLocCode(int start, int pageLength, System.Int32? ehsimsLocCode)
		{
			return GetByEhsimsLocCode(null, start, pageLength , ehsimsLocCode);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="ehsimsLocCode"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByEhsimsLocCode(TransactionManager transactionManager, System.Int32? ehsimsLocCode)
		{
			return GetByEhsimsLocCode(transactionManager, 0, int.MaxValue , ehsimsLocCode);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="ehsimsLocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByEhsimsLocCode(TransactionManager transactionManager, int start, int pageLength, System.Int32? ehsimsLocCode);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyIdSiteId
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyIdSiteId(System.Int32? companyId, System.Int32? siteId)
		{
			return GetByCompanyIdSiteId(null, 0, int.MaxValue , companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyIdSiteId(int start, int pageLength, System.Int32? companyId, System.Int32? siteId)
		{
			return GetByCompanyIdSiteId(null, start, pageLength , companyId, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32? companyId, System.Int32? siteId)
		{
			return GetByCompanyIdSiteId(transactionManager, 0, int.MaxValue , companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyIdSiteId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? siteId);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteNameIhs
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteNameIhs' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameIhs"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteNameIhs(System.String contCompanyCode, System.String siteNameIhs)
		{
			return GetByContCompanyCodeSiteNameIhs(null, 0, int.MaxValue , contCompanyCode, siteNameIhs);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteNameIhs' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameIhs"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteNameIhs(int start, int pageLength, System.String contCompanyCode, System.String siteNameIhs)
		{
			return GetByContCompanyCodeSiteNameIhs(null, start, pageLength , contCompanyCode, siteNameIhs);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteNameIhs' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameIhs"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeSiteNameIhs(TransactionManager transactionManager, System.String contCompanyCode, System.String siteNameIhs)
		{
			return GetByContCompanyCodeSiteNameIhs(transactionManager, 0, int.MaxValue , contCompanyCode, siteNameIhs);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeSiteNameIhs' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameIhs"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByContCompanyCodeSiteNameIhs(TransactionManager transactionManager, int start, int pageLength, System.String contCompanyCode, System.String siteNameIhs);
		
		#endregion

		
		#region _CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeEhsimsLocCode
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="locCode"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeEhsimsLocCode(System.String contCompanyCode, System.String locCode)
		{
			return GetByContCompanyCodeEhsimsLocCode(null, 0, int.MaxValue , contCompanyCode, locCode);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="locCode"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeEhsimsLocCode(int start, int pageLength, System.String contCompanyCode, System.String locCode)
		{
			return GetByContCompanyCodeEhsimsLocCode(null, start, pageLength , contCompanyCode, locCode);
		}
				
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="locCode"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByContCompanyCodeEhsimsLocCode(TransactionManager transactionManager, System.String contCompanyCode, System.String locCode)
		{
			return GetByContCompanyCodeEhsimsLocCode(transactionManager, 0, int.MaxValue , contCompanyCode, locCode);
		}
		
		/// <summary>
		///	This method wrap the '_CompaniesEhsimsMapSitesEhsimsIhsList_GetByContCompanyCodeEhsimsLocCode' stored procedure. 
		/// </summary>
		/// <param name="contCompanyCode"> A <c>System.String</c> instance.</param>
		/// <param name="locCode"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByContCompanyCodeEhsimsLocCode(TransactionManager transactionManager, int start, int pageLength, System.String contCompanyCode, System.String locCode);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt;"/></returns>
		protected static VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt; Fill(DataSet dataSet, VList<CompaniesEhsimsMapSitesEhsimsIhsList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompaniesEhsimsMapSitesEhsimsIhsList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompaniesEhsimsMapSitesEhsimsIhsList>"/></returns>
		protected static VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt; Fill(DataTable dataTable, VList<CompaniesEhsimsMapSitesEhsimsIhsList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompaniesEhsimsMapSitesEhsimsIhsList c = new CompaniesEhsimsMapSitesEhsimsIhsList();
					c.EhssimsMapId = (Convert.IsDBNull(row["EhssimsMapId"]))?(int)0:(System.Int32)row["EhssimsMapId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.ContCompanyCode = (Convert.IsDBNull(row["ContCompanyCode"]))?string.Empty:(System.String)row["ContCompanyCode"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.SiteNameIhs = (Convert.IsDBNull(row["SiteNameIhs"]))?string.Empty:(System.String)row["SiteNameIhs"];
					c.LocCode = (Convert.IsDBNull(row["LocCode"]))?string.Empty:(System.String)row["LocCode"];
					c.LocCodeSiteNameIhs = (Convert.IsDBNull(row["LocCodeSiteNameIhs"]))?string.Empty:(System.String)row["LocCodeSiteNameIhs"];
					c.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(row["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)row["SiteNameLocCodeSiteNameIhs"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.SiteAbbrev = (Convert.IsDBNull(row["SiteAbbrev"]))?string.Empty:(System.String)row["SiteAbbrev"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompaniesEhsimsMapSitesEhsimsIhsList&gt;"/></returns>
		protected VList<CompaniesEhsimsMapSitesEhsimsIhsList> Fill(IDataReader reader, VList<CompaniesEhsimsMapSitesEhsimsIhsList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompaniesEhsimsMapSitesEhsimsIhsList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompaniesEhsimsMapSitesEhsimsIhsList>("CompaniesEhsimsMapSitesEhsimsIhsList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompaniesEhsimsMapSitesEhsimsIhsList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhssimsMapId = (System.Int32)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.EhssimsMapId)];
					//entity.EhssimsMapId = (Convert.IsDBNull(reader["EhssimsMapId"]))?(int)0:(System.Int32)reader["EhssimsMapId"];
					entity.CompanyId = (System.Int32)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.ContCompanyCode = (System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.ContCompanyCode)];
					//entity.ContCompanyCode = (Convert.IsDBNull(reader["ContCompanyCode"]))?string.Empty:(System.String)reader["ContCompanyCode"];
					entity.SiteId = (System.Int32)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.SiteNameIhs = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameIhs)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameIhs)];
					//entity.SiteNameIhs = (Convert.IsDBNull(reader["SiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameIhs"];
					entity.LocCode = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCode)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCode)];
					//entity.LocCode = (Convert.IsDBNull(reader["LocCode"]))?string.Empty:(System.String)reader["LocCode"];
					entity.LocCodeSiteNameIhs = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCodeSiteNameIhs)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCodeSiteNameIhs)];
					//entity.LocCodeSiteNameIhs = (Convert.IsDBNull(reader["LocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["LocCodeSiteNameIhs"];
					entity.SiteNameLocCodeSiteNameIhs = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)];
					//entity.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(reader["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameLocCodeSiteNameIhs"];
					entity.SiteName = (System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.SiteAbbrev = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteAbbrev)];
					//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
					entity.CompanyName = (System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompaniesEhsimsMapSitesEhsimsIhsList entity)
		{
			reader.Read();
			entity.EhssimsMapId = (System.Int32)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.EhssimsMapId)];
			//entity.EhssimsMapId = (Convert.IsDBNull(reader["EhssimsMapId"]))?(int)0:(System.Int32)reader["EhssimsMapId"];
			entity.CompanyId = (System.Int32)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.ContCompanyCode = (System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.ContCompanyCode)];
			//entity.ContCompanyCode = (Convert.IsDBNull(reader["ContCompanyCode"]))?string.Empty:(System.String)reader["ContCompanyCode"];
			entity.SiteId = (System.Int32)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.SiteNameIhs = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameIhs)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameIhs)];
			//entity.SiteNameIhs = (Convert.IsDBNull(reader["SiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameIhs"];
			entity.LocCode = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCode)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCode)];
			//entity.LocCode = (Convert.IsDBNull(reader["LocCode"]))?string.Empty:(System.String)reader["LocCode"];
			entity.LocCodeSiteNameIhs = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCodeSiteNameIhs)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.LocCodeSiteNameIhs)];
			//entity.LocCodeSiteNameIhs = (Convert.IsDBNull(reader["LocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["LocCodeSiteNameIhs"];
			entity.SiteNameLocCodeSiteNameIhs = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)];
			//entity.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(reader["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameLocCodeSiteNameIhs"];
			entity.SiteName = (System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.SiteAbbrev = (reader.IsDBNull(((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.SiteAbbrev)];
			//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
			entity.CompanyName = (System.String)reader[((int)CompaniesEhsimsMapSitesEhsimsIhsListColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompaniesEhsimsMapSitesEhsimsIhsList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhssimsMapId = (Convert.IsDBNull(dataRow["EhssimsMapId"]))?(int)0:(System.Int32)dataRow["EhssimsMapId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.ContCompanyCode = (Convert.IsDBNull(dataRow["ContCompanyCode"]))?string.Empty:(System.String)dataRow["ContCompanyCode"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.SiteNameIhs = (Convert.IsDBNull(dataRow["SiteNameIhs"]))?string.Empty:(System.String)dataRow["SiteNameIhs"];
			entity.LocCode = (Convert.IsDBNull(dataRow["LocCode"]))?string.Empty:(System.String)dataRow["LocCode"];
			entity.LocCodeSiteNameIhs = (Convert.IsDBNull(dataRow["LocCodeSiteNameIhs"]))?string.Empty:(System.String)dataRow["LocCodeSiteNameIhs"];
			entity.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(dataRow["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)dataRow["SiteNameLocCodeSiteNameIhs"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.SiteAbbrev = (Convert.IsDBNull(dataRow["SiteAbbrev"]))?string.Empty:(System.String)dataRow["SiteAbbrev"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder : SqlFilterBuilder<CompaniesEhsimsMapSitesEhsimsIhsListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder class.
		/// </summary>
		public CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder

	#region CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesEhsimsMapSitesEhsimsIhsListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder class.
		/// </summary>
		public CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder
	
	#region CompaniesEhsimsMapSitesEhsimsIhsListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesEhsimsMapSitesEhsimsIhsListSortBuilder : SqlSortBuilder<CompaniesEhsimsMapSitesEhsimsIhsListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListSqlSortBuilder class.
		/// </summary>
		public CompaniesEhsimsMapSitesEhsimsIhsListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesEhsimsMapSitesEhsimsIhsListSortBuilder

} // end namespace
