﻿<%@ Page Title="Safety Qualification - Definitions & Instructions" Language="C#"
    MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true"
    Inherits="PopUps_SafetyPQ_Definitions" Codebehind="SafetyPQ_Definitions.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" language="javascript">  
         function doClose(e) // note: takes the event as an arg (IE doesn't)  
         {  
             if (!e) e = window.event; // fix IE  

             if (e.keyCode) // IE  
             {  
                 if (e.keyCode == "27") window.close();  
             }  
             else if (e.charCode) // Netscape/Firefox/Opera  
             {  
                 if (e.keyCode == "27") window.close();  
             } 
         }
         document.onkeydown = doClose;  
    </script>

    <table border="0" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td class="pageName" colspan="3" style="height: 17px">
                <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                    <span class="date">Questionnaire > Definitions and Instructions</span><br />
                    <img src="../images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
            </td>
        </tr>
    </table>
    <br />
    <div align="center">
        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" Text="Close (ESC)"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        </dx:ASPxButton>
    </div>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 30px; padding-left: 30px"
        width="100%">
        <tr>
            <td style="width: 100%; height: 13px">
                <span style="font-size: 10pt"><strong>1.0&nbsp; Definitions:<br />
                </strong>
                    <br />
                    1.1 Alcoa refers to Alcoa World Alumina – Australia.<?xml namespace="" ns="urn:schemas-microsoft-com:office:office"
                        prefix="o" ?><o:p></o:p></span><p class="MsoPlainText" style="margin: 0cm 0cm 0pt;
                            text-align: justify">
                            <span style="font-size: 10pt; font-family: Verdana">
                                <o:p>&nbsp;</o:p>
                                &nbsp; </span>
                        </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.2 Contracted Services
                        describes a company or the people employed by a company that performs support activities,
                        such as security, janitorial, cafeteria, uniform delivery, pest control, vending,
                        engineering, design, training, consulting, or other professional or non-professional
                        services and are not directly controlled or supervised by Alcoa.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.3 Contractor describes
                        a company or the people employed by a company that perform work governed by a contractual
                        arrangement between Alcoa and the company and who are not directly controlled or
                        supervised by Alcoa, but does not include subcontractors or contracted services.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.4 Contractor’s Safety
                        Plan or Job Specific Safety Plan details the abatement or control procedures or
                        steps for the hazards identified in the safety hazard assessment for a specific
                        job, task or service.&nbsp; Throughout the performance of the scope of work, the
                        safety plan must be reviewed and updated due to changes in the project scope or
                        conditions.&nbsp; A contractor’s safety plan can be prepared by a contractor, subcontractor
                        or contracted service.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.5 EPCM is an abbreviation
                        for Engineering, Procurement and Construction Management firms that provide services
                        for engineering projects.&nbsp; An EPCM is a Contracted Service.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.6 First Aid is the
                        immediate and temporary aid provided to a sick or injured person until medical treatment
                        can be provided.&nbsp; It generally consists of a series of simple medical techniques
                        that a person can be trained to perform with minimal equipment.&nbsp; The definition
                        of first aid and the treatments which qualify as first aid may be slightly different
                        depending on local regulations and consensus standards.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.7 Injury Free Event
                        is an incident that does not result in an injury or illness, but under slightly
                        different circumstances could have resulted in an injury or illness.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.8 Insurance Carrier
                        is the company that issues and consequently assumes the risk of an insurance policy.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.9 Lead Person is
                        considered the person responsible for the crew and the completion of specific tasks.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.10 Lost Workday is
                        one or more calendar days following the day an injury or illness occurs, that due
                        to the work related injury or illness, a person cannot work, whether scheduled to
                        work or not.&nbsp; The definition of lost workday may be slightly different depending
                        on local regulations and consensus standards.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.11 Medical Treatment
                        is any treatment above or beyond first aid, but does not include diagnostic procedures.&nbsp;
                        <u>The definition of medical treatment may be slightly different depending on local
                            regulations and consensus standards.<o:p></o:p></u></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.12 Other Specialty
                        Site Contractor is engaged in specialized trades, except foundation, structure and
                        building exterior contractor, building equipment contractors, building finishing
                        contractors, and site preparation contractors.&nbsp; The specialty trade work performed
                        includes new work, additions, alterations, maintenance and repairs.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.13 Owner is the individual
                        or individuals who own a company.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.14 Pre Job Briefing
                        is a short discussion with those assigned to a task to review critical steps, determine
                        where errors are likely to occur, protect from adverse consequences, share common
                        safety experiences related to the task and establish stop work criteria in the event
                        the task can not be completed as planned.&nbsp; Pre job briefings are normally held
                        when beginning a different phase of work where new or different hazards may be encountered
                        and may occur once or several times during a shift, depending on work activities.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.15 Prequalification
                        is a process to provide approval, if required, for a contractor, subcontractor or
                        contracted service to potentially perform work.&nbsp; The information necessary
                        for approval is provided by the contractor, subcontractor or contracted service
                        and the likelihood a contractor, subcontractor or contracted service will safely
                        perform work is assessed.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.16 Professional/Scientific/Technical/Environmental
                        Services specialize in performing professional, scientific and technical activities
                        for others.&nbsp; These activities require a high degree of expertise and training
                        and provide legal advise and representation; accounting, bookkeeping, and payroll
                        services; architectural, engineering and specialized design services; computer services;
                        consulting services; research services; advertising services; photographic services;
                        translation and interpretation services; environmental services; and other professional,
                        scientific, and technical services.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.17 Restricted Work
                        is when management prevents, or a physician or other licensed healthcare professional
                        recommends, a person not perform one or more routine functions of a normally assigned
                        job, or not work the full shift, due to a work related injury or illness.&nbsp;
                        The definition of restricted work may be slightly different depending on local regulations
                        and consensus standards.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.18 Risk Assessment
                        is a systematic process to describe and quantify the risks associated with the work
                        environment, the work method and the actions, inactions, knowledge, skills and other
                        characteristics of the workforce that can contribute to an incident.&nbsp; Many
                        risk assessment methods exist to help assess risk, from simple checklists listing
                        major hazards to the use of advanced risk assessment software.&nbsp; The selection
                        of a suitable risk assessment method depends on the scope of the activity, complexity
                        and potential consequences associated with an undesired event.&nbsp; Risk assessment
                        helps develop layers of protection to reduce the probability and severity of adverse
                        consequences.&nbsp; Changing site conditions might cause changes to risk, and as
                        the risk changes, the risk assessment needs to be reviewed and updated with corresponding
                        revisions to the contractor’s safety plan.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.19 Safety Hazard
                        Assessment shall mean the identification of all existing or potential hazards associated
                        with the location or the work, which need to be abated or controlled through the
                        job specific safety plan to ensure the safe completion of the work.&nbsp; Due to
                        changing site conditions, the safety hazard assessment may need to be reviewed and
                        updated, and changes may need to be made to the job specific safety plan.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.20 Selection is the
                        process of choosing an on-site contractor, subcontractor or contracted service to
                        perform work on a specific scope of work.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.21 Site Preparation/Demolition
                        Contractor is primarily engaged in site preparation activities, such as excavating
                        and grading, demolition of buildings and other structures, septic system installation,
                        and house moving.&nbsp; Earthmoving and land clearing for all types of sites (e.g.,
                        building, non-building, mining) is included in this industry.&nbsp; Establishments
                        primarily engaged in construction equipment rental with operator (except cranes)
                        are also included.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.22 Subcontractor
                        describes a company or the people employed by a company to perform activities or
                        services governed by a contractual arrangement between a contractor and the company,
                        who performs work for the contractor and who is not directly controlled, or supervised,
                        or paid by Alcoa.&nbsp; Subcontractors shall comply with the same requirements as
                        contractors.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.23 Supervised as
                        used in the definition of contracted services means assignment and direction of
                        the day to day activities of an individual.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.24 Waste Collection,
                        Treatment and Disposal Services is engaged in (1) collecting and/or hauling hazardous
                        waste, non-hazardous waste, and/or recyclable materials within a local area and/or
                        (2) operating hazardous or non-hazardous waste transfer stations. Hazardous waste
                        collection establishments may be responsible for the identification, treatment,
                        packaging, and labelling of waste for the purposes of transport.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1.25 Waste Remediation
                        Services is engaged in one or more of the following: (1) remediation and cleanup
                        of contaminated buildings, mine sites, soil, or ground water; (2) integrated mine
                        reclamation activities, including demolition, soil remediation, waste water treatment,
                        hazardous material removal, contouring land, and re-vegetation; and (3) asbestos,
                        lead paint, and other toxic material abatement.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <b><span style="font-family: Verdana"><span style="font-size: 10pt"></span></span>
                    </b>&nbsp;</p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    &nbsp;</p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    &nbsp;</p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <b><span style="font-family: Verdana"><span style="font-size: 10pt">2.0&nbsp; Instructions
                        for Completing Supplier Questionnaire:<o:p></o:p></span></span></b></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">The paragraph number
                        below matches the question with the same number in the Contractor Pre-qualification
                        Questionnaire.&nbsp; &nbsp; Please check the best answers to the questions and answer
                        all questions.&nbsp; For each question, there is space to enter an optional comment
                        to further clarify the checked answer, if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">1. Date form is completed.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">2. Please enter the
                        legal name of the company.&nbsp; The legal name is the name used to furnish taxes
                        and other reports to local authorities.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">3. Please enter the
                        operating name of the company if not the same as the legal name.&nbsp; If the same,
                        enter “same.”<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">4. <span style="color: black">
                        Please enter your company’s ABN (Australian Business Number).<o:p></o:p></span></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">5. Please enter the
                        company address.&nbsp; The company address is the address used when paying taxes
                        or filing other reports to local authorities.&nbsp; Please enter street address
                        in addition to any postal box, if used.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">6. Please enter contact
                        name.&nbsp; The contact is the person with the responsibility to complete this form.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">7. Please enter contact
                        title.&nbsp; For example enter, “Safety Manager” if the person completing the form
                        is the company safety manager.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">8. Please enter contact
                        e-mail address.&nbsp; If there is no e-mail address, state “none.”<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">9. Please enter contact
                        phone number.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">10. Please enter contact
                        fax number.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">11. Please enter the
                        one category that best describes work or services offered to all of your customers
                        by your company.&nbsp; Site Preparation Contractor, Other Specialty Site Contractor,
                        Waste Collection, Treatment and Disposal Services, and Waste Remediation Services
                        are in the definitions.
                        <o:p></o:p>
                    </span></span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">12. Safety hazard assessment
                        and job specific safety plans are in the definitions.&nbsp; Please answer Yes or
                        No.&nbsp; If Yes, please provide examples of a safety hazard assessment and job
                        specific safety plan.&nbsp; Provide an explanation of your company’s usual process
                        to eliminate safety hazards.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">13. Please enter the
                        one best answer as A, B, C, D or E.&nbsp; The question is asking the frequency at
                        which safety activities are conducted and documented.&nbsp; Enter an optional comment
                        if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">14. Please enter Yes
                        or No for A, B, C an D.&nbsp; Owner and lead person are in the definitions.&nbsp;
                        If a large company with clear functions, senior management would be equivalent to
                        owner.&nbsp; If a publicly traded company, senior management would represent the
                        stock owners.&nbsp; If other is checked yes, please specify others who participate.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">15. Please enter Yes
                        or No for A, B and C.&nbsp; Please provide details of significant changes that have
                        occurred in the last 3 years for A, B or C if Yes is answered.&nbsp; For publicly
                        traded companies, do not include stock trades under ownership.&nbsp; Do include
                        acquisition of your company by other companies.&nbsp; If insurance carriers has
                        changed, please provide details of why it has changed.&nbsp; Other changes which
                        may affect favourably affect safety performance results should be entered for C.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">16. Please enter Yes
                        or No for A, B and C.&nbsp; Please describe any advanced training completed by crafts
                        or crew leader in B and C.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">17. Please enter Yes
                        or No for A and B.&nbsp; If item B is checked Yes please attach an example of a
                        certificate issued by a recognized authority.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">18. Please enter the
                        one best answer as A, B, C or D.&nbsp; The phrase “working in the industry” is important.&nbsp;
                        Work force is total work force, both salary and hourly.&nbsp; Do not include any
                        contractors or subcontractors that may work for your company.&nbsp; Enter an optional
                        comment if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">19. Please enter the
                        one best answer as A, B, C or D.&nbsp; Lead person is in the definitions.&nbsp;
                        Enter an optional comment if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">20. Please enter Yes
                        or No for A and B.&nbsp; Safety trainer can be a person directly employed by your
                        company or a person or company contracted to perform safety training.&nbsp; Please
                        describe qualifications for any questions marked Yes.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">21. Please enter the
                        one best answer as A, B, C or D.&nbsp; Subcontractor is in the definitions.&nbsp;
                        Enter an optional comment if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">22. Please enter Yes
                        or No for A, B and C.&nbsp; Substance abuse is the use of illegal or illicit substances
                        or the abuse of approved substances.&nbsp; Enter an optional comment if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">23. Please enter the
                        one best answer as A, B, C or D.&nbsp; The question is asking the rate at which
                        either safety meetings, job safety conditions or job safety performance are audited
                        and documented.&nbsp; Please attach the results from an audit.&nbsp; Enter an optional
                        comment if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">24. Please enter the
                        one best answer as A, B, C or D.&nbsp; Work crew are the people actually doing the
                        work at the site.&nbsp; If Other is checked, please explain.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">25. Please enter Yes
                        or No for A, B and C.&nbsp; This question refers to your company and any subcontractors
                        during the last 5 years.&nbsp; Please attach details for every Yes answer.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">26. Please enter Yes
                        or No.&nbsp; Examples of a government agency are <span style="color: black">WorkSafe,
                            EnergySafe and Department of Consumer and Employment Protection (DOCEP)</span>.&nbsp;
                        Example of an insurance report is a claims report showing employee accidents or
                        illnesses.&nbsp; If yes, attach copies of last 3 years logs.&nbsp; Enter an optional
                        comment if desired.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">27. Please enter the
                        requested information for the designated years.&nbsp; Lost workday, restricted work,
                        medical treatment, first aid, injury free event and accident insurance premium multiplier
                        are in the definitions.&nbsp; Make sure to enter the number of cases, not days.&nbsp;
                        Do not enter first aid cases under medical treatment.&nbsp; The total hours worked
                        is for the whole company, including hourly and salary.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">28. Please provide
                        details related to your Work Cover requirements.&nbsp;
                        <o:p></o:p>
                    </span></span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">29(i). Please enter
                        Yes or No for A, B, C and D.&nbsp; This question refers to your company.&nbsp; Please
                        enter a description of the citation for each Yes answer.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">29(ii). Please provide
                        details of any prohibition, improvement, citations or other breaches issued from
                        regulatory authorities in the past three years.&nbsp;
                        <o:p></o:p>
                    </span></span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-size: 10pt; font-family: Verdana">
                        <o:p>&nbsp;</o:p>
                        &nbsp; </span>
                </p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt; text-align: justify">
                    <span style="font-family: Verdana"><span style="font-size: 10pt">30. Please enter the
                        one best answer as A, B or C.&nbsp; Please describe details of where your company
                        has worked during the last 3 years.<o:p></o:p></span></span></p>
                <p class="MsoPlainText" style="margin: 0cm 0cm 0pt 35.45pt; text-align: justify">
                    <br />
                </p>
            </td>
        </tr>
    </table>
    </p>
</asp:Content>
