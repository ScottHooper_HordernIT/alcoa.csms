﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Reports_MonthlyReports"
    CodeBehind="Reports_MonthlyReports.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<style type="text/css">
    .style1
    {
        height: 24px;
    }
</style>
<table border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td class="pageName" style="text-align: left; width: 846px; height: 43px;">
            <span class="title">Reports</span><br />
            <span class="date">Monthly Management Summary</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" />
        </td>
    </tr>
    <tr>
        <td class="pageName" style="text-align: left; width: 900px;">
            <table border="0">
                <tr>
                    <td style="width: 77px; height: 24px; text-align: right">
                        <strong>Operation: </strong>
                    </td>
                    <td class="style1">
                        <dx:ASPxComboBox ID="cbRegion" runat="server" DataSourceID="dsRegionsFilter" TextField="RegionName"
                            ValueField="RegionId" ValueType="System.Int32" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="170px" IncrementalFilteringMode="StartsWith"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings SetFocusOnError="True" ValidationGroup="Filter" Display="Dynamic">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td colspan="1" style="width: 90px; height: 24px; text-align: right">
                        <strong>Company: </strong>
                    </td>
                    <td colspan="1" style="width: 167px; height: 24px">
                        <dx:ASPxComboBox ID="cbCompany" runat="server" ValueType="System.Int32" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="170px" IncrementalFilteringMode="StartsWith"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings SetFocusOnError="True" ValidationGroup="Filter" Display="Dynamic">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td colspan="1" style="width: 96px; height: 24px; text-align: right">
                        <strong>Month/Year: </strong>
                    </td>
                    <td colspan="1" style="width: 55px; height: 24px">
                        <dx:ASPxComboBox ID="cbMonth" runat="server" ValueType="System.Int32" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="67px" DataSourceID="dsMonths" TextField="MonthName"
                            ValueField="MonthId" IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings SetFocusOnError="True" ValidationGroup="Filter" Display="Dynamic">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td colspan="1" style="width: 74px; height: 24px">
                        <dx:ASPxComboBox ID="cbYear" runat="server" ValueType="System.Int32" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="60px" IncrementalFilteringMode="StartsWith"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings Display="Dynamic">
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td colspan="1" style="width: 115px; height: 24px; text-align: right">
                        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" Text="Go / Refresh" ValidationGroup="site"
                            Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        </dx:ASPxButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align: right" align="right">
                        <table>
                            <tr>
                                <td>
                                    <strong style="text-align: right">Current Report:</strong>
                                </td>
                                <td>
                                    <dx:ASPxLabel ID="lblLayout" runat="server" Font-Bold="False" Font-Italic="False"
                                        Text="(Default)" Width="100%">
                                    </dx:ASPxLabel>
                                </td>
                                <td colspan="1" style="width: 115px; height: 24px; text-align: right">
                                    <dx:ASPxButton ID="btn1" runat="server" AutoPostBack="False" Text="Report Manager"
                                        UseSubmitBehavior="False" Width="120px">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="pageName" style="width: 900px; height: 30px; text-align: left">
            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"  
                Width="100%" DataSourceID="sqldsMMS_BySite" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize">
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Width="11px" />
                    <ExpandedButton Width="11px" />
                    <DetailCollapsedButton Width="11px" />
                    <DetailExpandedButton Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <SettingsPager PageSize="25">
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" />
                <Settings ShowGroupPanel="True" ShowGroupFooter="VisibleAlways" ShowFooter="True"
                    ShowGroupedColumns="True" ShowFilterRow="True" ShowPreview="True" ShowHorizontalScrollBar="True"
                    ShowVerticalScrollBar="True" VerticalScrollableHeight="245" />
                    <SettingsCookies CookiesID="mms" Version="0.1" />
                <Columns>
                    <dx:GridViewDataTextColumn UnboundType="DateTime" Visible="False">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="CompanyName" FixedStyle="Left" SortIndex="0"
                        SortOrder="Ascending" VisibleIndex="0" Width="205px">
                        <Settings SortMode="DisplayText" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
                        FieldName="CompanySiteCategoryId" SortIndex="1" SortOrder="Ascending" UnboundType="String"
                        VisibleIndex="1" FixedStyle="Left" Width="145px">
                        <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                            TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                            IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" FixedStyle="Left"
                        GroupIndex="1" Name="SiteBox" SortIndex="1" SortOrder="Ascending" UnboundType="String"
                        VisibleIndex="2" Settings-SortMode="DisplayText" Width="125px">
                        <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn FieldName="TOTAL MANHOURS CURRENT MONTH" Name="CurrentMonth"
                        VisibleIndex="3">
                        <PropertiesTextEdit DisplayFormatString="n" NullDisplayText="0">
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TOTAL MANHOURS TO PREVIOUS MONTH" Name="PreviousMonth"
                        Visible="False" VisibleIndex="3">
                        <PropertiesTextEdit DisplayFormatString="n">
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="MANHOURS YTD" ReadOnly="True" VisibleIndex="4">
                        <PropertiesTextEdit DisplayFormatString="n">
                        </PropertiesTextEdit>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Peak No people on site" ReadOnly="True" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Average No people on site" ReadOnly="True"
                        VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No of LWD" FieldName="Current LWD" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No of LWD Injuries" FieldName="Current LWDi"
                        VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No of RST" FieldName="Current RST" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No of MT" FieldName="Current MT" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No of FA" FieldName="Current FA" VisibleIndex="11">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="No of IFE" FieldName="Current IFE" VisibleIndex="12">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Current TRIFR" VisibleIndex="13">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Current LWDIFR" VisibleIndex="14">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Current AIFR" VisibleIndex="15">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Current IFEFR" VisibleIndex="16">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD LWD" ReadOnly="True" VisibleIndex="17">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD LWD Injuries" FieldName="YTD LWDi" ReadOnly="True"
                        VisibleIndex="18">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD RST" ReadOnly="True" VisibleIndex="19">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD MT" ReadOnly="True" VisibleIndex="20">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD FA" ReadOnly="True" VisibleIndex="21">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD IFE" ReadOnly="True" VisibleIndex="22">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD TRIFR" ReadOnly="True" VisibleIndex="23">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD LWDIFR" ReadOnly="True" VisibleIndex="24">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD AIFR" ReadOnly="True" VisibleIndex="25">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD IFEFR" ReadOnly="True" VisibleIndex="26">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD IFE/injury ytd" ReadOnly="True" VisibleIndex="27">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="H &amp; S Work Contacts" VisibleIndex="28">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD H &amp; S Work Contacts" ReadOnly="True"
                        VisibleIndex="29">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Behavioural Observations" VisibleIndex="30">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD Behavioural Observations" ReadOnly="True"
                        VisibleIndex="31">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD Workplace Inspections" ReadOnly="True"
                        VisibleIndex="32">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Workplace Inspections" VisibleIndex="33">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="YTD EHS Plan" ReadOnly="True" VisibleIndex="34">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="JSA Field Audits" FieldName="JSA Score" ReadOnly="True"
                        VisibleIndex="35">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Fatality Prevention" ReadOnly="True" VisibleIndex="36">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Mandated Training %" ReadOnly="True" VisibleIndex="36">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Compliance Score %" FieldName="ComplianceScoreMonth"
                        ReadOnly="True" VisibleIndex="38">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Compliance Score % (YTD)" FieldName="ComplianceScoreYTD"
                        ReadOnly="True" VisibleIndex="39">
                    </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="General Hours" ReadOnly="True" VisibleIndex="42">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Calciner Expense" ReadOnly="True" VisibleIndex="43">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Calciner Capital" ReadOnly="True" VisibleIndex="44">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Residue" ReadOnly="True" VisibleIndex="45">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Eng Project Hrs - Refinery" ReadOnly="True" VisibleIndex="46">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Eng Project Hrs - Residue" ReadOnly="True" VisibleIndex="47">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Eng Project Hrs - Smelting" ReadOnly="True" VisibleIndex="48">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Eng Project Hrs - Power" ReadOnly="True" VisibleIndex="49">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="No. Employees excess 16/64" ReadOnly="True" VisibleIndex="50">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="No. of Restricted Days" ReadOnly="True" VisibleIndex="51">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Alcoa Annual Audit Score" ReadOnly="True" VisibleIndex="52">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="No. of Non conformance issues" ReadOnly="True" VisibleIndex="53">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 1 Hours" ReadOnly="True" VisibleIndex="54">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 1 Po" ReadOnly="True" VisibleIndex="55">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 1 Line" ReadOnly="True" VisibleIndex="56">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 2 Hours" ReadOnly="True" VisibleIndex="57">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 2 Po" ReadOnly="True" VisibleIndex="58">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 2 Line" ReadOnly="True" VisibleIndex="59">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 3 Hours" ReadOnly="True" VisibleIndex="60">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 3 Po" ReadOnly="True" VisibleIndex="61">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 3 Line" ReadOnly="True" VisibleIndex="62">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 4 Hours" ReadOnly="True" VisibleIndex="63">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 4 Po" ReadOnly="True" VisibleIndex="64">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 4 Line" ReadOnly="True" VisibleIndex="65">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 5 Hours" ReadOnly="True" VisibleIndex="66">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 5 Po" ReadOnly="True" VisibleIndex="67">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 5 Line" ReadOnly="True" VisibleIndex="68">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 6 Hours" ReadOnly="True" VisibleIndex="69">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 6 Po" ReadOnly="True" VisibleIndex="70">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 6 Line" ReadOnly="True" VisibleIndex="71">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 7 Hours" ReadOnly="True" VisibleIndex="72">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 7 Po" ReadOnly="True" VisibleIndex="73">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 7 Line" ReadOnly="True" VisibleIndex="74">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 8 Hours" ReadOnly="True" VisibleIndex="75">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 8 Po" ReadOnly="True" VisibleIndex="76">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 8 Line" ReadOnly="True" VisibleIndex="77">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 9 Hours" ReadOnly="True" VisibleIndex="78">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 9 Po" ReadOnly="True" VisibleIndex="79">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 9 Line" ReadOnly="True" VisibleIndex="80">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 10 Hours" ReadOnly="True" VisibleIndex="81">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 10 Po" ReadOnly="True" VisibleIndex="82">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 10 Line" ReadOnly="True" VisibleIndex="83">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 11 Hours" ReadOnly="True" VisibleIndex="84">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 11 Po" ReadOnly="True" VisibleIndex="85">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 11 Line" ReadOnly="True" VisibleIndex="86">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 12 Hours" ReadOnly="True" VisibleIndex="87">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 12 Po" ReadOnly="True" VisibleIndex="88">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 12 Line" ReadOnly="True" VisibleIndex="89">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 13 Hours" ReadOnly="True" VisibleIndex="90">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 13 Po" ReadOnly="True" VisibleIndex="91">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 13 Line" ReadOnly="True" VisibleIndex="92">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 14 Hours" ReadOnly="True" VisibleIndex="93">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 14 Po" ReadOnly="True" VisibleIndex="94">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 14 Line" ReadOnly="True" VisibleIndex="95">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 15 Hours" ReadOnly="True" VisibleIndex="96">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 15 Po" ReadOnly="True" VisibleIndex="97">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Project Capital 15 Line" ReadOnly="True" VisibleIndex="98">
                </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD Blood Alcohol Tests" FieldName="YTD FFWBloodAlcoholTests" ReadOnly="True" VisibleIndex="40">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD Positive BAC Results" FieldName="YTD FFWPositiveBACResults" ReadOnly="True" VisibleIndex="41">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD Drug Tests" FieldName="YTD FFWDrugTests" ReadOnly="True" VisibleIndex="42">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD Non Negative Results" FieldName="YTD FFWNonNegativeResults" ReadOnly="True" VisibleIndex="43">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD Positive Results Medication Declared" FieldName="YTD FFWPositiveResultsMedDeclared" ReadOnly="True" VisibleIndex="44">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="YTD Positive Results Medication Not Declared" FieldName="YTD FFWPositiveResultsMedNotDeclared" ReadOnly="True" VisibleIndex="45">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <GroupSummary>
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="TOTAL MANHOURS CURRENT MONTH"
                        SummaryType="Sum" ShowInGroupFooterColumn="TOTAL MANHOURS CURRENT MONTH" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="TOTAL MANHOURS TO PREVIOUS MONTH"
                        SummaryType="Sum" ShowInGroupFooterColumn="TOTAL MANHOURS TO PREVIOUS MONTH" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="MANHOURS YTD" SummaryType="Sum"
                        ShowInGroupFooterColumn="MANHOURS YTD" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Peak No people on site" SummaryType="Sum"
                        ShowInGroupFooterColumn="Peak No people on site" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Average No people on site" SummaryType="Sum"
                        ShowInGroupFooterColumn="Average No people on site" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current LWD" SummaryType="Sum"
                        ShowInGroupFooterColumn="Current LWD" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current LWDi" SummaryType="Sum"
                        ShowInGroupFooterColumn="Current LWDi" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current RST" SummaryType="Sum"
                        ShowInGroupFooterColumn="Current RST" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current MT" SummaryType="Sum"
                        ShowInGroupFooterColumn="Current MT" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current FA" SummaryType="Sum"
                        ShowInGroupFooterColumn="Current FA" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current IFE" SummaryType="Sum"
                        ShowInGroupFooterColumn="Current IFE" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current TRIFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="Current TRIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current LWDIFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="Current LWDIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current AIFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="Current AIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current IFEFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="Current IFEFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD LWD" SummaryType="Sum" ShowInGroupFooterColumn="YTD LWD" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD LWDi" SummaryType="Sum" ShowInGroupFooterColumn="YTD LWDi" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD RST" SummaryType="Sum" ShowInGroupFooterColumn="YTD RST" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD MT" SummaryType="Sum" ShowInGroupFooterColumn="YTD MT" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FA" SummaryType="Sum" ShowInGroupFooterColumn="YTD FA" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD IFE" SummaryType="Sum" ShowInGroupFooterColumn="YTD IFE" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD TRIFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="YTD TRIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD LWDIFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="YTD LWDIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD AIFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="YTD AIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD IFEFR" SummaryType="Custom"
                        ShowInGroupFooterColumn="YTD IFEFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD IFE/injury ytd" SummaryType="Custom"
                        ShowInGroupFooterColumn="YTD IFE/injury ytd" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="H &amp; S Work Contacts" SummaryType="Sum"
                        ShowInGroupFooterColumn="H &amp; S Work Contacts" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD H &amp; S Work Contacts" SummaryType="Sum"
                        ShowInGroupFooterColumn="YTD H &amp; S Work Contacts" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Behavioural Observations" SummaryType="Sum"
                        ShowInGroupFooterColumn="Behavioural Observations" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD Behavioural Observations"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD Behavioural Observations" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD Workplace Inspections" SummaryType="Sum"
                        ShowInGroupFooterColumn="YTD Workplace Inspections" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Workplace Inspections" SummaryType="Sum"
                        ShowInGroupFooterColumn="Workplace Inspections" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD EHS Plan" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="JSA Score" SummaryType="Sum" ShowInGroupFooterColumn="JSA Score" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Fatality Prevention" SummaryType="Sum"
                        ShowInGroupFooterColumn="Fatality Prevention" />
                    <dx:ASPxSummaryItem FieldName="Mandated Training %" SummaryType="Average" DisplayFormat="{0:n}"
                        ShowInGroupFooterColumn="Mandated Training %" />
                    <dx:ASPxSummaryItem FieldName="ComplianceScoreMonth" SummaryType="Average" DisplayFormat="{0:n}"
                        ShowInGroupFooterColumn="ComplianceScoreMonth" />
                    <dx:ASPxSummaryItem FieldName="ComplianceScoreYTD" SummaryType="Average" DisplayFormat="{0:n}"
                        ShowInGroupFooterColumn="ComplianceScoreYTD" />

                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="General Hours" ShowInGroupFooterColumn="General Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Calciner Expense" ShowInGroupFooterColumn="Calciner Expense" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Calciner Capital" ShowInGroupFooterColumn="Calciner Capital" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Residue" ShowInGroupFooterColumn="Residue" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Refinery" ShowInGroupFooterColumn="Eng Project Hrs - Refinery" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Residue" ShowInGroupFooterColumn="Eng Project Hrs - Residue" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Smelting" ShowInGroupFooterColumn="Eng Project Hrs - Smelting" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Power" ShowInGroupFooterColumn="Eng Project Hrs - Power" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="No. Employees excess 16/64" ShowInGroupFooterColumn="No. Employees excess 16/64" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="No. of Restricted Days" ShowInGroupFooterColumn="No. of Restricted Days" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Alcoa Annual Audit Score" ShowInGroupFooterColumn="Alcoa Annual Audit Score" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="No. of Non conformance issues" ShowInGroupFooterColumn="No. of Non conformance issues" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 1 Hours" ShowInGroupFooterColumn="Project Capital 1 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 1 Po" ShowInGroupFooterColumn="Project Capital 1 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 1 Line" ShowInGroupFooterColumn="Project Capital 1 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 2 Hours" ShowInGroupFooterColumn="Project Capital 2 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 2 Po" ShowInGroupFooterColumn="Project Capital 2 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 2 Line" ShowInGroupFooterColumn="Project Capital 2 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 3 Hours" ShowInGroupFooterColumn="Project Capital 3 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 3 Po" ShowInGroupFooterColumn="Project Capital 3 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 3 Line" ShowInGroupFooterColumn="Project Capital 3 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 4 Hours" ShowInGroupFooterColumn="Project Capital 4 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 4 Po" ShowInGroupFooterColumn="Project Capital 4 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 4 Line" ShowInGroupFooterColumn="Project Capital 4 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 5 Hours" ShowInGroupFooterColumn="Project Capital 5 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 5 Po" ShowInGroupFooterColumn="Project Capital 5 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 5 Line" ShowInGroupFooterColumn="Project Capital 5 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 6 Hours" ShowInGroupFooterColumn="Project Capital 6 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 6 Po" ShowInGroupFooterColumn="Project Capital 6 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 6 Line" ShowInGroupFooterColumn="Project Capital 6 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 7 Hours" ShowInGroupFooterColumn="Project Capital 7 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 7 Po" ShowInGroupFooterColumn="Project Capital 7 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 7 Line" ShowInGroupFooterColumn="Project Capital 7 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 8 Hours" ShowInGroupFooterColumn="Project Capital 8 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 8 Po" ShowInGroupFooterColumn="Project Capital 8 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 8 Line" ShowInGroupFooterColumn="Project Capital 8 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 9 Hours" ShowInGroupFooterColumn="Project Capital 9 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 9 Po" ShowInGroupFooterColumn="Project Capital 9 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 9 Line" ShowInGroupFooterColumn="Project Capital 9 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 10 Hours" ShowInGroupFooterColumn="Project Capital 10 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 10 Po" ShowInGroupFooterColumn="Project Capital 10 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 10 Line" ShowInGroupFooterColumn="Project Capital 10 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 11 Hours" ShowInGroupFooterColumn="Project Capital 11 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 11 Po" ShowInGroupFooterColumn="Project Capital 11 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 11 Line" ShowInGroupFooterColumn="Project Capital 11 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 12 Hours" ShowInGroupFooterColumn="Project Capital 12 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 12 Po" ShowInGroupFooterColumn="Project Capital 12 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 12 Line" ShowInGroupFooterColumn="Project Capital 12 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 13 Hours" ShowInGroupFooterColumn="Project Capital 13 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 13 Po" ShowInGroupFooterColumn="Project Capital 13 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 13 Line" ShowInGroupFooterColumn="Project Capital 13 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 14 Hours" ShowInGroupFooterColumn="Project Capital 14 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 14 Po" ShowInGroupFooterColumn="Project Capital 14 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 14 Line" ShowInGroupFooterColumn="Project Capital 14 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 15 Hours" ShowInGroupFooterColumn="Project Capital 15 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 15 Po" ShowInGroupFooterColumn="Project Capital 15 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 15 Line" ShowInGroupFooterColumn="Project Capital 15 Line" SummaryType="Sum" />
                     <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="YTD FFWBloodAlcoholTests"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD FFWBloodAlcoholTests" />
                     <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="YTD FFWPositiveBACResults"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD FFWPositiveBACResults" />
                     <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="YTD FFWDrugTests"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD FFWDrugTests" />
                     <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="YTD FFWNonNegativeResults"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD FFWNonNegativeResults" />
                     <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="YTD FFWPositiveResultsMedDeclared"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD FFWPositiveResultsMedDeclared" />
                     <dx:ASPxSummaryItem DisplayFormat="{0}"
                        FieldName="YTD FFWPositiveResultsMedNotDeclared"
                        SummaryType="Sum" ShowInGroupFooterColumn="YTD FFWPositiveResultsMedNotDeclared" />
                </GroupSummary>
                <TotalSummary>
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="TOTAL MANHOURS CURRENT MONTH"
                        SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="TOTAL MANHOURS TO PREVIOUS MONTH"
                        SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="MANHOURS YTD" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Peak No people on site" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Average No people on site" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current LWD" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current LWDi" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current RST" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current MT" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current FA" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current IFE" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current TRIFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current LWDIFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current AIFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Current IFEFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD LWD" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD LWDi" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD RST" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD MT" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FA" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD IFE" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD TRIFR" SummaryType="Custom"
                        ShowInColumn="YTD TRIFR" ShowInGroupFooterColumn="YTD TRIFR" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD LWDIFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD AIFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD IFEFR" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD IFE/injury ytd" SummaryType="Custom" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="H &amp; S Work Contacts" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD H &amp; S Work Contacts" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Behavioural Observations" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD Behavioural Observations"
                        SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="EH &amp; S Audits" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD Workplace Inspections" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Workplace Inspections" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="JSA Score" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Fatality Prevention" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="Mandated Training %" SummaryType="Average" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="ComplianceScoreMonth" SummaryType="Average" />
                    <dx:ASPxSummaryItem DisplayFormat="{0:n}" FieldName="ComplianceScoreYTD" SummaryType="Average" />

                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="General Hours" ShowInGroupFooterColumn="General Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Calciner Expense" ShowInGroupFooterColumn="Calciner Expense" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Calciner Capital" ShowInGroupFooterColumn="Calciner Capital" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Residue" ShowInGroupFooterColumn="Residue" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Refinery" ShowInGroupFooterColumn="Eng Project Hrs - Refinery" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Residue" ShowInGroupFooterColumn="Eng Project Hrs - Residue" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Smelting" ShowInGroupFooterColumn="Eng Project Hrs - Smelting" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Eng Project Hrs - Power" ShowInGroupFooterColumn="Eng Project Hrs - Power" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="No. Employees excess 16/64" ShowInGroupFooterColumn="No. Employees excess 16/64" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="No. of Restricted Days" ShowInGroupFooterColumn="No. of Restricted Days" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Alcoa Annual Audit Score" ShowInGroupFooterColumn="Alcoa Annual Audit Score" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="No. of Non conformance issues" ShowInGroupFooterColumn="No. of Non conformance issues" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 1 Hours" ShowInGroupFooterColumn="Project Capital 1 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 1 Po" ShowInGroupFooterColumn="Project Capital 1 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 1 Line" ShowInGroupFooterColumn="Project Capital 1 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 2 Hours" ShowInGroupFooterColumn="Project Capital 2 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 2 Po" ShowInGroupFooterColumn="Project Capital 2 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 2 Line" ShowInGroupFooterColumn="Project Capital 2 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 3 Hours" ShowInGroupFooterColumn="Project Capital 3 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 3 Po" ShowInGroupFooterColumn="Project Capital 3 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 3 Line" ShowInGroupFooterColumn="Project Capital 3 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 4 Hours" ShowInGroupFooterColumn="Project Capital 4 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 4 Po" ShowInGroupFooterColumn="Project Capital 4 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 4 Line" ShowInGroupFooterColumn="Project Capital 4 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 5 Hours" ShowInGroupFooterColumn="Project Capital 5 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 5 Po" ShowInGroupFooterColumn="Project Capital 5 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 5 Line" ShowInGroupFooterColumn="Project Capital 5 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 6 Hours" ShowInGroupFooterColumn="Project Capital 6 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 6 Po" ShowInGroupFooterColumn="Project Capital 6 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 6 Line" ShowInGroupFooterColumn="Project Capital 6 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 7 Hours" ShowInGroupFooterColumn="Project Capital 7 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 7 Po" ShowInGroupFooterColumn="Project Capital 7 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 7 Line" ShowInGroupFooterColumn="Project Capital 7 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 8 Hours" ShowInGroupFooterColumn="Project Capital 8 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 8 Po" ShowInGroupFooterColumn="Project Capital 8 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 8 Line" ShowInGroupFooterColumn="Project Capital 8 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 9 Hours" ShowInGroupFooterColumn="Project Capital 9 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 9 Po" ShowInGroupFooterColumn="Project Capital 9 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 9 Line" ShowInGroupFooterColumn="Project Capital 9 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 10 Hours" ShowInGroupFooterColumn="Project Capital 10 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 10 Po" ShowInGroupFooterColumn="Project Capital 10 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 10 Line" ShowInGroupFooterColumn="Project Capital 10 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 11 Hours" ShowInGroupFooterColumn="Project Capital 11 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 11 Po" ShowInGroupFooterColumn="Project Capital 11 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 11 Line" ShowInGroupFooterColumn="Project Capital 11 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 12 Hours" ShowInGroupFooterColumn="Project Capital 12 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 12 Po" ShowInGroupFooterColumn="Project Capital 12 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 12 Line" ShowInGroupFooterColumn="Project Capital 12 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 13 Hours" ShowInGroupFooterColumn="Project Capital 13 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 13 Po" ShowInGroupFooterColumn="Project Capital 13 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 13 Line" ShowInGroupFooterColumn="Project Capital 13 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 14 Hours" ShowInGroupFooterColumn="Project Capital 14 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 14 Po" ShowInGroupFooterColumn="Project Capital 14 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 14 Line" ShowInGroupFooterColumn="Project Capital 14 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 15 Hours" ShowInGroupFooterColumn="Project Capital 15 Hours" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 15 Po" ShowInGroupFooterColumn="Project Capital 15 Po" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="Project Capital 15 Line" ShowInGroupFooterColumn="Project Capital 15 Line" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FFWBloodAlcoholTests" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FFWPositiveBACResults" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FFWDrugTests" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FFWNonNegativeResults" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FFWPositiveResultsMedDeclared" SummaryType="Sum" />
                    <dx:ASPxSummaryItem DisplayFormat="{0}" FieldName="YTD FFWPositiveResultsMedNotDeclared" SummaryType="Sum" />
                </TotalSummary>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dx:ASPxGridView>
        </td>
    </tr>
    <tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align: -moz-right;
            width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
    </tr>
</table>
<asp:ObjectDataSource ID="sqldsMMS_BySite" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.Kpi"
    TypeName="KaiZen.CSMS.Services.KpiService" SelectMethod="mmSummary_BySite" DeleteMethod="Delete"
    InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" UpdateMethod="Update">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="9" Name="PREVIOUSMONTH" SessionField="spVar_MonthPrevious"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="10" Name="CURRENTMONTH" SessionField="spVar_Month"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="2007" Name="CURRENTYEAR" SessionField="spVar_Year"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
            Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>
<asp:SqlDataSource ID="sqldsMMS_BySite2" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_mmSummary_BySite" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="9" Name="PREVIOUSMONTH" SessionField="spVar_MonthPrevious"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="10" Name="CURRENTMONTH" SessionField="spVar_Month"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="2007" Name="CURRENTYEAR" SessionField="spVar_Year"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsMMS_BySite_ByCompany" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_mmSummary_BySite_ByCompany" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="9" Name="PREVIOUSMONTH" SessionField="spVar_MonthPrevious"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="10" Name="CURRENTMONTH" SessionField="spVar_Month"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="2007" Name="CURRENTYEAR" SessionField="spVar_Year"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spVar_CompanyId"
            Type="Int32" />
        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True"
    Sort="Ordinal ASC">
</data:RegionsDataSource>
<data:CompaniesDataSource ID="dsCompaniesExcludeAlcoa" runat="server" Filter="CompanyName != 'Alcoa'"
    Sort="CompanyName ASC">
</data:CompaniesDataSource>
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True"
    Sort="SiteName ASC">
</data:SitesDataSource>
<data:MonthsDataSource ID="dsMonths" runat="server" Sort="MonthId" SelectMethod="GetAll">
</data:MonthsDataSource>
<asp:SqlDataSource ID="sqldsKPICompanies" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Companies.CompanyName, dbo.Companies.CompanyID FROM dbo.KPI INNER JOIN dbo.Companies ON dbo.KPI.companyID = dbo.Companies.CompanyID GROUP BY dbo.Companies.CompanyName, dbo.Companies.CompanyID ORDER BY dbo.Companies.CompanyName ASC" />
<dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" AllowDragging="True" AllowResize="True"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    EnableHotTrack="False" HeaderText="Report Manager" Height="213px" Modal="True"
    PopupElementID="btn1" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="623px">
    <SizeGripImage Height="16px" Width="16px" />
    <HeaderStyle>
        <Paddings PaddingRight="6px" />
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px" />
    <ContentCollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            <dx:ASPxGridView ID="grid2" runat="server" AutoGenerateColumns="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" DataSourceID="sqldsLayouts" KeyFieldName="ReportId"
                OnHtmlRowCreated="grid2_RowCreated" OnInitNewRow="grid2_InitNewRow" OnRowInserting="grid2_RowInserting"
                OnRowUpdating="grid2_RowUpdating" OnRowValidating="grid2_RowValidating" OnStartRowEditing="grid2_StartRowEditing"
                Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" Name="commandCol" ShowInCustomizationForm="False"
                        VisibleIndex="0" Width="100px">
                        <EditButton Text="Edit" Visible="True">
                            <Image AlternateText="Edit" Url="~/Images/gridEdit.gif">
                            </Image>
                        </EditButton>
                        <NewButton Text="New" Visible="True">
                            <Image AlternateText="New" Url="~/Images/gridNew.gif">
                            </Image>
                        </NewButton>
                        <DeleteButton Text="Delete" Visible="True">
                            <Image AlternateText="Delete" Url="~/Images/gridDelete.gif">
                            </Image>
                        </DeleteButton>
                        <CancelButton Text="Cancel" Visible="True">
                            <Image AlternateText="Cancel" Url="../Images/gridCancel.gif">
                            </Image>
                        </CancelButton>
                        <UpdateButton Text="Save" Visible="True">
                            <Image AlternateText="Save" Url="../Images/gridSave.gif">
                            </Image>
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="ReportId" ReadOnly="True" ShowInCustomizationForm="True"
                        Visible="False" VisibleIndex="1">
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption=" " ShowInCustomizationForm="True" ToolTip="Load Selected Custom Report"
                        VisibleIndex="1" Width="30px">
                        <EditFormSettings Visible="False" />
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 30px">
                                        <dx:ASPxHyperLink ID="hlLoad" runat="server" Text="Load">
                                        </dx:ASPxHyperLink>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Area" ShowInCustomizationForm="True" Visible="False"
                        VisibleIndex="2">
                        <EditFormSettings Visible="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ReportName" ShowInCustomizationForm="True"
                        VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="ReportDescription" Name="gcbReportDescription"
                        ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="4">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.String">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn FieldName="ReportLayout" ShowInCustomizationForm="True"
                        Visible="False" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="ModifiedByUserId" ReadOnly="True" ShowInCustomizationForm="True"
                        Visible="False" VisibleIndex="6">
                        <EditFormSettings Visible="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn FieldName="ModifiedDate" ReadOnly="True" ShowInCustomizationForm="True"
                        SortIndex="0" SortOrder="Descending" VisibleIndex="3">
                        <Settings SortMode="Value" />
                        <EditFormSettings Visible="True" />
                    </dx:GridViewDataDateColumn>
                </Columns>
                <SettingsBehavior ConfirmDelete="True" />
                <SettingsEditing Mode="Inline" />
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                </Styles>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sqldsLayouts" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                DeleteCommand="DELETE FROM [CustomReportingLayouts] WHERE [ReportId] = @ReportId"
                InsertCommand="INSERT INTO [CustomReportingLayouts] ([Area], [ReportName], [ReportDescription], [ReportLayout], 

[ModifiedByUserId], [ModifiedDate]) VALUES (@Area, @ReportName, @ReportDescription, @ReportLayout, @ModifiedByUserId, 

@ModifiedDate)" SelectCommand="SELECT * FROM [CustomReportingLayouts] WHERE ([ModifiedByUserId] = @ModifiedByUserId AND Area = 

'MMS')" UpdateCommand="UPDATE [CustomReportingLayouts] SET [Area] = @Area, [ReportName] = @ReportName, [ReportDescription] 

= @ReportDescription, [ReportLayout] = @ReportLayout, [ModifiedByUserId] = @ModifiedByUserId, [ModifiedDate] = @ModifiedDate WHERE 

[ReportId] = @ReportId">
                <DeleteParameters>
                    <asp:Parameter Name="ReportId" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Area" Type="String" />
                    <asp:Parameter Name="ReportName" Type="String" />
                    <asp:Parameter Name="ReportDescription" Type="String" />
                    <asp:Parameter Name="ReportLayout" Type="String" />
                    <asp:Parameter Name="ModifiedByUserId" Type="Int32" />
                    <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                    <asp:SessionParameter DefaultValue="0" Name="ModifiedByUserId" SessionField="UserId"
                        Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Area" Type="String" />
                    <asp:Parameter Name="ReportName" Type="String" />
                    <asp:Parameter Name="ReportDescription" Type="String" />
                    <asp:Parameter Name="ReportLayout" Type="String" />
                    <asp:Parameter Name="ModifiedByUserId" Type="Int32" />
                    <asp:Parameter Name="ModifiedDate" Type="DateTime" />
                    <asp:Parameter Name="ReportId" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>
