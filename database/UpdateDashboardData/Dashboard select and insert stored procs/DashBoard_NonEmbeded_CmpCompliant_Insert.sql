
/****** Object:  StoredProcedure [dbo].[DashBoard_NonEmbeded_CmpCompliant_Insert]    Script Date: 3/08/2015 1:28:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DashBoard_NonEmbeded_CmpCompliant_Insert]        
(        
 @Site int        
 ,@Month int        
 ,@Year int        
         
 ,@NumberofCompanies varchar(200)      
 ,@ContractorServicesAudit varchar(200)      
 ,@WorkplaceSafetyCompliance varchar(200)      
 ,@ManagementHealthSafetyWorkContacts varchar(200)      
 ,@BehaviouralObservations varchar(200)      
 ,@JSAFieldAuditVerifications varchar(200)       
 ,@ToolboxMeetings varchar(200)      
 ,@AlcoaWeeklyContractorsMeeting varchar(200)      
 ,@AlcoaMonthlyContractorsMeeting varchar(200)      
 ,@MandatedTraining varchar(200)      
        
)        
AS   
  
IF EXISTS(select * from DashBoard_NonEmbeded_CmpCompliant where SiteId=@Site and [Month]=@Month and [Year]=@Year)       
   
 UPDATE  DashBoard_NonEmbeded_CmpCompliant SET     
    [NE_Number of Companies:]  =  @NumberofCompanies   
     ,[NE_Contractor Services Audit]  =  @ContractorServicesAudit   
     ,[NE_Workplace Safety Compliance] =  @WorkplaceSafetyCompliance    
     ,[NE_Management Health Safety Work Contacts] = @ManagementHealthSafetyWorkContacts       
     ,[NE_Behavioural Observations]  =  @BehaviouralObservations          
     ,[NE_JSA Field Audit Verifications]  =  @JSAFieldAuditVerifications     
     ,[NE_Toolbox Meetings]  =   @ToolboxMeetings  
     ,[NE_Alcoa Weekly Contractors Meeting] = @AlcoaWeeklyContractorsMeeting       
     ,[NE_Alcoa Monthly Contractors Meeting] = @AlcoaMonthlyContractorsMeeting            
     ,[NE_Mandated Training]  = @MandatedTraining 
     ,[LastModifiedDate] = GETDATE() 
 WHERE [SiteId] = @Site                  
   AND [Month] = @Month                  
   AND [Year] = @Year   
ELSE  
    INSERT INTO [dbo].[DashBoard_NonEmbeded_CmpCompliant]        
     (        
     [SiteId]        
     ,[Month]        
     ,[Year]        
     ,[NE_Number of Companies:]        
     ,[NE_Contractor Services Audit]        
     ,[NE_Workplace Safety Compliance]        
     ,[NE_Management Health Safety Work Contacts]        
     ,[NE_Behavioural Observations]             
     ,[NE_JSA Field Audit Verifications]        
     ,[NE_Toolbox Meetings]        
     ,[NE_Alcoa Weekly Contractors Meeting]        
     ,[NE_Alcoa Monthly Contractors Meeting]             
     ,[NE_Mandated Training]        
     ,[LastModifiedDate]   
     )        
    VALUES        
     (        
     @Site        
     ,@Month        
     ,@Year        
     ,@NumberofCompanies        
     ,@ContractorServicesAudit        
     ,@WorkplaceSafetyCompliance        
     ,@ManagementHealthSafetyWorkContacts        
     ,@BehaviouralObservations             
     ,@JSAFieldAuditVerifications        
     ,@ToolboxMeetings        
     ,@AlcoaWeeklyContractorsMeeting        
     ,@AlcoaMonthlyContractorsMeeting             
     ,@MandatedTraining 
     ,GETDATE()       
     )        
----------------------------------------

GO


