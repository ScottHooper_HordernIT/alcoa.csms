﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Data.SqlClient;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;


public class UsersCompaniesMapData
{

    private static String _connectionString;
    private static Boolean _initialized;

    public static void Initialize()
    {
        // Initialize data source. Use "Northwind" connection string from configuration.

        if (ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"] == null ||
            ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString.Trim() == "")
        {
            throw new Exception("A connection string named 'AdventureWorksConnectionString' with a valid connection string " +
                                "must exist in the <connectionStrings> configuration section for the application.");
        }

        _connectionString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

        _initialized = true;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static DataTable GetUsersCompaniesMap(Int32 startRecord, Int32 maxRecords, String sortColumns)
    {
        VerifySortColumns(sortColumns);

        if (!_initialized) { Initialize(); }

        string field = string.Empty;
        string value = string.Empty;
        string whereCondition = string.Empty;
       // maxRecords = 10;
        int filterCount = 1;

        //whereCondition = "where A.Active='false' ";
        whereCondition = " where B.CompanyId<>(Select CompanyId from Users where UserId=A.UserId) ";

        if (HttpContext.Current.Session["UserIdVal"] != null)
        {
            field = "C.UserId";
            value = Convert.ToString(HttpContext.Current.Session["UserIdVal"]);           
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
       
        if (HttpContext.Current.Session["CompanyIdVal"] != null)
        {
            field = "B.CompanyId";
            value = Convert.ToString(HttpContext.Current.Session["CompanyIdVal"]);
            //whereCondition =" where "+ field + " = " + value;
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }       
        

        string sqlSortColumn = "B.CompanyName ASC";

        if (!String.IsNullOrEmpty(sortColumns))
            sqlSortColumn = sortColumns;

        if (sqlSortColumn.Contains("CompanyId"))
        {
            sqlSortColumn = sqlSortColumn.Replace("CompanyId", "B.CompanyName");
        }
        if (sqlSortColumn.Contains("UserId"))
        {
            sqlSortColumn = sqlSortColumn.Replace("UserId", "C.UserId");
        }
        if (sqlSortColumn.Contains("UserLogon"))
        {
            sqlSortColumn = sqlSortColumn.Replace("UserLogon", "C.UserLogon");
        }
       

        String sqlQuery = "SELECT ROW_NUMBER() OVER (ORDER BY " + sqlSortColumn + ") AS rownumber," +
                             "A.UserCompanyId,A.UserId,A.CompanyId,A.Active, "+
		                          "  C.UserLogon,B.CompanyName "+
                            " from userCompany A "+
                            " left outer join Companies B ON A.CompanyId=B.CompanyId "+
                            " left outer join Users C on A.UserId=C.UserId   " + whereCondition;

        if (!String.IsNullOrEmpty(sortColumns))
            sqlSortColumn = sortColumns;

        String sqlCommand = String.Format(
           "SELECT * FROM({0}) AS foo " +
           "WHERE rownumber >= {1} AND rownumber <= {2}",
           sqlQuery,
           startRecord + 1, startRecord + maxRecords
        );

        SqlConnection conn = new SqlConnection(_connectionString);
        SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);

        DataSet ds = new DataSet();

        try
        {
            conn.Open();
            da.Fill(ds, "People");
        }
        catch (SqlException e)
        {
            // Handle exception.
        }
        finally
        {
            conn.Close();
        }

        if (ds.Tables["People"] != null)
            return ds.Tables["People"];

        return null;
    }


    // columns are specified in the sort expression to avoid a SQL Injection attack.

    private static void VerifySortColumns(string sortColumns)
    {
        sortColumns = sortColumns.ToLowerInvariant().Replace(" asc", String.Empty).Replace(" desc", String.Empty);
        String[] columnNames = sortColumns.Split(',');

        foreach (String columnName in columnNames)
        {

            switch (columnName.Trim().ToLowerInvariant())
            {
                case "userid":
                case "userlogon":
                case "companyname":
                case "companyid":
                case "usercompanyid":                
                    break;
                default:
                    throw new ArgumentException("SortColumns contains an invalid column name.");
            }
        }
    }

    public static Int32 GetUsersCompaniesMapCount()
    {
        if (!_initialized) { Initialize(); }

        string field = string.Empty;
        string value = string.Empty;
        string whereCondition = string.Empty;        
        int filterCount = 0;

        //whereCondition = "where Active='false' ";
        whereCondition = " where CompanyId<>(Select CompanyId from Users where UserId=UserCompany.UserId) ";

        if (HttpContext.Current.Session["CompanyIdVal"] != null)
        {
            field = "CompanyId";
            value = Convert.ToString(HttpContext.Current.Session["CompanyIdVal"]);
            whereCondition = " and " + field + " = " + value;
            filterCount++;
        }

        if (HttpContext.Current.Session["UserIdVal"] != null)
        {
            field = "UserId";
            value = Convert.ToString(HttpContext.Current.Session["UserIdVal"]);
            whereCondition = " and " + field + " = " + value;
            filterCount++;
        }
        
        


        string sqlCommand = "SELECT COUNT ([UserCompanyId]) FROM [UserCompany] " + whereCondition;

        SqlConnection conn = new SqlConnection(_connectionString);
        SqlCommand command = new SqlCommand(sqlCommand, conn);
        SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);

        Int32 result = 0;

        try
        {
            conn.Open();
            result = (Int32)command.ExecuteScalar();
        }
        catch (SqlException e)
        {
            // Handle exception.
        }
        finally
        {
            conn.Close();
        }

        return result;
    }

    public static void UpdateUsersCompaniesMap(int UserCompanyId, int UserId, int CompanyId, bool Active)
    {
        try
        {                     
            
            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

            UserCompanyService ucser = new UserCompanyService();
            //KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(_companyId, _siteId, _year);

            KaiZen.CSMS.Entities.UserCompany uc = ucser.GetByUserCompanyId(UserCompanyId);


            KaiZen.CSMS.Entities.UserCompany ucUpdate = new KaiZen.CSMS.Entities.UserCompany();

            ucUpdate.UserCompanyId = uc.UserCompanyId;
            ucUpdate.UserId = uc.UserId;
            ucUpdate.CompanyId = CompanyId;
            ucUpdate.Active = ucUpdate.Active;            

            if (ucUpdate.EntityState != EntityState.Unchanged)
            {                
                DataRepository.UserCompanyProvider.Update(transactionManager, ucUpdate);
                transactionManager.Commit();
            }
        }
        catch (Exception ex)
        {
            
            
        }
    }

    public static void InsertUsersCompaniesMap(int UserId, int CompanyId, bool Active)
    {
        try
        {

            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

            
                KaiZen.CSMS.Entities.UserCompany ucUpdate = new KaiZen.CSMS.Entities.UserCompany();

                ucUpdate.UserId = UserId;
                ucUpdate.CompanyId = CompanyId;
                ucUpdate.Active = Active;

                if (ucUpdate.EntityState != EntityState.Unchanged)
                {
                    DataRepository.UserCompanyProvider.Insert(transactionManager, ucUpdate);
                    transactionManager.Commit();
                }
            
        }
        catch (Exception ex)
        {


        }
    }

    public static void DeleteUsersCompaniesMap(int UserCompanyId)
    {
        try
        {
            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

            UserCompanyService ucser = new UserCompanyService();
            KaiZen.CSMS.Entities.UserCompany uc = ucser.GetByUserCompanyId(UserCompanyId);

            DataRepository.UserCompanyProvider.Delete(transactionManager, uc);
            transactionManager.Commit();
            
        }
        catch (Exception ex)
        { 
            
        }
    }

    
 

}