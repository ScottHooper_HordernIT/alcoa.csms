﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportOverviewSubQuery2ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportOverviewSubQuery2ProviderBaseCore : EntityViewProviderBase<QuestionnaireReportOverviewSubQuery2>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubQuery2&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportOverviewSubQuery2&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubQuery2&gt; Fill(DataSet dataSet, VList<QuestionnaireReportOverviewSubQuery2> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportOverviewSubQuery2>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubQuery2&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportOverviewSubQuery2>"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubQuery2&gt; Fill(DataTable dataTable, VList<QuestionnaireReportOverviewSubQuery2> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportOverviewSubQuery2 c = new QuestionnaireReportOverviewSubQuery2();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.KwiArpName = (Convert.IsDBNull(row["KWI_ARP_Name"]))?string.Empty:(System.String)row["KWI_ARP_Name"];
					c.KwiCrpName = (Convert.IsDBNull(row["KWI_CRP_Name"]))?string.Empty:(System.String)row["KWI_CRP_Name"];
					c.PinArpName = (Convert.IsDBNull(row["PIN_ARP_Name"]))?string.Empty:(System.String)row["PIN_ARP_Name"];
					c.PinCrpName = (Convert.IsDBNull(row["PIN_CRP_Name"]))?string.Empty:(System.String)row["PIN_CRP_Name"];
					c.WgpArpName = (Convert.IsDBNull(row["WGP_ARP_Name"]))?string.Empty:(System.String)row["WGP_ARP_Name"];
					c.WgpCrpName = (Convert.IsDBNull(row["WGP_CRP_Name"]))?string.Empty:(System.String)row["WGP_CRP_Name"];
					c.HunArpName = (Convert.IsDBNull(row["HUN_ARP_Name"]))?string.Empty:(System.String)row["HUN_ARP_Name"];
					c.HunCrpName = (Convert.IsDBNull(row["HUN_CRP_Name"]))?string.Empty:(System.String)row["HUN_CRP_Name"];
					c.WdlArpName = (Convert.IsDBNull(row["WDL_ARP_Name"]))?string.Empty:(System.String)row["WDL_ARP_Name"];
					c.WdlCrpName = (Convert.IsDBNull(row["WDL_CRP_Name"]))?string.Empty:(System.String)row["WDL_CRP_Name"];
					c.BunArpName = (Convert.IsDBNull(row["BUN_ARP_Name"]))?string.Empty:(System.String)row["BUN_ARP_Name"];
					c.BunCrpName = (Convert.IsDBNull(row["BUN_CRP_Name"]))?string.Empty:(System.String)row["BUN_CRP_Name"];
					c.FmlArpName = (Convert.IsDBNull(row["FML_ARP_Name"]))?string.Empty:(System.String)row["FML_ARP_Name"];
					c.FmlCrpName = (Convert.IsDBNull(row["FML_CRP_Name"]))?string.Empty:(System.String)row["FML_CRP_Name"];
					c.BgnArpName = (Convert.IsDBNull(row["BGN_ARP_Name"]))?string.Empty:(System.String)row["BGN_ARP_Name"];
					c.BgnCrpName = (Convert.IsDBNull(row["BGN_CRP_Name"]))?string.Empty:(System.String)row["BGN_CRP_Name"];
					c.CeArpName = (Convert.IsDBNull(row["CE_ARP_Name"]))?string.Empty:(System.String)row["CE_ARP_Name"];
					c.CeCrpName = (Convert.IsDBNull(row["CE_CRP_Name"]))?string.Empty:(System.String)row["CE_CRP_Name"];
					c.AngArpName = (Convert.IsDBNull(row["ANG_ARP_Name"]))?string.Empty:(System.String)row["ANG_ARP_Name"];
					c.AngCrpName = (Convert.IsDBNull(row["ANG_CRP_Name"]))?string.Empty:(System.String)row["ANG_CRP_Name"];
					c.PtlArpName = (Convert.IsDBNull(row["PTL_ARP_Name"]))?string.Empty:(System.String)row["PTL_ARP_Name"];
					c.PtlCrpName = (Convert.IsDBNull(row["PTL_CRP_Name"]))?string.Empty:(System.String)row["PTL_CRP_Name"];
					c.PthArpName = (Convert.IsDBNull(row["PTH_ARP_Name"]))?string.Empty:(System.String)row["PTH_ARP_Name"];
					c.PthCrpName = (Convert.IsDBNull(row["PTH_CRP_Name"]))?string.Empty:(System.String)row["PTH_CRP_Name"];
					c.PelArpName = (Convert.IsDBNull(row["PEL_ARP_Name"]))?string.Empty:(System.String)row["PEL_ARP_Name"];
					c.PelCrpName = (Convert.IsDBNull(row["PEL_CRP_Name"]))?string.Empty:(System.String)row["PEL_CRP_Name"];
					c.ArpArpName = (Convert.IsDBNull(row["ARP_ARP_Name"]))?string.Empty:(System.String)row["ARP_ARP_Name"];
					c.ArpCrpName = (Convert.IsDBNull(row["ARP_CRP_Name"]))?string.Empty:(System.String)row["ARP_CRP_Name"];
					c.YenArpName = (Convert.IsDBNull(row["YEN_ARP_Name"]))?string.Empty:(System.String)row["YEN_ARP_Name"];
					c.YenCrpName = (Convert.IsDBNull(row["YEN_CRP_Name"]))?string.Empty:(System.String)row["YEN_CRP_Name"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportOverviewSubQuery2&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportOverviewSubQuery2&gt;"/></returns>
		protected VList<QuestionnaireReportOverviewSubQuery2> Fill(IDataReader reader, VList<QuestionnaireReportOverviewSubQuery2> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportOverviewSubQuery2 entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportOverviewSubQuery2>("QuestionnaireReportOverviewSubQuery2",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportOverviewSubQuery2();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubQuery2Column.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.KwiArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.KwiArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.KwiArpName)];
					//entity.KwiArpName = (Convert.IsDBNull(reader["KWI_ARP_Name"]))?string.Empty:(System.String)reader["KWI_ARP_Name"];
					entity.KwiCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.KwiCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.KwiCrpName)];
					//entity.KwiCrpName = (Convert.IsDBNull(reader["KWI_CRP_Name"]))?string.Empty:(System.String)reader["KWI_CRP_Name"];
					entity.PinArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PinArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PinArpName)];
					//entity.PinArpName = (Convert.IsDBNull(reader["PIN_ARP_Name"]))?string.Empty:(System.String)reader["PIN_ARP_Name"];
					entity.PinCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PinCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PinCrpName)];
					//entity.PinCrpName = (Convert.IsDBNull(reader["PIN_CRP_Name"]))?string.Empty:(System.String)reader["PIN_CRP_Name"];
					entity.WgpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WgpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WgpArpName)];
					//entity.WgpArpName = (Convert.IsDBNull(reader["WGP_ARP_Name"]))?string.Empty:(System.String)reader["WGP_ARP_Name"];
					entity.WgpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WgpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WgpCrpName)];
					//entity.WgpCrpName = (Convert.IsDBNull(reader["WGP_CRP_Name"]))?string.Empty:(System.String)reader["WGP_CRP_Name"];
					entity.HunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.HunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.HunArpName)];
					//entity.HunArpName = (Convert.IsDBNull(reader["HUN_ARP_Name"]))?string.Empty:(System.String)reader["HUN_ARP_Name"];
					entity.HunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.HunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.HunCrpName)];
					//entity.HunCrpName = (Convert.IsDBNull(reader["HUN_CRP_Name"]))?string.Empty:(System.String)reader["HUN_CRP_Name"];
					entity.WdlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WdlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WdlArpName)];
					//entity.WdlArpName = (Convert.IsDBNull(reader["WDL_ARP_Name"]))?string.Empty:(System.String)reader["WDL_ARP_Name"];
					entity.WdlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WdlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WdlCrpName)];
					//entity.WdlCrpName = (Convert.IsDBNull(reader["WDL_CRP_Name"]))?string.Empty:(System.String)reader["WDL_CRP_Name"];
					entity.BunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BunArpName)];
					//entity.BunArpName = (Convert.IsDBNull(reader["BUN_ARP_Name"]))?string.Empty:(System.String)reader["BUN_ARP_Name"];
					entity.BunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BunCrpName)];
					//entity.BunCrpName = (Convert.IsDBNull(reader["BUN_CRP_Name"]))?string.Empty:(System.String)reader["BUN_CRP_Name"];
					entity.FmlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.FmlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.FmlArpName)];
					//entity.FmlArpName = (Convert.IsDBNull(reader["FML_ARP_Name"]))?string.Empty:(System.String)reader["FML_ARP_Name"];
					entity.FmlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.FmlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.FmlCrpName)];
					//entity.FmlCrpName = (Convert.IsDBNull(reader["FML_CRP_Name"]))?string.Empty:(System.String)reader["FML_CRP_Name"];
					entity.BgnArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BgnArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BgnArpName)];
					//entity.BgnArpName = (Convert.IsDBNull(reader["BGN_ARP_Name"]))?string.Empty:(System.String)reader["BGN_ARP_Name"];
					entity.BgnCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BgnCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BgnCrpName)];
					//entity.BgnCrpName = (Convert.IsDBNull(reader["BGN_CRP_Name"]))?string.Empty:(System.String)reader["BGN_CRP_Name"];
					entity.CeArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.CeArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.CeArpName)];
					//entity.CeArpName = (Convert.IsDBNull(reader["CE_ARP_Name"]))?string.Empty:(System.String)reader["CE_ARP_Name"];
					entity.CeCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.CeCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.CeCrpName)];
					//entity.CeCrpName = (Convert.IsDBNull(reader["CE_CRP_Name"]))?string.Empty:(System.String)reader["CE_CRP_Name"];
					entity.AngArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.AngArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.AngArpName)];
					//entity.AngArpName = (Convert.IsDBNull(reader["ANG_ARP_Name"]))?string.Empty:(System.String)reader["ANG_ARP_Name"];
					entity.AngCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.AngCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.AngCrpName)];
					//entity.AngCrpName = (Convert.IsDBNull(reader["ANG_CRP_Name"]))?string.Empty:(System.String)reader["ANG_CRP_Name"];
					entity.PtlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PtlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PtlArpName)];
					//entity.PtlArpName = (Convert.IsDBNull(reader["PTL_ARP_Name"]))?string.Empty:(System.String)reader["PTL_ARP_Name"];
					entity.PtlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PtlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PtlCrpName)];
					//entity.PtlCrpName = (Convert.IsDBNull(reader["PTL_CRP_Name"]))?string.Empty:(System.String)reader["PTL_CRP_Name"];
					entity.PthArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PthArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PthArpName)];
					//entity.PthArpName = (Convert.IsDBNull(reader["PTH_ARP_Name"]))?string.Empty:(System.String)reader["PTH_ARP_Name"];
					entity.PthCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PthCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PthCrpName)];
					//entity.PthCrpName = (Convert.IsDBNull(reader["PTH_CRP_Name"]))?string.Empty:(System.String)reader["PTH_CRP_Name"];
					entity.PelArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PelArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PelArpName)];
					//entity.PelArpName = (Convert.IsDBNull(reader["PEL_ARP_Name"]))?string.Empty:(System.String)reader["PEL_ARP_Name"];
					entity.PelCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PelCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PelCrpName)];
					//entity.PelCrpName = (Convert.IsDBNull(reader["PEL_CRP_Name"]))?string.Empty:(System.String)reader["PEL_CRP_Name"];
					entity.ArpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.ArpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.ArpArpName)];
					//entity.ArpArpName = (Convert.IsDBNull(reader["ARP_ARP_Name"]))?string.Empty:(System.String)reader["ARP_ARP_Name"];
					entity.ArpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.ArpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.ArpCrpName)];
					//entity.ArpCrpName = (Convert.IsDBNull(reader["ARP_CRP_Name"]))?string.Empty:(System.String)reader["ARP_CRP_Name"];
					entity.YenArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.YenArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.YenArpName)];
					//entity.YenArpName = (Convert.IsDBNull(reader["YEN_ARP_Name"]))?string.Empty:(System.String)reader["YEN_ARP_Name"];
					entity.YenCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.YenCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.YenCrpName)];
					//entity.YenCrpName = (Convert.IsDBNull(reader["YEN_CRP_Name"]))?string.Empty:(System.String)reader["YEN_CRP_Name"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubQuery2"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubQuery2"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportOverviewSubQuery2 entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubQuery2Column.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.KwiArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.KwiArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.KwiArpName)];
			//entity.KwiArpName = (Convert.IsDBNull(reader["KWI_ARP_Name"]))?string.Empty:(System.String)reader["KWI_ARP_Name"];
			entity.KwiCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.KwiCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.KwiCrpName)];
			//entity.KwiCrpName = (Convert.IsDBNull(reader["KWI_CRP_Name"]))?string.Empty:(System.String)reader["KWI_CRP_Name"];
			entity.PinArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PinArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PinArpName)];
			//entity.PinArpName = (Convert.IsDBNull(reader["PIN_ARP_Name"]))?string.Empty:(System.String)reader["PIN_ARP_Name"];
			entity.PinCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PinCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PinCrpName)];
			//entity.PinCrpName = (Convert.IsDBNull(reader["PIN_CRP_Name"]))?string.Empty:(System.String)reader["PIN_CRP_Name"];
			entity.WgpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WgpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WgpArpName)];
			//entity.WgpArpName = (Convert.IsDBNull(reader["WGP_ARP_Name"]))?string.Empty:(System.String)reader["WGP_ARP_Name"];
			entity.WgpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WgpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WgpCrpName)];
			//entity.WgpCrpName = (Convert.IsDBNull(reader["WGP_CRP_Name"]))?string.Empty:(System.String)reader["WGP_CRP_Name"];
			entity.HunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.HunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.HunArpName)];
			//entity.HunArpName = (Convert.IsDBNull(reader["HUN_ARP_Name"]))?string.Empty:(System.String)reader["HUN_ARP_Name"];
			entity.HunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.HunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.HunCrpName)];
			//entity.HunCrpName = (Convert.IsDBNull(reader["HUN_CRP_Name"]))?string.Empty:(System.String)reader["HUN_CRP_Name"];
			entity.WdlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WdlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WdlArpName)];
			//entity.WdlArpName = (Convert.IsDBNull(reader["WDL_ARP_Name"]))?string.Empty:(System.String)reader["WDL_ARP_Name"];
			entity.WdlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.WdlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.WdlCrpName)];
			//entity.WdlCrpName = (Convert.IsDBNull(reader["WDL_CRP_Name"]))?string.Empty:(System.String)reader["WDL_CRP_Name"];
			entity.BunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BunArpName)];
			//entity.BunArpName = (Convert.IsDBNull(reader["BUN_ARP_Name"]))?string.Empty:(System.String)reader["BUN_ARP_Name"];
			entity.BunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BunCrpName)];
			//entity.BunCrpName = (Convert.IsDBNull(reader["BUN_CRP_Name"]))?string.Empty:(System.String)reader["BUN_CRP_Name"];
			entity.FmlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.FmlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.FmlArpName)];
			//entity.FmlArpName = (Convert.IsDBNull(reader["FML_ARP_Name"]))?string.Empty:(System.String)reader["FML_ARP_Name"];
			entity.FmlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.FmlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.FmlCrpName)];
			//entity.FmlCrpName = (Convert.IsDBNull(reader["FML_CRP_Name"]))?string.Empty:(System.String)reader["FML_CRP_Name"];
			entity.BgnArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BgnArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BgnArpName)];
			//entity.BgnArpName = (Convert.IsDBNull(reader["BGN_ARP_Name"]))?string.Empty:(System.String)reader["BGN_ARP_Name"];
			entity.BgnCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.BgnCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.BgnCrpName)];
			//entity.BgnCrpName = (Convert.IsDBNull(reader["BGN_CRP_Name"]))?string.Empty:(System.String)reader["BGN_CRP_Name"];
			entity.CeArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.CeArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.CeArpName)];
			//entity.CeArpName = (Convert.IsDBNull(reader["CE_ARP_Name"]))?string.Empty:(System.String)reader["CE_ARP_Name"];
			entity.CeCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.CeCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.CeCrpName)];
			//entity.CeCrpName = (Convert.IsDBNull(reader["CE_CRP_Name"]))?string.Empty:(System.String)reader["CE_CRP_Name"];
			entity.AngArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.AngArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.AngArpName)];
			//entity.AngArpName = (Convert.IsDBNull(reader["ANG_ARP_Name"]))?string.Empty:(System.String)reader["ANG_ARP_Name"];
			entity.AngCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.AngCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.AngCrpName)];
			//entity.AngCrpName = (Convert.IsDBNull(reader["ANG_CRP_Name"]))?string.Empty:(System.String)reader["ANG_CRP_Name"];
			entity.PtlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PtlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PtlArpName)];
			//entity.PtlArpName = (Convert.IsDBNull(reader["PTL_ARP_Name"]))?string.Empty:(System.String)reader["PTL_ARP_Name"];
			entity.PtlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PtlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PtlCrpName)];
			//entity.PtlCrpName = (Convert.IsDBNull(reader["PTL_CRP_Name"]))?string.Empty:(System.String)reader["PTL_CRP_Name"];
			entity.PthArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PthArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PthArpName)];
			//entity.PthArpName = (Convert.IsDBNull(reader["PTH_ARP_Name"]))?string.Empty:(System.String)reader["PTH_ARP_Name"];
			entity.PthCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PthCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PthCrpName)];
			//entity.PthCrpName = (Convert.IsDBNull(reader["PTH_CRP_Name"]))?string.Empty:(System.String)reader["PTH_CRP_Name"];
			entity.PelArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PelArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PelArpName)];
			//entity.PelArpName = (Convert.IsDBNull(reader["PEL_ARP_Name"]))?string.Empty:(System.String)reader["PEL_ARP_Name"];
			entity.PelCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.PelCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.PelCrpName)];
			//entity.PelCrpName = (Convert.IsDBNull(reader["PEL_CRP_Name"]))?string.Empty:(System.String)reader["PEL_CRP_Name"];
			entity.ArpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.ArpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.ArpArpName)];
			//entity.ArpArpName = (Convert.IsDBNull(reader["ARP_ARP_Name"]))?string.Empty:(System.String)reader["ARP_ARP_Name"];
			entity.ArpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.ArpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.ArpCrpName)];
			//entity.ArpCrpName = (Convert.IsDBNull(reader["ARP_CRP_Name"]))?string.Empty:(System.String)reader["ARP_CRP_Name"];
			entity.YenArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.YenArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.YenArpName)];
			//entity.YenArpName = (Convert.IsDBNull(reader["YEN_ARP_Name"]))?string.Empty:(System.String)reader["YEN_ARP_Name"];
			entity.YenCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubQuery2Column.YenCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewSubQuery2Column.YenCrpName)];
			//entity.YenCrpName = (Convert.IsDBNull(reader["YEN_CRP_Name"]))?string.Empty:(System.String)reader["YEN_CRP_Name"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubQuery2"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubQuery2"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportOverviewSubQuery2 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.KwiArpName = (Convert.IsDBNull(dataRow["KWI_ARP_Name"]))?string.Empty:(System.String)dataRow["KWI_ARP_Name"];
			entity.KwiCrpName = (Convert.IsDBNull(dataRow["KWI_CRP_Name"]))?string.Empty:(System.String)dataRow["KWI_CRP_Name"];
			entity.PinArpName = (Convert.IsDBNull(dataRow["PIN_ARP_Name"]))?string.Empty:(System.String)dataRow["PIN_ARP_Name"];
			entity.PinCrpName = (Convert.IsDBNull(dataRow["PIN_CRP_Name"]))?string.Empty:(System.String)dataRow["PIN_CRP_Name"];
			entity.WgpArpName = (Convert.IsDBNull(dataRow["WGP_ARP_Name"]))?string.Empty:(System.String)dataRow["WGP_ARP_Name"];
			entity.WgpCrpName = (Convert.IsDBNull(dataRow["WGP_CRP_Name"]))?string.Empty:(System.String)dataRow["WGP_CRP_Name"];
			entity.HunArpName = (Convert.IsDBNull(dataRow["HUN_ARP_Name"]))?string.Empty:(System.String)dataRow["HUN_ARP_Name"];
			entity.HunCrpName = (Convert.IsDBNull(dataRow["HUN_CRP_Name"]))?string.Empty:(System.String)dataRow["HUN_CRP_Name"];
			entity.WdlArpName = (Convert.IsDBNull(dataRow["WDL_ARP_Name"]))?string.Empty:(System.String)dataRow["WDL_ARP_Name"];
			entity.WdlCrpName = (Convert.IsDBNull(dataRow["WDL_CRP_Name"]))?string.Empty:(System.String)dataRow["WDL_CRP_Name"];
			entity.BunArpName = (Convert.IsDBNull(dataRow["BUN_ARP_Name"]))?string.Empty:(System.String)dataRow["BUN_ARP_Name"];
			entity.BunCrpName = (Convert.IsDBNull(dataRow["BUN_CRP_Name"]))?string.Empty:(System.String)dataRow["BUN_CRP_Name"];
			entity.FmlArpName = (Convert.IsDBNull(dataRow["FML_ARP_Name"]))?string.Empty:(System.String)dataRow["FML_ARP_Name"];
			entity.FmlCrpName = (Convert.IsDBNull(dataRow["FML_CRP_Name"]))?string.Empty:(System.String)dataRow["FML_CRP_Name"];
			entity.BgnArpName = (Convert.IsDBNull(dataRow["BGN_ARP_Name"]))?string.Empty:(System.String)dataRow["BGN_ARP_Name"];
			entity.BgnCrpName = (Convert.IsDBNull(dataRow["BGN_CRP_Name"]))?string.Empty:(System.String)dataRow["BGN_CRP_Name"];
			entity.CeArpName = (Convert.IsDBNull(dataRow["CE_ARP_Name"]))?string.Empty:(System.String)dataRow["CE_ARP_Name"];
			entity.CeCrpName = (Convert.IsDBNull(dataRow["CE_CRP_Name"]))?string.Empty:(System.String)dataRow["CE_CRP_Name"];
			entity.AngArpName = (Convert.IsDBNull(dataRow["ANG_ARP_Name"]))?string.Empty:(System.String)dataRow["ANG_ARP_Name"];
			entity.AngCrpName = (Convert.IsDBNull(dataRow["ANG_CRP_Name"]))?string.Empty:(System.String)dataRow["ANG_CRP_Name"];
			entity.PtlArpName = (Convert.IsDBNull(dataRow["PTL_ARP_Name"]))?string.Empty:(System.String)dataRow["PTL_ARP_Name"];
			entity.PtlCrpName = (Convert.IsDBNull(dataRow["PTL_CRP_Name"]))?string.Empty:(System.String)dataRow["PTL_CRP_Name"];
			entity.PthArpName = (Convert.IsDBNull(dataRow["PTH_ARP_Name"]))?string.Empty:(System.String)dataRow["PTH_ARP_Name"];
			entity.PthCrpName = (Convert.IsDBNull(dataRow["PTH_CRP_Name"]))?string.Empty:(System.String)dataRow["PTH_CRP_Name"];
			entity.PelArpName = (Convert.IsDBNull(dataRow["PEL_ARP_Name"]))?string.Empty:(System.String)dataRow["PEL_ARP_Name"];
			entity.PelCrpName = (Convert.IsDBNull(dataRow["PEL_CRP_Name"]))?string.Empty:(System.String)dataRow["PEL_CRP_Name"];
			entity.ArpArpName = (Convert.IsDBNull(dataRow["ARP_ARP_Name"]))?string.Empty:(System.String)dataRow["ARP_ARP_Name"];
			entity.ArpCrpName = (Convert.IsDBNull(dataRow["ARP_CRP_Name"]))?string.Empty:(System.String)dataRow["ARP_CRP_Name"];
			entity.YenArpName = (Convert.IsDBNull(dataRow["YEN_ARP_Name"]))?string.Empty:(System.String)dataRow["YEN_ARP_Name"];
			entity.YenCrpName = (Convert.IsDBNull(dataRow["YEN_CRP_Name"]))?string.Empty:(System.String)dataRow["YEN_CRP_Name"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportOverviewSubQuery2FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery2FilterBuilder : SqlFilterBuilder<QuestionnaireReportOverviewSubQuery2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2FilterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery2FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery2FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery2FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery2FilterBuilder

	#region QuestionnaireReportOverviewSubQuery2ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery2ParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportOverviewSubQuery2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2ParameterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery2ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery2ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery2ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery2ParameterBuilder
	
	#region QuestionnaireReportOverviewSubQuery2SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportOverviewSubQuery2SortBuilder : SqlSortBuilder<QuestionnaireReportOverviewSubQuery2Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2SqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery2SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportOverviewSubQuery2SortBuilder

} // end namespace
