﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IJobScheduleService
    {
        JobSchedule Get(Expression<Func<JobSchedule, bool>> where, List<Expression<Func<JobSchedule, object>>> includes);
        List<JobSchedule> GetMany(int? top, Expression<Func<JobSchedule, bool>> where, Func<IQueryable<JobSchedule>, IOrderedQueryable<JobSchedule>> orderBy, List<Expression<Func<JobSchedule, object>>> includes);
        List<JobSchedule> GetManyPaged(int pageNumber, int pageSize, Expression<Func<JobSchedule, bool>> where, Func<IQueryable<JobSchedule>, IOrderedQueryable<JobSchedule>> orderBy, List<Expression<Func<JobSchedule, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<JobSchedule, bool>> where, List<Expression<Func<JobSchedule, object>>> includes);
        void Insert(JobSchedule item, bool saveChanges = true);
        void Update(JobSchedule item, bool saveChanges = true);
        void Delete(JobSchedule item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class JobScheduleService : IJobScheduleService
    {
        private readonly IRepository<JobSchedule> _repository;

        public JobScheduleService(IRepository<JobSchedule> repository)
        {
            this._repository = repository;
        }

        public JobSchedule Get(Expression<Func<JobSchedule, bool>> where, List<Expression<Func<JobSchedule, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<JobSchedule> GetMany(int? top, Expression<Func<JobSchedule, bool>> where, Func<IQueryable<JobSchedule>, IOrderedQueryable<JobSchedule>> orderBy, List<Expression<Func<JobSchedule, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<JobSchedule> GetManyPaged(int pageNumber, int pageSize, Expression<Func<JobSchedule, bool>> where, Func<IQueryable<JobSchedule>, IOrderedQueryable<JobSchedule>> orderBy, List<Expression<Func<JobSchedule, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<JobSchedule, bool>> where, List<Expression<Func<JobSchedule, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(JobSchedule item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("JobSchedule");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(JobSchedule item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("JobSchedule");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(JobSchedule item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("JobSchedule");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
