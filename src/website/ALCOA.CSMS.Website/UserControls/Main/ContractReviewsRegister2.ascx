﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractReviewsRegister2.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContractReviewsRegister2" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>




    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">Contractor Management Team Shop Front Tools</span><br />
                <span class="date">Contract Reviews</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1" /><br />
            </span>
            </td>
            
    </tr>

    <tr>
       <td style="height: 28px;  width: 1170px;">
          <table style="width: 522px">
            <tr>
                <td style="width: 38px; text-align: right">
                    <strong>Year:&nbsp;</strong>
                </td>
                <td style="width: 22px; text-align: left">
                    <dxe:ASPxComboBox id="cmbYear" runat="server"
                     clientinstancename="cmbYear"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                     csspostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"                     
                     valuetype="System.String" width="60px" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic">
                        </ValidationSettings>
                    </dxe:ASPxComboBox>
                </td>
                <td style="width: 485px; text-align: left">
                    <dxe:ASPxButton ID="btnRefresh" runat="server" Text="Go/Refresh" ForeColor="Black" onclick="btnRefresh_Click" 
                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            csspostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ></dxe:ASPxButton>
        </td>
        </tr>
        
        </table>
        </td>
        </tr>
        <tr>
            <td style="width:1170px;">
               <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                DataSourceID="ods" OnHtmlRowCreated="grid_RowCreated"
                OnRowUpdating="grid_RowUpdating" OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter"
                  DataSourceForceStandardPaging="True" OnFilterControlCustomValueDisplayText="grid_FilterControlCustomValueDisplayText"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" OnCustomFilterExpressionDisplayText="grid_CustomFilterExpressionDisplayText"
                CssPostfix="Office2003Blue" KeyFieldName="ContractId"
                     onafterperformcallback="grid_AfterPerformCallback">
                <SettingsEditing Mode="EditFormAndDisplayRow" />
                 <Settings ShowFilterRow="true" ShowGroupedColumns="true" ShowFilterBar="Visible" />
                      <SettingsPager PageSize="10">
                          <AllButton Visible="True"></AllButton>
                      </SettingsPager>
                 <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ExpandedButton Height="12px" Width="11px" />
                    <CollapsedButton Height="12px" Width="11px" />
                    <DetailCollapsedButton Height="12px" Width="11px" />
                    <DetailExpandedButton Height="12px" Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px" >
                    </LoadingPanel>
                </Styles>
                <GroupSummary>
                <dxwgv:ASPxSummaryItem SummaryType="Count" />
                </GroupSummary>



                <Columns>
                    

                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Caption="Action">
                        <EditButton Visible="true"></EditButton>
                         <ClearFilterButton Visible="true" ></ClearFilterButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataTextColumn Caption="ContractId" FieldName="ContractId" Visible="false">
                    <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    
                     <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="1" Caption="Company Name" FieldName="CompanyId" Name="CompanyBox" SortIndex="2" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="SqlDataSourceCompanies" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True"/>
                        <Settings SortMode="DisplayText"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Site Name" FieldName="SiteId" Name="SiteBox">
                        <PropertiesComboBox DataSourceID="SitesDataSource2" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dx:GridViewBandColumn Caption="Month" VisibleIndex="3">
                    <HeaderStyle HorizontalAlign="Center" />
                    <Columns>
                    <dxwgv:GridViewDataCheckColumn FieldName="January" ReadOnly="false" Caption="Jan" VisibleIndex="3">
                        <DataItemTemplate>
                     <asp:Image ID="imgJan" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                    
                        </DataItemTemplate>                      
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />                        
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="February" Caption="Feb" VisibleIndex="4">
                   
                        <DataItemTemplate>
                     <asp:Image ID="imgFeb" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="March" Caption="Mar" VisibleIndex="5" >
                        <DataItemTemplate>
                     <asp:Image ID="imgMar" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                    
                        </DataItemTemplate>

                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="April" Caption="Apr" VisibleIndex="6">
                        <DataItemTemplate>
                     <asp:Image ID="imgApr" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>

                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="May" Caption="May" VisibleIndex="7">
                        <DataItemTemplate>
                     <asp:Image ID="imgMay" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                       

                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="June" Caption="Jun" VisibleIndex="8">
                        <DataItemTemplate>
                     <asp:Image ID="imgJun" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>
                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="July" Caption="Jul" VisibleIndex="9">
                        <DataItemTemplate>
                     <asp:Image ID="imgJul" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="August" Caption="Aug" VisibleIndex="10">
                        <DataItemTemplate>
                     <asp:Image ID="imgAug" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="September" Caption="Sep" VisibleIndex="11">
                        <DataItemTemplate>
                     <asp:Image ID="imgSep" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                       
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="October" Caption="Oct" VisibleIndex="12">
                        <DataItemTemplate>
                     <asp:Image ID="imgOct" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                       
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="November" Caption="Nov" VisibleIndex="13">
                  
                        <DataItemTemplate>
                     <asp:Image ID="imgNov" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="December" Caption="Dec" VisibleIndex="14">
                        <DataItemTemplate>
                     <asp:Image ID="imgDec" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <%-- <dx:GridViewDataComboBoxColumn  FieldName="YearCount" Name="YearCount" Caption="Year" VisibleIndex="15" UnboundType="Integer">
                    
                     <PropertiesComboBox DropDownButton-Visible="false" DropDownHeight="150px"
                             ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                            <Items>
                               
                                <dx:ListEditItem Text="0" Value="0" />
                                <dx:ListEditItem Text="1" Value="1" />
                                <dx:ListEditItem Text="2" Value="2" />
                                <dx:ListEditItem Text="3" Value="3" />
                                <dx:ListEditItem Text="4" Value="4" />
                                <dx:ListEditItem Text="5" Value="5" />
                                <dx:ListEditItem Text="6" Value="6" />
                                <dx:ListEditItem Text="7" Value="7" />
                                <dx:ListEditItem Text="8" Value="8" />
                                <dx:ListEditItem Text="9" Value="9" />
                                <dx:ListEditItem Text="10" Value="10" />
                                <dx:ListEditItem Text="11" Value="11" />
                                <dx:ListEditItem Text="12" Value="12" />
                            </Items>
                        </PropertiesComboBox>
                        <EditFormSettings Visible="false" />
                        
                    </dx:GridViewDataComboBoxColumn>--%>
                    <dxwgv:GridViewDataTextColumn FieldName="YearCount" VisibleIndex="15" Caption="Year">
                    <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>


                    </Columns>
                    </dx:GridViewBandColumn>                                       

                    <dxwgv:GridViewDataComboBoxColumn Caption="Sponsor" FieldName="LocationSponsorUserId" 
                        VisibleIndex="5">
                        <PropertiesComboBox DropDownButton-Visible="false" DataSourceID="SqlDataSourceSponsor" DropDownHeight="150px" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="false" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="SPA(s)" FieldName="LocationSpaUserId" 
                        VisibleIndex="6">
                        <PropertiesComboBox DropDownButton-Visible="false" DataSourceID="SqlDataSourceSPA" DropDownHeight="150px" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="false" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>    

                </Columns>
            </dxwgv:ASPxGridView> 
            <div width="100%" align="right" style="padding-top:5px">
            <dxe:ASPxButton ID="btnShowAll" runat="server" Text="Show All" ForeColor="Black"  
                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" OnClick="btnShowAll_Click"
            csspostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ></dxe:ASPxButton>
            </div>
           
            </td>
        </tr>
        </table>
        <br />
      

<asp:ObjectDataSource ID="ods" runat="server" SortParameterName="sortColumns" EnablePaging="true"
                StartRowIndexParameterName="startRecord" MaximumRowsParameterName="maxRecords"
                SelectCountMethod="GetPeopleCount" SelectMethod="GetPeople" UpdateMethod="UpdatePeople" TypeName="People"></asp:ObjectDataSource>

<asp:SqlDataSource ID="SqlDataSourceCompanies" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select CompanyId, CompanyName from companies where companyid in  
(select companyid from CompanySiteCategoryStandard) order by CompanyName">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SitesDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select SiteId,SiteName from sites where siteid not in (21,23,26) order by SiteName">
</asp:SqlDataSource>


<asp:SqlDataSource ID="SqlDataSourceSponsor" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct(Lastname+', '+FirstName) as UserFullName,UserId from  users
 where UserId in (select LocationSponsorUserId from CompanySiteCategoryStandard)
 order by UserFullName">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataSourceSPA" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct(Lastname+', '+FirstName) as UserFullName,UserId from  users
 where UserId in (select LocationSpaUserId from CompanySiteCategoryStandard)
 order by UserFullName">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataSourceDistinctCompanyID" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct companyId from companySiteCategoryStandard order by CompanyId" SelectCommandType="Text">
</asp:SqlDataSource>

