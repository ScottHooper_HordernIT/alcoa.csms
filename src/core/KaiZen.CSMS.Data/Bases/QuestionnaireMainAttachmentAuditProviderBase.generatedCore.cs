﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainAttachmentAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireMainAttachmentAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByQuestionnaireIdAnswerId(System.Int32? _questionnaireId, System.Int32? _answerId)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(null,_questionnaireId, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByQuestionnaireIdAnswerId(System.Int32? _questionnaireId, System.Int32? _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(null, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32? _questionnaireId, System.Int32? _answerId)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(transactionManager, _questionnaireId, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32? _questionnaireId, System.Int32? _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(transactionManager, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByQuestionnaireIdAnswerId(System.Int32? _questionnaireId, System.Int32? _answerId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdAnswerId(null, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public abstract TList<QuestionnaireMainAttachmentAudit> GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32? _questionnaireId, System.Int32? _answerId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireMainAttachmentAudit_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByModifiedByUserId(System.Int32? _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(null,_modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByModifiedByUserId(System.Int32? _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit_1 index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachmentAudit> GetByModifiedByUserId(System.Int32? _modifiedByUserId, int start, int pageLength, out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachmentAudit_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/> class.</returns>
		public abstract TList<QuestionnaireMainAttachmentAudit> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireMainAttachmentAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireMainAttachmentAudit&gt;"/></returns>
		public static TList<QuestionnaireMainAttachmentAudit> Fill(IDataReader reader, TList<QuestionnaireMainAttachmentAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireMainAttachmentAudit")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireMainAttachmentAudit>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireMainAttachmentAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditEventId - 1)];
					c.AttachmentId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.AttachmentId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.AttachmentId - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.QuestionnaireId - 1)];
					c.AnswerId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.AnswerId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.AnswerId - 1)];
					c.FileName = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.FileName - 1)))?null:(System.String)reader[((int)QuestionnaireMainAttachmentAuditColumn.FileName - 1)];
					c.FileHash = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.FileHash - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainAttachmentAuditColumn.FileHash - 1)];
					c.ContentLength = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.ContentLength - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.ContentLength - 1)];
					c.Content = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.Content - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainAttachmentAuditColumn.Content - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireMainAttachmentAuditColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)QuestionnaireMainAttachmentAuditColumn.AuditEventId - 1)];
			entity.AttachmentId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.AttachmentId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.AttachmentId - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.QuestionnaireId - 1)];
			entity.AnswerId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.AnswerId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.AnswerId - 1)];
			entity.FileName = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.FileName - 1)))?null:(System.String)reader[((int)QuestionnaireMainAttachmentAuditColumn.FileName - 1)];
			entity.FileHash = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.FileHash - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainAttachmentAuditColumn.FileHash - 1)];
			entity.ContentLength = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.ContentLength - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.ContentLength - 1)];
			entity.Content = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.Content - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainAttachmentAuditColumn.Content - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireMainAttachmentAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireMainAttachmentAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireMainAttachmentAuditColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.AttachmentId = Convert.IsDBNull(dataRow["AttachmentId"]) ? null : (System.Int32?)dataRow["AttachmentId"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.Int32?)dataRow["QuestionnaireId"];
			entity.AnswerId = Convert.IsDBNull(dataRow["AnswerId"]) ? null : (System.Int32?)dataRow["AnswerId"];
			entity.FileName = Convert.IsDBNull(dataRow["FileName"]) ? null : (System.String)dataRow["FileName"];
			entity.FileHash = Convert.IsDBNull(dataRow["FileHash"]) ? null : (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = Convert.IsDBNull(dataRow["ContentLength"]) ? null : (System.Int32?)dataRow["ContentLength"];
			entity.Content = Convert.IsDBNull(dataRow["Content"]) ? null : (System.Byte[])dataRow["Content"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireMainAttachmentAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireMainAttachmentAudit</c>
	///</summary>
	public enum QuestionnaireMainAttachmentAuditChildEntityTypes
	{
	}
	
	#endregion QuestionnaireMainAttachmentAuditChildEntityTypes
	
	#region QuestionnaireMainAttachmentAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainAttachmentAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentAuditFilterBuilder : SqlFilterBuilder<QuestionnaireMainAttachmentAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditFilterBuilder class.
		/// </summary>
		public QuestionnaireMainAttachmentAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentAuditFilterBuilder
	
	#region QuestionnaireMainAttachmentAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainAttachmentAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentAuditParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainAttachmentAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditParameterBuilder class.
		/// </summary>
		public QuestionnaireMainAttachmentAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentAuditParameterBuilder
	
	#region QuestionnaireMainAttachmentAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainAttachmentAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachmentAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainAttachmentAuditSortBuilder : SqlSortBuilder<QuestionnaireMainAttachmentAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainAttachmentAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainAttachmentAuditSortBuilder
	
} // end namespace
