﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_CompaniesVendors" Codebehind="companiesVendors.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>











<br />
<div>Company Vendor List</div>
<br />
<div style="width: 100%; text-align: right; padding-bottom: 2px">
<a href="javascript:ShowHideCustomizationWindow2();"><span style="color: #0000ff; text-decoration: none">
                Customize</span></a>
</div>
<dxwgv:ASPxGridView
    ID="grid_Companies_Vendors" ClientInstanceName="grid2"
    runat="server" 
    AutoGenerateColumns="False"
    Width="100%"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" 
    DataSourceID="CompanyVendorDataSource" 
    KeyFieldName="VENDOR_ID">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewDataTextColumn FieldName="VENDOR_ID" ReadOnly="True"
            Visible="true" VisibleIndex="1" Caption="Vendor ID">
            <CellStyle HorizontalAlign="Left"></CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Vendor Number" FieldName="VENDOR_NUMBER" 
             SortOrder="Ascending" VisibleIndex="2">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Vendor Name" FieldName="VENDOR_NAME" 
            VisibleIndex="3">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Vendor Name Alt" FieldName="VENDOR_NAME_ALT" 
            VisibleIndex="4" Visible="false" ShowInCustomizationForm="true">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn Caption="Last Update Date" FieldName="LAST_UPDATE_DATE" VisibleIndex="5"
             PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy hh:ss:mm">
        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:ss:mm"></PropertiesDateEdit>
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataDateColumn Caption="Start Date Active" FieldName="START_DATE_ACTIVE" VisibleIndex="6" Visible="false"
             PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy" ShowInCustomizationForm="true">
        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
        <CellStyle HorizontalAlign="Left"></CellStyle>
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataDateColumn Caption="End Date Active" FieldName="END_DATE_ACTIVE" VisibleIndex="7" Visible="false"
             PropertiesDateEdit-DisplayFormatString="dd/MM/yyyy" ShowInCustomizationForm="true">
        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit>
        <CellStyle HorizontalAlign="Left"></CellStyle>
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataTextColumn FieldName="TAX_REGISTRATION_NUMBER" Visible="true" VisibleIndex="8" Caption="ABN">
        </dxwgv:GridViewDataTextColumn>
     </Columns>
    <Settings ShowFilterBar="Visible" ShowFilterRow="true" />
    <SettingsCustomizationWindow Enabled="True" PopupVerticalAlign="Above"></SettingsCustomizationWindow>
    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn"></SettingsBehavior>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
</dxwgv:ASPxGridView>
<div align="right" style="padding-top: 2px;">
    <uc1:ExportButtons ID="companyVendorExportButtons" runat="server" />
</div>

<asp:SqlDataSource ID="CompanyVendorDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="CompanyVendor_Get_List" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
