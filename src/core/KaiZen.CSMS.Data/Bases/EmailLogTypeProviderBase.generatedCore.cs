﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EmailLogTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EmailLogTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EmailLogType, KaiZen.CSMS.Entities.EmailLogTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLogTypeKey key)
		{
			return Delete(transactionManager, key.EmailLogTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_emailLogTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _emailLogTypeId)
		{
			return Delete(null, _emailLogTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _emailLogTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EmailLogType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLogTypeKey key, int start, int pageLength)
		{
			return GetByEmailLogTypeId(transactionManager, key.EmailLogTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EmailLogType index.
		/// </summary>
		/// <param name="_emailLogTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeId(System.Int32 _emailLogTypeId)
		{
			int count = -1;
			return GetByEmailLogTypeId(null,_emailLogTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLogType index.
		/// </summary>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeId(System.Int32 _emailLogTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogTypeId(null, _emailLogTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLogType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeId(TransactionManager transactionManager, System.Int32 _emailLogTypeId)
		{
			int count = -1;
			return GetByEmailLogTypeId(transactionManager, _emailLogTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLogType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeId(TransactionManager transactionManager, System.Int32 _emailLogTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogTypeId(transactionManager, _emailLogTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLogType index.
		/// </summary>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeId(System.Int32 _emailLogTypeId, int start, int pageLength, out int count)
		{
			return GetByEmailLogTypeId(null, _emailLogTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EmailLogType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeId(TransactionManager transactionManager, System.Int32 _emailLogTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EmailLogType index.
		/// </summary>
		/// <param name="_emailLogTypeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeName(System.String _emailLogTypeName)
		{
			int count = -1;
			return GetByEmailLogTypeName(null,_emailLogTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLogType index.
		/// </summary>
		/// <param name="_emailLogTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeName(System.String _emailLogTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogTypeName(null, _emailLogTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLogType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeName(TransactionManager transactionManager, System.String _emailLogTypeName)
		{
			int count = -1;
			return GetByEmailLogTypeName(transactionManager, _emailLogTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLogType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeName(TransactionManager transactionManager, System.String _emailLogTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailLogTypeName(transactionManager, _emailLogTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLogType index.
		/// </summary>
		/// <param name="_emailLogTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeName(System.String _emailLogTypeName, int start, int pageLength, out int count)
		{
			return GetByEmailLogTypeName(null, _emailLogTypeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EmailLogType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailLogTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EmailLogType GetByEmailLogTypeName(TransactionManager transactionManager, System.String _emailLogTypeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EmailLogType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EmailLogType&gt;"/></returns>
		public static TList<EmailLogType> Fill(IDataReader reader, TList<EmailLogType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EmailLogType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EmailLogType")
					.Append("|").Append((System.Int32)reader[((int)EmailLogTypeColumn.EmailLogTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EmailLogType>(
					key.ToString(), // EntityTrackingKey
					"EmailLogType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EmailLogType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EmailLogTypeId = (System.Int32)reader[((int)EmailLogTypeColumn.EmailLogTypeId - 1)];
					c.EmailLogTypeName = (System.String)reader[((int)EmailLogTypeColumn.EmailLogTypeName - 1)];
					c.EmailLogTypeDesc = (reader.IsDBNull(((int)EmailLogTypeColumn.EmailLogTypeDesc - 1)))?null:(System.String)reader[((int)EmailLogTypeColumn.EmailLogTypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EmailLogType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EmailLogType entity)
		{
			if (!reader.Read()) return;
			
			entity.EmailLogTypeId = (System.Int32)reader[((int)EmailLogTypeColumn.EmailLogTypeId - 1)];
			entity.EmailLogTypeName = (System.String)reader[((int)EmailLogTypeColumn.EmailLogTypeName - 1)];
			entity.EmailLogTypeDesc = (reader.IsDBNull(((int)EmailLogTypeColumn.EmailLogTypeDesc - 1)))?null:(System.String)reader[((int)EmailLogTypeColumn.EmailLogTypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EmailLogType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EmailLogType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EmailLogType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EmailLogTypeId = (System.Int32)dataRow["EmailLogTypeId"];
			entity.EmailLogTypeName = (System.String)dataRow["EmailLogTypeName"];
			entity.EmailLogTypeDesc = Convert.IsDBNull(dataRow["EmailLogTypeDesc"]) ? null : (System.String)dataRow["EmailLogTypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EmailLogType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EmailLogType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLogType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByEmailLogTypeId methods when available
			
			#region EmailLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EmailLog>|EmailLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EmailLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EmailLogCollection = DataRepository.EmailLogProvider.GetByEmailLogTypeId(transactionManager, entity.EmailLogTypeId);

				if (deep && entity.EmailLogCollection.Count > 0)
				{
					deepHandles.Add("EmailLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EmailLog>) DataRepository.EmailLogProvider.DeepLoad,
						new object[] { transactionManager, entity.EmailLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EmailLogType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EmailLogType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EmailLogType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EmailLogType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<EmailLog>
				if (CanDeepSave(entity.EmailLogCollection, "List<EmailLog>|EmailLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EmailLog child in entity.EmailLogCollection)
					{
						if(child.EmailLogTypeIdSource != null)
						{
							child.EmailLogTypeId = child.EmailLogTypeIdSource.EmailLogTypeId;
						}
						else
						{
							child.EmailLogTypeId = entity.EmailLogTypeId;
						}

					}

					if (entity.EmailLogCollection.Count > 0 || entity.EmailLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EmailLogProvider.Save(transactionManager, entity.EmailLogCollection);
						
						deepHandles.Add("EmailLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EmailLog >) DataRepository.EmailLogProvider.DeepSave,
							new object[] { transactionManager, entity.EmailLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EmailLogTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EmailLogType</c>
	///</summary>
	public enum EmailLogTypeChildEntityTypes
	{

		///<summary>
		/// Collection of <c>EmailLogType</c> as OneToMany for EmailLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<EmailLog>))]
		EmailLogCollection,
	}
	
	#endregion EmailLogTypeChildEntityTypes
	
	#region EmailLogTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EmailLogTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeFilterBuilder : SqlFilterBuilder<EmailLogTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeFilterBuilder class.
		/// </summary>
		public EmailLogTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogTypeFilterBuilder
	
	#region EmailLogTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EmailLogTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeParameterBuilder : ParameterizedSqlFilterBuilder<EmailLogTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeParameterBuilder class.
		/// </summary>
		public EmailLogTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogTypeParameterBuilder
	
	#region EmailLogTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EmailLogTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EmailLogTypeSortBuilder : SqlSortBuilder<EmailLogTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeSqlSortBuilder class.
		/// </summary>
		public EmailLogTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EmailLogTypeSortBuilder
	
} // end namespace
