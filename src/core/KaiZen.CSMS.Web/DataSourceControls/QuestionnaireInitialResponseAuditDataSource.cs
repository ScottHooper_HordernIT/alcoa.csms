﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialResponseAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialResponseAuditDataSourceDesigner))]
	public class QuestionnaireInitialResponseAuditDataSource : ProviderDataSource<QuestionnaireInitialResponseAudit, QuestionnaireInitialResponseAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditDataSource class.
		/// </summary>
		public QuestionnaireInitialResponseAuditDataSource() : base(new QuestionnaireInitialResponseAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialResponseAuditDataSourceView used by the QuestionnaireInitialResponseAuditDataSource.
		/// </summary>
		protected QuestionnaireInitialResponseAuditDataSourceView QuestionnaireInitialResponseAuditView
		{
			get { return ( View as QuestionnaireInitialResponseAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialResponseAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialResponseAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialResponseAuditSelectMethod selectMethod = QuestionnaireInitialResponseAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialResponseAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialResponseAuditDataSourceView class that is to be
		/// used by the QuestionnaireInitialResponseAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialResponseAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialResponseAudit, QuestionnaireInitialResponseAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialResponseAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialResponseAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialResponseAuditDataSourceView : ProviderDataSourceView<QuestionnaireInitialResponseAudit, QuestionnaireInitialResponseAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialResponseAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialResponseAuditDataSourceView(QuestionnaireInitialResponseAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialResponseAuditDataSource QuestionnaireInitialResponseAuditOwner
		{
			get { return Owner as QuestionnaireInitialResponseAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialResponseAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialResponseAuditOwner.SelectMethod; }
			set { QuestionnaireInitialResponseAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialResponseAuditService QuestionnaireInitialResponseAuditProvider
		{
			get { return Provider as QuestionnaireInitialResponseAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialResponseAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialResponseAudit> results = null;
			QuestionnaireInitialResponseAudit item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32? _questionnaireId_nullable;
			System.String _questionId_nullable;
			System.Int32? _modifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialResponseAuditSelectMethod.Get:
					QuestionnaireInitialResponseAuditKey entityKey  = new QuestionnaireInitialResponseAuditKey();
					entityKey.Load(values);
					item = QuestionnaireInitialResponseAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialResponseAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialResponseAuditSelectMethod.GetAll:
                    results = QuestionnaireInitialResponseAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialResponseAuditSelectMethod.GetPaged:
					results = QuestionnaireInitialResponseAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialResponseAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialResponseAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialResponseAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialResponseAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialResponseAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireInitialResponseAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialResponseAuditSelectMethod.GetByQuestionnaireIdQuestionId:
					_questionnaireId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32?));
					_questionId_nullable = (System.String) EntityUtil.ChangeType(values["QuestionId"], typeof(System.String));
					results = QuestionnaireInitialResponseAuditProvider.GetByQuestionnaireIdQuestionId(_questionnaireId_nullable, _questionId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialResponseAuditSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireInitialResponseAuditProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialResponseAuditSelectMethod.Get || SelectMethod == QuestionnaireInitialResponseAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialResponseAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialResponseAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialResponseAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialResponseAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialResponseAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialResponseAuditDataSource class.
	/// </summary>
	public class QuestionnaireInitialResponseAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialResponseAudit, QuestionnaireInitialResponseAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialResponseAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialResponseAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialResponseAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialResponseAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialResponseAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialResponseAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialResponseAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialResponseAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialResponseAuditDataSourceActionList(QuestionnaireInitialResponseAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialResponseAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialResponseAuditDataSourceActionList
	
	#endregion QuestionnaireInitialResponseAuditDataSourceDesigner
	
	#region QuestionnaireInitialResponseAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialResponseAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialResponseAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdQuestionId method.
		/// </summary>
		GetByQuestionnaireIdQuestionId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireInitialResponseAuditSelectMethod

	#region QuestionnaireInitialResponseAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseAuditFilter : SqlFilter<QuestionnaireInitialResponseAuditColumn>
	{
	}
	
	#endregion QuestionnaireInitialResponseAuditFilter

	#region QuestionnaireInitialResponseAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialResponseAuditColumn>
	{
	}
	
	#endregion QuestionnaireInitialResponseAuditExpressionBuilder	

	#region QuestionnaireInitialResponseAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialResponseAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseAuditProperty : ChildEntityProperty<QuestionnaireInitialResponseAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialResponseAuditProperty
}

