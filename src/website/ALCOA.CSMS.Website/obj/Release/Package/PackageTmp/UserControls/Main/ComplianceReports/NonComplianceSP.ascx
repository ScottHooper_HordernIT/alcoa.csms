﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_NonComplianceSP"
    CodeBehind="NonComplianceSP.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <span class="bodycopy"><span class="title">Compliance Report</span><br />

            <%--Change Safety Plan to Safety Plans (For Embedded and Non-Embedded 1) by Sayani Sil
            For Task# 8--%>
                <span class="date">Safety Plans (For Embedded and Non-Embedded 1)</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1"></span>
        </td>
    </tr>
    <tr>
        <td style="width: 346px; height: 28px;">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td style="width: 44px; height: 28px; text-align: right">
                        &nbsp;<dxe:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Text="Year:">
                        </dxe:ASPxLabel>&nbsp;
                    </td>
                    <td style="width: 100px; height: 28px">
                        <dxe:ASPxComboBox ID="cmbYear" runat="server" AutoPostBack="True" ClientInstanceName="cmbYear"
                            DataSourceID="sqldsKPIYear" TextField="Year" ValueField="Year" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" EnableClientSideAPI="True" Width="55px" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" EnableIncrementalFiltering="True" 
                            >
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { grid.PerformCallback(s.GetValue()); }" />
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="width: 44px; height: 28px; text-align: right">
                        &nbsp;<dxe:ASPxLabel ID="lblRegion" runat="server" Font-Bold="True" Text="Region:">
                        </dxe:ASPxLabel>&nbsp;
                    </td>
                    <td style="width: 100%; height: 28px">
                        <dxe:ASPxComboBox ID="ddlRegions" runat="server" AutoPostBack="True" ValueType="System.Int32">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { grid.PerformCallback(s.GetValue()); }" />
                        </dxe:ASPxComboBox>
                    </td>

                </tr>
            </table>
        </td>
        <td style="width: 700px; height: 28px;">
            
        </td>
        <td style="width: 276px; text-align: right; height: 28px;">
            <button name="btnPrint" onclick="javascript:print();" style="width: 80px; height: 25px;
                font-weight: bold">
                Print</button>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                DataSourceID="sqldsNonComplianceReportYearSP" Width="900px" OnHtmlRowCreated="grid_RowCreated"
                KeyFieldName="CompanyId" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                OnCustomCallback="grid_CustomCallback">
                <Columns>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
                        VisibleIndex="0"  Settings-SortMode="DisplayText" >
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>

                   <%-- Change below caption Embeded to Category By Sayani Sil For Task# 9
                   -- Below field name also change from ResidentialStatus to CategoryDesc for Task# 9
                   SqldsCategoryList
                   --%>
                   <%--Change by Sayani for Task# 9--%>
                   <%-- <dxwgv:GridViewDataTextColumn Caption="Category" SortIndex="0" SortOrder="Ascending"
                        FieldName="CategoryDesc" VisibleIndex="1">
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataTextColumn>--%>
                    
                     <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" 
                     Settings-SortMode="DisplayText" Name="CategoryBox"
                        FieldName="CategoryId" VisibleIndex="1">
                        <PropertiesComboBox DataSourceID="SqldsCategoryList" DropDownHeight="150px" TextField="CategoryDesc"
                            ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Assigned H&amp;S Assessor" FieldName="EhsConsultantUserFullName"
                        VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="Submitted" FieldName="Submitted" ReadOnly="True"
                        VisibleIndex="3" Width="100px">
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Image ID="changeImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                            GenerateEmptyAlternateText="True" />
                                    </td>
                                    <td style="width: 30px">
                                        <asp:Label ID="noSPSubmitted" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Latest Plan Current Status" FieldName="StatusName"
                        Name="StatusBox" VisibleIndex="4" Width="150px">
                        <PropertiesComboBox DataSourceID="SafetyPlanStatusDataSource" DropDownHeight="150px"
                            TextField="StatusName" ValueField="StatusName" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="False" />
                        <Settings SortMode="DisplayText" />
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 107px">
                                        <dxe:ASPxHyperLink ID="hlSE" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="SelfAssessment_ResponseId"
                        Visible="false" ShowInCustomizationForm="false">
                    </dxwgv:GridViewDataComboBoxColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupedColumns="True" ShowFilterRow="True" />
                <SettingsPager Visible="true" PageSize="20" Mode="ShowPager">
                    <AllButton Visible="true">
                    </AllButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" />
                <SettingsCookies CookiesID="NonComplianceSP" Version="0.1" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
        </td>
    </tr>
</table>
<table width="900px">
    <tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align: -moz-right;
            width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
    </tr>
</table>
<%-- Change below caption Embeded to Category By Sayani Sil For Task# 9
                   -- Below field name also change from ResidentialStatus to CategoryDesc for Task# 9
                   SqldsCategoryList
                   --%>

    <asp:SqlDataSource ID="sqldsNonComplianceReportYearSP" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_FileDb_GetReport_Compliance_SP2" SelectCommandType="StoredProcedure">

<SelectParameters>
        <asp:ControlParameter ControlID="cmbYear" DefaultValue="2012" Name="YEAR" PropertyName="Value" Type="Int32" />
        <asp:ControlParameter ControlID="ddlRegions" DefaultValue="0" Name="RegionId" PropertyName="Value" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
</asp:SqlDataSource>
<data:SafetyPlanStatusDataSource ID="SafetyPlanStatusDataSource" runat="server" SelectMethod="GetPaged"
    EnableSorting="true" EnableCaching="true" EnableDeepLoad="False" Sort="StatusName ASC">
</data:SafetyPlanStatusDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<%--Change by Sayani for Task# 9--%>
<asp:SqlDataSource ID="SqldsCategoryList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CompanySiteCategory_SiteCategory" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsRegionsList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Regions_Get_List" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>