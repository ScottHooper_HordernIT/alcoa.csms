﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EnumQuestionnaireMainAnswerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EnumQuestionnaireMainAnswerProviderBaseCore : EntityViewProviderBase<EnumQuestionnaireMainAnswer>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EnumQuestionnaireMainAnswer&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EnumQuestionnaireMainAnswer&gt;"/></returns>
		protected static VList&lt;EnumQuestionnaireMainAnswer&gt; Fill(DataSet dataSet, VList<EnumQuestionnaireMainAnswer> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EnumQuestionnaireMainAnswer>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EnumQuestionnaireMainAnswer&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EnumQuestionnaireMainAnswer>"/></returns>
		protected static VList&lt;EnumQuestionnaireMainAnswer&gt; Fill(DataTable dataTable, VList<EnumQuestionnaireMainAnswer> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EnumQuestionnaireMainAnswer c = new EnumQuestionnaireMainAnswer();
					c.AnswerId = (Convert.IsDBNull(row["AnswerId"]))?(int)0:(System.Int32)row["AnswerId"];
					c.QuestionAnswerNo = (Convert.IsDBNull(row["QuestionAnswerNo"]))?string.Empty:(System.String)row["QuestionAnswerNo"];
					c.AnswerText = (Convert.IsDBNull(row["AnswerText"]))?string.Empty:(System.String)row["AnswerText"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EnumQuestionnaireMainAnswer&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EnumQuestionnaireMainAnswer&gt;"/></returns>
		protected VList<EnumQuestionnaireMainAnswer> Fill(IDataReader reader, VList<EnumQuestionnaireMainAnswer> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EnumQuestionnaireMainAnswer entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EnumQuestionnaireMainAnswer>("EnumQuestionnaireMainAnswer",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EnumQuestionnaireMainAnswer();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AnswerId = (System.Int32)reader[((int)EnumQuestionnaireMainAnswerColumn.AnswerId)];
					//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
					entity.QuestionAnswerNo = (System.String)reader[((int)EnumQuestionnaireMainAnswerColumn.QuestionAnswerNo)];
					//entity.QuestionAnswerNo = (Convert.IsDBNull(reader["QuestionAnswerNo"]))?string.Empty:(System.String)reader["QuestionAnswerNo"];
					entity.AnswerText = (reader.IsDBNull(((int)EnumQuestionnaireMainAnswerColumn.AnswerText)))?null:(System.String)reader[((int)EnumQuestionnaireMainAnswerColumn.AnswerText)];
					//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EnumQuestionnaireMainAnswer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EnumQuestionnaireMainAnswer"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EnumQuestionnaireMainAnswer entity)
		{
			reader.Read();
			entity.AnswerId = (System.Int32)reader[((int)EnumQuestionnaireMainAnswerColumn.AnswerId)];
			//entity.AnswerId = (Convert.IsDBNull(reader["AnswerId"]))?(int)0:(System.Int32)reader["AnswerId"];
			entity.QuestionAnswerNo = (System.String)reader[((int)EnumQuestionnaireMainAnswerColumn.QuestionAnswerNo)];
			//entity.QuestionAnswerNo = (Convert.IsDBNull(reader["QuestionAnswerNo"]))?string.Empty:(System.String)reader["QuestionAnswerNo"];
			entity.AnswerText = (reader.IsDBNull(((int)EnumQuestionnaireMainAnswerColumn.AnswerText)))?null:(System.String)reader[((int)EnumQuestionnaireMainAnswerColumn.AnswerText)];
			//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EnumQuestionnaireMainAnswer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EnumQuestionnaireMainAnswer"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EnumQuestionnaireMainAnswer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerId = (Convert.IsDBNull(dataRow["AnswerId"]))?(int)0:(System.Int32)dataRow["AnswerId"];
			entity.QuestionAnswerNo = (Convert.IsDBNull(dataRow["QuestionAnswerNo"]))?string.Empty:(System.String)dataRow["QuestionAnswerNo"];
			entity.AnswerText = (Convert.IsDBNull(dataRow["AnswerText"]))?string.Empty:(System.String)dataRow["AnswerText"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EnumQuestionnaireMainAnswerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainAnswerFilterBuilder : SqlFilterBuilder<EnumQuestionnaireMainAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerFilterBuilder class.
		/// </summary>
		public EnumQuestionnaireMainAnswerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainAnswerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainAnswerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainAnswerFilterBuilder

	#region EnumQuestionnaireMainAnswerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainAnswerParameterBuilder : ParameterizedSqlFilterBuilder<EnumQuestionnaireMainAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerParameterBuilder class.
		/// </summary>
		public EnumQuestionnaireMainAnswerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainAnswerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainAnswerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainAnswerParameterBuilder
	
	#region EnumQuestionnaireMainAnswerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EnumQuestionnaireMainAnswerSortBuilder : SqlSortBuilder<EnumQuestionnaireMainAnswerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerSqlSortBuilder class.
		/// </summary>
		public EnumQuestionnaireMainAnswerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EnumQuestionnaireMainAnswerSortBuilder

} // end namespace
