﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit15Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit15DataSourceDesigner))]
	public class TwentyOnePointAudit15DataSource : ProviderDataSource<TwentyOnePointAudit15, TwentyOnePointAudit15Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15DataSource class.
		/// </summary>
		public TwentyOnePointAudit15DataSource() : base(new TwentyOnePointAudit15Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit15DataSourceView used by the TwentyOnePointAudit15DataSource.
		/// </summary>
		protected TwentyOnePointAudit15DataSourceView TwentyOnePointAudit15View
		{
			get { return ( View as TwentyOnePointAudit15DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit15DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit15SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit15SelectMethod selectMethod = TwentyOnePointAudit15SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit15SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit15DataSourceView class that is to be
		/// used by the TwentyOnePointAudit15DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit15DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit15, TwentyOnePointAudit15Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit15DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit15DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit15DataSourceView : ProviderDataSourceView<TwentyOnePointAudit15, TwentyOnePointAudit15Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit15DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit15DataSourceView(TwentyOnePointAudit15DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit15DataSource TwentyOnePointAudit15Owner
		{
			get { return Owner as TwentyOnePointAudit15DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit15SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit15Owner.SelectMethod; }
			set { TwentyOnePointAudit15Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit15Service TwentyOnePointAudit15Provider
		{
			get { return Provider as TwentyOnePointAudit15Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit15> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit15> results = null;
			TwentyOnePointAudit15 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit15SelectMethod.Get:
					TwentyOnePointAudit15Key entityKey  = new TwentyOnePointAudit15Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit15Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit15>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit15SelectMethod.GetAll:
                    results = TwentyOnePointAudit15Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit15SelectMethod.GetPaged:
					results = TwentyOnePointAudit15Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit15SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit15Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit15Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit15SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit15Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit15>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit15SelectMethod.Get || SelectMethod == TwentyOnePointAudit15SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit15 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit15Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit15> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit15Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit15DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit15DataSource class.
	/// </summary>
	public class TwentyOnePointAudit15DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit15, TwentyOnePointAudit15Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit15DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit15SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit15DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit15DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit15DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit15DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit15DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit15DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit15DataSourceActionList(TwentyOnePointAudit15DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit15SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit15DataSourceActionList
	
	#endregion TwentyOnePointAudit15DataSourceDesigner
	
	#region TwentyOnePointAudit15SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit15DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit15SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit15SelectMethod

	#region TwentyOnePointAudit15Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15Filter : SqlFilter<TwentyOnePointAudit15Column>
	{
	}
	
	#endregion TwentyOnePointAudit15Filter

	#region TwentyOnePointAudit15ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit15Column>
	{
	}
	
	#endregion TwentyOnePointAudit15ExpressionBuilder	

	#region TwentyOnePointAudit15Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit15ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15Property : ChildEntityProperty<TwentyOnePointAudit15ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit15Property
}

