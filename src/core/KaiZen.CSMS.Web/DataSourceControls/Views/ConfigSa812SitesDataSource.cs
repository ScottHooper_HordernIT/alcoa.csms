﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ConfigSa812SitesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(ConfigSa812SitesDataSourceDesigner))]
	public class ConfigSa812SitesDataSource : ReadOnlyDataSource<ConfigSa812Sites>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesDataSource class.
		/// </summary>
		public ConfigSa812SitesDataSource() : base(new ConfigSa812SitesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ConfigSa812SitesDataSourceView used by the ConfigSa812SitesDataSource.
		/// </summary>
		protected ConfigSa812SitesDataSourceView ConfigSa812SitesView
		{
			get { return ( View as ConfigSa812SitesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ConfigSa812SitesDataSourceView class that is to be
		/// used by the ConfigSa812SitesDataSource.
		/// </summary>
		/// <returns>An instance of the ConfigSa812SitesDataSourceView class.</returns>
		protected override BaseDataSourceView<ConfigSa812Sites, Object> GetNewDataSourceView()
		{
			return new ConfigSa812SitesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ConfigSa812SitesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ConfigSa812SitesDataSourceView : ReadOnlyDataSourceView<ConfigSa812Sites>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ConfigSa812SitesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ConfigSa812SitesDataSourceView(ConfigSa812SitesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ConfigSa812SitesDataSource ConfigSa812SitesOwner
		{
			get { return Owner as ConfigSa812SitesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ConfigSa812SitesService ConfigSa812SitesProvider
		{
			get { return Provider as ConfigSa812SitesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region ConfigSa812SitesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ConfigSa812SitesDataSource class.
	/// </summary>
	public class ConfigSa812SitesDataSourceDesigner : ReadOnlyDataSourceDesigner<ConfigSa812Sites>
	{
	}

	#endregion ConfigSa812SitesDataSourceDesigner

	#region ConfigSa812SitesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812SitesFilter : SqlFilter<ConfigSa812SitesColumn>
	{
	}

	#endregion ConfigSa812SitesFilter

	#region ConfigSa812SitesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812SitesExpressionBuilder : SqlExpressionBuilder<ConfigSa812SitesColumn>
	{
	}
	
	#endregion ConfigSa812SitesExpressionBuilder		
}

