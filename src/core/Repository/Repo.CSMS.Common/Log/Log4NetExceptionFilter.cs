﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;

namespace Repo.CSMS.Common.Log
{
    public class Log4NetExceptionFilter : IExceptionFilter 
    {
        public void OnException(ExceptionContext context)     
        {         
            Exception ex = context.Exception;         
                
            if (!(ex is HttpException)) //ignore "file not found"         
            {
                //Log error
                var logger = DependencyResolver.Current.GetService<ILogService>();
                logger.LogError(string.Format("System error. {0}", ex.Message), ex);
            }     
        } 
    }
}
