<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SafetyPlans_Assess" Codebehind="SafetyPlans_Assess.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxhe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxwsc" %>

<%--<script language="JavaScript">
  var needToConfirm = true;
  
  window.onbeforeunload = confirmExit;
  function confirmExit()
  {
    if (needToConfirm)
      return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
  }
</script>--%>

<div align="center" style="height: auto; width: auto">
        <table style="width: 900px">
            <tr>
                <td style="text-align: center" colspan="3">
        <div align="center">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td colspan="2">
                                &nbsp;<dxe:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" OnClick="btnSave_Click"
                                    Text="Save & Close" Width="200px" 
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center">
                                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label></td>
                        </tr>
                    </table>
                                <dxe:ASPxButton ID="btnHome" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Font-Bold="True" ForeColor="Green" OnClick="btnHome_Click"
                                    Text="Go Back" Width="100px" Visible="False" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxButton>
                                </div>
                </td>
            </tr>
        </table>
        </div>
        <asp:Panel ID="Panel1" runat="server" Width="890px">
        <table width="900px">
            <tr>
                <td style="width: 890px;">
                    <dxrp:aspxroundpanel id="rp1" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" enabledefaultappearance="False" 
                        headertext="Question 1" width="890px" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion1" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer1" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px; height: 78px;">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments1" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px; height: 78px;">
        </td>
        <td style="width: 110px; height: 78px;">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments1" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner
                            Width="9px" />
                        <BottomLeftCorner
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                </td>
                </tr>
<tr><td>
<dxrp:aspxroundpanel id="rp2" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 2" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion2" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer2" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px"  IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments2" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments2" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp3" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 3" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion3" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer3" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments3" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments3" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp4" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 4" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion4" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer4" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments4" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments4" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp5" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 5" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion5" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer5" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments5" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments5" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp6" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 6" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion6" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer6" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments6" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments6" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp7" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 7" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion7" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer7" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" >
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments7" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments7" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp8" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 8" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion8" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer8" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments8" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments8" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp9" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 9" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion9" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer9" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments9" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments9" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
                    </td></tr>
<tr><td>
<dxrp:aspxroundpanel id="rp10" runat="server" backcolor="#DDECFE" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        csspostfix="Office2003Blue" 
        enabledefaultappearance="False" headertext="Question 10" width="890px" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<TopEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</TopEdge>

<BottomRightCorner Height="9px" Width="9px"></BottomRightCorner>

<ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>

<NoHeaderTopRightCorner Height="9px" Width="9px"></NoHeaderTopRightCorner>

<HeaderRightEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderRightEdge>

<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>

<HeaderLeftEdge>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderLeftEdge>

<HeaderStyle BackColor="#7BA4E0">
<Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>

<BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
</HeaderStyle>

<TopRightCorner Height="9px" Width="9px"></TopRightCorner>

<HeaderContent>
<BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" 
        Repeat="RepeatX" HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
</HeaderContent>

<DisabledStyle ForeColor="Gray"></DisabledStyle>

<NoHeaderTopEdge BackColor="#DDECFE"></NoHeaderTopEdge>

<NoHeaderTopLeftCorner Height="9px" Width="9px"></NoHeaderTopLeftCorner>
<PanelCollection>
<dxp:PanelContent runat="server"><TABLE width="850" id="TABLE1"><TBODY><TR><TD style="WIDTH: 700px; height: 18px;"><asp:Label runat="server" ForeColor="Maroon" ID="lblQuestion10" Font-Names="Tahoma" Font-Size="11pt"></asp:Label>
 </TD><TD style="WIDTH: 40px; height: 18px; text-align: center;">
     &nbsp;<asp:Label ID="lblAnswer10" runat="server" Font-Names="Tahoma" Font-Size="11pt"
         ForeColor="Green" Font-Bold="True">Yes</asp:Label>
 </TD>
    <td style="width: 110px; height: 18px;">
        <dxe:ASPxComboBox ID="cb10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue"
            ValueType="System.String" Width="110px" IncrementalFilteringMode="StartsWith" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <Items>
                <dxe:ListEditItem Text=" " Value="-1" />
                <dxe:ListEditItem Text="Not Approved" Value="0" />
                <dxe:ListEditItem Text="Approved" Value="1" />
            </Items>
            <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </loadingpanelimage>
            <ButtonStyle Width="13px">
            </ButtonStyle>
        </dxe:ASPxComboBox>
    </td>
</tr>
    <tr>
        <td style="width: 700px">
            <strong>Comments<br />
            </strong>
            <asp:Label ID="lblComments10" runat="server" Font-Names="Tahoma" Font-Size="11pt"
                ForeColor="Black"></asp:Label>
            <br />
            <br />
            <strong>Reviewer Comments</strong></td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
    <tr>
        <td style="width: 700px">
            <dxe:ASPxMemo ID="memoAssessorComments10" runat="server" Height="100px" 
                Width="700px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxMemo>
        </td>
        <td style="width: 40px">
        </td>
        <td style="width: 110px">
        </td>
    </tr>
</tbody>
</table>
</dxp:PanelContent>
</PanelCollection>
                        <TopLeftCorner Height="9px"
                            Width="9px" />
                        <BottomLeftCorner Height="9px"
                            Width="9px" />
                    </dxrp:ASPxRoundPanel>
</td>

            </tr>
        </table>
        </asp:Panel>
<br />
<strong>Review Summary</strong><br />
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="width: 100px">
            <dxhe:ASPxHtmlEditor runat="server" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" ID="htmlAssessorComments" Height="250px" 
                Width="700px">
    <SettingsImageUpload>
        <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png">
        </ValidationSettings>
    </SettingsImageUpload>
    <SettingsSpellChecker>
        <Dictionaries>
            <dxwsc:ASPxSpellCheckerOpenOfficeDictionary GrammarPath="~/Common/en_AU.aff" DictionaryPath="~/Common/en_AU.dic">
            </dxwsc:ASPxSpellCheckerOpenOfficeDictionary>
        </Dictionaries>
    </SettingsSpellChecker>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanel Url="~/App_Themes/Office2003Blue/HtmlEditor/Loading.gif">
        </LoadingPanel>
    </Images>
    <Settings AllowHtmlView="False" AllowPreview="False" />
    <Toolbars>
        <dxhe:HtmlEditorToolbar>
            <Items>
                <dxhe:ToolbarCutButton>
                </dxhe:ToolbarCutButton>
                <dxhe:ToolbarCopyButton>
                </dxhe:ToolbarCopyButton>
                <dxhe:ToolbarPasteButton>
                </dxhe:ToolbarPasteButton>
                <dxhe:ToolbarUndoButton BeginGroup="True">
                </dxhe:ToolbarUndoButton>
                <dxhe:ToolbarRedoButton>
                </dxhe:ToolbarRedoButton>
                <dxhe:ToolbarRemoveFormatButton BeginGroup="True">
                </dxhe:ToolbarRemoveFormatButton>
                <dxhe:ToolbarSuperscriptButton BeginGroup="True">
                </dxhe:ToolbarSuperscriptButton>
                <dxhe:ToolbarSubscriptButton>
                </dxhe:ToolbarSubscriptButton>
                <dxhe:ToolbarInsertOrderedListButton BeginGroup="True">
                </dxhe:ToolbarInsertOrderedListButton>
                <dxhe:ToolbarInsertUnorderedListButton>
                </dxhe:ToolbarInsertUnorderedListButton>
                <dxhe:ToolbarIndentButton BeginGroup="True">
                </dxhe:ToolbarIndentButton>
                <dxhe:ToolbarOutdentButton>
                </dxhe:ToolbarOutdentButton>
                <dxhe:ToolbarInsertLinkDialogButton BeginGroup="True">
                </dxhe:ToolbarInsertLinkDialogButton>
                <dxhe:ToolbarUnlinkButton>
                </dxhe:ToolbarUnlinkButton>
                <dxhe:ToolbarCheckSpellingButton BeginGroup="True">
                </dxhe:ToolbarCheckSpellingButton>
                <dxhe:ToolbarPrintButton>
                </dxhe:ToolbarPrintButton>
            </Items>
        </dxhe:HtmlEditorToolbar>
        <dxhe:HtmlEditorToolbar>
            <Items>
                <dxhe:ToolbarParagraphFormattingEdit Width="120px">
                    <Items>
                        <dxhe:ToolbarListEditItem Text="Normal" Value="p" />
                        <dxhe:ToolbarListEditItem Text="Heading  1" Value="h1" />
                        <dxhe:ToolbarListEditItem Text="Heading  2" Value="h2" />
                        <dxhe:ToolbarListEditItem Text="Heading  3" Value="h3" />
                        <dxhe:ToolbarListEditItem Text="Heading  4" Value="h4" />
                        <dxhe:ToolbarListEditItem Text="Heading  5" Value="h5" />
                        <dxhe:ToolbarListEditItem Text="Heading  6" Value="h6" />
                        <dxhe:ToolbarListEditItem Text="Address" Value="address" />
                        <dxhe:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                    </Items>
                </dxhe:ToolbarParagraphFormattingEdit>
                <dxhe:ToolbarFontNameEdit>
                    <Items>
                        <dxhe:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                        <dxhe:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                        <dxhe:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                        <dxhe:ToolbarListEditItem Text="Arial" Value="Arial" />
                        <dxhe:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                        <dxhe:ToolbarListEditItem Text="Courier" Value="Courier" />
                    </Items>
                </dxhe:ToolbarFontNameEdit>
                <dxhe:ToolbarFontSizeEdit>
                    <Items>
                        <dxhe:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                        <dxhe:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                        <dxhe:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                        <dxhe:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                        <dxhe:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                        <dxhe:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                        <dxhe:ToolbarListEditItem Text="7 (36pt)" Value="7" />
                    </Items>
                </dxhe:ToolbarFontSizeEdit>
                <dxhe:ToolbarBoldButton BeginGroup="True">
                </dxhe:ToolbarBoldButton>
                <dxhe:ToolbarItalicButton>
                </dxhe:ToolbarItalicButton>
                <dxhe:ToolbarUnderlineButton>
                </dxhe:ToolbarUnderlineButton>
                <dxhe:ToolbarStrikethroughButton>
                </dxhe:ToolbarStrikethroughButton>
                <dxhe:ToolbarJustifyLeftButton BeginGroup="True">
                </dxhe:ToolbarJustifyLeftButton>
                <dxhe:ToolbarJustifyCenterButton>
                </dxhe:ToolbarJustifyCenterButton>
                <dxhe:ToolbarJustifyRightButton>
                </dxhe:ToolbarJustifyRightButton>
                <dxhe:ToolbarJustifyFullButton>
                </dxhe:ToolbarJustifyFullButton>
                <dxhe:ToolbarBackColorButton BeginGroup="True">
                </dxhe:ToolbarBackColorButton>
                <dxhe:ToolbarFontColorButton>
                </dxhe:ToolbarFontColorButton>
            </Items>
        </dxhe:HtmlEditorToolbar>
    </Toolbars>
    <Styles CssPostfix="Office2003Blue" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
        <ViewArea>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </ViewArea>
    </Styles>
    <PartsRoundPanel>
        <TopLeftCorner Height="9px"
            Width="9px" />
        <NoHeaderTopLeftCorner Height="9px"
            Width="9px" />
        <TopRightCorner Height="9px"
            Width="9px" />
        <NoHeaderTopRightCorner Height="9px"
            Width="9px" />
        <BottomRightCorner Height="9px"
            Width="9px" />
        <BottomLeftCorner Height="9px"
            Width="9px" />
        <HeaderLeftEdge>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </HeaderLeftEdge>
        <HeaderContent>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </HeaderContent>
        <HeaderRightEdge>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </HeaderRightEdge>
        <NoHeaderTopEdge BackColor="#DDECFE">
        </NoHeaderTopEdge>
        <TopEdge>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </TopEdge>
    </PartsRoundPanel>
    <StylesRoundPanel>
        <ControlStyle BackColor="#DDECFE">
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </ControlStyle>
    </StylesRoundPanel>
    <StylesStatusBar>
        <ActiveTab BackColor="White">
        </ActiveTab>
    </StylesStatusBar>
</dxhe:ASPxHtmlEditor>
        </td>
        <td style="width: 100px">
            <dxe:ASPxButton ID="btnSave2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" OnClick="btnSave_Click"
                                    Text="Save & Close" Width="135px" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxButton>
        </td>
    </tr>
</table>
<br />
