﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompanyChangeApprovalList.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Main.CompanyChangeApprovalList" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>


<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 43px">
            <span class="bodycopy"><span class="title">Manager - Procurement Operations : Approvals
                <br>
                <span class="date">Company Status Change Requests</span>
                <br />
                <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
        </td>
    </tr>
</table>
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Width="900px">
    <TabPages>
        <dx:TabPage Text="Awaiting Approval">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    The following are company change status requests which are pending approval from
                    the Manager - Procurement Operations or CSMS Administrator:<br />
                    <dx:ASPxGridView ID="gridCcsrAwaitingApproval" runat="server" AutoGenerateColumns="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                        DataSourceID="CompanyStatusChangeApprovalDataSource1" KeyFieldName="CompanyStatusChangeApprovalId"
                        OnRowUpdating="ASPxGridView1_RowUpdating">
                        <Columns>
                            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0">
                                <EditButton Visible="True">
                                </EditButton>
                            </dx:GridViewCommandColumn>
                            <%--DT300 6/1/2015 Cindi Thornton - hid id column, added formatting for the editing layout.--%>

                            <dx:GridViewDataTextColumn Caption="ID" FieldName="CompanyStatusChangeApprovalId" ShowInCustomizationForm="True"
                                Visible="false" VisibleIndex="1" ReadOnly="true">
                                <EditFormSettings Visible="false" VisibleIndex="1" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="RequestedDate" ReadOnly="True" ShowInCustomizationForm="True"
                                VisibleIndex="4">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Requested By" FieldName="RequestedByUserId"
                                ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="5">
                                <PropertiesComboBox DataSourceID="UsersFullNameDataSource1" TextField="UserFullName"
                                    ValueField="UserId" ValueType="System.Int32">
                                </PropertiesComboBox>
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataMemoColumn FieldName="Comments" ReadOnly="True" ShowInCustomizationForm="True"
                                VisibleIndex="9">
                                <EditFormSettings Visible="True" VisibleIndex="3" ColumnSpan="8" />
                            </dx:GridViewDataMemoColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" ReadOnly="True"
                                ShowInCustomizationForm="True" VisibleIndex="6">
                                <PropertiesComboBox DataSourceID="CompaniesDataSource1" TextField="CompanyName" ValueField="CompanyId"
                                    ValueType="System.Int32">
                                </PropertiesComboBox>
                                <EditFormSettings Visible="True" VisibleIndex="1" ColumnSpan="4" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Requested Company Status" FieldName="RequestedCompanyStatusId"
                                ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="8">
                                <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" TextField="CompanyStatusDesc"
                                    ValueField="CompanyStatusId" ValueType="System.Int32">
                                </PropertiesComboBox>
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataMemoColumn FieldName="AssessorComments" ShowInCustomizationForm="True"
                                VisibleIndex="3" Visible="False">
                                <EditFormSettings Visible="True" VisibleIndex="4" ColumnSpan="8" />
                            </dx:GridViewDataMemoColumn>
                            <dx:GridViewDataCheckColumn Caption="Approved" FieldName="Approved" ShowInCustomizationForm="True"
                                Visible="false" VisibleIndex="2">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Current Company Status" FieldName="CurrentCompanyStatusId"
                                ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="7">
                                <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" TextField="CompanyStatusDesc"
                                    ValueField="CompanyStatusId" ValueType="System.Int32">
                                </PropertiesComboBox>
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataComboBoxColumn>
                            <%--Changed for Item#11--%>
                            <dx:GridViewDataComboBoxColumn Caption="Approved" FieldName="dlApproved"
                                ShowInCustomizationForm="False" VisibleIndex="11" Visible="false" ReadOnly="false" UnboundType="String">
                                <PropertiesComboBox ValueType="System.Int32" DropDownStyle="DropDown">
                                    <Items>
                                    <dx:ListEditItem Text=" " Value="0" Selected="true"/>
                                    <dx:ListEditItem Text="Yes" Value="1" />
                                    <dx:ListEditItem Text="No" Value="2" />
                                     </Items>
                                </PropertiesComboBox>
                                <EditFormSettings Visible="True" VisibleIndex="3" ColumnSpan="1" />
                            </dx:GridViewDataComboBoxColumn>
                            <%--End Changed--%>
                        </Columns>
                        <%--Added for DT2554--%>
                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <%--End--%>
                        <SettingsEditing EditFormColumnCount="8">
                        </SettingsEditing>
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif">
                                <%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                    </dx:ASPxGridView>
                    <%--DT300 6/1/2015 Cindi Thornton - align right.--%>
                    <div width="900px" text-align="right" align="right">
                        <uc1:ExportButtons ID="ExportButtons1" runat="server" />
                    </div>
                </dx:ContentControl>
            </ContentCollection>

        </dx:TabPage>
        <dx:TabPage Text="History">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    The following is a historical list of all company status change requests, which
                    have then been approved or not approved:<br />
                    <dx:ASPxGridView ID="gridCcsrHistory" runat="server" AutoGenerateColumns="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                        DataSourceID="CompanyStatusChangeApprovalDataSource2" KeyFieldName="CompanyStatusChangeApprovalId"
                        OnRowUpdating="ASPxGridView1_RowUpdating">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="CompanyStatusChangeApprovalId" ShowInCustomizationForm="True"
                                Visible="False" VisibleIndex="2">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="RequestedDate" ReadOnly="True" ShowInCustomizationForm="True"  
                                VisibleIndex="6">
                                <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Requested By" FieldName="RequestedByUserId"
                                ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="8">
                                <PropertiesComboBox DataSourceID="UsersFullNameDataSource1" TextField="UserFullName"
                                    ValueField="UserId" ValueType="System.Int32">
                                </PropertiesComboBox>
                                <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataTextColumn FieldName="Comments" ReadOnly="True"  Visible="False"
                                >
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" ReadOnly="True"
                                ShowInCustomizationForm="True" VisibleIndex="7">
                                <PropertiesComboBox DataSourceID="CompaniesDataSource1" TextField="CompanyName" ValueField="CompanyId"
                                    ValueType="System.Int32">
                                </PropertiesComboBox>
                                <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Requested Company Status" FieldName="RequestedCompanyStatusId"
                                ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="9">
                                <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" TextField="CompanyStatusDesc"
                                    ValueField="CompanyStatusId" ValueType="System.Int32">
                                </PropertiesComboBox>
                                <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataMemoColumn FieldName="AssessorComments"  Visible="False" VisibleIndex="0">
                            </dx:GridViewDataMemoColumn>
                            <dx:GridViewDataCheckColumn Caption="Approved" FieldName="Approved" ShowInCustomizationForm="True"
                                VisibleIndex="3">
                                <Settings AllowHeaderFilter="True" />
                            </dx:GridViewDataCheckColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Current Company Status" FieldName="CurrentCompanyStatusId"
                                ReadOnly="True"  Visible="False" VisibleIndex="1">
                                <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" TextField="CompanyStatusDesc"
                                    ValueField="CompanyStatusId" ValueType="System.Int32">
                                </PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Assessed By" FieldName="AssessedByUserId"
                                ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="5">
                                <PropertiesComboBox DataSourceID="UsersFullNameDataSource1" TextField="UserFullName"
                                    ValueField="UserId" ValueType="System.Int32">
                                </PropertiesComboBox>
                                <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataDateColumn FieldName="AssessedDate" ReadOnly="True" ShowInCustomizationForm="True"
                                VisibleIndex="4">
                                <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                            </dx:GridViewDataDateColumn>
                        </Columns>
                        <%--Added for DT2554--%>
                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <%--End--%>
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                        <Templates>
                            <DataRow>
                                <table class="templateTable">
                                    <tr>
                                        <td class="dataTitle">Company:</td>
                                        <td class="value" colspan="3">
                                            <dx:ASPxLabel ID="lblHistoryCompanyName" runat="server" Text='<%# Eval("CompanyName") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="dataTitle">Requested:</td>
                                        <td class="templateTable_RequestedbyCell">
                                            <dx:ASPxLabel ID="lblHistoryRequested" runat="server" Text='<%# Eval("RequestedDate", "{0:d}") + " by " + Eval("RequestedByUserName") %>' />
                                        </td>
                                        <td class="dataTitle">Approved: </td>
                                        <td class="value"><dx:ASPxCheckBox ID="chkHistoryApproved"  runat="server" ReadOnly="true" Checked='<%# Eval("Approved") %>'   />
                                            <dx:ASPxLabel ID="lblHistoryApproved" runat="server" Text='<%# Eval("AssessedDate", "{0:d}") + " by " + Eval("AssessedByUserName") %>'  />
                                     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="dataTitle">Current Company Status:</td>
                                        <td class="value" colspan="3">
                                            <dx:ASPxLabel ID="lblHistoryCurrentCompanyStatus" runat="server" Text='<%# Eval("CurrentCompanyStatus") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="dataTitle">Requested Company Status:</td>
                                        <td class="value" colspan="3">
                                            <dx:ASPxLabel ID="lblHistoryRequestedCompanyStatus" runat="server" Text='<%# Eval("RequestedCompanyStatus") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="dataTitle">Comments:</td>
                                        <td class="value" colspan="3">
                                            <dx:ASPxLabel ID="lblHistoryComments" runat="server" Text='<%# Eval("Comments") %>' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="dataTitle">Assessor Comments:</td>
                                        <td class="value" colspan="3">
                                            <dx:ASPxLabel ID="lblHistoryAssessorComments" runat="server" Text='<%# Eval("AssessorComments") %>' />
                                        </td>
                                    </tr>
                                    <tr class="templateTable_newRow">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </DataRow>
                        </Templates>
                    </dx:ASPxGridView>
                    <%--DT300 6/1/2015 Cindi Thornton - align right.--%>
                    <div width="900px" text-align="right" align="right">
                        <uc1:ExportButtons ID="ExportButtons2" runat="server" />
                    </div>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>
<data:CompanyStatusDataSource ID="CompanyStatusDataSource1" runat="server">
</data:CompanyStatusDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server">
</data:CompaniesDataSource>
<data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server">
</data:UsersFullNameDataSource>
<asp:SqlDataSource ID="CompanyStatusChangeApprovalDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CompanyStatusChangeApproval_ListAwaitingApproval"
    SelectCommandType="StoredProcedure"></asp:SqlDataSource>
<asp:SqlDataSource ID="CompanyStatusChangeApprovalDataSource2" runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CompanyStatusChangeApproval_ListHistory"
    SelectCommandType="StoredProcedure"></asp:SqlDataSource>

