//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class YearlyTarget
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public Nullable<int> EHSAudits { get; set; }
        public Nullable<int> WorkplaceSafetyCompliance { get; set; }
        public Nullable<int> HealthSafetyWorkContacts { get; set; }
        public Nullable<decimal> BehaviouralSafetyProgram { get; set; }
        public Nullable<int> QuarterlyAuditScore { get; set; }
        public Nullable<int> ToolboxMeetingsPerMonth { get; set; }
        public Nullable<int> AlcoaWeeklyContractorsMeeting { get; set; }
        public Nullable<int> AlcoaMonthlyContractorsMeeting { get; set; }
        public Nullable<int> SafetyPlan { get; set; }
        public Nullable<int> JSAScore { get; set; }
        public Nullable<int> FatalityPrevention { get; set; }
        public Nullable<decimal> TRIFR { get; set; }
        public Nullable<decimal> AIFR { get; set; }
        public Nullable<int> Training { get; set; }
        public Nullable<int> MedicalSchedule { get; set; }
        public Nullable<int> TrainingSchedule { get; set; }
    }
}
