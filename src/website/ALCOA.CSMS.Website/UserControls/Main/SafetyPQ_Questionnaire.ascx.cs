using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using System.Drawing;

public partial class UserControls_SafetyPQ_Questionnaire : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        SessionHandler.spVar_Questionnaire_FrontPageLink = "Questionnaire";
        if (!postBack)
        {

            Session["IgnoreWarning"] = "0";

            bool disableButton = false;
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    if (!Helper.General.isEhsConsultant(auth.UserId))
                    {
                        disableButton = true;
                    }
                    else
                    {
                        btn2.Enabled = true;
                    }
                    if (Helper.General.isProcurementUser(auth.UserId)) btn2.Enabled = true;
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.PreQual):
                    break;
                case ((int)RoleList.Administrator):
                    btn2.Enabled = true;
                    break;
                default:
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }

            if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
            {
                string q = "New";

                QuestionnaireService qs = new QuestionnaireService();
                DataSet qDataSet = qs.GetLastModifiedQuestionnaireId(auth.CompanyId);

                if (qDataSet.Tables[0].Rows.Count > 0)
                {
                    q = qDataSet.Tables[0].Rows[qDataSet.Tables[0].Rows.Count - 1].ItemArray[0].ToString();
                }

                Response.Redirect(String.Format("SafetyPQ_QuestionnaireModify.aspx?q={0}", q), false);
            }

            UsersProcurementService upService = new UsersProcurementService();
            UsersProcurement up = upService.GetByUserId(auth.UserId);
            if (auth.RoleId != (int)RoleList.Administrator && up != null)
            {
                if (up.Enabled == true)
                {
                    Label1.ForeColor = Color.Maroon;
                    Label1.Text = "The Work List below shows company questionnaires that you have initiated but are still incomplete.<br />Click on the 'Assign SubContractor Company Status' tab to update or review the Company Status to complete the Safety Qualification Process.";
                    Label1.Visible = true;
                }
                disableButton = false;
            }
            if (disableButton == true)
            {
                btn1.Enabled = false;
                ASPxPopupControl1.Enabled = false;
            }

        }
    }
    //Added for item #15 by Vikas
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        tbCompanyName.SelectedIndex = 1;
        tbCompanyName.Items.Clear();
        tbCompanyName.Enabled = true;
        btnCreate.Enabled = true;
        string ABN = tbAbn.Text.Replace(" ", "");
        DataSet dsCompanies = DataRepository.CompaniesRelationshipProvider.GetCompaniesByABN(ABN);
        if (dsCompanies != null)
        {
            tbCompanyName.DataSource = dsCompanies;
            tbCompanyName.TextField = "VENDOR_NAME";
            tbCompanyName.DataBind();
        }
        tbCompanyName.SelectedIndex = -1;
        //tbCompanyName.Items.Insert(0,new ListEditItem("Please Select a Company"));
        //tbCompanyName.SelectedIndex = 0;
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (!ValidationUtilities.CheckValidity.Abn(tbAbn.Text))
        {
            throw new Exception("ABN is Invalid.");
        }

        CompaniesService cService = new CompaniesService();

        //Check against Company Name
        
        Companies c = cService.GetByCompanyName(tbCompanyName.Text);
        if (c != null)
        {
            c = null;
            int cDatabase = 0;
            QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();
            qroFilters.Append(QuestionnaireReportOverviewColumn.CompanyName, tbCompanyName.Text);
            using (VList<QuestionnaireReportOverview> _cList =
                    DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 100, out cDatabase))
            {
                if (cDatabase > 0)
                {
                    string message = String.Format("A Company already exists with the ABN Number: {0}. Check Database Report", tbCompanyName.Text);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "ShowAlerttbCompanyName1", "alert('" + message + "');", true);
                    return;

                    //throw new Exception(String.Format("A Company already exists with the name: {0}. Check Database Report", tbCompanyName.Text));
                }
                else
                {
                    string message = String.Format("A Company already exists with the ABN Number: {0}. Check Database Report", tbCompanyName.Text);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "ShowAlerttbCompanyName2", "alert('" + message + "');", true);
                    return;
                    //throw new Exception(String.Format("A Company already exists with the name: {0}. Check Status Report", tbCompanyName.Text));
                }
            }
        }

        //Check against entered Company ABN
        c = cService.GetByCompanyAbn(tbAbn.Text.Replace(" ", ""));
        if (c != null)
        {
            c = null;
            int cDatabase = 0;
            QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();
            qroFilters.Append(QuestionnaireReportOverviewColumn.CompanyAbn, tbAbn.Text.Replace(" ", ""));
            using (VList<QuestionnaireReportOverview> _cList =
                DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 100, out cDatabase))
            {

                if (cDatabase > 0)
                {
                    string message = String.Format("A Company already exists with the ABN Number: {0}. Check Database Report", tbAbn.Text);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "ShowAlerttbAbn1", "alert('" + message + "');", true);
                    return;
                    //throw new Exception(String.Format("A Company already exists with the ABN Number: {0}. Check Status Report", tbAbn.Text));
                }
                else
                {
                    string message = String.Format("A Company already exists with the ABN Number: {0}. Check Status Report", tbAbn.Text);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "ShowAlerttbAbn2", "alert('" + message + "');", true);
                    return;
                    //throw new Exception(String.Format("A Company already exists with the ABN Number: {0}. Check Status Report", tbAbn.Text));
                }
            }
        }

        //If no exception thrown by now, then this is 99.99% chance a new company for input into CSMS:
        try
        {
            if (c == null)
            {
                c = new Companies();
                c.CompanyName = tbCompanyName.Text;
                c.CompanyAbn = tbAbn.Text.Replace(" ", "");
                c.CompanyStatusId = (int)CompanyStatusList.Being_Assessed;
                c.CompanyStatus2Id = (int)CompanyStatus2List.Contractor;
                cService.Save(c);

                if (c.CompanyId != 0)
                {
                    Initiate(c.CompanyId, false, false);
                }
                else
                {
                    throw new Exception("Could not create company.");
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblSave2.Text = String.Format("Error Occurred: {0}", ex.Message);
        }
    }

    //Spec#25:Commenting out entire code of Initiate button click
    //protected void btnInitiate_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (ddlCompanies.Value == null)
    //        {
    //            lblSave.Text = "Please select a company.";
    //        }
    //        else
    //        {
    //            int _CompanyId = Convert.ToInt32(ddlCompanies.Value);
    //            int count = 0;
    //            DateTime dt = DateTime.Now;
    //            dt = dt.AddDays(-305);

    //            QuestionnaireFilters qFilters = new QuestionnaireFilters();
    //            qFilters.Append(QuestionnaireColumn.CompanyId, _CompanyId.ToString());
    //            TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), null, 0, 100, out count);
    //            if (count > 0)
    //            {
    //                // 19.01.2011 Peter Wisdom requests that nobody has ability to create blank questionnaires if one exists already (including Admins).

    //                //if (auth.RoleId == (int)RoleList.Administrator)
    //                //{
    //                //    //2. Check if not 'Assessment Complete'
    //                //    foreach (Questionnaire q in qList)
    //                //    {
    //                //        if (q.Status < (int)QuestionnaireStatusList.AssessmentComplete)
    //                //        {
    //                //            throw new Exception("Could not initiate questionnaire.<br />A questionnaire already exists for this company that has not been completed.");
    //                //        }

    //                //        if (Session["IgnoreWarning"].ToString() != _CompanyId.ToString())
    //                //        {
    //                //            DateTime dt2 = (DateTime)q.ApprovedDate;
    //                //            if (q.ApprovedDate >= dt)
    //                //            {
    //                //                Session["IgnoreWarning"] = _CompanyId.ToString();
    //                //                throw new Exception(String.Format("A Safety Questionnaire was assessed for this company on {0:dd/MM/yyyy}.<br />If you are sure you wish to initiate another one and ignore this warning please click the Initiate button again.", dt2));
    //                //            }
    //                //        }
    //                //    }
    //                //}
    //                //else
    //                //{
    //                    throw new Exception("Could not initiate questionnaire.<br />A questionnaire already exists for this company.");
    //                //}
    //            }

    //            Initiate(_CompanyId, true, false);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
    //        lblSave.Text = String.Format("Error Occurred: {0}", ex.Message);
    //    } 
    //}

    protected void tbAbn_OnValidation(object sender, ValidationEventArgs e)
    {
        if (e.Value != null)
        {
            if (ValidationUtilities.CheckValidity.Abn(e.Value.ToString()))
            {
                return;
            }
        }
       
        e.IsValid = false;
        e.ErrorText = "Invalid ABN";
    }

    protected bool existsSubCQ(int CompanyId)
    {
        bool foundQ = false;
        string q = "";
        QuestionnaireService qs = new QuestionnaireService();
        DataSet qDataSet = qs.GetLastModifiedQuestionnaireId(CompanyId);

        foreach (DataRow dr in qDataSet.Tables[0].Rows)
        {
            q = dr[0].ToString();
        }

        if (!String.IsNullOrEmpty(q))
        {
            Questionnaire questionnaire = qs.GetByQuestionnaireId(Convert.ToInt32(q));
            if (questionnaire != null)
            {
                if (questionnaire.InitialStatus == (int)QuestionnaireStatusList.BeingAssessed) foundQ = true;
            }
        }

        return foundQ;
    }
    //protected void createSubCQ(int CompanyId)
    //{
    //    QuestionnaireService qs = new QuestionnaireService();
    //    using (Questionnaire qu = new Questionnaire())
    //    {
    //        qu.CreatedByUserId = auth.UserId;
    //        qu.CreatedDate = DateTime.Now;
    //        qu.CompanyId = CompanyId;
    //        qu.ModifiedByUserId = auth.UserId;
    //        qu.ModifiedDate = DateTime.Now;
    //        qu.Status = (int)QuestionnaireStatusList.Incomplete;
    //        qu.InitialStatus = (int)QuestionnaireStatusList.BeingAssessed;
    //        qu.MainStatus = (int)QuestionnaireStatusList.Incomplete;
    //        qu.IsMainRequired = true;
    //        qu.VerificationStatus = (int)QuestionnaireStatusList.Incomplete;
    //        qu.IsVerificationRequired = false;
    //        qu.ProcurementModified = false;
    //        qu.IsReQualification = false;
    //        qu.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
    //        qu.QuestionnairePresentlyWithSince = DateTime.Now;
    //        qs.Save(qu);
    //        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
    //        using (QuestionnaireInitialResponse qir9 = new QuestionnaireInitialResponse())
    //        {
    //            qir9.QuestionnaireId = qu.QuestionnaireId;
    //            qir9.QuestionId = "9";
    //            qir9.AnswerText = "Sub-Contract";
    //            qirService.Save(qir9);
    //        }
    //    }
    //}

    protected void btnCreate2_Click(object sender, EventArgs e)
    {
        if (!ValidationUtilities.CheckValidity.Abn(tbAbn2.Text))
        {
            throw new Exception("ABN is Invalid.");
        }

        CompaniesService cService = new CompaniesService();
        Companies c = cService.GetByCompanyName(tbCompanyName2.Text);
        if (c != null)
        {
            c = null;
            throw new Exception("A Company already exists with the name: " + tbCompanyName2.Text);
        }

        c = cService.GetByCompanyAbn(tbAbn2.Text.Replace(" ", ""));
        if (c != null)
        {
            c = null;
            throw new Exception("A Company already exists with the ABN Number: " + tbAbn2.Text);
        }

        //check direct company requesting sub contractor
        //int _count = 0;
        //QuestionnaireFilters qFilters = new QuestionnaireFilters();
        //qFilters.Append(QuestionnaireColumn.CompanyId, cbDirectCompany.SelectedItem.Value.ToString());
        //TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(String.Format("CompanyId = {0}", cbDirectCompany.SelectedItem.Value), "ModifiedDate DESC", 0, 9999, out _count);

        //if (!Helper.Questionnaire.General.IsValidForSubContractors(Convert.ToInt32(cbDirectCompany.SelectedItem.Value.ToString()))) throw new Exception("This Company is not authorised to engage sub contractors because they have not successfully completed the verification questionnaire process, is a InDirectly supervised company and/or completed their Safety Qualification.");

        int CompanyId = 0;
        try
        {
            c = cService.GetByCompanyName(tbCompanyName2.Text);
            if (c == null)
            {
                c = new Companies();
                c.CompanyName = tbCompanyName2.Text;
                c.CompanyAbn = tbAbn2.Text.Replace(" ", "");
                c.CompanyStatusId = (int)CompanyStatusList.Being_Assessed;
                c.CompanyStatus2Id = (int)CompanyStatus2List.SubContractor;
                cService.Save(c);
                CompanyId = c.CompanyId;

                if (CompanyId != 0)
                {
                    Initiate(CompanyId, false, true);
                }
                else
                {
                    throw new Exception("Could not create company.");
                }
            }
            else
            {
                throw new Exception(String.Format("'{0}' already exists.", tbCompanyName2.Text));
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblSave2_2.Text = String.Format("Error Occurred: {0}", ex.Message);
        }
    }

    protected void Initiate(int c, bool initiate, bool SubContractor)
    {
        try
        {
            QuestionnaireService qs = new QuestionnaireService();
            using (Questionnaire qu = new Questionnaire())
            {
                qu.CreatedByUserId = auth.UserId;
                qu.CreatedDate = DateTime.Now;
                qu.CompanyId = c;
                qu.ModifiedByUserId = auth.UserId;
                qu.ModifiedDate = DateTime.Now;
                qu.Status = (int)QuestionnaireStatusList.Incomplete;
                qu.InitialStatus = (int)QuestionnaireStatusList.Incomplete;
                qu.MainStatus = (int)QuestionnaireStatusList.Incomplete;
                qu.VerificationStatus = (int)QuestionnaireStatusList.Incomplete;
                qu.ProcurementModified = false;
                qu.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.ProcurementQuestionnaire;
                qu.QuestionnairePresentlyWithSince = DateTime.Now;
                if (SubContractor)
                    qu.SubContractor = true;
                else
                {
                    qu.IsReQualification = false;
                    qu.SubContractor = false;
                }
                qs.Save(qu);

                //Action Logging
                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementQuestionnaireCreated,
                                                        qu.QuestionnaireId,
                                                        auth.UserId,
                                                        auth.FirstName,
                                                        auth.LastName,
                                                        auth.RoleId,
                                                        auth.CompanyId,
                                                        auth.CompanyName,
                                                        null, null, null, null, null, null) == false)
                {
                    //for now we do nothing if action log fails.
                }

                //If Creator = Procurement then set them as the Procurement Contact for the questionnaire.
                UsersProcurementService upService = new UsersProcurementService();
                UsersProcurement up = upService.GetByUserId(auth.UserId);
                if (up != null)
                    if (up.Enabled)
                    {
                        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                        using (QuestionnaireInitialResponse qir = new QuestionnaireInitialResponse())
                        {
                            qir.QuestionnaireId = qu.QuestionnaireId;
                            qir.QuestionId = "2";
                            qir.AnswerText = auth.UserId.ToString();
                            qir.ModifiedByUserId = auth.UserId;
                            qir.ModifiedDate = DateTime.Now;
                            qirService.Save(qir);

                            if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.ProcurementContactAssigned,
                                                        qu.QuestionnaireId,                         //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        "Automatic Assignment",                     //Comments
                                                        "(Not Assigned)",                           //OldAssigned
                                                        auth.FirstName + " " + auth.LastName,       //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
                            {
                                //for now we do nothing if action log fails.
                            }
                        }
                    }
                Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx?q={0}", qu.QuestionnaireId), true);
            } //+ "&s=1"
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            if (!SubContractor)
            {
                if (initiate == true)
                    lblSave.Text = String.Format("Save Unsuccessful. Error occurred: {0}", ex.Message);
            }
        }
    }

    protected void tbAbn2_OnValidation(object sender, ValidationEventArgs e)
    {
        if (e.Value != null)
        {
            if (ValidationUtilities.CheckValidity.Abn(e.Value.ToString()))
            {
                return;
            }
        }

        e.IsValid = false;
        e.ErrorText = "Invalid ABN";
    }
}