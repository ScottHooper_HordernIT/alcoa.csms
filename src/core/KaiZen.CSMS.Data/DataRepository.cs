﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Web.Configuration;
using System.Web;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;

#endregion

namespace KaiZen.CSMS.Data
{
	/// <summary>
	/// This class represents the Data source repository and gives access to all the underlying providers.
	/// </summary>
	[CLSCompliant(true)]
	public sealed class DataRepository 
	{
		private static volatile NetTiersProvider _provider = null;
        private static volatile NetTiersProviderCollection _providers = null;
		private static volatile NetTiersServiceSection _section = null;
		private static volatile Configuration _config = null;
        
        private static object SyncRoot = new object();
				
		private DataRepository()
		{
		}
		
		#region Public LoadProvider
		/// <summary>
        /// Enables the DataRepository to programatically create and 
        /// pass in a <c>NetTiersProvider</c> during runtime.
        /// </summary>
        /// <param name="provider">An instatiated NetTiersProvider.</param>
        public static void LoadProvider(NetTiersProvider provider)
        {
			LoadProvider(provider, false);
        }
		
		/// <summary>
        /// Enables the DataRepository to programatically create and 
        /// pass in a <c>NetTiersProvider</c> during runtime.
        /// </summary>
        /// <param name="provider">An instatiated NetTiersProvider.</param>
        /// <param name="setAsDefault">ability to set any valid provider as the default provider for the DataRepository.</param>
		public static void LoadProvider(NetTiersProvider provider, bool setAsDefault)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            if (_providers == null)
			{
				lock(SyncRoot)
				{
            		if (_providers == null)
						_providers = new NetTiersProviderCollection();
				}
			}
			
            if (_providers[provider.Name] == null)
            {
                lock (_providers.SyncRoot)
                {
                    _providers.Add(provider);
                }
            }

            if (_provider == null || setAsDefault)
            {
                lock (SyncRoot)
                {
                    if(_provider == null || setAsDefault)
                         _provider = provider;
                }
            }
        }
		#endregion 
		
		///<summary>
		/// Configuration based provider loading, will load the providers on first call.
		///</summary>
		private static void LoadProviders()
        {
            // Avoid claiming lock if providers are already loaded
            if (_provider == null)
            {
                lock (SyncRoot)
                {
                    // Do this again to make sure _provider is still null
                    if (_provider == null)
                    {
                        // Load registered providers and point _provider to the default provider
                        _providers = new NetTiersProviderCollection();

                        ProvidersHelper.InstantiateProviders(NetTiersSection.Providers, _providers, typeof(NetTiersProvider));
						_provider = _providers[NetTiersSection.DefaultProvider];

                        if (_provider == null)
                        {
                            throw new ProviderException("Unable to load default NetTiersProvider");
                        }
                    }
                }
            }
        }

		/// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        public static NetTiersProvider Provider
        {
            get { LoadProviders(); return _provider; }
        }

		/// <summary>
        /// Gets the provider collection.
        /// </summary>
        /// <value>The providers.</value>
        public static NetTiersProviderCollection Providers
        {
            get { LoadProviders(); return _providers; }
        }
		
		/// <summary>
		/// Creates a new <see cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public TransactionManager CreateTransaction()
		{
			return _provider.CreateTransaction();
		}

		#region Configuration

		/// <summary>
		/// Gets a reference to the configured NetTiersServiceSection object.
		/// </summary>
		public static NetTiersServiceSection NetTiersSection
		{
			get
			{
				// Try to get a reference to the default <netTiersService> section
				_section = WebConfigurationManager.GetSection("netTiersService") as NetTiersServiceSection;

				if ( _section == null )
				{
					// otherwise look for section based on the assembly name
					_section = WebConfigurationManager.GetSection("KaiZen.CSMS.Data") as NetTiersServiceSection;
				}

				#region Design-Time Support

				if ( _section == null )
				{
					// lastly, try to find the specific NetTiersServiceSection for this assembly
					foreach ( ConfigurationSection temp in Configuration.Sections )
					{
						if ( temp is NetTiersServiceSection )
						{
							_section = temp as NetTiersServiceSection;
							break;
						}
					}
				}

				#endregion Design-Time Support
				
				if ( _section == null )
				{
					throw new ProviderException("Unable to load NetTiersServiceSection");
				}

				return _section;
			}
		}

		#region Design-Time Support

		/// <summary>
		/// Gets a reference to the application configuration object.
		/// </summary>
		public static Configuration Configuration
		{
			get
			{
				if ( _config == null )
				{
					// load specific config file
					if ( HttpContext.Current != null )
					{
						_config = WebConfigurationManager.OpenWebConfiguration("~");
					}
					else
					{
						String configFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile.Replace(".config", "").Replace(".temp", "");

						// check for design mode
						if ( configFile.ToLower().Contains("devenv.exe") )
						{
							_config = GetDesignTimeConfig();
						}
						else
						{
							_config = ConfigurationManager.OpenExeConfiguration(configFile);
						}
					}
				}

				return _config;
			}
		}

		private static Configuration GetDesignTimeConfig()
		{
			ExeConfigurationFileMap configMap = null;
			Configuration config = null;
			String path = null;

			// Get an instance of the currently running Visual Studio IDE.
			EnvDTE80.DTE2 dte = (EnvDTE80.DTE2) System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE.10.0");
			
			if ( dte != null )
			{
				dte.SuppressUI = true;

				EnvDTE.ProjectItem item = dte.Solution.FindProjectItem("web.config");
				if ( item != null )
				{
					if (!item.ContainingProject.FullName.ToLower().StartsWith("http:"))
               {
                  System.IO.FileInfo info = new System.IO.FileInfo(item.ContainingProject.FullName);
                  path = String.Format("{0}\\{1}", info.Directory.FullName, item.Name);
                  configMap = new ExeConfigurationFileMap();
                  configMap.ExeConfigFilename = path;
               }
               else
               {
                  configMap = new ExeConfigurationFileMap();
                  configMap.ExeConfigFilename = item.get_FileNames(0);
               }}

				/*
				Array projects = (Array) dte2.ActiveSolutionProjects;
				EnvDTE.Project project = (EnvDTE.Project) projects.GetValue(0);
				System.IO.FileInfo info;

				foreach ( EnvDTE.ProjectItem item in project.ProjectItems )
				{
					if ( String.Compare(item.Name, "web.config", true) == 0 )
					{
						info = new System.IO.FileInfo(project.FullName);
						path = String.Format("{0}\\{1}", info.Directory.FullName, item.Name);
						configMap = new ExeConfigurationFileMap();
						configMap.ExeConfigFilename = path;
						break;
					}
				}
				*/
			}

			config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
			return config;
		}

		#endregion Design-Time Support

		#endregion Configuration

		#region Connections

		/// <summary>
		/// Gets a reference to the ConnectionStringSettings collection.
		/// </summary>
		public static ConnectionStringSettingsCollection ConnectionStrings
		{
			get
			{
					// use default ConnectionStrings if _section has already been discovered
					if ( _config == null && _section != null )
					{
						return WebConfigurationManager.ConnectionStrings;
					}
					
					return Configuration.ConnectionStrings.ConnectionStrings;
			}
		}

		// dictionary of connection providers
		private static Dictionary<String, ConnectionProvider> _connections;

		/// <summary>
		/// Gets the dictionary of connection providers.
		/// </summary>
		public static Dictionary<String, ConnectionProvider> Connections
		{
			get
			{
				if ( _connections == null )
				{
					lock (SyncRoot)
                	{
						if (_connections == null)
						{
							_connections = new Dictionary<String, ConnectionProvider>();
		
							// add a connection provider for each configured connection string
							foreach ( ConnectionStringSettings conn in ConnectionStrings )
							{
								_connections.Add(conn.Name, new ConnectionProvider(conn.Name, conn.ConnectionString));
							}
						}
					}
				}

				return _connections;
			}
		}

		/// <summary>
		/// Adds the specified connection string to the map of connection strings.
		/// </summary>
		/// <param name="connectionStringName">The connection string name.</param>
		/// <param name="connectionString">The provider specific connection information.</param>
		public static void AddConnection(String connectionStringName, String connectionString)
		{
			lock (SyncRoot)
            {
				Connections.Remove(connectionStringName);
				ConnectionProvider connection = new ConnectionProvider(connectionStringName, connectionString);
				Connections.Add(connectionStringName, connection);
			}
		}

		/// <summary>
		/// Provides ability to switch connection string at runtime.
		/// </summary>
		public sealed class ConnectionProvider
		{
			private NetTiersProvider _provider;
			private NetTiersProviderCollection _providers;
			private String _connectionStringName;
			private String _connectionString;


			/// <summary>
			/// Initializes a new instance of the ConnectionProvider class.
			/// </summary>
			/// <param name="connectionStringName">The connection string name.</param>
			/// <param name="connectionString">The provider specific connection information.</param>
			public ConnectionProvider(String connectionStringName, String connectionString)
			{
				_connectionString = connectionString;
				_connectionStringName = connectionStringName;
			}

			/// <summary>
			/// Gets the provider.
			/// </summary>
			public NetTiersProvider Provider
			{
				get { LoadProviders(); return _provider; }
			}

			/// <summary>
			/// Gets the provider collection.
			/// </summary>
			public NetTiersProviderCollection Providers
			{
				get { LoadProviders(); return _providers; }
			}

			/// <summary>
			/// Instantiates the configured providers based on the supplied connection string.
			/// </summary>
			private void LoadProviders()
			{
				DataRepository.LoadProviders();

				// Avoid claiming lock if providers are already loaded
				if ( _providers == null )
				{
					lock ( SyncRoot )
					{
						// Do this again to make sure _provider is still null
						if ( _providers == null )
						{
							// apply connection information to each provider
							for ( int i = 0; i < NetTiersSection.Providers.Count; i++ )
							{
								NetTiersSection.Providers[i].Parameters["connectionStringName"] = _connectionStringName;
								// remove previous connection string, if any
								NetTiersSection.Providers[i].Parameters.Remove("connectionString");

								if ( !String.IsNullOrEmpty(_connectionString) )
								{
									NetTiersSection.Providers[i].Parameters["connectionString"] = _connectionString;
								}
							}

							// Load registered providers and point _provider to the default provider
							_providers = new NetTiersProviderCollection();

							ProvidersHelper.InstantiateProviders(NetTiersSection.Providers, _providers, typeof(NetTiersProvider));
							_provider = _providers[NetTiersSection.DefaultProvider];
						}
					}
				}
			}
		}

		#endregion Connections

		#region Static properties
		
		#region QuestionnaireMainAnswerProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainAnswerProviderBase QuestionnaireMainAnswerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainAnswerProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireServicesCategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireServicesCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireServicesCategoryProviderBase QuestionnaireServicesCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireServicesCategoryProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationAssessmentAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationAssessmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationAssessmentAuditProviderBase QuestionnaireVerificationAssessmentAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationAssessmentAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireTypeProviderBase QuestionnaireTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireTypeProvider;
			}
		}
		
		#endregion
		
		#region RoleProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Role"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RoleProviderBase RoleProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RoleProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireStatusProviderBase QuestionnaireStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireStatusProvider;
			}
		}
		
		#endregion
		
		#region CompanyStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanyStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanyStatusProviderBase CompanyStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanyStatusProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireServicesSelectedAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireServicesSelectedAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireServicesSelectedAuditProviderBase QuestionnaireServicesSelectedAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireServicesSelectedAuditProvider;
			}
		}
		
		#endregion
		
		#region CompanyStatus2Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanyStatus2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanyStatus2ProviderBase CompanyStatus2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanyStatus2Provider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationAttachmentAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationAttachmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationAttachmentAuditProviderBase QuestionnaireVerificationAttachmentAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationAttachmentAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainQuestionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainQuestionProviderBase QuestionnaireMainQuestionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainQuestionProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationRationaleProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationRationale"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationRationaleProviderBase QuestionnaireVerificationRationaleProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationRationaleProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Questionnaire"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireProviderBase QuestionnaireProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireServicesSelectedProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireServicesSelected"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireServicesSelectedProviderBase QuestionnaireServicesSelectedProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireServicesSelectedProvider;
			}
		}
		
		#endregion
		
		#region ResidentialProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Residential"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ResidentialProviderBase ResidentialProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ResidentialProvider;
			}
		}
		
		#endregion
		
		#region AccessLogsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AccessLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AccessLogsProviderBase AccessLogsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AccessLogsProvider;
			}
		}
		
		#endregion
		
		#region QuestionnairePresentlyWithUsersProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithUsers"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnairePresentlyWithUsersProviderBase QuestionnairePresentlyWithUsersProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnairePresentlyWithUsersProvider;
			}
		}
		
		#endregion
		
		#region RegionsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Regions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RegionsProviderBase RegionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RegionsProvider;
			}
		}
		
		#endregion
		
		#region CompaniesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Companies"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesProviderBase CompaniesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesProvider;
			}
		}
		
		#endregion
		
		#region UsersProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Users"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProviderBase UsersProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationAssessmentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationAssessment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationAssessmentProviderBase QuestionnaireVerificationAssessmentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationAssessmentProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationResponseProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationResponseProviderBase QuestionnaireVerificationResponseProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationResponseProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationResponseAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationResponseAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationResponseAuditProviderBase QuestionnaireVerificationResponseAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationResponseAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationAttachmentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationAttachment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationAttachmentProviderBase QuestionnaireVerificationAttachmentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationAttachmentProvider;
			}
		}
		
		#endregion
		
		#region RegionsSitesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RegionsSites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RegionsSitesProviderBase RegionsSitesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RegionsSitesProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireVerificationSectionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireVerificationSection"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireVerificationSectionProviderBase QuestionnaireVerificationSectionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireVerificationSectionProvider;
			}
		}
		
		#endregion
		
		#region QuestionnairePresentlyWithMetricProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithMetric"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnairePresentlyWithMetricProviderBase QuestionnairePresentlyWithMetricProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnairePresentlyWithMetricProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialContactStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialContactStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialContactStatusProviderBase QuestionnaireInitialContactStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialContactStatusProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialLocationProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialLocation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialLocationProviderBase QuestionnaireInitialLocationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialLocationProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialLocationAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialLocationAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialLocationAuditProviderBase QuestionnaireInitialLocationAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialLocationAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialContactProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialContactProviderBase QuestionnaireInitialContactProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialContactProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialContactEmailTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialContactEmailType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialContactEmailTypeProviderBase QuestionnaireInitialContactEmailTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialContactEmailTypeProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireActionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireAction"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireActionProviderBase QuestionnaireActionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireActionProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialContactEmailProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialContactEmail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialContactEmailProviderBase QuestionnaireInitialContactEmailProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialContactEmailProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialResponseProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialResponseProviderBase QuestionnaireInitialResponseProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialResponseProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialContactAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialContactAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialContactAuditProviderBase QuestionnaireInitialContactAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialContactAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainResponseProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainResponseProviderBase QuestionnaireMainResponseProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainResponseProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireInitialResponseAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireInitialResponseAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireInitialResponseAuditProviderBase QuestionnaireInitialResponseAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireInitialResponseAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainResponseAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainResponseAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainResponseAuditProviderBase QuestionnaireMainResponseAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainResponseAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainRationaleProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainRationale"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainRationaleProviderBase QuestionnaireMainRationaleProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainRationaleProvider;
			}
		}
		
		#endregion
		
		#region QuestionnairePresentlyWithActionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithAction"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnairePresentlyWithActionProviderBase QuestionnairePresentlyWithActionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnairePresentlyWithActionProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainAssessmentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainAssessment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainAssessmentProviderBase QuestionnaireMainAssessmentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainAssessmentProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainAttachmentAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainAttachmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainAttachmentAuditProviderBase QuestionnaireMainAttachmentAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainAttachmentAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainAssessmentAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainAssessmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainAssessmentAuditProviderBase QuestionnaireMainAssessmentAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainAssessmentAuditProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireContractorRsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireContractorRs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireContractorRsProviderBase QuestionnaireContractorRsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireContractorRsProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainAttachmentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainAttachment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainAttachmentProviderBase QuestionnaireMainAttachmentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainAttachmentProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit18Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit18"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit18ProviderBase TwentyOnePointAudit18Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit18Provider;
			}
		}
		
		#endregion
		
		#region SafetyPlansSeAnswersProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SafetyPlansSeAnswers"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SafetyPlansSeAnswersProviderBase SafetyPlansSeAnswersProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SafetyPlansSeAnswersProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit19Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit19"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit19ProviderBase TwentyOnePointAudit19Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit19Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit17Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit17"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit17ProviderBase TwentyOnePointAudit17Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit17Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit20Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit20"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit20ProviderBase TwentyOnePointAudit20Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit20Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit13Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit13"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit13ProviderBase TwentyOnePointAudit13Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit13Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit16Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit16"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit16ProviderBase TwentyOnePointAudit16Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit16Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit14Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit14"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit14ProviderBase TwentyOnePointAudit14Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit14Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit21Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit21"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit21ProviderBase TwentyOnePointAudit21Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit21Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit15Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit15"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit15ProviderBase TwentyOnePointAudit15Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit15Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAuditQtrProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAuditQtr"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAuditQtrProviderBase TwentyOnePointAuditQtrProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAuditQtrProvider;
			}
		}
		
		#endregion
		
		#region UsersPrivileged3Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersPrivileged3"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersPrivileged3ProviderBase UsersPrivileged3Provider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersPrivileged3Provider;
			}
		}
		
		#endregion
		
		#region UserPrivilegeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UserPrivilege"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UserPrivilegeProviderBase UserPrivilegeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UserPrivilegeProvider;
			}
		}
		
		#endregion
		
		#region UsersProcurementProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersProcurement"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProcurementProviderBase UsersProcurementProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProcurementProvider;
			}
		}
		
		#endregion
		
		#region UsersPrivileged2Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersPrivileged2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersPrivileged2ProviderBase UsersPrivileged2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersPrivileged2Provider;
			}
		}
		
		#endregion
		
		#region UsersProcurementAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersProcurementAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProcurementAuditProviderBase UsersProcurementAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProcurementAuditProvider;
			}
		}
		
		#endregion
		
		#region UsersAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersAuditProviderBase UsersAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersAuditProvider;
			}
		}
		
		#endregion
		
		#region UsersPrivilegedProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersPrivileged"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersPrivilegedProviderBase UsersPrivilegedProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersPrivilegedProvider;
			}
		}
		
		#endregion
		
		#region UsersEbiProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersEbi"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersEbiProviderBase UsersEbiProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersEbiProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit12Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit12"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit12ProviderBase TwentyOnePointAudit12Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit12Provider;
			}
		}
		
		#endregion
		
		#region UsersEbiAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersEbiAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersEbiAuditProviderBase UsersEbiAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersEbiAuditProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit11Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit11"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit11ProviderBase TwentyOnePointAudit11Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit11Provider;
			}
		}
		
		#endregion
		
		#region SitesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Sites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SitesProviderBase SitesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SitesProvider;
			}
		}
		
		#endregion
		
		#region SafetyPlansSeQuestionsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SafetyPlansSeQuestions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SafetyPlansSeQuestionsProviderBase SafetyPlansSeQuestionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SafetyPlansSeQuestionsProvider;
			}
		}
		
		#endregion
		
		#region TemplateProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Template"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TemplateProviderBase TemplateProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TemplateProvider;
			}
		}
		
		#endregion
		
		#region SystemLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SystemLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SystemLogProviderBase SystemLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SystemLogProvider;
			}
		}
		
		#endregion
		
		#region TemplateTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TemplateType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TemplateTypeProviderBase TemplateTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TemplateTypeProvider;
			}
		}
		
		#endregion
		
		#region SqExemptionAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SqExemptionAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SqExemptionAuditProviderBase SqExemptionAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SqExemptionAuditProvider;
			}
		}
		
		#endregion
		
		#region SafetyPlansSeResponsesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SafetyPlansSeResponses"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SafetyPlansSeResponsesProviderBase SafetyPlansSeResponsesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SafetyPlansSeResponsesProvider;
			}
		}
		
		#endregion
		
		#region SqExemptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SqExemption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SqExemptionProviderBase SqExemptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SqExemptionProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit10Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit10"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit10ProviderBase TwentyOnePointAudit10Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit10Provider;
			}
		}
		
		#endregion
		
		#region SafetyPlanStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SafetyPlanStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SafetyPlanStatusProviderBase SafetyPlanStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SafetyPlanStatusProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit07Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit07"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit07ProviderBase TwentyOnePointAudit07Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit07Provider;
			}
		}
		
		#endregion
		
		#region QuestionnaireCommentsAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireCommentsAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireCommentsAuditProviderBase QuestionnaireCommentsAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireCommentsAuditProvider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit08Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit08"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit08ProviderBase TwentyOnePointAudit08Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit08Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit01Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit01"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit01ProviderBase TwentyOnePointAudit01Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit01Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit09Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit09"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit09ProviderBase TwentyOnePointAudit09Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit09Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit02Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit02"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit02ProviderBase TwentyOnePointAudit02Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit02Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit06Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit06"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit06ProviderBase TwentyOnePointAudit06Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit06Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit05Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit05"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit05ProviderBase TwentyOnePointAudit05Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit05Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit04Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit04"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit04ProviderBase TwentyOnePointAudit04Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit04Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAudit03Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit03"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAudit03ProviderBase TwentyOnePointAudit03Provider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAudit03Provider;
			}
		}
		
		#endregion
		
		#region TwentyOnePointAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TwentyOnePointAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TwentyOnePointAuditProviderBase TwentyOnePointAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TwentyOnePointAuditProvider;
			}
		}
		
		#endregion
		
		#region ConfigNavigationProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ConfigNavigation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigNavigationProviderBase ConfigNavigationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigNavigationProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireCommentsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireComments"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireCommentsProviderBase QuestionnaireCommentsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireCommentsProvider;
			}
		}
		
		#endregion
		
		#region ConfigSa812Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ConfigSa812"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigSa812ProviderBase ConfigSa812Provider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigSa812Provider;
			}
		}
		
		#endregion
		
		#region ConfigProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Config"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigProviderBase ConfigProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigProvider;
			}
		}
		
		#endregion
		
		#region ConfigTextTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ConfigTextType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigTextTypeProviderBase ConfigTextTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigTextTypeProvider;
			}
		}
		
		#endregion
		
		#region CompanyStatusChangeApprovalProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanyStatusChangeApproval"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanyStatusChangeApprovalProviderBase CompanyStatusChangeApprovalProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanyStatusChangeApprovalProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryExceptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryException"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryExceptionProviderBase CompanySiteCategoryExceptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryExceptionProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryStandardAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryStandardAuditProviderBase CompanySiteCategoryStandardAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryStandardAuditProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryException2Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryException2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryException2ProviderBase CompanySiteCategoryException2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryException2Provider;
			}
		}
		
		#endregion
		
		#region ConfigTextProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ConfigText"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigTextProviderBase ConfigTextProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigTextProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryStandardProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryStandard"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryStandardProviderBase CompanySiteCategoryStandardProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryStandardProvider;
			}
		}
		
		#endregion
		
		#region CsmsEmailLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsEmailLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsEmailLogProviderBase CsmsEmailLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsEmailLogProvider;
			}
		}
		
		#endregion
		
		#region ConfigText2Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ConfigText2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigText2ProviderBase ConfigText2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigText2Provider;
			}
		}
		
		#endregion
		
		#region CsaProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Csa"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsaProviderBase CsaProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsaProvider;
			}
		}
		
		#endregion
		
		#region CsmsFileTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsFileType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsFileTypeProviderBase CsmsFileTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsFileTypeProvider;
			}
		}
		
		#endregion
		
		#region CsmsEmailLogRecipientProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsEmailLogRecipient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsEmailLogRecipientProviderBase CsmsEmailLogRecipientProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsEmailLogRecipientProvider;
			}
		}
		
		#endregion
		
		#region CsmsAccessProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsAccess"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsAccessProviderBase CsmsAccessProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsAccessProvider;
			}
		}
		
		#endregion
		
		#region ContactsAlcoaProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ContactsAlcoa"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ContactsAlcoaProviderBase ContactsAlcoaProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ContactsAlcoaProvider;
			}
		}
		
		#endregion
		
		#region CsaAnswersProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsaAnswers"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsaAnswersProviderBase CsaAnswersProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsaAnswersProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryProviderBase CompanySiteCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryProvider;
			}
		}
		
		#endregion
		
		#region ContactsContractorsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ContactsContractors"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ContactsContractorsProviderBase ContactsContractorsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ContactsContractorsProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailTemplateProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailTemplate"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailTemplateProviderBase AdminTaskEmailTemplateProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailTemplateProvider;
			}
		}
		
		#endregion
		
		#region CompaniesRelationshipProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesRelationship"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesRelationshipProviderBase CompaniesRelationshipProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesRelationshipProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailRecipientProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailRecipient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailRecipientProviderBase AdminTaskEmailRecipientProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailRecipientProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskSourceProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskSource"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskSourceProviderBase AdminTaskSourceProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskSourceProvider;
			}
		}
		
		#endregion
		
		#region AdHocRadarProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdHocRadar"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdHocRadarProviderBase AdHocRadarProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdHocRadarProvider;
			}
		}
		
		#endregion
		
		#region AdHocRadarItems2Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdHocRadarItems2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdHocRadarItems2ProviderBase AdHocRadarItems2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.AdHocRadarItems2Provider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailTemplateRecipientProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateRecipient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailTemplateRecipientProviderBase AdminTaskEmailTemplateRecipientProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailTemplateRecipientProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskStatusProviderBase AdminTaskStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskStatusProvider;
			}
		}
		
		#endregion
		
		#region AdHocRadarItemsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdHocRadarItems"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdHocRadarItemsProviderBase AdHocRadarItemsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdHocRadarItemsProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskTypeProviderBase AdminTaskTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskTypeProvider;
			}
		}
		
		#endregion
		
		#region AnnualTargetsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AnnualTargets"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AnnualTargetsProviderBase AnnualTargetsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AnnualTargetsProvider;
			}
		}
		
		#endregion
		
		#region CompaniesHrMapProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesHrMap"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesHrMapProviderBase CompaniesHrMapProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesHrMapProvider;
			}
		}
		
		#endregion
		
		#region CompaniesEhsimsMapProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesEhsimsMap"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesEhsimsMapProviderBase CompaniesEhsimsMapProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesEhsimsMapProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTask"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskProviderBase AdminTaskProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskProvider;
			}
		}
		
		#endregion
		
		#region CompaniesHrCrpDataProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesHrCrpData"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesHrCrpDataProviderBase CompaniesHrCrpDataProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesHrCrpDataProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailLogProviderBase AdminTaskEmailLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailLogProvider;
			}
		}
		
		#endregion
		
		#region ApssLogsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ApssLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ApssLogsProviderBase ApssLogsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ApssLogsProvider;
			}
		}
		
		#endregion
		
		#region CompaniesAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesAuditProviderBase CompaniesAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesAuditProvider;
			}
		}
		
		#endregion
		
		#region AuditFileVaultProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AuditFileVault"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AuditFileVaultProviderBase AuditFileVaultProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AuditFileVaultProvider;
			}
		}
		
		#endregion
		
		#region CsmsEmailRecipientTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsEmailRecipientType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsEmailRecipientTypeProviderBase CsmsEmailRecipientTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsEmailRecipientTypeProvider;
			}
		}
		
		#endregion
		
		#region AuditFileVaultTableProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AuditFileVaultTable"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AuditFileVaultTableProviderBase AuditFileVaultTableProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AuditFileVaultTableProvider;
			}
		}
		
		#endregion
		
		#region KpiProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Kpi"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiProviderBase KpiProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiProvider;
			}
		}
		
		#endregion
		
		#region CsmsFileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsFile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsFileProviderBase CsmsFileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsFileProvider;
			}
		}
		
		#endregion
		
		#region FileVaultSubCategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultSubCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultSubCategoryProviderBase FileVaultSubCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultSubCategoryProvider;
			}
		}
		
		#endregion
		
		#region KpiAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiAuditProviderBase KpiAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiAuditProvider;
			}
		}
		
		#endregion
		
		#region FileVaultCategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultCategoryProviderBase FileVaultCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultCategoryProvider;
			}
		}
		
		#endregion
		
		#region FileDbMedicalTrainingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileDbMedicalTraining"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileDbMedicalTrainingProviderBase FileDbMedicalTrainingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileDbMedicalTrainingProvider;
			}
		}
		
		#endregion
		
		#region FileVaultAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultAuditProviderBase FileVaultAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultAuditProvider;
			}
		}
		
		#endregion
		
		#region FileVaultProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVault"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultProviderBase FileVaultProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultProvider;
			}
		}
		
		#endregion
		
		#region KpiProjectListProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiProjectList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiProjectListProviderBase KpiProjectListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiProjectListProvider;
			}
		}
		
		#endregion
		
		#region FileVaultTableProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultTable"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultTableProviderBase FileVaultTableProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultTableProvider;
			}
		}
		
		#endregion
		
		#region KpiProjectsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiProjects"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiProjectsProviderBase KpiProjectsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiProjectsProvider;
			}
		}
		
		#endregion
		
		#region PrivilegeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Privilege"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PrivilegeProviderBase PrivilegeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PrivilegeProvider;
			}
		}
		
		#endregion
		
		#region KpiPurchaseOrderListProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiPurchaseOrderList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiPurchaseOrderListProviderBase KpiPurchaseOrderListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiPurchaseOrderListProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireActionLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireActionLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireActionLogProviderBase QuestionnaireActionLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireActionLogProvider;
			}
		}
		
		#endregion
		
		#region PermissionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Permission"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PermissionProviderBase PermissionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PermissionProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireAuditProviderBase QuestionnaireAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireAuditProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailTemplateAttachmentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateAttachment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailTemplateAttachmentProviderBase AdminTaskEmailTemplateAttachmentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailTemplateAttachmentProvider;
			}
		}
		
		#endregion
		
		#region News2Provider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="News2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static News2ProviderBase News2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.News2Provider;
			}
		}
		
		#endregion
		
		#region MonthsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Months"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MonthsProviderBase MonthsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MonthsProvider;
			}
		}
		
		#endregion
		
		#region FileDbAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileDbAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileDbAuditProviderBase FileDbAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileDbAuditProvider;
			}
		}
		
		#endregion
		
		#region NewsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="News"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static NewsProviderBase NewsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.NewsProvider;
			}
		}
		
		#endregion
		
		#region FileDbAdminProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileDbAdmin"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileDbAdminProviderBase FileDbAdminProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileDbAdminProvider;
			}
		}
		
		#endregion
		
		#region EhsConsultantProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EhsConsultant"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EhsConsultantProviderBase EhsConsultantProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EhsConsultantProvider;
			}
		}
		
		#endregion
		
		#region EbiIssueProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EbiIssue"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiIssueProviderBase EbiIssueProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiIssueProvider;
			}
		}
		
		#endregion
		
		#region EbiDailyReportProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EbiDailyReport"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiDailyReportProviderBase EbiDailyReportProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiDailyReportProvider;
			}
		}
		
		#endregion
		
		#region EbiMetricProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EbiMetric"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiMetricProviderBase EbiMetricProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiMetricProvider;
			}
		}
		
		#endregion
		
		#region CustomReportingLayoutsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CustomReportingLayouts"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CustomReportingLayoutsProviderBase CustomReportingLayoutsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CustomReportingLayoutsProvider;
			}
		}
		
		#endregion
		
		#region EbiProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Ebi"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiProviderBase EbiProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiProvider;
			}
		}
		
		#endregion
		
		#region DocumentsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Documents"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DocumentsProviderBase DocumentsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DocumentsProvider;
			}
		}
		
		#endregion
		
		#region EhsimsExceptionsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EhsimsExceptions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EhsimsExceptionsProviderBase EhsimsExceptionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EhsimsExceptionsProvider;
			}
		}
		
		#endregion
		
		#region DocumentsDownloadLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="DocumentsDownloadLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DocumentsDownloadLogProviderBase DocumentsDownloadLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DocumentsDownloadLogProvider;
			}
		}
		
		#endregion
		
		#region ElmahErrorProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ElmahError"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ElmahErrorProviderBase ElmahErrorProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ElmahErrorProvider;
			}
		}
		
		#endregion
		
		#region EscalationChainSafetyRolesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EscalationChainSafetyRoles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EscalationChainSafetyRolesProviderBase EscalationChainSafetyRolesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EscalationChainSafetyRolesProvider;
			}
		}
		
		#endregion
		
		#region EmailLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EmailLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EmailLogProviderBase EmailLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EmailLogProvider;
			}
		}
		
		#endregion
		
		#region FileDbProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileDb"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileDbProviderBase FileDbProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileDbProvider;
			}
		}
		
		#endregion
		
		#region EscalationChainSafetyProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EscalationChainSafety"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EscalationChainSafetyProviderBase EscalationChainSafetyProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EscalationChainSafetyProvider;
			}
		}
		
		#endregion
		
		#region EscalationChainProcurementRolesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EscalationChainProcurementRoles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EscalationChainProcurementRolesProviderBase EscalationChainProcurementRolesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EscalationChainProcurementRolesProvider;
			}
		}
		
		#endregion
		
		#region EmailLogTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EmailLogType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EmailLogTypeProviderBase EmailLogTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EmailLogTypeProvider;
			}
		}
		
		#endregion
		
		#region EscalationChainProcurementProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EscalationChainProcurement"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EscalationChainProcurementProviderBase EscalationChainProcurementProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EscalationChainProcurementProvider;
			}
		}
		
		#endregion
		
		#region EnumApprovalProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EnumApproval"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EnumApprovalProviderBase EnumApprovalProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EnumApprovalProvider;
			}
		}
		
		#endregion
		
		#region YearlyTargetsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="YearlyTargets"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static YearlyTargetsProviderBase YearlyTargetsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.YearlyTargetsProvider;
			}
		}
		
		#endregion
		
		#region EnumQuestionnaireRiskRatingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EnumQuestionnaireRiskRating"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EnumQuestionnaireRiskRatingProviderBase EnumQuestionnaireRiskRatingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EnumQuestionnaireRiskRatingProvider;
			}
		}
		
		#endregion
		
		
		#region CurrentCompaniesEhsConsultantIdProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CurrentCompaniesEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrentCompaniesEhsConsultantIdProviderBase CurrentCompaniesEhsConsultantIdProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrentCompaniesEhsConsultantIdProvider;
			}
		}
		
		#endregion
		
		#region CurrentQuestionnaireProcurementContactProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CurrentQuestionnaireProcurementContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrentQuestionnaireProcurementContactProviderBase CurrentQuestionnaireProcurementContactProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrentQuestionnaireProcurementContactProvider;
			}
		}
		
		#endregion
		
		#region CurrentQuestionnaireProcurementFunctionalManagerProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrentQuestionnaireProcurementFunctionalManagerProviderBase CurrentQuestionnaireProcurementFunctionalManagerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrentQuestionnaireProcurementFunctionalManagerProvider;
			}
		}
		
		#endregion
		
		#region CurrentSmpEhsConsultantIdProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CurrentSmpEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrentSmpEhsConsultantIdProviderBase CurrentSmpEhsConsultantIdProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrentSmpEhsConsultantIdProvider;
			}
		}
		
		#endregion
		
		#region OrphanCompaniesEhsConsultantIdProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrphanCompaniesEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrphanCompaniesEhsConsultantIdProviderBase OrphanCompaniesEhsConsultantIdProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrphanCompaniesEhsConsultantIdProvider;
			}
		}
		
		#endregion
		
		#region OrphanQuestionnaireProcurementContactProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrphanQuestionnaireProcurementContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrphanQuestionnaireProcurementContactProviderBase OrphanQuestionnaireProcurementContactProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrphanQuestionnaireProcurementContactProvider;
			}
		}
		
		#endregion
		
		#region OrphanQuestionnaireProcurementFunctionalManagerProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrphanQuestionnaireProcurementFunctionalManagerProviderBase OrphanQuestionnaireProcurementFunctionalManagerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrphanQuestionnaireProcurementFunctionalManagerProvider;
			}
		}
		
		#endregion
		
		#region OrphanSmpEhsConsultantIdProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrphanSmpEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrphanSmpEhsConsultantIdProviderBase OrphanSmpEhsConsultantIdProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrphanSmpEhsConsultantIdProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailRecipientsListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailRecipientsList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailRecipientsListProviderBase AdminTaskEmailRecipientsListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailRecipientsListProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailTemplateListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailTemplateListProviderBase AdminTaskEmailTemplateListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailTemplateListProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskEmailTemplateListWithAttachmentsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateListWithAttachments"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskEmailTemplateListWithAttachmentsProviderBase AdminTaskEmailTemplateListWithAttachmentsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskEmailTemplateListWithAttachmentsProvider;
			}
		}
		
		#endregion
		
		#region AdminTaskHistoryProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AdminTaskHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AdminTaskHistoryProviderBase AdminTaskHistoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AdminTaskHistoryProvider;
			}
		}
		
		#endregion
		
		#region CompaniesActiveProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesActive"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesActiveProviderBase CompaniesActiveProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesActiveProvider;
			}
		}
		
		#endregion
		
		#region CompaniesEhsConsultantsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesEhsConsultants"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesEhsConsultantsProviderBase CompaniesEhsConsultantsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesEhsConsultantsProvider;
			}
		}
		
		#endregion
		
		#region CompaniesEhsimsMapSitesEhsimsIhsListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesEhsimsMapSitesEhsimsIhsListProviderBase CompaniesEhsimsMapSitesEhsimsIhsListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesEhsimsMapSitesEhsimsIhsListProvider;
			}
		}
		
		#endregion
		
		#region CompaniesHrCrpDataListAllProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompaniesHrCrpDataListAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompaniesHrCrpDataListAllProviderBase CompaniesHrCrpDataListAllProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompaniesHrCrpDataListAllProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryStandardDetailsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardDetails"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryStandardDetailsProviderBase CompanySiteCategoryStandardDetailsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryStandardDetailsProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryStandardNamesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardNames"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryStandardNamesProviderBase CompanySiteCategoryStandardNamesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryStandardNamesProvider;
			}
		}
		
		#endregion
		
		#region CompanySiteCategoryStandardRegionProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardRegion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CompanySiteCategoryStandardRegionProviderBase CompanySiteCategoryStandardRegionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CompanySiteCategoryStandardRegionProvider;
			}
		}
		
		#endregion
		
		#region ConfigSa812SitesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ConfigSa812Sites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ConfigSa812SitesProviderBase ConfigSa812SitesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ConfigSa812SitesProvider;
			}
		}
		
		#endregion
		
		#region CsaCompanySiteCategoryProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsaCompanySiteCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsaCompanySiteCategoryProviderBase CsaCompanySiteCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsaCompanySiteCategoryProvider;
			}
		}
		
		#endregion
		
		#region CsmsEmailLogListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CsmsEmailLogList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CsmsEmailLogListProviderBase CsmsEmailLogListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CsmsEmailLogListProvider;
			}
		}
		
		#endregion
		
		#region EbiLastTimeOnSiteProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EbiLastTimeOnSite"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiLastTimeOnSiteProviderBase EbiLastTimeOnSiteProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiLastTimeOnSiteProvider;
			}
		}
		
		#endregion
		
		#region EbiLastTimeOnSiteDistinctProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EbiLastTimeOnSiteDistinct"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiLastTimeOnSiteDistinctProviderBase EbiLastTimeOnSiteDistinctProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiLastTimeOnSiteDistinctProvider;
			}
		}
		
		#endregion
		
		#region EbiLastTimeOnSiteDistinctAnySiteProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EbiLastTimeOnSiteDistinctAnySite"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EbiLastTimeOnSiteDistinctAnySiteProviderBase EbiLastTimeOnSiteDistinctAnySiteProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EbiLastTimeOnSiteDistinctAnySiteProvider;
			}
		}
		
		#endregion
		
		#region EhsimsExceptionsCompaniesSitesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EhsimsExceptionsCompaniesSites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EhsimsExceptionsCompaniesSitesProviderBase EhsimsExceptionsCompaniesSitesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EhsimsExceptionsCompaniesSitesProvider;
			}
		}
		
		#endregion
		
		#region EmailLogAutoProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EmailLogAuto"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EmailLogAutoProviderBase EmailLogAutoProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EmailLogAutoProvider;
			}
		}
		
		#endregion
		
		#region EmailLogIhsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EmailLogIhs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EmailLogIhsProviderBase EmailLogIhsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EmailLogIhsProvider;
			}
		}
		
		#endregion
		
		#region EnumQuestionnaireMainAnswerProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EnumQuestionnaireMainAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EnumQuestionnaireMainAnswerProviderBase EnumQuestionnaireMainAnswerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EnumQuestionnaireMainAnswerProvider;
			}
		}
		
		#endregion
		
		#region EnumQuestionnaireMainQuestionProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EnumQuestionnaireMainQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EnumQuestionnaireMainQuestionProviderBase EnumQuestionnaireMainQuestionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EnumQuestionnaireMainQuestionProvider;
			}
		}
		
		#endregion
		
		#region EnumQuestionnaireMainQuestionAnswerProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="EnumQuestionnaireMainQuestionAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static EnumQuestionnaireMainQuestionAnswerProviderBase EnumQuestionnaireMainQuestionAnswerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.EnumQuestionnaireMainQuestionAnswerProvider;
			}
		}
		
		#endregion
		
		#region FileDbUsersCompaniesSitesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileDbUsersCompaniesSites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileDbUsersCompaniesSitesProviderBase FileDbUsersCompaniesSitesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileDbUsersCompaniesSitesProvider;
			}
		}
		
		#endregion
		
		#region FileVaultListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultListProviderBase FileVaultListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultListProvider;
			}
		}
		
		#endregion
		
		#region FileVaultListHelpFilesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultListHelpFiles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultListHelpFilesProviderBase FileVaultListHelpFilesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultListHelpFilesProvider;
			}
		}
		
		#endregion
		
		#region FileVaultListSqExemptionProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultListSqExemption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultListSqExemptionProviderBase FileVaultListSqExemptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultListSqExemptionProvider;
			}
		}
		
		#endregion
		
		#region FileVaultTableListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultTableList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultTableListProviderBase FileVaultTableListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultTableListProvider;
			}
		}
		
		#endregion
		
		#region FileVaultTableListHelpFilesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FileVaultTableListHelpFiles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FileVaultTableListHelpFilesProviderBase FileVaultTableListHelpFilesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FileVaultTableListHelpFilesProvider;
			}
		}
		
		#endregion
		
		#region KpiCompanySiteCategoryProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiCompanySiteCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiCompanySiteCategoryProviderBase KpiCompanySiteCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiCompanySiteCategoryProvider;
			}
		}
		
		#endregion
		
		#region KpiEngineeringProjectHoursProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiEngineeringProjectHours"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiEngineeringProjectHoursProviderBase KpiEngineeringProjectHoursProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiEngineeringProjectHoursProvider;
			}
		}
		
		#endregion
		
		#region KpiListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiListProviderBase KpiListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiListProvider;
			}
		}
		
		#endregion
		
		#region KpiProjectListOpenProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiProjectListOpen"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiProjectListOpenProviderBase KpiProjectListOpenProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiProjectListOpenProvider;
			}
		}
		
		#endregion
		
		#region KpiPurchaseOrderListOpenProjectsAscProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiPurchaseOrderListOpenProjectsAscProviderBase KpiPurchaseOrderListOpenProjectsAscProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiPurchaseOrderListOpenProjectsAscProvider;
			}
		}
		
		#endregion
		
		#region KpiPurchaseOrderListOpenProjectsDistinctAscProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static KpiPurchaseOrderListOpenProjectsDistinctAscProviderBase KpiPurchaseOrderListOpenProjectsDistinctAscProvider
		{
			get 
			{
				LoadProviders();
				return _provider.KpiPurchaseOrderListOpenProjectsDistinctAscProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireActionLogFriendlyProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireActionLogFriendly"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireActionLogFriendlyProviderBase QuestionnaireActionLogFriendlyProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireActionLogFriendlyProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireContactProcurementProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireContactProcurement"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireContactProcurementProviderBase QuestionnaireContactProcurementProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireContactProcurementProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireEscalationHsAssessorListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireEscalationHsAssessorList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireEscalationHsAssessorListProviderBase QuestionnaireEscalationHsAssessorListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireEscalationHsAssessorListProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireEscalationProcurementListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireEscalationProcurementList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireEscalationProcurementListProviderBase QuestionnaireEscalationProcurementListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireEscalationProcurementListProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireMainQuestionAnswerProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireMainQuestionAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireMainQuestionAnswerProviderBase QuestionnaireMainQuestionAnswerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireMainQuestionAnswerProvider;
			}
		}
		
		#endregion
		
		#region QuestionnairePresentlyWithUsersActionsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithUsersActions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnairePresentlyWithUsersActionsProviderBase QuestionnairePresentlyWithUsersActionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnairePresentlyWithUsersActionsProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportDaysSinceActivityProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportDaysSinceActivity"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportDaysSinceActivityProviderBase QuestionnaireReportDaysSinceActivityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportDaysSinceActivityProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportExpiryProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportExpiry"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportExpiryProviderBase QuestionnaireReportExpiryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportExpiryProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportOverviewProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportOverview"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportOverviewProviderBase QuestionnaireReportOverviewProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportOverviewProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportOverviewSubQuery1Provider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubQuery1"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportOverviewSubQuery1ProviderBase QuestionnaireReportOverviewSubQuery1Provider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportOverviewSubQuery1Provider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportOverviewSubQuery2Provider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubQuery2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportOverviewSubQuery2ProviderBase QuestionnaireReportOverviewSubQuery2Provider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportOverviewSubQuery2Provider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportOverviewSubQuery3Provider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubQuery3"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportOverviewSubQuery3ProviderBase QuestionnaireReportOverviewSubQuery3Provider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportOverviewSubQuery3Provider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportOverviewSubContractorsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubContractors"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportOverviewSubContractorsProviderBase QuestionnaireReportOverviewSubContractorsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportOverviewSubContractorsProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportProgressProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportProgress"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportProgressProviderBase QuestionnaireReportProgressProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportProgressProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportServicesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportServices"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportServicesProviderBase QuestionnaireReportServicesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportServicesProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportServicesAllProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportServicesAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportServicesAllProviderBase QuestionnaireReportServicesAllProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportServicesAllProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportServicesListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportServicesList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportServicesListProviderBase QuestionnaireReportServicesListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportServicesListProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportServicesListAllProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportServicesListAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportServicesListAllProviderBase QuestionnaireReportServicesListAllProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportServicesListAllProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportServicesOtherProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportServicesOther"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportServicesOtherProviderBase QuestionnaireReportServicesOtherProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportServicesOtherProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportServicesOtherAllProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportServicesOtherAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportServicesOtherAllProviderBase QuestionnaireReportServicesOtherAllProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportServicesOtherAllProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportTimeDelayAssessmentCompleteProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportTimeDelayAssessmentCompleteProviderBase QuestionnaireReportTimeDelayAssessmentCompleteProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportTimeDelayAssessmentCompleteProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportTimeDelayBeingAssessedProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportTimeDelayBeingAssessedProviderBase QuestionnaireReportTimeDelayBeingAssessedProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportTimeDelayBeingAssessedProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireReportTimeDelayWithSupplierProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireReportTimeDelayWithSupplier"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireReportTimeDelayWithSupplierProviderBase QuestionnaireReportTimeDelayWithSupplierProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireReportTimeDelayWithSupplierProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireWithLocationApprovalViewProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireWithLocationApprovalView"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireWithLocationApprovalViewProviderBase QuestionnaireWithLocationApprovalViewProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireWithLocationApprovalViewProvider;
			}
		}
		
		#endregion
		
		#region QuestionnaireWithLocationApprovalViewLatestProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="QuestionnaireWithLocationApprovalViewLatest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static QuestionnaireWithLocationApprovalViewLatestProviderBase QuestionnaireWithLocationApprovalViewLatestProvider
		{
			get 
			{
				LoadProviders();
				return _provider.QuestionnaireWithLocationApprovalViewLatestProvider;
			}
		}
		
		#endregion
		
		#region SitesEhsimsIhsListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SitesEhsimsIhsList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SitesEhsimsIhsListProviderBase SitesEhsimsIhsListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SitesEhsimsIhsListProvider;
			}
		}
		
		#endregion
		
		#region SqExemptionFriendlyAllProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SqExemptionFriendlyAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SqExemptionFriendlyAllProviderBase SqExemptionFriendlyAllProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SqExemptionFriendlyAllProvider;
			}
		}
		
		#endregion
		
		#region SqExemptionFriendlyCurrentProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SqExemptionFriendlyCurrent"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SqExemptionFriendlyCurrentProviderBase SqExemptionFriendlyCurrentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SqExemptionFriendlyCurrentProvider;
			}
		}
		
		#endregion
		
		#region SqExemptionFriendlyExpiredProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SqExemptionFriendlyExpired"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SqExemptionFriendlyExpiredProviderBase SqExemptionFriendlyExpiredProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SqExemptionFriendlyExpiredProvider;
			}
		}
		
		#endregion
		
		#region UsersAlcoanProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersAlcoan"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersAlcoanProviderBase UsersAlcoanProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersAlcoanProvider;
			}
		}
		
		#endregion
		
		#region UsersCompaniesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersCompanies"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersCompaniesProviderBase UsersCompaniesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersCompaniesProvider;
			}
		}
		
		#endregion
		
		#region UsersCompaniesActiveAllProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersCompaniesActiveAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersCompaniesActiveAllProviderBase UsersCompaniesActiveAllProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersCompaniesActiveAllProvider;
			}
		}
		
		#endregion
		
		#region UsersCompaniesActiveContractorsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersCompaniesActiveContractors"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersCompaniesActiveContractorsProviderBase UsersCompaniesActiveContractorsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersCompaniesActiveContractorsProvider;
			}
		}
		
		#endregion
		
		#region UsersEhsConsultantsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersEhsConsultants"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersEhsConsultantsProviderBase UsersEhsConsultantsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersEhsConsultantsProvider;
			}
		}
		
		#endregion
		
		#region UsersFullNameProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersFullName"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersFullNameProviderBase UsersFullNameProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersFullNameProvider;
			}
		}
		
		#endregion
		
		#region UsersHsAssessorListWithLocationProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersHsAssessorListWithLocation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersHsAssessorListWithLocationProviderBase UsersHsAssessorListWithLocationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersHsAssessorListWithLocationProvider;
			}
		}
		
		#endregion
		
		#region UsersProcurementEscalationListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersProcurementEscalationList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProcurementEscalationListProviderBase UsersProcurementEscalationListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProcurementEscalationListProvider;
			}
		}
		
		#endregion
		
		#region UsersProcurementListProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersProcurementList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProcurementListProviderBase UsersProcurementListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProcurementListProvider;
			}
		}
		
		#endregion
		
		#region UsersProcurementListWithLocationProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UsersProcurementListWithLocation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UsersProcurementListWithLocationProviderBase UsersProcurementListWithLocationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UsersProcurementListWithLocationProvider;
			}
		}
		
		#endregion



        #region KpiHelpFilesProvider

        ///<summary>
        /// Gets the current instance of the Data Access Logic Component for the <see cref="FileVault"/> business entity.
        /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
        ///</summary>
        public static KpiHelpFilesProviderBase KpiHelpFilesProvider
        {
            get
            {
                LoadProviders();
                return _provider.KpiHelpFilesProvider;
            }
        }


        #endregion


        #region ContractsReviewRegister
        public static ContractReviewsRegisterProviderBase ContractReviewsRegisterProvider
        {
            get
            {
                LoadProviders();
                return _provider.ContractReviewsRegisterProvider;
            }
        }
        #endregion

        #region UsersCompaniesMapProvider

        ///<summary>
        /// Gets the current instance of the Data Access Logic Component for the <see cref="UsersCompanies"/> business entity.
        /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
        ///</summary>
        public static UsersCompaniesMapProviderBase UsersCompaniesMapProvider
        {
            get
            {
                LoadProviders();
                return _provider.UsersCompaniesMapProvider;
            }
        }

        #endregion


        #region UserCompany
        public static UserCompanyProviderBase UserCompanyProvider
        {
            get
            {
                LoadProviders();
                return _provider.UserCompanyProvider;
            }
        }
        #endregion

        #region CompanyNoteProvider

        ///<summary>
        /// Gets the current instance of the Data Access Logic Component for the <see cref="CompanyNote"/> business entity.
        /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
        ///</summary>
        public static CompanyNoteProviderBase CompanyNoteProvider
        {
            get
            {
                LoadProviders();
                return _provider.CompanyNoteProvider;
            }
        }

        #endregion



		#endregion
	}
	
	#region Query/Filters
		
	#region QuestionnaireMainAnswerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerFilters : QuestionnaireMainAnswerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerFilters class.
		/// </summary>
		public QuestionnaireMainAnswerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAnswerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAnswerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAnswerFilters
	
	#region QuestionnaireMainAnswerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainAnswerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerQuery : QuestionnaireMainAnswerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerQuery class.
		/// </summary>
		public QuestionnaireMainAnswerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAnswerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAnswerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAnswerQuery
		
	#region QuestionnaireServicesCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesCategoryFilters : QuestionnaireServicesCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryFilters class.
		/// </summary>
		public QuestionnaireServicesCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesCategoryFilters
	
	#region QuestionnaireServicesCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireServicesCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesCategoryQuery : QuestionnaireServicesCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryQuery class.
		/// </summary>
		public QuestionnaireServicesCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesCategoryQuery
		
	#region QuestionnaireVerificationAssessmentAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditFilters : QuestionnaireVerificationAssessmentAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditFilters class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentAuditFilters
	
	#region QuestionnaireVerificationAssessmentAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationAssessmentAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditQuery : QuestionnaireVerificationAssessmentAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditQuery class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentAuditQuery
		
	#region QuestionnaireTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeFilters : QuestionnaireTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeFilters class.
		/// </summary>
		public QuestionnaireTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireTypeFilters
	
	#region QuestionnaireTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeQuery : QuestionnaireTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeQuery class.
		/// </summary>
		public QuestionnaireTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireTypeQuery
		
	#region RoleFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Role"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RoleFilters : RoleFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RoleFilters class.
		/// </summary>
		public RoleFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RoleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RoleFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RoleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RoleFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RoleFilters
	
	#region RoleQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RoleParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Role"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RoleQuery : RoleParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RoleQuery class.
		/// </summary>
		public RoleQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RoleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RoleQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RoleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RoleQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RoleQuery
		
	#region QuestionnaireStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireStatusFilters : QuestionnaireStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusFilters class.
		/// </summary>
		public QuestionnaireStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireStatusFilters
	
	#region QuestionnaireStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireStatusQuery : QuestionnaireStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusQuery class.
		/// </summary>
		public QuestionnaireStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireStatusQuery
		
	#region CompanyStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusFilters : CompanyStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusFilters class.
		/// </summary>
		public CompanyStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusFilters
	
	#region CompanyStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanyStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanyStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusQuery : CompanyStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusQuery class.
		/// </summary>
		public CompanyStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusQuery
		
	#region QuestionnaireServicesSelectedAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelectedAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedAuditFilters : QuestionnaireServicesSelectedAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditFilters class.
		/// </summary>
		public QuestionnaireServicesSelectedAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesSelectedAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesSelectedAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesSelectedAuditFilters
	
	#region QuestionnaireServicesSelectedAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireServicesSelectedAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelectedAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedAuditQuery : QuestionnaireServicesSelectedAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditQuery class.
		/// </summary>
		public QuestionnaireServicesSelectedAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesSelectedAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesSelectedAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesSelectedAuditQuery
		
	#region CompanyStatus2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2Filters : CompanyStatus2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2Filters class.
		/// </summary>
		public CompanyStatus2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatus2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatus2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatus2Filters
	
	#region CompanyStatus2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanyStatus2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2Query : CompanyStatus2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2Query class.
		/// </summary>
		public CompanyStatus2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatus2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatus2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatus2Query
		
	#region QuestionnaireVerificationAttachmentAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentAuditFilters : QuestionnaireVerificationAttachmentAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentAuditFilters class.
		/// </summary>
		public QuestionnaireVerificationAttachmentAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAttachmentAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAttachmentAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAttachmentAuditFilters
	
	#region QuestionnaireVerificationAttachmentAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationAttachmentAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentAuditQuery : QuestionnaireVerificationAttachmentAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentAuditQuery class.
		/// </summary>
		public QuestionnaireVerificationAttachmentAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAttachmentAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAttachmentAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAttachmentAuditQuery
		
	#region QuestionnaireMainQuestionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionFilters : QuestionnaireMainQuestionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionFilters class.
		/// </summary>
		public QuestionnaireMainQuestionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionFilters
	
	#region QuestionnaireMainQuestionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainQuestionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionQuery : QuestionnaireMainQuestionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionQuery class.
		/// </summary>
		public QuestionnaireMainQuestionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionQuery
		
	#region QuestionnaireVerificationRationaleFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleFilters : QuestionnaireVerificationRationaleFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleFilters class.
		/// </summary>
		public QuestionnaireVerificationRationaleFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationRationaleFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationRationaleFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationRationaleFilters
	
	#region QuestionnaireVerificationRationaleQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationRationaleParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleQuery : QuestionnaireVerificationRationaleParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleQuery class.
		/// </summary>
		public QuestionnaireVerificationRationaleQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationRationaleQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationRationaleQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationRationaleQuery
		
	#region QuestionnaireFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireFilters : QuestionnaireFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireFilters class.
		/// </summary>
		public QuestionnaireFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireFilters
	
	#region QuestionnaireQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireQuery : QuestionnaireParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireQuery class.
		/// </summary>
		public QuestionnaireQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireQuery
		
	#region QuestionnaireServicesSelectedFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedFilters : QuestionnaireServicesSelectedFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedFilters class.
		/// </summary>
		public QuestionnaireServicesSelectedFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesSelectedFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesSelectedFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesSelectedFilters
	
	#region QuestionnaireServicesSelectedQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireServicesSelectedParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedQuery : QuestionnaireServicesSelectedParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedQuery class.
		/// </summary>
		public QuestionnaireServicesSelectedQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireServicesSelectedQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireServicesSelectedQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireServicesSelectedQuery
		
	#region ResidentialFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Residential"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResidentialFilters : ResidentialFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ResidentialFilters class.
		/// </summary>
		public ResidentialFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ResidentialFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ResidentialFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ResidentialFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ResidentialFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ResidentialFilters
	
	#region ResidentialQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ResidentialParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Residential"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResidentialQuery : ResidentialParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ResidentialQuery class.
		/// </summary>
		public ResidentialQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ResidentialQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ResidentialQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ResidentialQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ResidentialQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ResidentialQuery
		
	#region AccessLogsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccessLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccessLogsFilters : AccessLogsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccessLogsFilters class.
		/// </summary>
		public AccessLogsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccessLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccessLogsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccessLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccessLogsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccessLogsFilters
	
	#region AccessLogsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AccessLogsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AccessLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccessLogsQuery : AccessLogsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccessLogsQuery class.
		/// </summary>
		public AccessLogsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccessLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccessLogsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccessLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccessLogsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccessLogsQuery
		
	#region QuestionnairePresentlyWithUsersFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersFilters : QuestionnairePresentlyWithUsersFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersFilters class.
		/// </summary>
		public QuestionnairePresentlyWithUsersFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersFilters
	
	#region QuestionnairePresentlyWithUsersQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnairePresentlyWithUsersParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersQuery : QuestionnairePresentlyWithUsersParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersQuery class.
		/// </summary>
		public QuestionnairePresentlyWithUsersQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersQuery
		
	#region RegionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsFilters : RegionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsFilters class.
		/// </summary>
		public RegionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsFilters
	
	#region RegionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RegionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsQuery : RegionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsQuery class.
		/// </summary>
		public RegionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsQuery
		
	#region CompaniesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesFilters : CompaniesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesFilters class.
		/// </summary>
		public CompaniesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesFilters
	
	#region CompaniesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesQuery : CompaniesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesQuery class.
		/// </summary>
		public CompaniesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesQuery
		
	#region UsersFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFilters : UsersFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFilters class.
		/// </summary>
		public UsersFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersFilters
	
	#region UsersQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Users"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersQuery : UsersParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersQuery class.
		/// </summary>
		public UsersQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersQuery
		
	#region QuestionnaireVerificationAssessmentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentFilters : QuestionnaireVerificationAssessmentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentFilters class.
		/// </summary>
		public QuestionnaireVerificationAssessmentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentFilters
	
	#region QuestionnaireVerificationAssessmentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationAssessmentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentQuery : QuestionnaireVerificationAssessmentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentQuery class.
		/// </summary>
		public QuestionnaireVerificationAssessmentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentQuery
		
	#region QuestionnaireVerificationResponseFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseFilters : QuestionnaireVerificationResponseFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseFilters class.
		/// </summary>
		public QuestionnaireVerificationResponseFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationResponseFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationResponseFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationResponseFilters
	
	#region QuestionnaireVerificationResponseQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationResponseParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseQuery : QuestionnaireVerificationResponseParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseQuery class.
		/// </summary>
		public QuestionnaireVerificationResponseQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationResponseQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationResponseQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationResponseQuery
		
	#region QuestionnaireVerificationResponseAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditFilters : QuestionnaireVerificationResponseAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditFilters class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationResponseAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationResponseAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationResponseAuditFilters
	
	#region QuestionnaireVerificationResponseAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationResponseAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseAuditQuery : QuestionnaireVerificationResponseAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditQuery class.
		/// </summary>
		public QuestionnaireVerificationResponseAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationResponseAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationResponseAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationResponseAuditQuery
		
	#region QuestionnaireVerificationAttachmentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentFilters : QuestionnaireVerificationAttachmentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentFilters class.
		/// </summary>
		public QuestionnaireVerificationAttachmentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAttachmentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAttachmentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAttachmentFilters
	
	#region QuestionnaireVerificationAttachmentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationAttachmentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentQuery : QuestionnaireVerificationAttachmentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentQuery class.
		/// </summary>
		public QuestionnaireVerificationAttachmentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAttachmentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAttachmentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAttachmentQuery
		
	#region RegionsSitesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesFilters : RegionsSitesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesFilters class.
		/// </summary>
		public RegionsSitesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsSitesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsSitesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsSitesFilters
	
	#region RegionsSitesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RegionsSitesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesQuery : RegionsSitesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesQuery class.
		/// </summary>
		public RegionsSitesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsSitesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsSitesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsSitesQuery
		
	#region QuestionnaireVerificationSectionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionFilters : QuestionnaireVerificationSectionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionFilters class.
		/// </summary>
		public QuestionnaireVerificationSectionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationSectionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationSectionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationSectionFilters
	
	#region QuestionnaireVerificationSectionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireVerificationSectionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationSection"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationSectionQuery : QuestionnaireVerificationSectionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionQuery class.
		/// </summary>
		public QuestionnaireVerificationSectionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationSectionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationSectionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationSectionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationSectionQuery
		
	#region QuestionnairePresentlyWithMetricFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricFilters : QuestionnairePresentlyWithMetricFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricFilters class.
		/// </summary>
		public QuestionnairePresentlyWithMetricFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithMetricFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithMetricFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithMetricFilters
	
	#region QuestionnairePresentlyWithMetricQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnairePresentlyWithMetricParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricQuery : QuestionnairePresentlyWithMetricParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricQuery class.
		/// </summary>
		public QuestionnairePresentlyWithMetricQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithMetricQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithMetricQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithMetricQuery
		
	#region QuestionnaireInitialContactStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusFilters : QuestionnaireInitialContactStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusFilters class.
		/// </summary>
		public QuestionnaireInitialContactStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactStatusFilters
	
	#region QuestionnaireInitialContactStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialContactStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusQuery : QuestionnaireInitialContactStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusQuery class.
		/// </summary>
		public QuestionnaireInitialContactStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactStatusQuery
		
	#region QuestionnaireInitialLocationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationFilters : QuestionnaireInitialLocationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationFilters class.
		/// </summary>
		public QuestionnaireInitialLocationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialLocationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialLocationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialLocationFilters
	
	#region QuestionnaireInitialLocationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialLocationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationQuery : QuestionnaireInitialLocationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationQuery class.
		/// </summary>
		public QuestionnaireInitialLocationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialLocationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialLocationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialLocationQuery
		
	#region QuestionnaireInitialLocationAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationAuditFilters : QuestionnaireInitialLocationAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditFilters class.
		/// </summary>
		public QuestionnaireInitialLocationAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialLocationAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialLocationAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialLocationAuditFilters
	
	#region QuestionnaireInitialLocationAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialLocationAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationAuditQuery : QuestionnaireInitialLocationAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditQuery class.
		/// </summary>
		public QuestionnaireInitialLocationAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialLocationAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialLocationAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialLocationAuditQuery
		
	#region QuestionnaireInitialContactFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactFilters : QuestionnaireInitialContactFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactFilters class.
		/// </summary>
		public QuestionnaireInitialContactFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactFilters
	
	#region QuestionnaireInitialContactQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialContactParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactQuery : QuestionnaireInitialContactParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactQuery class.
		/// </summary>
		public QuestionnaireInitialContactQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactQuery
		
	#region QuestionnaireInitialContactEmailTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeFilters : QuestionnaireInitialContactEmailTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeFilters class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailTypeFilters
	
	#region QuestionnaireInitialContactEmailTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialContactEmailTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeQuery : QuestionnaireInitialContactEmailTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeQuery class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailTypeQuery
		
	#region QuestionnaireActionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionFilters : QuestionnaireActionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionFilters class.
		/// </summary>
		public QuestionnaireActionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionFilters
	
	#region QuestionnaireActionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireActionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionQuery : QuestionnaireActionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionQuery class.
		/// </summary>
		public QuestionnaireActionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionQuery
		
	#region QuestionnaireInitialContactEmailFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailFilters : QuestionnaireInitialContactEmailFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailFilters class.
		/// </summary>
		public QuestionnaireInitialContactEmailFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailFilters
	
	#region QuestionnaireInitialContactEmailQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialContactEmailParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailQuery : QuestionnaireInitialContactEmailParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailQuery class.
		/// </summary>
		public QuestionnaireInitialContactEmailQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailQuery
		
	#region QuestionnaireInitialResponseFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseFilters : QuestionnaireInitialResponseFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseFilters class.
		/// </summary>
		public QuestionnaireInitialResponseFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialResponseFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialResponseFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialResponseFilters
	
	#region QuestionnaireInitialResponseQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialResponseParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseQuery : QuestionnaireInitialResponseParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseQuery class.
		/// </summary>
		public QuestionnaireInitialResponseQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialResponseQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialResponseQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialResponseQuery
		
	#region QuestionnaireInitialContactAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditFilters : QuestionnaireInitialContactAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditFilters class.
		/// </summary>
		public QuestionnaireInitialContactAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactAuditFilters
	
	#region QuestionnaireInitialContactAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialContactAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditQuery : QuestionnaireInitialContactAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditQuery class.
		/// </summary>
		public QuestionnaireInitialContactAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactAuditQuery
		
	#region QuestionnaireMainResponseFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseFilters : QuestionnaireMainResponseFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseFilters class.
		/// </summary>
		public QuestionnaireMainResponseFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainResponseFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainResponseFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainResponseFilters
	
	#region QuestionnaireMainResponseQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainResponseParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseQuery : QuestionnaireMainResponseParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseQuery class.
		/// </summary>
		public QuestionnaireMainResponseQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainResponseQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainResponseQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainResponseQuery
		
	#region QuestionnaireInitialResponseAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseAuditFilters : QuestionnaireInitialResponseAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditFilters class.
		/// </summary>
		public QuestionnaireInitialResponseAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialResponseAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialResponseAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialResponseAuditFilters
	
	#region QuestionnaireInitialResponseAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireInitialResponseAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialResponseAuditQuery : QuestionnaireInitialResponseAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditQuery class.
		/// </summary>
		public QuestionnaireInitialResponseAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialResponseAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialResponseAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialResponseAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialResponseAuditQuery
		
	#region QuestionnaireMainResponseAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseAuditFilters : QuestionnaireMainResponseAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditFilters class.
		/// </summary>
		public QuestionnaireMainResponseAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainResponseAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainResponseAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainResponseAuditFilters
	
	#region QuestionnaireMainResponseAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainResponseAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponseAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseAuditQuery : QuestionnaireMainResponseAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditQuery class.
		/// </summary>
		public QuestionnaireMainResponseAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainResponseAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainResponseAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainResponseAuditQuery
		
	#region QuestionnaireMainRationaleFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleFilters : QuestionnaireMainRationaleFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleFilters class.
		/// </summary>
		public QuestionnaireMainRationaleFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainRationaleFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainRationaleFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainRationaleFilters
	
	#region QuestionnaireMainRationaleQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainRationaleParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleQuery : QuestionnaireMainRationaleParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleQuery class.
		/// </summary>
		public QuestionnaireMainRationaleQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainRationaleQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainRationaleQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainRationaleQuery
		
	#region QuestionnairePresentlyWithActionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionFilters : QuestionnairePresentlyWithActionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionFilters class.
		/// </summary>
		public QuestionnairePresentlyWithActionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithActionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithActionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithActionFilters
	
	#region QuestionnairePresentlyWithActionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnairePresentlyWithActionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionQuery : QuestionnairePresentlyWithActionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionQuery class.
		/// </summary>
		public QuestionnairePresentlyWithActionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithActionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithActionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithActionQuery
		
	#region QuestionnaireMainAssessmentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentFilters : QuestionnaireMainAssessmentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentFilters class.
		/// </summary>
		public QuestionnaireMainAssessmentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAssessmentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAssessmentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAssessmentFilters
	
	#region QuestionnaireMainAssessmentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainAssessmentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentQuery : QuestionnaireMainAssessmentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentQuery class.
		/// </summary>
		public QuestionnaireMainAssessmentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAssessmentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAssessmentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAssessmentQuery
		
	#region QuestionnaireMainAttachmentAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentAuditFilters : QuestionnaireMainAttachmentAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditFilters class.
		/// </summary>
		public QuestionnaireMainAttachmentAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentAuditFilters
	
	#region QuestionnaireMainAttachmentAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainAttachmentAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentAuditQuery : QuestionnaireMainAttachmentAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditQuery class.
		/// </summary>
		public QuestionnaireMainAttachmentAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentAuditQuery
		
	#region QuestionnaireMainAssessmentAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentAuditFilters : QuestionnaireMainAssessmentAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditFilters class.
		/// </summary>
		public QuestionnaireMainAssessmentAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAssessmentAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAssessmentAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAssessmentAuditFilters
	
	#region QuestionnaireMainAssessmentAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainAssessmentAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAssessmentAuditQuery : QuestionnaireMainAssessmentAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditQuery class.
		/// </summary>
		public QuestionnaireMainAssessmentAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAssessmentAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAssessmentAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAssessmentAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAssessmentAuditQuery
		
	#region QuestionnaireContractorRsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsFilters : QuestionnaireContractorRsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsFilters class.
		/// </summary>
		public QuestionnaireContractorRsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContractorRsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContractorRsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContractorRsFilters
	
	#region QuestionnaireContractorRsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireContractorRsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsQuery : QuestionnaireContractorRsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsQuery class.
		/// </summary>
		public QuestionnaireContractorRsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContractorRsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContractorRsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContractorRsQuery
		
	#region QuestionnaireMainAttachmentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentFilters : QuestionnaireMainAttachmentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentFilters class.
		/// </summary>
		public QuestionnaireMainAttachmentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentFilters
	
	#region QuestionnaireMainAttachmentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainAttachmentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentQuery : QuestionnaireMainAttachmentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentQuery class.
		/// </summary>
		public QuestionnaireMainAttachmentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentQuery
		
	#region TwentyOnePointAudit18Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18Filters : TwentyOnePointAudit18FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Filters class.
		/// </summary>
		public TwentyOnePointAudit18Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit18Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit18Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit18Filters
	
	#region TwentyOnePointAudit18Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit18ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit18"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit18Query : TwentyOnePointAudit18ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Query class.
		/// </summary>
		public TwentyOnePointAudit18Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit18Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit18Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit18Query
		
	#region SafetyPlansSeAnswersFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersFilters : SafetyPlansSeAnswersFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersFilters class.
		/// </summary>
		public SafetyPlansSeAnswersFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeAnswersFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeAnswersFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeAnswersFilters
	
	#region SafetyPlansSeAnswersQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SafetyPlansSeAnswersParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeAnswersQuery : SafetyPlansSeAnswersParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersQuery class.
		/// </summary>
		public SafetyPlansSeAnswersQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeAnswersQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeAnswersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeAnswersQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeAnswersQuery
		
	#region TwentyOnePointAudit19Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit19"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit19Filters : TwentyOnePointAudit19FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19Filters class.
		/// </summary>
		public TwentyOnePointAudit19Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit19Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit19Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit19Filters
	
	#region TwentyOnePointAudit19Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit19ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit19"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit19Query : TwentyOnePointAudit19ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19Query class.
		/// </summary>
		public TwentyOnePointAudit19Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit19Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit19Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit19Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit19Query
		
	#region TwentyOnePointAudit17Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17Filters : TwentyOnePointAudit17FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17Filters class.
		/// </summary>
		public TwentyOnePointAudit17Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit17Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit17Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit17Filters
	
	#region TwentyOnePointAudit17Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit17ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17Query : TwentyOnePointAudit17ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17Query class.
		/// </summary>
		public TwentyOnePointAudit17Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit17Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit17Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit17Query
		
	#region TwentyOnePointAudit20Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit20"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit20Filters : TwentyOnePointAudit20FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20Filters class.
		/// </summary>
		public TwentyOnePointAudit20Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit20Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit20Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit20Filters
	
	#region TwentyOnePointAudit20Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit20ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit20"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit20Query : TwentyOnePointAudit20ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20Query class.
		/// </summary>
		public TwentyOnePointAudit20Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit20Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit20Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit20Query
		
	#region TwentyOnePointAudit13Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit13"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit13Filters : TwentyOnePointAudit13FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13Filters class.
		/// </summary>
		public TwentyOnePointAudit13Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit13Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit13Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit13Filters
	
	#region TwentyOnePointAudit13Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit13ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit13"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit13Query : TwentyOnePointAudit13ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13Query class.
		/// </summary>
		public TwentyOnePointAudit13Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit13Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit13Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit13Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit13Query
		
	#region TwentyOnePointAudit16Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit16"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit16Filters : TwentyOnePointAudit16FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16Filters class.
		/// </summary>
		public TwentyOnePointAudit16Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit16Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit16Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit16Filters
	
	#region TwentyOnePointAudit16Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit16ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit16"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit16Query : TwentyOnePointAudit16ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16Query class.
		/// </summary>
		public TwentyOnePointAudit16Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit16Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit16Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit16Query
		
	#region TwentyOnePointAudit14Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit14"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit14Filters : TwentyOnePointAudit14FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14Filters class.
		/// </summary>
		public TwentyOnePointAudit14Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit14Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit14Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit14Filters
	
	#region TwentyOnePointAudit14Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit14ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit14"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit14Query : TwentyOnePointAudit14ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14Query class.
		/// </summary>
		public TwentyOnePointAudit14Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit14Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit14Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit14Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit14Query
		
	#region TwentyOnePointAudit21Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit21"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit21Filters : TwentyOnePointAudit21FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit21Filters class.
		/// </summary>
		public TwentyOnePointAudit21Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit21Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit21Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit21Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit21Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit21Filters
	
	#region TwentyOnePointAudit21Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit21ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit21"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit21Query : TwentyOnePointAudit21ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit21Query class.
		/// </summary>
		public TwentyOnePointAudit21Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit21Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit21Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit21Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit21Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit21Query
		
	#region TwentyOnePointAudit15Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15Filters : TwentyOnePointAudit15FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15Filters class.
		/// </summary>
		public TwentyOnePointAudit15Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit15Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit15Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit15Filters
	
	#region TwentyOnePointAudit15Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit15ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit15"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit15Query : TwentyOnePointAudit15ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15Query class.
		/// </summary>
		public TwentyOnePointAudit15Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit15Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit15Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit15Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit15Query
		
	#region TwentyOnePointAuditQtrFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrFilters : TwentyOnePointAuditQtrFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrFilters class.
		/// </summary>
		public TwentyOnePointAuditQtrFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditQtrFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditQtrFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditQtrFilters
	
	#region TwentyOnePointAuditQtrQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAuditQtrParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrQuery : TwentyOnePointAuditQtrParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrQuery class.
		/// </summary>
		public TwentyOnePointAuditQtrQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditQtrQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditQtrQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditQtrQuery
		
	#region UsersPrivileged3Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged3Filters : UsersPrivileged3FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3Filters class.
		/// </summary>
		public UsersPrivileged3Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersPrivileged3Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersPrivileged3Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersPrivileged3Filters
	
	#region UsersPrivileged3Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersPrivileged3ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged3Query : UsersPrivileged3ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3Query class.
		/// </summary>
		public UsersPrivileged3Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersPrivileged3Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged3Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersPrivileged3Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersPrivileged3Query
		
	#region UserPrivilegeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeFilters : UserPrivilegeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeFilters class.
		/// </summary>
		public UserPrivilegeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserPrivilegeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserPrivilegeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserPrivilegeFilters
	
	#region UserPrivilegeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UserPrivilegeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeQuery : UserPrivilegeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeQuery class.
		/// </summary>
		public UserPrivilegeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserPrivilegeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserPrivilegeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserPrivilegeQuery
		
	#region UsersProcurementFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementFilters : UsersProcurementFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementFilters class.
		/// </summary>
		public UsersProcurementFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementFilters
	
	#region UsersProcurementQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersProcurementParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementQuery : UsersProcurementParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementQuery class.
		/// </summary>
		public UsersProcurementQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementQuery
		
	#region UsersPrivileged2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged2Filters : UsersPrivileged2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2Filters class.
		/// </summary>
		public UsersPrivileged2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersPrivileged2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersPrivileged2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersPrivileged2Filters
	
	#region UsersPrivileged2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersPrivileged2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged2Query : UsersPrivileged2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2Query class.
		/// </summary>
		public UsersPrivileged2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersPrivileged2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersPrivileged2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersPrivileged2Query
		
	#region UsersProcurementAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditFilters : UsersProcurementAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditFilters class.
		/// </summary>
		public UsersProcurementAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementAuditFilters
	
	#region UsersProcurementAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersProcurementAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditQuery : UsersProcurementAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditQuery class.
		/// </summary>
		public UsersProcurementAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementAuditQuery
		
	#region UsersAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditFilters : UsersAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditFilters class.
		/// </summary>
		public UsersAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersAuditFilters
	
	#region UsersAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditQuery : UsersAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditQuery class.
		/// </summary>
		public UsersAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersAuditQuery
		
	#region UsersPrivilegedFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivilegedFilters : UsersPrivilegedFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedFilters class.
		/// </summary>
		public UsersPrivilegedFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersPrivilegedFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersPrivilegedFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersPrivilegedFilters
	
	#region UsersPrivilegedQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersPrivilegedParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivilegedQuery : UsersPrivilegedParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedQuery class.
		/// </summary>
		public UsersPrivilegedQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersPrivilegedQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersPrivilegedQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersPrivilegedQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersPrivilegedQuery
		
	#region UsersEbiFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEbi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiFilters : UsersEbiFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEbiFilters class.
		/// </summary>
		public UsersEbiFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEbiFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEbiFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEbiFilters
	
	#region UsersEbiQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersEbiParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersEbi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiQuery : UsersEbiParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEbiQuery class.
		/// </summary>
		public UsersEbiQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEbiQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEbiQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEbiQuery
		
	#region TwentyOnePointAudit12Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit12"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit12Filters : TwentyOnePointAudit12FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12Filters class.
		/// </summary>
		public TwentyOnePointAudit12Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit12Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit12Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit12Filters
	
	#region TwentyOnePointAudit12Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit12ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit12"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit12Query : TwentyOnePointAudit12ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12Query class.
		/// </summary>
		public TwentyOnePointAudit12Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit12Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit12Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit12Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit12Query
		
	#region UsersEbiAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEbiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiAuditFilters : UsersEbiAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditFilters class.
		/// </summary>
		public UsersEbiAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEbiAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEbiAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEbiAuditFilters
	
	#region UsersEbiAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersEbiAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersEbiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiAuditQuery : UsersEbiAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditQuery class.
		/// </summary>
		public UsersEbiAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEbiAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEbiAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEbiAuditQuery
		
	#region TwentyOnePointAudit11Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit11"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit11Filters : TwentyOnePointAudit11FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit11Filters class.
		/// </summary>
		public TwentyOnePointAudit11Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit11Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit11Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit11Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit11Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit11Filters
	
	#region TwentyOnePointAudit11Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit11ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit11"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit11Query : TwentyOnePointAudit11ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit11Query class.
		/// </summary>
		public TwentyOnePointAudit11Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit11Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit11Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit11Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit11Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit11Query
		
	#region SitesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesFilters : SitesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesFilters class.
		/// </summary>
		public SitesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesFilters
	
	#region SitesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SitesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesQuery : SitesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesQuery class.
		/// </summary>
		public SitesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesQuery
		
	#region SafetyPlansSeQuestionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeQuestions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeQuestionsFilters : SafetyPlansSeQuestionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsFilters class.
		/// </summary>
		public SafetyPlansSeQuestionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeQuestionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeQuestionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeQuestionsFilters
	
	#region SafetyPlansSeQuestionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SafetyPlansSeQuestionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeQuestions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeQuestionsQuery : SafetyPlansSeQuestionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsQuery class.
		/// </summary>
		public SafetyPlansSeQuestionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeQuestionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeQuestionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeQuestionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeQuestionsQuery
		
	#region TemplateFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Template"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateFilters : TemplateFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateFilters class.
		/// </summary>
		public TemplateFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TemplateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TemplateFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TemplateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TemplateFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TemplateFilters
	
	#region TemplateQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TemplateParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Template"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateQuery : TemplateParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateQuery class.
		/// </summary>
		public TemplateQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TemplateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TemplateQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TemplateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TemplateQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TemplateQuery
		
	#region SystemLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogFilters : SystemLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogFilters class.
		/// </summary>
		public SystemLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SystemLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SystemLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SystemLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SystemLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SystemLogFilters
	
	#region SystemLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SystemLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogQuery : SystemLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogQuery class.
		/// </summary>
		public SystemLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SystemLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SystemLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SystemLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SystemLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SystemLogQuery
		
	#region TemplateTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeFilters : TemplateTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeFilters class.
		/// </summary>
		public TemplateTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TemplateTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TemplateTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TemplateTypeFilters
	
	#region TemplateTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TemplateTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeQuery : TemplateTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeQuery class.
		/// </summary>
		public TemplateTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TemplateTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TemplateTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TemplateTypeQuery
		
	#region SqExemptionAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditFilters : SqExemptionAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditFilters class.
		/// </summary>
		public SqExemptionAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionAuditFilters
	
	#region SqExemptionAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqExemptionAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditQuery : SqExemptionAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditQuery class.
		/// </summary>
		public SqExemptionAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionAuditQuery
		
	#region SafetyPlansSeResponsesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeResponses"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeResponsesFilters : SafetyPlansSeResponsesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesFilters class.
		/// </summary>
		public SafetyPlansSeResponsesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeResponsesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeResponsesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeResponsesFilters
	
	#region SafetyPlansSeResponsesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SafetyPlansSeResponsesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeResponses"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeResponsesQuery : SafetyPlansSeResponsesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesQuery class.
		/// </summary>
		public SafetyPlansSeResponsesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlansSeResponsesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlansSeResponsesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlansSeResponsesQuery
		
	#region SqExemptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFilters : SqExemptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFilters class.
		/// </summary>
		public SqExemptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFilters
	
	#region SqExemptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqExemptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionQuery : SqExemptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionQuery class.
		/// </summary>
		public SqExemptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionQuery
		
	#region TwentyOnePointAudit10Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10Filters : TwentyOnePointAudit10FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10Filters class.
		/// </summary>
		public TwentyOnePointAudit10Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit10Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit10Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit10Filters
	
	#region TwentyOnePointAudit10Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit10ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10Query : TwentyOnePointAudit10ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10Query class.
		/// </summary>
		public TwentyOnePointAudit10Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit10Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit10Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit10Query
		
	#region SafetyPlanStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusFilters : SafetyPlanStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusFilters class.
		/// </summary>
		public SafetyPlanStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlanStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlanStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlanStatusFilters
	
	#region SafetyPlanStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SafetyPlanStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusQuery : SafetyPlanStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusQuery class.
		/// </summary>
		public SafetyPlanStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlanStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlanStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlanStatusQuery
		
	#region TwentyOnePointAudit07Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07Filters : TwentyOnePointAudit07FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07Filters class.
		/// </summary>
		public TwentyOnePointAudit07Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit07Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit07Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit07Filters
	
	#region TwentyOnePointAudit07Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit07ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit07"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit07Query : TwentyOnePointAudit07ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07Query class.
		/// </summary>
		public TwentyOnePointAudit07Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit07Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit07Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit07Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit07Query
		
	#region QuestionnaireCommentsAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditFilters : QuestionnaireCommentsAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditFilters class.
		/// </summary>
		public QuestionnaireCommentsAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsAuditFilters
	
	#region QuestionnaireCommentsAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireCommentsAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditQuery : QuestionnaireCommentsAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditQuery class.
		/// </summary>
		public QuestionnaireCommentsAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsAuditQuery
		
	#region TwentyOnePointAudit08Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit08"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit08Filters : TwentyOnePointAudit08FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08Filters class.
		/// </summary>
		public TwentyOnePointAudit08Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit08Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit08Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit08Filters
	
	#region TwentyOnePointAudit08Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit08ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit08"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit08Query : TwentyOnePointAudit08ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08Query class.
		/// </summary>
		public TwentyOnePointAudit08Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit08Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit08Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit08Query
		
	#region TwentyOnePointAudit01Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01Filters : TwentyOnePointAudit01FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01Filters class.
		/// </summary>
		public TwentyOnePointAudit01Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit01Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit01Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit01Filters
	
	#region TwentyOnePointAudit01Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit01ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01Query : TwentyOnePointAudit01ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01Query class.
		/// </summary>
		public TwentyOnePointAudit01Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit01Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit01Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit01Query
		
	#region TwentyOnePointAudit09Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit09"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit09Filters : TwentyOnePointAudit09FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09Filters class.
		/// </summary>
		public TwentyOnePointAudit09Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit09Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit09Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit09Filters
	
	#region TwentyOnePointAudit09Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit09ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit09"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit09Query : TwentyOnePointAudit09ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09Query class.
		/// </summary>
		public TwentyOnePointAudit09Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit09Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit09Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit09Query
		
	#region TwentyOnePointAudit02Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit02"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit02Filters : TwentyOnePointAudit02FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02Filters class.
		/// </summary>
		public TwentyOnePointAudit02Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit02Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit02Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit02Filters
	
	#region TwentyOnePointAudit02Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit02ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit02"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit02Query : TwentyOnePointAudit02ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02Query class.
		/// </summary>
		public TwentyOnePointAudit02Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit02Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit02Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit02Query
		
	#region TwentyOnePointAudit06Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06Filters : TwentyOnePointAudit06FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06Filters class.
		/// </summary>
		public TwentyOnePointAudit06Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit06Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit06Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit06Filters
	
	#region TwentyOnePointAudit06Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit06ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit06"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit06Query : TwentyOnePointAudit06ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06Query class.
		/// </summary>
		public TwentyOnePointAudit06Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit06Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit06Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit06Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit06Query
		
	#region TwentyOnePointAudit05Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit05"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit05Filters : TwentyOnePointAudit05FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit05Filters class.
		/// </summary>
		public TwentyOnePointAudit05Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit05Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit05Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit05Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit05Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit05Filters
	
	#region TwentyOnePointAudit05Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit05ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit05"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit05Query : TwentyOnePointAudit05ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit05Query class.
		/// </summary>
		public TwentyOnePointAudit05Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit05Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit05Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit05Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit05Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit05Query
		
	#region TwentyOnePointAudit04Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit04"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit04Filters : TwentyOnePointAudit04FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04Filters class.
		/// </summary>
		public TwentyOnePointAudit04Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit04Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit04Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit04Filters
	
	#region TwentyOnePointAudit04Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit04ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit04"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit04Query : TwentyOnePointAudit04ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04Query class.
		/// </summary>
		public TwentyOnePointAudit04Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit04Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit04Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit04Query
		
	#region TwentyOnePointAudit03Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit03"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit03Filters : TwentyOnePointAudit03FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03Filters class.
		/// </summary>
		public TwentyOnePointAudit03Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit03Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit03Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit03Filters
	
	#region TwentyOnePointAudit03Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAudit03ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit03"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit03Query : TwentyOnePointAudit03ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03Query class.
		/// </summary>
		public TwentyOnePointAudit03Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit03Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit03Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit03Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit03Query
		
	#region TwentyOnePointAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditFilters : TwentyOnePointAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditFilters class.
		/// </summary>
		public TwentyOnePointAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditFilters
	
	#region TwentyOnePointAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TwentyOnePointAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQuery : TwentyOnePointAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQuery class.
		/// </summary>
		public TwentyOnePointAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditQuery
		
	#region ConfigNavigationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationFilters : ConfigNavigationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationFilters class.
		/// </summary>
		public ConfigNavigationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigNavigationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigNavigationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigNavigationFilters
	
	#region ConfigNavigationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigNavigationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationQuery : ConfigNavigationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationQuery class.
		/// </summary>
		public ConfigNavigationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigNavigationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigNavigationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigNavigationQuery
		
	#region QuestionnaireCommentsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsFilters : QuestionnaireCommentsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsFilters class.
		/// </summary>
		public QuestionnaireCommentsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsFilters
	
	#region QuestionnaireCommentsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireCommentsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsQuery : QuestionnaireCommentsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsQuery class.
		/// </summary>
		public QuestionnaireCommentsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsQuery
		
	#region ConfigSa812Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812Filters : ConfigSa812FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812Filters class.
		/// </summary>
		public ConfigSa812Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812Filters
	
	#region ConfigSa812Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigSa812ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812Query : ConfigSa812ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812Query class.
		/// </summary>
		public ConfigSa812Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812Query
		
	#region ConfigFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Config"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigFilters : ConfigFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigFilters class.
		/// </summary>
		public ConfigFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigFilters
	
	#region ConfigQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Config"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigQuery : ConfigParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigQuery class.
		/// </summary>
		public ConfigQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigQuery
		
	#region ConfigTextTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigTextType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextTypeFilters : ConfigTextTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeFilters class.
		/// </summary>
		public ConfigTextTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextTypeFilters
	
	#region ConfigTextTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigTextTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ConfigTextType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextTypeQuery : ConfigTextTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeQuery class.
		/// </summary>
		public ConfigTextTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextTypeQuery
		
	#region CompanyStatusChangeApprovalFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalFilters : CompanyStatusChangeApprovalFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalFilters class.
		/// </summary>
		public CompanyStatusChangeApprovalFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusChangeApprovalFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusChangeApprovalFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusChangeApprovalFilters
	
	#region CompanyStatusChangeApprovalQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanyStatusChangeApprovalParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanyStatusChangeApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusChangeApprovalQuery : CompanyStatusChangeApprovalParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalQuery class.
		/// </summary>
		public CompanyStatusChangeApprovalQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusChangeApprovalQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusChangeApprovalQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusChangeApprovalQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusChangeApprovalQuery
		
	#region CompanySiteCategoryExceptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryExceptionFilters : CompanySiteCategoryExceptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionFilters class.
		/// </summary>
		public CompanySiteCategoryExceptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryExceptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryExceptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryExceptionFilters
	
	#region CompanySiteCategoryExceptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryExceptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryExceptionQuery : CompanySiteCategoryExceptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionQuery class.
		/// </summary>
		public CompanySiteCategoryExceptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryExceptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryExceptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryExceptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryExceptionQuery
		
	#region CompanySiteCategoryStandardAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditFilters : CompanySiteCategoryStandardAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditFilters class.
		/// </summary>
		public CompanySiteCategoryStandardAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardAuditFilters
	
	#region CompanySiteCategoryStandardAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryStandardAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditQuery : CompanySiteCategoryStandardAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditQuery class.
		/// </summary>
		public CompanySiteCategoryStandardAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardAuditQuery
		
	#region CompanySiteCategoryException2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2Filters : CompanySiteCategoryException2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2Filters class.
		/// </summary>
		public CompanySiteCategoryException2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryException2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryException2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryException2Filters
	
	#region CompanySiteCategoryException2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryException2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2Query : CompanySiteCategoryException2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2Query class.
		/// </summary>
		public CompanySiteCategoryException2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryException2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryException2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryException2Query
		
	#region ConfigTextFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextFilters : ConfigTextFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextFilters class.
		/// </summary>
		public ConfigTextFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextFilters
	
	#region ConfigTextQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigTextParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextQuery : ConfigTextParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextQuery class.
		/// </summary>
		public ConfigTextQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextQuery
		
	#region CompanySiteCategoryStandardFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardFilters : CompanySiteCategoryStandardFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardFilters class.
		/// </summary>
		public CompanySiteCategoryStandardFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardFilters
	
	#region CompanySiteCategoryStandardQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryStandardParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardQuery : CompanySiteCategoryStandardParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardQuery class.
		/// </summary>
		public CompanySiteCategoryStandardQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardQuery
		
	#region CsmsEmailLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogFilters : CsmsEmailLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogFilters class.
		/// </summary>
		public CsmsEmailLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogFilters
	
	#region CsmsEmailLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsEmailLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogQuery : CsmsEmailLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogQuery class.
		/// </summary>
		public CsmsEmailLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogQuery
		
	#region ConfigText2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2Filters : ConfigText2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2Filters class.
		/// </summary>
		public ConfigText2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigText2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigText2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigText2Filters
	
	#region ConfigText2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigText2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2Query : ConfigText2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2Query class.
		/// </summary>
		public ConfigText2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigText2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigText2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigText2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigText2Query
		
	#region CsaFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaFilters : CsaFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaFilters class.
		/// </summary>
		public CsaFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaFilters
	
	#region CsaQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsaParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaQuery : CsaParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaQuery class.
		/// </summary>
		public CsaQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaQuery
		
	#region CsmsFileTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFileType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileTypeFilters : CsmsFileTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeFilters class.
		/// </summary>
		public CsmsFileTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsFileTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsFileTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsFileTypeFilters
	
	#region CsmsFileTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsFileTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsFileType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileTypeQuery : CsmsFileTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeQuery class.
		/// </summary>
		public CsmsFileTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsFileTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsFileTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsFileTypeQuery
		
	#region CsmsEmailLogRecipientFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientFilters : CsmsEmailLogRecipientFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientFilters class.
		/// </summary>
		public CsmsEmailLogRecipientFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogRecipientFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogRecipientFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogRecipientFilters
	
	#region CsmsEmailLogRecipientQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsEmailLogRecipientParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientQuery : CsmsEmailLogRecipientParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientQuery class.
		/// </summary>
		public CsmsEmailLogRecipientQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogRecipientQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogRecipientQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogRecipientQuery
		
	#region CsmsAccessFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessFilters : CsmsAccessFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessFilters class.
		/// </summary>
		public CsmsAccessFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsAccessFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsAccessFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsAccessFilters
	
	#region CsmsAccessQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsAccessParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsAccess"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsAccessQuery : CsmsAccessParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsAccessQuery class.
		/// </summary>
		public CsmsAccessQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsAccessQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsAccessQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsAccessQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsAccessQuery
		
	#region ContactsAlcoaFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsAlcoa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsAlcoaFilters : ContactsAlcoaFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaFilters class.
		/// </summary>
		public ContactsAlcoaFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContactsAlcoaFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContactsAlcoaFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContactsAlcoaFilters
	
	#region ContactsAlcoaQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ContactsAlcoaParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ContactsAlcoa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsAlcoaQuery : ContactsAlcoaParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaQuery class.
		/// </summary>
		public ContactsAlcoaQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContactsAlcoaQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContactsAlcoaQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContactsAlcoaQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContactsAlcoaQuery
		
	#region CsaAnswersFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersFilters : CsaAnswersFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersFilters class.
		/// </summary>
		public CsaAnswersFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaAnswersFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaAnswersFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaAnswersFilters
	
	#region CsaAnswersQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsaAnswersParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersQuery : CsaAnswersParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersQuery class.
		/// </summary>
		public CsaAnswersQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaAnswersQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaAnswersQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaAnswersQuery
		
	#region CompanySiteCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryFilters : CompanySiteCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryFilters class.
		/// </summary>
		public CompanySiteCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryFilters
	
	#region CompanySiteCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryQuery : CompanySiteCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryQuery class.
		/// </summary>
		public CompanySiteCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryQuery
		
	#region ContactsContractorsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsFilters : ContactsContractorsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsFilters class.
		/// </summary>
		public ContactsContractorsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContactsContractorsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContactsContractorsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContactsContractorsFilters
	
	#region ContactsContractorsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ContactsContractorsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsQuery : ContactsContractorsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsQuery class.
		/// </summary>
		public ContactsContractorsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContactsContractorsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContactsContractorsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContactsContractorsQuery
		
	#region AdminTaskEmailTemplateFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateFilters : AdminTaskEmailTemplateFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateFilters class.
		/// </summary>
		public AdminTaskEmailTemplateFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateFilters
	
	#region AdminTaskEmailTemplateQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailTemplateParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateQuery : AdminTaskEmailTemplateParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateQuery class.
		/// </summary>
		public AdminTaskEmailTemplateQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateQuery
		
	#region CompaniesRelationshipFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipFilters : CompaniesRelationshipFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipFilters class.
		/// </summary>
		public CompaniesRelationshipFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesRelationshipFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesRelationshipFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesRelationshipFilters
	
	#region CompaniesRelationshipQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesRelationshipParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipQuery : CompaniesRelationshipParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipQuery class.
		/// </summary>
		public CompaniesRelationshipQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesRelationshipQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesRelationshipQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesRelationshipQuery
		
	#region AdminTaskEmailRecipientFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientFilters : AdminTaskEmailRecipientFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientFilters class.
		/// </summary>
		public AdminTaskEmailRecipientFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientFilters
	
	#region AdminTaskEmailRecipientQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailRecipientParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientQuery : AdminTaskEmailRecipientParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientQuery class.
		/// </summary>
		public AdminTaskEmailRecipientQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientQuery
		
	#region AdminTaskSourceFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceFilters : AdminTaskSourceFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceFilters class.
		/// </summary>
		public AdminTaskSourceFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskSourceFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskSourceFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskSourceFilters
	
	#region AdminTaskSourceQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskSourceParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskSource"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskSourceQuery : AdminTaskSourceParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceQuery class.
		/// </summary>
		public AdminTaskSourceQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskSourceQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskSourceQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskSourceQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskSourceQuery
		
	#region AdHocRadarFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarFilters : AdHocRadarFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarFilters class.
		/// </summary>
		public AdHocRadarFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarFilters
	
	#region AdHocRadarQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdHocRadarParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarQuery : AdHocRadarParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarQuery class.
		/// </summary>
		public AdHocRadarQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarQuery
		
	#region AdHocRadarItems2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2Filters : AdHocRadarItems2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2Filters class.
		/// </summary>
		public AdHocRadarItems2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItems2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItems2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItems2Filters
	
	#region AdHocRadarItems2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdHocRadarItems2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2Query : AdHocRadarItems2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2Query class.
		/// </summary>
		public AdHocRadarItems2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItems2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItems2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItems2Query
		
	#region AdminTaskEmailTemplateRecipientFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientFilters : AdminTaskEmailTemplateRecipientFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientFilters class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateRecipientFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateRecipientFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateRecipientFilters
	
	#region AdminTaskEmailTemplateRecipientQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailTemplateRecipientParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientQuery : AdminTaskEmailTemplateRecipientParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientQuery class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateRecipientQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateRecipientQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateRecipientQuery
		
	#region AdminTaskStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusFilters : AdminTaskStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusFilters class.
		/// </summary>
		public AdminTaskStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskStatusFilters
	
	#region AdminTaskStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusQuery : AdminTaskStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusQuery class.
		/// </summary>
		public AdminTaskStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskStatusQuery
		
	#region AdHocRadarItemsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsFilters : AdHocRadarItemsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsFilters class.
		/// </summary>
		public AdHocRadarItemsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItemsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItemsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItemsFilters
	
	#region AdHocRadarItemsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdHocRadarItemsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsQuery : AdHocRadarItemsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsQuery class.
		/// </summary>
		public AdHocRadarItemsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItemsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItemsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItemsQuery
		
	#region AdminTaskTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeFilters : AdminTaskTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeFilters class.
		/// </summary>
		public AdminTaskTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskTypeFilters
	
	#region AdminTaskTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskTypeQuery : AdminTaskTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeQuery class.
		/// </summary>
		public AdminTaskTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskTypeQuery
		
	#region AnnualTargetsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsFilters : AnnualTargetsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsFilters class.
		/// </summary>
		public AnnualTargetsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AnnualTargetsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AnnualTargetsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AnnualTargetsFilters
	
	#region AnnualTargetsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AnnualTargetsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AnnualTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AnnualTargetsQuery : AnnualTargetsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsQuery class.
		/// </summary>
		public AnnualTargetsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AnnualTargetsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AnnualTargetsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AnnualTargetsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AnnualTargetsQuery
		
	#region CompaniesHrMapFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapFilters : CompaniesHrMapFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapFilters class.
		/// </summary>
		public CompaniesHrMapFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrMapFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrMapFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrMapFilters
	
	#region CompaniesHrMapQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesHrMapParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapQuery : CompaniesHrMapParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapQuery class.
		/// </summary>
		public CompaniesHrMapQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrMapQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrMapQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrMapQuery
		
	#region CompaniesEhsimsMapFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapFilters : CompaniesEhsimsMapFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapFilters class.
		/// </summary>
		public CompaniesEhsimsMapFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapFilters
	
	#region CompaniesEhsimsMapQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesEhsimsMapParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapQuery : CompaniesEhsimsMapParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapQuery class.
		/// </summary>
		public CompaniesEhsimsMapQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapQuery
		
	#region AdminTaskFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskFilters : AdminTaskFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskFilters class.
		/// </summary>
		public AdminTaskFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskFilters
	
	#region AdminTaskQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTask"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskQuery : AdminTaskParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskQuery class.
		/// </summary>
		public AdminTaskQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskQuery
		
	#region CompaniesHrCrpDataFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataFilters : CompaniesHrCrpDataFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataFilters class.
		/// </summary>
		public CompaniesHrCrpDataFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataFilters
	
	#region CompaniesHrCrpDataQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesHrCrpDataParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpData"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataQuery : CompaniesHrCrpDataParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataQuery class.
		/// </summary>
		public CompaniesHrCrpDataQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataQuery
		
	#region AdminTaskEmailLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogFilters : AdminTaskEmailLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogFilters class.
		/// </summary>
		public AdminTaskEmailLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailLogFilters
	
	#region AdminTaskEmailLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailLogQuery : AdminTaskEmailLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogQuery class.
		/// </summary>
		public AdminTaskEmailLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailLogQuery
		
	#region ApssLogsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsFilters : ApssLogsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsFilters class.
		/// </summary>
		public ApssLogsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ApssLogsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ApssLogsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ApssLogsFilters
	
	#region ApssLogsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ApssLogsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsQuery : ApssLogsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsQuery class.
		/// </summary>
		public ApssLogsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ApssLogsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ApssLogsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ApssLogsQuery
		
	#region CompaniesAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditFilters : CompaniesAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditFilters class.
		/// </summary>
		public CompaniesAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesAuditFilters
	
	#region CompaniesAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditQuery : CompaniesAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditQuery class.
		/// </summary>
		public CompaniesAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesAuditQuery
		
	#region AuditFileVaultFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AuditFileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultFilters : AuditFileVaultFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultFilters class.
		/// </summary>
		public AuditFileVaultFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AuditFileVaultFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AuditFileVaultFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AuditFileVaultFilters
	
	#region AuditFileVaultQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AuditFileVaultParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AuditFileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultQuery : AuditFileVaultParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultQuery class.
		/// </summary>
		public AuditFileVaultQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AuditFileVaultQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AuditFileVaultQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AuditFileVaultQuery
		
	#region CsmsEmailRecipientTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeFilters : CsmsEmailRecipientTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeFilters class.
		/// </summary>
		public CsmsEmailRecipientTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailRecipientTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailRecipientTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailRecipientTypeFilters
	
	#region CsmsEmailRecipientTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsEmailRecipientTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeQuery : CsmsEmailRecipientTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeQuery class.
		/// </summary>
		public CsmsEmailRecipientTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailRecipientTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailRecipientTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailRecipientTypeQuery
		
	#region AuditFileVaultTableFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AuditFileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultTableFilters : AuditFileVaultTableFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultTableFilters class.
		/// </summary>
		public AuditFileVaultTableFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultTableFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AuditFileVaultTableFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultTableFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AuditFileVaultTableFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AuditFileVaultTableFilters
	
	#region AuditFileVaultTableQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AuditFileVaultTableParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AuditFileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultTableQuery : AuditFileVaultTableParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultTableQuery class.
		/// </summary>
		public AuditFileVaultTableQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultTableQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AuditFileVaultTableQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultTableQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AuditFileVaultTableQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AuditFileVaultTableQuery
		
	#region KpiFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiFilters : KpiFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiFilters class.
		/// </summary>
		public KpiFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiFilters
	
	#region KpiQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiQuery : KpiParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiQuery class.
		/// </summary>
		public KpiQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiQuery
		
	#region CsmsFileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileFilters : CsmsFileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileFilters class.
		/// </summary>
		public CsmsFileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsFileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsFileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsFileFilters
	
	#region CsmsFileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsFileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileQuery : CsmsFileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileQuery class.
		/// </summary>
		public CsmsFileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsFileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsFileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsFileQuery
		
	#region FileVaultSubCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryFilters : FileVaultSubCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryFilters class.
		/// </summary>
		public FileVaultSubCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultSubCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultSubCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultSubCategoryFilters
	
	#region FileVaultSubCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultSubCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryQuery : FileVaultSubCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryQuery class.
		/// </summary>
		public FileVaultSubCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultSubCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultSubCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultSubCategoryQuery
		
	#region KpiAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditFilters : KpiAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditFilters class.
		/// </summary>
		public KpiAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiAuditFilters
	
	#region KpiAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditQuery : KpiAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditQuery class.
		/// </summary>
		public KpiAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiAuditQuery
		
	#region FileVaultCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultCategoryFilters : FileVaultCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryFilters class.
		/// </summary>
		public FileVaultCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultCategoryFilters
	
	#region FileVaultCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultCategoryQuery : FileVaultCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryQuery class.
		/// </summary>
		public FileVaultCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultCategoryQuery
		
	#region FileDbMedicalTrainingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingFilters : FileDbMedicalTrainingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingFilters class.
		/// </summary>
		public FileDbMedicalTrainingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbMedicalTrainingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbMedicalTrainingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbMedicalTrainingFilters
	
	#region FileDbMedicalTrainingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileDbMedicalTrainingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileDbMedicalTraining"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbMedicalTrainingQuery : FileDbMedicalTrainingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingQuery class.
		/// </summary>
		public FileDbMedicalTrainingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbMedicalTrainingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbMedicalTrainingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbMedicalTrainingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbMedicalTrainingQuery
		
	#region FileVaultAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultAuditFilters : FileVaultAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditFilters class.
		/// </summary>
		public FileVaultAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultAuditFilters
	
	#region FileVaultAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultAuditQuery : FileVaultAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditQuery class.
		/// </summary>
		public FileVaultAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultAuditQuery
		
	#region FileVaultFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultFilters : FileVaultFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultFilters class.
		/// </summary>
		public FileVaultFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultFilters
	
	#region FileVaultQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultQuery : FileVaultParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultQuery class.
		/// </summary>
		public FileVaultQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultQuery
		
	#region KpiProjectListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListFilters : KpiProjectListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListFilters class.
		/// </summary>
		public KpiProjectListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListFilters
	
	#region KpiProjectListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiProjectListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiProjectList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListQuery : KpiProjectListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListQuery class.
		/// </summary>
		public KpiProjectListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListQuery
		
	#region FileVaultTableFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableFilters : FileVaultTableFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableFilters class.
		/// </summary>
		public FileVaultTableFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableFilters
	
	#region FileVaultTableQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultTableParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableQuery : FileVaultTableParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableQuery class.
		/// </summary>
		public FileVaultTableQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableQuery
		
	#region KpiProjectsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsFilters : KpiProjectsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsFilters class.
		/// </summary>
		public KpiProjectsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectsFilters
	
	#region KpiProjectsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiProjectsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsQuery : KpiProjectsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsQuery class.
		/// </summary>
		public KpiProjectsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectsQuery
		
	#region PrivilegeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeFilters : PrivilegeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeFilters class.
		/// </summary>
		public PrivilegeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PrivilegeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PrivilegeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PrivilegeFilters
	
	#region PrivilegeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PrivilegeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeQuery : PrivilegeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeQuery class.
		/// </summary>
		public PrivilegeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PrivilegeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PrivilegeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PrivilegeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PrivilegeQuery
		
	#region KpiPurchaseOrderListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListFilters : KpiPurchaseOrderListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListFilters class.
		/// </summary>
		public KpiPurchaseOrderListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListFilters
	
	#region KpiPurchaseOrderListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiPurchaseOrderListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListQuery : KpiPurchaseOrderListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListQuery class.
		/// </summary>
		public KpiPurchaseOrderListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListQuery
		
	#region QuestionnaireActionLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFilters : QuestionnaireActionLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFilters class.
		/// </summary>
		public QuestionnaireActionLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogFilters
	
	#region QuestionnaireActionLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireActionLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogQuery : QuestionnaireActionLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogQuery class.
		/// </summary>
		public QuestionnaireActionLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogQuery
		
	#region PermissionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Permission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionFilters : PermissionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PermissionFilters class.
		/// </summary>
		public PermissionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PermissionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PermissionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PermissionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PermissionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PermissionFilters
	
	#region PermissionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PermissionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Permission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PermissionQuery : PermissionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PermissionQuery class.
		/// </summary>
		public PermissionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PermissionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PermissionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PermissionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PermissionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PermissionQuery
		
	#region QuestionnaireAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditFilters : QuestionnaireAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditFilters class.
		/// </summary>
		public QuestionnaireAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireAuditFilters
	
	#region QuestionnaireAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditQuery : QuestionnaireAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditQuery class.
		/// </summary>
		public QuestionnaireAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireAuditQuery
		
	#region AdminTaskEmailTemplateAttachmentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateAttachmentFilters : AdminTaskEmailTemplateAttachmentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentFilters class.
		/// </summary>
		public AdminTaskEmailTemplateAttachmentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateAttachmentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateAttachmentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateAttachmentFilters
	
	#region AdminTaskEmailTemplateAttachmentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailTemplateAttachmentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateAttachmentQuery : AdminTaskEmailTemplateAttachmentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentQuery class.
		/// </summary>
		public AdminTaskEmailTemplateAttachmentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateAttachmentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateAttachmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateAttachmentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateAttachmentQuery
		
	#region News2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="News2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class News2Filters : News2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the News2Filters class.
		/// </summary>
		public News2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the News2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public News2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the News2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public News2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion News2Filters
	
	#region News2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="News2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="News2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class News2Query : News2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the News2Query class.
		/// </summary>
		public News2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the News2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public News2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the News2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public News2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion News2Query
		
	#region MonthsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Months"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MonthsFilters : MonthsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MonthsFilters class.
		/// </summary>
		public MonthsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MonthsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MonthsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MonthsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MonthsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MonthsFilters
	
	#region MonthsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MonthsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Months"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MonthsQuery : MonthsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MonthsQuery class.
		/// </summary>
		public MonthsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MonthsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MonthsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MonthsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MonthsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MonthsQuery
		
	#region FileDbAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditFilters : FileDbAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditFilters class.
		/// </summary>
		public FileDbAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAuditFilters
	
	#region FileDbAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileDbAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditQuery : FileDbAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditQuery class.
		/// </summary>
		public FileDbAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAuditQuery
		
	#region NewsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="News"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NewsFilters : NewsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NewsFilters class.
		/// </summary>
		public NewsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the NewsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NewsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NewsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NewsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NewsFilters
	
	#region NewsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="NewsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="News"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NewsQuery : NewsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NewsQuery class.
		/// </summary>
		public NewsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the NewsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NewsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NewsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NewsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NewsQuery
		
	#region FileDbAdminFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminFilters : FileDbAdminFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminFilters class.
		/// </summary>
		public FileDbAdminFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAdminFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAdminFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAdminFilters
	
	#region FileDbAdminQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileDbAdminParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminQuery : FileDbAdminParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminQuery class.
		/// </summary>
		public FileDbAdminQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAdminQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAdminQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAdminQuery
		
	#region EhsConsultantFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantFilters : EhsConsultantFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantFilters class.
		/// </summary>
		public EhsConsultantFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsConsultantFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsConsultantFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsConsultantFilters
	
	#region EhsConsultantQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EhsConsultantParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantQuery : EhsConsultantParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantQuery class.
		/// </summary>
		public EhsConsultantQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsConsultantQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsConsultantQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsConsultantQuery
		
	#region EbiIssueFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueFilters : EbiIssueFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueFilters class.
		/// </summary>
		public EbiIssueFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiIssueFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiIssueFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiIssueFilters
	
	#region EbiIssueQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiIssueParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueQuery : EbiIssueParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueQuery class.
		/// </summary>
		public EbiIssueQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiIssueQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiIssueQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiIssueQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiIssueQuery
		
	#region EbiDailyReportFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportFilters : EbiDailyReportFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportFilters class.
		/// </summary>
		public EbiDailyReportFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiDailyReportFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiDailyReportFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiDailyReportFilters
	
	#region EbiDailyReportQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiDailyReportParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportQuery : EbiDailyReportParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportQuery class.
		/// </summary>
		public EbiDailyReportQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiDailyReportQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiDailyReportQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiDailyReportQuery
		
	#region EbiMetricFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricFilters : EbiMetricFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricFilters class.
		/// </summary>
		public EbiMetricFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiMetricFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiMetricFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiMetricFilters
	
	#region EbiMetricQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiMetricParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EbiMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiMetricQuery : EbiMetricParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiMetricQuery class.
		/// </summary>
		public EbiMetricQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiMetricQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiMetricQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiMetricQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiMetricQuery
		
	#region CustomReportingLayoutsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsFilters : CustomReportingLayoutsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsFilters class.
		/// </summary>
		public CustomReportingLayoutsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomReportingLayoutsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomReportingLayoutsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomReportingLayoutsFilters
	
	#region CustomReportingLayoutsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CustomReportingLayoutsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsQuery : CustomReportingLayoutsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsQuery class.
		/// </summary>
		public CustomReportingLayoutsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomReportingLayoutsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomReportingLayoutsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomReportingLayoutsQuery
		
	#region EbiFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiFilters : EbiFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiFilters class.
		/// </summary>
		public EbiFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiFilters
	
	#region EbiQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiQuery : EbiParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiQuery class.
		/// </summary>
		public EbiQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiQuery
		
	#region DocumentsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsFilters : DocumentsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsFilters class.
		/// </summary>
		public DocumentsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsFilters
	
	#region DocumentsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DocumentsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsQuery : DocumentsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsQuery class.
		/// </summary>
		public DocumentsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsQuery
		
	#region EhsimsExceptionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsFilters : EhsimsExceptionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsFilters class.
		/// </summary>
		public EhsimsExceptionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsFilters
	
	#region EhsimsExceptionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EhsimsExceptionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsQuery : EhsimsExceptionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsQuery class.
		/// </summary>
		public EhsimsExceptionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsQuery
		
	#region DocumentsDownloadLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogFilters : DocumentsDownloadLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogFilters class.
		/// </summary>
		public DocumentsDownloadLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsDownloadLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsDownloadLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsDownloadLogFilters
	
	#region DocumentsDownloadLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DocumentsDownloadLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogQuery : DocumentsDownloadLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogQuery class.
		/// </summary>
		public DocumentsDownloadLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsDownloadLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsDownloadLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsDownloadLogQuery
		
	#region ElmahErrorFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ElmahError"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ElmahErrorFilters : ElmahErrorFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ElmahErrorFilters class.
		/// </summary>
		public ElmahErrorFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ElmahErrorFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ElmahErrorFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ElmahErrorFilters
	
	#region ElmahErrorQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ElmahErrorParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ElmahError"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ElmahErrorQuery : ElmahErrorParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ElmahErrorQuery class.
		/// </summary>
		public ElmahErrorQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ElmahErrorQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ElmahErrorQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ElmahErrorQuery
		
	#region EscalationChainSafetyRolesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafetyRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyRolesFilters : EscalationChainSafetyRolesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesFilters class.
		/// </summary>
		public EscalationChainSafetyRolesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainSafetyRolesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainSafetyRolesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainSafetyRolesFilters
	
	#region EscalationChainSafetyRolesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EscalationChainSafetyRolesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafetyRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyRolesQuery : EscalationChainSafetyRolesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesQuery class.
		/// </summary>
		public EscalationChainSafetyRolesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainSafetyRolesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainSafetyRolesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainSafetyRolesQuery
		
	#region EmailLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogFilters : EmailLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogFilters class.
		/// </summary>
		public EmailLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogFilters
	
	#region EmailLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EmailLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogQuery : EmailLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogQuery class.
		/// </summary>
		public EmailLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogQuery
		
	#region FileDbFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbFilters : FileDbFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbFilters class.
		/// </summary>
		public FileDbFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbFilters
	
	#region FileDbQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileDbParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbQuery : FileDbParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbQuery class.
		/// </summary>
		public FileDbQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbQuery
		
	#region EscalationChainSafetyFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyFilters : EscalationChainSafetyFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyFilters class.
		/// </summary>
		public EscalationChainSafetyFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainSafetyFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainSafetyFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainSafetyFilters
	
	#region EscalationChainSafetyQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EscalationChainSafetyParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyQuery : EscalationChainSafetyParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyQuery class.
		/// </summary>
		public EscalationChainSafetyQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainSafetyQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainSafetyQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainSafetyQuery
		
	#region EscalationChainProcurementRolesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesFilters : EscalationChainProcurementRolesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesFilters class.
		/// </summary>
		public EscalationChainProcurementRolesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementRolesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementRolesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementRolesFilters
	
	#region EscalationChainProcurementRolesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EscalationChainProcurementRolesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesQuery : EscalationChainProcurementRolesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesQuery class.
		/// </summary>
		public EscalationChainProcurementRolesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementRolesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementRolesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementRolesQuery
		
	#region EmailLogTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeFilters : EmailLogTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeFilters class.
		/// </summary>
		public EmailLogTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogTypeFilters
	
	#region EmailLogTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EmailLogTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeQuery : EmailLogTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeQuery class.
		/// </summary>
		public EmailLogTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogTypeQuery
		
	#region EscalationChainProcurementFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementFilters : EscalationChainProcurementFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementFilters class.
		/// </summary>
		public EscalationChainProcurementFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementFilters
	
	#region EscalationChainProcurementQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EscalationChainProcurementParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementQuery : EscalationChainProcurementParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementQuery class.
		/// </summary>
		public EscalationChainProcurementQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementQuery
		
	#region EnumApprovalFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumApprovalFilters : EnumApprovalFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumApprovalFilters class.
		/// </summary>
		public EnumApprovalFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumApprovalFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumApprovalFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumApprovalFilters
	
	#region EnumApprovalQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EnumApprovalParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EnumApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumApprovalQuery : EnumApprovalParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumApprovalQuery class.
		/// </summary>
		public EnumApprovalQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumApprovalQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumApprovalQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumApprovalQuery
		
	#region YearlyTargetsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsFilters : YearlyTargetsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsFilters class.
		/// </summary>
		public YearlyTargetsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public YearlyTargetsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public YearlyTargetsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion YearlyTargetsFilters
	
	#region YearlyTargetsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="YearlyTargetsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsQuery : YearlyTargetsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsQuery class.
		/// </summary>
		public YearlyTargetsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public YearlyTargetsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public YearlyTargetsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion YearlyTargetsQuery
		
	#region EnumQuestionnaireRiskRatingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingFilters : EnumQuestionnaireRiskRatingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingFilters class.
		/// </summary>
		public EnumQuestionnaireRiskRatingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireRiskRatingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireRiskRatingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireRiskRatingFilters
	
	#region EnumQuestionnaireRiskRatingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EnumQuestionnaireRiskRatingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingQuery : EnumQuestionnaireRiskRatingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingQuery class.
		/// </summary>
		public EnumQuestionnaireRiskRatingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireRiskRatingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireRiskRatingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireRiskRatingQuery
		
	#region CurrentCompaniesEhsConsultantIdFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentCompaniesEhsConsultantIdFilters : CurrentCompaniesEhsConsultantIdFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdFilters class.
		/// </summary>
		public CurrentCompaniesEhsConsultantIdFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentCompaniesEhsConsultantIdFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentCompaniesEhsConsultantIdFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentCompaniesEhsConsultantIdFilters
	
	#region CurrentCompaniesEhsConsultantIdQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrentCompaniesEhsConsultantIdParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentCompaniesEhsConsultantIdQuery : CurrentCompaniesEhsConsultantIdParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdQuery class.
		/// </summary>
		public CurrentCompaniesEhsConsultantIdQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentCompaniesEhsConsultantIdQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentCompaniesEhsConsultantIdQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentCompaniesEhsConsultantIdQuery
		
	#region CurrentQuestionnaireProcurementContactFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementContactFilters : CurrentQuestionnaireProcurementContactFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactFilters class.
		/// </summary>
		public CurrentQuestionnaireProcurementContactFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementContactFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementContactFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementContactFilters
	
	#region CurrentQuestionnaireProcurementContactQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrentQuestionnaireProcurementContactParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementContactQuery : CurrentQuestionnaireProcurementContactParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactQuery class.
		/// </summary>
		public CurrentQuestionnaireProcurementContactQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementContactQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementContactQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementContactQuery
		
	#region CurrentQuestionnaireProcurementFunctionalManagerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementFunctionalManagerFilters : CurrentQuestionnaireProcurementFunctionalManagerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerFilters class.
		/// </summary>
		public CurrentQuestionnaireProcurementFunctionalManagerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementFunctionalManagerFilters
	
	#region CurrentQuestionnaireProcurementFunctionalManagerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementFunctionalManagerQuery : CurrentQuestionnaireProcurementFunctionalManagerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerQuery class.
		/// </summary>
		public CurrentQuestionnaireProcurementFunctionalManagerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementFunctionalManagerQuery
		
	#region CurrentSmpEhsConsultantIdFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentSmpEhsConsultantIdFilters : CurrentSmpEhsConsultantIdFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdFilters class.
		/// </summary>
		public CurrentSmpEhsConsultantIdFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentSmpEhsConsultantIdFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentSmpEhsConsultantIdFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentSmpEhsConsultantIdFilters
	
	#region CurrentSmpEhsConsultantIdQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrentSmpEhsConsultantIdParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CurrentSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentSmpEhsConsultantIdQuery : CurrentSmpEhsConsultantIdParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdQuery class.
		/// </summary>
		public CurrentSmpEhsConsultantIdQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentSmpEhsConsultantIdQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentSmpEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentSmpEhsConsultantIdQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentSmpEhsConsultantIdQuery
		
	#region OrphanCompaniesEhsConsultantIdFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanCompaniesEhsConsultantIdFilters : OrphanCompaniesEhsConsultantIdFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdFilters class.
		/// </summary>
		public OrphanCompaniesEhsConsultantIdFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanCompaniesEhsConsultantIdFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanCompaniesEhsConsultantIdFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanCompaniesEhsConsultantIdFilters
	
	#region OrphanCompaniesEhsConsultantIdQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrphanCompaniesEhsConsultantIdParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanCompaniesEhsConsultantIdQuery : OrphanCompaniesEhsConsultantIdParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdQuery class.
		/// </summary>
		public OrphanCompaniesEhsConsultantIdQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanCompaniesEhsConsultantIdQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanCompaniesEhsConsultantIdQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanCompaniesEhsConsultantIdQuery
		
	#region OrphanQuestionnaireProcurementContactFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementContactFilters : OrphanQuestionnaireProcurementContactFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactFilters class.
		/// </summary>
		public OrphanQuestionnaireProcurementContactFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementContactFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementContactFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementContactFilters
	
	#region OrphanQuestionnaireProcurementContactQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrphanQuestionnaireProcurementContactParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementContactQuery : OrphanQuestionnaireProcurementContactParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactQuery class.
		/// </summary>
		public OrphanQuestionnaireProcurementContactQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementContactQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementContactQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementContactQuery
		
	#region OrphanQuestionnaireProcurementFunctionalManagerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementFunctionalManagerFilters : OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerFilters class.
		/// </summary>
		public OrphanQuestionnaireProcurementFunctionalManagerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementFunctionalManagerFilters
	
	#region OrphanQuestionnaireProcurementFunctionalManagerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementFunctionalManagerQuery : OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerQuery class.
		/// </summary>
		public OrphanQuestionnaireProcurementFunctionalManagerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementFunctionalManagerQuery
		
	#region OrphanSmpEhsConsultantIdFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanSmpEhsConsultantIdFilters : OrphanSmpEhsConsultantIdFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdFilters class.
		/// </summary>
		public OrphanSmpEhsConsultantIdFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanSmpEhsConsultantIdFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanSmpEhsConsultantIdFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanSmpEhsConsultantIdFilters
	
	#region OrphanSmpEhsConsultantIdQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrphanSmpEhsConsultantIdParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanSmpEhsConsultantIdQuery : OrphanSmpEhsConsultantIdParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdQuery class.
		/// </summary>
		public OrphanSmpEhsConsultantIdQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanSmpEhsConsultantIdQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanSmpEhsConsultantIdQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanSmpEhsConsultantIdQuery
		
	#region AdminTaskEmailRecipientsListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientsListFilters : AdminTaskEmailRecipientsListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListFilters class.
		/// </summary>
		public AdminTaskEmailRecipientsListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientsListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientsListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientsListFilters
	
	#region AdminTaskEmailRecipientsListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailRecipientsListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientsListQuery : AdminTaskEmailRecipientsListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListQuery class.
		/// </summary>
		public AdminTaskEmailRecipientsListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientsListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientsListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientsListQuery
		
	#region AdminTaskEmailTemplateListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListFilters : AdminTaskEmailTemplateListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListFilters class.
		/// </summary>
		public AdminTaskEmailTemplateListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListFilters
	
	#region AdminTaskEmailTemplateListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailTemplateListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListQuery : AdminTaskEmailTemplateListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListQuery class.
		/// </summary>
		public AdminTaskEmailTemplateListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListQuery
		
	#region AdminTaskEmailTemplateListWithAttachmentsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListWithAttachmentsFilters : AdminTaskEmailTemplateListWithAttachmentsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsFilters class.
		/// </summary>
		public AdminTaskEmailTemplateListWithAttachmentsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListWithAttachmentsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListWithAttachmentsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListWithAttachmentsFilters
	
	#region AdminTaskEmailTemplateListWithAttachmentsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskEmailTemplateListWithAttachmentsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListWithAttachmentsQuery : AdminTaskEmailTemplateListWithAttachmentsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsQuery class.
		/// </summary>
		public AdminTaskEmailTemplateListWithAttachmentsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListWithAttachmentsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListWithAttachmentsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListWithAttachmentsQuery
		
	#region AdminTaskHistoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskHistoryFilters : AdminTaskHistoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryFilters class.
		/// </summary>
		public AdminTaskHistoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskHistoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskHistoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskHistoryFilters
	
	#region AdminTaskHistoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AdminTaskHistoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AdminTaskHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskHistoryQuery : AdminTaskHistoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryQuery class.
		/// </summary>
		public AdminTaskHistoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskHistoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskHistoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskHistoryQuery
		
	#region CompaniesActiveFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesActiveFilters : CompaniesActiveFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveFilters class.
		/// </summary>
		public CompaniesActiveFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesActiveFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesActiveFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesActiveFilters
	
	#region CompaniesActiveQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesActiveParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesActiveQuery : CompaniesActiveParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveQuery class.
		/// </summary>
		public CompaniesActiveQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesActiveQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesActiveQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesActiveQuery
		
	#region CompaniesEhsConsultantsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsConsultantsFilters : CompaniesEhsConsultantsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsFilters class.
		/// </summary>
		public CompaniesEhsConsultantsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsConsultantsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsConsultantsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsConsultantsFilters
	
	#region CompaniesEhsConsultantsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesEhsConsultantsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsConsultantsQuery : CompaniesEhsConsultantsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsQuery class.
		/// </summary>
		public CompaniesEhsConsultantsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsConsultantsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsConsultantsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsConsultantsQuery
		
	#region CompaniesEhsimsMapSitesEhsimsIhsListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapSitesEhsimsIhsListFilters : CompaniesEhsimsMapSitesEhsimsIhsListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListFilters class.
		/// </summary>
		public CompaniesEhsimsMapSitesEhsimsIhsListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapSitesEhsimsIhsListFilters
	
	#region CompaniesEhsimsMapSitesEhsimsIhsListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapSitesEhsimsIhsListQuery : CompaniesEhsimsMapSitesEhsimsIhsListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListQuery class.
		/// </summary>
		public CompaniesEhsimsMapSitesEhsimsIhsListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesEhsimsMapSitesEhsimsIhsListQuery
		
	#region CompaniesHrCrpDataListAllFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataListAllFilters : CompaniesHrCrpDataListAllFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllFilters class.
		/// </summary>
		public CompaniesHrCrpDataListAllFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataListAllFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataListAllFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataListAllFilters
	
	#region CompaniesHrCrpDataListAllQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompaniesHrCrpDataListAllParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataListAllQuery : CompaniesHrCrpDataListAllParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllQuery class.
		/// </summary>
		public CompaniesHrCrpDataListAllQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataListAllQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataListAllQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataListAllQuery
		
	#region CompanySiteCategoryStandardDetailsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardDetailsFilters : CompanySiteCategoryStandardDetailsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsFilters class.
		/// </summary>
		public CompanySiteCategoryStandardDetailsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardDetailsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardDetailsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardDetailsFilters
	
	#region CompanySiteCategoryStandardDetailsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryStandardDetailsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardDetailsQuery : CompanySiteCategoryStandardDetailsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsQuery class.
		/// </summary>
		public CompanySiteCategoryStandardDetailsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardDetailsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardDetailsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardDetailsQuery
		
	#region CompanySiteCategoryStandardNamesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardNamesFilters : CompanySiteCategoryStandardNamesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesFilters class.
		/// </summary>
		public CompanySiteCategoryStandardNamesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardNamesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardNamesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardNamesFilters
	
	#region CompanySiteCategoryStandardNamesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryStandardNamesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardNamesQuery : CompanySiteCategoryStandardNamesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesQuery class.
		/// </summary>
		public CompanySiteCategoryStandardNamesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardNamesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardNamesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardNamesQuery
		
	#region CompanySiteCategoryStandardRegionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardRegionFilters : CompanySiteCategoryStandardRegionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionFilters class.
		/// </summary>
		public CompanySiteCategoryStandardRegionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardRegionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardRegionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardRegionFilters
	
	#region CompanySiteCategoryStandardRegionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CompanySiteCategoryStandardRegionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardRegionQuery : CompanySiteCategoryStandardRegionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionQuery class.
		/// </summary>
		public CompanySiteCategoryStandardRegionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardRegionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardRegionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardRegionQuery
		
	#region ConfigSa812SitesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812SitesFilters : ConfigSa812SitesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesFilters class.
		/// </summary>
		public ConfigSa812SitesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812SitesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812SitesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812SitesFilters
	
	#region ConfigSa812SitesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ConfigSa812SitesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ConfigSa812Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812SitesQuery : ConfigSa812SitesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesQuery class.
		/// </summary>
		public ConfigSa812SitesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812SitesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812SitesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812SitesQuery
		
	#region CsaCompanySiteCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaCompanySiteCategoryFilters : CsaCompanySiteCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryFilters class.
		/// </summary>
		public CsaCompanySiteCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaCompanySiteCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaCompanySiteCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaCompanySiteCategoryFilters
	
	#region CsaCompanySiteCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsaCompanySiteCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaCompanySiteCategoryQuery : CsaCompanySiteCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryQuery class.
		/// </summary>
		public CsaCompanySiteCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaCompanySiteCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaCompanySiteCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaCompanySiteCategoryQuery
		
	#region CsmsEmailLogListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogListFilters : CsmsEmailLogListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListFilters class.
		/// </summary>
		public CsmsEmailLogListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogListFilters
	
	#region CsmsEmailLogListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CsmsEmailLogListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogListQuery : CsmsEmailLogListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListQuery class.
		/// </summary>
		public CsmsEmailLogListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogListQuery
		
	#region EbiLastTimeOnSiteFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteFilters : EbiLastTimeOnSiteFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteFilters class.
		/// </summary>
		public EbiLastTimeOnSiteFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteFilters
	
	#region EbiLastTimeOnSiteQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiLastTimeOnSiteParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteQuery : EbiLastTimeOnSiteParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteQuery class.
		/// </summary>
		public EbiLastTimeOnSiteQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteQuery
		
	#region EbiLastTimeOnSiteDistinctFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctFilters : EbiLastTimeOnSiteDistinctFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctFilters class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctFilters
	
	#region EbiLastTimeOnSiteDistinctQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiLastTimeOnSiteDistinctParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctQuery : EbiLastTimeOnSiteDistinctParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctQuery class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctQuery
		
	#region EbiLastTimeOnSiteDistinctAnySiteFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctAnySiteFilters : EbiLastTimeOnSiteDistinctAnySiteFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteFilters class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctAnySiteFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctAnySiteFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctAnySiteFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctAnySiteFilters
	
	#region EbiLastTimeOnSiteDistinctAnySiteQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EbiLastTimeOnSiteDistinctAnySiteParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctAnySiteQuery : EbiLastTimeOnSiteDistinctAnySiteParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteQuery class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctAnySiteQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctAnySiteQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctAnySiteQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctAnySiteQuery
		
	#region EhsimsExceptionsCompaniesSitesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsCompaniesSitesFilters : EhsimsExceptionsCompaniesSitesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesFilters class.
		/// </summary>
		public EhsimsExceptionsCompaniesSitesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsCompaniesSitesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsCompaniesSitesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsCompaniesSitesFilters
	
	#region EhsimsExceptionsCompaniesSitesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EhsimsExceptionsCompaniesSitesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptionsCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsCompaniesSitesQuery : EhsimsExceptionsCompaniesSitesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesQuery class.
		/// </summary>
		public EhsimsExceptionsCompaniesSitesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsimsExceptionsCompaniesSitesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsCompaniesSitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsimsExceptionsCompaniesSitesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsimsExceptionsCompaniesSitesQuery
		
	#region EmailLogAutoFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogAutoFilters : EmailLogAutoFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoFilters class.
		/// </summary>
		public EmailLogAutoFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogAutoFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogAutoFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogAutoFilters
	
	#region EmailLogAutoQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EmailLogAutoParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EmailLogAuto"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogAutoQuery : EmailLogAutoParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoQuery class.
		/// </summary>
		public EmailLogAutoQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogAutoQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogAutoQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogAutoQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogAutoQuery
		
	#region EmailLogIhsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogIhs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogIhsFilters : EmailLogIhsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsFilters class.
		/// </summary>
		public EmailLogIhsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogIhsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogIhsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogIhsFilters
	
	#region EmailLogIhsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EmailLogIhsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EmailLogIhs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogIhsQuery : EmailLogIhsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsQuery class.
		/// </summary>
		public EmailLogIhsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EmailLogIhsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EmailLogIhsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EmailLogIhsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EmailLogIhsQuery
		
	#region EnumQuestionnaireMainAnswerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainAnswerFilters : EnumQuestionnaireMainAnswerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerFilters class.
		/// </summary>
		public EnumQuestionnaireMainAnswerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainAnswerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainAnswerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainAnswerFilters
	
	#region EnumQuestionnaireMainAnswerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EnumQuestionnaireMainAnswerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainAnswerQuery : EnumQuestionnaireMainAnswerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerQuery class.
		/// </summary>
		public EnumQuestionnaireMainAnswerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainAnswerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainAnswerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainAnswerQuery
		
	#region EnumQuestionnaireMainQuestionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionFilters : EnumQuestionnaireMainQuestionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionFilters class.
		/// </summary>
		public EnumQuestionnaireMainQuestionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionFilters
	
	#region EnumQuestionnaireMainQuestionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EnumQuestionnaireMainQuestionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionQuery : EnumQuestionnaireMainQuestionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionQuery class.
		/// </summary>
		public EnumQuestionnaireMainQuestionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionQuery
		
	#region EnumQuestionnaireMainQuestionAnswerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionAnswerFilters : EnumQuestionnaireMainQuestionAnswerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerFilters class.
		/// </summary>
		public EnumQuestionnaireMainQuestionAnswerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionAnswerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionAnswerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionAnswerFilters
	
	#region EnumQuestionnaireMainQuestionAnswerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="EnumQuestionnaireMainQuestionAnswerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionAnswerQuery : EnumQuestionnaireMainQuestionAnswerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerQuery class.
		/// </summary>
		public EnumQuestionnaireMainQuestionAnswerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionAnswerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionAnswerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionAnswerQuery
		
	#region FileDbUsersCompaniesSitesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbUsersCompaniesSitesFilters : FileDbUsersCompaniesSitesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesFilters class.
		/// </summary>
		public FileDbUsersCompaniesSitesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbUsersCompaniesSitesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbUsersCompaniesSitesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbUsersCompaniesSitesFilters
	
	#region FileDbUsersCompaniesSitesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileDbUsersCompaniesSitesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbUsersCompaniesSitesQuery : FileDbUsersCompaniesSitesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesQuery class.
		/// </summary>
		public FileDbUsersCompaniesSitesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbUsersCompaniesSitesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbUsersCompaniesSitesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbUsersCompaniesSitesQuery
		
	#region FileVaultListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListFilters : FileVaultListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListFilters class.
		/// </summary>
		public FileVaultListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListFilters
	
	#region FileVaultListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListQuery : FileVaultListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListQuery class.
		/// </summary>
		public FileVaultListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListQuery
		
	#region FileVaultListHelpFilesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListHelpFilesFilters : FileVaultListHelpFilesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesFilters class.
		/// </summary>
		public FileVaultListHelpFilesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListHelpFilesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListHelpFilesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListHelpFilesFilters
	
	#region FileVaultListHelpFilesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultListHelpFilesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListHelpFilesQuery : FileVaultListHelpFilesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesQuery class.
		/// </summary>
		public FileVaultListHelpFilesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListHelpFilesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListHelpFilesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListHelpFilesQuery
		
	#region FileVaultListSqExemptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListSqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListSqExemptionFilters : FileVaultListSqExemptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionFilters class.
		/// </summary>
		public FileVaultListSqExemptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListSqExemptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListSqExemptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListSqExemptionFilters
	
	#region FileVaultListSqExemptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultListSqExemptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultListSqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListSqExemptionQuery : FileVaultListSqExemptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionQuery class.
		/// </summary>
		public FileVaultListSqExemptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListSqExemptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListSqExemptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListSqExemptionQuery
		
	#region FileVaultTableListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListFilters : FileVaultTableListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListFilters class.
		/// </summary>
		public FileVaultTableListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableListFilters
	
	#region FileVaultTableListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultTableListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListQuery : FileVaultTableListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListQuery class.
		/// </summary>
		public FileVaultTableListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableListQuery
		
	#region FileVaultTableListHelpFilesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListHelpFilesFilters : FileVaultTableListHelpFilesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesFilters class.
		/// </summary>
		public FileVaultTableListHelpFilesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableListHelpFilesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableListHelpFilesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableListHelpFilesFilters
	
	#region FileVaultTableListHelpFilesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FileVaultTableListHelpFilesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FileVaultTableListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListHelpFilesQuery : FileVaultTableListHelpFilesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesQuery class.
		/// </summary>
		public FileVaultTableListHelpFilesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableListHelpFilesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableListHelpFilesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableListHelpFilesQuery
		
	#region KpiCompanySiteCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiCompanySiteCategoryFilters : KpiCompanySiteCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryFilters class.
		/// </summary>
		public KpiCompanySiteCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiCompanySiteCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiCompanySiteCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiCompanySiteCategoryFilters
	
	#region KpiCompanySiteCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiCompanySiteCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiCompanySiteCategoryQuery : KpiCompanySiteCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryQuery class.
		/// </summary>
		public KpiCompanySiteCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiCompanySiteCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiCompanySiteCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiCompanySiteCategoryQuery
		
	#region KpiEngineeringProjectHoursFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiEngineeringProjectHoursFilters : KpiEngineeringProjectHoursFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursFilters class.
		/// </summary>
		public KpiEngineeringProjectHoursFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiEngineeringProjectHoursFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiEngineeringProjectHoursFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiEngineeringProjectHoursFilters
	
	#region KpiEngineeringProjectHoursQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiEngineeringProjectHoursParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiEngineeringProjectHoursQuery : KpiEngineeringProjectHoursParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursQuery class.
		/// </summary>
		public KpiEngineeringProjectHoursQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiEngineeringProjectHoursQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiEngineeringProjectHoursQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiEngineeringProjectHoursQuery
		
	#region KpiListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiListFilters : KpiListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListFilters class.
		/// </summary>
		public KpiListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiListFilters
	
	#region KpiListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiListQuery : KpiListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListQuery class.
		/// </summary>
		public KpiListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiListQuery
		
	#region KpiProjectListOpenFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListOpenFilters : KpiProjectListOpenFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenFilters class.
		/// </summary>
		public KpiProjectListOpenFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListOpenFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListOpenFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListOpenFilters
	
	#region KpiProjectListOpenQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiProjectListOpenParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListOpenQuery : KpiProjectListOpenParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenQuery class.
		/// </summary>
		public KpiProjectListOpenQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiProjectListOpenQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiProjectListOpenQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiProjectListOpenQuery
		
	#region KpiPurchaseOrderListOpenProjectsAscFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsAscFilters : KpiPurchaseOrderListOpenProjectsAscFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscFilters class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsAscFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsAscFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsAscFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsAscFilters
	
	#region KpiPurchaseOrderListOpenProjectsAscQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiPurchaseOrderListOpenProjectsAscParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsAscQuery : KpiPurchaseOrderListOpenProjectsAscParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscQuery class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsAscQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsAscQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsAscQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsAscQuery
		
	#region KpiPurchaseOrderListOpenProjectsDistinctAscFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscFilters : KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscFilters class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAscFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscFilters
	
	#region KpiPurchaseOrderListOpenProjectsDistinctAscQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscQuery : KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscQuery class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAscQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscQuery
		
	#region QuestionnaireActionLogFriendlyFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFriendlyFilters : QuestionnaireActionLogFriendlyFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyFilters class.
		/// </summary>
		public QuestionnaireActionLogFriendlyFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogFriendlyFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogFriendlyFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogFriendlyFilters
	
	#region QuestionnaireActionLogFriendlyQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireActionLogFriendlyParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFriendlyQuery : QuestionnaireActionLogFriendlyParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyQuery class.
		/// </summary>
		public QuestionnaireActionLogFriendlyQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogFriendlyQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogFriendlyQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogFriendlyQuery
		
	#region QuestionnaireContactProcurementFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContactProcurementFilters : QuestionnaireContactProcurementFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementFilters class.
		/// </summary>
		public QuestionnaireContactProcurementFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContactProcurementFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContactProcurementFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContactProcurementFilters
	
	#region QuestionnaireContactProcurementQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireContactProcurementParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContactProcurementQuery : QuestionnaireContactProcurementParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementQuery class.
		/// </summary>
		public QuestionnaireContactProcurementQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContactProcurementQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContactProcurementQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContactProcurementQuery
		
	#region QuestionnaireEscalationHsAssessorListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationHsAssessorListFilters : QuestionnaireEscalationHsAssessorListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListFilters class.
		/// </summary>
		public QuestionnaireEscalationHsAssessorListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationHsAssessorListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationHsAssessorListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationHsAssessorListFilters
	
	#region QuestionnaireEscalationHsAssessorListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireEscalationHsAssessorListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationHsAssessorListQuery : QuestionnaireEscalationHsAssessorListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListQuery class.
		/// </summary>
		public QuestionnaireEscalationHsAssessorListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationHsAssessorListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationHsAssessorListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationHsAssessorListQuery
		
	#region QuestionnaireEscalationProcurementListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationProcurementListFilters : QuestionnaireEscalationProcurementListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListFilters class.
		/// </summary>
		public QuestionnaireEscalationProcurementListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationProcurementListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationProcurementListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationProcurementListFilters
	
	#region QuestionnaireEscalationProcurementListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireEscalationProcurementListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationProcurementListQuery : QuestionnaireEscalationProcurementListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListQuery class.
		/// </summary>
		public QuestionnaireEscalationProcurementListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireEscalationProcurementListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationProcurementListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireEscalationProcurementListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireEscalationProcurementListQuery
		
	#region QuestionnaireMainQuestionAnswerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionAnswerFilters : QuestionnaireMainQuestionAnswerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerFilters class.
		/// </summary>
		public QuestionnaireMainQuestionAnswerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionAnswerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionAnswerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionAnswerFilters
	
	#region QuestionnaireMainQuestionAnswerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireMainQuestionAnswerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestionAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionAnswerQuery : QuestionnaireMainQuestionAnswerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerQuery class.
		/// </summary>
		public QuestionnaireMainQuestionAnswerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainQuestionAnswerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionAnswerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainQuestionAnswerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainQuestionAnswerQuery
		
	#region QuestionnairePresentlyWithUsersActionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersActionsFilters : QuestionnairePresentlyWithUsersActionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsFilters class.
		/// </summary>
		public QuestionnairePresentlyWithUsersActionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersActionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersActionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersActionsFilters
	
	#region QuestionnairePresentlyWithUsersActionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnairePresentlyWithUsersActionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersActionsQuery : QuestionnairePresentlyWithUsersActionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsQuery class.
		/// </summary>
		public QuestionnairePresentlyWithUsersActionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersActionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersActionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersActionsQuery
		
	#region QuestionnaireReportDaysSinceActivityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportDaysSinceActivityFilters : QuestionnaireReportDaysSinceActivityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityFilters class.
		/// </summary>
		public QuestionnaireReportDaysSinceActivityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportDaysSinceActivityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportDaysSinceActivityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportDaysSinceActivityFilters
	
	#region QuestionnaireReportDaysSinceActivityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportDaysSinceActivityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportDaysSinceActivityQuery : QuestionnaireReportDaysSinceActivityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityQuery class.
		/// </summary>
		public QuestionnaireReportDaysSinceActivityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportDaysSinceActivityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportDaysSinceActivityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportDaysSinceActivityQuery
		
	#region QuestionnaireReportExpiryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportExpiryFilters : QuestionnaireReportExpiryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryFilters class.
		/// </summary>
		public QuestionnaireReportExpiryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportExpiryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportExpiryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportExpiryFilters
	
	#region QuestionnaireReportExpiryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportExpiryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportExpiry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportExpiryQuery : QuestionnaireReportExpiryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryQuery class.
		/// </summary>
		public QuestionnaireReportExpiryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportExpiryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportExpiryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportExpiryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportExpiryQuery
		
	#region QuestionnaireReportOverviewFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewFilters : QuestionnaireReportOverviewFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewFilters class.
		/// </summary>
		public QuestionnaireReportOverviewFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewFilters
	
	#region QuestionnaireReportOverviewQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportOverviewParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewQuery : QuestionnaireReportOverviewParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewQuery class.
		/// </summary>
		public QuestionnaireReportOverviewQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewQuery
		
	#region QuestionnaireReportOverviewSubQuery1Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery1Filters : QuestionnaireReportOverviewSubQuery1FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1Filters class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery1Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery1Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery1Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery1Filters
	
	#region QuestionnaireReportOverviewSubQuery1Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportOverviewSubQuery1ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery1Query : QuestionnaireReportOverviewSubQuery1ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1Query class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery1Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery1Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery1Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery1Query
		
	#region QuestionnaireReportOverviewSubQuery2Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery2Filters : QuestionnaireReportOverviewSubQuery2FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2Filters class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery2Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery2Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery2Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery2Filters
	
	#region QuestionnaireReportOverviewSubQuery2Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportOverviewSubQuery2ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery2Query : QuestionnaireReportOverviewSubQuery2ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2Query class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery2Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery2Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery2Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery2Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery2Query
		
	#region QuestionnaireReportOverviewSubQuery3Filters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery3Filters : QuestionnaireReportOverviewSubQuery3FilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Filters class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3Filters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery3Filters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Filters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery3Filters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery3Filters
	
	#region QuestionnaireReportOverviewSubQuery3Query
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportOverviewSubQuery3ParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery3"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery3Query : QuestionnaireReportOverviewSubQuery3ParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Query class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery3Query() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubQuery3Query(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery3Query class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubQuery3Query(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubQuery3Query
		
	#region QuestionnaireReportOverviewSubContractorsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubContractorsFilters : QuestionnaireReportOverviewSubContractorsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsFilters class.
		/// </summary>
		public QuestionnaireReportOverviewSubContractorsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubContractorsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubContractorsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubContractorsFilters
	
	#region QuestionnaireReportOverviewSubContractorsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportOverviewSubContractorsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubContractorsQuery : QuestionnaireReportOverviewSubContractorsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsQuery class.
		/// </summary>
		public QuestionnaireReportOverviewSubContractorsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubContractorsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubContractorsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubContractorsQuery
		
	#region QuestionnaireReportProgressFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportProgressFilters : QuestionnaireReportProgressFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressFilters class.
		/// </summary>
		public QuestionnaireReportProgressFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportProgressFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportProgressFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportProgressFilters
	
	#region QuestionnaireReportProgressQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportProgressParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportProgressQuery : QuestionnaireReportProgressParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressQuery class.
		/// </summary>
		public QuestionnaireReportProgressQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportProgressQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportProgressQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportProgressQuery
		
	#region QuestionnaireReportServicesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServices"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesFilters : QuestionnaireReportServicesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesFilters class.
		/// </summary>
		public QuestionnaireReportServicesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesFilters
	
	#region QuestionnaireReportServicesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportServicesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServices"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesQuery : QuestionnaireReportServicesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesQuery class.
		/// </summary>
		public QuestionnaireReportServicesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesQuery
		
	#region QuestionnaireReportServicesAllFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesAllFilters : QuestionnaireReportServicesAllFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllFilters class.
		/// </summary>
		public QuestionnaireReportServicesAllFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesAllFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesAllFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesAllFilters
	
	#region QuestionnaireReportServicesAllQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportServicesAllParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesAllQuery : QuestionnaireReportServicesAllParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllQuery class.
		/// </summary>
		public QuestionnaireReportServicesAllQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesAllQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesAllQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesAllQuery
		
	#region QuestionnaireReportServicesListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListFilters : QuestionnaireReportServicesListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListFilters class.
		/// </summary>
		public QuestionnaireReportServicesListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListFilters
	
	#region QuestionnaireReportServicesListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportServicesListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListQuery : QuestionnaireReportServicesListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListQuery class.
		/// </summary>
		public QuestionnaireReportServicesListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListQuery
		
	#region QuestionnaireReportServicesListAllFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListAllFilters : QuestionnaireReportServicesListAllFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllFilters class.
		/// </summary>
		public QuestionnaireReportServicesListAllFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListAllFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListAllFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListAllFilters
	
	#region QuestionnaireReportServicesListAllQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportServicesListAllParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesListAllQuery : QuestionnaireReportServicesListAllParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllQuery class.
		/// </summary>
		public QuestionnaireReportServicesListAllQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesListAllQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesListAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesListAllQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesListAllQuery
		
	#region QuestionnaireReportServicesOtherFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherFilters : QuestionnaireReportServicesOtherFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherFilters class.
		/// </summary>
		public QuestionnaireReportServicesOtherFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesOtherFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesOtherFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesOtherFilters
	
	#region QuestionnaireReportServicesOtherQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportServicesOtherParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOther"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherQuery : QuestionnaireReportServicesOtherParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherQuery class.
		/// </summary>
		public QuestionnaireReportServicesOtherQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesOtherQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesOtherQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesOtherQuery
		
	#region QuestionnaireReportServicesOtherAllFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOtherAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherAllFilters : QuestionnaireReportServicesOtherAllFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllFilters class.
		/// </summary>
		public QuestionnaireReportServicesOtherAllFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesOtherAllFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesOtherAllFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesOtherAllFilters
	
	#region QuestionnaireReportServicesOtherAllQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportServicesOtherAllParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOtherAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherAllQuery : QuestionnaireReportServicesOtherAllParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllQuery class.
		/// </summary>
		public QuestionnaireReportServicesOtherAllQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesOtherAllQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesOtherAllQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesOtherAllQuery
		
	#region QuestionnaireReportTimeDelayAssessmentCompleteFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayAssessmentCompleteFilters : QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteFilters class.
		/// </summary>
		public QuestionnaireReportTimeDelayAssessmentCompleteFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayAssessmentCompleteFilters
	
	#region QuestionnaireReportTimeDelayAssessmentCompleteQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayAssessmentCompleteQuery : QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteQuery class.
		/// </summary>
		public QuestionnaireReportTimeDelayAssessmentCompleteQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayAssessmentCompleteQuery
		
	#region QuestionnaireReportTimeDelayBeingAssessedFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayBeingAssessedFilters : QuestionnaireReportTimeDelayBeingAssessedFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedFilters class.
		/// </summary>
		public QuestionnaireReportTimeDelayBeingAssessedFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayBeingAssessedFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayBeingAssessedFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayBeingAssessedFilters
	
	#region QuestionnaireReportTimeDelayBeingAssessedQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportTimeDelayBeingAssessedParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayBeingAssessedQuery : QuestionnaireReportTimeDelayBeingAssessedParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedQuery class.
		/// </summary>
		public QuestionnaireReportTimeDelayBeingAssessedQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayBeingAssessedQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayBeingAssessedQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayBeingAssessedQuery
		
	#region QuestionnaireReportTimeDelayWithSupplierFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayWithSupplierFilters : QuestionnaireReportTimeDelayWithSupplierFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierFilters class.
		/// </summary>
		public QuestionnaireReportTimeDelayWithSupplierFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayWithSupplierFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayWithSupplierFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayWithSupplierFilters
	
	#region QuestionnaireReportTimeDelayWithSupplierQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireReportTimeDelayWithSupplierParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayWithSupplierQuery : QuestionnaireReportTimeDelayWithSupplierParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierQuery class.
		/// </summary>
		public QuestionnaireReportTimeDelayWithSupplierQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayWithSupplierQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayWithSupplierQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayWithSupplierQuery
		
	#region QuestionnaireWithLocationApprovalViewFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewFilters : QuestionnaireWithLocationApprovalViewFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewFilters class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewFilters
	
	#region QuestionnaireWithLocationApprovalViewQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireWithLocationApprovalViewParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewQuery : QuestionnaireWithLocationApprovalViewParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewQuery class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewQuery
		
	#region QuestionnaireWithLocationApprovalViewLatestFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewLatestFilters : QuestionnaireWithLocationApprovalViewLatestFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestFilters class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewLatestFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewLatestFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewLatestFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewLatestFilters
	
	#region QuestionnaireWithLocationApprovalViewLatestQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="QuestionnaireWithLocationApprovalViewLatestParameterBuilder"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalViewLatest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewLatestQuery : QuestionnaireWithLocationApprovalViewLatestParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestQuery class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewLatestQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewLatestQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewLatestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewLatestQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewLatestQuery
		
	#region SitesEhsimsIhsListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesEhsimsIhsListFilters : SitesEhsimsIhsListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListFilters class.
		/// </summary>
		public SitesEhsimsIhsListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesEhsimsIhsListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesEhsimsIhsListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesEhsimsIhsListFilters
	
	#region SitesEhsimsIhsListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SitesEhsimsIhsListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesEhsimsIhsListQuery : SitesEhsimsIhsListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListQuery class.
		/// </summary>
		public SitesEhsimsIhsListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesEhsimsIhsListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesEhsimsIhsListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesEhsimsIhsListQuery
		
	#region SqExemptionFriendlyAllFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyAllFilters : SqExemptionFriendlyAllFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllFilters class.
		/// </summary>
		public SqExemptionFriendlyAllFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyAllFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyAllFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyAllFilters
	
	#region SqExemptionFriendlyAllQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqExemptionFriendlyAllParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyAllQuery : SqExemptionFriendlyAllParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllQuery class.
		/// </summary>
		public SqExemptionFriendlyAllQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyAllQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyAllQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyAllQuery
		
	#region SqExemptionFriendlyCurrentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyCurrentFilters : SqExemptionFriendlyCurrentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentFilters class.
		/// </summary>
		public SqExemptionFriendlyCurrentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyCurrentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyCurrentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyCurrentFilters
	
	#region SqExemptionFriendlyCurrentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqExemptionFriendlyCurrentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyCurrentQuery : SqExemptionFriendlyCurrentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentQuery class.
		/// </summary>
		public SqExemptionFriendlyCurrentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyCurrentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyCurrentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyCurrentQuery
		
	#region SqExemptionFriendlyExpiredFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyExpired"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyExpiredFilters : SqExemptionFriendlyExpiredFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredFilters class.
		/// </summary>
		public SqExemptionFriendlyExpiredFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyExpiredFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyExpiredFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyExpiredFilters
	
	#region SqExemptionFriendlyExpiredQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqExemptionFriendlyExpiredParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyExpired"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyExpiredQuery : SqExemptionFriendlyExpiredParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredQuery class.
		/// </summary>
		public SqExemptionFriendlyExpiredQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyExpiredQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyExpiredQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyExpiredQuery
		
	#region UsersAlcoanFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAlcoan"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAlcoanFilters : UsersAlcoanFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanFilters class.
		/// </summary>
		public UsersAlcoanFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersAlcoanFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersAlcoanFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersAlcoanFilters
	
	#region UsersAlcoanQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersAlcoanParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersAlcoan"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAlcoanQuery : UsersAlcoanParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanQuery class.
		/// </summary>
		public UsersAlcoanQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersAlcoanQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersAlcoanQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersAlcoanQuery
		
	#region UsersCompaniesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompanies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesFilters : UsersCompaniesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesFilters class.
		/// </summary>
		public UsersCompaniesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesFilters
	
	#region UsersCompaniesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersCompaniesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersCompanies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesQuery : UsersCompaniesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesQuery class.
		/// </summary>
		public UsersCompaniesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesQuery
		
	#region UsersCompaniesActiveAllFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveAllFilters : UsersCompaniesActiveAllFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllFilters class.
		/// </summary>
		public UsersCompaniesActiveAllFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveAllFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveAllFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveAllFilters
	
	#region UsersCompaniesActiveAllQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersCompaniesActiveAllParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveAllQuery : UsersCompaniesActiveAllParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllQuery class.
		/// </summary>
		public UsersCompaniesActiveAllQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveAllQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveAllQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveAllQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveAllQuery
		
	#region UsersCompaniesActiveContractorsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveContractorsFilters : UsersCompaniesActiveContractorsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsFilters class.
		/// </summary>
		public UsersCompaniesActiveContractorsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveContractorsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveContractorsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveContractorsFilters
	
	#region UsersCompaniesActiveContractorsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersCompaniesActiveContractorsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveContractorsQuery : UsersCompaniesActiveContractorsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsQuery class.
		/// </summary>
		public UsersCompaniesActiveContractorsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveContractorsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveContractorsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveContractorsQuery
		
	#region UsersEhsConsultantsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEhsConsultantsFilters : UsersEhsConsultantsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsFilters class.
		/// </summary>
		public UsersEhsConsultantsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEhsConsultantsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEhsConsultantsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEhsConsultantsFilters
	
	#region UsersEhsConsultantsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersEhsConsultantsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEhsConsultantsQuery : UsersEhsConsultantsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsQuery class.
		/// </summary>
		public UsersEhsConsultantsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEhsConsultantsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEhsConsultantsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEhsConsultantsQuery
		
	#region UsersFullNameFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersFullName"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFullNameFilters : UsersFullNameFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFullNameFilters class.
		/// </summary>
		public UsersFullNameFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersFullNameFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersFullNameFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersFullNameFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersFullNameFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersFullNameFilters
	
	#region UsersFullNameQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersFullNameParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersFullName"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersFullNameQuery : UsersFullNameParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersFullNameQuery class.
		/// </summary>
		public UsersFullNameQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersFullNameQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersFullNameQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersFullNameQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersFullNameQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersFullNameQuery
		
	#region UsersHsAssessorListWithLocationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersHsAssessorListWithLocationFilters : UsersHsAssessorListWithLocationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationFilters class.
		/// </summary>
		public UsersHsAssessorListWithLocationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersHsAssessorListWithLocationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersHsAssessorListWithLocationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersHsAssessorListWithLocationFilters
	
	#region UsersHsAssessorListWithLocationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersHsAssessorListWithLocationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersHsAssessorListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersHsAssessorListWithLocationQuery : UsersHsAssessorListWithLocationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationQuery class.
		/// </summary>
		public UsersHsAssessorListWithLocationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersHsAssessorListWithLocationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersHsAssessorListWithLocationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersHsAssessorListWithLocationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersHsAssessorListWithLocationQuery
		
	#region UsersProcurementEscalationListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementEscalationListFilters : UsersProcurementEscalationListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListFilters class.
		/// </summary>
		public UsersProcurementEscalationListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementEscalationListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementEscalationListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementEscalationListFilters
	
	#region UsersProcurementEscalationListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersProcurementEscalationListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersProcurementEscalationList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementEscalationListQuery : UsersProcurementEscalationListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListQuery class.
		/// </summary>
		public UsersProcurementEscalationListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementEscalationListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementEscalationListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementEscalationListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementEscalationListQuery
		
	#region UsersProcurementListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListFilters : UsersProcurementListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListFilters class.
		/// </summary>
		public UsersProcurementListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListFilters
	
	#region UsersProcurementListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersProcurementListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListQuery : UsersProcurementListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListQuery class.
		/// </summary>
		public UsersProcurementListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListQuery
		
	#region UsersProcurementListWithLocationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListWithLocationFilters : UsersProcurementListWithLocationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationFilters class.
		/// </summary>
		public UsersProcurementListWithLocationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListWithLocationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListWithLocationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListWithLocationFilters
	
	#region UsersProcurementListWithLocationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UsersProcurementListWithLocationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListWithLocationQuery : UsersProcurementListWithLocationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationQuery class.
		/// </summary>
		public UsersProcurementListWithLocationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListWithLocationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListWithLocationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListWithLocationQuery

    #region CompanyNoteFilters

    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
    /// that is used exclusively with a <see cref="CompanyNote"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanyNoteFilters : CompanyNoteFilterBuilder
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CompanyNoteFilters class.
        /// </summary>
        public CompanyNoteFilters() : base() { }

        /// <summary>
        /// Initializes a new instance of the CompanyNoteFilters class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        public CompanyNoteFilters(bool ignoreCase) : base(ignoreCase) { }

        /// <summary>
        /// Initializes a new instance of the CompanyNoteFilters class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        /// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public CompanyNoteFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

        #endregion Constructors
    }

    #endregion CompanyNoteFilters

    #region CompanyNoteQuery

    /// <summary>
    /// A strongly-typed instance of the <see cref="CompanyNoteParameterBuilder"/> class
    /// that is used exclusively with a <see cref="CompanyNote"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanyNoteQuery : CompanyNoteParameterBuilder
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CompanyNoteQuery class.
        /// </summary>
        public CompanyNoteQuery() : base() { }

        /// <summary>
        /// Initializes a new instance of the CompanyNoteQuery class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        public CompanyNoteQuery(bool ignoreCase) : base(ignoreCase) { }

        /// <summary>
        /// Initializes a new instance of the CompanyNoteQuery class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        /// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public CompanyNoteQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

        #endregion Constructors
    }

    #endregion CompanyNoteQuery
    #endregion

	
}
