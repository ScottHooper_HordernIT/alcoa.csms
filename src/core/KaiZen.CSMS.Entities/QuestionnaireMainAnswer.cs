﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireMainAnswer' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireMainAnswer : QuestionnaireMainAnswerBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireMainAnswer"/> instance.
		///</summary>
		public QuestionnaireMainAnswer():base(){}	
		
		#endregion
	}
}
