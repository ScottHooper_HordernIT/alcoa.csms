﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SafetyPlans" Codebehind="SafetyPlans.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dxm" %>
<%--commented part ends for DT 3443--%>
<%--<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxTimer"
    TagPrefix="dxt" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>--%>
<%--commented part ends for DT 3443--%>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<%--implemented part starts for DT 3343--%>
<style type="text/css">
    
    .loadingImage
    {
        src: "~/App_Themes/Blue/Web/Loading.gif";
    }
    .editButtonImage
    {
        src: "~/Images/gridEdit.gif";
    }
    .newButtonImage
    {
        src: "~/Images/gridNew.gif";
    }
    .deleteButtonImage
    {
        src: "~/Images/gridDelete.gif";
    }
    .cancelButtonImage
    {
        src: "~/Images/gridCancel.gif";
    }
    .saveButtonImage
    {
        src: "~/Images/gridSave.gif";
    }
</style>
<%--implemented part ends for DT 3343--%>


<div style="text-align: left">
    <table border="0" cellpadding="2" cellspacing="0" width="900px">
        <tr>
            <td class="pageName" colspan="3" style="height: 43px">
                <span class="bodycopy"><span class="title">Australian Safety Management Plan Process</span><br />
                    <span class="date">
                        <asp:Label ID="lblYour" runat="server" Text="Your Company's Safety Management Plan Assessor is: "></asp:Label>
                        <asp:Label ID="lblAlcoaContact" runat="server" Font-Bold="True" ForeColor="#000000"
                            Text="[Currently Not Designated]"></asp:Label></span><br />
                    <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
                </span>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                    <tr>
                        <td colspan="7" style="height: 35px; width: 830px; text-align: center; text-align: -moz-center;"
                            align="center">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="width: 100%; height: 40px; text-align: center; text-align: -moz-center;"
                                        align="center">
                                        <div align="center">
                                            <dxe:ASPxButton ID="btnSubmitSE" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" Text="Create Questionnaire & Safety Management Plan"
                                                Width="372px" OnClick="btnSubmitSE_Click" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            </dxe:ASPxButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right" class="pageName" style="width: 70px; height: 35px; text-align: right;">
                            <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: underline">
                                Customize</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" style="height: 68px">
                            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" DataSourceID="FileDBDataSource" AutoGenerateColumns="False"
                                KeyFieldName="FileId" OnCellEditorInitialize="grid_CellEditorInitialize" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                OnRowUpdating="grid_RowUpdating" OnHtmlRowCreated="grid_RowCreated"
                                Width="900px">
                                <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True"></SettingsBehavior>
                                <SettingsEditing Mode="PopupEditForm" PopupEditFormModal="True" PopupEditFormShowHeader="False" />
                                <ImagesFilterControl>
                                    <%--<LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">--%> <%--commented for DT 3443--%>
                                    <LoadingPanel SpriteProperties-CssClass="loadingImage"> <%--implemented for DT 3443--%>
                                    </LoadingPanel>
                                </ImagesFilterControl>
                                <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                    <Header SortingImageSpacing="5px" ImageSpacing="5px">
                                    </Header>
                                    <AlternatingRow Enabled="True">
                                    </AlternatingRow>
                                    <LoadingPanel ImageSpacing="10px">
                                    </LoadingPanel>
                                </Styles>
                                <Columns>
                                    <dxwgv:GridViewCommandColumn Caption="Action" Name="commandCol" ShowInCustomizationForm="False"
                                        ShowSelectCheckbox="True" Visible="False" VisibleIndex="0" Width="60px">
                                        <EditButton Text="Assign" Visible="True">
                                            <%--commented part starts DT 3443--%>
                                        <%--<Image AlternateText="Edit" Url="~/Images/gridEdit.gif">
                                            </Image>
                                        </EditButton>
                                        <NewButton Text="New">
                                            <Image AlternateText="New" Url="~/Images/gridNew.gif">
                                            </Image>
                                        </NewButton>
                                        <DeleteButton Text="Delete">
                                            <Image AlternateText="Delete" Url="~/Images/gridDelete.gif">
                                            </Image>
                                        </DeleteButton>
                                        <CancelButton Text="Cancel" Visible="True">
                                            <Image AlternateText="Cancel" Url="../Images/gridCancel.gif">
                                            </Image>
                                        </CancelButton>
                                        <UpdateButton Text="Save" Visible="True">
                                            <Image AlternateText="Save" Url="../Images/gridSave.gif">
                                            </Image>--%>
                                        <%--commented part ends DT 3443--%>
                                         <%--implemented part starts DT 3443--%>
                                            <Image AlternateText="Edit" SpriteProperties-CssClass="editButtonImage">
                                            </Image>
                                        </EditButton>
                                        <NewButton Text="New">
                                            <Image AlternateText="New" SpriteProperties-CssClass="newButtonImage">
                                            </Image>
                                        </NewButton>
                                        <DeleteButton Text="Delete">
                                            <Image AlternateText="Delete" SpriteProperties-CssClass="deleteButtonImage">
                                            </Image>
                                        </DeleteButton>
                                        <CancelButton Text="Cancel" Visible="True">
                                            <Image AlternateText="Cancel" SpriteProperties-CssClass="cancelButtonImage">
                                            </Image>
                                        </CancelButton>
                                        <UpdateButton Text="Save" Visible="True">
                                            <Image AlternateText="Save" SpriteProperties-CssClass="saveButtonImage">
                                            </Image>
                                             <%--implemented part ends DT 3443--%>
                                        </UpdateButton>
                                        <ClearFilterButton Visible="True">
                                        </ClearFilterButton>
                                        <HeaderStyle Font-Bold="True" />
                                    </dxwgv:GridViewCommandColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" Name="CompanyBox"
                                        ReadOnly="True" UnboundType="String" VisibleIndex="0">
                                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                            TextField="CompanyName" ValueField="CompanyId" ValueType="System.String">
                                        </PropertiesComboBox>
                                        <Settings SortMode="DisplayText" />
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="File Name" FieldName="FileId" Name="FileName"
                                        VisibleIndex="1">
                                        <PropertiesHyperLinkEdit NavigateUrlFormatString="~/Common/GetFile.aspx?Type=CSMS&SubType=SafetyPlans&File={0}"
                                            TextField="FileName">
                                        </PropertiesHyperLinkEdit>
                                        <EditFormSettings Visible="False" />
                                        <DataItemTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 100%">
                                                        <dxe:ASPxHyperLink ID="hlAttachment" runat="server" Text="">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <CellStyle ForeColor="Orange" HorizontalAlign="Left">
                                        </CellStyle>
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="FileName" ShowInCustomizationForm="False"
                                        Visible="False">
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="Description" VisibleIndex="2">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Last Modified Date" FieldName="ModifiedDate"
                                        SortIndex="0" SortOrder="Descending" VisibleIndex="3" Width="90px">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Last Modified By" FieldName="ModifiedByUserId"
                                        ReadOnly="True" UnboundType="String" VisibleIndex="4">
                                        <PropertiesComboBox DataSourceID="UsersFullNameDataSource" DropDownHeight="150px"
                                            TextField="UserFullName" ValueField="UserId" ValueType="System.String">
                                            <DropDownButton Enabled="False">
                                            </DropDownButton>
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Last Modified by (UserName)" FieldName="ModifiedByUserId"
                                        ReadOnly="True" UnboundType="String" Visible="False">
                                        <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" TextField="UserLogon"
                                            ValueField="UserId" ValueType="System.String">
                                            <DropDownButton Enabled="False">
                                            </DropDownButton>
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Assigned to" FieldName="EhsConsultantId"
                                        VisibleIndex="5" Width="75px">
                                        <PropertiesComboBox DataSourceID="UsersEhsConsultantsGetActiveAllDataSource" DropDownHeight="150px"
                                            IncrementalFilteringMode="StartsWith" TextField="UserFullName" ValueField="EhsConsultantId"
                                            ValueType="System.Int32">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="True" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Status" FieldName="StatusId" VisibleIndex="6"
                                        Width="100px">
                                        <PropertiesComboBox DataSourceID="SafetyPlanStatusDataSource" DropDownHeight="115px"
                                            IncrementalFilteringMode="StartsWith" TextField="StatusName" ValueField="StatusId"
                                            ValueType="System.Int32">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Questionnaire" FieldName="SelfAssessment_ResponseId"
                                        VisibleIndex="7" Width="90px">
                                        <Settings AutoFilterCondition="Contains" />
                                        <EditFormSettings Visible="False" />
                                        <DataItemTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 107px">
                                                        <dxe:ASPxHyperLink ID="hlSE" runat="server" Text="">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                            </table>
                                        </DataItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                    </LoadingPanelOnStatusBar>
                                    <CollapsedButton Width="11px">
                                    </CollapsedButton>
                                    <ExpandedButton Width="11px">
                                    </ExpandedButton>
                                    <DetailCollapsedButton Width="11px">
                                    </DetailCollapsedButton>
                                    <DetailExpandedButton Width="11px">
                                    </DetailExpandedButton>
                                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                    </LoadingPanel>
                                </Images>
                                <SettingsCookies CookiesID="kpiSP" Version="0.1"></SettingsCookies>
                                <SettingsPager>
                                    <AllButton Visible="True">
                                    </AllButton>
                                </SettingsPager>
                                <Settings ShowGroupPanel="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="225">
                                </Settings>
                                <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                <StylesEditors>
                                    <ProgressBar Height="25px">
                                    </ProgressBar>
                                </StylesEditors>
                                <Templates>
                                    <PagerBar>
                                        <table>
                                            <tr>
                                                <td>
                                                    <dxwgv:ASPxGridViewTemplateReplacement ID="Pager" runat="server" ReplacementType="Pager" />
                                                </td>
                                                <%--<td>
                                            <select onchange="grid.PerformCallback(this.value);" >
                                                <option value="10"<%# WriteSelectedIndex(10) %>  >10</option>
                                                <option value="20" <%# WriteSelectedIndex(20) %> >15</option>
                                                <option value="50" <%# WriteSelectedIndex(50) %> >20</option>
                                            </select>
                                        </td>--%>
                                            </tr>
                                        </table>
                                    </PagerBar>
                                </Templates>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="8" style="padding-top: 2px; text-align: center; text-align: -moz-center;"
                            align="center">
                            <div align="center">
                                <dxe:ASPxButton ID="btnCompare" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Text="Compare Selected Safety Management Plans" Width="294px"
                                    EnableViewState="False" OnClick="ASPxButton1_Click" Visible="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxButton>
                            </div>
                        </td>
                    </tr>
                    <%--<tr>
                <td colspan="8" style="text-align: right">
                                            Number of Rows to show per page:<asp:DropDownList ID="ddlPager" runat="server"
                                                AutoPostBack="True" OnSelectedIndexChanged="ddlPager_SelectedIndexChanged">
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                            </asp:DropDownList></td>
            </tr>--%>
                    <tr align="right">
                        <td class="pageName" colspan="8" style="height: 30px; text-align: right; width: 900px;
                            text-align: -moz-right;" align="right">
                            <div align="right">
                                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <dxm:ASPxPopupMenu ID="pmColumnMenu" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" ShowPopOutImages="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <RootItemSubMenuOffset FirstItemX="2" LastItemX="2" X="2" />
        <Items>
            <dxm:MenuItem Name="Customize" Text="Customize">
            </dxm:MenuItem>
        </Items>
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <ItemSubMenuOffset FirstItemX="2" LastItemX="2" X="2" />
        <SubMenuStyle GutterWidth="17px" />
        <ClientSideEvents ItemClick="function(s, e) {
	if(e.item.name == 'Customize')
        grid.ShowCustomizationWindow();
}" />
    </dxm:ASPxPopupMenu>
    <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    <data:UsersDataSource ID="UsersDataSource" runat="server" EnableDeepLoad="False"
        EnableSorting="true" SelectMethod="GetPaged">
    </data:UsersDataSource>
    <%--       <data:FileDbDataSource ID="FileDBDataSource" runat="server" EnableDeepLoad="False"
            EnableSorting="true" SelectMethod="GetPaged">
        </data:FileDbDataSource>--%>
    <asp:SqlDataSource ID="FileDBDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="SELECT [FileId], [CompanyId], [ModifiedByUserId], [ModifiedDate], [Description], [FileName], [StatusId], [StatusComments], [StatusModifiedByUserId], [SelfAssessment_ResponseId], [EHSConsultantId] As [EhsConsultantId] FROM [FileDB]"
        DeleteCommand="DELETE FROM [FileDB] WHERE [FileId] = @FileId" UpdateCommand="UPDATE [FileDB] SET [CompanyId] = @CompanyId, [Description] = @Description, [FileName] = @FileName, [StatusId] = @StatusId, [StatusComments] = @StatusComments, [StatusModifiedByUserId] = @StatusModifiedByUserId, [EHSConsultantId] = @EHSConsultantId WHERE [FileId] = @FileId">
        <DeleteParameters>
            <asp:Parameter Name="FileId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="CompanyId" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="FileName" Type="String" />
            <asp:Parameter Name="StatusId" Type="Int32" />
            <asp:Parameter Name="StatusComments" Type="String" />
            <asp:Parameter Name="StatusModifiedByUserId" Type="Int32" />
            <asp:Parameter Name="EHSConsultantId" Type="Int32" />
            <asp:Parameter Name="FileId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="CompanyId" Type="Int32" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="FileName" Type="String" />
            <asp:Parameter Name="StatusId" Type="Int32" />
            <asp:Parameter Name="StatusComments" Type="String" />
            <asp:Parameter Name="StatusModifiedByUserId" Type="Int32" />
            <asp:Parameter Name="EHSConsultantId" Type="Int32" />
        </InsertParameters>
    </asp:SqlDataSource>
    <data:SafetyPlanStatusDataSource ID="SafetyPlanStatusDataSource" runat="server" EnableDeepLoad="False"
        EnableSorting="true" SelectMethod="GetPaged">
    </data:SafetyPlanStatusDataSource>
    <data:UsersFullNameDataSource runat="server" ID="UsersFullNameDataSource" SelectMethod="GetPaged">
    </data:UsersFullNameDataSource>
    <asp:ObjectDataSource ID="odsFileDbGetByCompanyId" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.FileDb"
        TypeName="KaiZen.CSMS.Services.FileDbService" SelectMethod="GetByCompanyId_Custom"
        DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
        UpdateMethod="Update">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="CompanyID" SessionField="spVar_CompanyId"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="fileId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="odsFileDbGetByCompanyId_Contractor" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.FileDb"
        TypeName="KaiZen.CSMS.Services.FileDbService" SelectMethod="GetByCompanyID_Custom"
        DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
        UpdateMethod="Update">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="spVar_CompanyId"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="fileId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="SELECT [StatusComments] FROM [FileDB]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsAlcoaContact" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Companies_AlcoaContact" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CompanyID" SessionField="spVar_CompanyId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="UsersEhsConsultantsGetActiveAllDataSource" runat="server"
        ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_UsersEhsConsultants_GetAll_Active" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
</div>
