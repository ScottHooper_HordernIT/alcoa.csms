﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TemplateProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TemplateDataSourceDesigner))]
	public class TemplateDataSource : ProviderDataSource<Template, TemplateKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateDataSource class.
		/// </summary>
		public TemplateDataSource() : base(new TemplateService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TemplateDataSourceView used by the TemplateDataSource.
		/// </summary>
		protected TemplateDataSourceView TemplateView
		{
			get { return ( View as TemplateDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TemplateDataSource control invokes to retrieve data.
		/// </summary>
		public TemplateSelectMethod SelectMethod
		{
			get
			{
				TemplateSelectMethod selectMethod = TemplateSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TemplateSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TemplateDataSourceView class that is to be
		/// used by the TemplateDataSource.
		/// </summary>
		/// <returns>An instance of the TemplateDataSourceView class.</returns>
		protected override BaseDataSourceView<Template, TemplateKey> GetNewDataSourceView()
		{
			return new TemplateDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TemplateDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TemplateDataSourceView : ProviderDataSourceView<Template, TemplateKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TemplateDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TemplateDataSourceView(TemplateDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TemplateDataSource TemplateOwner
		{
			get { return Owner as TemplateDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TemplateSelectMethod SelectMethod
		{
			get { return TemplateOwner.SelectMethod; }
			set { TemplateOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TemplateService TemplateProvider
		{
			get { return Provider as TemplateService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Template> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Template> results = null;
			Template item;
			count = 0;
			
			System.Int32 _templateId;

			switch ( SelectMethod )
			{
				case TemplateSelectMethod.Get:
					TemplateKey entityKey  = new TemplateKey();
					entityKey.Load(values);
					item = TemplateProvider.Get(entityKey);
					results = new TList<Template>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TemplateSelectMethod.GetAll:
                    results = TemplateProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TemplateSelectMethod.GetPaged:
					results = TemplateProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TemplateSelectMethod.Find:
					if ( FilterParameters != null )
						results = TemplateProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TemplateProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TemplateSelectMethod.GetByTemplateId:
					_templateId = ( values["TemplateId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["TemplateId"], typeof(System.Int32)) : (int)0;
					item = TemplateProvider.GetByTemplateId(_templateId);
					results = new TList<Template>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TemplateSelectMethod.Get || SelectMethod == TemplateSelectMethod.GetByTemplateId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Template entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TemplateProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Template> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TemplateProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TemplateDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TemplateDataSource class.
	/// </summary>
	public class TemplateDataSourceDesigner : ProviderDataSourceDesigner<Template, TemplateKey>
	{
		/// <summary>
		/// Initializes a new instance of the TemplateDataSourceDesigner class.
		/// </summary>
		public TemplateDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TemplateSelectMethod SelectMethod
		{
			get { return ((TemplateDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TemplateDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TemplateDataSourceActionList

	/// <summary>
	/// Supports the TemplateDataSourceDesigner class.
	/// </summary>
	internal class TemplateDataSourceActionList : DesignerActionList
	{
		private TemplateDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TemplateDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TemplateDataSourceActionList(TemplateDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TemplateSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TemplateDataSourceActionList
	
	#endregion TemplateDataSourceDesigner
	
	#region TemplateSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TemplateDataSource.SelectMethod property.
	/// </summary>
	public enum TemplateSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByTemplateId method.
		/// </summary>
		GetByTemplateId
	}
	
	#endregion TemplateSelectMethod

	#region TemplateFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Template"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateFilter : SqlFilter<TemplateColumn>
	{
	}
	
	#endregion TemplateFilter

	#region TemplateExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Template"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateExpressionBuilder : SqlExpressionBuilder<TemplateColumn>
	{
	}
	
	#endregion TemplateExpressionBuilder	

	#region TemplateProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TemplateChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Template"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateProperty : ChildEntityProperty<TemplateChildEntityTypes>
	{
	}
	
	#endregion TemplateProperty
}

