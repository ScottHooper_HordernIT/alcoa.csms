﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Security.Principal;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;

using LumenWorks.Framework.IO.Csv;
using ALCOA.CSMS.Batch.Common.Log;

namespace ALCOA.CSMS.BatchJobScripts
{
    public class Dashboard
    {
        //BatchJobScripts job For Item#21
        //BatchJobScripts job For Item#21
        public static void UpdateItem21DashBoardTables(string condition)
        {
            string errorMsg = string.Empty;
            try
            {
                DataSet ds = new DataSet();
                string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                if (condition == "all")
                {

                    //DeleteItem21Tables("Dashboard_KpiCompanySiteCategory_Compliance", sqldb, errorMsg);
                    //DeleteItem21Tables("Dashboard_SMP_HS_Assessor", sqldb, errorMsg);
                    //DeleteItem21Tables("DashBoard_CSA", sqldb, errorMsg);
                    //DeleteItem21Tables("DashBoard_EBI", sqldb, errorMsg);
                    //DeleteItem21Tables("DashBoard_Embeded_CmpCompliant", sqldb, errorMsg);
                    //DeleteItem21Tables("DashBoard_NonEmbeded_CmpCompliant", sqldb, errorMsg);
                    //DeleteItem21Tables("DashBoard_SafetyQualificationProcess", sqldb, errorMsg);
                }
                else if (condition == "live")
                {
                    //DeleteCurentMonth("Dashboard_KpiCompanySiteCategory_Compliance", sqldb, errorMsg);
                    //DeleteCurentMonth("Dashboard_SMP_HS_Assessor", sqldb, errorMsg);
                    //DeleteCurentMonth("DashBoard_CSA", sqldb, errorMsg);
                    //DeleteCurentMonth("DashBoard_EBI", sqldb, errorMsg);
                    //DeleteCurentMonth("DashBoard_Embeded_CmpCompliant", sqldb, errorMsg);
                    //DeleteCurentMonth("DashBoard_NonEmbeded_CmpCompliant", sqldb, errorMsg);
                    //DeleteCurentMonth("DashBoard_SafetyQualificationProcess", sqldb, errorMsg);
                }



                try
                {
                    using (SqlConnection cn1 = new SqlConnection(sqldb))
                    {
                        //DT 3127 Changes
                        //Log4NetService.LogInfo("..Selecting data using Dashboard_KpiCompanySiteCategory_Compliance_Insert" + "...");
                        Log4NetService.LogInfo("..Selecting Site and Region Informations..");

                        cn1.Open();

                        string strQuery = "select SiteId,SiteName from sites  " +
                                                    " WHERE IsVisible = 'True' AND Editable = 'True' " +
                                                    " Union " +
                                                   " select (-1*RegionId) as SiteId,RegionName as RegionName from Regions " +
                                                   " where RegionId <>3 ";

                        SqlCommand cmd1 = new SqlCommand(strQuery, cn1);

                        SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);

                        sqlda.Fill(ds);


                        cn1.Close();
                    }
                }
                catch (Exception ex)
                {
                    //DT 3127 Changes
                    Log4NetService.LogInfo("Error when Selecting Site and Region Informations :");
                    Log4NetService.LogInfo(ex.Message.ToString());
                    throw;
                }

                //DT 3127 Changes
                Log4NetService.LogInfo("..Processing DashBoard informations..");

                if (condition == "all")
                {

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int yearToStart = 2012;
                        int CurrentYear = DateTime.Now.Year;
                        int currentMonth = DateTime.Now.Month;

                        while (yearToStart <= CurrentYear)
                        {
                            int max = 12;
                            if (yearToStart == CurrentYear) { max = currentMonth; }

                            if (yearToStart == 2012)
                            {
                                int mon = 12;
                                Insert_KpiCompanySiteCategory_Compliance_DashBoard(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                Insert_Dashboard_SMP_HS_Assessor(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                Insert_DashBoard_CSA(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                Insert_DashBoard_EBI(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                Insert_DashBoard_Embeded_CmpCompliant(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                Insert_DashBoard_NonEmbeded_CmpCompliant(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                Insert_DashBoard_SafetyQualificationProcess(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                UpdateLastModifiedDate("Dashboard_KpiCompanySiteCategory_Compliance", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);
                                UpdateLastModifiedDate("Dashboard_SMP_HS_Assessor", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);
                                UpdateLastModifiedDate("DashBoard_CSA", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);
                                UpdateLastModifiedDate("DashBoard_EBI", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);
                                UpdateLastModifiedDate("DashBoard_Embeded_CmpCompliant", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);
                                UpdateLastModifiedDate("DashBoard_NonEmbeded_CmpCompliant", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);
                                UpdateLastModifiedDate("DashBoard_SafetyQualificationProcess", sqldb, Convert.ToString(yearToStart), Convert.ToString(mon), errorMsg);


                            }
                            else
                            {

                                for (int mon = 1; mon <= max; mon++)
                                {
                                    if (mon == DateTime.Now.Month)
                                    {
                                        Insert_KpiCompanySiteCategory_Compliance_DashBoard(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        Insert_Dashboard_SMP_HS_Assessor(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        Insert_DashBoard_CSA(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        Insert_DashBoard_EBI(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        Insert_DashBoard_Embeded_CmpCompliant(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        Insert_DashBoard_NonEmbeded_CmpCompliant(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        Insert_DashBoard_SafetyQualificationProcess(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                    }
                                }
                            }

                            yearToStart++;
                        }


                    }
                }
                else if (condition == "live")
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int mon = DateTime.Now.Month;
                        int yearToStart = DateTime.Now.Year;
                        Insert_KpiCompanySiteCategory_Compliance_DashBoard(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        Insert_Dashboard_SMP_HS_Assessor(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        Insert_DashBoard_CSA(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        Insert_DashBoard_EBI(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        Insert_DashBoard_Embeded_CmpCompliant(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        Insert_DashBoard_NonEmbeded_CmpCompliant(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        Insert_DashBoard_SafetyQualificationProcess(Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                    }
                }

                if (condition == "all")
                {

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int yearToStart = 2012;
                        int CurrentYear = DateTime.Now.Year;
                        int currentMonth = DateTime.Now.Month;

                        while (yearToStart <= CurrentYear)
                        {
                            int max = 12;
                            if (yearToStart == CurrentYear) { max = currentMonth; }

                            if (yearToStart == 2012)
                            {
                                int mon = 12;
                                InsertPopupData("_Kpi_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_CSA_Emb_Actual", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_CSA_NonEmb_Actual", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_EBI_Yes_sqCurrent", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_EBI_NO_CurrentSQNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                InsertPopupData("_EBI_No_SqExpired", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_EBI_No_AccessSiteNotGranted", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_EBI_No_SafetyQualificationNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_EBI_ValidSQExemption", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_EBI_No_InformationNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                InsertPopupData("Procurement_CurrentlyWithProcurement", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_CurrentlyWithSupplier", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_ExpiringGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_QuestionnaireGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_AssessingGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                InsertPopupData("Procurement_SQProcessTotal", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_QuestionnaireGT28Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_Questionnaire_Resubmitting", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_AccessGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_ExpiredCurrentlyWithAssessor", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                InsertPopupData("Procurement_AssessingProcurementGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_AssignCompanyStatusGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("Procurement_CurrentlyWithHSAssessor", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_Emb_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                InsertPopupData("_NonEmb_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                            }
                            else
                            {
                                for (int mon = 1; mon <= max; mon++)
                                {
                                    if (mon == currentMonth)
                                    {

                                        InsertPopupData("_Kpi_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_CSA_Emb_Actual", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_CSA_NonEmb_Actual", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_EBI_Yes_sqCurrent", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_EBI_NO_CurrentSQNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                        InsertPopupData("_EBI_No_SqExpired", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_EBI_No_AccessSiteNotGranted", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_EBI_No_SafetyQualificationNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_EBI_ValidSQExemption", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_EBI_No_InformationNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                        InsertPopupData("Procurement_CurrentlyWithProcurement", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_CurrentlyWithSupplier", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_ExpiringGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_QuestionnaireGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_AssessingGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                        InsertPopupData("Procurement_SQProcessTotal", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_QuestionnaireGT28Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_Questionnaire_Resubmitting", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_AccessGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_ExpiredCurrentlyWithAssessor", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                                        InsertPopupData("Procurement_AssessingProcurementGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_AssignCompanyStatusGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("Procurement_CurrentlyWithHSAssessor", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_Emb_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                        InsertPopupData("_NonEmb_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                                    }
                                }
                            }

                            yearToStart++;
                        }


                    }
                }
                else if (condition == "live")
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int mon = DateTime.Now.Month;
                        int yearToStart = DateTime.Now.Year;

                        InsertPopupData("_Kpi_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_CSA_Emb_Actual", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_CSA_NonEmb_Actual", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_EBI_Yes_sqCurrent", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_EBI_NO_CurrentSQNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                        InsertPopupData("_EBI_No_SqExpired", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_EBI_No_AccessSiteNotGranted", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_EBI_No_SafetyQualificationNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_EBI_ValidSQExemption", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_EBI_No_InformationNotFound", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                        InsertPopupData("Procurement_CurrentlyWithProcurement", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_CurrentlyWithSupplier", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_ExpiringGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_QuestionnaireGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_AssessingGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                        InsertPopupData("Procurement_SQProcessTotal", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_QuestionnaireGT28Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_Questionnaire_Resubmitting", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_AccessGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_ExpiredCurrentlyWithAssessor", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                        InsertPopupData("Procurement_AssessingProcurementGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_AssignCompanyStatusGT7Days", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("Procurement_CurrentlyWithHSAssessor", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_Emb_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);
                        InsertPopupData("_NonEmb_NonCompliant_Companies", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]), mon, yearToStart, sqldb, errorMsg);

                    }
                }
                //lHealthCheck.Update(true, "UpdateItem21DashBoardTables");

            }
            catch (Exception ex)
            {
                errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(ex.Message.ToString());
                throw;
                //HealthCheck.Update(false, "UpdateItem21DashBoardTables", "Exception: " + ex.Message);
            }

        }

        public static void DeleteItem21Tables(string tableName, string sqldb, string erroerMsg)
        {
            try
            {
                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    Log4NetService.LogInfo("..Deleting data from table: " + tableName + "...");
                    SqlCommand cmdDel = new SqlCommand("truncate table " + tableName, cnDel);
                    cmdDel.CommandTimeout = 120;
                    cmdDel.ExecuteNonQuery();


                    cnDel.Close();
                }
            }
            catch (Exception ex)
            {
                erroerMsg += ex.Message + ";";
                Log4NetService.LogInfo(ex.Message.ToString());
            }

        }


        public static void UpdateLastModifiedDate(string tableName, string sqldb, string year, string month, string erroerMsg)
        {
            try
            {
                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    //DT 3127 Changes
                    erroerMsg = "";
                    //Log4NetService.LogInfo("..Updating Last Modified Date from table: " + tableName + "...");
                    SqlCommand cmdDel = new SqlCommand("UPDATE " + tableName + " SET LastModifiedDate='2012-12-31' where [Year]=" + year + " and [Month]=" + month, cnDel);
                    cmdDel.CommandTimeout = 2000;
                    cmdDel.ExecuteNonQuery();
                    cnDel.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                erroerMsg += "Error when Updating Last Modified Date from table: " + tableName + ":";
                erroerMsg += ex.Message + ";";
                Log4NetService.LogInfo(erroerMsg.ToString());
            }

        }


        public static void DeleteCurentMonth(string tableName, string sqldb, string erroerMsg)
        {
            try
            {
                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    Log4NetService.LogInfo("..Deleting data from table: " + tableName + "...");
                    string strQuery = "delete from " + tableName + " where [Month]=" + Convert.ToString(DateTime.Now.Month);
                    SqlCommand cmdDel = new SqlCommand("delete from " + tableName + " where [Month]=" + Convert.ToString(DateTime.Now.Month), cnDel);
                    cmdDel.CommandTimeout = 120;
                    cmdDel.ExecuteNonQuery();


                    cnDel.Close();
                }
            }
            catch (Exception ex)
            {
                erroerMsg += ex.Message + ";";
                Log4NetService.LogInfo(ex.Message.ToString());
            }

        }

        public static void InsertPopupData(string procedureName, int SiteId, int month, int year, string sqldb, string erroerMsg)
        {
            try
            {
                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    //DT 3127 Changes
                    //Log4NetService.LogInfo("...Inserting/Updating data using StoredProcedure: " + procedureName + "...");
                    SqlCommand cmdDel = new SqlCommand(procedureName, cnDel);
                    cmdDel.CommandTimeout = 8000;
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.Parameters.AddWithValue("@SiteId", SiteId);
                    cmdDel.Parameters.AddWithValue("@Month", month);
                    cmdDel.Parameters.AddWithValue("@Year", year);
                    cmdDel.ExecuteNonQuery();
                    cnDel.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                erroerMsg += "Error when Inserting/Updating data using StoredProcedure: " + procedureName + ":" + "For Site" + SiteId + " :";
                erroerMsg += ex.Message + ";";
                Log4NetService.LogInfo(erroerMsg.ToString());
            }

        }


        public static void Insert_KpiCompanySiteCategory_Compliance_DashBoard(int siteid, int month, int year, string sqldb, string errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _KpiCompanySiteCategory_Compliance_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_KpiCompanySiteCategory_Compliance_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 120;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                errorMsg += "Error when Selecting data using _KpiCompanySiteCategory_Compliance_DashBoard :" + "For Site" + siteid + " :";
                errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(errorMsg.ToString());
            }

            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            //Log4NetService.LogInfo("..Updating data in Table: Dashboard_KpiCompanySiteCategory_Compliance" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("Dashboard_KpiCompanySiteCategory_Compliance_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }
                            if (ds.Tables[0].Rows[0]["Average No. Of People On Site"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AverageNoOfPeopleOnSite", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AverageNoOfPeopleOnSite", Convert.ToDecimal(ds.Tables[0].Rows[0]["Average No. Of People On Site"]));
                            }


                            if (ds.Tables[0].Rows[0]["Total Man Hours"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@TotalManHours", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@TotalManHours", Convert.ToDecimal(ds.Tables[0].Rows[0]["Total Man Hours"]));
                            }

                            if (ds.Tables[0].Rows[0]["LWDFR"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@LWDFR", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@LWDFR", Convert.ToDecimal(ds.Tables[0].Rows[0]["LWDFR"]));
                            }

                            if (ds.Tables[0].Rows[0]["TRIFR"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@TRIFR", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@TRIFR", Convert.ToDecimal(ds.Tables[0].Rows[0]["TRIFR"]));
                            }
                            if (ds.Tables[0].Rows[0]["AIFR"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AIFR", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AIFR", Convert.ToDecimal(ds.Tables[0].Rows[0]["AIFR"]));
                            }
                            if (ds.Tables[0].Rows[0]["DART"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@DART", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@DART", Convert.ToDecimal(ds.Tables[0].Rows[0]["DART"]));
                            }

                            if (ds.Tables[0].Rows[0]["RST"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@RST", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@RST", Convert.ToDecimal(ds.Tables[0].Rows[0]["RST"]));
                            }
                            if (ds.Tables[0].Rows[0]["LWD"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@LWD", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@LWD", Convert.ToDecimal(ds.Tables[0].Rows[0]["LWD"]));
                            }

                            if (ds.Tables[0].Rows[0]["FA"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@FA", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@FA", Convert.ToDecimal(ds.Tables[0].Rows[0]["FA"]));
                            }
                            if (ds.Tables[0].Rows[0]["MT"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@MT", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@MT", Convert.ToDecimal(ds.Tables[0].Rows[0]["MT"]));
                            }

                            if (ds.Tables[0].Rows[0]["IFE"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@IFE", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@IFE", Convert.ToDecimal(ds.Tables[0].Rows[0]["IFE"]));
                            }
                            if (ds.Tables[0].Rows[0]["RN"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@RN", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@RN", Convert.ToDecimal(ds.Tables[0].Rows[0]["RN"]));
                            }
                            if (ds.Tables[0].Rows[0]["Risk Notifications"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@RiskNotifications", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@RiskNotifications", Convert.ToDecimal(ds.Tables[0].Rows[0]["Risk Notifications"]));
                            }

                            if (ds.Tables[0].Rows[0]["IFE Injury Ratio"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@IFEInjuryRatio", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@IFEInjuryRatio", Convert.ToDecimal(ds.Tables[0].Rows[0]["IFE Injury Ratio"]));
                            }

                            if (ds.Tables[0].Rows[0]["RN Injury Ratio"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@RNInjuryRatio", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@RNInjuryRatio", Convert.ToDecimal(ds.Tables[0].Rows[0]["RN Injury Ratio"]));
                            }

                            if (ds.Tables[0].Rows[0]["IFE+RN Count"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@IFERNCount", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@IFERNCount", Convert.ToDecimal(ds.Tables[0].Rows[0]["IFE+RN Count"]));
                            }
                            if (ds.Tables[0].Rows[0]["IFERN Injury Ratio"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@IFERNInjuryRatio", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@IFERNInjuryRatio", Convert.ToDecimal(ds.Tables[0].Rows[0]["IFERN Injury Ratio"]));
                            }
                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            errorMsg += "Error when Updating data in Table: Dashboard_KpiCompanySiteCategory_Compliance :" + "For Site" + siteid + " :";
                            errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(ex.Message.ToString());
                        }
                    }
                }
            }
        }

        public static void Insert_Dashboard_SMP_HS_Assessor(int siteid, int month, int year, string sqldb, string _errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    _errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _SMP_HS_Assessor_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_SMP_HS_Assessor_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 120;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                _errorMsg += "Error when Selecting data using _KpiCompanySiteCategory_Compliance_DashBoard :" + "For Site" + siteid +" :";
                _errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(_errorMsg.ToString());
            }

            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            //Log4NetService.LogInfo("..Inserting data into Table: Dashboard_SMP_HS_Assessor" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("Dashboard_SMP_HS_Assessor_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }
                            if (ds.Tables[0].Rows[0]["HS Assessors – Required (# Companies)"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsRequiredCompanies", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsRequiredCompanies", Convert.ToDecimal(ds.Tables[0].Rows[0]["HS Assessors – Required (# Companies)"]));
                            }


                            if (ds.Tables[0].Rows[0]["HS Assessors – Being Assessed (# Companies)"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsBeingAssessedCompanies", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsBeingAssessedCompanies", Convert.ToDecimal(ds.Tables[0].Rows[0]["HS Assessors – Being Assessed (# Companies)"]));
                            }

                            if (ds.Tables[0].Rows[0]["HS Assessors – Awaiting Assignment"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsAwaitingAssignment", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsAwaitingAssignment", Convert.ToDecimal(ds.Tables[0].Rows[0]["HS Assessors – Awaiting Assignment"]));
                            }

                            if (ds.Tables[0].Rows[0]["HS Assessors – Approved"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsApproved", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsApproved", Convert.ToDecimal(ds.Tables[0].Rows[0]["HS Assessors – Approved"]));
                            }
                            if (ds.Tables[0].Rows[0]["HS Assessors – Approved %"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsApprovedPer", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@HSAssessorsApprovedPer", Convert.ToDecimal(ds.Tables[0].Rows[0]["HS Assessors – Approved %"]));
                            }

                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            _errorMsg += "Error when Inserting data into Table: Dashboard_SMP_HS_Assessor :" + "For Site" + siteid + " :";
                            _errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(_errorMsg.ToString());
                        }
                    }
                }
            }
        }

        public static void Insert_DashBoard_CSA(int siteid, int month, int year, string sqldb, string _errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    _errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _SMP_HS_Assessor_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_CSA_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 120;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                _errorMsg += "Error when Selecting data using  _SMP_HS_Assessor_DashBoard :" + "For Site" + siteid + " :";
                _errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(_errorMsg.ToString());
            }
            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            //Log4NetService.LogInfo("..Updating data in Table: DashBoard_CSA" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("DashBoard_CSA_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }
                            if (ds.Tables[0].Rows[0]["CAS Embedded –Required"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@CASEmbeddedRequired", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@CASEmbeddedRequired", Convert.ToDecimal(ds.Tables[0].Rows[0]["CAS Embedded –Required"]));
                            }


                            if (ds.Tables[0].Rows[0]["CSA Embedded – Required to date"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@CSAEmbeddedRequiredtodate", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@CSAEmbeddedRequiredtodate", Convert.ToDecimal(ds.Tables[0].Rows[0]["CSA Embedded – Required to date"]));
                            }

                            if (ds.Tables[0].Rows[0]["Embedded – Actual completed"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@EmbeddedActualcompleted", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@EmbeddedActualcompleted", Convert.ToDecimal(ds.Tables[0].Rows[0]["Embedded – Actual completed"]));
                            }

                            if (ds.Tables[0].Rows[0]["CSA Non-Embedded 1 – Total Required"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@CSANonEmbedded1TotalRequired", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@CSANonEmbedded1TotalRequired", Convert.ToDecimal(ds.Tables[0].Rows[0]["CSA Non-Embedded 1 – Total Required"]));
                            }
                            if (ds.Tables[0].Rows[0]["CSA – Non-Embedded 1 – Required to date"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@CSANonEmbedded1Requiredtodate", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@CSANonEmbedded1Requiredtodate", Convert.ToDecimal(ds.Tables[0].Rows[0]["CSA – Non-Embedded 1 – Required to date"]));
                            }

                            if (ds.Tables[0].Rows[0]["Non-Embedded 1 – Actual completed"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NonEmbedded1Actualcompleted", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NonEmbedded1Actualcompleted", Convert.ToDecimal(ds.Tables[0].Rows[0]["Non-Embedded 1 – Actual completed"]));
                            }

                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            _errorMsg += "Error when Updating data in Table: DashBoard_CSA :" + "For Site" + siteid + " :";
                            _errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(_errorMsg.ToString());
                        }
                    }
                }
            }
        }


        public static void Insert_DashBoard_EBI(int siteid, int month, int year, string sqldb, string _errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    _errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _EBI_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_EBI_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 12000;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                _errorMsg += "Selecting data using  _EBI_DashBoard :" + "For Site" + siteid + " :";
                _errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(_errorMsg.ToString());
            }

            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            //Log4NetService.LogInfo("..Updating data in Table: DashBoard_EBI" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("DashBoard_EBI_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }



                            if (ds.Tables[0].Rows[0]["Yes - SQ Current"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@YesSQCurrent", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@YesSQCurrent", Convert.ToDecimal(ds.Tables[0].Rows[0]["Yes - SQ Current"]));
                            }

                            if (ds.Tables[0].Rows[0]["No - Current SQ not found"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NoCurrentSQnotfound", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NoCurrentSQnotfound", Convert.ToDecimal(ds.Tables[0].Rows[0]["No - Current SQ not found"]));
                            }

                            if (ds.Tables[0].Rows[0]["No - SQ Expired"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NoSQExpired", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NoSQExpired", Convert.ToDecimal(ds.Tables[0].Rows[0]["No - SQ Expired"]));
                            }
                            if (ds.Tables[0].Rows[0]["No - Access to Site Not Granted"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NoAccesstoSiteNotGranted", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NoAccesstoSiteNotGranted", Convert.ToDecimal(ds.Tables[0].Rows[0]["No - Access to Site Not Granted"]));
                            }

                            if (ds.Tables[0].Rows[0]["No - Safety Qualification not found"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NoSafetyQualificationnotfound", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NoSafetyQualificationnotfound", Convert.ToDecimal(ds.Tables[0].Rows[0]["No - Safety Qualification not found"]));
                            }

                            if (ds.Tables[0].Rows[0]["No - Information could not be found"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NoInformationcouldnotbefound", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NoInformationcouldnotbefound", Convert.ToDecimal(ds.Tables[0].Rows[0]["No - Information could not be found"]));
                            }

                            if (ds.Tables[0].Rows[0]["Company has a valid SQ Exemption"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@CompanyhasavalidSQExemption", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@CompanyhasavalidSQExemption", Convert.ToDecimal(ds.Tables[0].Rows[0]["Company has a valid SQ Exemption"]));
                            }

                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            _errorMsg += "Error when Updating data in Table: DashBoard_EBI :" + "For Site" + siteid + " :";
                            _errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(_errorMsg.ToString());
                        }
                    }
                }
            }
        }

        public static void Insert_DashBoard_Embeded_CmpCompliant(int siteid, int month, int year, string sqldb, string _errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    _errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _Embeded_CmpCompliant_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_Embeded_CmpCompliant_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 120;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                _errorMsg += "Error when Selecting data using  _Embeded_CmpCompliant_DashBoard :" + "For Site" + siteid + " :";
                _errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(_errorMsg.ToString());
            }
            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            _errorMsg = "";
                            //Log4NetService.LogInfo("..Updating data in Table: DashBoard_Embeded_CmpCompliant" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("DashBoard_Embeded_CmpCompliant_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }
                            if (ds.Tables[0].Rows[0]["Number of Companies:"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NumberofCompanies", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NumberofCompanies", Convert.ToString(ds.Tables[0].Rows[0]["Number of Companies:"]));
                            }


                            if (ds.Tables[0].Rows[0]["Contractor Services Audit"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ContractorServicesAudit", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ContractorServicesAudit", Convert.ToString(ds.Tables[0].Rows[0]["Contractor Services Audit"]));
                            }

                            if (ds.Tables[0].Rows[0]["Workplace Safety Compliance"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@WorkplaceSafetyCompliance", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@WorkplaceSafetyCompliance", Convert.ToString(ds.Tables[0].Rows[0]["Workplace Safety Compliance"]));
                            }
                            if (ds.Tables[0].Rows[0]["Management Health Safety Work Contacts"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ManagementHealthSafetyWorkContacts", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ManagementHealthSafetyWorkContacts", Convert.ToString(ds.Tables[0].Rows[0]["Management Health Safety Work Contacts"]));
                            }

                            if (ds.Tables[0].Rows[0]["Behavioural Observations"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@BehaviouralObservations", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@BehaviouralObservations", Convert.ToString(ds.Tables[0].Rows[0]["Behavioural Observations"]));
                            }

                            if (ds.Tables[0].Rows[0]["Fatality Prevention Program"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@FatalityPreventionProgram", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@FatalityPreventionProgram", Convert.ToString(ds.Tables[0].Rows[0]["Fatality Prevention Program"]));
                            }

                            if (ds.Tables[0].Rows[0]["JSA Field Audit Verifications"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@JSAFieldAuditVerifications", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@JSAFieldAuditVerifications", Convert.ToString(ds.Tables[0].Rows[0]["JSA Field Audit Verifications"]));
                            }
                            if (ds.Tables[0].Rows[0]["Toolbox Meetings"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ToolboxMeetings", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ToolboxMeetings", Convert.ToString(ds.Tables[0].Rows[0]["Toolbox Meetings"]));
                            }
                            if (ds.Tables[0].Rows[0]["Alcoa Weekly Contractors Meeting"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AlcoaWeeklyContractorsMeeting", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AlcoaWeeklyContractorsMeeting", Convert.ToString(ds.Tables[0].Rows[0]["Alcoa Weekly Contractors Meeting"]));
                            }
                            if (ds.Tables[0].Rows[0]["Alcoa Monthly Contractors Meeting"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AlcoaMonthlyContractorsMeeting", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AlcoaMonthlyContractorsMeeting", Convert.ToString(ds.Tables[0].Rows[0]["Alcoa Monthly Contractors Meeting"]));
                            }
                            if (ds.Tables[0].Rows[0]["Safety Plan(s) Submitted"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@SafetyPlansSubmitted", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@SafetyPlansSubmitted", Convert.ToString(ds.Tables[0].Rows[0]["Safety Plan(s) Submitted"]));
                            }
                            if (ds.Tables[0].Rows[0]["Mandated Training"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@MandatedTraining", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@MandatedTraining", Convert.ToString(ds.Tables[0].Rows[0]["Mandated Training"]));
                            }

                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            _errorMsg += "Error when Updating data in Table: DashBoard_Embeded_CmpCompliant :" + "For Site" + siteid + " :";
                            _errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(_errorMsg.ToString());
                        }
                    }
                }
            }
        }

        public static void Insert_DashBoard_NonEmbeded_CmpCompliant(int siteid, int month, int year, string sqldb, string _errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    _errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _NonEmbeded_CmpCompliant_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_NonEmbeded_CmpCompliant_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 120;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                _errorMsg += "Error when Selecting data using  _NonEmbeded_CmpCompliant_DashBoard :" + "For Site" + siteid + " :";
                _errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(_errorMsg.ToString());
            }

            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            //Log4NetService.LogInfo("..Updating data in Table: DashBoard_NonEmbeded_CmpCompliant" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("DashBoard_NonEmbeded_CmpCompliant_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }
                            if (ds.Tables[0].Rows[0]["NE_Number of Companies:"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@NumberofCompanies", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NumberofCompanies", Convert.ToString(ds.Tables[0].Rows[0]["NE_Number of Companies:"]));
                            }


                            if (ds.Tables[0].Rows[0]["NE_Contractor Services Audit"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ContractorServicesAudit", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ContractorServicesAudit", Convert.ToString(ds.Tables[0].Rows[0]["NE_Contractor Services Audit"]));
                            }

                            if (ds.Tables[0].Rows[0]["NE_Workplace Safety Compliance"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@WorkplaceSafetyCompliance", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@WorkplaceSafetyCompliance", Convert.ToString(ds.Tables[0].Rows[0]["NE_Workplace Safety Compliance"]));
                            }
                            if (ds.Tables[0].Rows[0]["NE_Management Health Safety Work Contacts"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ManagementHealthSafetyWorkContacts", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ManagementHealthSafetyWorkContacts", Convert.ToString(ds.Tables[0].Rows[0]["NE_Management Health Safety Work Contacts"]));
                            }

                            if (ds.Tables[0].Rows[0]["NE_Behavioural Observations"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@BehaviouralObservations", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@BehaviouralObservations", Convert.ToString(ds.Tables[0].Rows[0]["NE_Behavioural Observations"]));
                            }

                            if (ds.Tables[0].Rows[0]["NE_JSA Field Audit Verifications"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@JSAFieldAuditVerifications", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@JSAFieldAuditVerifications", Convert.ToString(ds.Tables[0].Rows[0]["NE_JSA Field Audit Verifications"]));
                            }
                            if (ds.Tables[0].Rows[0]["NE_Toolbox Meetings"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ToolboxMeetings", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ToolboxMeetings", Convert.ToString(ds.Tables[0].Rows[0]["NE_Toolbox Meetings"]));
                            }
                            if (ds.Tables[0].Rows[0]["NE_Alcoa Weekly Contractors Meeting"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AlcoaWeeklyContractorsMeeting", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AlcoaWeeklyContractorsMeeting", Convert.ToString(ds.Tables[0].Rows[0]["NE_Alcoa Weekly Contractors Meeting"]));
                            }
                            if (ds.Tables[0].Rows[0]["NE_Alcoa Monthly Contractors Meeting"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AlcoaMonthlyContractorsMeeting", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AlcoaMonthlyContractorsMeeting", Convert.ToString(ds.Tables[0].Rows[0]["NE_Alcoa Monthly Contractors Meeting"]));
                            }

                            if (ds.Tables[0].Rows[0]["NE_Mandated Training"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@MandatedTraining", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@MandatedTraining", Convert.ToString(ds.Tables[0].Rows[0]["NE_Mandated Training"]));
                            }

                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            _errorMsg += "Error when Updating data in Table: DashBoard_NonEmbeded_CmpCompliant :" + "For Site" + siteid + " :";
                            _errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(_errorMsg.ToString());
                        }
                    }
                }
            }
        }


        public static void Insert_DashBoard_SafetyQualificationProcess(int siteid, int month, int year, string sqldb, string _errorMsg)
        {
            DataSet ds = new DataSet();
            //DT 3127 Changes
            try
            {
                using (SqlConnection cn1 = new SqlConnection(sqldb))
                {
                    //DT 3127 Changes
                    _errorMsg = "";
                    //Log4NetService.LogInfo("..Selecting data using  _SafetyQualificationProcess_DashBoard " + "...");
                    cn1.Open();
                    SqlCommand cmd1 = new SqlCommand("_SafetyQualificationProcess_DashBoard", cn1);
                    cmd1.Parameters.AddWithValue("@SiteId", siteid);
                    cmd1.Parameters.AddWithValue("@Month", month);
                    cmd1.Parameters.AddWithValue("@Year", year);
                    cmd1.CommandTimeout = 120;
                    cmd1.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd1);
                    sqlda.Fill(ds);
                    cn1.Close();
                }
            }
            catch (Exception ex)
            {
                //DT 3127 Changes
                _errorMsg += "Error when Selecting data using  _SafetyQualificationProcess_DashBoard :" + "For Site" + siteid + " :";
                _errorMsg += ex.Message + ";";
                Log4NetService.LogInfo(_errorMsg.ToString());
            }

            using (SqlConnection cn = new SqlConnection(sqldb))
            {
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            //DT 3127 Changes
                            //Log4NetService.LogInfo("..Updating data in Table: DashBoard_SafetyQualificationProcess" + "...");
                            cn.Open();

                            SqlCommand cmd = new SqlCommand("DashBoard_SafetyQualificationProcess_Insert", cn);

                            cmd.CommandTimeout = 60000;
                            cmd.CommandType = CommandType.StoredProcedure;

                            #region Fields
                            if (siteid.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Site", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Site", siteid);
                            }

                            if (month.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Month", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Month", month);
                            }

                            if (year.ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Year", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Year", year);
                            }
                            if (ds.Tables[0].Rows[0]["Expired - Currently With Procurement"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ExpiredCurrentlyWithProcurement", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ExpiredCurrentlyWithProcurement", Convert.ToDecimal(ds.Tables[0].Rows[0]["Expired - Currently With Procurement"]));
                            }


                            if (ds.Tables[0].Rows[0]["Expired - Currently With Supplier"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ExpiredCurrentlyWithSupplier", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ExpiredCurrentlyWithSupplier", Convert.ToDecimal(ds.Tables[0].Rows[0]["Expired - Currently With Supplier"]));
                            }

                            if (ds.Tables[0].Rows[0]["Expiring > 7 days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Expiring7days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Expiring7days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Expiring > 7 days"]));
                            }

                            if (ds.Tables[0].Rows[0]["Questionnaire  > 7 days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Questionnaire7days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Questionnaire7days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Questionnaire  > 7 days"]));
                            }
                            if (ds.Tables[0].Rows[0]["Assign Company Status > 7 days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AssignCompanyStatus7days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AssignCompanyStatus7days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Assign Company Status > 7 days"]));
                            }

                            if (ds.Tables[0].Rows[0]["Questionnaire > 28 Days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Questionnaire28Days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Questionnaire28Days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Questionnaire > 28 Days"]));
                            }

                            if (ds.Tables[0].Rows[0]["Questionnaire - Resubmitting"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@QuestionnaireResubmitting", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@QuestionnaireResubmitting", Convert.ToDecimal(ds.Tables[0].Rows[0]["Questionnaire - Resubmitting"]));
                            }

                            if (ds.Tables[0].Rows[0]["Access > 7 Days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@Access7Days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@Access7Days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Access > 7 Days"]));
                            }

                            if (ds.Tables[0].Rows[0]["Expired - Currently With H&S Assessor"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@ExpiredCurrentlyWithHandSAssessor", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@ExpiredCurrentlyWithHandSAssessor", Convert.ToDecimal(ds.Tables[0].Rows[0]["Expired - Currently With H&S Assessor"]));
                            }

                            if (ds.Tables[0].Rows[0]["Assessing Procurement Questionnaire > 7 days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AssessingProcurementQuestionnaire7days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AssessingProcurementQuestionnaire7days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Assessing Procurement Questionnaire > 7 days"]));
                            }

                            if (ds.Tables[0].Rows[0]["Assessing Questionnaire > 7 days"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@AssessingQuestionnaire7days", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@AssessingQuestionnaire7days", Convert.ToDecimal(ds.Tables[0].Rows[0]["Assessing Questionnaire > 7 days"]));
                            }
                            if (ds.Tables[0].Rows[0]["SQ process total"].ToString() == "")
                            {
                                cmd.Parameters.AddWithValue("@SQprocesstotal", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@SQprocesstotal", Convert.ToDecimal(ds.Tables[0].Rows[0]["SQ process total"]));
                            }

                            #endregion


                            cmd.ExecuteNonQuery();

                            cn.Close();
                        }
                        catch (Exception ex)
                        {
                            //DT 3127 Changes
                            _errorMsg += "Error when Updating data in Table: DashBoard_SafetyQualificationProcess :" + "For Site" + siteid + " :";
                            _errorMsg += ex.Message + ";";
                            Log4NetService.LogInfo(_errorMsg.ToString());
                        }
                    }
                }
            }
        }

        //End BatchJobScripts job For Item#21
    }
}
