using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;

using System.Drawing;

public partial class UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Expire : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        SessionHandler.spVar_Questionnaire_FrontPageLink = "QuestionnaireReport_Expire";
        if(!postBack)
        {
            
            ASPxComboBox1.Value = 2;
            grid.FilterExpression = "ExpiresIn <= 60";
            //GridViewColumn gc = grid.Columns["Valid To"];
            //grid.SortBy(gc, DevExpress.Data.ColumnSortOrder.Ascending);
            grid.DataBind();
            grid.Settings.ShowFilterBar = DevExpress.Web.ASPxGridView.GridViewStatusBarMode.Hidden;
        }

        // Export
        String exportFileName = @"ALCOA CSMS - Safety Qualification Report - Expiry"; //Chrome & IE is fine.
        String userAgent = Request.Headers.Get("User-Agent");
        if (
            (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
            (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
            )
        {
            exportFileName = exportFileName.Replace(" ", "_");
        }
        Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true, true);

    }
    protected void ASPxComboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(2000);
        DateTime dt = DateTime.Now;
        switch ((int)ASPxComboBox1.Value)
        {
            case 0:
                grid.SettingsText.Title = "(No Filter)";
                grid.FilterExpression = "";
                break;
            case 1:
                dt = dt.AddDays(30);
                grid.SettingsText.Title = "Within 30 days of Expiry";
                grid.FilterExpression = "ExpiresIn <= 30";
                break;
            case 2:
                dt = dt.AddDays(60);
                grid.SettingsText.Title = "Within 60 days of Expiry";
                grid.FilterExpression = "ExpiresIn <= 60";
                break;
            case 3:
                grid.SettingsText.Title = "Expired";
                grid.FilterExpression = "ExpiresIn <= 0";
                break;
            default:
                break;
        }
        grid.DataBind();
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblExpiresIn") as Label;
        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlReQualify") as ASPxHyperLink;
        Label lblNa = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblNa") as Label;
        //Start:Change by Debashis for Export issue
        if (label != null)
        {
            if (grid.GetRowValues(e.VisibleIndex, "ExpiresIn") != null)
            {
                int ExpiresIn = (int)grid.GetRowValues(e.VisibleIndex, "ExpiresIn");
                label.Text = ExpiresIn.ToString() + " Days";

                if (grid.GetRowValues(e.VisibleIndex, "Status") != null && grid.GetRowValues(e.VisibleIndex, "QuestionnaireId") != null)
                {
                    int qId = (int)grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");

                    if (ExpiresIn <= 60)
                    {
                        //label.ForeColor = Color.Orange;
                        //if ((int)grid.GetRowValues(e.VisibleIndex, "Status") != (int)QuestionnaireStatusList.Re_Qualification)
                        //{

                        //is AssessmentComplete?
                        if (grid.GetRowValues(e.VisibleIndex, "Status").ToString() == ((int)QuestionnaireStatusList.AssessmentComplete).ToString())
                        {
                            //hl.Text = "Re-Qual";
                        }
                        //if (grid.GetRowValues(e.VisibleIndex, "CompanyId") != null)
                        //{

                        //    //is latest?
                        //    //int CompanyId = (int)grid.GetRowValues(e.VisibleIndex, "CompanyId");
                        //    //DataSet ds = qService.GetLatestAssessmentComplete_ByCompanyId(CompanyId);
                        //    //bool valid = false;
                        //    //foreach (DataRow dr in ds.Tables[0].Rows)
                        //    //{
                        //    //    if (dr[0].ToString() == qId.ToString()) valid = true;
                        //    //}


                        //    CompaniesService cService = new CompaniesService();
                        //    Companies c = cService.GetByCompanyId(CompanyId);
                        //    if (c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                        //    {
                        //        //hl.Text = "Re-Qual<br />(in Progress)";
                        //        valid = true;
                        //    }

                        //    if (valid)
                        //    {

                        //    }
                        //}
                        //}
                    }
                    if (ExpiresIn < 0)
                    {
                        label.ForeColor = Color.Red;
                        label.Font.Bold = true;
                        label.Text = "Expired " + (ExpiresIn.ToString()).Replace("-", "") + " Days ago";
                    }
                    hl.Visible = true;
                    lblNa.Visible = false;

                    hl.NavigateUrl = "~/SafetyPQ_QuestionnaireModify.aspx" +
                                        QueryStringModule.Encrypt("q=" + qId.ToString());
                }



            }
            else
            {
                label.Text = "?";
            }
        }
        //End:Change by Debashis for Export issue
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        //if (e.Column.FieldName == "ExpiresIn")
        //{
        //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
        //    //comboBox.Items.Clear();
        //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem('?', '?', ''); s.RemoveItem(-1);}";
        //}
    }

    //protected void ASPxbtnSearch_Click(object sender, EventArgs e)
    //{

    //}
}
