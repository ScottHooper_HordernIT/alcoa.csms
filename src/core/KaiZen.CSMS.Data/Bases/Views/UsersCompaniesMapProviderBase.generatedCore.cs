﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersCompaniesMapProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersCompaniesMapProviderBaseCore : EntityViewProviderBase<UsersCompaniesMap>
	{
		#region Custom Methods
		
		
		#region _UsersCompaniesMap_GetByUserLogon
		
		/// <summary>
		///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
		/// </summary>
		/// <param name="userLogon"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByUserLogon(System.String userLogon)
		{
			return GetByUserLogon(null, 0, int.MaxValue , userLogon);
		}
		
		/// <summary>
		///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
		/// </summary>
		/// <param name="userLogon"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByUserLogon(int start, int pageLength, System.String userLogon)
		{
			return GetByUserLogon(null, start, pageLength , userLogon);
		}
				
		/// <summary>
		///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
		/// </summary>
		/// <param name="userLogon"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByUserLogon(TransactionManager transactionManager, System.String userLogon)
		{
			return GetByUserLogon(transactionManager, 0, int.MaxValue , userLogon);
		}
		
		/// <summary>
		///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
		/// </summary>
		/// <param name="userLogon"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByUserLogon(TransactionManager transactionManager, int start, int pageLength, System.String userLogon);
		
		#endregion



        #region _UsersCompaniesMap_GetByUserLogonCompanyId

        /// <summary>
        ///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
        /// </summary>
        /// <param name="userLogon"> A <c>System.String</c> instance.</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet GetByUserLogonCompanyId(System.String userLogon, System.Int32 companyId)
        {
            return GetByUserLogonCompanyId(null, 0, int.MaxValue, userLogon, companyId);
        }

        /// <summary>
        ///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
        /// </summary>
        /// <param name="userLogon"> A <c>System.String</c> instance.</param>
        /// <param name="start">Row number at which to start reading.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet GetByUserLogonCompanyId(int start, int pageLength, System.String userLogon, System.Int32 companyId)
        {
            return GetByUserLogonCompanyId(null, start, pageLength, userLogon, companyId);
        }

        /// <summary>
        ///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
        /// </summary>
        /// <param name="userLogon"> A <c>System.String</c> instance.</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet GetByUserLogonCompanyId(TransactionManager transactionManager, System.String userLogon, System.Int32 companyId)
        {
            return GetByUserLogonCompanyId(transactionManager, 0, int.MaxValue, userLogon, companyId);
        }

        /// <summary>
        ///	This method wrap the '_UsersCompaniesMap_GetByUserLogon' stored procedure. 
        /// </summary>
        /// <param name="userLogon"> A <c>System.String</c> instance.</param>
        /// <param name="start">Row number at which to start reading.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public abstract DataSet GetByUserLogonCompanyId(TransactionManager transactionManager, int start, int pageLength, System.String userLogon, System.Int32 companyId);

        #endregion

		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersCompaniesMap&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersCompaniesMap&gt;"/></returns>
		protected static VList&lt;UsersCompaniesMap&gt; Fill(DataSet dataSet, VList<UsersCompaniesMap> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersCompaniesMap>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersCompaniesMap&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersCompaniesMap>"/></returns>
		protected static VList&lt;UsersCompaniesMap&gt; Fill(DataTable dataTable, VList<UsersCompaniesMap> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersCompaniesMap c = new UsersCompaniesMap();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserLogon = (Convert.IsDBNull(row["UserLogon"]))?string.Empty:(System.String)row["UserLogon"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.FirstName = (Convert.IsDBNull(row["FirstName"]))?string.Empty:(System.String)row["FirstName"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.Role = (Convert.IsDBNull(row["Role"]))?string.Empty:(System.String)row["Role"];
					c.RoleId = (Convert.IsDBNull(row["RoleId"]))?(int)0:(System.Int32)row["RoleId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.CompanyAbn = (Convert.IsDBNull(row["CompanyAbn"]))?string.Empty:(System.String)row["CompanyAbn"];
					c.Deactivated = (Convert.IsDBNull(row["Deactivated"]))?false:(System.Boolean?)row["Deactivated"];
					c.StartDate = (Convert.IsDBNull(row["StartDate"]))?DateTime.MinValue:(System.DateTime?)row["StartDate"];
					c.EndDate = (Convert.IsDBNull(row["EndDate"]))?DateTime.MinValue:(System.DateTime?)row["EndDate"];
					c.CompanyStatus2Id = (Convert.IsDBNull(row["CompanyStatus2Id"]))?(int)0:(System.Int32)row["CompanyStatus2Id"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersCompaniesMap&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersCompaniesMap&gt;"/></returns>
		protected VList<UsersCompaniesMap> Fill(IDataReader reader, VList<UsersCompaniesMap> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersCompaniesMap entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersCompaniesMap>("UsersCompaniesMap",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersCompaniesMap();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Int32)reader[((int)UsersCompaniesMapColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserLogon = (System.String)reader[((int)UsersCompaniesMapColumn.UserLogon)];
					//entity.UserLogon = (Convert.IsDBNull(reader["UserLogon"]))?string.Empty:(System.String)reader["UserLogon"];
					entity.LastName = (System.String)reader[((int)UsersCompaniesMapColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.FirstName = (System.String)reader[((int)UsersCompaniesMapColumn.FirstName)];
					//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
					entity.Email = (System.String)reader[((int)UsersCompaniesMapColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.Role = (System.String)reader[((int)UsersCompaniesMapColumn.Role)];
					//entity.Role = (Convert.IsDBNull(reader["Role"]))?string.Empty:(System.String)reader["Role"];
					entity.RoleId = (System.Int32)reader[((int)UsersCompaniesMapColumn.RoleId)];
					//entity.RoleId = (Convert.IsDBNull(reader["RoleId"]))?(int)0:(System.Int32)reader["RoleId"];
					entity.CompanyId = (System.Int32)reader[((int)UsersCompaniesMapColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)UsersCompaniesMapColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.CompanyAbn = (System.String)reader[((int)UsersCompaniesMapColumn.CompanyAbn)];
					//entity.CompanyAbn = (Convert.IsDBNull(reader["CompanyAbn"]))?string.Empty:(System.String)reader["CompanyAbn"];
					entity.Deactivated = (reader.IsDBNull(((int)UsersCompaniesMapColumn.Deactivated)))?null:(System.Boolean?)reader[((int)UsersCompaniesMapColumn.Deactivated)];
					//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
					entity.StartDate = (reader.IsDBNull(((int)UsersCompaniesMapColumn.StartDate)))?null:(System.DateTime?)reader[((int)UsersCompaniesMapColumn.StartDate)];
					//entity.StartDate = (Convert.IsDBNull(reader["StartDate"]))?DateTime.MinValue:(System.DateTime?)reader["StartDate"];
					entity.EndDate = (reader.IsDBNull(((int)UsersCompaniesMapColumn.EndDate)))?null:(System.DateTime?)reader[((int)UsersCompaniesMapColumn.EndDate)];
					//entity.EndDate = (Convert.IsDBNull(reader["EndDate"]))?DateTime.MinValue:(System.DateTime?)reader["EndDate"];
					entity.CompanyStatus2Id = (System.Int32)reader[((int)UsersCompaniesMapColumn.CompanyStatus2Id)];
					//entity.CompanyStatus2Id = (Convert.IsDBNull(reader["CompanyStatus2Id"]))?(int)0:(System.Int32)reader["CompanyStatus2Id"];
                    entity.UserCompanyId = (System.Int32)reader[((int)UsersCompaniesMapColumn.UserCompanyId)];
                    //entity.CompanyStatus2Id = (Convert.IsDBNull(reader["CompanyStatus2Id"]))?(int)0:(System.Int32)reader["CompanyStatus2Id"];
					
                    entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersCompaniesMap"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersCompaniesMap"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersCompaniesMap entity)
		{
			reader.Read();
			entity.UserId = (System.Int32)reader[((int)UsersCompaniesMapColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserLogon = (System.String)reader[((int)UsersCompaniesMapColumn.UserLogon)];
			//entity.UserLogon = (Convert.IsDBNull(reader["UserLogon"]))?string.Empty:(System.String)reader["UserLogon"];
			entity.LastName = (System.String)reader[((int)UsersCompaniesMapColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.FirstName = (System.String)reader[((int)UsersCompaniesMapColumn.FirstName)];
			//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
			entity.Email = (System.String)reader[((int)UsersCompaniesMapColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.Role = (System.String)reader[((int)UsersCompaniesMapColumn.Role)];
			//entity.Role = (Convert.IsDBNull(reader["Role"]))?string.Empty:(System.String)reader["Role"];
			entity.RoleId = (System.Int32)reader[((int)UsersCompaniesMapColumn.RoleId)];
			//entity.RoleId = (Convert.IsDBNull(reader["RoleId"]))?(int)0:(System.Int32)reader["RoleId"];
			entity.CompanyId = (System.Int32)reader[((int)UsersCompaniesMapColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)UsersCompaniesMapColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.CompanyAbn = (System.String)reader[((int)UsersCompaniesMapColumn.CompanyAbn)];
			//entity.CompanyAbn = (Convert.IsDBNull(reader["CompanyAbn"]))?string.Empty:(System.String)reader["CompanyAbn"];
			entity.Deactivated = (reader.IsDBNull(((int)UsersCompaniesMapColumn.Deactivated)))?null:(System.Boolean?)reader[((int)UsersCompaniesMapColumn.Deactivated)];
			//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
			entity.StartDate = (reader.IsDBNull(((int)UsersCompaniesMapColumn.StartDate)))?null:(System.DateTime?)reader[((int)UsersCompaniesMapColumn.StartDate)];
			//entity.StartDate = (Convert.IsDBNull(reader["StartDate"]))?DateTime.MinValue:(System.DateTime?)reader["StartDate"];
			entity.EndDate = (reader.IsDBNull(((int)UsersCompaniesMapColumn.EndDate)))?null:(System.DateTime?)reader[((int)UsersCompaniesMapColumn.EndDate)];
			//entity.EndDate = (Convert.IsDBNull(reader["EndDate"]))?DateTime.MinValue:(System.DateTime?)reader["EndDate"];
			entity.CompanyStatus2Id = (System.Int32)reader[((int)UsersCompaniesMapColumn.CompanyStatus2Id)];
			//entity.CompanyStatus2Id = (Convert.IsDBNull(reader["CompanyStatus2Id"]))?(int)0:(System.Int32)reader["CompanyStatus2Id"];
            entity.UserCompanyId = (System.Int32)reader[((int)UsersCompaniesMapColumn.UserCompanyId)];
            //entity.CompanyStatus2Id = (Convert.IsDBNull(reader["CompanyStatus2Id"]))?(int)0:(System.Int32)reader["CompanyStatus2Id"];
			
            reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersCompaniesMap"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersCompaniesMap"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersCompaniesMap entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserLogon = (Convert.IsDBNull(dataRow["UserLogon"]))?string.Empty:(System.String)dataRow["UserLogon"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.FirstName = (Convert.IsDBNull(dataRow["FirstName"]))?string.Empty:(System.String)dataRow["FirstName"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.Role = (Convert.IsDBNull(dataRow["Role"]))?string.Empty:(System.String)dataRow["Role"];
			entity.RoleId = (Convert.IsDBNull(dataRow["RoleId"]))?(int)0:(System.Int32)dataRow["RoleId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.CompanyAbn = (Convert.IsDBNull(dataRow["CompanyAbn"]))?string.Empty:(System.String)dataRow["CompanyAbn"];
			entity.Deactivated = (Convert.IsDBNull(dataRow["Deactivated"]))?false:(System.Boolean?)dataRow["Deactivated"];
			entity.StartDate = (Convert.IsDBNull(dataRow["StartDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["StartDate"];
			entity.EndDate = (Convert.IsDBNull(dataRow["EndDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["EndDate"];
			entity.CompanyStatus2Id = (Convert.IsDBNull(dataRow["CompanyStatus2Id"]))?(int)0:(System.Int32)dataRow["CompanyStatus2Id"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersCompaniesMapFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesMapFilterBuilder : SqlFilterBuilder<UsersCompaniesMapColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapFilterBuilder class.
		/// </summary>
		public UsersCompaniesMapFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesMapFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesMapFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesMapFilterBuilder

	#region UsersCompaniesMapParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesMapParameterBuilder : ParameterizedSqlFilterBuilder<UsersCompaniesMapColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapParameterBuilder class.
		/// </summary>
		public UsersCompaniesMapParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesMapParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesMapParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesMapParameterBuilder
	
	#region UsersCompaniesMapSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesMap"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersCompaniesMapSortBuilder : SqlSortBuilder<UsersCompaniesMapColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesMapSqlSortBuilder class.
		/// </summary>
		public UsersCompaniesMapSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersCompaniesMapSortBuilder

} // end namespace
