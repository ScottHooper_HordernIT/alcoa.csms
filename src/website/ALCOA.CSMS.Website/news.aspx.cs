﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

using System.Text;

public partial class news : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        News2Service n2Service = new News2Service();
        News2 n2 = n2Service.GetByNews2Id(1);
        if (n2 == null)
        {
            n2 = new News2();
            n2.News2Id = 1;
            n2Service.Save(n2);
        }
        if (n2.Content != null)
        {
            ASPxHtmlEditor1.Html = Encoding.ASCII.GetString(n2.Content);
        }
        else
        {
            ASPxHtmlEditor1.Html = "";
        }
    }
}
