﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialContactProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialContactDataSourceDesigner))]
	public class QuestionnaireInitialContactDataSource : ProviderDataSource<QuestionnaireInitialContact, QuestionnaireInitialContactKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactDataSource class.
		/// </summary>
		public QuestionnaireInitialContactDataSource() : base(new QuestionnaireInitialContactService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialContactDataSourceView used by the QuestionnaireInitialContactDataSource.
		/// </summary>
		protected QuestionnaireInitialContactDataSourceView QuestionnaireInitialContactView
		{
			get { return ( View as QuestionnaireInitialContactDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialContactDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialContactSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialContactSelectMethod selectMethod = QuestionnaireInitialContactSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialContactSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialContactDataSourceView class that is to be
		/// used by the QuestionnaireInitialContactDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialContactDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialContact, QuestionnaireInitialContactKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialContactDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialContactDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialContactDataSourceView : ProviderDataSourceView<QuestionnaireInitialContact, QuestionnaireInitialContactKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialContactDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialContactDataSourceView(QuestionnaireInitialContactDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialContactDataSource QuestionnaireInitialContactOwner
		{
			get { return Owner as QuestionnaireInitialContactDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialContactSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialContactOwner.SelectMethod; }
			set { QuestionnaireInitialContactOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialContactService QuestionnaireInitialContactProvider
		{
			get { return Provider as QuestionnaireInitialContactService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialContact> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialContact> results = null;
			QuestionnaireInitialContact item;
			count = 0;
			
			System.Int32 _questionnaireInitialContactId;
			System.Int32 _questionnaireId;
			System.Int32 _contactStatusId;
			System.Int32 _createdByUserId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialContactSelectMethod.Get:
					QuestionnaireInitialContactKey entityKey  = new QuestionnaireInitialContactKey();
					entityKey.Load(values);
					item = QuestionnaireInitialContactProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialContact>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialContactSelectMethod.GetAll:
                    results = QuestionnaireInitialContactProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialContactSelectMethod.GetPaged:
					results = QuestionnaireInitialContactProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialContactSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialContactProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialContactProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialContactSelectMethod.GetByQuestionnaireInitialContactId:
					_questionnaireInitialContactId = ( values["QuestionnaireInitialContactId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireInitialContactId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialContactProvider.GetByQuestionnaireInitialContactId(_questionnaireInitialContactId);
					results = new TList<QuestionnaireInitialContact>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialContactSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialContactProvider.GetByQuestionnaireId(_questionnaireId);
					results = new TList<QuestionnaireInitialContact>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case QuestionnaireInitialContactSelectMethod.GetByContactStatusId:
					_contactStatusId = ( values["ContactStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContactStatusId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactProvider.GetByContactStatusId(_contactStatusId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialContactSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialContactSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialContactProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialContactSelectMethod.Get || SelectMethod == QuestionnaireInitialContactSelectMethod.GetByQuestionnaireInitialContactId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialContact entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialContactProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialContact> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialContactProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialContactDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialContactDataSource class.
	/// </summary>
	public class QuestionnaireInitialContactDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialContact, QuestionnaireInitialContactKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialContactDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialContactDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialContactDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialContactDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialContactDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialContactDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialContactDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialContactDataSourceActionList(QuestionnaireInitialContactDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialContactDataSourceActionList
	
	#endregion QuestionnaireInitialContactDataSourceDesigner
	
	#region QuestionnaireInitialContactSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialContactDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialContactSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireInitialContactId method.
		/// </summary>
		GetByQuestionnaireInitialContactId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByContactStatusId method.
		/// </summary>
		GetByContactStatusId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireInitialContactSelectMethod

	#region QuestionnaireInitialContactFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactFilter : SqlFilter<QuestionnaireInitialContactColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactFilter

	#region QuestionnaireInitialContactExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialContactColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactExpressionBuilder	

	#region QuestionnaireInitialContactProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialContactChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactProperty : ChildEntityProperty<QuestionnaireInitialContactChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialContactProperty
}

