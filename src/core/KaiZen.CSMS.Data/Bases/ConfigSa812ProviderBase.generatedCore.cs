﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ConfigSa812ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ConfigSa812ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ConfigSa812, KaiZen.CSMS.Entities.ConfigSa812Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigSa812Key key)
		{
			return Delete(transactionManager, key.Sa812Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_sa812Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _sa812Id)
		{
			return Delete(null, _sa812Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sa812Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _sa812Id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_Users key.
		///		FK_ConfigSa812_Users Description: 
		/// </summary>
		/// <param name="_supervisorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetBySupervisorUserId(System.Int32? _supervisorUserId)
		{
			int count = -1;
			return GetBySupervisorUserId(_supervisorUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_Users key.
		///		FK_ConfigSa812_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		/// <remarks></remarks>
		public TList<ConfigSa812> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId)
		{
			int count = -1;
			return GetBySupervisorUserId(transactionManager, _supervisorUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_Users key.
		///		FK_ConfigSa812_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId, int start, int pageLength)
		{
			int count = -1;
			return GetBySupervisorUserId(transactionManager, _supervisorUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_Users key.
		///		fkConfigSa812Users Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supervisorUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetBySupervisorUserId(System.Int32? _supervisorUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySupervisorUserId(null, _supervisorUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_Users key.
		///		fkConfigSa812Users Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetBySupervisorUserId(System.Int32? _supervisorUserId, int start, int pageLength,out int count)
		{
			return GetBySupervisorUserId(null, _supervisorUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_Users key.
		///		FK_ConfigSa812_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public abstract TList<ConfigSa812> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_EHSConsultant key.
		///		FK_ConfigSa812_EHSConsultant Description: 
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetByEhsConsultantId(System.Int32? _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(_ehsConsultantId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_EHSConsultant key.
		///		FK_ConfigSa812_EHSConsultant Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		/// <remarks></remarks>
		public TList<ConfigSa812> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_EHSConsultant key.
		///		FK_ConfigSa812_EHSConsultant Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_EHSConsultant key.
		///		fkConfigSa812EhsConsultant Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ehsConsultantId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetByEhsConsultantId(System.Int32? _ehsConsultantId, int start, int pageLength)
		{
			int count =  -1;
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_EHSConsultant key.
		///		fkConfigSa812EhsConsultant Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public TList<ConfigSa812> GetByEhsConsultantId(System.Int32? _ehsConsultantId, int start, int pageLength,out int count)
		{
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigSa812_EHSConsultant key.
		///		FK_ConfigSa812_EHSConsultant Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigSa812 objects.</returns>
		public abstract TList<ConfigSa812> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ConfigSa812 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigSa812Key key, int start, int pageLength)
		{
			return GetBySa812Id(transactionManager, key.Sa812Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ConfigSa812 index.
		/// </summary>
		/// <param name="_sa812Id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySa812Id(System.Int32 _sa812Id)
		{
			int count = -1;
			return GetBySa812Id(null,_sa812Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigSa812 index.
		/// </summary>
		/// <param name="_sa812Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySa812Id(System.Int32 _sa812Id, int start, int pageLength)
		{
			int count = -1;
			return GetBySa812Id(null, _sa812Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigSa812 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sa812Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySa812Id(TransactionManager transactionManager, System.Int32 _sa812Id)
		{
			int count = -1;
			return GetBySa812Id(transactionManager, _sa812Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigSa812 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sa812Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySa812Id(TransactionManager transactionManager, System.Int32 _sa812Id, int start, int pageLength)
		{
			int count = -1;
			return GetBySa812Id(transactionManager, _sa812Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigSa812 index.
		/// </summary>
		/// <param name="_sa812Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySa812Id(System.Int32 _sa812Id, int start, int pageLength, out int count)
		{
			return GetBySa812Id(null, _sa812Id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigSa812 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sa812Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigSa812 GetBySa812Id(TransactionManager transactionManager, System.Int32 _sa812Id, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ConfigSa812 index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(null,_siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigSa812 index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigSa812 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigSa812 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigSa812 index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigSa812 GetBySiteId(System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ConfigSa812 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigSa812 GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ConfigSa812&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ConfigSa812&gt;"/></returns>
		public static TList<ConfigSa812> Fill(IDataReader reader, TList<ConfigSa812> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ConfigSa812 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ConfigSa812")
					.Append("|").Append((System.Int32)reader[((int)ConfigSa812Column.Sa812Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ConfigSa812>(
					key.ToString(), // EntityTrackingKey
					"ConfigSa812",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ConfigSa812();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Sa812Id = (System.Int32)reader[((int)ConfigSa812Column.Sa812Id - 1)];
					c.SiteId = (System.Int32)reader[((int)ConfigSa812Column.SiteId - 1)];
					c.DateTrainingCompleted = (System.DateTime)reader[((int)ConfigSa812Column.DateTrainingCompleted - 1)];
					c.DateTrainingCompleted2 = (reader.IsDBNull(((int)ConfigSa812Column.DateTrainingCompleted2 - 1)))?null:(System.DateTime?)reader[((int)ConfigSa812Column.DateTrainingCompleted2 - 1)];
					c.SupervisorUserId = (reader.IsDBNull(((int)ConfigSa812Column.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)ConfigSa812Column.SupervisorUserId - 1)];
					c.EhsConsultantId = (reader.IsDBNull(((int)ConfigSa812Column.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)ConfigSa812Column.EhsConsultantId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ConfigSa812 entity)
		{
			if (!reader.Read()) return;
			
			entity.Sa812Id = (System.Int32)reader[((int)ConfigSa812Column.Sa812Id - 1)];
			entity.SiteId = (System.Int32)reader[((int)ConfigSa812Column.SiteId - 1)];
			entity.DateTrainingCompleted = (System.DateTime)reader[((int)ConfigSa812Column.DateTrainingCompleted - 1)];
			entity.DateTrainingCompleted2 = (reader.IsDBNull(((int)ConfigSa812Column.DateTrainingCompleted2 - 1)))?null:(System.DateTime?)reader[((int)ConfigSa812Column.DateTrainingCompleted2 - 1)];
			entity.SupervisorUserId = (reader.IsDBNull(((int)ConfigSa812Column.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)ConfigSa812Column.SupervisorUserId - 1)];
			entity.EhsConsultantId = (reader.IsDBNull(((int)ConfigSa812Column.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)ConfigSa812Column.EhsConsultantId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ConfigSa812 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Sa812Id = (System.Int32)dataRow["Sa812Id"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.DateTrainingCompleted = (System.DateTime)dataRow["DateTrainingCompleted"];
			entity.DateTrainingCompleted2 = Convert.IsDBNull(dataRow["DateTrainingCompleted2"]) ? null : (System.DateTime?)dataRow["DateTrainingCompleted2"];
			entity.SupervisorUserId = Convert.IsDBNull(dataRow["SupervisorUserId"]) ? null : (System.Int32?)dataRow["SupervisorUserId"];
			entity.EhsConsultantId = Convert.IsDBNull(dataRow["EhsConsultantId"]) ? null : (System.Int32?)dataRow["EhsConsultantId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigSa812"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigSa812 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigSa812 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SupervisorUserIdSource	
			if (CanDeepLoad(entity, "Users|SupervisorUserIdSource", deepLoadType, innerList) 
				&& entity.SupervisorUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupervisorUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupervisorUserIdSource = tmpEntity;
				else
					entity.SupervisorUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.SupervisorUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupervisorUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupervisorUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.SupervisorUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupervisorUserIdSource

			#region EhsConsultantIdSource	
			if (CanDeepLoad(entity, "EhsConsultant|EhsConsultantIdSource", deepLoadType, innerList) 
				&& entity.EhsConsultantIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.EhsConsultantId ?? (int)0);
				EhsConsultant tmpEntity = EntityManager.LocateEntity<EhsConsultant>(EntityLocator.ConstructKeyFromPkItems(typeof(EhsConsultant), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.EhsConsultantIdSource = tmpEntity;
				else
					entity.EhsConsultantIdSource = DataRepository.EhsConsultantProvider.GetByEhsConsultantId(transactionManager, (entity.EhsConsultantId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EhsConsultantIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.EhsConsultantIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.EhsConsultantProvider.DeepLoad(transactionManager, entity.EhsConsultantIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion EhsConsultantIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ConfigSa812 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ConfigSa812 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigSa812 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigSa812 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SupervisorUserIdSource
			if (CanDeepSave(entity, "Users|SupervisorUserIdSource", deepSaveType, innerList) 
				&& entity.SupervisorUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.SupervisorUserIdSource);
				entity.SupervisorUserId = entity.SupervisorUserIdSource.UserId;
			}
			#endregion 
			
			#region EhsConsultantIdSource
			if (CanDeepSave(entity, "EhsConsultant|EhsConsultantIdSource", deepSaveType, innerList) 
				&& entity.EhsConsultantIdSource != null)
			{
				DataRepository.EhsConsultantProvider.Save(transactionManager, entity.EhsConsultantIdSource);
				entity.EhsConsultantId = entity.EhsConsultantIdSource.EhsConsultantId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ConfigSa812ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ConfigSa812</c>
	///</summary>
	public enum ConfigSa812ChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at SupervisorUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>EhsConsultant</c> at EhsConsultantIdSource
		///</summary>
		[ChildEntityType(typeof(EhsConsultant))]
		EhsConsultant,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion ConfigSa812ChildEntityTypes
	
	#region ConfigSa812FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ConfigSa812Column&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812FilterBuilder : SqlFilterBuilder<ConfigSa812Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812FilterBuilder class.
		/// </summary>
		public ConfigSa812FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812FilterBuilder
	
	#region ConfigSa812ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ConfigSa812Column&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigSa812ParameterBuilder : ParameterizedSqlFilterBuilder<ConfigSa812Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812ParameterBuilder class.
		/// </summary>
		public ConfigSa812ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigSa812ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigSa812ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigSa812ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigSa812ParameterBuilder
	
	#region ConfigSa812SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ConfigSa812Column&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigSa812"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ConfigSa812SortBuilder : SqlSortBuilder<ConfigSa812Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigSa812SqlSortBuilder class.
		/// </summary>
		public ConfigSa812SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ConfigSa812SortBuilder
	
} // end namespace
