﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CurrentQuestionnaireProcurementContactProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CurrentQuestionnaireProcurementContactProviderBaseCore : EntityViewProviderBase<CurrentQuestionnaireProcurementContact>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CurrentQuestionnaireProcurementContact&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CurrentQuestionnaireProcurementContact&gt;"/></returns>
		protected static VList&lt;CurrentQuestionnaireProcurementContact&gt; Fill(DataSet dataSet, VList<CurrentQuestionnaireProcurementContact> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CurrentQuestionnaireProcurementContact>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CurrentQuestionnaireProcurementContact&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CurrentQuestionnaireProcurementContact>"/></returns>
		protected static VList&lt;CurrentQuestionnaireProcurementContact&gt; Fill(DataTable dataTable, VList<CurrentQuestionnaireProcurementContact> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CurrentQuestionnaireProcurementContact c = new CurrentQuestionnaireProcurementContact();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32?)row["UserId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CurrentQuestionnaireProcurementContact&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CurrentQuestionnaireProcurementContact&gt;"/></returns>
		protected VList<CurrentQuestionnaireProcurementContact> Fill(IDataReader reader, VList<CurrentQuestionnaireProcurementContact> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CurrentQuestionnaireProcurementContact entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CurrentQuestionnaireProcurementContact>("CurrentQuestionnaireProcurementContact",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CurrentQuestionnaireProcurementContact();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (reader.IsDBNull(((int)CurrentQuestionnaireProcurementContactColumn.UserId)))?null:(System.Int32?)reader[((int)CurrentQuestionnaireProcurementContactColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
					entity.UserFullName = (System.String)reader[((int)CurrentQuestionnaireProcurementContactColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CurrentQuestionnaireProcurementContact"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentQuestionnaireProcurementContact"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CurrentQuestionnaireProcurementContact entity)
		{
			reader.Read();
			entity.UserId = (reader.IsDBNull(((int)CurrentQuestionnaireProcurementContactColumn.UserId)))?null:(System.Int32?)reader[((int)CurrentQuestionnaireProcurementContactColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32?)reader["UserId"];
			entity.UserFullName = (System.String)reader[((int)CurrentQuestionnaireProcurementContactColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CurrentQuestionnaireProcurementContact"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CurrentQuestionnaireProcurementContact"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CurrentQuestionnaireProcurementContact entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32?)dataRow["UserId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CurrentQuestionnaireProcurementContactFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementContactFilterBuilder : SqlFilterBuilder<CurrentQuestionnaireProcurementContactColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactFilterBuilder class.
		/// </summary>
		public CurrentQuestionnaireProcurementContactFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementContactFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementContactFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementContactFilterBuilder

	#region CurrentQuestionnaireProcurementContactParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementContactParameterBuilder : ParameterizedSqlFilterBuilder<CurrentQuestionnaireProcurementContactColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactParameterBuilder class.
		/// </summary>
		public CurrentQuestionnaireProcurementContactParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrentQuestionnaireProcurementContactParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrentQuestionnaireProcurementContactParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrentQuestionnaireProcurementContactParameterBuilder
	
	#region CurrentQuestionnaireProcurementContactSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CurrentQuestionnaireProcurementContactSortBuilder : SqlSortBuilder<CurrentQuestionnaireProcurementContactColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactSqlSortBuilder class.
		/// </summary>
		public CurrentQuestionnaireProcurementContactSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CurrentQuestionnaireProcurementContactSortBuilder

} // end namespace
