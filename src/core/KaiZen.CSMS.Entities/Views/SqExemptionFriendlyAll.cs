﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'SqExemption_Friendly_All' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SqExemptionFriendlyAll : SqExemptionFriendlyAllBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SqExemptionFriendlyAll"/> instance.
		///</summary>
		public SqExemptionFriendlyAll():base(){}	
		
		#endregion
	}
}
