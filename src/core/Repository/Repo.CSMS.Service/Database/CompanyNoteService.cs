﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyNoteService
    {
        CompanyNote Get(Expression<Func<CompanyNote, bool>> where, List<Expression<Func<CompanyNote, object>>> includes);
        List<CompanyNote> GetMany(int? top, Expression<Func<CompanyNote, bool>> where, Func<IQueryable<CompanyNote>, IOrderedQueryable<CompanyNote>> orderBy, List<Expression<Func<CompanyNote, object>>> includes);
        List<CompanyNote> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyNote, bool>> where, Func<IQueryable<CompanyNote>, IOrderedQueryable<CompanyNote>> orderBy, List<Expression<Func<CompanyNote, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyNote, bool>> where, List<Expression<Func<CompanyNote, object>>> includes);
        void Insert(CompanyNote item, bool saveChanges = true);
        void Update(CompanyNote item, bool saveChanges = true);
        void Delete(CompanyNote item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyNoteService : ICompanyNoteService
    {
        private readonly IRepository<CompanyNote> _repository;

        public CompanyNoteService(IRepository<CompanyNote> repository)
        {
            this._repository = repository;
        }

        public CompanyNote Get(Expression<Func<CompanyNote, bool>> where, List<Expression<Func<CompanyNote, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyNote> GetMany(int? top, Expression<Func<CompanyNote, bool>> where, Func<IQueryable<CompanyNote>, IOrderedQueryable<CompanyNote>> orderBy, List<Expression<Func<CompanyNote, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyNote> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyNote, bool>> where, Func<IQueryable<CompanyNote>, IOrderedQueryable<CompanyNote>> orderBy, List<Expression<Func<CompanyNote, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyNote, bool>> where, List<Expression<Func<CompanyNote, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyNote item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyNote");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyNote item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyNote");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyNote item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyNote");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
