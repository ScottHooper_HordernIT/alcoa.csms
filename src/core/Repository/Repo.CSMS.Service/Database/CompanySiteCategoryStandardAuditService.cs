﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanySiteCategoryStandardAuditService
    {
        CompanySiteCategoryStandardAudit Get(Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes);
        List<CompanySiteCategoryStandardAudit> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, Func<IQueryable<CompanySiteCategoryStandardAudit>, IOrderedQueryable<CompanySiteCategoryStandardAudit>> orderBy, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes);
        List<CompanySiteCategoryStandardAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, Func<IQueryable<CompanySiteCategoryStandardAudit>, IOrderedQueryable<CompanySiteCategoryStandardAudit>> orderBy, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes);
        void Insert(CompanySiteCategoryStandardAudit item, bool saveChanges = true);
        void Update(CompanySiteCategoryStandardAudit item, bool saveChanges = true);
        void Delete(CompanySiteCategoryStandardAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryStandardAuditService : ICompanySiteCategoryStandardAuditService
    {
        private readonly IRepository<CompanySiteCategoryStandardAudit> _repository;

        public CompanySiteCategoryStandardAuditService(IRepository<CompanySiteCategoryStandardAudit> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryStandardAudit Get(Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryStandardAudit> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, Func<IQueryable<CompanySiteCategoryStandardAudit>, IOrderedQueryable<CompanySiteCategoryStandardAudit>> orderBy, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryStandardAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, Func<IQueryable<CompanySiteCategoryStandardAudit>, IOrderedQueryable<CompanySiteCategoryStandardAudit>> orderBy, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryStandardAudit, bool>> where, List<Expression<Func<CompanySiteCategoryStandardAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanySiteCategoryStandardAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandardAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanySiteCategoryStandardAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandardAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanySiteCategoryStandardAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandardAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
