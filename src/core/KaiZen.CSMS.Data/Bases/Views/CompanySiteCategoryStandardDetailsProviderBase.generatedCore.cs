﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryStandardDetailsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompanySiteCategoryStandardDetailsProviderBaseCore : EntityViewProviderBase<CompanySiteCategoryStandardDetails>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompanySiteCategoryStandardDetails&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompanySiteCategoryStandardDetails&gt;"/></returns>
		protected static VList&lt;CompanySiteCategoryStandardDetails&gt; Fill(DataSet dataSet, VList<CompanySiteCategoryStandardDetails> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompanySiteCategoryStandardDetails>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompanySiteCategoryStandardDetails&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompanySiteCategoryStandardDetails>"/></returns>
		protected static VList&lt;CompanySiteCategoryStandardDetails&gt; Fill(DataTable dataTable, VList<CompanySiteCategoryStandardDetails> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompanySiteCategoryStandardDetails c = new CompanySiteCategoryStandardDetails();
					c.CompanySiteCategoryStandardId = (Convert.IsDBNull(row["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)row["CompanySiteCategoryStandardId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.CompanySiteCategoryId = (Convert.IsDBNull(row["CompanySiteCategoryId"]))?(int)0:(System.Int32?)row["CompanySiteCategoryId"];
					c.LocationSpaUserId = (Convert.IsDBNull(row["LocationSpaUserId"]))?(int)0:(System.Int32?)row["LocationSpaUserId"];
					c.LocationSponsorUserId = (Convert.IsDBNull(row["LocationSponsorUserId"]))?(int)0:(System.Int32?)row["LocationSponsorUserId"];
					c.ArpUserId = (Convert.IsDBNull(row["ArpUserId"]))?(int)0:(System.Int32?)row["ArpUserId"];
					c.Area = (Convert.IsDBNull(row["Area"]))?string.Empty:(System.String)row["Area"];
					c.Approved = (Convert.IsDBNull(row["Approved"]))?false:(System.Boolean?)row["Approved"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.SiteAbbrev = (Convert.IsDBNull(row["SiteAbbrev"]))?string.Empty:(System.String)row["SiteAbbrev"];
					c.CategoryName = (Convert.IsDBNull(row["CategoryName"]))?string.Empty:(System.String)row["CategoryName"];
					c.CategoryDesc = (Convert.IsDBNull(row["CategoryDesc"]))?string.Empty:(System.String)row["CategoryDesc"];
					c.ApprovalText = (Convert.IsDBNull(row["ApprovalText"]))?string.Empty:(System.String)row["ApprovalText"];
					c.LocationSpaUserFullName = (Convert.IsDBNull(row["LocationSpaUserFullName"]))?string.Empty:(System.String)row["LocationSpaUserFullName"];
					c.LocationSponsorUserFullName = (Convert.IsDBNull(row["LocationSponsorUserFullName"]))?string.Empty:(System.String)row["LocationSponsorUserFullName"];
					c.ArpUserFullName = (Convert.IsDBNull(row["ArpUserFullName"]))?string.Empty:(System.String)row["ArpUserFullName"];
					c.CrpUserFullNames = (Convert.IsDBNull(row["CrpUserFullNames"]))?string.Empty:(System.String)row["CrpUserFullNames"];
					c.ApprovedByUserFullName = (Convert.IsDBNull(row["ApprovedByUserFullName"]))?string.Empty:(System.String)row["ApprovedByUserFullName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompanySiteCategoryStandardDetails&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompanySiteCategoryStandardDetails&gt;"/></returns>
		protected VList<CompanySiteCategoryStandardDetails> Fill(IDataReader reader, VList<CompanySiteCategoryStandardDetails> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompanySiteCategoryStandardDetails entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompanySiteCategoryStandardDetails>("CompanySiteCategoryStandardDetails",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompanySiteCategoryStandardDetails();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanySiteCategoryStandardId)];
					//entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(reader["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)reader["CompanySiteCategoryStandardId"];
					entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanySiteCategoryId)];
					//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
					entity.LocationSpaUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserId)];
					//entity.LocationSpaUserId = (Convert.IsDBNull(reader["LocationSpaUserId"]))?(int)0:(System.Int32?)reader["LocationSpaUserId"];
					entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserId)];
					//entity.LocationSponsorUserId = (Convert.IsDBNull(reader["LocationSponsorUserId"]))?(int)0:(System.Int32?)reader["LocationSponsorUserId"];
					entity.ArpUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ArpUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.ArpUserId)];
					//entity.ArpUserId = (Convert.IsDBNull(reader["ArpUserId"]))?(int)0:(System.Int32?)reader["ArpUserId"];
					entity.Area = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.Area)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.Area)];
					//entity.Area = (Convert.IsDBNull(reader["Area"]))?string.Empty:(System.String)reader["Area"];
					entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.Approved)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardDetailsColumn.Approved)];
					//entity.Approved = (Convert.IsDBNull(reader["Approved"]))?false:(System.Boolean?)reader["Approved"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.CompanyName = (System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SiteName = (System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.SiteAbbrev = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.SiteAbbrev)];
					//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
					entity.CategoryName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CategoryName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CategoryName)];
					//entity.CategoryName = (Convert.IsDBNull(reader["CategoryName"]))?string.Empty:(System.String)reader["CategoryName"];
					entity.CategoryDesc = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CategoryDesc)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CategoryDesc)];
					//entity.CategoryDesc = (Convert.IsDBNull(reader["CategoryDesc"]))?string.Empty:(System.String)reader["CategoryDesc"];
					entity.ApprovalText = (System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovalText)];
					//entity.ApprovalText = (Convert.IsDBNull(reader["ApprovalText"]))?string.Empty:(System.String)reader["ApprovalText"];
					entity.LocationSpaUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserFullName)];
					//entity.LocationSpaUserFullName = (Convert.IsDBNull(reader["LocationSpaUserFullName"]))?string.Empty:(System.String)reader["LocationSpaUserFullName"];
					entity.LocationSponsorUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserFullName)];
					//entity.LocationSponsorUserFullName = (Convert.IsDBNull(reader["LocationSponsorUserFullName"]))?string.Empty:(System.String)reader["LocationSponsorUserFullName"];
					entity.ArpUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ArpUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.ArpUserFullName)];
					//entity.ArpUserFullName = (Convert.IsDBNull(reader["ArpUserFullName"]))?string.Empty:(System.String)reader["ArpUserFullName"];
					entity.CrpUserFullNames = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CrpUserFullNames)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CrpUserFullNames)];
					//entity.CrpUserFullNames = (Convert.IsDBNull(reader["CrpUserFullNames"]))?string.Empty:(System.String)reader["CrpUserFullNames"];
					entity.ApprovedByUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserFullName)];
					//entity.ApprovedByUserFullName = (Convert.IsDBNull(reader["ApprovedByUserFullName"]))?string.Empty:(System.String)reader["ApprovedByUserFullName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompanySiteCategoryStandardDetails"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompanySiteCategoryStandardDetails"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompanySiteCategoryStandardDetails entity)
		{
			reader.Read();
			entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanySiteCategoryStandardId)];
			//entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(reader["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)reader["CompanySiteCategoryStandardId"];
			entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanySiteCategoryId)];
			//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
			entity.LocationSpaUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserId)];
			//entity.LocationSpaUserId = (Convert.IsDBNull(reader["LocationSpaUserId"]))?(int)0:(System.Int32?)reader["LocationSpaUserId"];
			entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserId)];
			//entity.LocationSponsorUserId = (Convert.IsDBNull(reader["LocationSponsorUserId"]))?(int)0:(System.Int32?)reader["LocationSponsorUserId"];
			entity.ArpUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ArpUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.ArpUserId)];
			//entity.ArpUserId = (Convert.IsDBNull(reader["ArpUserId"]))?(int)0:(System.Int32?)reader["ArpUserId"];
			entity.Area = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.Area)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.Area)];
			//entity.Area = (Convert.IsDBNull(reader["Area"]))?string.Empty:(System.String)reader["Area"];
			entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.Approved)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardDetailsColumn.Approved)];
			//entity.Approved = (Convert.IsDBNull(reader["Approved"]))?false:(System.Boolean?)reader["Approved"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardDetailsColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.CompanyName = (System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SiteName = (System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.SiteAbbrev = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.SiteAbbrev)];
			//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
			entity.CategoryName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CategoryName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CategoryName)];
			//entity.CategoryName = (Convert.IsDBNull(reader["CategoryName"]))?string.Empty:(System.String)reader["CategoryName"];
			entity.CategoryDesc = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CategoryDesc)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CategoryDesc)];
			//entity.CategoryDesc = (Convert.IsDBNull(reader["CategoryDesc"]))?string.Empty:(System.String)reader["CategoryDesc"];
			entity.ApprovalText = (System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovalText)];
			//entity.ApprovalText = (Convert.IsDBNull(reader["ApprovalText"]))?string.Empty:(System.String)reader["ApprovalText"];
			entity.LocationSpaUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSpaUserFullName)];
			//entity.LocationSpaUserFullName = (Convert.IsDBNull(reader["LocationSpaUserFullName"]))?string.Empty:(System.String)reader["LocationSpaUserFullName"];
			entity.LocationSponsorUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.LocationSponsorUserFullName)];
			//entity.LocationSponsorUserFullName = (Convert.IsDBNull(reader["LocationSponsorUserFullName"]))?string.Empty:(System.String)reader["LocationSponsorUserFullName"];
			entity.ArpUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ArpUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.ArpUserFullName)];
			//entity.ArpUserFullName = (Convert.IsDBNull(reader["ArpUserFullName"]))?string.Empty:(System.String)reader["ArpUserFullName"];
			entity.CrpUserFullNames = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.CrpUserFullNames)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.CrpUserFullNames)];
			//entity.CrpUserFullNames = (Convert.IsDBNull(reader["CrpUserFullNames"]))?string.Empty:(System.String)reader["CrpUserFullNames"];
			entity.ApprovedByUserFullName = (reader.IsDBNull(((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserFullName)))?null:(System.String)reader[((int)CompanySiteCategoryStandardDetailsColumn.ApprovedByUserFullName)];
			//entity.ApprovedByUserFullName = (Convert.IsDBNull(reader["ApprovedByUserFullName"]))?string.Empty:(System.String)reader["ApprovedByUserFullName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompanySiteCategoryStandardDetails"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompanySiteCategoryStandardDetails"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompanySiteCategoryStandardDetails entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(dataRow["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)dataRow["CompanySiteCategoryStandardId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.CompanySiteCategoryId = (Convert.IsDBNull(dataRow["CompanySiteCategoryId"]))?(int)0:(System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.LocationSpaUserId = (Convert.IsDBNull(dataRow["LocationSpaUserId"]))?(int)0:(System.Int32?)dataRow["LocationSpaUserId"];
			entity.LocationSponsorUserId = (Convert.IsDBNull(dataRow["LocationSponsorUserId"]))?(int)0:(System.Int32?)dataRow["LocationSponsorUserId"];
			entity.ArpUserId = (Convert.IsDBNull(dataRow["ArpUserId"]))?(int)0:(System.Int32?)dataRow["ArpUserId"];
			entity.Area = (Convert.IsDBNull(dataRow["Area"]))?string.Empty:(System.String)dataRow["Area"];
			entity.Approved = (Convert.IsDBNull(dataRow["Approved"]))?false:(System.Boolean?)dataRow["Approved"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.SiteAbbrev = (Convert.IsDBNull(dataRow["SiteAbbrev"]))?string.Empty:(System.String)dataRow["SiteAbbrev"];
			entity.CategoryName = (Convert.IsDBNull(dataRow["CategoryName"]))?string.Empty:(System.String)dataRow["CategoryName"];
			entity.CategoryDesc = (Convert.IsDBNull(dataRow["CategoryDesc"]))?string.Empty:(System.String)dataRow["CategoryDesc"];
			entity.ApprovalText = (Convert.IsDBNull(dataRow["ApprovalText"]))?string.Empty:(System.String)dataRow["ApprovalText"];
			entity.LocationSpaUserFullName = (Convert.IsDBNull(dataRow["LocationSpaUserFullName"]))?string.Empty:(System.String)dataRow["LocationSpaUserFullName"];
			entity.LocationSponsorUserFullName = (Convert.IsDBNull(dataRow["LocationSponsorUserFullName"]))?string.Empty:(System.String)dataRow["LocationSponsorUserFullName"];
			entity.ArpUserFullName = (Convert.IsDBNull(dataRow["ArpUserFullName"]))?string.Empty:(System.String)dataRow["ArpUserFullName"];
			entity.CrpUserFullNames = (Convert.IsDBNull(dataRow["CrpUserFullNames"]))?string.Empty:(System.String)dataRow["CrpUserFullNames"];
			entity.ApprovedByUserFullName = (Convert.IsDBNull(dataRow["ApprovedByUserFullName"]))?string.Empty:(System.String)dataRow["ApprovedByUserFullName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompanySiteCategoryStandardDetailsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardDetailsFilterBuilder : SqlFilterBuilder<CompanySiteCategoryStandardDetailsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsFilterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardDetailsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardDetailsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardDetailsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardDetailsFilterBuilder

	#region CompanySiteCategoryStandardDetailsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardDetails"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardDetailsParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryStandardDetailsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardDetailsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardDetailsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardDetailsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardDetailsParameterBuilder
	
	#region CompanySiteCategoryStandardDetailsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardDetails"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategoryStandardDetailsSortBuilder : SqlSortBuilder<CompanySiteCategoryStandardDetailsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDetailsSqlSortBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardDetailsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategoryStandardDetailsSortBuilder

} // end namespace
