﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit04Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit04DataSourceDesigner))]
	public class TwentyOnePointAudit04DataSource : ProviderDataSource<TwentyOnePointAudit04, TwentyOnePointAudit04Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04DataSource class.
		/// </summary>
		public TwentyOnePointAudit04DataSource() : base(new TwentyOnePointAudit04Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit04DataSourceView used by the TwentyOnePointAudit04DataSource.
		/// </summary>
		protected TwentyOnePointAudit04DataSourceView TwentyOnePointAudit04View
		{
			get { return ( View as TwentyOnePointAudit04DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit04DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit04SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit04SelectMethod selectMethod = TwentyOnePointAudit04SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit04SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit04DataSourceView class that is to be
		/// used by the TwentyOnePointAudit04DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit04DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit04, TwentyOnePointAudit04Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit04DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit04DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit04DataSourceView : ProviderDataSourceView<TwentyOnePointAudit04, TwentyOnePointAudit04Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit04DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit04DataSourceView(TwentyOnePointAudit04DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit04DataSource TwentyOnePointAudit04Owner
		{
			get { return Owner as TwentyOnePointAudit04DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit04SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit04Owner.SelectMethod; }
			set { TwentyOnePointAudit04Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit04Service TwentyOnePointAudit04Provider
		{
			get { return Provider as TwentyOnePointAudit04Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit04> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit04> results = null;
			TwentyOnePointAudit04 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit04SelectMethod.Get:
					TwentyOnePointAudit04Key entityKey  = new TwentyOnePointAudit04Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit04Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit04>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit04SelectMethod.GetAll:
                    results = TwentyOnePointAudit04Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit04SelectMethod.GetPaged:
					results = TwentyOnePointAudit04Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit04SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit04Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit04Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit04SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit04Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit04>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit04SelectMethod.Get || SelectMethod == TwentyOnePointAudit04SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit04 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit04Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit04> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit04Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit04DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit04DataSource class.
	/// </summary>
	public class TwentyOnePointAudit04DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit04, TwentyOnePointAudit04Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit04DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit04SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit04DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit04DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit04DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit04DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit04DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit04DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit04DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit04DataSourceActionList(TwentyOnePointAudit04DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit04SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit04DataSourceActionList
	
	#endregion TwentyOnePointAudit04DataSourceDesigner
	
	#region TwentyOnePointAudit04SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit04DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit04SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit04SelectMethod

	#region TwentyOnePointAudit04Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit04"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit04Filter : SqlFilter<TwentyOnePointAudit04Column>
	{
	}
	
	#endregion TwentyOnePointAudit04Filter

	#region TwentyOnePointAudit04ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit04"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit04ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit04Column>
	{
	}
	
	#endregion TwentyOnePointAudit04ExpressionBuilder	

	#region TwentyOnePointAudit04Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit04ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit04"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit04Property : ChildEntityProperty<TwentyOnePointAudit04ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit04Property
}

