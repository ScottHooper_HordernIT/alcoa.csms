﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_welcome2" Codebehind="welcome2.ascx.cs" %>
<asp:Image ID="Image1" runat="server" Height="140px" ImageUrl="~/Images/logo.jpg" Width="200px"/>
<div class="dxncItemContent_Blue" style="width: 200px; padding-top: 10px; padding-left: 5px;">
<p align="left">
    Welcome to the Alcoa World Alumina, Australian Operation's Contractor Services Management System web portal.
    <br /><br />
    This website provides Alcoa’s Australian Contractor's a communication channel whereby you can submit your company's compulsory State Government Statutory reporting requirements and retrieve the latest copies of the relevant site operations procedures.
    <br /><br />
    It is your organisation's responsibility to ensure you have the most up to date information PRIOR to engaging on any task within Alcoa Australian operations.
    <br /><br />
</p>
</div>