﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.PrivilegeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(PrivilegeDataSourceDesigner))]
	public class PrivilegeDataSource : ProviderDataSource<Privilege, PrivilegeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeDataSource class.
		/// </summary>
		public PrivilegeDataSource() : base(new PrivilegeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the PrivilegeDataSourceView used by the PrivilegeDataSource.
		/// </summary>
		protected PrivilegeDataSourceView PrivilegeView
		{
			get { return ( View as PrivilegeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the PrivilegeDataSource control invokes to retrieve data.
		/// </summary>
		public PrivilegeSelectMethod SelectMethod
		{
			get
			{
				PrivilegeSelectMethod selectMethod = PrivilegeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (PrivilegeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the PrivilegeDataSourceView class that is to be
		/// used by the PrivilegeDataSource.
		/// </summary>
		/// <returns>An instance of the PrivilegeDataSourceView class.</returns>
		protected override BaseDataSourceView<Privilege, PrivilegeKey> GetNewDataSourceView()
		{
			return new PrivilegeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the PrivilegeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class PrivilegeDataSourceView : ProviderDataSourceView<Privilege, PrivilegeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PrivilegeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the PrivilegeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public PrivilegeDataSourceView(PrivilegeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal PrivilegeDataSource PrivilegeOwner
		{
			get { return Owner as PrivilegeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal PrivilegeSelectMethod SelectMethod
		{
			get { return PrivilegeOwner.SelectMethod; }
			set { PrivilegeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal PrivilegeService PrivilegeProvider
		{
			get { return Provider as PrivilegeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Privilege> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Privilege> results = null;
			Privilege item;
			count = 0;
			
			System.Int32 _privilegeId;
			System.String _privilegeName;

			switch ( SelectMethod )
			{
				case PrivilegeSelectMethod.Get:
					PrivilegeKey entityKey  = new PrivilegeKey();
					entityKey.Load(values);
					item = PrivilegeProvider.Get(entityKey);
					results = new TList<Privilege>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case PrivilegeSelectMethod.GetAll:
                    results = PrivilegeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case PrivilegeSelectMethod.GetPaged:
					results = PrivilegeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case PrivilegeSelectMethod.Find:
					if ( FilterParameters != null )
						results = PrivilegeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = PrivilegeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case PrivilegeSelectMethod.GetByPrivilegeId:
					_privilegeId = ( values["PrivilegeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["PrivilegeId"], typeof(System.Int32)) : (int)0;
					item = PrivilegeProvider.GetByPrivilegeId(_privilegeId);
					results = new TList<Privilege>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case PrivilegeSelectMethod.GetByPrivilegeName:
					_privilegeName = ( values["PrivilegeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["PrivilegeName"], typeof(System.String)) : string.Empty;
					item = PrivilegeProvider.GetByPrivilegeName(_privilegeName);
					results = new TList<Privilege>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == PrivilegeSelectMethod.Get || SelectMethod == PrivilegeSelectMethod.GetByPrivilegeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Privilege entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					PrivilegeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Privilege> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			PrivilegeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region PrivilegeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the PrivilegeDataSource class.
	/// </summary>
	public class PrivilegeDataSourceDesigner : ProviderDataSourceDesigner<Privilege, PrivilegeKey>
	{
		/// <summary>
		/// Initializes a new instance of the PrivilegeDataSourceDesigner class.
		/// </summary>
		public PrivilegeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PrivilegeSelectMethod SelectMethod
		{
			get { return ((PrivilegeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new PrivilegeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region PrivilegeDataSourceActionList

	/// <summary>
	/// Supports the PrivilegeDataSourceDesigner class.
	/// </summary>
	internal class PrivilegeDataSourceActionList : DesignerActionList
	{
		private PrivilegeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the PrivilegeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public PrivilegeDataSourceActionList(PrivilegeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public PrivilegeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion PrivilegeDataSourceActionList
	
	#endregion PrivilegeDataSourceDesigner
	
	#region PrivilegeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the PrivilegeDataSource.SelectMethod property.
	/// </summary>
	public enum PrivilegeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByPrivilegeId method.
		/// </summary>
		GetByPrivilegeId,
		/// <summary>
		/// Represents the GetByPrivilegeName method.
		/// </summary>
		GetByPrivilegeName
	}
	
	#endregion PrivilegeSelectMethod

	#region PrivilegeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeFilter : SqlFilter<PrivilegeColumn>
	{
	}
	
	#endregion PrivilegeFilter

	#region PrivilegeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeExpressionBuilder : SqlExpressionBuilder<PrivilegeColumn>
	{
	}
	
	#endregion PrivilegeExpressionBuilder	

	#region PrivilegeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;PrivilegeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Privilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PrivilegeProperty : ChildEntityProperty<PrivilegeChildEntityTypes>
	{
	}
	
	#endregion PrivilegeProperty
}

