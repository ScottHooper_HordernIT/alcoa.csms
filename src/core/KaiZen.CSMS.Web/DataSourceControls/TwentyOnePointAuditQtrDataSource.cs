﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAuditQtrProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAuditQtrDataSourceDesigner))]
	public class TwentyOnePointAuditQtrDataSource : ProviderDataSource<TwentyOnePointAuditQtr, TwentyOnePointAuditQtrKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrDataSource class.
		/// </summary>
		public TwentyOnePointAuditQtrDataSource() : base(new TwentyOnePointAuditQtrService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAuditQtrDataSourceView used by the TwentyOnePointAuditQtrDataSource.
		/// </summary>
		protected TwentyOnePointAuditQtrDataSourceView TwentyOnePointAuditQtrView
		{
			get { return ( View as TwentyOnePointAuditQtrDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAuditQtrDataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAuditQtrSelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAuditQtrSelectMethod selectMethod = TwentyOnePointAuditQtrSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAuditQtrSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAuditQtrDataSourceView class that is to be
		/// used by the TwentyOnePointAuditQtrDataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAuditQtrDataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAuditQtr, TwentyOnePointAuditQtrKey> GetNewDataSourceView()
		{
			return new TwentyOnePointAuditQtrDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAuditQtrDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAuditQtrDataSourceView : ProviderDataSourceView<TwentyOnePointAuditQtr, TwentyOnePointAuditQtrKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAuditQtrDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAuditQtrDataSourceView(TwentyOnePointAuditQtrDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAuditQtrDataSource TwentyOnePointAuditQtrOwner
		{
			get { return Owner as TwentyOnePointAuditQtrDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAuditQtrSelectMethod SelectMethod
		{
			get { return TwentyOnePointAuditQtrOwner.SelectMethod; }
			set { TwentyOnePointAuditQtrOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAuditQtrService TwentyOnePointAuditQtrProvider
		{
			get { return Provider as TwentyOnePointAuditQtrService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAuditQtr> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAuditQtr> results = null;
			TwentyOnePointAuditQtr item;
			count = 0;
			
			System.Int32 _qtrId;
			System.String _qtrName;

			switch ( SelectMethod )
			{
				case TwentyOnePointAuditQtrSelectMethod.Get:
					TwentyOnePointAuditQtrKey entityKey  = new TwentyOnePointAuditQtrKey();
					entityKey.Load(values);
					item = TwentyOnePointAuditQtrProvider.Get(entityKey);
					results = new TList<TwentyOnePointAuditQtr>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAuditQtrSelectMethod.GetAll:
                    results = TwentyOnePointAuditQtrProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAuditQtrSelectMethod.GetPaged:
					results = TwentyOnePointAuditQtrProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAuditQtrSelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAuditQtrProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAuditQtrProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAuditQtrSelectMethod.GetByQtrId:
					_qtrId = ( values["QtrId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QtrId"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAuditQtrProvider.GetByQtrId(_qtrId);
					results = new TList<TwentyOnePointAuditQtr>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case TwentyOnePointAuditQtrSelectMethod.GetByQtrName:
					_qtrName = ( values["QtrName"] != null ) ? (System.String) EntityUtil.ChangeType(values["QtrName"], typeof(System.String)) : string.Empty;
					item = TwentyOnePointAuditQtrProvider.GetByQtrName(_qtrName);
					results = new TList<TwentyOnePointAuditQtr>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAuditQtrSelectMethod.Get || SelectMethod == TwentyOnePointAuditQtrSelectMethod.GetByQtrId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAuditQtr entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAuditQtrProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAuditQtr> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAuditQtrProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAuditQtrDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAuditQtrDataSource class.
	/// </summary>
	public class TwentyOnePointAuditQtrDataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAuditQtr, TwentyOnePointAuditQtrKey>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrDataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAuditQtrDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAuditQtrSelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAuditQtrDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAuditQtrDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAuditQtrDataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAuditQtrDataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAuditQtrDataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAuditQtrDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditQtrDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAuditQtrDataSourceActionList(TwentyOnePointAuditQtrDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAuditQtrSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAuditQtrDataSourceActionList
	
	#endregion TwentyOnePointAuditQtrDataSourceDesigner
	
	#region TwentyOnePointAuditQtrSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAuditQtrDataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAuditQtrSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQtrId method.
		/// </summary>
		GetByQtrId,
		/// <summary>
		/// Represents the GetByQtrName method.
		/// </summary>
		GetByQtrName
	}
	
	#endregion TwentyOnePointAuditQtrSelectMethod

	#region TwentyOnePointAuditQtrFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrFilter : SqlFilter<TwentyOnePointAuditQtrColumn>
	{
	}
	
	#endregion TwentyOnePointAuditQtrFilter

	#region TwentyOnePointAuditQtrExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAuditQtrColumn>
	{
	}
	
	#endregion TwentyOnePointAuditQtrExpressionBuilder	

	#region TwentyOnePointAuditQtrProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAuditQtrChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAuditQtr"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditQtrProperty : ChildEntityProperty<TwentyOnePointAuditQtrChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAuditQtrProperty
}

