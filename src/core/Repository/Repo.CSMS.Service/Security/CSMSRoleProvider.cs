﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web;
using System.Web.Mvc;
using Repo.CSMS.Service.Database;

namespace Repo.CSMS.Service.Security
{
    public class CSMSRoleProvider : RoleProvider
    {
        private string applicationname = "CSMSRoleProvider";

        public override string ApplicationName
        {
            get
            {
                return this.applicationname;
            }
            set
            {
                this.applicationname = value;
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            List<string> roles = new List<string>();

            if (HttpContext.Current.Session["user_roles"] != null)
            {
                roles = HttpContext.Current.Session["user_roles"] as List<string>;
            }
            else
            {
                try
                {
                    var userService = DependencyResolver.Current.GetService<IUserService>();

                    if (userService != null)
                    {
                        //Get the logged in user
                        var user = userService.Get(e => e.UserLogon == username, null);

                        if (user != null)
                        {
                            roles.Add(user.Role.Role1);
                        }
                    }

                    HttpContext.Current.Session["user_roles"] = roles;
                }
                catch { }
            }

            return roles.ToArray();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}
