﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ILogDetailService
    {
        LogDetail Get(Expression<Func<LogDetail, bool>> where, List<Expression<Func<LogDetail, object>>> includes);
        List<LogDetail> GetMany(int? top, Expression<Func<LogDetail, bool>> where, Func<IQueryable<LogDetail>, IOrderedQueryable<LogDetail>> orderBy, List<Expression<Func<LogDetail, object>>> includes);
        List<LogDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<LogDetail, bool>> where, Func<IQueryable<LogDetail>, IOrderedQueryable<LogDetail>> orderBy, List<Expression<Func<LogDetail, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<LogDetail, bool>> where, List<Expression<Func<LogDetail, object>>> includes);
        void Insert(LogDetail item, bool saveChanges = true);
        void Update(LogDetail item, bool saveChanges = true);
        void Delete(LogDetail item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class LogDetailService : ILogDetailService
    {
        private readonly IRepository<LogDetail> _repository;

        public LogDetailService(IRepository<LogDetail> repository)
        {
            this._repository = repository;
        }

        public LogDetail Get(Expression<Func<LogDetail, bool>> where, List<Expression<Func<LogDetail, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<LogDetail> GetMany(int? top, Expression<Func<LogDetail, bool>> where, Func<IQueryable<LogDetail>, IOrderedQueryable<LogDetail>> orderBy, List<Expression<Func<LogDetail, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<LogDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<LogDetail, bool>> where, Func<IQueryable<LogDetail>, IOrderedQueryable<LogDetail>> orderBy, List<Expression<Func<LogDetail, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<LogDetail, bool>> where, List<Expression<Func<LogDetail, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(LogDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("LogDetail");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(LogDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("LogDetail");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(LogDetail item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("LogDetail");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
