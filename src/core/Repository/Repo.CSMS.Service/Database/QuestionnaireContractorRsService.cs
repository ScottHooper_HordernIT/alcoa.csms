﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireContractorRsService
    {
        QuestionnaireContractorRs Get(Expression<Func<QuestionnaireContractorRs, bool>> where, List<Expression<Func<QuestionnaireContractorRs, object>>> includes);
        List<QuestionnaireContractorRs> GetMany(int? top, Expression<Func<QuestionnaireContractorRs, bool>> where, Func<IQueryable<QuestionnaireContractorRs>, IOrderedQueryable<QuestionnaireContractorRs>> orderBy, List<Expression<Func<QuestionnaireContractorRs, object>>> includes);
        List<QuestionnaireContractorRs> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireContractorRs, bool>> where, Func<IQueryable<QuestionnaireContractorRs>, IOrderedQueryable<QuestionnaireContractorRs>> orderBy, List<Expression<Func<QuestionnaireContractorRs, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireContractorRs, bool>> where, List<Expression<Func<QuestionnaireContractorRs, object>>> includes);
        void Insert(QuestionnaireContractorRs item, bool saveChanges = true);
        void Update(QuestionnaireContractorRs item, bool saveChanges = true);
        void Delete(QuestionnaireContractorRs item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireContractorRsService : IQuestionnaireContractorRsService
    {
        private readonly IRepository<QuestionnaireContractorRs> _repository;

        public QuestionnaireContractorRsService(IRepository<QuestionnaireContractorRs> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireContractorRs Get(Expression<Func<QuestionnaireContractorRs, bool>> where, List<Expression<Func<QuestionnaireContractorRs, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireContractorRs> GetMany(int? top, Expression<Func<QuestionnaireContractorRs, bool>> where, Func<IQueryable<QuestionnaireContractorRs>, IOrderedQueryable<QuestionnaireContractorRs>> orderBy, List<Expression<Func<QuestionnaireContractorRs, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireContractorRs> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireContractorRs, bool>> where, Func<IQueryable<QuestionnaireContractorRs>, IOrderedQueryable<QuestionnaireContractorRs>> orderBy, List<Expression<Func<QuestionnaireContractorRs, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireContractorRs, bool>> where, List<Expression<Func<QuestionnaireContractorRs, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireContractorRs item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireContractorRs");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireContractorRs item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireContractorRs");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireContractorRs item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireContractorRs");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
