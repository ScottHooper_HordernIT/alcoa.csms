﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsEmailLogRecipientProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsmsEmailLogRecipientDataSourceDesigner))]
	public class CsmsEmailLogRecipientDataSource : ProviderDataSource<CsmsEmailLogRecipient, CsmsEmailLogRecipientKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientDataSource class.
		/// </summary>
		public CsmsEmailLogRecipientDataSource() : base(new CsmsEmailLogRecipientService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsEmailLogRecipientDataSourceView used by the CsmsEmailLogRecipientDataSource.
		/// </summary>
		protected CsmsEmailLogRecipientDataSourceView CsmsEmailLogRecipientView
		{
			get { return ( View as CsmsEmailLogRecipientDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsmsEmailLogRecipientDataSource control invokes to retrieve data.
		/// </summary>
		public CsmsEmailLogRecipientSelectMethod SelectMethod
		{
			get
			{
				CsmsEmailLogRecipientSelectMethod selectMethod = CsmsEmailLogRecipientSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsmsEmailLogRecipientSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsEmailLogRecipientDataSourceView class that is to be
		/// used by the CsmsEmailLogRecipientDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsEmailLogRecipientDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsEmailLogRecipient, CsmsEmailLogRecipientKey> GetNewDataSourceView()
		{
			return new CsmsEmailLogRecipientDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsEmailLogRecipientDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsEmailLogRecipientDataSourceView : ProviderDataSourceView<CsmsEmailLogRecipient, CsmsEmailLogRecipientKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsEmailLogRecipientDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsEmailLogRecipientDataSourceView(CsmsEmailLogRecipientDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsEmailLogRecipientDataSource CsmsEmailLogRecipientOwner
		{
			get { return Owner as CsmsEmailLogRecipientDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsmsEmailLogRecipientSelectMethod SelectMethod
		{
			get { return CsmsEmailLogRecipientOwner.SelectMethod; }
			set { CsmsEmailLogRecipientOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsEmailLogRecipientService CsmsEmailLogRecipientProvider
		{
			get { return Provider as CsmsEmailLogRecipientService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsmsEmailLogRecipient> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsmsEmailLogRecipient> results = null;
			CsmsEmailLogRecipient item;
			count = 0;
			
			System.Int32 _csmsEmailRecipientId;
			System.Int32 _csmsEmailLogId;
			System.Int32 _csmsEmailRecipientTypeId;

			switch ( SelectMethod )
			{
				case CsmsEmailLogRecipientSelectMethod.Get:
					CsmsEmailLogRecipientKey entityKey  = new CsmsEmailLogRecipientKey();
					entityKey.Load(values);
					item = CsmsEmailLogRecipientProvider.Get(entityKey);
					results = new TList<CsmsEmailLogRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsmsEmailLogRecipientSelectMethod.GetAll:
                    results = CsmsEmailLogRecipientProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsmsEmailLogRecipientSelectMethod.GetPaged:
					results = CsmsEmailLogRecipientProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsmsEmailLogRecipientSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsmsEmailLogRecipientProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsmsEmailLogRecipientProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsmsEmailLogRecipientSelectMethod.GetByCsmsEmailRecipientId:
					_csmsEmailRecipientId = ( values["CsmsEmailRecipientId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailRecipientId"], typeof(System.Int32)) : (int)0;
					item = CsmsEmailLogRecipientProvider.GetByCsmsEmailRecipientId(_csmsEmailRecipientId);
					results = new TList<CsmsEmailLogRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case CsmsEmailLogRecipientSelectMethod.GetByCsmsEmailLogId:
					_csmsEmailLogId = ( values["CsmsEmailLogId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailLogId"], typeof(System.Int32)) : (int)0;
					results = CsmsEmailLogRecipientProvider.GetByCsmsEmailLogId(_csmsEmailLogId, this.StartIndex, this.PageSize, out count);
					break;
				case CsmsEmailLogRecipientSelectMethod.GetByCsmsEmailRecipientTypeId:
					_csmsEmailRecipientTypeId = ( values["CsmsEmailRecipientTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailRecipientTypeId"], typeof(System.Int32)) : (int)0;
					results = CsmsEmailLogRecipientProvider.GetByCsmsEmailRecipientTypeId(_csmsEmailRecipientTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsmsEmailLogRecipientSelectMethod.Get || SelectMethod == CsmsEmailLogRecipientSelectMethod.GetByCsmsEmailRecipientId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsmsEmailLogRecipient entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsmsEmailLogRecipientProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsmsEmailLogRecipient> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsmsEmailLogRecipientProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsmsEmailLogRecipientDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsEmailLogRecipientDataSource class.
	/// </summary>
	public class CsmsEmailLogRecipientDataSourceDesigner : ProviderDataSourceDesigner<CsmsEmailLogRecipient, CsmsEmailLogRecipientKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientDataSourceDesigner class.
		/// </summary>
		public CsmsEmailLogRecipientDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsEmailLogRecipientSelectMethod SelectMethod
		{
			get { return ((CsmsEmailLogRecipientDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsmsEmailLogRecipientDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsmsEmailLogRecipientDataSourceActionList

	/// <summary>
	/// Supports the CsmsEmailLogRecipientDataSourceDesigner class.
	/// </summary>
	internal class CsmsEmailLogRecipientDataSourceActionList : DesignerActionList
	{
		private CsmsEmailLogRecipientDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsmsEmailLogRecipientDataSourceActionList(CsmsEmailLogRecipientDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsEmailLogRecipientSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsmsEmailLogRecipientDataSourceActionList
	
	#endregion CsmsEmailLogRecipientDataSourceDesigner
	
	#region CsmsEmailLogRecipientSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsmsEmailLogRecipientDataSource.SelectMethod property.
	/// </summary>
	public enum CsmsEmailLogRecipientSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsmsEmailRecipientId method.
		/// </summary>
		GetByCsmsEmailRecipientId,
		/// <summary>
		/// Represents the GetByCsmsEmailLogId method.
		/// </summary>
		GetByCsmsEmailLogId,
		/// <summary>
		/// Represents the GetByCsmsEmailRecipientTypeId method.
		/// </summary>
		GetByCsmsEmailRecipientTypeId
	}
	
	#endregion CsmsEmailLogRecipientSelectMethod

	#region CsmsEmailLogRecipientFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientFilter : SqlFilter<CsmsEmailLogRecipientColumn>
	{
	}
	
	#endregion CsmsEmailLogRecipientFilter

	#region CsmsEmailLogRecipientExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientExpressionBuilder : SqlExpressionBuilder<CsmsEmailLogRecipientColumn>
	{
	}
	
	#endregion CsmsEmailLogRecipientExpressionBuilder	

	#region CsmsEmailLogRecipientProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsmsEmailLogRecipientChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientProperty : ChildEntityProperty<CsmsEmailLogRecipientChildEntityTypes>
	{
	}
	
	#endregion CsmsEmailLogRecipientProperty
}

