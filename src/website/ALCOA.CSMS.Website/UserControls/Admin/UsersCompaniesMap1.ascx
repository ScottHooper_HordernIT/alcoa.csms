﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersCompaniesMap1.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.UsersCompaniesMap1" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>


<script type="text/javascript">
        // <![CDATA[
    var startTime;
    function OnBeginCallback() {
        startTime = new Date();
    }
    function OnEndCallback() {
        var result = new Date() - startTime;
        result /= 1000;
        result = result.toString();
        if (result.length > 4)
            result = result.substr(0, 4);
        time.SetText(result.toString() + " sec");
        label.SetText("Time to retrieve the last data:");
    }
        // ]]> 
        </script>


<dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" OnStartRowEditing="ASPxGridView1_StartRowEditing" 
     OnRowValidating="ASPxGridView1_RowValidating" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" DataSourceID="ods"  OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter"
        onafterperformcallback="grid_AfterPerformCallback"
    KeyFieldName="UserCompanyId" OnRowUpdating="grid_RowUpdating"
     OnRowInserting="grid_RowInserting" OnRowDeleting="grid_RowDeleting" DataSourceForceStandardPaging="True" >
    <Settings  ShowFilterRow="true" ShowHeaderFilterButton="false" ShowFilterBar="Visible" />
    <SettingsBehavior ConfirmDelete="true" />
    <SettingsText ConfirmDelete="Are you sure you wish to delete this User/Company Mapping?" />
    <Columns>
        <dx:GridViewCommandColumn VisibleIndex="0">
            <EditButton Visible="True">
            </EditButton>           
            <DeleteButton Visible="True">
            </DeleteButton>            
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="UserCompanyId" ReadOnly="True" 
            Visible="False" VisibleIndex="1">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <%--<dx:GridViewDataComboBoxColumn
                    Caption="User Logon"
                    FieldName="UserLogon"
                    unboundType="String"  SortIndex="0" Settings-SortMode="DisplayText" SortOrder="Ascending"
                    VisibleIndex="2"
                    Width="400px">
                    <PropertiesComboBox
                        DataSourceID="sqldaUsersList"
                        DropDownHeight="150px"
                        TextField="UserLogon" 
                        ValueField="UserLogon" IncrementalFilteringMode="Contains"
                        ValueType="System.String">
                    </PropertiesComboBox>
                     <EditFormSettings Visible="True" />
                    </dx:GridViewDataComboBoxColumn>--%>
                    <dx:GridViewDataTextColumn FieldName="UserLogon" Caption="User Logon"
                                            SortIndex="0" SortOrder="Ascending" VisibleIndex="2">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True"  />
                                           
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                            <%--<EditItemTemplate>
                                                 <dx:ASPxComboBox id="cmbUsers1" runat="server" Width="160px"
                                                    clientinstancename="cmbUsers1"
                                                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    csspostfix="Office2003Blue" IncrementalFilteringMode="Contains"                     
                                                    valuetype="System.String" DataSourceID="sqldaUsersList" TextField="UserLogon" ValueField="UserLogon"
                                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                    </LoadingPanelImage>
                                                    <ButtonStyle Width="13px">
                                                    </ButtonStyle>
                                                    <ValidationSettings Display="Dynamic">
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </EditItemTemplate>--%>
                    </dx:GridViewDataTextColumn>
        
        <dx:GridViewDataComboBoxColumn
                    Caption="Company Name"
                    FieldName="CompanyId"
                    unboundType="String"  SortIndex="0" Settings-SortMode="DisplayText" SortOrder="Ascending"
                    VisibleIndex="2"
                    Width="400px">
                    <PropertiesComboBox
                        DataSourceID="sqldsCompaniesList"
                        DropDownHeight="150px"
                        TextField="CompanyName" 
                        ValueField="CompanyId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                    </PropertiesComboBox>
                    <EditFormSettings Visible="True" />
                    </dx:GridViewDataComboBoxColumn>
        
    </Columns>
    
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>
<br />



<div width="100%" style="padding-left:5px" align="left">
<dx:ASPxHyperLink ID="hlChangeCompanyStatus" ForeColor="Red" runat="server" Visible="true" ClientInstanceName="hlChangeCompanyStatus"
                                                        NavigateUrl="javascript:void(0);" Text="Add A New User/Company Mapping">
                                                    </dx:ASPxHyperLink></div>
<dx:ASPxPopupControl ID="popupChangeCompanyStatus" runat="server" AllowDragging="True"
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
        HeaderText="Change Company Status" Modal="True" PopupElementID="hlChangeCompanyStatus"
        Width="690px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
        ShowHeader="False">
        <SizeGripImage Height="16px" Width="16px" />
        <ContentCollection>
            <dx:PopupControlContentControl ID="popupContentChangeCompanyStatus" runat="server"
                BackColor="#DDECFE">
                <dx:ASPxRoundPanel ID="rpChangeCompanyStatus" runat="server" BackColor="#DDECFE"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                    ShowHeader="false" EnableDefaultAppearance="False" HeaderText="Change Company Status"
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="630px">
                    <TopEdge>
                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                            Repeat="RepeatX" VerticalPosition="top" />
                    </TopEdge>
                    <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                    <HeaderRightEdge>
                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                            Repeat="RepeatX" VerticalPosition="top" />
                    </HeaderRightEdge>
                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                    <HeaderLeftEdge>
                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                            Repeat="RepeatX" VerticalPosition="top" />
                    </HeaderLeftEdge>
                    <HeaderStyle BackColor="#7BA4E0">
                        <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                        <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                    </HeaderStyle>
                    <HeaderContent>
                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                            Repeat="RepeatX" VerticalPosition="top" />
                    </HeaderContent>
                    <DisabledStyle ForeColor="Gray">
                    </DisabledStyle>
                    <NoHeaderTopEdge BackColor="#DDECFE">
                    </NoHeaderTopEdge>
                    <PanelCollection>
                        <dx:PanelContent ID="PanelContent4" runat="server">
                            <dx:ASPxPanel ID="panelChangeCompanyStatus" runat="server" Width="630px">
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server">
                                        <table width="590px" style="text-align:left">
                                            <tr>
                                                <td class="style1" style="text-align:right" width="120px">
                                                    <b>User Logon: </b>
                                                </td>
                                                <td style="text-align: left">
                                                    <dx:ASPxComboBox ID="cmbUsers" Width="150px" 
                                                     EnableCallbackMode="true" CallbackPageSize="10"
                                                     ValueField="UserId" TextField="UserLogon"
                                                     OnItemsRequestedByFilterCondition="ASPxComboBox_OnItemsRequestedByFilterCondition_SQL"
                OnItemRequestedByValue="ASPxComboBox_OnItemRequestedByValue_SQL"
                       ClientInstanceName="UserId" runat="server" IncrementalFilteringMode="Contains" 
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"                         
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                       <ValidationSettings Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a User!" ErrorImage-AlternateText="Please select a User!" ValidationGroup="saveUserCompany">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a User!" />
                        </ValidationSettings>
                        <ClientSideEvents BeginCallback="function(s, e) { /*OnBeginCallback();*/ }" EndCallback="function(s, e) { /*OnEndCallback();*/ } " />

                    </dx:ASPxComboBox>
                                                    
                                                </td>

                                                <td class="style1" style="text-align:left;">
                                                    <b>Company: </b>
                                                </td>
                                                <td style="text-align: left">
                                                    
                                                    <dx:ASPxComboBox ID="cmbCompanies" Width="150px" DataSourceID="sqldsCompaniesList" 
                                                     ValueField="CompanyId" TextField="CompanyName"
                       ClientInstanceName="cmbCompanies" runat="server" IncrementalFilteringMode="StartsWith"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"                         
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                       <ValidationSettings Display="Dynamic" ValidationGroup="saveUserCompany"  ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a Company!" ErrorImage-AlternateText="Please select a Company!">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a Company!" />
                                                    </ValidationSettings>
                    </dx:ASPxComboBox>
                                                </td>
                                                <td>
                                                    <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                Text="Save" OnClick="btnSave_Click" 
                                                 ValidationGroup="saveUserCompany">
                                                
                                            </dx:ASPxButton>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <td colspan="5" align="left">
                                                <asp:Label ID="lblMessage" runat="server" Visible="false" ForeColor="Red" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                        <br />
                                        
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                            
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxRoundPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <CloseButtonImage Height="12px" Width="13px" />
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>


<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select CompanyId,CompanyName from Companies order by CompanyName" SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldaUsersList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select top 25 UserId,UserLogon from users order by UserLogon" SelectCommandType="Text">
</asp:SqlDataSource>


<asp:ObjectDataSource ID="ods" runat="server" SortParameterName="sortColumns" EnablePaging="true"
                StartRowIndexParameterName="startRecord" MaximumRowsParameterName="maxRecords"
                SelectCountMethod="GetUsersCompaniesMapCount" SelectMethod="GetUsersCompaniesMap"
                 UpdateMethod="UpdateUsersCompaniesMap" InsertMethod="InsertUsersCompaniesMap"
                  DeleteMethod="DeleteUsersCompaniesMap"
                 TypeName="UsersCompaniesMapData"></asp:ObjectDataSource>

 

