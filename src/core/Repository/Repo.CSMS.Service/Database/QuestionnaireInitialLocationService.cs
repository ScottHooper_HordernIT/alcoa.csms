﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialLocationService
    {
        QuestionnaireInitialLocation Get(Expression<Func<QuestionnaireInitialLocation, bool>> where, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes);
        List<QuestionnaireInitialLocation> GetMany(int? top, Expression<Func<QuestionnaireInitialLocation, bool>> where, Func<IQueryable<QuestionnaireInitialLocation>, IOrderedQueryable<QuestionnaireInitialLocation>> orderBy, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes);
        List<QuestionnaireInitialLocation> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialLocation, bool>> where, Func<IQueryable<QuestionnaireInitialLocation>, IOrderedQueryable<QuestionnaireInitialLocation>> orderBy, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialLocation, bool>> where, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes);
        void Insert(QuestionnaireInitialLocation item, bool saveChanges = true);
        void Update(QuestionnaireInitialLocation item, bool saveChanges = true);
        void Delete(QuestionnaireInitialLocation item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialLocationService : IQuestionnaireInitialLocationService
    {
        private readonly IRepository<QuestionnaireInitialLocation> _repository;

        public QuestionnaireInitialLocationService(IRepository<QuestionnaireInitialLocation> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialLocation Get(Expression<Func<QuestionnaireInitialLocation, bool>> where, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialLocation> GetMany(int? top, Expression<Func<QuestionnaireInitialLocation, bool>> where, Func<IQueryable<QuestionnaireInitialLocation>, IOrderedQueryable<QuestionnaireInitialLocation>> orderBy, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialLocation> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialLocation, bool>> where, Func<IQueryable<QuestionnaireInitialLocation>, IOrderedQueryable<QuestionnaireInitialLocation>> orderBy, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialLocation, bool>> where, List<Expression<Func<QuestionnaireInitialLocation, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialLocation item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialLocation");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialLocation item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialLocation");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialLocation item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialLocation");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
