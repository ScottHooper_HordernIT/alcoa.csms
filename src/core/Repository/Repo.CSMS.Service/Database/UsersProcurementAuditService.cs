﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersProcurementAuditService
    {
        UsersProcurementAudit Get(Expression<Func<UsersProcurementAudit, bool>> where, List<Expression<Func<UsersProcurementAudit, object>>> includes);
        List<UsersProcurementAudit> GetMany(int? top, Expression<Func<UsersProcurementAudit, bool>> where, Func<IQueryable<UsersProcurementAudit>, IOrderedQueryable<UsersProcurementAudit>> orderBy, List<Expression<Func<UsersProcurementAudit, object>>> includes);
        List<UsersProcurementAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersProcurementAudit, bool>> where, Func<IQueryable<UsersProcurementAudit>, IOrderedQueryable<UsersProcurementAudit>> orderBy, List<Expression<Func<UsersProcurementAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsersProcurementAudit, bool>> where, List<Expression<Func<UsersProcurementAudit, object>>> includes);
        void Insert(UsersProcurementAudit item, bool saveChanges = true);
        void Update(UsersProcurementAudit item, bool saveChanges = true);
        void Delete(UsersProcurementAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersProcurementAuditService : IUsersProcurementAuditService
    {
        private readonly IRepository<UsersProcurementAudit> _repository;

        public UsersProcurementAuditService(IRepository<UsersProcurementAudit> repository)
        {
            this._repository = repository;
        }

        public UsersProcurementAudit Get(Expression<Func<UsersProcurementAudit, bool>> where, List<Expression<Func<UsersProcurementAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersProcurementAudit> GetMany(int? top, Expression<Func<UsersProcurementAudit, bool>> where, Func<IQueryable<UsersProcurementAudit>, IOrderedQueryable<UsersProcurementAudit>> orderBy, List<Expression<Func<UsersProcurementAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersProcurementAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersProcurementAudit, bool>> where, Func<IQueryable<UsersProcurementAudit>, IOrderedQueryable<UsersProcurementAudit>> orderBy, List<Expression<Func<UsersProcurementAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersProcurementAudit, bool>> where, List<Expression<Func<UsersProcurementAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersProcurementAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurementAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersProcurementAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurementAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersProcurementAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurementAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
