﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EmailLogProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EmailLogDataSourceDesigner))]
	public class EmailLogDataSource : ProviderDataSource<EmailLog, EmailLogKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogDataSource class.
		/// </summary>
		public EmailLogDataSource() : base(new EmailLogService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EmailLogDataSourceView used by the EmailLogDataSource.
		/// </summary>
		protected EmailLogDataSourceView EmailLogView
		{
			get { return ( View as EmailLogDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EmailLogDataSource control invokes to retrieve data.
		/// </summary>
		public EmailLogSelectMethod SelectMethod
		{
			get
			{
				EmailLogSelectMethod selectMethod = EmailLogSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EmailLogSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EmailLogDataSourceView class that is to be
		/// used by the EmailLogDataSource.
		/// </summary>
		/// <returns>An instance of the EmailLogDataSourceView class.</returns>
		protected override BaseDataSourceView<EmailLog, EmailLogKey> GetNewDataSourceView()
		{
			return new EmailLogDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EmailLogDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EmailLogDataSourceView : ProviderDataSourceView<EmailLog, EmailLogKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EmailLogDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EmailLogDataSourceView(EmailLogDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EmailLogDataSource EmailLogOwner
		{
			get { return Owner as EmailLogDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EmailLogSelectMethod SelectMethod
		{
			get { return EmailLogOwner.SelectMethod; }
			set { EmailLogOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EmailLogService EmailLogProvider
		{
			get { return Provider as EmailLogService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EmailLog> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EmailLog> results = null;
			EmailLog item;
			count = 0;
			
			System.Int32 _emailLogId;
			System.String _emailTo;
			System.Int32 _emailLogTypeId;

			switch ( SelectMethod )
			{
				case EmailLogSelectMethod.Get:
					EmailLogKey entityKey  = new EmailLogKey();
					entityKey.Load(values);
					item = EmailLogProvider.Get(entityKey);
					results = new TList<EmailLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EmailLogSelectMethod.GetAll:
                    results = EmailLogProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EmailLogSelectMethod.GetPaged:
					results = EmailLogProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EmailLogSelectMethod.Find:
					if ( FilterParameters != null )
						results = EmailLogProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EmailLogProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EmailLogSelectMethod.GetByEmailLogId:
					_emailLogId = ( values["EmailLogId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EmailLogId"], typeof(System.Int32)) : (int)0;
					item = EmailLogProvider.GetByEmailLogId(_emailLogId);
					results = new TList<EmailLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EmailLogSelectMethod.GetByEmailTo:
					_emailTo = ( values["EmailTo"] != null ) ? (System.String) EntityUtil.ChangeType(values["EmailTo"], typeof(System.String)) : string.Empty;
					results = EmailLogProvider.GetByEmailTo(_emailTo, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case EmailLogSelectMethod.GetByEmailLogTypeId:
					_emailLogTypeId = ( values["EmailLogTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EmailLogTypeId"], typeof(System.Int32)) : (int)0;
					results = EmailLogProvider.GetByEmailLogTypeId(_emailLogTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EmailLogSelectMethod.Get || SelectMethod == EmailLogSelectMethod.GetByEmailLogId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EmailLog entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EmailLogProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EmailLog> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EmailLogProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EmailLogDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EmailLogDataSource class.
	/// </summary>
	public class EmailLogDataSourceDesigner : ProviderDataSourceDesigner<EmailLog, EmailLogKey>
	{
		/// <summary>
		/// Initializes a new instance of the EmailLogDataSourceDesigner class.
		/// </summary>
		public EmailLogDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EmailLogSelectMethod SelectMethod
		{
			get { return ((EmailLogDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EmailLogDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EmailLogDataSourceActionList

	/// <summary>
	/// Supports the EmailLogDataSourceDesigner class.
	/// </summary>
	internal class EmailLogDataSourceActionList : DesignerActionList
	{
		private EmailLogDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EmailLogDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EmailLogDataSourceActionList(EmailLogDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EmailLogSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EmailLogDataSourceActionList
	
	#endregion EmailLogDataSourceDesigner
	
	#region EmailLogSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EmailLogDataSource.SelectMethod property.
	/// </summary>
	public enum EmailLogSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEmailLogId method.
		/// </summary>
		GetByEmailLogId,
		/// <summary>
		/// Represents the GetByEmailTo method.
		/// </summary>
		GetByEmailTo,
		/// <summary>
		/// Represents the GetByEmailLogTypeId method.
		/// </summary>
		GetByEmailLogTypeId
	}
	
	#endregion EmailLogSelectMethod

	#region EmailLogFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogFilter : SqlFilter<EmailLogColumn>
	{
	}
	
	#endregion EmailLogFilter

	#region EmailLogExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogExpressionBuilder : SqlExpressionBuilder<EmailLogColumn>
	{
	}
	
	#endregion EmailLogExpressionBuilder	

	#region EmailLogProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EmailLogChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogProperty : ChildEntityProperty<EmailLogChildEntityTypes>
	{
	}
	
	#endregion EmailLogProperty
}

