﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface IQuestionnaireWithLocationApprovalViewService
    {
        QuestionnaireWithLocationApprovalView Get(Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes);
        List<QuestionnaireWithLocationApprovalView> GetMany(int? top, Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, Func<IQueryable<QuestionnaireWithLocationApprovalView>, IOrderedQueryable<QuestionnaireWithLocationApprovalView>> orderBy, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes);
        List<QuestionnaireWithLocationApprovalView> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, Func<IQueryable<QuestionnaireWithLocationApprovalView>, IOrderedQueryable<QuestionnaireWithLocationApprovalView>> orderBy, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes);
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireWithLocationApprovalViewService : IQuestionnaireWithLocationApprovalViewService
    {
        private readonly IRepository<QuestionnaireWithLocationApprovalView> _repository;

        public QuestionnaireWithLocationApprovalViewService(IRepository<QuestionnaireWithLocationApprovalView> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireWithLocationApprovalView Get(Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireWithLocationApprovalView> GetMany(int? top, Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, Func<IQueryable<QuestionnaireWithLocationApprovalView>, IOrderedQueryable<QuestionnaireWithLocationApprovalView>> orderBy, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireWithLocationApprovalView> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, Func<IQueryable<QuestionnaireWithLocationApprovalView>, IOrderedQueryable<QuestionnaireWithLocationApprovalView>> orderBy, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireWithLocationApprovalView, bool>> where, List<Expression<Func<QuestionnaireWithLocationApprovalView, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }
    }

    #endregion
}
