﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminTasks.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Admin.AdminTasks" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Src="AdminTaskMetrics.ascx" TagName="AdminTaskMetrics" TagPrefix="uc1" %>
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Width="900px">
    <TabPages>
        <dx:TabPage Text="Work List">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <table style="width: 870px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 870px; padding-top: 2px; text-align: center" class="pageName" align="right">
                                    <div align="right">
                                        <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                                            Customize</a>
                                    </div>
                                </td>
                            </tr>
                            <tr style="padding-top: 2px">
                                <td style="height: 13px; text-align: left">
                                    <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                        DataSourceID="sqlDaAdminTask" KeyFieldName="AdminTaskId" OnHtmlRowCreated="grid_RowCreated"
                                        Width="870px">
                                        <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="Action" FieldName="QuestionnaireId" FixedStyle="Left"
                                                ReadOnly="True" VisibleIndex="0" Width="105px">
                                                <Settings AllowHeaderFilter="False" AllowAutoFilter="False" />
                                                <EditFormSettings Visible="False" />
                                                <Settings AllowHeaderFilter="False" AllowAutoFilter="False" />
                                                <EditFormSettings Visible="False" />
                                                <DataItemTemplate>
                                                    <asp:HyperLink ID="hlPerformTask" runat="server" Text="Perform Task" NavigateUrl=""></asp:HyperLink>
                                                    <asp:HyperLink ID="hlChangeStatus" runat="server" Text="Change Status" NavigateUrl=""></asp:HyperLink>
                                                </DataItemTemplate>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="AdminTaskId" ReadOnly="True" VisibleIndex="1"
                                                Caption="Task #" Width="75px" FixedStyle="Left">
                                                <EditFormSettings Visible="False" />
                                                
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="AdminTaskStatusId" VisibleIndex="2" Caption="Status"
                                                FixedStyle="Left" GroupIndex="0" SortIndex="0" SortOrder="Descending" Settings-SortMode="DisplayText">
                                                <PropertiesComboBox DataSourceID="AdminTaskStatusDataSource1" TextField="StatusDesc"
                                                    ValueField="AdminTaskStatusId" ValueType="System.Int32">
                                                </PropertiesComboBox>
                                                <DataItemTemplate>
                                                    <dx:ASPxLabel ID="lblStatus" runat="server" Text="-">
                                                    </dx:ASPxLabel>
                                                </DataItemTemplate>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="AdminTaskSourceId" Visible="false" Caption="Source">
                                                <PropertiesComboBox DataSourceID="AdminTaskSourceDataSource1" TextField="AdminTaskSourceName"
                                                    ValueField="AdminTaskSourceId" ValueType="System.Int32">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="AdminTaskTypeId" VisibleIndex="7" Caption="Reason" Width="280px" >
                                                <PropertiesComboBox DataSourceID="AdminTaskTypeDataSource2" TextField="AdminTaskTypeName"
                                                    ValueField="AdminTaskTypeId" ValueType="System.Int32">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText" />
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="AdminTaskTypeId" VisibleIndex="6" Caption="Task"
                                                Visible="false">
                                                <PropertiesComboBox DataSourceID="AdminTaskTypeDataSource2" TextField="AdminTaskTypeName"
                                                    ValueField="AdminTaskTypeId" ValueType="System.Int32">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataDateColumn FieldName="DateOpened" VisibleIndex="2" SortOrder="Descending"
                                                SortIndex="1" Width="120px">
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="OpenedByUserId" VisibleIndex="4" Visible="false"
                                                Caption="Opened By">
                                                <PropertiesComboBox DataSourceID="UsersOpenedByDS" TextField="UserFullNameLogon"
                                                    ValueField="OpenedByUserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText" />
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataDateColumn FieldName="DateClosed" Visible="false" VisibleIndex="3">
                                            </dx:GridViewDataDateColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="ClosedByUserId" VisibleIndex="5" Visible="false"
                                                Caption="Closed By"  >
                                                <PropertiesComboBox DataSourceID="UsersClosedByDS" TextField="UserFullNameLogon"
                                                    ValueField="ClosedByUserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith" >
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText" />
                                            </dx:GridViewDataComboBoxColumn>
                                            <dx:GridViewDataComboBoxColumn FieldName="CsmsAccessId" VisibleIndex="6" Visible="false"
                                                Caption="Access">
                                                <PropertiesComboBox DataSourceID="CsmsAccessDataSource1" TextField="AccessDesc" ValueField="CsmsAccessId"
                                                    ValueType="System.Int32">
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>

                                            <dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" 
                                             VisibleIndex="8">
                                            <PropertiesComboBox DataSourceID="CompanyDS"  
                                                TextField="CompanyName" ValueField="CompanyId">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" FilterMode="Value"/>
                                           
                                        </dx:GridViewDataComboBoxColumn>

                                            <%--<dx:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" VisibleIndex="8">
                                                <PropertiesComboBox DataSourceID="CompaniesDataSource1" TextField="CompanyName" ValueField="CompanyId"
                                                    ValueType="System.Int32" DropDownButton-Visible="true" >
                                                </PropertiesComboBox>
                                                <Settings FilterMode="DisplayText" SortMode="DisplayText"  />
                                            </dx:GridViewDataComboBoxColumn>--%>
                                            <dx:GridViewDataTextColumn FieldName="Login" VisibleIndex="10" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="EmailAddress" VisibleIndex="11">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="12" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="13" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="JobRole" VisibleIndex="14" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="JobTitle" VisibleIndex="15" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="MobileNo" VisibleIndex="16" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="TelephoneNo" VisibleIndex="17" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="FaxNo" VisibleIndex="18" Visible="False">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                        <Settings ShowGroupPanel="True" ShowFilterRow="true" ShowFilterBar="Visible"/>
                                        <SettingsPager AlwaysShowPager="true" AllButton-Visible="true" />
                                        <SettingsBehavior ColumnResizeMode="NextColumn" />
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <br />
                    <dx:ASPxHyperLink ID="hlAddNewTask" runat="server" ClientInstanceName="hlAddNewTask"
                        NavigateUrl="javascript:void(0);" Text="Add New Task">
                    </dx:ASPxHyperLink>
                    <br />
                    <dx:ASPxPopupControl ID="popupAddNewTask" runat="server" AllowDragging="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" HeaderText="Change Company Status" Modal="True" PopupElementID="hlAddNewTask"
                        Width="520px" Font-Bold="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ShowHeader="False">
                        <SizeGripImage Height="16px" Width="16px" />
                        <ContentCollection>
                            <dx:PopupControlContentControl ID="popupContentAddNewTask" runat="server" BackColor="#DDECFE">
                                <dx:ASPxRoundPanel ID="rpAddNewTask" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" ShowHeader="false" EnableDefaultAppearance="False"
                                    HeaderText="Change Company Status" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    Width="490px">
                                    <TopEdge>
                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                                            Repeat="RepeatX" VerticalPosition="top" />
                                        <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                            HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                    </TopEdge>
                                    <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
                                    <HeaderRightEdge>
                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                            Repeat="RepeatX" VerticalPosition="top" />
                                        <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                            HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                    </HeaderRightEdge>
                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                    <HeaderLeftEdge>
                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                            Repeat="RepeatX" VerticalPosition="top" />
                                        <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                            HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                    </HeaderLeftEdge>
                                    <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                                    <HeaderStyle BackColor="#7BA4E0">
                                        <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
                                        <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                        <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px">
                                        </Paddings>
                                        <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                                    </HeaderStyle>
                                    <HeaderContent>
                                        <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                                            Repeat="RepeatX" VerticalPosition="top" />
                                        <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                            HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                    </HeaderContent>
                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                                    <DisabledStyle ForeColor="Gray">
                                    </DisabledStyle>
                                    <NoHeaderTopEdge BackColor="#DDECFE">
                                    </NoHeaderTopEdge>
                                    <PanelCollection>
                                        <dx:PanelContent ID="PanelContent4" runat="server">
                                            <dx:ASPxLabel ID="lblAddNewTaskError" runat="server" Font-Bold="True" ForeColor="Red">
                                            </dx:ASPxLabel>
                                            <table width="490px">
                                                <tr>
                                                    <td class="style1" style="text-align: right;">
                                                        Type:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxComboBox ID="cbAddNewTaskType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" DataSourceID="AdminTaskTypeDataSource1" EnableIncrementalFiltering="True"
                                                            IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            TextField="AdminTaskTypeName" ValueField="AdminTaskTypeId" ValueType="System.Int32"
                                                            Width="240px">
                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                            </LoadingPanelImage>
                                                            <ButtonStyle Width="13px">
                                                            </ButtonStyle>
                                                            <ValidationSettings Display="Dynamic" ValidationGroup="AddNewTask">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Access:<strong> </strong>
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                            DataSourceID="CsmsAccessDataSource1" TextField="AccessDesc" ValueField="CsmsAccessId"
                                                            Width="240px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            ID="cbAddNewTaskAccess" Enabled="true">
                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                            </LoadingPanelImage>
                                                            <ButtonStyle Width="13px">
                                                            </ButtonStyle>
                                                            <ValidationSettings Display="Dynamic" ValidationGroup="AddNewTask">
                                                                <RequiredField IsRequired="True" />
                                                                <RequiredField IsRequired="True"></RequiredField>
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Login (DOMAIN\login or email):
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskLogin" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        E-Mail Address:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                            <ValidationSettings>
                                                                <RequiredField IsRequired="True" />
                                                                <RegularExpression ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                    ErrorText="Not a valid E-Mail." />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Company:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxComboBox ID="cbAddNewTaskCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" DataSourceID="CompaniesDataSource1" EnableIncrementalFiltering="True"
                                                            IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32" Width="240px">
                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                            </LoadingPanelImage>
                                                            <ButtonStyle Width="13px">
                                                            </ButtonStyle>
                                                            <ValidationSettings Display="Dynamic" ValidationGroup="AddNewTask">
                                                                <RequiredField IsRequired="True" />
                                                            </ValidationSettings>
                                                        </dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        First Name:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskFirstName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Last Name:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskLastName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Job Title:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskJobTitle" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Mobile Number:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskMobileNumber" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Telephone Number:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskTelephoneNumber" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        Fax Number:
                                                    </td>
                                                    <td style="text-align: left">
                                                        <dx:ASPxTextBox ID="tbAddNewTaskFaxNumber" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="240px">
                                                        </dx:ASPxTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" style="text-align: right;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="text-align: left">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" colspan="2" style="text-align: left;">
                                                        Task Comments (Stored against task - Not sent to User):
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" colspan="2" style="text-align: left;">
                                                        <dx:ASPxMemo ID="mAddNewTaskMemoComments" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" Height="71px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                            Width="435px">
                                                        </dx:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <div align="center">
                                                <dx:ASPxButton ID="btnAddNewTask" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                    Text="Add New Task" Width="236px" OnClick="btnAddNewTask_Click" ValidationGroup="AddNewTask">
                                                    <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm('Are you sure you want to add this new task?');}" />
                                                    <ClientSideEvents Click="function (s, e) {e.processOnServer = confirm(&#39;Are you sure you want to add this new task?&#39;);}">
                                                    </ClientSideEvents>
                                                </dx:ASPxButton>
                                            </div>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxRoundPanel>
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                        <CloseButtonImage Height="12px" Width="13px" />
                        <CloseButtonImage Height="12px" Width="13px">
                        </CloseButtonImage>
                        <SizeGripImage Height="16px" Width="16px">
                        </SizeGripImage>
                        <HeaderStyle>
                            <Paddings PaddingRight="6px" />
                            <Paddings PaddingRight="6px"></Paddings>
                        </HeaderStyle>
                    </dx:ASPxPopupControl>
                    <data:AdminTaskDataSource ID="AdminTaskDataSource1" runat="server">
                    </data:AdminTaskDataSource>
                    <data:AdminTaskSourceDataSource ID="AdminTaskSourceDataSource1" runat="server" Sort="AdminTaskSourceName ASC">
                    </data:AdminTaskSourceDataSource>
                    <data:AdminTaskTypeDataSource ID="AdminTaskTypeDataSource1" runat="server">
                    </data:AdminTaskTypeDataSource>
                    <data:AdminTaskTypeDataSource ID="AdminTaskTypeDataSource2" runat="server" Sort="AdminTaskTypeName ASC">
                    </data:AdminTaskTypeDataSource>
                    <data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" EnableCaching="False"
                        Sort="CompanyName ASC">
                    </data:CompaniesDataSource>
                    <data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server" Sort="UserFullNameLogon ASC">
                    </data:UsersFullNameDataSource>
                    <data:AdminTaskStatusDataSource ID="AdminTaskStatusDataSource1" runat="server">
                    </data:AdminTaskStatusDataSource>
                    <data:CsmsAccessDataSource ID="CsmsAccessDataSource1" runat="server">
                    </data:CsmsAccessDataSource>
                    <asp:SqlDataSource ID="CompanyDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.CompanyId,B.CompanyName from 
dbo.AdminTask A LEFT OUTER JOIN dbo.Companies B ON
A.CompanyId=B.CompanyId Order By CompanyName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="UsersOpenedByDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.OpenedByUserId,B.UserFullNameLogon from 
dbo.AdminTask A LEFT OUTER JOIN dbo.UsersFullName B ON
A.OpenedByUserId=B.UserId Order By UserFullNameLogon">
</asp:SqlDataSource>
<asp:SqlDataSource ID="UsersClosedByDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.ClosedByUserId,B.UserFullNameLogon from 
dbo.AdminTask A LEFT OUTER JOIN dbo.UsersFullName B ON
A.ClosedByUserId=B.UserId where A.ClosedByUserId is Not Null Order By UserFullNameLogon">
</asp:SqlDataSource>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Text="Metrics">
            <ContentCollection>
                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <uc1:AdminTaskMetrics ID="AdminTaskMetrics1" runat="server" />
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dx:ASPxPageControl>
<br />
<br />
<asp:SqlDataSource ID="sqlDaAdminTask" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
 runat="server" SelectCommand="dbo.AdminTask_Get_List" SelectCommandType="StoredProcedure">

</asp:SqlDataSource>