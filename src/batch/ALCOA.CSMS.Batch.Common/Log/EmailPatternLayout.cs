﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;

namespace ALCOA.CSMS.Batch.Common.Log
{
    public class EmailPatternLayout : log4net.Layout.PatternLayout
    {
        public override string Header
        {
            get
            {
                string triggerLevelString = "NULL";

                if (Log4NetService.TriggerLevel != null)
                    triggerLevelString = Log4NetService.TriggerLevel.DisplayName;

                return String.Format("Username: {0}\nMachine: {1}\nTrigger Level: {2}\n\n", Environment.UserName, Environment.MachineName, triggerLevelString);
            }
        }
    }
}
