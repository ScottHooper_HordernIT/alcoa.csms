﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsmsEmailLogListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CsmsEmailLogListProviderBaseCore : EntityViewProviderBase<CsmsEmailLogList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CsmsEmailLogList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CsmsEmailLogList&gt;"/></returns>
		protected static VList&lt;CsmsEmailLogList&gt; Fill(DataSet dataSet, VList<CsmsEmailLogList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CsmsEmailLogList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CsmsEmailLogList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CsmsEmailLogList>"/></returns>
		protected static VList&lt;CsmsEmailLogList&gt; Fill(DataTable dataTable, VList<CsmsEmailLogList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CsmsEmailLogList c = new CsmsEmailLogList();
					c.CsmsEmailLogId = (Convert.IsDBNull(row["CsmsEmailLogId"]))?(int)0:(System.Int32)row["CsmsEmailLogId"];
					c.SentByUserId = (Convert.IsDBNull(row["SentByUserId"]))?(int)0:(System.Int32)row["SentByUserId"];
					c.SentByUserFullName = (Convert.IsDBNull(row["SentByUserFullName"]))?string.Empty:(System.String)row["SentByUserFullName"];
					c.EmailSentDateTime = (Convert.IsDBNull(row["EmailSentDateTime"]))?DateTime.MinValue:(System.DateTime)row["EmailSentDateTime"];
					c.Subject = (Convert.IsDBNull(row["Subject"]))?string.Empty:(System.String)row["Subject"];
					c.Body = (Convert.IsDBNull(row["Body"]))?new byte[] {}:(System.Byte[])row["Body"];
					c.EmailTo = (Convert.IsDBNull(row["EmailTo"]))?string.Empty:(System.String)row["EmailTo"];
					c.EmailCc = (Convert.IsDBNull(row["EmailCc"]))?string.Empty:(System.String)row["EmailCc"];
					c.EmailBcc = (Convert.IsDBNull(row["EmailBcc"]))?string.Empty:(System.String)row["EmailBcc"];
					c.AdminTaskId = (Convert.IsDBNull(row["AdminTaskId"]))?(int)0:(System.Int32)row["AdminTaskId"];
					c.AdminTaskSourceDesc = (Convert.IsDBNull(row["AdminTaskSourceDesc"]))?string.Empty:(System.String)row["AdminTaskSourceDesc"];
					c.AdminTaskTypeName = (Convert.IsDBNull(row["AdminTaskTypeName"]))?string.Empty:(System.String)row["AdminTaskTypeName"];
					c.AdminTaskTypeDesc = (Convert.IsDBNull(row["AdminTaskTypeDesc"]))?string.Empty:(System.String)row["AdminTaskTypeDesc"];
					c.StatusDesc = (Convert.IsDBNull(row["StatusDesc"]))?string.Empty:(System.String)row["StatusDesc"];
					c.AdminTaskComments = (Convert.IsDBNull(row["AdminTaskComments"]))?string.Empty:(System.String)row["AdminTaskComments"];
					c.DateOpened = (Convert.IsDBNull(row["DateOpened"]))?DateTime.MinValue:(System.DateTime)row["DateOpened"];
					c.DateClosed = (Convert.IsDBNull(row["DateClosed"]))?DateTime.MinValue:(System.DateTime?)row["DateClosed"];
					c.OpenedByUser = (Convert.IsDBNull(row["OpenedByUser"]))?string.Empty:(System.String)row["OpenedByUser"];
					c.ClosedByUser = (Convert.IsDBNull(row["ClosedByUser"]))?string.Empty:(System.String)row["ClosedByUser"];
					c.DaysWith = (Convert.IsDBNull(row["DaysWith"]))?(int)0:(System.Int32?)row["DaysWith"];
					c.BusinessDaysWith = (Convert.IsDBNull(row["BusinessDaysWith"]))?(int)0:(System.Int32?)row["BusinessDaysWith"];
					c.HoursWith = (Convert.IsDBNull(row["HoursWith"]))?(int)0:(System.Int32?)row["HoursWith"];
					c.MinsWith = (Convert.IsDBNull(row["MinsWith"]))?(int)0:(System.Int32?)row["MinsWith"];
					c.AccessDesc = (Convert.IsDBNull(row["AccessDesc"]))?string.Empty:(System.String)row["AccessDesc"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.Login = (Convert.IsDBNull(row["Login"]))?string.Empty:(System.String)row["Login"];
					c.EmailAddress = (Convert.IsDBNull(row["EmailAddress"]))?string.Empty:(System.String)row["EmailAddress"];
					c.FirstName = (Convert.IsDBNull(row["FirstName"]))?string.Empty:(System.String)row["FirstName"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.JobRole = (Convert.IsDBNull(row["JobRole"]))?string.Empty:(System.String)row["JobRole"];
					c.JobTitle = (Convert.IsDBNull(row["JobTitle"]))?string.Empty:(System.String)row["JobTitle"];
					c.MobileNo = (Convert.IsDBNull(row["MobileNo"]))?string.Empty:(System.String)row["MobileNo"];
					c.TelephoneNo = (Convert.IsDBNull(row["TelephoneNo"]))?string.Empty:(System.String)row["TelephoneNo"];
					c.FaxNo = (Convert.IsDBNull(row["FaxNo"]))?string.Empty:(System.String)row["FaxNo"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CsmsEmailLogList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CsmsEmailLogList&gt;"/></returns>
		protected VList<CsmsEmailLogList> Fill(IDataReader reader, VList<CsmsEmailLogList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CsmsEmailLogList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CsmsEmailLogList>("CsmsEmailLogList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CsmsEmailLogList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CsmsEmailLogId = (System.Int32)reader[((int)CsmsEmailLogListColumn.CsmsEmailLogId)];
					//entity.CsmsEmailLogId = (Convert.IsDBNull(reader["CsmsEmailLogId"]))?(int)0:(System.Int32)reader["CsmsEmailLogId"];
					entity.SentByUserId = (System.Int32)reader[((int)CsmsEmailLogListColumn.SentByUserId)];
					//entity.SentByUserId = (Convert.IsDBNull(reader["SentByUserId"]))?(int)0:(System.Int32)reader["SentByUserId"];
					entity.SentByUserFullName = (System.String)reader[((int)CsmsEmailLogListColumn.SentByUserFullName)];
					//entity.SentByUserFullName = (Convert.IsDBNull(reader["SentByUserFullName"]))?string.Empty:(System.String)reader["SentByUserFullName"];
					entity.EmailSentDateTime = (System.DateTime)reader[((int)CsmsEmailLogListColumn.EmailSentDateTime)];
					//entity.EmailSentDateTime = (Convert.IsDBNull(reader["EmailSentDateTime"]))?DateTime.MinValue:(System.DateTime)reader["EmailSentDateTime"];
					entity.Subject = (System.String)reader[((int)CsmsEmailLogListColumn.Subject)];
					//entity.Subject = (Convert.IsDBNull(reader["Subject"]))?string.Empty:(System.String)reader["Subject"];
					entity.Body = (System.Byte[])reader[((int)CsmsEmailLogListColumn.Body)];
					//entity.Body = (Convert.IsDBNull(reader["Body"]))?new byte[] {}:(System.Byte[])reader["Body"];
					entity.EmailTo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.EmailTo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.EmailTo)];
					//entity.EmailTo = (Convert.IsDBNull(reader["EmailTo"]))?string.Empty:(System.String)reader["EmailTo"];
					entity.EmailCc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.EmailCc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.EmailCc)];
					//entity.EmailCc = (Convert.IsDBNull(reader["EmailCc"]))?string.Empty:(System.String)reader["EmailCc"];
					entity.EmailBcc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.EmailBcc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.EmailBcc)];
					//entity.EmailBcc = (Convert.IsDBNull(reader["EmailBcc"]))?string.Empty:(System.String)reader["EmailBcc"];
					entity.AdminTaskId = (System.Int32)reader[((int)CsmsEmailLogListColumn.AdminTaskId)];
					//entity.AdminTaskId = (Convert.IsDBNull(reader["AdminTaskId"]))?(int)0:(System.Int32)reader["AdminTaskId"];
					entity.AdminTaskSourceDesc = (System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskSourceDesc)];
					//entity.AdminTaskSourceDesc = (Convert.IsDBNull(reader["AdminTaskSourceDesc"]))?string.Empty:(System.String)reader["AdminTaskSourceDesc"];
					entity.AdminTaskTypeName = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AdminTaskTypeName)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskTypeName)];
					//entity.AdminTaskTypeName = (Convert.IsDBNull(reader["AdminTaskTypeName"]))?string.Empty:(System.String)reader["AdminTaskTypeName"];
					entity.AdminTaskTypeDesc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AdminTaskTypeDesc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskTypeDesc)];
					//entity.AdminTaskTypeDesc = (Convert.IsDBNull(reader["AdminTaskTypeDesc"]))?string.Empty:(System.String)reader["AdminTaskTypeDesc"];
					entity.StatusDesc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.StatusDesc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.StatusDesc)];
					//entity.StatusDesc = (Convert.IsDBNull(reader["StatusDesc"]))?string.Empty:(System.String)reader["StatusDesc"];
					entity.AdminTaskComments = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AdminTaskComments)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskComments)];
					//entity.AdminTaskComments = (Convert.IsDBNull(reader["AdminTaskComments"]))?string.Empty:(System.String)reader["AdminTaskComments"];
					entity.DateOpened = (System.DateTime)reader[((int)CsmsEmailLogListColumn.DateOpened)];
					//entity.DateOpened = (Convert.IsDBNull(reader["DateOpened"]))?DateTime.MinValue:(System.DateTime)reader["DateOpened"];
					entity.DateClosed = (reader.IsDBNull(((int)CsmsEmailLogListColumn.DateClosed)))?null:(System.DateTime?)reader[((int)CsmsEmailLogListColumn.DateClosed)];
					//entity.DateClosed = (Convert.IsDBNull(reader["DateClosed"]))?DateTime.MinValue:(System.DateTime?)reader["DateClosed"];
					entity.OpenedByUser = (reader.IsDBNull(((int)CsmsEmailLogListColumn.OpenedByUser)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.OpenedByUser)];
					//entity.OpenedByUser = (Convert.IsDBNull(reader["OpenedByUser"]))?string.Empty:(System.String)reader["OpenedByUser"];
					entity.ClosedByUser = (System.String)reader[((int)CsmsEmailLogListColumn.ClosedByUser)];
					//entity.ClosedByUser = (Convert.IsDBNull(reader["ClosedByUser"]))?string.Empty:(System.String)reader["ClosedByUser"];
					entity.DaysWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.DaysWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.DaysWith)];
					//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
					entity.BusinessDaysWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.BusinessDaysWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.BusinessDaysWith)];
					//entity.BusinessDaysWith = (Convert.IsDBNull(reader["BusinessDaysWith"]))?(int)0:(System.Int32?)reader["BusinessDaysWith"];
					entity.HoursWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.HoursWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.HoursWith)];
					//entity.HoursWith = (Convert.IsDBNull(reader["HoursWith"]))?(int)0:(System.Int32?)reader["HoursWith"];
					entity.MinsWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.MinsWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.MinsWith)];
					//entity.MinsWith = (Convert.IsDBNull(reader["MinsWith"]))?(int)0:(System.Int32?)reader["MinsWith"];
					entity.AccessDesc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AccessDesc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AccessDesc)];
					//entity.AccessDesc = (Convert.IsDBNull(reader["AccessDesc"]))?string.Empty:(System.String)reader["AccessDesc"];
					entity.CompanyName = (reader.IsDBNull(((int)CsmsEmailLogListColumn.CompanyName)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.Login = (System.String)reader[((int)CsmsEmailLogListColumn.Login)];
					//entity.Login = (Convert.IsDBNull(reader["Login"]))?string.Empty:(System.String)reader["Login"];
					entity.EmailAddress = (System.String)reader[((int)CsmsEmailLogListColumn.EmailAddress)];
					//entity.EmailAddress = (Convert.IsDBNull(reader["EmailAddress"]))?string.Empty:(System.String)reader["EmailAddress"];
					entity.FirstName = (System.String)reader[((int)CsmsEmailLogListColumn.FirstName)];
					//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
					entity.LastName = (System.String)reader[((int)CsmsEmailLogListColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.JobRole = (System.String)reader[((int)CsmsEmailLogListColumn.JobRole)];
					//entity.JobRole = (Convert.IsDBNull(reader["JobRole"]))?string.Empty:(System.String)reader["JobRole"];
					entity.JobTitle = (System.String)reader[((int)CsmsEmailLogListColumn.JobTitle)];
					//entity.JobTitle = (Convert.IsDBNull(reader["JobTitle"]))?string.Empty:(System.String)reader["JobTitle"];
					entity.MobileNo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.MobileNo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.MobileNo)];
					//entity.MobileNo = (Convert.IsDBNull(reader["MobileNo"]))?string.Empty:(System.String)reader["MobileNo"];
					entity.TelephoneNo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.TelephoneNo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.TelephoneNo)];
					//entity.TelephoneNo = (Convert.IsDBNull(reader["TelephoneNo"]))?string.Empty:(System.String)reader["TelephoneNo"];
					entity.FaxNo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.FaxNo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.FaxNo)];
					//entity.FaxNo = (Convert.IsDBNull(reader["FaxNo"]))?string.Empty:(System.String)reader["FaxNo"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CsmsEmailLogList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CsmsEmailLogList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CsmsEmailLogList entity)
		{
			reader.Read();
			entity.CsmsEmailLogId = (System.Int32)reader[((int)CsmsEmailLogListColumn.CsmsEmailLogId)];
			//entity.CsmsEmailLogId = (Convert.IsDBNull(reader["CsmsEmailLogId"]))?(int)0:(System.Int32)reader["CsmsEmailLogId"];
			entity.SentByUserId = (System.Int32)reader[((int)CsmsEmailLogListColumn.SentByUserId)];
			//entity.SentByUserId = (Convert.IsDBNull(reader["SentByUserId"]))?(int)0:(System.Int32)reader["SentByUserId"];
			entity.SentByUserFullName = (System.String)reader[((int)CsmsEmailLogListColumn.SentByUserFullName)];
			//entity.SentByUserFullName = (Convert.IsDBNull(reader["SentByUserFullName"]))?string.Empty:(System.String)reader["SentByUserFullName"];
			entity.EmailSentDateTime = (System.DateTime)reader[((int)CsmsEmailLogListColumn.EmailSentDateTime)];
			//entity.EmailSentDateTime = (Convert.IsDBNull(reader["EmailSentDateTime"]))?DateTime.MinValue:(System.DateTime)reader["EmailSentDateTime"];
			entity.Subject = (System.String)reader[((int)CsmsEmailLogListColumn.Subject)];
			//entity.Subject = (Convert.IsDBNull(reader["Subject"]))?string.Empty:(System.String)reader["Subject"];
			entity.Body = (System.Byte[])reader[((int)CsmsEmailLogListColumn.Body)];
			//entity.Body = (Convert.IsDBNull(reader["Body"]))?new byte[] {}:(System.Byte[])reader["Body"];
			entity.EmailTo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.EmailTo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.EmailTo)];
			//entity.EmailTo = (Convert.IsDBNull(reader["EmailTo"]))?string.Empty:(System.String)reader["EmailTo"];
			entity.EmailCc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.EmailCc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.EmailCc)];
			//entity.EmailCc = (Convert.IsDBNull(reader["EmailCc"]))?string.Empty:(System.String)reader["EmailCc"];
			entity.EmailBcc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.EmailBcc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.EmailBcc)];
			//entity.EmailBcc = (Convert.IsDBNull(reader["EmailBcc"]))?string.Empty:(System.String)reader["EmailBcc"];
			entity.AdminTaskId = (System.Int32)reader[((int)CsmsEmailLogListColumn.AdminTaskId)];
			//entity.AdminTaskId = (Convert.IsDBNull(reader["AdminTaskId"]))?(int)0:(System.Int32)reader["AdminTaskId"];
			entity.AdminTaskSourceDesc = (System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskSourceDesc)];
			//entity.AdminTaskSourceDesc = (Convert.IsDBNull(reader["AdminTaskSourceDesc"]))?string.Empty:(System.String)reader["AdminTaskSourceDesc"];
			entity.AdminTaskTypeName = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AdminTaskTypeName)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskTypeName)];
			//entity.AdminTaskTypeName = (Convert.IsDBNull(reader["AdminTaskTypeName"]))?string.Empty:(System.String)reader["AdminTaskTypeName"];
			entity.AdminTaskTypeDesc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AdminTaskTypeDesc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskTypeDesc)];
			//entity.AdminTaskTypeDesc = (Convert.IsDBNull(reader["AdminTaskTypeDesc"]))?string.Empty:(System.String)reader["AdminTaskTypeDesc"];
			entity.StatusDesc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.StatusDesc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.StatusDesc)];
			//entity.StatusDesc = (Convert.IsDBNull(reader["StatusDesc"]))?string.Empty:(System.String)reader["StatusDesc"];
			entity.AdminTaskComments = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AdminTaskComments)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AdminTaskComments)];
			//entity.AdminTaskComments = (Convert.IsDBNull(reader["AdminTaskComments"]))?string.Empty:(System.String)reader["AdminTaskComments"];
			entity.DateOpened = (System.DateTime)reader[((int)CsmsEmailLogListColumn.DateOpened)];
			//entity.DateOpened = (Convert.IsDBNull(reader["DateOpened"]))?DateTime.MinValue:(System.DateTime)reader["DateOpened"];
			entity.DateClosed = (reader.IsDBNull(((int)CsmsEmailLogListColumn.DateClosed)))?null:(System.DateTime?)reader[((int)CsmsEmailLogListColumn.DateClosed)];
			//entity.DateClosed = (Convert.IsDBNull(reader["DateClosed"]))?DateTime.MinValue:(System.DateTime?)reader["DateClosed"];
			entity.OpenedByUser = (reader.IsDBNull(((int)CsmsEmailLogListColumn.OpenedByUser)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.OpenedByUser)];
			//entity.OpenedByUser = (Convert.IsDBNull(reader["OpenedByUser"]))?string.Empty:(System.String)reader["OpenedByUser"];
			entity.ClosedByUser = (System.String)reader[((int)CsmsEmailLogListColumn.ClosedByUser)];
			//entity.ClosedByUser = (Convert.IsDBNull(reader["ClosedByUser"]))?string.Empty:(System.String)reader["ClosedByUser"];
			entity.DaysWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.DaysWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.DaysWith)];
			//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
			entity.BusinessDaysWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.BusinessDaysWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.BusinessDaysWith)];
			//entity.BusinessDaysWith = (Convert.IsDBNull(reader["BusinessDaysWith"]))?(int)0:(System.Int32?)reader["BusinessDaysWith"];
			entity.HoursWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.HoursWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.HoursWith)];
			//entity.HoursWith = (Convert.IsDBNull(reader["HoursWith"]))?(int)0:(System.Int32?)reader["HoursWith"];
			entity.MinsWith = (reader.IsDBNull(((int)CsmsEmailLogListColumn.MinsWith)))?null:(System.Int32?)reader[((int)CsmsEmailLogListColumn.MinsWith)];
			//entity.MinsWith = (Convert.IsDBNull(reader["MinsWith"]))?(int)0:(System.Int32?)reader["MinsWith"];
			entity.AccessDesc = (reader.IsDBNull(((int)CsmsEmailLogListColumn.AccessDesc)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.AccessDesc)];
			//entity.AccessDesc = (Convert.IsDBNull(reader["AccessDesc"]))?string.Empty:(System.String)reader["AccessDesc"];
			entity.CompanyName = (reader.IsDBNull(((int)CsmsEmailLogListColumn.CompanyName)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.Login = (System.String)reader[((int)CsmsEmailLogListColumn.Login)];
			//entity.Login = (Convert.IsDBNull(reader["Login"]))?string.Empty:(System.String)reader["Login"];
			entity.EmailAddress = (System.String)reader[((int)CsmsEmailLogListColumn.EmailAddress)];
			//entity.EmailAddress = (Convert.IsDBNull(reader["EmailAddress"]))?string.Empty:(System.String)reader["EmailAddress"];
			entity.FirstName = (System.String)reader[((int)CsmsEmailLogListColumn.FirstName)];
			//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
			entity.LastName = (System.String)reader[((int)CsmsEmailLogListColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.JobRole = (System.String)reader[((int)CsmsEmailLogListColumn.JobRole)];
			//entity.JobRole = (Convert.IsDBNull(reader["JobRole"]))?string.Empty:(System.String)reader["JobRole"];
			entity.JobTitle = (System.String)reader[((int)CsmsEmailLogListColumn.JobTitle)];
			//entity.JobTitle = (Convert.IsDBNull(reader["JobTitle"]))?string.Empty:(System.String)reader["JobTitle"];
			entity.MobileNo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.MobileNo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.MobileNo)];
			//entity.MobileNo = (Convert.IsDBNull(reader["MobileNo"]))?string.Empty:(System.String)reader["MobileNo"];
			entity.TelephoneNo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.TelephoneNo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.TelephoneNo)];
			//entity.TelephoneNo = (Convert.IsDBNull(reader["TelephoneNo"]))?string.Empty:(System.String)reader["TelephoneNo"];
			entity.FaxNo = (reader.IsDBNull(((int)CsmsEmailLogListColumn.FaxNo)))?null:(System.String)reader[((int)CsmsEmailLogListColumn.FaxNo)];
			//entity.FaxNo = (Convert.IsDBNull(reader["FaxNo"]))?string.Empty:(System.String)reader["FaxNo"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CsmsEmailLogList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CsmsEmailLogList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CsmsEmailLogList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsmsEmailLogId = (Convert.IsDBNull(dataRow["CsmsEmailLogId"]))?(int)0:(System.Int32)dataRow["CsmsEmailLogId"];
			entity.SentByUserId = (Convert.IsDBNull(dataRow["SentByUserId"]))?(int)0:(System.Int32)dataRow["SentByUserId"];
			entity.SentByUserFullName = (Convert.IsDBNull(dataRow["SentByUserFullName"]))?string.Empty:(System.String)dataRow["SentByUserFullName"];
			entity.EmailSentDateTime = (Convert.IsDBNull(dataRow["EmailSentDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["EmailSentDateTime"];
			entity.Subject = (Convert.IsDBNull(dataRow["Subject"]))?string.Empty:(System.String)dataRow["Subject"];
			entity.Body = (Convert.IsDBNull(dataRow["Body"]))?new byte[] {}:(System.Byte[])dataRow["Body"];
			entity.EmailTo = (Convert.IsDBNull(dataRow["EmailTo"]))?string.Empty:(System.String)dataRow["EmailTo"];
			entity.EmailCc = (Convert.IsDBNull(dataRow["EmailCc"]))?string.Empty:(System.String)dataRow["EmailCc"];
			entity.EmailBcc = (Convert.IsDBNull(dataRow["EmailBcc"]))?string.Empty:(System.String)dataRow["EmailBcc"];
			entity.AdminTaskId = (Convert.IsDBNull(dataRow["AdminTaskId"]))?(int)0:(System.Int32)dataRow["AdminTaskId"];
			entity.AdminTaskSourceDesc = (Convert.IsDBNull(dataRow["AdminTaskSourceDesc"]))?string.Empty:(System.String)dataRow["AdminTaskSourceDesc"];
			entity.AdminTaskTypeName = (Convert.IsDBNull(dataRow["AdminTaskTypeName"]))?string.Empty:(System.String)dataRow["AdminTaskTypeName"];
			entity.AdminTaskTypeDesc = (Convert.IsDBNull(dataRow["AdminTaskTypeDesc"]))?string.Empty:(System.String)dataRow["AdminTaskTypeDesc"];
			entity.StatusDesc = (Convert.IsDBNull(dataRow["StatusDesc"]))?string.Empty:(System.String)dataRow["StatusDesc"];
			entity.AdminTaskComments = (Convert.IsDBNull(dataRow["AdminTaskComments"]))?string.Empty:(System.String)dataRow["AdminTaskComments"];
			entity.DateOpened = (Convert.IsDBNull(dataRow["DateOpened"]))?DateTime.MinValue:(System.DateTime)dataRow["DateOpened"];
			entity.DateClosed = (Convert.IsDBNull(dataRow["DateClosed"]))?DateTime.MinValue:(System.DateTime?)dataRow["DateClosed"];
			entity.OpenedByUser = (Convert.IsDBNull(dataRow["OpenedByUser"]))?string.Empty:(System.String)dataRow["OpenedByUser"];
			entity.ClosedByUser = (Convert.IsDBNull(dataRow["ClosedByUser"]))?string.Empty:(System.String)dataRow["ClosedByUser"];
			entity.DaysWith = (Convert.IsDBNull(dataRow["DaysWith"]))?(int)0:(System.Int32?)dataRow["DaysWith"];
			entity.BusinessDaysWith = (Convert.IsDBNull(dataRow["BusinessDaysWith"]))?(int)0:(System.Int32?)dataRow["BusinessDaysWith"];
			entity.HoursWith = (Convert.IsDBNull(dataRow["HoursWith"]))?(int)0:(System.Int32?)dataRow["HoursWith"];
			entity.MinsWith = (Convert.IsDBNull(dataRow["MinsWith"]))?(int)0:(System.Int32?)dataRow["MinsWith"];
			entity.AccessDesc = (Convert.IsDBNull(dataRow["AccessDesc"]))?string.Empty:(System.String)dataRow["AccessDesc"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.Login = (Convert.IsDBNull(dataRow["Login"]))?string.Empty:(System.String)dataRow["Login"];
			entity.EmailAddress = (Convert.IsDBNull(dataRow["EmailAddress"]))?string.Empty:(System.String)dataRow["EmailAddress"];
			entity.FirstName = (Convert.IsDBNull(dataRow["FirstName"]))?string.Empty:(System.String)dataRow["FirstName"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.JobRole = (Convert.IsDBNull(dataRow["JobRole"]))?string.Empty:(System.String)dataRow["JobRole"];
			entity.JobTitle = (Convert.IsDBNull(dataRow["JobTitle"]))?string.Empty:(System.String)dataRow["JobTitle"];
			entity.MobileNo = (Convert.IsDBNull(dataRow["MobileNo"]))?string.Empty:(System.String)dataRow["MobileNo"];
			entity.TelephoneNo = (Convert.IsDBNull(dataRow["TelephoneNo"]))?string.Empty:(System.String)dataRow["TelephoneNo"];
			entity.FaxNo = (Convert.IsDBNull(dataRow["FaxNo"]))?string.Empty:(System.String)dataRow["FaxNo"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CsmsEmailLogListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogListFilterBuilder : SqlFilterBuilder<CsmsEmailLogListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListFilterBuilder class.
		/// </summary>
		public CsmsEmailLogListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogListFilterBuilder

	#region CsmsEmailLogListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogListParameterBuilder : ParameterizedSqlFilterBuilder<CsmsEmailLogListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListParameterBuilder class.
		/// </summary>
		public CsmsEmailLogListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogListParameterBuilder
	
	#region CsmsEmailLogListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsmsEmailLogListSortBuilder : SqlSortBuilder<CsmsEmailLogListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogListSqlSortBuilder class.
		/// </summary>
		public CsmsEmailLogListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsmsEmailLogListSortBuilder

} // end namespace
