﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'Privilege' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table Privilege</remark>
	[Serializable]
	public enum PrivilegeList
	{
		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		ViewSafetyQualifications = 1, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		ViewFinancialReports = 2, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		TrainingPackageTrainers = 3, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		ViewEbiData = 4, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		ProcurementUser = 5, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		HSAssessor = 6, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		LeadHSAssessorWao = 7, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		LeadHSAssessorVic = 8, 

		/// <summary> 
		/// -
		/// </summary>
		[EnumTextValue(@"-")]
		LeadHSAssessorAus = 9, 

		/// <summary> 
		/// Typically used by Fluor / Worley Parsons for project hour calculations
		/// </summary>
		[EnumTextValue(@"Typically used by Fluor / Worley Parsons for project hour calculations")]
		ViewKpiEngineeringProjectHours = 10

	}
}
