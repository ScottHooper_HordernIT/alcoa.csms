<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Procurement" Codebehind="UsersProcurement.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<dxwgv:ASPxGridView
    ID="ASPxGridView1"
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="UsersProcurementDataSource1"
    KeyFieldName="UsersProcurementId"
    Width="900px"
    OnHtmlRowCreated="grid_RowCreated" 
    OnInitNewRow="grid_InitNewRow" OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating"
    >
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="120px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="UsersProcurementId" ReadOnly="True" Visible="False"
            VisibleIndex="0" Width="0px">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        
        <dxwgv:GridViewDataComboBoxColumn
                    Caption="Name"
                    FieldName="UserId"
                    unboundType="String"
                    Settings-SortMode="DisplayText"
                    VisibleIndex="1" SortIndex="0" SortOrder="Ascending">
                    <PropertiesComboBox
                        DataSourceID="UsersDataSource"
                        DropDownHeight="150px"
                        TextField="UserDetails"
                        ValueField="UserId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
        <dxwgv:GridViewDataCheckColumn Caption="Active" FieldName="Enabled" VisibleIndex="2" Width="60px">
        </dxwgv:GridViewDataCheckColumn>
        
        <dxwgv:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId"
                unboundType="String" VisibleIndex="3" Width="160px">
               <%-- <PropertiesComboBox DataSourceID="dsRegionsFilter" DropDownHeight="150px" TextField="RegionName"
                    ValueField="RegionId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                </PropertiesComboBox>--%>
                <PropertiesComboBox DropDownHeight="150px" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        <Items>
                        <dxe:ListEditItem Text="WAO" Value="1" />
                        <dxe:ListEditItem Text="VICOPS" Value="4" />    
                        <dxe:ListEditItem Text="ARP" Value="7" />  
                        </Items>
                        </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        
        <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId"
                unboundType="String" VisibleIndex="3" Width="160px">
                <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                    ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        
        <dxwgv:GridViewDataTextColumn Caption="# SQ Assigned as Proc Contact" VisibleIndex="3" Width="120px">
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:35px;" align="center"><asp:Label ID="noSQProcContact" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="# SQ Assigned as Func Proc Mngr" VisibleIndex="3" Width="120px">
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:35px;" align="center"><asp:Label ID="noSQFuncProcMgr" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
        </dxwgv:GridViewDataTextColumn>
        
        
    </Columns>
    <SettingsPager NumericButtonCount="5" PageSize="5" AlwaysShowPager="True" >
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <Settings ShowHeaderFilterButton="true" />
    <SettingsEditing Mode="Inline" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>


<data:UsersProcurementDataSource ID="UsersProcurementDataSource1" runat="server" EnableDeepLoad="False"
          EnableSorting="true" SelectMethod="GetAll">
</data:UsersProcurementDataSource>

<asp:SqlDataSource
	ID="UsersDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ') (' + dbo.Companies.CompanyName + ')' As UserDetails FROM dbo.Users INNER JOIN dbo.Companies ON dbo.Users.CompanyId = dbo.Companies.CompanyID AND (dbo.Users.RoleId < 3) ORDER BY dbo.Users.LastName">
</asp:SqlDataSource>

<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True" Sort="SiteName ASC"></data:SitesDataSource>
<%--<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="RegionInternalName = 'WAO' OR RegionInternalName = 'VICOPS'" Sort="RegionName ASC"></data:RegionsDataSource> --%>