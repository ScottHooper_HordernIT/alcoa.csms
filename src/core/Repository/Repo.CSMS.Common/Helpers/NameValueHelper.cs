﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using AutoMapper;
using System.Reflection;

namespace Repo.CSMS.Common.Helpers
{
    public class NameValuePair
    {
        public NameValuePair()
        {

        }

        public NameValuePair(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        public string Name { get; set; }
        public string Value { get; set; }

        public string NameValue
        {
            get
            {
                return string.Format("{0} - {1}", this.Value, this.Name);
            }
        }
    }
}
