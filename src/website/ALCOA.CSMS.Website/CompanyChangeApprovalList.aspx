﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" CodeBehind="CompanyChangeApprovalList.aspx.cs" Inherits="ALCOA.CSMS.Website.CompanyChangeApprovalList" %>
<%@ Register src="UserControls/Main/CompanyChangeApprovalList.ascx" tagname="CompanyChangeApprovalList" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
<%--DT300 7/1/15 Cindi Thornton - add style for history data row template--%>
     <style type="text/css">
        .templateTable
        {
            border-collapse: collapse;
            width: 100%;
        }
         .templateTable td {
             padding-top:6px;
         }
        .templateTable tr.templateTable_newRow
        {
            border-bottom-style: solid; 
            border-width: thin;
            margin: 6px;
            padding-bottom: 3px;
        }
        .templateTable td.dataTitle
        {
            font-weight: bold; 
            width: 200px;
            text-align:right;
            padding-right:6px;

        }
         .templateTable td.templateTable_RequestedbyCell {
             width: 200px;
         }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <uc1:CompanyChangeApprovalList ID="CompanyChangeApprovalList1" runat="server" />
</asp:Content>
