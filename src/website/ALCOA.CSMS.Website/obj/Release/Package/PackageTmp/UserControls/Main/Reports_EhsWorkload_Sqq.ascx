<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Reports_EhsWorkload_Sqq"
    CodeBehind="Reports_EhsWorkload_Sqq.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtonsPivot.ascx" TagName="ExportButtonsPivot"
    TagPrefix="uc3" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/Main/WorkLevelling/Workload_Sqq_Procurement.ascx"
    TagName="Workload_Sqq_Procurement" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/Main/WorkLevelling/Workload_Sqq_HsAssessor.ascx"
    TagName="Workload_Sqq_HsAssessor" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxCallbackPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v14.1" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v14.1" namespace="DevExpress.Web.ASPxPivotGrid.Export" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1.Web" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.1" namespace="DevExpress.XtraCharts" tagprefix="cc2" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<script type="text/javascript">
    var expanded1 = false;
    var expanded2 = false;

    function ExpandOrCollapse1() {
        if (expanded1 == true) {
            gridsqm.CollapseAll();
            expanded1 = false;
        }
        else {
            gridsqm.ExpandAll();
            expanded1 = true;
        }
    }

    function ExpandOrCollapse2() {
        if (expanded2 == true) {
            gridsqm2.CollapseAll();
            expanded2 = false;
        }
        else {
            gridsqm2.ExpandAll();
            expanded2 = true;
        }
    }
</script>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <span class="bodycopy"><span class="title">Performance Management Reports</span><br />
                <span class="date">Work Levelling Tool > Safety Qualification Questionnaires</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1"></span>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="3" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" Width="880px">
                <TabPages>
                    <dxtc:TabPage Text="EHS Consultants">
                        <ContentCollection>
                            <dxw:ContentControl runat="server">
                                <uc1:Workload_Sqq_HsAssessor runat="server" ID="Workload_Sqq_HsAssessor"></uc1:Workload_Sqq_HsAssessor>
                            </dxw:ContentControl>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Text="Procurement">
                        <ContentCollection>
                            <dxw:ContentControl runat="server">
                                <uc2:Workload_Sqq_Procurement runat="server" ID="Workload_Sqq_Procurement"></uc2:Workload_Sqq_Procurement>
                            </dxw:ContentControl>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Name="ActivityTab" Text="Days Since Activity">
                        <ContentCollection>
                            <dxw:ContentControl runat="server">
                                 <div style="overflow:auto; height:500px; width:inherit">  <%--Div added for Scroll Bar--%>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <dxe:ASPxButton ID="btn1" runat="server" AutoPostBack="False" Text="Report Manager"
                                                    UseSubmitBehavior="False" Width="120px">
                                                </dxe:ASPxButton>
                                            </td>
                                            <td style="width: 120px; padding-left: 10px">
                                                <strong>Current Report:</strong>
                                            </td>
                                            <td style="width: 600px">
                                                <dxe:ASPxLabel ID="lblLayout" runat="server" Width="100%" Font-Bold="False" Font-Italic="False"
                                                    Text="Default">
                                                </dxe:ASPxLabel>
                                            </td>
                                            <td align="right" style="text-align: right; padding-left: 50px" width="100px">
                                                <a href="javascript:gridPivot.ChangeCustomizationFieldsVisibility();">Customize</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
                                    <PanelCollection>
                                        <dx:PanelContent runat="server">
                                            <br />
                                            <dx:ASPxPivotGrid ID="gridPivot" ClientInstanceName="gridPivot" runat="server" Width="850px"
                                                DataSourceID="QuestionnaireReportDaysSinceActivityDataSource1" EnableCallBacks="False" OnHtmlFieldValuePrepared="gridPivot_OnHtmlFieldValuePrepared">
                                                <Fields>
                                                    <dx:PivotGridField ID="fieldUrl" AreaIndex="0" FieldName="QuestionnaireId" Caption="Q"
                                                        SortMode="DisplayText" Area="RowArea" Width="1">
                                                        <ValueTemplate>
                                                            <dxe:ASPxHyperLink ID="ASPxHyperLink1" runat="server" OnDataBinding="OnHyperLinkDataBinding"
                                                                NavigateUrl='1' ImageUrl="~/Images/questionnaireLink.gif" />
                                                        </ValueTemplate>
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldQuestionnaireId" AreaIndex="1" FieldName="CompanyName"
                                                        Caption="Company Name" SortMode="DisplayText" Area="RowArea">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldValidity" AreaIndex="3" FieldName="Validity" SortMode="DisplayText"
                                                        Area="RowArea">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldExpiresIn" AreaIndex="2" FieldName="ExpiresIn" SortMode="DisplayText"
                                                        Caption="Expires In" Area="RowArea">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldProcurementContact" AreaIndex="3" FieldName="ProcurementContact"
                                                        Caption="Procurement Contact" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldFunctionalManager" AreaIndex="5" FieldName="FunctionalManager"
                                                        Caption="Functional Supervisor" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldCompanyStatus" AreaIndex="4" FieldName="CompanyStatusDesc"
                                                        Caption="Company Status" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldProcurementRegion" AreaIndex="4" FieldName="ProcurementRegion"
                                                        Caption="Procurement Region" SortMode="DisplayText" Visible="False">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldProcurementSite" AreaIndex="6" FieldName="ProcurementSite"
                                                        Caption="Procurement Site" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldSafetyAssessorRegion" AreaIndex="4" FieldName="HsAssessorRegion"
                                                        Caption="Safety Assessor Region" SortMode="DisplayText" Visible="False">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldSafetyAssessorSite" AreaIndex="6" FieldName="HsAssessorSite"
                                                        Caption="Safety Assessor Site" Visible="False" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldSafetyAssessor" AreaIndex="7" FieldName="SafetyAssessor"
                                                        Caption="Safety Assessor" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldQuestionnaireStatus" AreaIndex="0" FieldName="QuestionnaireStatus"
                                                        Caption="Questionnaire Status" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldPresentlyWith" AreaIndex="2" Caption="Presently With"
                                                        FieldName="UserDescription" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldPresentlyWithDetails" AreaIndex="1" Caption="Presently With (Details)"
                                                        FieldName="ActionDescription" SortMode="DisplayText">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldDaysSinceActivity" AreaIndex="0" Caption="Days Since Activity" Options-AllowDrag="False"
                                                        FieldName="QuestionnairePresentlyWithSinceDaysCount" Area="DataArea" SortMode="Value">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldDaysSinceActivity2" AreaIndex="0" Caption="Days Since Activity"
                                                        FieldName="QuestionnairePresentlyWithSinceDaysCount" Area="ColumnArea" SortMode="Value">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldLastTimeOnSite" AreaIndex="4" FieldName="LastTimeOnSite"
                                                        Caption="Last Time On Site (EBI)" SortMode="DisplayText" Visible="False">
                                                    </dx:PivotGridField>
                                                    <dx:PivotGridField ID="fieldTypeContractor" AreaIndex="4" FieldName="TypeContractor"
                                                        Caption="Contractor Type" SortMode="DisplayText" Visible="False">
                                                    </dx:PivotGridField>
                                                </Fields>
                                                <OptionsCustomization CustomizationFormStyle="Simple" />
                                                <OptionsChartDataSource  FieldValuesProvideMode="DisplayText" />
                                                <OptionsPager Position="Bottom" RowsPerPage="5" NumericButtonCount="5">
                                                    <AllButton Visible="True">
                                                    </AllButton>
                                                </OptionsPager>
                                                <OptionsLoadingPanel>
                                                    <Image Url="~/App_Themes/Office2003BlueOld/PivotGrid/Loading.gif">
                                                    </Image>
                                                </OptionsLoadingPanel>
                                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <CustomizationFieldsBackground Url="~/App_Themes/Office2003Blue/PivotGrid/pgCustomizationFormBackground.gif">
                                                    </CustomizationFieldsBackground>
                                                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/PivotGrid/Loading.gif">
                                                    </LoadingPanel>
                                                </Images>
                                                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                    <CustomizationFieldsHeaderStyle>
                                                        <Paddings PaddingLeft="12px" PaddingRight="6px" />
                                                    </CustomizationFieldsHeaderStyle>
                                                </Styles>
                                                <OptionsView ShowColumnGrandTotals="False" ShowColumnTotals="False" ShowRowGrandTotals="False"
                                                    ShowRowTotals="False" DataHeadersPopupMinCount="1" EnableFilterControlPopupMenuScrolling="True" />
                                            </dx:ASPxPivotGrid>
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 850px; height: 100%">
                                                <tr>
                                                    <td style="width: 880px; text-align: right">
                                                        <div align="right">
                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div style="padding-top: 6px">
                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="right" style="vertical-align: middle; padding-right: 3px; text-align: -moz-right;
                                                                                        text-align: right;" valign="middle">
                                                                                        <strong>Export:</strong>
                                                                                    </td>
                                                                                    <td align="right" style="vertical-align: middle; padding-right: 2px; text-align: -moz-right;
                                                                                        text-align: right;" valign="middle">
                                                                                        <asp:ImageButton ID="imgBtnExcel" OnClick="imgBtnExcel_Click" runat="server" ToolTip="Export table to Microsoft Excel (XLS)"
                                                                                            ImageUrl="~/Images/ExportLogos/excel.gif"></asp:ImageButton>
                                                                                        <asp:ImageButton ID="imgBtnWord" OnClick="imgBtnWord_Click" runat="server" ToolTip="Export table to Microsoft Word Document (RTF)"
                                                                                            ImageUrl="~/Images/ExportLogos/word.gif"></asp:ImageButton>
                                                                                        <asp:ImageButton ID="imgBtnPdf" OnClick="imgBtnPdf_Click" runat="server" ToolTip="Export table to Adobe Acrobat (PDF)"
                                                                                            ImageUrl="~/Images/ExportLogos/pdf.gif"></asp:ImageButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="imgBtnExcel"></asp:PostBackTrigger>
                                                                    <asp:PostBackTrigger ControlID="imgBtnWord"></asp:PostBackTrigger>
                                                                    <asp:PostBackTrigger ControlID="imgBtnPdf"></asp:PostBackTrigger>
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <dx:ASPxPivotGridExporter ID="gridPivotExporter" runat="server" ASPxPivotGridID="gridPivot">
                                                            </dx:ASPxPivotGridExporter>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />


                                            <dxchartsui:WebChartControl ID="WebChartControl1" runat="server"  DataSourceID="gridPivot"
                                                Height="700px" Width="1000px" SeriesDataMember="Series" EnableCallBacks="False">
                                                <seriestemplate argumentdatamember="Arguments" valuedatamembersserializable="Values">
                                                    <ViewSerializable>
                                                        <cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
                                                        </cc1:SideBySideBarSeriesView>
                                                    </ViewSerializable>
                                                    <LabelSerializable>
                                                        <cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" Antialiasing="True">
                                                            <FillStyle >
                                                                <OptionsSerializable>
                                                                    <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                                                </OptionsSerializable>
                                                            </FillStyle>
                                                        </cc1:SideBySideBarSeriesLabel>
                                                    </LabelSerializable>
                                                    <PointOptionsSerializable>
                                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                                        </cc1:PointOptions>
                                                    </PointOptionsSerializable>
                                                    <LegendPointOptionsSerializable>
                                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                                        </cc1:PointOptions>
                                                    </LegendPointOptionsSerializable>
                                                </seriestemplate>
                                                <fillstyle>
                                                    <OptionsSerializable>
                                                        <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                                    </OptionsSerializable>
                                                </fillstyle>
                                                <diagramserializable>
                                                    <cc1:XYDiagram Rotated="True" >
                                                        <axisx visibleinpanesserializable="-1" reverse="True">
                                                            <Range SideMarginsEnabled="True"></Range>
                                                            <Label MaxWidth="200"></Label>
                                                        </axisx>
                                                        <axisy visibleinpanesserializable="-1">
                                                            <Range SideMarginsEnabled="True"></Range>
                                                        </axisy>
                                                    </cc1:XYDiagram>
                                                </diagramserializable>
                                                <legend visible="False"></legend>
                                            </dxchartsui:WebChartControl>
                                            <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" HeaderText="Report Manager"
                                                Height="213px" Width="623px" CssPostfix="Office2003Blue" EnableHotTrack="False"
                                                Modal="True" PopupElementID="btn1" AllowDragging="True" AllowResize="True" PopupHorizontalAlign="WindowCenter"
                                                PopupVerticalAlign="WindowCenter" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <SizeGripImage Height="16px" Width="16px" />
                                                <HeaderStyle>
                                                    <Paddings PaddingRight="6px" />
                                                </HeaderStyle>
                                                <CloseButtonImage Height="12px" Width="13px" />
                                                <ContentCollection>
                                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
                                                        SupportsDisabledAttribute="True">
                                                        <dxwgv:ASPxGridView runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" KeyFieldName="ReportId" AutoGenerateColumns="False"
                                                            DataSourceID="sqldsLayouts" Width="100%" ID="ASPxGridView1" OnHtmlRowCreated="grid2_RowCreated"
                                                            OnRowUpdating="grid2_RowUpdating" OnRowInserting="grid2_RowInserting" OnInitNewRow="grid2_InitNewRow"
                                                            OnRowValidating="grid2_RowValidating" OnStartRowEditing="grid2_StartRowEditing">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowInCustomizationForm="False" Name="commandCol" Width="100px"
                                                                    Caption="Action" VisibleIndex="0">
                                                                    <EditButton Visible="True" Text="Edit">
                                                                        <Image AlternateText="Edit" Url="~/Images/gridEdit.gif">
                                                                        </Image>
                                                                    </EditButton>
                                                                    <NewButton Visible="True" Text="New">
                                                                        <Image AlternateText="New" Url="~/Images/gridNew.gif">
                                                                        </Image>
                                                                    </NewButton>
                                                                    <DeleteButton Visible="True" Text="Delete">
                                                                        <Image AlternateText="Delete" Url="~/Images/gridDelete.gif">
                                                                        </Image>
                                                                    </DeleteButton>
                                                                    <CancelButton Visible="True" Text="Cancel">
                                                                        <Image AlternateText="Cancel" Url="../Images/gridCancel.gif">
                                                                        </Image>
                                                                    </CancelButton>
                                                                    <UpdateButton Visible="True" Text="Save">
                                                                        <Image AlternateText="Save" Url="../Images/gridSave.gif">
                                                                        </Image>
                                                                    </UpdateButton>
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ReportId" ReadOnly="True" ShowInCustomizationForm="True"
                                                                    Visible="False" VisibleIndex="1">
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="True" Width="30px" Caption=" "
                                                                    ToolTip="Load Selected Custom Report" VisibleIndex="1">
                                                                    <EditFormSettings Visible="False"></EditFormSettings>
                                                                    <DataItemTemplate>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="width: 30px">
                                                                                    <dxe:ASPxHyperLink ID="hlLoad" runat="server" Text="Load">
                                                                                    </dxe:ASPxHyperLink>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </DataItemTemplate>
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Area" ShowInCustomizationForm="True" Visible="False"
                                                                    VisibleIndex="2">
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ReportName" ShowInCustomizationForm="True"
                                                                    VisibleIndex="2">
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="ReportDescription" ReadOnly="True" ShowInCustomizationForm="True"
                                                                    Name="gcbReportDescription" Caption="Company" Visible="False" VisibleIndex="4">
                                                                    <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                                                                        ValueType="System.String" DropDownHeight="150px">
                                                                    </PropertiesComboBox>
                                                                    <EditFormSettings Visible="False"></EditFormSettings>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ReportLayout" ShowInCustomizationForm="True"
                                                                    Visible="False" VisibleIndex="5">
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" ReadOnly="True" ShowInCustomizationForm="True"
                                                                    Visible="False" VisibleIndex="6">
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" SortIndex="0" SortOrder="Descending"
                                                                    ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3">
                                                                    <Settings SortMode="Value"></Settings>
                                                                    <EditFormSettings Visible="True"></EditFormSettings>
                                                                </dxwgv:GridViewDataDateColumn>
                                                            </Columns>
                                                            <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
                                                            <SettingsEditing Mode="Inline"></SettingsEditing>
                                                            <SettingsPager AllButton-Visible="true" AlwaysShowPager="true">
                                                                <AllButton Visible="True">
                                                                </AllButton>
                                                            </SettingsPager>
                                                            <SettingsCookies CookiesID="CsmsWorkLevellingToolReportManager" Enabled="True" Version="0.1" />
                                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                                </LoadingPanelOnStatusBar>
                                                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--Change by Debashis--%>
                                                                </LoadingPanel>
                                                            </Images>
                                                            <ImagesFilterControl>
                                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                                </LoadingPanel>
                                                            </ImagesFilterControl>
                                                            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                                                <LoadingPanel ImageSpacing="10px">
                                                                </LoadingPanel>
                                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                                </Header>
                                                            </Styles>
                                                        </dxwgv:ASPxGridView>
                                                        <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                                                            DeleteCommand="DELETE FROM [CustomReportingLayouts] WHERE [ReportId] = @ReportId"
                                                            InsertCommand="INSERT INTO [CustomReportingLayouts] ([Area], [ReportName], [ReportDescription], [ReportLayout], [ModifiedByUserId], [ModifiedDate]) VALUES (@Area, @ReportName, @ReportDescription, @ReportLayout, @ModifiedByUserId, @ModifiedDate)"
                                                            SelectCommand="SELECT * FROM [CustomReportingLayouts] WHERE ([ModifiedByUserId] = @ModifiedByUserId AND Area = 'WLT_SQQ_DSA')"
                                                            UpdateCommand="UPDATE [CustomReportingLayouts] SET [Area] = @Area, [ReportName] = @ReportName, [ReportDescription] = @ReportDescription, [ReportLayout] = @ReportLayout, [ModifiedByUserId] = @ModifiedByUserId, [ModifiedDate] = @ModifiedDate WHERE [ReportId] = @ReportId"
                                                            ID="sqldsLayouts">
                                                            <DeleteParameters>
                                                                <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
                                                            </DeleteParameters>
                                                            <InsertParameters>
                                                                <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                                                            </InsertParameters>
                                                            <SelectParameters>
                                                                <asp:SessionParameter SessionField="UserId" DefaultValue="0" Name="ModifiedByUserId"
                                                                    Type="Int32"></asp:SessionParameter>
                                                            </SelectParameters>
                                                            <UpdateParameters>
                                                                <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                                                                <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                                                                <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
                                                            </UpdateParameters>
                                                        </asp:SqlDataSource>
                                                    </dxpc:PopupControlContentControl>
                                                </ContentCollection>
                                            </dxpc:ASPxPopupControl>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxCallbackPanel>
                               </div>       <%--Div end for scroll--%>
                            </dxw:ContentControl>
                           
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Text="Safety Qualification Metrics">
                        <ContentCollection>
                            <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxPageControl ID="ASPxPageControl2" runat="server" ActiveTabIndex="3" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    Width="900px">
                                    <TabPages>
                                        <dx:TabPage Text="Presently With">
                                            <ContentCollection>
                                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                    <p>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    Select Year:
                                                                </td>
                                                                <td style="padding-left: 2px">
                                                                    <dx:ASPxComboBox ID="cbYear1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                        Width="80px" ValueType="System.Int32">
                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                        </LoadingPanelImage>
                                                                        <ButtonStyle Width="13px">
                                                                        </ButtonStyle>
                                                                        <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                            ErrorText="Year Must Be Selected" ValidationGroup="Filter1">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                                <td style="padding-left: 2px">
                                                                    <dx:ASPxButton ID="btnFilter" runat="server" Text="Filter" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" OnClick="btnFilter_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                        ValidationGroup="Filter1">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td style="padding-left: 2px">
                                                                    <a href="javascript:ExpandOrCollapse1();">Expand/Collapse All Rows</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </p>
                                                    <dx:ASPxGridView ID="gridsqm" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" AutoGenerateColumns="False" Width="900px" ClientInstanceName="gridsqm">
                                                        <TotalSummary>
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week1" ShowInColumn="01" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="01" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week2" ShowInColumn="02" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="02" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week3" ShowInColumn="03" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="03" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week4" ShowInColumn="04" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="04" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week5" ShowInColumn="05" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="05" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week6" ShowInColumn="06" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="06" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week7" ShowInColumn="07" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="07" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week8" ShowInColumn="08" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="08" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week9" ShowInColumn="09" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="09" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week10" ShowInColumn="10" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="10" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week11" ShowInColumn="11" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="11" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week12" ShowInColumn="12" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="12" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week13" ShowInColumn="13" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="13" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week14" ShowInColumn="14" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="14" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week15" ShowInColumn="15" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="15" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week16" ShowInColumn="16" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="16" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week17" ShowInColumn="17" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="17" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week18" ShowInColumn="18" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="18" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week19" ShowInColumn="19" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="19" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week20" ShowInColumn="20" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="20" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week21" ShowInColumn="21" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="21" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week22" ShowInColumn="22" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="22" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week23" ShowInColumn="23" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="23" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week24" ShowInColumn="24" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="24" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week25" ShowInColumn="25" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="25" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week26" ShowInColumn="26" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="26" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week27" ShowInColumn="27" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="27" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week28" ShowInColumn="28" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="28" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week29" ShowInColumn="29" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="29" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week30" ShowInColumn="30" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="30" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week31" ShowInColumn="31" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="31" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week32" ShowInColumn="32" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="32" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week33" ShowInColumn="33" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="33" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week34" ShowInColumn="34" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="34" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week35" ShowInColumn="35" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="35" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week36" ShowInColumn="36" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="36" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week37" ShowInColumn="37" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="37" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week38" ShowInColumn="38" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="38" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week39" ShowInColumn="39" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="39" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week40" ShowInColumn="40" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="40" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week41" ShowInColumn="41" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="41" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week42" ShowInColumn="42" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="42" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week43" ShowInColumn="43" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="43" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week44" ShowInColumn="44" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="44" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week45" ShowInColumn="45" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="45" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week46" ShowInColumn="46" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="46" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week47" ShowInColumn="47" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="47" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week48" ShowInColumn="48" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="48" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week49" ShowInColumn="49" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="49" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week50" ShowInColumn="50" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="50" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week51" ShowInColumn="51" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="51" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week52" ShowInColumn="52" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="52" />
                                                        </TotalSummary>
                                                        <GroupSummary>
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week1" SummaryType="Sum" ShowInGroupFooterColumn="01" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week2" SummaryType="Sum" ShowInGroupFooterColumn="02" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week3" SummaryType="Sum" ShowInGroupFooterColumn="03" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week4" SummaryType="Sum" ShowInGroupFooterColumn="04" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week5" SummaryType="Sum" ShowInGroupFooterColumn="05" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week6" SummaryType="Sum" ShowInGroupFooterColumn="06" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week7" SummaryType="Sum" ShowInGroupFooterColumn="07" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week8" SummaryType="Sum" ShowInGroupFooterColumn="08" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week9" SummaryType="Sum" ShowInGroupFooterColumn="09" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week10" SummaryType="Sum" ShowInGroupFooterColumn="10" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week11" SummaryType="Sum" ShowInGroupFooterColumn="11" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week12" SummaryType="Sum" ShowInGroupFooterColumn="12" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week13" SummaryType="Sum" ShowInGroupFooterColumn="13" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week14" SummaryType="Sum" ShowInGroupFooterColumn="14" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week15" SummaryType="Sum" ShowInGroupFooterColumn="15" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week16" SummaryType="Sum" ShowInGroupFooterColumn="16" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week17" SummaryType="Sum" ShowInGroupFooterColumn="17" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week18" SummaryType="Sum" ShowInGroupFooterColumn="18" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week19" SummaryType="Sum" ShowInGroupFooterColumn="19" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week20" SummaryType="Sum" ShowInGroupFooterColumn="20" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week21" SummaryType="Sum" ShowInGroupFooterColumn="21" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week22" SummaryType="Sum" ShowInGroupFooterColumn="22" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week23" SummaryType="Sum" ShowInGroupFooterColumn="23" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week24" SummaryType="Sum" ShowInGroupFooterColumn="24" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week25" SummaryType="Sum" ShowInGroupFooterColumn="25" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week26" SummaryType="Sum" ShowInGroupFooterColumn="26" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week27" SummaryType="Sum" ShowInGroupFooterColumn="27" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week28" SummaryType="Sum" ShowInGroupFooterColumn="28" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week29" SummaryType="Sum" ShowInGroupFooterColumn="29" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week30" SummaryType="Sum" ShowInGroupFooterColumn="30" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week31" SummaryType="Sum" ShowInGroupFooterColumn="31" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week32" SummaryType="Sum" ShowInGroupFooterColumn="32" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week33" SummaryType="Sum" ShowInGroupFooterColumn="33" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week34" SummaryType="Sum" ShowInGroupFooterColumn="34" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week35" SummaryType="Sum" ShowInGroupFooterColumn="35" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week36" SummaryType="Sum" ShowInGroupFooterColumn="36" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week37" SummaryType="Sum" ShowInGroupFooterColumn="37" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week38" SummaryType="Sum" ShowInGroupFooterColumn="38" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week39" SummaryType="Sum" ShowInGroupFooterColumn="39" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week40" SummaryType="Sum" ShowInGroupFooterColumn="40" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week41" SummaryType="Sum" ShowInGroupFooterColumn="41" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week42" SummaryType="Sum" ShowInGroupFooterColumn="42" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week43" SummaryType="Sum" ShowInGroupFooterColumn="43" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week44" SummaryType="Sum" ShowInGroupFooterColumn="44" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week45" SummaryType="Sum" ShowInGroupFooterColumn="45" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week46" SummaryType="Sum" ShowInGroupFooterColumn="46" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week47" SummaryType="Sum" ShowInGroupFooterColumn="47" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week48" SummaryType="Sum" ShowInGroupFooterColumn="48" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week49" SummaryType="Sum" ShowInGroupFooterColumn="49" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week50" SummaryType="Sum" ShowInGroupFooterColumn="50" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week51" SummaryType="Sum" ShowInGroupFooterColumn="51" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week52" SummaryType="Sum" ShowInGroupFooterColumn="52" />
                                                        </GroupSummary>
                                                        <Columns>
                                                            <dx:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId" VisibleIndex="0"
                                                                FixedStyle="Left" GroupIndex="0">
                                                                <PropertiesComboBox DataSourceID="RegionsDataSource1" TextField="RegionName" ValueField="RegionId"
                                                                    ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="1"
                                                                FixedStyle="Left" GroupIndex="1">
                                                                <PropertiesComboBox DataSourceID="SitesDataSource1" TextField="SiteName" ValueField="SiteId"
                                                                    ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Presently With" FieldName="QuestionnairePresentlyWithUserId"
                                                                VisibleIndex="3" FixedStyle="Left" Width="150px">
                                                                <PropertiesComboBox DataSourceID="QuestionnairePresentlyWithUsersDataSource1" TextField="UserDescription"
                                                                    ValueField="QuestionnairePresentlyWithUserId" ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewBandColumn Caption="Week" VisibleIndex="4">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="01" FieldName="Week1" ShowInCustomizationForm="True"
                                                                        VisibleIndex="0" Width="45px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="02" FieldName="Week2" ShowInCustomizationForm="True"
                                                                        VisibleIndex="1" Width="45px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week3" VisibleIndex="6" Width="45px" Caption="03">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week4" VisibleIndex="7" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="04">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week5" VisibleIndex="8" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="05">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week6" VisibleIndex="9" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="06">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week7" VisibleIndex="10" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="07">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week8" VisibleIndex="11" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="08">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week9" VisibleIndex="12" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="09">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week10" VisibleIndex="13" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="10">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week11" VisibleIndex="14" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="11">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week12" VisibleIndex="15" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="12">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week13" VisibleIndex="16" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="13">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week14" VisibleIndex="17" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="14">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week15" VisibleIndex="18" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="15">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week16" VisibleIndex="19" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="16">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week17" VisibleIndex="20" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="17">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week18" VisibleIndex="21" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="18">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week19" VisibleIndex="22" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="19">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week20" VisibleIndex="23" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="20">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week21" VisibleIndex="24" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="21">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week22" VisibleIndex="25" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="22">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week23" VisibleIndex="26" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="23">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week24" VisibleIndex="27" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="24">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week25" VisibleIndex="28" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="25">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week26" VisibleIndex="29" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="26">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week27" VisibleIndex="30" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="27">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week28" VisibleIndex="31" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="28">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week29" VisibleIndex="32" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="29">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week30" VisibleIndex="33" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="30">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week31" VisibleIndex="34" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="31">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week32" VisibleIndex="35" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="32">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week33" VisibleIndex="36" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="33">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week34" VisibleIndex="37" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="34">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week35" VisibleIndex="38" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="35">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week36" VisibleIndex="39" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="36">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week37" VisibleIndex="40" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="37">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week38" VisibleIndex="41" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="38">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week39" VisibleIndex="42" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="39">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week40" VisibleIndex="43" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="40">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week41" VisibleIndex="44" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="41">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week42" VisibleIndex="45" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="42">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week43" VisibleIndex="46" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="43">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week44" VisibleIndex="47" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="44">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week45" VisibleIndex="48" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="45">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week46" VisibleIndex="49" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="46">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week47" VisibleIndex="50" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="47">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week48" VisibleIndex="51" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="48">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week49" VisibleIndex="52" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="49">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week50" VisibleIndex="53" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="50">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week51" VisibleIndex="54" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="51">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week52" VisibleIndex="55" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="52">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="CompanyStatusName" VisibleIndex="56" Width="125px" ShowInCustomizationForm="True"
                                                                        Caption="Company Status">
                                                                        <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dx:GridViewBandColumn>
                                                        </Columns>
                                                        <SettingsPager Mode="ShowAllRecords" PageSize="200">
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFilterBar="Auto" ShowHorizontalScrollBar="True"
                                                            ShowVerticalScrollBar="True" ShowHeaderFilterButton="True" ShowGroupFooter="VisibleAlways"
                                                            ShowFooter="True" />
                                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                            </LoadingPanelOnStatusBar>
                                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--change by Debashis--%>
                                                            </LoadingPanel>
                                                        </Images>
                                                        <ImagesFilterControl>
                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                            </LoadingPanel>
                                                        </ImagesFilterControl>
                                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                            </Header>
                                                            <LoadingPanel ImageSpacing="10px">
                                                            </LoadingPanel>
                                                        </Styles>
                                                        <StylesEditors>
                                                            <ProgressBar Height="25px">
                                                            </ProgressBar>
                                                        </StylesEditors>
                                                    </dx:ASPxGridView>
                                                    <table width="900px">
                                                        <tr align="right">
                                                            <td style="padding-top: 6px; text-align: right; text-align: -moz-right; width: 100%"
                                                                align="right">
                                                                <div align="right">
                                                                    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                        <dx:TabPage Text="Presently With Details">
                                            <ContentCollection>
                                                <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                    <p>
                                                        <table cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    Select Year:
                                                                </td>
                                                                <td style="padding-left: 2px">
                                                                    <dx:ASPxComboBox ID="cbYear2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                        Width="80px" ValueType="System.Int32">
                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                        </LoadingPanelImage>
                                                                        <ButtonStyle Width="13px">
                                                                        </ButtonStyle>
                                                                        <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                                            ErrorText="Year Must Be Selected" ValidationGroup="Filter2">
                                                                        </ValidationSettings>
                                                                    </dx:ASPxComboBox>
                                                                </td>
                                                                <td style="padding-left: 2px">
                                                                    <dx:ASPxButton ID="btnFilter2" runat="server" Text="Filter" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" OnClick="btnFilter2_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                        ValidationGroup="Filter2">
                                                                    </dx:ASPxButton>
                                                                </td>
                                                                <td style="padding-left: 2px">
                                                                    <a href="javascript:ExpandOrCollapse2();">Expand/Collapse All Rows</a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </p>
                                                    <dx:ASPxGridView ID="gridsqm2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" AutoGenerateColumns="False" Width="900px" ClientInstanceName="gridsqm2">
                                                        <Columns>
                                                            <dx:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId" VisibleIndex="0"
                                                                FixedStyle="Left" GroupIndex="0">
                                                                <PropertiesComboBox DataSourceID="RegionsDataSource1" TextField="RegionName" ValueField="RegionId"
                                                                    ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="1"
                                                                FixedStyle="Left" GroupIndex="1">
                                                                <PropertiesComboBox DataSourceID="SitesDataSource1" TextField="SiteName" ValueField="SiteId"
                                                                    ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Presently With Details" FieldName="QuestionnairePresentlyWithActionId"
                                                                VisibleIndex="2" FixedStyle="Left" Width="200px">
                                                                <PropertiesComboBox DataSourceID="QuestionnairePresentlyWithActionDataSource1" TextField="ActionDescription"
                                                                    ValueField="QuestionnairePresentlyWithActionId" ValueType="System.Int32">
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                            <dx:GridViewBandColumn Caption="Week" VisibleIndex="3">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="01" FieldName="Week1" ShowInCustomizationForm="True"
                                                                        VisibleIndex="0" Width="45px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="02" FieldName="Week2" ShowInCustomizationForm="True"
                                                                        VisibleIndex="1" Width="45px">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week3" VisibleIndex="6" Width="45px" Caption="03">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week4" VisibleIndex="7" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="04">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week5" VisibleIndex="8" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="05">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week6" VisibleIndex="9" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="06">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week7" VisibleIndex="10" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="07">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week8" VisibleIndex="11" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="08">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week9" VisibleIndex="12" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="09">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week10" VisibleIndex="13" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="10">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week11" VisibleIndex="14" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="11">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week12" VisibleIndex="15" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="12">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week13" VisibleIndex="16" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="13">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week14" VisibleIndex="17" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="14">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week15" VisibleIndex="18" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="15">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week16" VisibleIndex="19" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="16">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week17" VisibleIndex="20" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="17">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week18" VisibleIndex="21" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="18">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week19" VisibleIndex="22" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="19">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week20" VisibleIndex="23" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="20">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week21" VisibleIndex="24" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="21">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week22" VisibleIndex="25" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="22">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week23" VisibleIndex="26" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="23">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week24" VisibleIndex="27" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="24">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week25" VisibleIndex="28" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="25">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week26" VisibleIndex="29" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="26">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week27" VisibleIndex="30" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="27">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week28" VisibleIndex="31" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="28">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week29" VisibleIndex="32" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="29">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week30" VisibleIndex="33" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="30">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week31" VisibleIndex="34" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="31">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week32" VisibleIndex="35" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="32">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week33" VisibleIndex="36" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="33">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week34" VisibleIndex="37" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="34">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week35" VisibleIndex="38" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="35">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week36" VisibleIndex="39" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="36">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week37" VisibleIndex="40" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="37">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week38" VisibleIndex="41" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="38">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week39" VisibleIndex="42" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="39">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week40" VisibleIndex="43" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="40">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week41" VisibleIndex="44" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="41">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week42" VisibleIndex="45" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="42">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week43" VisibleIndex="46" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="43">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week44" VisibleIndex="47" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="44">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week45" VisibleIndex="48" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="45">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week46" VisibleIndex="49" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="46">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week47" VisibleIndex="50" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="47">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week48" VisibleIndex="51" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="48">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week49" VisibleIndex="52" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="49">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week50" VisibleIndex="53" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="50">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week51" VisibleIndex="54" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="51">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="Week52" VisibleIndex="55" Width="45px" ShowInCustomizationForm="True"
                                                                        Caption="52">
                                                                        <Settings AllowHeaderFilter="False" />
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn FieldName="CompanyStatusName" VisibleIndex="56" Width="125px" ShowInCustomizationForm="True"
                                                                        Caption="Company Status">
                                                                        <Settings AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dx:GridViewBandColumn>
                                                        </Columns>
                                                        <TotalSummary>
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week1" ShowInColumn="01" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="01" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week2" ShowInColumn="02" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="02" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week3" ShowInColumn="03" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="03" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week4" ShowInColumn="04" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="04" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week5" ShowInColumn="05" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="05" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week6" ShowInColumn="06" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="06" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week7" ShowInColumn="07" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="07" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week8" ShowInColumn="08" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="08" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week9" ShowInColumn="09" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="09" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week10" ShowInColumn="10" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="10" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week11" ShowInColumn="11" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="11" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week12" ShowInColumn="12" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="12" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week13" ShowInColumn="13" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="13" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week14" ShowInColumn="14" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="14" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week15" ShowInColumn="15" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="15" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week16" ShowInColumn="16" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="16" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week17" ShowInColumn="17" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="17" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week18" ShowInColumn="18" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="18" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week19" ShowInColumn="19" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="19" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week20" ShowInColumn="20" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="20" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week21" ShowInColumn="21" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="21" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week22" ShowInColumn="22" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="22" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week23" ShowInColumn="23" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="23" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week24" ShowInColumn="24" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="24" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week25" ShowInColumn="25" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="25" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week26" ShowInColumn="26" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="26" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week27" ShowInColumn="27" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="27" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week28" ShowInColumn="28" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="28" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week29" ShowInColumn="29" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="29" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week30" ShowInColumn="30" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="30" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week31" ShowInColumn="31" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="31" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week32" ShowInColumn="32" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="32" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week33" ShowInColumn="33" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="33" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week34" ShowInColumn="34" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="34" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week35" ShowInColumn="35" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="35" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week36" ShowInColumn="36" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="36" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week37" ShowInColumn="37" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="37" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week38" ShowInColumn="38" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="38" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week39" ShowInColumn="39" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="39" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week40" ShowInColumn="40" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="40" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week41" ShowInColumn="41" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="41" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week42" ShowInColumn="42" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="42" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week43" ShowInColumn="43" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="43" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week44" ShowInColumn="44" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="44" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week45" ShowInColumn="45" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="45" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week46" ShowInColumn="46" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="46" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week47" ShowInColumn="47" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="47" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week48" ShowInColumn="48" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="48" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week49" ShowInColumn="49" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="49" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week50" ShowInColumn="50" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="50" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week51" ShowInColumn="51" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="51" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week52" ShowInColumn="52" SummaryType="Sum"
                                                                ShowInGroupFooterColumn="52" />
                                                        </TotalSummary>
                                                        <GroupSummary>
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week1" SummaryType="Sum" ShowInGroupFooterColumn="01" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week2" SummaryType="Sum" ShowInGroupFooterColumn="02" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week3" SummaryType="Sum" ShowInGroupFooterColumn="03" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week4" SummaryType="Sum" ShowInGroupFooterColumn="04" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week5" SummaryType="Sum" ShowInGroupFooterColumn="05" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week6" SummaryType="Sum" ShowInGroupFooterColumn="06" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week7" SummaryType="Sum" ShowInGroupFooterColumn="07" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week8" SummaryType="Sum" ShowInGroupFooterColumn="08" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week9" SummaryType="Sum" ShowInGroupFooterColumn="09" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week10" SummaryType="Sum" ShowInGroupFooterColumn="10" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week11" SummaryType="Sum" ShowInGroupFooterColumn="11" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week12" SummaryType="Sum" ShowInGroupFooterColumn="12" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week13" SummaryType="Sum" ShowInGroupFooterColumn="13" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week14" SummaryType="Sum" ShowInGroupFooterColumn="14" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week15" SummaryType="Sum" ShowInGroupFooterColumn="15" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week16" SummaryType="Sum" ShowInGroupFooterColumn="16" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week17" SummaryType="Sum" ShowInGroupFooterColumn="17" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week18" SummaryType="Sum" ShowInGroupFooterColumn="18" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week19" SummaryType="Sum" ShowInGroupFooterColumn="19" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week20" SummaryType="Sum" ShowInGroupFooterColumn="20" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week21" SummaryType="Sum" ShowInGroupFooterColumn="21" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week22" SummaryType="Sum" ShowInGroupFooterColumn="22" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week23" SummaryType="Sum" ShowInGroupFooterColumn="23" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week24" SummaryType="Sum" ShowInGroupFooterColumn="24" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week25" SummaryType="Sum" ShowInGroupFooterColumn="25" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week26" SummaryType="Sum" ShowInGroupFooterColumn="26" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week27" SummaryType="Sum" ShowInGroupFooterColumn="27" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week28" SummaryType="Sum" ShowInGroupFooterColumn="28" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week29" SummaryType="Sum" ShowInGroupFooterColumn="29" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week30" SummaryType="Sum" ShowInGroupFooterColumn="30" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week31" SummaryType="Sum" ShowInGroupFooterColumn="31" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week32" SummaryType="Sum" ShowInGroupFooterColumn="32" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week33" SummaryType="Sum" ShowInGroupFooterColumn="33" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week34" SummaryType="Sum" ShowInGroupFooterColumn="34" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week35" SummaryType="Sum" ShowInGroupFooterColumn="35" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week36" SummaryType="Sum" ShowInGroupFooterColumn="36" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week37" SummaryType="Sum" ShowInGroupFooterColumn="37" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week38" SummaryType="Sum" ShowInGroupFooterColumn="38" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week39" SummaryType="Sum" ShowInGroupFooterColumn="39" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week40" SummaryType="Sum" ShowInGroupFooterColumn="40" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week41" SummaryType="Sum" ShowInGroupFooterColumn="41" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week42" SummaryType="Sum" ShowInGroupFooterColumn="42" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week43" SummaryType="Sum" ShowInGroupFooterColumn="43" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week44" SummaryType="Sum" ShowInGroupFooterColumn="44" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week45" SummaryType="Sum" ShowInGroupFooterColumn="45" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week46" SummaryType="Sum" ShowInGroupFooterColumn="46" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week47" SummaryType="Sum" ShowInGroupFooterColumn="47" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week48" SummaryType="Sum" ShowInGroupFooterColumn="48" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week49" SummaryType="Sum" ShowInGroupFooterColumn="49" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week50" SummaryType="Sum" ShowInGroupFooterColumn="50" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week51" SummaryType="Sum" ShowInGroupFooterColumn="51" />
                                                            <dx:ASPxSummaryItem DisplayFormat="{0:n0}" FieldName="Week52" SummaryType="Sum" ShowInGroupFooterColumn="52" />
                                                        </GroupSummary>
                                                        <SettingsPager Mode="ShowAllRecords" PageSize="200">
                                                        </SettingsPager>
                                                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFilterBar="Auto" ShowHorizontalScrollBar="True"
                                                            ShowVerticalScrollBar="True" ShowHeaderFilterButton="True" ShowGroupFooter="VisibleAlways"
                                                            ShowFooter="True" />
                                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                            </LoadingPanelOnStatusBar>
                                                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--change by Debashis--%>
                                                            </LoadingPanel>
                                                        </Images>
                                                        <ImagesFilterControl>
                                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                            </LoadingPanel>
                                                        </ImagesFilterControl>
                                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                            </Header>
                                                            <LoadingPanel ImageSpacing="10px">
                                                            </LoadingPanel>
                                                        </Styles>
                                                        <StylesEditors>
                                                            <ProgressBar Height="25px">
                                                            </ProgressBar>
                                                        </StylesEditors>
                                                    </dx:ASPxGridView>
                                                    <table width="900px">
                                                        <tr align="right">
                                                            <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
                                                                width: 100%" align="right">
                                                                <div align="right">
                                                                    <uc1:ExportButtons ID="ucExportButtons2" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                    </TabPages>
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <LoadingPanelStyle ImageSpacing="6px">
                                    </LoadingPanelStyle>
                                    <ContentStyle>
                                        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                    </ContentStyle>
                                </dx:ASPxPageControl>
                                <data:QuestionnairePresentlyWithUsersDataSource ID="QuestionnairePresentlyWithUsersDataSource1"
                                    runat="server">
                                </data:QuestionnairePresentlyWithUsersDataSource>
                                <data:SitesDataSource ID="SitesDataSource1" runat="server">
                                </data:SitesDataSource>
                                <data:RegionsDataSource ID="RegionsDataSource1" runat="server">
                                </data:RegionsDataSource>
                                <data:QuestionnairePresentlyWithActionDataSource ID="QuestionnairePresentlyWithActionDataSource1"
                                    runat="server">
                                </data:QuestionnairePresentlyWithActionDataSource>
                            </dxw:ContentControl>
                        </ContentCollection>
                    </dxtc:TabPage>
                </TabPages>
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <LoadingPanelStyle ImageSpacing="6px">
                </LoadingPanelStyle>
                <ContentStyle>
                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </ContentStyle>
            </dxtc:ASPxPageControl>
            <br />
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="UsersEhsConsultantsDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_UsersEhsConsultants_GetAll_Active" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:ObjectDataSource ID="dsQuestionnaire_CountEhsConsultantsUnAllocated" runat="server"
    TypeName="KaiZen.CSMS.Services.QuestionnaireService" SelectMethod="CountEhsConsultantsUnAllocated">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="%" Name="CurrentYear" SessionField="spVar_Year"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="dsQuestionnaire_GetDistinctYears" runat="server" TypeName="KaiZen.CSMS.Services.QuestionnaireService"
    SelectMethod="GetDistinctYears"></asp:ObjectDataSource>
&nbsp;<br />
<asp:SqlDataSource ID="dsQuestionnaire_GetProcurementWorkLoad" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Questionnaire_GetProcurementWorkLoad" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="-1" Name="FunctionalProcurementManagerUserId"
            SessionField="FunctionalProcurementManagerUserId" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<data:UsersProcurementListDataSource ID="UsersProcurementListDataSource1" runat="server">
</data:UsersProcurementListDataSource>
<asp:SqlDataSource ID="dsQuestionnaire_GetAssignedFunctionalProcurementManagers"
    runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Questionnaire_GetAssignedFunctionalProcurementManagers" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<data:QuestionnaireReportTimeDelayAssessmentCompleteDataSource ID="QuestionnaireReportTimeDelayAssessmentCompleteDataSource1"
    runat="server">
</data:QuestionnaireReportTimeDelayAssessmentCompleteDataSource>
<data:QuestionnaireReportTimeDelayBeingAssessedDataSource ID="QuestionnaireReportTimeDelayBeingAssessedDataSource1"
    runat="server">
</data:QuestionnaireReportTimeDelayBeingAssessedDataSource>
<data:QuestionnaireReportDaysSinceActivityDataSource ID="QuestionnaireReportDaysSinceActivityDataSource1"
    runat="server">
</data:QuestionnaireReportDaysSinceActivityDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
