﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TwentyOnePointAudit_14' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TwentyOnePointAudit14 : TwentyOnePointAudit14Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TwentyOnePointAudit14"/> instance.
		///</summary>
		public TwentyOnePointAudit14():base(){}	
		
		#endregion
	}
}
