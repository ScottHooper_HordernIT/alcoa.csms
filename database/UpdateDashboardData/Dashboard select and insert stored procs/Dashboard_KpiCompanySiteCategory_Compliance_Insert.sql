USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[Dashboard_KpiCompanySiteCategory_Compliance_Insert]    Script Date: 3/08/2015 1:17:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Dashboard_KpiCompanySiteCategory_Compliance_Insert]              
(              
 @Site int              
 ,@Month int              
 ,@Year int              
 ,@AverageNoOfPeopleOnSite decimal(18,2)              
 ,@TotalManHours decimal(18,2)              
 ,@LWDFR decimal(18,2)              
 ,@TRIFR decimal(18,2)              
 ,@AIFR decimal(18,2)              
 ,@DART decimal(18,2)              
 ,@LWD decimal(18,2)              
 ,@RST decimal(18,2)              
 ,@MT decimal(18,2)              
 ,@FA decimal(18,2)              
 ,@IFE decimal(18,2)              
 ,@RN decimal(18,2)              
 ,@RiskNotifications decimal(18,2)              
 ,@IFEInjuryRatio decimal(18,2)              
 ,@RNInjuryRatio decimal(18,2)              
 ,@IFERNCount decimal(18,2)              
 ,@IFERNInjuryRatio decimal(18,2)              
              
)              
AS              
              
if exists(Select * from  Dashboard_KpiCompanySiteCategory_Compliance where SiteId=@Site and [Month]=@Month  
 and [Year]=@Year)  
 begin   
 Update Dashboard_KpiCompanySiteCategory_Compliance Set  
   [Average No. Of People On Site]  =  @AverageNoOfPeopleOnSite           
     ,[Total Man Hours] =  @TotalManHours      
     ,[LWDFR]=    @LWDFR      
     ,[TRIFR]= @TRIFR            
     ,[AIFR]= @AIFR          
     ,[DART] =@DART             
     ,[LWD] = @LWD             
     ,[RST] = @RST             
     ,[MT]= @MT             
     ,[FA] = @FA             
     ,[IFE] = @IFE             
     ,[RN]  = @RN            
     ,[Risk Notifications] = @RiskNotifications             
     ,[IFE Injury Ratio] = @IFEInjuryRatio             
     ,[RN Injury Ratio] = @RNInjuryRatio             
     ,[IFE+RN Count]  = @IFERNCount            
     ,[IFERN Injury Ratio] =@IFERNInjuryRatio  
	 ,[LastModifiedDate] = GETDATE()
where SiteId=@Site AND [Month]=@Month AND [Year]=@Year  
   end  
else  
begin            
    INSERT INTO [dbo].[Dashboard_KpiCompanySiteCategory_Compliance]              
     (              
     [SiteId]              
     ,[Month]              
     ,[Year]              
     ,[Average No. Of People On Site]              
     ,[Total Man Hours]               
     ,[LWDFR]              
     ,[TRIFR]              
     ,[AIFR]              
     ,[DART]              
     ,[LWD]              
     ,[RST]              
     ,[MT]              
     ,[FA]              
     ,[IFE]              
     ,[RN]              
     ,[Risk Notifications]              
     ,[IFE Injury Ratio]              
     ,[RN Injury Ratio]              
     ,[IFE+RN Count]              
     ,[IFERN Injury Ratio] 
     ,[LastModifiedDate]             
     )              
    VALUES              
     (              
     @Site              
     ,@Month              
     ,@Year              
     ,@AverageNoOfPeopleOnSite              
     ,@TotalManHours               
     ,@LWDFR              
     ,@TRIFR              
     ,@AIFR              
     ,@DART              
     ,@LWD              
     ,@RST              
     ,@MT              
     ,@FA              
     ,@IFE              
     ,@RN              
     ,@RiskNotifications              
     ,@IFEInjuryRatio              
     ,@RNInjuryRatio              
     ,@IFERNCount              
     ,@IFERNInjuryRatio 
     ,GETDATE()             
     )     
end  
----------------------------------------

GO


