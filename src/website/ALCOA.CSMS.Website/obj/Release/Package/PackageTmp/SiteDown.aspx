﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="SiteDown" Codebehind="SiteDown.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Alcoa | Contractor Services Management System | Site Currently Offline</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: center">
        <strong>
            <img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" style="font-weight: bold"
                width="153" /><br />
        </strong>
        <div style="text-align: center">
            <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" style="font-weight: bold"
                width="900">
                <tr>
                    <td align="right" style="height: 15px; text-align: center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <img height="3" src="Images/spacer.gif" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <span style="color: #003399">Alcoa World Alumina - Australian Operations</span></td>
                </tr>
                <tr>
                    <td align="right" style="text-align: center" valign="bottom">
                        <span class="sitetitleblue"><a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">
                            Contractor Services Management System</a></span>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="text-align: center; height: 13px;" valign="bottom">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="height: 13px; text-align: center" valign="bottom">
                        <img src="Images/Temp-Unavailable.gif" /></td>
                </tr>
                <tr>
                    <td align="right" style="text-align: center; height: 60px;" valign="bottom">
                        <span style="font-size: 10pt; font-weight:normal;">
                            <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%">This website is currently offline for <asp:Label id="Label1" runat="server" Text="Scheduled Maintanence" ForeColor="DimGray" Font-Size="10pt" Font-Bold="False"></asp:Label>.<BR />The estimated time that service will be restored by is <asp:Label id="Label2" runat="server" Text="Monday 21st, 2:00PM (GMT +8)" ForeColor="DimGray" Font-Size="10pt" Font-Bold="False"></asp:Label>. <BR />Please <A href="javascript:window.close();">Close this window</A> and&nbsp;try again&nbsp;after the above specified time.<BR /><BR /><BR />For further information please contact the <dxe:ASPxHyperLink id="ASPxHyperLinkContact" runat="server" Text="AUA Alcoa Contractor Services Team"></dxe:ASPxHyperLink>.</asp:Panel>
                        </span>&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="height: 15px; text-align: center" valign="bottom">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="height: 13px; text-align: center" valign="bottom">
        </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
