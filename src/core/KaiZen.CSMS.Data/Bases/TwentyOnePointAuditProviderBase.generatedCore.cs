﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit, KaiZen.CSMS.Entities.TwentyOnePointAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAuditKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Sites key.
		///		FK_TwentyOnePointAudit_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Sites key.
		///		FK_TwentyOnePointAudit_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Sites key.
		///		FK_TwentyOnePointAudit_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Sites key.
		///		fkTwentyOnePointAuditSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Sites key.
		///		fkTwentyOnePointAuditSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Sites key.
		///		FK_TwentyOnePointAudit_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_01 key.
		///		FK_TwentyOnePointAudit_01 Description: 
		/// </summary>
		/// <param name="_point01Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint01Id(System.Int32? _point01Id)
		{
			int count = -1;
			return GetByPoint01Id(_point01Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_01 key.
		///		FK_TwentyOnePointAudit_01 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point01Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint01Id(TransactionManager transactionManager, System.Int32? _point01Id)
		{
			int count = -1;
			return GetByPoint01Id(transactionManager, _point01Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_01 key.
		///		FK_TwentyOnePointAudit_01 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point01Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint01Id(TransactionManager transactionManager, System.Int32? _point01Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint01Id(transactionManager, _point01Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_01 key.
		///		fkTwentyOnePointAudit01 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point01Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint01Id(System.Int32? _point01Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint01Id(null, _point01Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_01 key.
		///		fkTwentyOnePointAudit01 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point01Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint01Id(System.Int32? _point01Id, int start, int pageLength,out int count)
		{
			return GetByPoint01Id(null, _point01Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_01 key.
		///		FK_TwentyOnePointAudit_01 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point01Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint01Id(TransactionManager transactionManager, System.Int32? _point01Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_02 key.
		///		FK_TwentyOnePointAudit_02 Description: 
		/// </summary>
		/// <param name="_point02Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint02Id(System.Int32? _point02Id)
		{
			int count = -1;
			return GetByPoint02Id(_point02Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_02 key.
		///		FK_TwentyOnePointAudit_02 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point02Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint02Id(TransactionManager transactionManager, System.Int32? _point02Id)
		{
			int count = -1;
			return GetByPoint02Id(transactionManager, _point02Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_02 key.
		///		FK_TwentyOnePointAudit_02 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point02Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint02Id(TransactionManager transactionManager, System.Int32? _point02Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint02Id(transactionManager, _point02Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_02 key.
		///		fkTwentyOnePointAudit02 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point02Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint02Id(System.Int32? _point02Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint02Id(null, _point02Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_02 key.
		///		fkTwentyOnePointAudit02 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point02Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint02Id(System.Int32? _point02Id, int start, int pageLength,out int count)
		{
			return GetByPoint02Id(null, _point02Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_02 key.
		///		FK_TwentyOnePointAudit_02 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point02Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint02Id(TransactionManager transactionManager, System.Int32? _point02Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_03 key.
		///		FK_TwentyOnePointAudit_03 Description: 
		/// </summary>
		/// <param name="_point03Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint03Id(System.Int32? _point03Id)
		{
			int count = -1;
			return GetByPoint03Id(_point03Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_03 key.
		///		FK_TwentyOnePointAudit_03 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point03Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint03Id(TransactionManager transactionManager, System.Int32? _point03Id)
		{
			int count = -1;
			return GetByPoint03Id(transactionManager, _point03Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_03 key.
		///		FK_TwentyOnePointAudit_03 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point03Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint03Id(TransactionManager transactionManager, System.Int32? _point03Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint03Id(transactionManager, _point03Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_03 key.
		///		fkTwentyOnePointAudit03 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point03Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint03Id(System.Int32? _point03Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint03Id(null, _point03Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_03 key.
		///		fkTwentyOnePointAudit03 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point03Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint03Id(System.Int32? _point03Id, int start, int pageLength,out int count)
		{
			return GetByPoint03Id(null, _point03Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_03 key.
		///		FK_TwentyOnePointAudit_03 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point03Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint03Id(TransactionManager transactionManager, System.Int32? _point03Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_04 key.
		///		FK_TwentyOnePointAudit_04 Description: 
		/// </summary>
		/// <param name="_point04Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint04Id(System.Int32? _point04Id)
		{
			int count = -1;
			return GetByPoint04Id(_point04Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_04 key.
		///		FK_TwentyOnePointAudit_04 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point04Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint04Id(TransactionManager transactionManager, System.Int32? _point04Id)
		{
			int count = -1;
			return GetByPoint04Id(transactionManager, _point04Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_04 key.
		///		FK_TwentyOnePointAudit_04 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point04Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint04Id(TransactionManager transactionManager, System.Int32? _point04Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint04Id(transactionManager, _point04Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_04 key.
		///		fkTwentyOnePointAudit04 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point04Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint04Id(System.Int32? _point04Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint04Id(null, _point04Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_04 key.
		///		fkTwentyOnePointAudit04 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point04Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint04Id(System.Int32? _point04Id, int start, int pageLength,out int count)
		{
			return GetByPoint04Id(null, _point04Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_04 key.
		///		FK_TwentyOnePointAudit_04 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point04Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint04Id(TransactionManager transactionManager, System.Int32? _point04Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_05 key.
		///		FK_TwentyOnePointAudit_05 Description: 
		/// </summary>
		/// <param name="_point05Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint05Id(System.Int32? _point05Id)
		{
			int count = -1;
			return GetByPoint05Id(_point05Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_05 key.
		///		FK_TwentyOnePointAudit_05 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point05Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint05Id(TransactionManager transactionManager, System.Int32? _point05Id)
		{
			int count = -1;
			return GetByPoint05Id(transactionManager, _point05Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_05 key.
		///		FK_TwentyOnePointAudit_05 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point05Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint05Id(TransactionManager transactionManager, System.Int32? _point05Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint05Id(transactionManager, _point05Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_05 key.
		///		fkTwentyOnePointAudit05 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point05Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint05Id(System.Int32? _point05Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint05Id(null, _point05Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_05 key.
		///		fkTwentyOnePointAudit05 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point05Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint05Id(System.Int32? _point05Id, int start, int pageLength,out int count)
		{
			return GetByPoint05Id(null, _point05Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_05 key.
		///		FK_TwentyOnePointAudit_05 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point05Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint05Id(TransactionManager transactionManager, System.Int32? _point05Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_06 key.
		///		FK_TwentyOnePointAudit_06 Description: 
		/// </summary>
		/// <param name="_point06Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint06Id(System.Int32? _point06Id)
		{
			int count = -1;
			return GetByPoint06Id(_point06Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_06 key.
		///		FK_TwentyOnePointAudit_06 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point06Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint06Id(TransactionManager transactionManager, System.Int32? _point06Id)
		{
			int count = -1;
			return GetByPoint06Id(transactionManager, _point06Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_06 key.
		///		FK_TwentyOnePointAudit_06 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point06Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint06Id(TransactionManager transactionManager, System.Int32? _point06Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint06Id(transactionManager, _point06Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_06 key.
		///		fkTwentyOnePointAudit06 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point06Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint06Id(System.Int32? _point06Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint06Id(null, _point06Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_06 key.
		///		fkTwentyOnePointAudit06 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point06Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint06Id(System.Int32? _point06Id, int start, int pageLength,out int count)
		{
			return GetByPoint06Id(null, _point06Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_06 key.
		///		FK_TwentyOnePointAudit_06 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point06Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint06Id(TransactionManager transactionManager, System.Int32? _point06Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_07 key.
		///		FK_TwentyOnePointAudit_07 Description: 
		/// </summary>
		/// <param name="_point07Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint07Id(System.Int32? _point07Id)
		{
			int count = -1;
			return GetByPoint07Id(_point07Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_07 key.
		///		FK_TwentyOnePointAudit_07 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point07Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint07Id(TransactionManager transactionManager, System.Int32? _point07Id)
		{
			int count = -1;
			return GetByPoint07Id(transactionManager, _point07Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_07 key.
		///		FK_TwentyOnePointAudit_07 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point07Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint07Id(TransactionManager transactionManager, System.Int32? _point07Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint07Id(transactionManager, _point07Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_07 key.
		///		fkTwentyOnePointAudit07 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point07Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint07Id(System.Int32? _point07Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint07Id(null, _point07Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_07 key.
		///		fkTwentyOnePointAudit07 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point07Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint07Id(System.Int32? _point07Id, int start, int pageLength,out int count)
		{
			return GetByPoint07Id(null, _point07Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_07 key.
		///		FK_TwentyOnePointAudit_07 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point07Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint07Id(TransactionManager transactionManager, System.Int32? _point07Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_08 key.
		///		FK_TwentyOnePointAudit_08 Description: 
		/// </summary>
		/// <param name="_point08Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint08Id(System.Int32? _point08Id)
		{
			int count = -1;
			return GetByPoint08Id(_point08Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_08 key.
		///		FK_TwentyOnePointAudit_08 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point08Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint08Id(TransactionManager transactionManager, System.Int32? _point08Id)
		{
			int count = -1;
			return GetByPoint08Id(transactionManager, _point08Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_08 key.
		///		FK_TwentyOnePointAudit_08 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point08Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint08Id(TransactionManager transactionManager, System.Int32? _point08Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint08Id(transactionManager, _point08Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_08 key.
		///		fkTwentyOnePointAudit08 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point08Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint08Id(System.Int32? _point08Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint08Id(null, _point08Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_08 key.
		///		fkTwentyOnePointAudit08 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point08Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint08Id(System.Int32? _point08Id, int start, int pageLength,out int count)
		{
			return GetByPoint08Id(null, _point08Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_08 key.
		///		FK_TwentyOnePointAudit_08 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point08Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint08Id(TransactionManager transactionManager, System.Int32? _point08Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_09 key.
		///		FK_TwentyOnePointAudit_09 Description: 
		/// </summary>
		/// <param name="_point09Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint09Id(System.Int32? _point09Id)
		{
			int count = -1;
			return GetByPoint09Id(_point09Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_09 key.
		///		FK_TwentyOnePointAudit_09 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point09Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint09Id(TransactionManager transactionManager, System.Int32? _point09Id)
		{
			int count = -1;
			return GetByPoint09Id(transactionManager, _point09Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_09 key.
		///		FK_TwentyOnePointAudit_09 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point09Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint09Id(TransactionManager transactionManager, System.Int32? _point09Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint09Id(transactionManager, _point09Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_09 key.
		///		fkTwentyOnePointAudit09 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point09Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint09Id(System.Int32? _point09Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint09Id(null, _point09Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_09 key.
		///		fkTwentyOnePointAudit09 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point09Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint09Id(System.Int32? _point09Id, int start, int pageLength,out int count)
		{
			return GetByPoint09Id(null, _point09Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_09 key.
		///		FK_TwentyOnePointAudit_09 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point09Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint09Id(TransactionManager transactionManager, System.Int32? _point09Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_10 key.
		///		FK_TwentyOnePointAudit_10 Description: 
		/// </summary>
		/// <param name="_point10Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint10Id(System.Int32? _point10Id)
		{
			int count = -1;
			return GetByPoint10Id(_point10Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_10 key.
		///		FK_TwentyOnePointAudit_10 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point10Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint10Id(TransactionManager transactionManager, System.Int32? _point10Id)
		{
			int count = -1;
			return GetByPoint10Id(transactionManager, _point10Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_10 key.
		///		FK_TwentyOnePointAudit_10 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point10Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint10Id(TransactionManager transactionManager, System.Int32? _point10Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint10Id(transactionManager, _point10Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_10 key.
		///		fkTwentyOnePointAudit10 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point10Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint10Id(System.Int32? _point10Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint10Id(null, _point10Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_10 key.
		///		fkTwentyOnePointAudit10 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point10Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint10Id(System.Int32? _point10Id, int start, int pageLength,out int count)
		{
			return GetByPoint10Id(null, _point10Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_10 key.
		///		FK_TwentyOnePointAudit_10 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point10Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint10Id(TransactionManager transactionManager, System.Int32? _point10Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_11 key.
		///		FK_TwentyOnePointAudit_11 Description: 
		/// </summary>
		/// <param name="_point11Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint11Id(System.Int32? _point11Id)
		{
			int count = -1;
			return GetByPoint11Id(_point11Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_11 key.
		///		FK_TwentyOnePointAudit_11 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point11Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint11Id(TransactionManager transactionManager, System.Int32? _point11Id)
		{
			int count = -1;
			return GetByPoint11Id(transactionManager, _point11Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_11 key.
		///		FK_TwentyOnePointAudit_11 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point11Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint11Id(TransactionManager transactionManager, System.Int32? _point11Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint11Id(transactionManager, _point11Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_11 key.
		///		fkTwentyOnePointAudit11 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point11Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint11Id(System.Int32? _point11Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint11Id(null, _point11Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_11 key.
		///		fkTwentyOnePointAudit11 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point11Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint11Id(System.Int32? _point11Id, int start, int pageLength,out int count)
		{
			return GetByPoint11Id(null, _point11Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_11 key.
		///		FK_TwentyOnePointAudit_11 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point11Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint11Id(TransactionManager transactionManager, System.Int32? _point11Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_12 key.
		///		FK_TwentyOnePointAudit_12 Description: 
		/// </summary>
		/// <param name="_point12Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint12Id(System.Int32? _point12Id)
		{
			int count = -1;
			return GetByPoint12Id(_point12Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_12 key.
		///		FK_TwentyOnePointAudit_12 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point12Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint12Id(TransactionManager transactionManager, System.Int32? _point12Id)
		{
			int count = -1;
			return GetByPoint12Id(transactionManager, _point12Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_12 key.
		///		FK_TwentyOnePointAudit_12 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point12Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint12Id(TransactionManager transactionManager, System.Int32? _point12Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint12Id(transactionManager, _point12Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_12 key.
		///		fkTwentyOnePointAudit12 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point12Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint12Id(System.Int32? _point12Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint12Id(null, _point12Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_12 key.
		///		fkTwentyOnePointAudit12 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point12Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint12Id(System.Int32? _point12Id, int start, int pageLength,out int count)
		{
			return GetByPoint12Id(null, _point12Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_12 key.
		///		FK_TwentyOnePointAudit_12 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point12Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint12Id(TransactionManager transactionManager, System.Int32? _point12Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_13 key.
		///		FK_TwentyOnePointAudit_13 Description: 
		/// </summary>
		/// <param name="_point13Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint13Id(System.Int32? _point13Id)
		{
			int count = -1;
			return GetByPoint13Id(_point13Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_13 key.
		///		FK_TwentyOnePointAudit_13 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point13Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint13Id(TransactionManager transactionManager, System.Int32? _point13Id)
		{
			int count = -1;
			return GetByPoint13Id(transactionManager, _point13Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_13 key.
		///		FK_TwentyOnePointAudit_13 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point13Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint13Id(TransactionManager transactionManager, System.Int32? _point13Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint13Id(transactionManager, _point13Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_13 key.
		///		fkTwentyOnePointAudit13 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point13Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint13Id(System.Int32? _point13Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint13Id(null, _point13Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_13 key.
		///		fkTwentyOnePointAudit13 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point13Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint13Id(System.Int32? _point13Id, int start, int pageLength,out int count)
		{
			return GetByPoint13Id(null, _point13Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_13 key.
		///		FK_TwentyOnePointAudit_13 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point13Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint13Id(TransactionManager transactionManager, System.Int32? _point13Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_14 key.
		///		FK_TwentyOnePointAudit_14 Description: 
		/// </summary>
		/// <param name="_point14Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint14Id(System.Int32? _point14Id)
		{
			int count = -1;
			return GetByPoint14Id(_point14Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_14 key.
		///		FK_TwentyOnePointAudit_14 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point14Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint14Id(TransactionManager transactionManager, System.Int32? _point14Id)
		{
			int count = -1;
			return GetByPoint14Id(transactionManager, _point14Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_14 key.
		///		FK_TwentyOnePointAudit_14 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point14Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint14Id(TransactionManager transactionManager, System.Int32? _point14Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint14Id(transactionManager, _point14Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_14 key.
		///		fkTwentyOnePointAudit14 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point14Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint14Id(System.Int32? _point14Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint14Id(null, _point14Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_14 key.
		///		fkTwentyOnePointAudit14 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point14Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint14Id(System.Int32? _point14Id, int start, int pageLength,out int count)
		{
			return GetByPoint14Id(null, _point14Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_14 key.
		///		FK_TwentyOnePointAudit_14 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point14Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint14Id(TransactionManager transactionManager, System.Int32? _point14Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_15 key.
		///		FK_TwentyOnePointAudit_15 Description: 
		/// </summary>
		/// <param name="_point15Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint15Id(System.Int32? _point15Id)
		{
			int count = -1;
			return GetByPoint15Id(_point15Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_15 key.
		///		FK_TwentyOnePointAudit_15 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point15Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint15Id(TransactionManager transactionManager, System.Int32? _point15Id)
		{
			int count = -1;
			return GetByPoint15Id(transactionManager, _point15Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_15 key.
		///		FK_TwentyOnePointAudit_15 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point15Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint15Id(TransactionManager transactionManager, System.Int32? _point15Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint15Id(transactionManager, _point15Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_15 key.
		///		fkTwentyOnePointAudit15 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point15Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint15Id(System.Int32? _point15Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint15Id(null, _point15Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_15 key.
		///		fkTwentyOnePointAudit15 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point15Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint15Id(System.Int32? _point15Id, int start, int pageLength,out int count)
		{
			return GetByPoint15Id(null, _point15Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_15 key.
		///		FK_TwentyOnePointAudit_15 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point15Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint15Id(TransactionManager transactionManager, System.Int32? _point15Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_16 key.
		///		FK_TwentyOnePointAudit_16 Description: 
		/// </summary>
		/// <param name="_point16Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint16Id(System.Int32? _point16Id)
		{
			int count = -1;
			return GetByPoint16Id(_point16Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_16 key.
		///		FK_TwentyOnePointAudit_16 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point16Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint16Id(TransactionManager transactionManager, System.Int32? _point16Id)
		{
			int count = -1;
			return GetByPoint16Id(transactionManager, _point16Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_16 key.
		///		FK_TwentyOnePointAudit_16 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point16Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint16Id(TransactionManager transactionManager, System.Int32? _point16Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint16Id(transactionManager, _point16Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_16 key.
		///		fkTwentyOnePointAudit16 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point16Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint16Id(System.Int32? _point16Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint16Id(null, _point16Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_16 key.
		///		fkTwentyOnePointAudit16 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point16Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint16Id(System.Int32? _point16Id, int start, int pageLength,out int count)
		{
			return GetByPoint16Id(null, _point16Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_16 key.
		///		FK_TwentyOnePointAudit_16 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point16Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint16Id(TransactionManager transactionManager, System.Int32? _point16Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_17 key.
		///		FK_TwentyOnePointAudit_17 Description: 
		/// </summary>
		/// <param name="_point17Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint17Id(System.Int32? _point17Id)
		{
			int count = -1;
			return GetByPoint17Id(_point17Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_17 key.
		///		FK_TwentyOnePointAudit_17 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point17Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint17Id(TransactionManager transactionManager, System.Int32? _point17Id)
		{
			int count = -1;
			return GetByPoint17Id(transactionManager, _point17Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_17 key.
		///		FK_TwentyOnePointAudit_17 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point17Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint17Id(TransactionManager transactionManager, System.Int32? _point17Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint17Id(transactionManager, _point17Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_17 key.
		///		fkTwentyOnePointAudit17 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point17Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint17Id(System.Int32? _point17Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint17Id(null, _point17Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_17 key.
		///		fkTwentyOnePointAudit17 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point17Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint17Id(System.Int32? _point17Id, int start, int pageLength,out int count)
		{
			return GetByPoint17Id(null, _point17Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_17 key.
		///		FK_TwentyOnePointAudit_17 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point17Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint17Id(TransactionManager transactionManager, System.Int32? _point17Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_18 key.
		///		FK_TwentyOnePointAudit_18 Description: 
		/// </summary>
		/// <param name="_point18Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint18Id(System.Int32? _point18Id)
		{
			int count = -1;
			return GetByPoint18Id(_point18Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_18 key.
		///		FK_TwentyOnePointAudit_18 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point18Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint18Id(TransactionManager transactionManager, System.Int32? _point18Id)
		{
			int count = -1;
			return GetByPoint18Id(transactionManager, _point18Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_18 key.
		///		FK_TwentyOnePointAudit_18 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point18Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint18Id(TransactionManager transactionManager, System.Int32? _point18Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint18Id(transactionManager, _point18Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_18 key.
		///		fkTwentyOnePointAudit18 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point18Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint18Id(System.Int32? _point18Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint18Id(null, _point18Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_18 key.
		///		fkTwentyOnePointAudit18 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point18Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint18Id(System.Int32? _point18Id, int start, int pageLength,out int count)
		{
			return GetByPoint18Id(null, _point18Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_18 key.
		///		FK_TwentyOnePointAudit_18 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point18Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint18Id(TransactionManager transactionManager, System.Int32? _point18Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_19 key.
		///		FK_TwentyOnePointAudit_19 Description: 
		/// </summary>
		/// <param name="_point19Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint19Id(System.Int32? _point19Id)
		{
			int count = -1;
			return GetByPoint19Id(_point19Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_19 key.
		///		FK_TwentyOnePointAudit_19 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point19Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint19Id(TransactionManager transactionManager, System.Int32? _point19Id)
		{
			int count = -1;
			return GetByPoint19Id(transactionManager, _point19Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_19 key.
		///		FK_TwentyOnePointAudit_19 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point19Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint19Id(TransactionManager transactionManager, System.Int32? _point19Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint19Id(transactionManager, _point19Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_19 key.
		///		fkTwentyOnePointAudit19 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point19Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint19Id(System.Int32? _point19Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint19Id(null, _point19Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_19 key.
		///		fkTwentyOnePointAudit19 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point19Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint19Id(System.Int32? _point19Id, int start, int pageLength,out int count)
		{
			return GetByPoint19Id(null, _point19Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_19 key.
		///		FK_TwentyOnePointAudit_19 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point19Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint19Id(TransactionManager transactionManager, System.Int32? _point19Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_20 key.
		///		FK_TwentyOnePointAudit_20 Description: 
		/// </summary>
		/// <param name="_point20Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint20Id(System.Int32? _point20Id)
		{
			int count = -1;
			return GetByPoint20Id(_point20Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_20 key.
		///		FK_TwentyOnePointAudit_20 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point20Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint20Id(TransactionManager transactionManager, System.Int32? _point20Id)
		{
			int count = -1;
			return GetByPoint20Id(transactionManager, _point20Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_20 key.
		///		FK_TwentyOnePointAudit_20 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point20Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint20Id(TransactionManager transactionManager, System.Int32? _point20Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint20Id(transactionManager, _point20Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_20 key.
		///		fkTwentyOnePointAudit20 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point20Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint20Id(System.Int32? _point20Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint20Id(null, _point20Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_20 key.
		///		fkTwentyOnePointAudit20 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point20Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint20Id(System.Int32? _point20Id, int start, int pageLength,out int count)
		{
			return GetByPoint20Id(null, _point20Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_20 key.
		///		FK_TwentyOnePointAudit_20 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point20Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint20Id(TransactionManager transactionManager, System.Int32? _point20Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_21 key.
		///		FK_TwentyOnePointAudit_21 Description: 
		/// </summary>
		/// <param name="_point21Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint21Id(System.Int32? _point21Id)
		{
			int count = -1;
			return GetByPoint21Id(_point21Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_21 key.
		///		FK_TwentyOnePointAudit_21 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point21Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByPoint21Id(TransactionManager transactionManager, System.Int32? _point21Id)
		{
			int count = -1;
			return GetByPoint21Id(transactionManager, _point21Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_21 key.
		///		FK_TwentyOnePointAudit_21 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point21Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint21Id(TransactionManager transactionManager, System.Int32? _point21Id, int start, int pageLength)
		{
			int count = -1;
			return GetByPoint21Id(transactionManager, _point21Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_21 key.
		///		fkTwentyOnePointAudit21 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point21Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint21Id(System.Int32? _point21Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByPoint21Id(null, _point21Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_21 key.
		///		fkTwentyOnePointAudit21 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_point21Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByPoint21Id(System.Int32? _point21Id, int start, int pageLength,out int count)
		{
			return GetByPoint21Id(null, _point21Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_21 key.
		///		FK_TwentyOnePointAudit_21 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_point21Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByPoint21Id(TransactionManager transactionManager, System.Int32? _point21Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_CreatedBy key.
		///		FK_TwentyOnePointAudit_Users_CreatedBy Description: 
		/// </summary>
		/// <param name="_createdbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCreatedbyUserId(System.Int32 _createdbyUserId)
		{
			int count = -1;
			return GetByCreatedbyUserId(_createdbyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_CreatedBy key.
		///		FK_TwentyOnePointAudit_Users_CreatedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByCreatedbyUserId(TransactionManager transactionManager, System.Int32 _createdbyUserId)
		{
			int count = -1;
			return GetByCreatedbyUserId(transactionManager, _createdbyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_CreatedBy key.
		///		FK_TwentyOnePointAudit_Users_CreatedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCreatedbyUserId(TransactionManager transactionManager, System.Int32 _createdbyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedbyUserId(transactionManager, _createdbyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_CreatedBy key.
		///		fkTwentyOnePointAuditUsersCreatedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdbyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCreatedbyUserId(System.Int32 _createdbyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedbyUserId(null, _createdbyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_CreatedBy key.
		///		fkTwentyOnePointAuditUsersCreatedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdbyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCreatedbyUserId(System.Int32 _createdbyUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedbyUserId(null, _createdbyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_CreatedBy key.
		///		FK_TwentyOnePointAudit_Users_CreatedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByCreatedbyUserId(TransactionManager transactionManager, System.Int32 _createdbyUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Companies key.
		///		FK_TwentyOnePointAudit_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Companies key.
		///		FK_TwentyOnePointAudit_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Companies key.
		///		FK_TwentyOnePointAudit_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Companies key.
		///		fkTwentyOnePointAuditCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Companies key.
		///		fkTwentyOnePointAuditCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Companies key.
		///		FK_TwentyOnePointAudit_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_ModifiedBy key.
		///		FK_TwentyOnePointAudit_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(_modifiedbyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_ModifiedBy key.
		///		FK_TwentyOnePointAudit_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		/// <remarks></remarks>
		public TList<TwentyOnePointAudit> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_ModifiedBy key.
		///		FK_TwentyOnePointAudit_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_ModifiedBy key.
		///		fkTwentyOnePointAuditUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_ModifiedBy key.
		///		fkTwentyOnePointAuditUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public TList<TwentyOnePointAudit> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_TwentyOnePointAudit_Users_ModifiedBy key.
		///		FK_TwentyOnePointAudit_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.TwentyOnePointAudit objects.</returns>
		public abstract TList<TwentyOnePointAudit> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAuditKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetByCompanyIdSiteIdQtrIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(null,_companyId, _siteId, _qtrId, _year, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetByCompanyIdSiteIdQtrIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(null, _companyId, _siteId, _qtrId, _year, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetByCompanyIdSiteIdQtrIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(transactionManager, _companyId, _siteId, _qtrId, _year, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetByCompanyIdSiteIdQtrIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(transactionManager, _companyId, _siteId, _qtrId, _year, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit GetByCompanyIdSiteIdQtrIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteIdQtrIdYear(null, _companyId, _siteId, _qtrId, _year, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_TwentyOnePointAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit GetByCompanyIdSiteIdQtrIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _TwentyOnePointAudit_ComplianceReport 
		
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport(null, 0, int.MaxValue , currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(int start, int pageLength, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport(null, start, pageLength , currentyear, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(TransactionManager transactionManager, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport(transactionManager, 0, int.MaxValue , currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport(TransactionManager transactionManager, int start, int pageLength , System.Int32? currentyear, System.Int32? siteId);
		
		#endregion
		
		#region _TwentyOnePointAudit_ComplianceReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport_ByCompany(null, 0, int.MaxValue , companyId, currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(int start, int pageLength, System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport_ByCompany(null, start, pageLength , companyId, currentyear, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport_ByCompany(transactionManager, 0, int.MaxValue , companyId, currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_TwentyOnePointAudit_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit&gt;"/></returns>
		public static TList<TwentyOnePointAudit> Fill(IDataReader reader, TList<TwentyOnePointAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAuditColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAuditColumn.Id - 1)];
					c.CompanyId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.CompanyId - 1)];
					c.SiteId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.SiteId - 1)];
					c.CreatedbyUserId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.CreatedbyUserId - 1)];
					c.ModifiedbyUserId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.ModifiedbyUserId - 1)];
					c.DateAdded = (System.DateTime)reader[((int)TwentyOnePointAuditColumn.DateAdded - 1)];
					c.DateModified = (System.DateTime)reader[((int)TwentyOnePointAuditColumn.DateModified - 1)];
					c.QtrId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.QtrId - 1)];
					c.Year = (System.Int64)reader[((int)TwentyOnePointAuditColumn.Year - 1)];
					c.TotalPossibleScore = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.TotalPossibleScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.TotalPossibleScore - 1)];
					c.TotalActualScore = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.TotalActualScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.TotalActualScore - 1)];
					c.TotalRating = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.TotalRating - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.TotalRating - 1)];
					c.Point01Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point01Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point01Id - 1)];
					c.Point02Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point02Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point02Id - 1)];
					c.Point03Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point03Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point03Id - 1)];
					c.Point04Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point04Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point04Id - 1)];
					c.Point05Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point05Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point05Id - 1)];
					c.Point06Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point06Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point06Id - 1)];
					c.Point07Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point07Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point07Id - 1)];
					c.Point08Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point08Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point08Id - 1)];
					c.Point09Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point09Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point09Id - 1)];
					c.Point10Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point10Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point10Id - 1)];
					c.Point11Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point11Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point11Id - 1)];
					c.Point12Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point12Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point12Id - 1)];
					c.Point13Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point13Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point13Id - 1)];
					c.Point14Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point14Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point14Id - 1)];
					c.Point15Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point15Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point15Id - 1)];
					c.Point16Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point16Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point16Id - 1)];
					c.Point17Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point17Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point17Id - 1)];
					c.Point18Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point18Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point18Id - 1)];
					c.Point19Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point19Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point19Id - 1)];
					c.Point20Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point20Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point20Id - 1)];
					c.Point21Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point21Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point21Id - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAuditColumn.Id - 1)];
			entity.CompanyId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.CompanyId - 1)];
			entity.SiteId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.SiteId - 1)];
			entity.CreatedbyUserId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.CreatedbyUserId - 1)];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.ModifiedbyUserId - 1)];
			entity.DateAdded = (System.DateTime)reader[((int)TwentyOnePointAuditColumn.DateAdded - 1)];
			entity.DateModified = (System.DateTime)reader[((int)TwentyOnePointAuditColumn.DateModified - 1)];
			entity.QtrId = (System.Int32)reader[((int)TwentyOnePointAuditColumn.QtrId - 1)];
			entity.Year = (System.Int64)reader[((int)TwentyOnePointAuditColumn.Year - 1)];
			entity.TotalPossibleScore = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.TotalPossibleScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.TotalPossibleScore - 1)];
			entity.TotalActualScore = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.TotalActualScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.TotalActualScore - 1)];
			entity.TotalRating = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.TotalRating - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.TotalRating - 1)];
			entity.Point01Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point01Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point01Id - 1)];
			entity.Point02Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point02Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point02Id - 1)];
			entity.Point03Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point03Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point03Id - 1)];
			entity.Point04Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point04Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point04Id - 1)];
			entity.Point05Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point05Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point05Id - 1)];
			entity.Point06Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point06Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point06Id - 1)];
			entity.Point07Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point07Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point07Id - 1)];
			entity.Point08Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point08Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point08Id - 1)];
			entity.Point09Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point09Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point09Id - 1)];
			entity.Point10Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point10Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point10Id - 1)];
			entity.Point11Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point11Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point11Id - 1)];
			entity.Point12Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point12Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point12Id - 1)];
			entity.Point13Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point13Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point13Id - 1)];
			entity.Point14Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point14Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point14Id - 1)];
			entity.Point15Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point15Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point15Id - 1)];
			entity.Point16Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point16Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point16Id - 1)];
			entity.Point17Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point17Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point17Id - 1)];
			entity.Point18Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point18Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point18Id - 1)];
			entity.Point19Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point19Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point19Id - 1)];
			entity.Point20Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point20Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point20Id - 1)];
			entity.Point21Id = (reader.IsDBNull(((int)TwentyOnePointAuditColumn.Point21Id - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAuditColumn.Point21Id - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CreatedbyUserId = (System.Int32)dataRow["CreatedbyUserId"];
			entity.ModifiedbyUserId = (System.Int32)dataRow["ModifiedbyUserId"];
			entity.DateAdded = (System.DateTime)dataRow["DateAdded"];
			entity.DateModified = (System.DateTime)dataRow["DateModified"];
			entity.QtrId = (System.Int32)dataRow["QtrId"];
			entity.Year = (System.Int64)dataRow["Year"];
			entity.TotalPossibleScore = Convert.IsDBNull(dataRow["TotalPossibleScore"]) ? null : (System.Int32?)dataRow["TotalPossibleScore"];
			entity.TotalActualScore = Convert.IsDBNull(dataRow["TotalActualScore"]) ? null : (System.Int32?)dataRow["TotalActualScore"];
			entity.TotalRating = Convert.IsDBNull(dataRow["TotalRating"]) ? null : (System.Int32?)dataRow["TotalRating"];
			entity.Point01Id = Convert.IsDBNull(dataRow["Point_01_Id"]) ? null : (System.Int32?)dataRow["Point_01_Id"];
			entity.Point02Id = Convert.IsDBNull(dataRow["Point_02_Id"]) ? null : (System.Int32?)dataRow["Point_02_Id"];
			entity.Point03Id = Convert.IsDBNull(dataRow["Point_03_Id"]) ? null : (System.Int32?)dataRow["Point_03_Id"];
			entity.Point04Id = Convert.IsDBNull(dataRow["Point_04_Id"]) ? null : (System.Int32?)dataRow["Point_04_Id"];
			entity.Point05Id = Convert.IsDBNull(dataRow["Point_05_Id"]) ? null : (System.Int32?)dataRow["Point_05_Id"];
			entity.Point06Id = Convert.IsDBNull(dataRow["Point_06_Id"]) ? null : (System.Int32?)dataRow["Point_06_Id"];
			entity.Point07Id = Convert.IsDBNull(dataRow["Point_07_Id"]) ? null : (System.Int32?)dataRow["Point_07_Id"];
			entity.Point08Id = Convert.IsDBNull(dataRow["Point_08_Id"]) ? null : (System.Int32?)dataRow["Point_08_Id"];
			entity.Point09Id = Convert.IsDBNull(dataRow["Point_09_Id"]) ? null : (System.Int32?)dataRow["Point_09_Id"];
			entity.Point10Id = Convert.IsDBNull(dataRow["Point_10_Id"]) ? null : (System.Int32?)dataRow["Point_10_Id"];
			entity.Point11Id = Convert.IsDBNull(dataRow["Point_11_Id"]) ? null : (System.Int32?)dataRow["Point_11_Id"];
			entity.Point12Id = Convert.IsDBNull(dataRow["Point_12_Id"]) ? null : (System.Int32?)dataRow["Point_12_Id"];
			entity.Point13Id = Convert.IsDBNull(dataRow["Point_13_Id"]) ? null : (System.Int32?)dataRow["Point_13_Id"];
			entity.Point14Id = Convert.IsDBNull(dataRow["Point_14_Id"]) ? null : (System.Int32?)dataRow["Point_14_Id"];
			entity.Point15Id = Convert.IsDBNull(dataRow["Point_15_Id"]) ? null : (System.Int32?)dataRow["Point_15_Id"];
			entity.Point16Id = Convert.IsDBNull(dataRow["Point_16_Id"]) ? null : (System.Int32?)dataRow["Point_16_Id"];
			entity.Point17Id = Convert.IsDBNull(dataRow["Point_17_Id"]) ? null : (System.Int32?)dataRow["Point_17_Id"];
			entity.Point18Id = Convert.IsDBNull(dataRow["Point_18_Id"]) ? null : (System.Int32?)dataRow["Point_18_Id"];
			entity.Point19Id = Convert.IsDBNull(dataRow["Point_19_Id"]) ? null : (System.Int32?)dataRow["Point_19_Id"];
			entity.Point20Id = Convert.IsDBNull(dataRow["Point_20_Id"]) ? null : (System.Int32?)dataRow["Point_20_Id"];
			entity.Point21Id = Convert.IsDBNull(dataRow["Point_21_Id"]) ? null : (System.Int32?)dataRow["Point_21_Id"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource

			#region Point01IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit01|Point01IdSource", deepLoadType, innerList) 
				&& entity.Point01IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point01Id ?? (int)0);
				TwentyOnePointAudit01 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit01>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit01), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point01IdSource = tmpEntity;
				else
					entity.Point01IdSource = DataRepository.TwentyOnePointAudit01Provider.GetById(transactionManager, (entity.Point01Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point01IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point01IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit01Provider.DeepLoad(transactionManager, entity.Point01IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point01IdSource

			#region Point02IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit02|Point02IdSource", deepLoadType, innerList) 
				&& entity.Point02IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point02Id ?? (int)0);
				TwentyOnePointAudit02 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit02>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit02), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point02IdSource = tmpEntity;
				else
					entity.Point02IdSource = DataRepository.TwentyOnePointAudit02Provider.GetById(transactionManager, (entity.Point02Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point02IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point02IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit02Provider.DeepLoad(transactionManager, entity.Point02IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point02IdSource

			#region Point03IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit03|Point03IdSource", deepLoadType, innerList) 
				&& entity.Point03IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point03Id ?? (int)0);
				TwentyOnePointAudit03 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit03>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit03), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point03IdSource = tmpEntity;
				else
					entity.Point03IdSource = DataRepository.TwentyOnePointAudit03Provider.GetById(transactionManager, (entity.Point03Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point03IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point03IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit03Provider.DeepLoad(transactionManager, entity.Point03IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point03IdSource

			#region Point04IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit04|Point04IdSource", deepLoadType, innerList) 
				&& entity.Point04IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point04Id ?? (int)0);
				TwentyOnePointAudit04 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit04>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit04), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point04IdSource = tmpEntity;
				else
					entity.Point04IdSource = DataRepository.TwentyOnePointAudit04Provider.GetById(transactionManager, (entity.Point04Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point04IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point04IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit04Provider.DeepLoad(transactionManager, entity.Point04IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point04IdSource

			#region Point05IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit05|Point05IdSource", deepLoadType, innerList) 
				&& entity.Point05IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point05Id ?? (int)0);
				TwentyOnePointAudit05 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit05>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit05), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point05IdSource = tmpEntity;
				else
					entity.Point05IdSource = DataRepository.TwentyOnePointAudit05Provider.GetById(transactionManager, (entity.Point05Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point05IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point05IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit05Provider.DeepLoad(transactionManager, entity.Point05IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point05IdSource

			#region Point06IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit06|Point06IdSource", deepLoadType, innerList) 
				&& entity.Point06IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point06Id ?? (int)0);
				TwentyOnePointAudit06 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit06>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit06), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point06IdSource = tmpEntity;
				else
					entity.Point06IdSource = DataRepository.TwentyOnePointAudit06Provider.GetById(transactionManager, (entity.Point06Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point06IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point06IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit06Provider.DeepLoad(transactionManager, entity.Point06IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point06IdSource

			#region Point07IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit07|Point07IdSource", deepLoadType, innerList) 
				&& entity.Point07IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point07Id ?? (int)0);
				TwentyOnePointAudit07 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit07>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit07), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point07IdSource = tmpEntity;
				else
					entity.Point07IdSource = DataRepository.TwentyOnePointAudit07Provider.GetById(transactionManager, (entity.Point07Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point07IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point07IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit07Provider.DeepLoad(transactionManager, entity.Point07IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point07IdSource

			#region Point08IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit08|Point08IdSource", deepLoadType, innerList) 
				&& entity.Point08IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point08Id ?? (int)0);
				TwentyOnePointAudit08 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit08>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit08), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point08IdSource = tmpEntity;
				else
					entity.Point08IdSource = DataRepository.TwentyOnePointAudit08Provider.GetById(transactionManager, (entity.Point08Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point08IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point08IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit08Provider.DeepLoad(transactionManager, entity.Point08IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point08IdSource

			#region Point09IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit09|Point09IdSource", deepLoadType, innerList) 
				&& entity.Point09IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point09Id ?? (int)0);
				TwentyOnePointAudit09 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit09>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit09), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point09IdSource = tmpEntity;
				else
					entity.Point09IdSource = DataRepository.TwentyOnePointAudit09Provider.GetById(transactionManager, (entity.Point09Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point09IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point09IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit09Provider.DeepLoad(transactionManager, entity.Point09IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point09IdSource

			#region Point10IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit10|Point10IdSource", deepLoadType, innerList) 
				&& entity.Point10IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point10Id ?? (int)0);
				TwentyOnePointAudit10 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit10>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit10), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point10IdSource = tmpEntity;
				else
					entity.Point10IdSource = DataRepository.TwentyOnePointAudit10Provider.GetById(transactionManager, (entity.Point10Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point10IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point10IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit10Provider.DeepLoad(transactionManager, entity.Point10IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point10IdSource

			#region Point11IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit11|Point11IdSource", deepLoadType, innerList) 
				&& entity.Point11IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point11Id ?? (int)0);
				TwentyOnePointAudit11 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit11>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit11), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point11IdSource = tmpEntity;
				else
					entity.Point11IdSource = DataRepository.TwentyOnePointAudit11Provider.GetById(transactionManager, (entity.Point11Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point11IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point11IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit11Provider.DeepLoad(transactionManager, entity.Point11IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point11IdSource

			#region Point12IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit12|Point12IdSource", deepLoadType, innerList) 
				&& entity.Point12IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point12Id ?? (int)0);
				TwentyOnePointAudit12 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit12>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit12), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point12IdSource = tmpEntity;
				else
					entity.Point12IdSource = DataRepository.TwentyOnePointAudit12Provider.GetById(transactionManager, (entity.Point12Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point12IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point12IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit12Provider.DeepLoad(transactionManager, entity.Point12IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point12IdSource

			#region Point13IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit13|Point13IdSource", deepLoadType, innerList) 
				&& entity.Point13IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point13Id ?? (int)0);
				TwentyOnePointAudit13 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit13>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit13), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point13IdSource = tmpEntity;
				else
					entity.Point13IdSource = DataRepository.TwentyOnePointAudit13Provider.GetById(transactionManager, (entity.Point13Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point13IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point13IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit13Provider.DeepLoad(transactionManager, entity.Point13IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point13IdSource

			#region Point14IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit14|Point14IdSource", deepLoadType, innerList) 
				&& entity.Point14IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point14Id ?? (int)0);
				TwentyOnePointAudit14 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit14>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit14), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point14IdSource = tmpEntity;
				else
					entity.Point14IdSource = DataRepository.TwentyOnePointAudit14Provider.GetById(transactionManager, (entity.Point14Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point14IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point14IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit14Provider.DeepLoad(transactionManager, entity.Point14IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point14IdSource

			#region Point15IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit15|Point15IdSource", deepLoadType, innerList) 
				&& entity.Point15IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point15Id ?? (int)0);
				TwentyOnePointAudit15 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit15>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit15), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point15IdSource = tmpEntity;
				else
					entity.Point15IdSource = DataRepository.TwentyOnePointAudit15Provider.GetById(transactionManager, (entity.Point15Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point15IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point15IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit15Provider.DeepLoad(transactionManager, entity.Point15IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point15IdSource

			#region Point16IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit16|Point16IdSource", deepLoadType, innerList) 
				&& entity.Point16IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point16Id ?? (int)0);
				TwentyOnePointAudit16 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit16>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit16), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point16IdSource = tmpEntity;
				else
					entity.Point16IdSource = DataRepository.TwentyOnePointAudit16Provider.GetById(transactionManager, (entity.Point16Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point16IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point16IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit16Provider.DeepLoad(transactionManager, entity.Point16IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point16IdSource

			#region Point17IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit17|Point17IdSource", deepLoadType, innerList) 
				&& entity.Point17IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point17Id ?? (int)0);
				TwentyOnePointAudit17 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit17>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit17), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point17IdSource = tmpEntity;
				else
					entity.Point17IdSource = DataRepository.TwentyOnePointAudit17Provider.GetById(transactionManager, (entity.Point17Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point17IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point17IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit17Provider.DeepLoad(transactionManager, entity.Point17IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point17IdSource

			#region Point18IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit18|Point18IdSource", deepLoadType, innerList) 
				&& entity.Point18IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point18Id ?? (int)0);
				TwentyOnePointAudit18 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit18>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit18), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point18IdSource = tmpEntity;
				else
					entity.Point18IdSource = DataRepository.TwentyOnePointAudit18Provider.GetById(transactionManager, (entity.Point18Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point18IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point18IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit18Provider.DeepLoad(transactionManager, entity.Point18IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point18IdSource

			#region Point19IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit19|Point19IdSource", deepLoadType, innerList) 
				&& entity.Point19IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point19Id ?? (int)0);
				TwentyOnePointAudit19 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit19>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit19), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point19IdSource = tmpEntity;
				else
					entity.Point19IdSource = DataRepository.TwentyOnePointAudit19Provider.GetById(transactionManager, (entity.Point19Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point19IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point19IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit19Provider.DeepLoad(transactionManager, entity.Point19IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point19IdSource

			#region Point20IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit20|Point20IdSource", deepLoadType, innerList) 
				&& entity.Point20IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point20Id ?? (int)0);
				TwentyOnePointAudit20 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit20>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit20), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point20IdSource = tmpEntity;
				else
					entity.Point20IdSource = DataRepository.TwentyOnePointAudit20Provider.GetById(transactionManager, (entity.Point20Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point20IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point20IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit20Provider.DeepLoad(transactionManager, entity.Point20IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point20IdSource

			#region Point21IdSource	
			if (CanDeepLoad(entity, "TwentyOnePointAudit21|Point21IdSource", deepLoadType, innerList) 
				&& entity.Point21IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.Point21Id ?? (int)0);
				TwentyOnePointAudit21 tmpEntity = EntityManager.LocateEntity<TwentyOnePointAudit21>(EntityLocator.ConstructKeyFromPkItems(typeof(TwentyOnePointAudit21), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.Point21IdSource = tmpEntity;
				else
					entity.Point21IdSource = DataRepository.TwentyOnePointAudit21Provider.GetById(transactionManager, (entity.Point21Id ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Point21IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.Point21IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TwentyOnePointAudit21Provider.DeepLoad(transactionManager, entity.Point21IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion Point21IdSource

			#region CreatedbyUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedbyUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedbyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedbyUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedbyUserIdSource = tmpEntity;
				else
					entity.CreatedbyUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedbyUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedbyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedbyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedbyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedbyUserIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region ModifiedbyUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedbyUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedbyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedbyUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedbyUserIdSource = tmpEntity;
				else
					entity.ModifiedbyUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedbyUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedbyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedbyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedbyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedbyUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			
			#region Point01IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit01|Point01IdSource", deepSaveType, innerList) 
				&& entity.Point01IdSource != null)
			{
				DataRepository.TwentyOnePointAudit01Provider.Save(transactionManager, entity.Point01IdSource);
				entity.Point01Id = entity.Point01IdSource.Id;
			}
			#endregion 
			
			#region Point02IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit02|Point02IdSource", deepSaveType, innerList) 
				&& entity.Point02IdSource != null)
			{
				DataRepository.TwentyOnePointAudit02Provider.Save(transactionManager, entity.Point02IdSource);
				entity.Point02Id = entity.Point02IdSource.Id;
			}
			#endregion 
			
			#region Point03IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit03|Point03IdSource", deepSaveType, innerList) 
				&& entity.Point03IdSource != null)
			{
				DataRepository.TwentyOnePointAudit03Provider.Save(transactionManager, entity.Point03IdSource);
				entity.Point03Id = entity.Point03IdSource.Id;
			}
			#endregion 
			
			#region Point04IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit04|Point04IdSource", deepSaveType, innerList) 
				&& entity.Point04IdSource != null)
			{
				DataRepository.TwentyOnePointAudit04Provider.Save(transactionManager, entity.Point04IdSource);
				entity.Point04Id = entity.Point04IdSource.Id;
			}
			#endregion 
			
			#region Point05IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit05|Point05IdSource", deepSaveType, innerList) 
				&& entity.Point05IdSource != null)
			{
				DataRepository.TwentyOnePointAudit05Provider.Save(transactionManager, entity.Point05IdSource);
				entity.Point05Id = entity.Point05IdSource.Id;
			}
			#endregion 
			
			#region Point06IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit06|Point06IdSource", deepSaveType, innerList) 
				&& entity.Point06IdSource != null)
			{
				DataRepository.TwentyOnePointAudit06Provider.Save(transactionManager, entity.Point06IdSource);
				entity.Point06Id = entity.Point06IdSource.Id;
			}
			#endregion 
			
			#region Point07IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit07|Point07IdSource", deepSaveType, innerList) 
				&& entity.Point07IdSource != null)
			{
				DataRepository.TwentyOnePointAudit07Provider.Save(transactionManager, entity.Point07IdSource);
				entity.Point07Id = entity.Point07IdSource.Id;
			}
			#endregion 
			
			#region Point08IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit08|Point08IdSource", deepSaveType, innerList) 
				&& entity.Point08IdSource != null)
			{
				DataRepository.TwentyOnePointAudit08Provider.Save(transactionManager, entity.Point08IdSource);
				entity.Point08Id = entity.Point08IdSource.Id;
			}
			#endregion 
			
			#region Point09IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit09|Point09IdSource", deepSaveType, innerList) 
				&& entity.Point09IdSource != null)
			{
				DataRepository.TwentyOnePointAudit09Provider.Save(transactionManager, entity.Point09IdSource);
				entity.Point09Id = entity.Point09IdSource.Id;
			}
			#endregion 
			
			#region Point10IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit10|Point10IdSource", deepSaveType, innerList) 
				&& entity.Point10IdSource != null)
			{
				DataRepository.TwentyOnePointAudit10Provider.Save(transactionManager, entity.Point10IdSource);
				entity.Point10Id = entity.Point10IdSource.Id;
			}
			#endregion 
			
			#region Point11IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit11|Point11IdSource", deepSaveType, innerList) 
				&& entity.Point11IdSource != null)
			{
				DataRepository.TwentyOnePointAudit11Provider.Save(transactionManager, entity.Point11IdSource);
				entity.Point11Id = entity.Point11IdSource.Id;
			}
			#endregion 
			
			#region Point12IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit12|Point12IdSource", deepSaveType, innerList) 
				&& entity.Point12IdSource != null)
			{
				DataRepository.TwentyOnePointAudit12Provider.Save(transactionManager, entity.Point12IdSource);
				entity.Point12Id = entity.Point12IdSource.Id;
			}
			#endregion 
			
			#region Point13IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit13|Point13IdSource", deepSaveType, innerList) 
				&& entity.Point13IdSource != null)
			{
				DataRepository.TwentyOnePointAudit13Provider.Save(transactionManager, entity.Point13IdSource);
				entity.Point13Id = entity.Point13IdSource.Id;
			}
			#endregion 
			
			#region Point14IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit14|Point14IdSource", deepSaveType, innerList) 
				&& entity.Point14IdSource != null)
			{
				DataRepository.TwentyOnePointAudit14Provider.Save(transactionManager, entity.Point14IdSource);
				entity.Point14Id = entity.Point14IdSource.Id;
			}
			#endregion 
			
			#region Point15IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit15|Point15IdSource", deepSaveType, innerList) 
				&& entity.Point15IdSource != null)
			{
				DataRepository.TwentyOnePointAudit15Provider.Save(transactionManager, entity.Point15IdSource);
				entity.Point15Id = entity.Point15IdSource.Id;
			}
			#endregion 
			
			#region Point16IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit16|Point16IdSource", deepSaveType, innerList) 
				&& entity.Point16IdSource != null)
			{
				DataRepository.TwentyOnePointAudit16Provider.Save(transactionManager, entity.Point16IdSource);
				entity.Point16Id = entity.Point16IdSource.Id;
			}
			#endregion 
			
			#region Point17IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit17|Point17IdSource", deepSaveType, innerList) 
				&& entity.Point17IdSource != null)
			{
				DataRepository.TwentyOnePointAudit17Provider.Save(transactionManager, entity.Point17IdSource);
				entity.Point17Id = entity.Point17IdSource.Id;
			}
			#endregion 
			
			#region Point18IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit18|Point18IdSource", deepSaveType, innerList) 
				&& entity.Point18IdSource != null)
			{
				DataRepository.TwentyOnePointAudit18Provider.Save(transactionManager, entity.Point18IdSource);
				entity.Point18Id = entity.Point18IdSource.Id;
			}
			#endregion 
			
			#region Point19IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit19|Point19IdSource", deepSaveType, innerList) 
				&& entity.Point19IdSource != null)
			{
				DataRepository.TwentyOnePointAudit19Provider.Save(transactionManager, entity.Point19IdSource);
				entity.Point19Id = entity.Point19IdSource.Id;
			}
			#endregion 
			
			#region Point20IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit20|Point20IdSource", deepSaveType, innerList) 
				&& entity.Point20IdSource != null)
			{
				DataRepository.TwentyOnePointAudit20Provider.Save(transactionManager, entity.Point20IdSource);
				entity.Point20Id = entity.Point20IdSource.Id;
			}
			#endregion 
			
			#region Point21IdSource
			if (CanDeepSave(entity, "TwentyOnePointAudit21|Point21IdSource", deepSaveType, innerList) 
				&& entity.Point21IdSource != null)
			{
				DataRepository.TwentyOnePointAudit21Provider.Save(transactionManager, entity.Point21IdSource);
				entity.Point21Id = entity.Point21IdSource.Id;
			}
			#endregion 
			
			#region CreatedbyUserIdSource
			if (CanDeepSave(entity, "Users|CreatedbyUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedbyUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedbyUserIdSource);
				entity.CreatedbyUserId = entity.CreatedbyUserIdSource.UserId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region ModifiedbyUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedbyUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedbyUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedbyUserIdSource);
				entity.ModifiedbyUserId = entity.ModifiedbyUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit</c>
	///</summary>
	public enum TwentyOnePointAuditChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit01</c> at Point01IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit01))]
		TwentyOnePointAudit01,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit02</c> at Point02IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit02))]
		TwentyOnePointAudit02,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit03</c> at Point03IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit03))]
		TwentyOnePointAudit03,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit04</c> at Point04IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit04))]
		TwentyOnePointAudit04,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit05</c> at Point05IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit05))]
		TwentyOnePointAudit05,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit06</c> at Point06IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit06))]
		TwentyOnePointAudit06,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit07</c> at Point07IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit07))]
		TwentyOnePointAudit07,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit08</c> at Point08IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit08))]
		TwentyOnePointAudit08,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit09</c> at Point09IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit09))]
		TwentyOnePointAudit09,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit10</c> at Point10IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit10))]
		TwentyOnePointAudit10,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit11</c> at Point11IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit11))]
		TwentyOnePointAudit11,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit12</c> at Point12IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit12))]
		TwentyOnePointAudit12,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit13</c> at Point13IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit13))]
		TwentyOnePointAudit13,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit14</c> at Point14IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit14))]
		TwentyOnePointAudit14,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit15</c> at Point15IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit15))]
		TwentyOnePointAudit15,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit16</c> at Point16IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit16))]
		TwentyOnePointAudit16,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit17</c> at Point17IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit17))]
		TwentyOnePointAudit17,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit18</c> at Point18IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit18))]
		TwentyOnePointAudit18,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit19</c> at Point19IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit19))]
		TwentyOnePointAudit19,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit20</c> at Point20IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit20))]
		TwentyOnePointAudit20,
			
		///<summary>
		/// Composite Property for <c>TwentyOnePointAudit21</c> at Point21IdSource
		///</summary>
		[ChildEntityType(typeof(TwentyOnePointAudit21))]
		TwentyOnePointAudit21,
			
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedbyUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
		}
	
	#endregion TwentyOnePointAuditChildEntityTypes
	
	#region TwentyOnePointAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditFilterBuilder : SqlFilterBuilder<TwentyOnePointAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditFilterBuilder class.
		/// </summary>
		public TwentyOnePointAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditFilterBuilder
	
	#region TwentyOnePointAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAuditParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditParameterBuilder class.
		/// </summary>
		public TwentyOnePointAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAuditParameterBuilder
	
	#region TwentyOnePointAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAuditSortBuilder : SqlSortBuilder<TwentyOnePointAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAuditSqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAuditSortBuilder
	
} // end namespace
