﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompaniesDataSourceDesigner))]
	public class CompaniesDataSource : ProviderDataSource<Companies, CompaniesKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesDataSource class.
		/// </summary>
		public CompaniesDataSource() : base(new CompaniesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesDataSourceView used by the CompaniesDataSource.
		/// </summary>
		protected CompaniesDataSourceView CompaniesView
		{
			get { return ( View as CompaniesDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompaniesDataSource control invokes to retrieve data.
		/// </summary>
		public CompaniesSelectMethod SelectMethod
		{
			get
			{
				CompaniesSelectMethod selectMethod = CompaniesSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompaniesSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesDataSourceView class that is to be
		/// used by the CompaniesDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesDataSourceView class.</returns>
		protected override BaseDataSourceView<Companies, CompaniesKey> GetNewDataSourceView()
		{
			return new CompaniesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesDataSourceView : ProviderDataSourceView<Companies, CompaniesKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesDataSourceView(CompaniesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesDataSource CompaniesOwner
		{
			get { return Owner as CompaniesDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompaniesSelectMethod SelectMethod
		{
			get { return CompaniesOwner.SelectMethod; }
			set { CompaniesOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesService CompaniesProvider
		{
			get { return Provider as CompaniesService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Companies> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Companies> results = null;
			Companies item;
			count = 0;
			
			System.Int32 _companyId;
			System.String _companyName;
			System.String _iprocSupplierNo_nullable;
			System.String _companyAbn;
			System.Int32? _ehsConsultantId_nullable;
			System.String _companyNameEbi_nullable;
			System.Int32 _companyStatusId;
			System.Int32 _companyStatus2Id;
			System.Int32 _modifiedbyUserId;
			System.Int32 _parentId;
			System.Int32 _childId;

			switch ( SelectMethod )
			{
				case CompaniesSelectMethod.Get:
					CompaniesKey entityKey  = new CompaniesKey();
					entityKey.Load(values);
					item = CompaniesProvider.Get(entityKey);
					results = new TList<Companies>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesSelectMethod.GetAll:
                    results = CompaniesProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompaniesSelectMethod.GetPaged:
					results = CompaniesProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompaniesSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompaniesProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompaniesProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompaniesSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					item = CompaniesProvider.GetByCompanyId(_companyId);
					results = new TList<Companies>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompaniesSelectMethod.GetByCompanyName:
					_companyName = ( values["CompanyName"] != null ) ? (System.String) EntityUtil.ChangeType(values["CompanyName"], typeof(System.String)) : string.Empty;
					item = CompaniesProvider.GetByCompanyName(_companyName);
					results = new TList<Companies>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesSelectMethod.GetByIprocSupplierNo:
					_iprocSupplierNo_nullable = (System.String) EntityUtil.ChangeType(values["IprocSupplierNo"], typeof(System.String));
					results = CompaniesProvider.GetByIprocSupplierNo(_iprocSupplierNo_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesSelectMethod.GetByCompanyAbn:
					_companyAbn = ( values["CompanyAbn"] != null ) ? (System.String) EntityUtil.ChangeType(values["CompanyAbn"], typeof(System.String)) : string.Empty;
					item = CompaniesProvider.GetByCompanyAbn(_companyAbn);
					results = new TList<Companies>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompaniesSelectMethod.GetByEhsConsultantId:
					_ehsConsultantId_nullable = (System.Int32?) EntityUtil.ChangeType(values["EhsConsultantId"], typeof(System.Int32?));
					results = CompaniesProvider.GetByEhsConsultantId(_ehsConsultantId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesSelectMethod.GetByCompanyNameEbi:
					_companyNameEbi_nullable = (System.String) EntityUtil.ChangeType(values["CompanyNameEbi"], typeof(System.String));
					results = CompaniesProvider.GetByCompanyNameEbi(_companyNameEbi_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case CompaniesSelectMethod.GetByCompanyStatusId:
					_companyStatusId = ( values["CompanyStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyStatusId"], typeof(System.Int32)) : (int)0;
					results = CompaniesProvider.GetByCompanyStatusId(_companyStatusId, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesSelectMethod.GetByCompanyStatus2Id:
					_companyStatus2Id = ( values["CompanyStatus2Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyStatus2Id"], typeof(System.Int32)) : (int)0;
					results = CompaniesProvider.GetByCompanyStatus2Id(_companyStatus2Id, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesSelectMethod.GetByModifiedbyUserId:
					_modifiedbyUserId = ( values["ModifiedbyUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedbyUserId"], typeof(System.Int32)) : (int)0;
					results = CompaniesProvider.GetByModifiedbyUserId(_modifiedbyUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				case CompaniesSelectMethod.GetByParentIdFromCompaniesRelationship:
					_parentId = ( values["ParentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ParentId"], typeof(System.Int32)) : (int)0;
					results = CompaniesProvider.GetByParentIdFromCompaniesRelationship(_parentId, this.StartIndex, this.PageSize, out count);
					break;
				case CompaniesSelectMethod.GetByChildIdFromCompaniesRelationship:
					_childId = ( values["ChildId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ChildId"], typeof(System.Int32)) : (int)0;
					results = CompaniesProvider.GetByChildIdFromCompaniesRelationship(_childId, this.StartIndex, this.PageSize, out count);
					break;
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompaniesSelectMethod.Get || SelectMethod == CompaniesSelectMethod.GetByCompanyId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Companies entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompaniesProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Companies> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompaniesProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompaniesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesDataSource class.
	/// </summary>
	public class CompaniesDataSourceDesigner : ProviderDataSourceDesigner<Companies, CompaniesKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompaniesDataSourceDesigner class.
		/// </summary>
		public CompaniesDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesSelectMethod SelectMethod
		{
			get { return ((CompaniesDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompaniesDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompaniesDataSourceActionList

	/// <summary>
	/// Supports the CompaniesDataSourceDesigner class.
	/// </summary>
	internal class CompaniesDataSourceActionList : DesignerActionList
	{
		private CompaniesDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompaniesDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompaniesDataSourceActionList(CompaniesDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompaniesSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompaniesDataSourceActionList
	
	#endregion CompaniesDataSourceDesigner
	
	#region CompaniesSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompaniesDataSource.SelectMethod property.
	/// </summary>
	public enum CompaniesSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCompanyName method.
		/// </summary>
		GetByCompanyName,
		/// <summary>
		/// Represents the GetByIprocSupplierNo method.
		/// </summary>
		GetByIprocSupplierNo,
		/// <summary>
		/// Represents the GetByCompanyAbn method.
		/// </summary>
		GetByCompanyAbn,
		/// <summary>
		/// Represents the GetByEhsConsultantId method.
		/// </summary>
		GetByEhsConsultantId,
		/// <summary>
		/// Represents the GetByCompanyNameEbi method.
		/// </summary>
		GetByCompanyNameEbi,
		/// <summary>
		/// Represents the GetByCompanyStatusId method.
		/// </summary>
		GetByCompanyStatusId,
		/// <summary>
		/// Represents the GetByCompanyStatus2Id method.
		/// </summary>
		GetByCompanyStatus2Id,
		/// <summary>
		/// Represents the GetByModifiedbyUserId method.
		/// </summary>
		GetByModifiedbyUserId,
		/// <summary>
		/// Represents the GetByParentIdFromCompaniesRelationship method.
		/// </summary>
		GetByParentIdFromCompaniesRelationship,
		/// <summary>
		/// Represents the GetByChildIdFromCompaniesRelationship method.
		/// </summary>
		GetByChildIdFromCompaniesRelationship
	}
	
	#endregion CompaniesSelectMethod

	#region CompaniesFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesFilter : SqlFilter<CompaniesColumn>
	{
	}
	
	#endregion CompaniesFilter

	#region CompaniesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesExpressionBuilder : SqlExpressionBuilder<CompaniesColumn>
	{
	}
	
	#endregion CompaniesExpressionBuilder	

	#region CompaniesProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompaniesChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesProperty : ChildEntityProperty<CompaniesChildEntityTypes>
	{
	}
	
	#endregion CompaniesProperty
}

