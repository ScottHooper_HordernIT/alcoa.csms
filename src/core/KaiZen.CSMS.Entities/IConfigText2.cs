﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'ConfigText2' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IConfigText2 
	{
		/// <summary>			
		/// ConfigText2Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ConfigText2"</remarks>
		System.Int32 ConfigText2Id { get; set; }
				
		
		
		/// <summary>
		/// ConfigTextTypeId : 
		/// </summary>
		System.Int32  ConfigTextTypeId  { get; set; }
		
		/// <summary>
		/// ConfigText : 
		/// </summary>
		System.Byte[]  ConfigText  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


