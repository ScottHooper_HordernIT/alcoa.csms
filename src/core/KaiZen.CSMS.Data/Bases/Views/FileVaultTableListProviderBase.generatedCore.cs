﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileVaultTableListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class FileVaultTableListProviderBaseCore : EntityViewProviderBase<FileVaultTableList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;FileVaultTableList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;FileVaultTableList&gt;"/></returns>
		protected static VList&lt;FileVaultTableList&gt; Fill(DataSet dataSet, VList<FileVaultTableList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<FileVaultTableList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;FileVaultTableList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<FileVaultTableList>"/></returns>
		protected static VList&lt;FileVaultTableList&gt; Fill(DataTable dataTable, VList<FileVaultTableList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					FileVaultTableList c = new FileVaultTableList();
					c.FileVaultTableId = (Convert.IsDBNull(row["FileVaultTableId"]))?(int)0:(System.Int32?)row["FileVaultTableId"];
					c.FileNameCustom = (Convert.IsDBNull(row["FileNameCustom"]))?string.Empty:(System.String)row["FileNameCustom"];
					c.FileVaultTableModifiedByUserId = (Convert.IsDBNull(row["FileVaultTableModifiedByUserId"]))?(int)0:(System.Int32?)row["FileVaultTableModifiedByUserId"];
					c.FileVaultTableModifiedDate = (Convert.IsDBNull(row["FileVaultTableModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["FileVaultTableModifiedDate"];
					c.FileVaultSubCategoryId = (Convert.IsDBNull(row["FileVaultSubCategoryId"]))?(int)0:(System.Int32?)row["FileVaultSubCategoryId"];
					c.SubCategoryName = (Convert.IsDBNull(row["SubCategoryName"]))?string.Empty:(System.String)row["SubCategoryName"];
					c.SubCategoryDesc = (Convert.IsDBNull(row["SubCategoryDesc"]))?string.Empty:(System.String)row["SubCategoryDesc"];
					c.FileVaultId = (Convert.IsDBNull(row["FileVaultId"]))?(int)0:(System.Int32)row["FileVaultId"];
					c.FileName = (Convert.IsDBNull(row["FileName"]))?string.Empty:(System.String)row["FileName"];
					c.FileHash = (Convert.IsDBNull(row["FileHash"]))?new byte[] {}:(System.Byte[])row["FileHash"];
					c.ContentLength = (Convert.IsDBNull(row["ContentLength"]))?(int)0:(System.Int32)row["ContentLength"];
					c.FileVaultModifiedDate = (Convert.IsDBNull(row["FileVaultModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["FileVaultModifiedDate"];
					c.FileVaultModifiedByUserId = (Convert.IsDBNull(row["FileVaultModifiedByUserId"]))?(int)0:(System.Int32)row["FileVaultModifiedByUserId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;FileVaultTableList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;FileVaultTableList&gt;"/></returns>
		protected VList<FileVaultTableList> Fill(IDataReader reader, VList<FileVaultTableList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					FileVaultTableList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<FileVaultTableList>("FileVaultTableList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new FileVaultTableList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.FileVaultTableId = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultTableId)))?null:(System.Int32?)reader[((int)FileVaultTableListColumn.FileVaultTableId)];
					//entity.FileVaultTableId = (Convert.IsDBNull(reader["FileVaultTableId"]))?(int)0:(System.Int32?)reader["FileVaultTableId"];
					entity.FileNameCustom = (reader.IsDBNull(((int)FileVaultTableListColumn.FileNameCustom)))?null:(System.String)reader[((int)FileVaultTableListColumn.FileNameCustom)];
					//entity.FileNameCustom = (Convert.IsDBNull(reader["FileNameCustom"]))?string.Empty:(System.String)reader["FileNameCustom"];
					entity.FileVaultTableModifiedByUserId = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultTableModifiedByUserId)))?null:(System.Int32?)reader[((int)FileVaultTableListColumn.FileVaultTableModifiedByUserId)];
					//entity.FileVaultTableModifiedByUserId = (Convert.IsDBNull(reader["FileVaultTableModifiedByUserId"]))?(int)0:(System.Int32?)reader["FileVaultTableModifiedByUserId"];
					entity.FileVaultTableModifiedDate = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultTableModifiedDate)))?null:(System.DateTime?)reader[((int)FileVaultTableListColumn.FileVaultTableModifiedDate)];
					//entity.FileVaultTableModifiedDate = (Convert.IsDBNull(reader["FileVaultTableModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["FileVaultTableModifiedDate"];
					entity.FileVaultSubCategoryId = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultSubCategoryId)))?null:(System.Int32?)reader[((int)FileVaultTableListColumn.FileVaultSubCategoryId)];
					//entity.FileVaultSubCategoryId = (Convert.IsDBNull(reader["FileVaultSubCategoryId"]))?(int)0:(System.Int32?)reader["FileVaultSubCategoryId"];
					entity.SubCategoryName = (reader.IsDBNull(((int)FileVaultTableListColumn.SubCategoryName)))?null:(System.String)reader[((int)FileVaultTableListColumn.SubCategoryName)];
					//entity.SubCategoryName = (Convert.IsDBNull(reader["SubCategoryName"]))?string.Empty:(System.String)reader["SubCategoryName"];
					entity.SubCategoryDesc = (reader.IsDBNull(((int)FileVaultTableListColumn.SubCategoryDesc)))?null:(System.String)reader[((int)FileVaultTableListColumn.SubCategoryDesc)];
					//entity.SubCategoryDesc = (Convert.IsDBNull(reader["SubCategoryDesc"]))?string.Empty:(System.String)reader["SubCategoryDesc"];
					entity.FileVaultId = (System.Int32)reader[((int)FileVaultTableListColumn.FileVaultId)];
					//entity.FileVaultId = (Convert.IsDBNull(reader["FileVaultId"]))?(int)0:(System.Int32)reader["FileVaultId"];
					entity.FileName = (System.String)reader[((int)FileVaultTableListColumn.FileName)];
					//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
					entity.FileHash = (System.Byte[])reader[((int)FileVaultTableListColumn.FileHash)];
					//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
					entity.ContentLength = (System.Int32)reader[((int)FileVaultTableListColumn.ContentLength)];
					//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
					entity.FileVaultModifiedDate = (System.DateTime)reader[((int)FileVaultTableListColumn.FileVaultModifiedDate)];
					//entity.FileVaultModifiedDate = (Convert.IsDBNull(reader["FileVaultModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["FileVaultModifiedDate"];
					entity.FileVaultModifiedByUserId = (System.Int32)reader[((int)FileVaultTableListColumn.FileVaultModifiedByUserId)];
					//entity.FileVaultModifiedByUserId = (Convert.IsDBNull(reader["FileVaultModifiedByUserId"]))?(int)0:(System.Int32)reader["FileVaultModifiedByUserId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="FileVaultTableList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="FileVaultTableList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, FileVaultTableList entity)
		{
			reader.Read();
			entity.FileVaultTableId = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultTableId)))?null:(System.Int32?)reader[((int)FileVaultTableListColumn.FileVaultTableId)];
			//entity.FileVaultTableId = (Convert.IsDBNull(reader["FileVaultTableId"]))?(int)0:(System.Int32?)reader["FileVaultTableId"];
			entity.FileNameCustom = (reader.IsDBNull(((int)FileVaultTableListColumn.FileNameCustom)))?null:(System.String)reader[((int)FileVaultTableListColumn.FileNameCustom)];
			//entity.FileNameCustom = (Convert.IsDBNull(reader["FileNameCustom"]))?string.Empty:(System.String)reader["FileNameCustom"];
			entity.FileVaultTableModifiedByUserId = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultTableModifiedByUserId)))?null:(System.Int32?)reader[((int)FileVaultTableListColumn.FileVaultTableModifiedByUserId)];
			//entity.FileVaultTableModifiedByUserId = (Convert.IsDBNull(reader["FileVaultTableModifiedByUserId"]))?(int)0:(System.Int32?)reader["FileVaultTableModifiedByUserId"];
			entity.FileVaultTableModifiedDate = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultTableModifiedDate)))?null:(System.DateTime?)reader[((int)FileVaultTableListColumn.FileVaultTableModifiedDate)];
			//entity.FileVaultTableModifiedDate = (Convert.IsDBNull(reader["FileVaultTableModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["FileVaultTableModifiedDate"];
			entity.FileVaultSubCategoryId = (reader.IsDBNull(((int)FileVaultTableListColumn.FileVaultSubCategoryId)))?null:(System.Int32?)reader[((int)FileVaultTableListColumn.FileVaultSubCategoryId)];
			//entity.FileVaultSubCategoryId = (Convert.IsDBNull(reader["FileVaultSubCategoryId"]))?(int)0:(System.Int32?)reader["FileVaultSubCategoryId"];
			entity.SubCategoryName = (reader.IsDBNull(((int)FileVaultTableListColumn.SubCategoryName)))?null:(System.String)reader[((int)FileVaultTableListColumn.SubCategoryName)];
			//entity.SubCategoryName = (Convert.IsDBNull(reader["SubCategoryName"]))?string.Empty:(System.String)reader["SubCategoryName"];
			entity.SubCategoryDesc = (reader.IsDBNull(((int)FileVaultTableListColumn.SubCategoryDesc)))?null:(System.String)reader[((int)FileVaultTableListColumn.SubCategoryDesc)];
			//entity.SubCategoryDesc = (Convert.IsDBNull(reader["SubCategoryDesc"]))?string.Empty:(System.String)reader["SubCategoryDesc"];
			entity.FileVaultId = (System.Int32)reader[((int)FileVaultTableListColumn.FileVaultId)];
			//entity.FileVaultId = (Convert.IsDBNull(reader["FileVaultId"]))?(int)0:(System.Int32)reader["FileVaultId"];
			entity.FileName = (System.String)reader[((int)FileVaultTableListColumn.FileName)];
			//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
			entity.FileHash = (System.Byte[])reader[((int)FileVaultTableListColumn.FileHash)];
			//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
			entity.ContentLength = (System.Int32)reader[((int)FileVaultTableListColumn.ContentLength)];
			//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
			entity.FileVaultModifiedDate = (System.DateTime)reader[((int)FileVaultTableListColumn.FileVaultModifiedDate)];
			//entity.FileVaultModifiedDate = (Convert.IsDBNull(reader["FileVaultModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["FileVaultModifiedDate"];
			entity.FileVaultModifiedByUserId = (System.Int32)reader[((int)FileVaultTableListColumn.FileVaultModifiedByUserId)];
			//entity.FileVaultModifiedByUserId = (Convert.IsDBNull(reader["FileVaultModifiedByUserId"]))?(int)0:(System.Int32)reader["FileVaultModifiedByUserId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="FileVaultTableList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="FileVaultTableList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, FileVaultTableList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileVaultTableId = (Convert.IsDBNull(dataRow["FileVaultTableId"]))?(int)0:(System.Int32?)dataRow["FileVaultTableId"];
			entity.FileNameCustom = (Convert.IsDBNull(dataRow["FileNameCustom"]))?string.Empty:(System.String)dataRow["FileNameCustom"];
			entity.FileVaultTableModifiedByUserId = (Convert.IsDBNull(dataRow["FileVaultTableModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["FileVaultTableModifiedByUserId"];
			entity.FileVaultTableModifiedDate = (Convert.IsDBNull(dataRow["FileVaultTableModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["FileVaultTableModifiedDate"];
			entity.FileVaultSubCategoryId = (Convert.IsDBNull(dataRow["FileVaultSubCategoryId"]))?(int)0:(System.Int32?)dataRow["FileVaultSubCategoryId"];
			entity.SubCategoryName = (Convert.IsDBNull(dataRow["SubCategoryName"]))?string.Empty:(System.String)dataRow["SubCategoryName"];
			entity.SubCategoryDesc = (Convert.IsDBNull(dataRow["SubCategoryDesc"]))?string.Empty:(System.String)dataRow["SubCategoryDesc"];
			entity.FileVaultId = (Convert.IsDBNull(dataRow["FileVaultId"]))?(int)0:(System.Int32)dataRow["FileVaultId"];
			entity.FileName = (Convert.IsDBNull(dataRow["FileName"]))?string.Empty:(System.String)dataRow["FileName"];
			entity.FileHash = (Convert.IsDBNull(dataRow["FileHash"]))?new byte[] {}:(System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (Convert.IsDBNull(dataRow["ContentLength"]))?(int)0:(System.Int32)dataRow["ContentLength"];
			entity.FileVaultModifiedDate = (Convert.IsDBNull(dataRow["FileVaultModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["FileVaultModifiedDate"];
			entity.FileVaultModifiedByUserId = (Convert.IsDBNull(dataRow["FileVaultModifiedByUserId"]))?(int)0:(System.Int32)dataRow["FileVaultModifiedByUserId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region FileVaultTableListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListFilterBuilder : SqlFilterBuilder<FileVaultTableListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListFilterBuilder class.
		/// </summary>
		public FileVaultTableListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableListFilterBuilder

	#region FileVaultTableListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListParameterBuilder : ParameterizedSqlFilterBuilder<FileVaultTableListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListParameterBuilder class.
		/// </summary>
		public FileVaultTableListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableListParameterBuilder
	
	#region FileVaultTableListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileVaultTableListSortBuilder : SqlSortBuilder<FileVaultTableListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListSqlSortBuilder class.
		/// </summary>
		public FileVaultTableListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileVaultTableListSortBuilder

} // end namespace
