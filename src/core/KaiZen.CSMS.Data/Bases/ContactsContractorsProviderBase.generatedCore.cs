﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ContactsContractorsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ContactsContractorsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ContactsContractors, KaiZen.CSMS.Entities.ContactsContractorsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContactsContractorsKey key)
		{
			return Delete(transactionManager, key.ContactId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_contactId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _contactId)
		{
			return Delete(null, _contactId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _contactId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_User_ModifiedByUserId key.
		///		FK_ContactsContractors_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_User_ModifiedByUserId key.
		///		FK_ContactsContractors_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		/// <remarks></remarks>
		public TList<ContactsContractors> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_User_ModifiedByUserId key.
		///		FK_ContactsContractors_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_User_ModifiedByUserId key.
		///		fkContactsContractorsUserModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_User_ModifiedByUserId key.
		///		fkContactsContractorsUserModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_User_ModifiedByUserId key.
		///		FK_ContactsContractors_User_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public abstract TList<ContactsContractors> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Sites key.
		///		FK_ContactsContractors_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetBySiteId(System.Int32? _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Sites key.
		///		FK_ContactsContractors_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		/// <remarks></remarks>
		public TList<ContactsContractors> GetBySiteId(TransactionManager transactionManager, System.Int32? _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Sites key.
		///		FK_ContactsContractors_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetBySiteId(TransactionManager transactionManager, System.Int32? _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Sites key.
		///		fkContactsContractorsSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetBySiteId(System.Int32? _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Sites key.
		///		fkContactsContractorsSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetBySiteId(System.Int32? _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Sites key.
		///		FK_ContactsContractors_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public abstract TList<ContactsContractors> GetBySiteId(TransactionManager transactionManager, System.Int32? _siteId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Regions key.
		///		FK_ContactsContractors_Regions Description: 
		/// </summary>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByRegionId(System.Int32? _regionId)
		{
			int count = -1;
			return GetByRegionId(_regionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Regions key.
		///		FK_ContactsContractors_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		/// <remarks></remarks>
		public TList<ContactsContractors> GetByRegionId(TransactionManager transactionManager, System.Int32? _regionId)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Regions key.
		///		FK_ContactsContractors_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByRegionId(TransactionManager transactionManager, System.Int32? _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Regions key.
		///		fkContactsContractorsRegions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByRegionId(System.Int32? _regionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRegionId(null, _regionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Regions key.
		///		fkContactsContractorsRegions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByRegionId(System.Int32? _regionId, int start, int pageLength,out int count)
		{
			return GetByRegionId(null, _regionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Regions key.
		///		FK_ContactsContractors_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public abstract TList<ContactsContractors> GetByRegionId(TransactionManager transactionManager, System.Int32? _regionId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Companies key.
		///		FK_ContactsContractors_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Companies key.
		///		FK_ContactsContractors_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		/// <remarks></remarks>
		public TList<ContactsContractors> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Companies key.
		///		FK_ContactsContractors_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Companies key.
		///		fkContactsContractorsCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Companies key.
		///		fkContactsContractorsCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public TList<ContactsContractors> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ContactsContractors_Companies key.
		///		FK_ContactsContractors_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ContactsContractors objects.</returns>
		public abstract TList<ContactsContractors> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ContactsContractors Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContactsContractorsKey key, int start, int pageLength)
		{
			return GetByContactId(transactionManager, key.ContactId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ContactsContractors index.
		/// </summary>
		/// <param name="_contactId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactId(System.Int32 _contactId)
		{
			int count = -1;
			return GetByContactId(null,_contactId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ContactsContractors index.
		/// </summary>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactId(System.Int32 _contactId, int start, int pageLength)
		{
			int count = -1;
			return GetByContactId(null, _contactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ContactsContractors index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactId(TransactionManager transactionManager, System.Int32 _contactId)
		{
			int count = -1;
			return GetByContactId(transactionManager, _contactId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ContactsContractors index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactId(TransactionManager transactionManager, System.Int32 _contactId, int start, int pageLength)
		{
			int count = -1;
			return GetByContactId(transactionManager, _contactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ContactsContractors index.
		/// </summary>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactId(System.Int32 _contactId, int start, int pageLength, out int count)
		{
			return GetByContactId(null, _contactId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ContactsContractors index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ContactsContractors GetByContactId(TransactionManager transactionManager, System.Int32 _contactId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ContactsContractors index.
		/// </summary>
		/// <param name="_contactLastName"></param>
		/// <param name="_contactFirstName"></param>
		/// <param name="_companyId"></param>
		/// <param name="_contactRole"></param>
		/// <param name="_contactTitle"></param>
		/// <param name="_contactPhone"></param>
		/// <param name="_contactMobile"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(System.String _contactLastName, System.String _contactFirstName, System.Int32 _companyId, System.String _contactRole, System.String _contactTitle, System.String _contactPhone, System.String _contactMobile, System.String _contactEmail, System.Int32? _siteId)
		{
			int count = -1;
			return GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(null,_contactLastName, _contactFirstName, _companyId, _contactRole, _contactTitle, _contactPhone, _contactMobile, _contactEmail, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ContactsContractors index.
		/// </summary>
		/// <param name="_contactLastName"></param>
		/// <param name="_contactFirstName"></param>
		/// <param name="_companyId"></param>
		/// <param name="_contactRole"></param>
		/// <param name="_contactTitle"></param>
		/// <param name="_contactPhone"></param>
		/// <param name="_contactMobile"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(System.String _contactLastName, System.String _contactFirstName, System.Int32 _companyId, System.String _contactRole, System.String _contactTitle, System.String _contactPhone, System.String _contactMobile, System.String _contactEmail, System.Int32? _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(null, _contactLastName, _contactFirstName, _companyId, _contactRole, _contactTitle, _contactPhone, _contactMobile, _contactEmail, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ContactsContractors index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactLastName"></param>
		/// <param name="_contactFirstName"></param>
		/// <param name="_companyId"></param>
		/// <param name="_contactRole"></param>
		/// <param name="_contactTitle"></param>
		/// <param name="_contactPhone"></param>
		/// <param name="_contactMobile"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(TransactionManager transactionManager, System.String _contactLastName, System.String _contactFirstName, System.Int32 _companyId, System.String _contactRole, System.String _contactTitle, System.String _contactPhone, System.String _contactMobile, System.String _contactEmail, System.Int32? _siteId)
		{
			int count = -1;
			return GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(transactionManager, _contactLastName, _contactFirstName, _companyId, _contactRole, _contactTitle, _contactPhone, _contactMobile, _contactEmail, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ContactsContractors index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactLastName"></param>
		/// <param name="_contactFirstName"></param>
		/// <param name="_companyId"></param>
		/// <param name="_contactRole"></param>
		/// <param name="_contactTitle"></param>
		/// <param name="_contactPhone"></param>
		/// <param name="_contactMobile"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(TransactionManager transactionManager, System.String _contactLastName, System.String _contactFirstName, System.Int32 _companyId, System.String _contactRole, System.String _contactTitle, System.String _contactPhone, System.String _contactMobile, System.String _contactEmail, System.Int32? _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(transactionManager, _contactLastName, _contactFirstName, _companyId, _contactRole, _contactTitle, _contactPhone, _contactMobile, _contactEmail, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ContactsContractors index.
		/// </summary>
		/// <param name="_contactLastName"></param>
		/// <param name="_contactFirstName"></param>
		/// <param name="_companyId"></param>
		/// <param name="_contactRole"></param>
		/// <param name="_contactTitle"></param>
		/// <param name="_contactPhone"></param>
		/// <param name="_contactMobile"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public KaiZen.CSMS.Entities.ContactsContractors GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(System.String _contactLastName, System.String _contactFirstName, System.Int32 _companyId, System.String _contactRole, System.String _contactTitle, System.String _contactPhone, System.String _contactMobile, System.String _contactEmail, System.Int32? _siteId, int start, int pageLength, out int count)
		{
			return GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(null, _contactLastName, _contactFirstName, _companyId, _contactRole, _contactTitle, _contactPhone, _contactMobile, _contactEmail, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ContactsContractors index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactLastName"></param>
		/// <param name="_contactFirstName"></param>
		/// <param name="_companyId"></param>
		/// <param name="_contactRole"></param>
		/// <param name="_contactTitle"></param>
		/// <param name="_contactPhone"></param>
		/// <param name="_contactMobile"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ContactsContractors GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(TransactionManager transactionManager, System.String _contactLastName, System.String _contactFirstName, System.Int32 _companyId, System.String _contactRole, System.String _contactTitle, System.String _contactPhone, System.String _contactMobile, System.String _contactEmail, System.Int32? _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ContactsContractors&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ContactsContractors&gt;"/></returns>
		public static TList<ContactsContractors> Fill(IDataReader reader, TList<ContactsContractors> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ContactsContractors c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ContactsContractors")
					.Append("|").Append((System.Int32)reader[((int)ContactsContractorsColumn.ContactId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ContactsContractors>(
					key.ToString(), // EntityTrackingKey
					"ContactsContractors",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ContactsContractors();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ContactId = (System.Int32)reader[((int)ContactsContractorsColumn.ContactId - 1)];
					c.CompanyId = (System.Int32)reader[((int)ContactsContractorsColumn.CompanyId - 1)];
					c.ContactFirstName = (System.String)reader[((int)ContactsContractorsColumn.ContactFirstName - 1)];
					c.ContactLastName = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactLastName - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactLastName - 1)];
					c.ContactRole = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactRole - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactRole - 1)];
					c.ContactTitle = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactTitle - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactTitle - 1)];
					c.ContactEmail = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactEmail - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactEmail - 1)];
					c.ContactMobile = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactMobile - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactMobile - 1)];
					c.ContactPhone = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactPhone - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactPhone - 1)];
					c.RegionId = (reader.IsDBNull(((int)ContactsContractorsColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)ContactsContractorsColumn.RegionId - 1)];
					c.SiteId = (reader.IsDBNull(((int)ContactsContractorsColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)ContactsContractorsColumn.SiteId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)ContactsContractorsColumn.ModifiedByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ContactsContractors entity)
		{
			if (!reader.Read()) return;
			
			entity.ContactId = (System.Int32)reader[((int)ContactsContractorsColumn.ContactId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)ContactsContractorsColumn.CompanyId - 1)];
			entity.ContactFirstName = (System.String)reader[((int)ContactsContractorsColumn.ContactFirstName - 1)];
			entity.ContactLastName = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactLastName - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactLastName - 1)];
			entity.ContactRole = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactRole - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactRole - 1)];
			entity.ContactTitle = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactTitle - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactTitle - 1)];
			entity.ContactEmail = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactEmail - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactEmail - 1)];
			entity.ContactMobile = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactMobile - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactMobile - 1)];
			entity.ContactPhone = (reader.IsDBNull(((int)ContactsContractorsColumn.ContactPhone - 1)))?null:(System.String)reader[((int)ContactsContractorsColumn.ContactPhone - 1)];
			entity.RegionId = (reader.IsDBNull(((int)ContactsContractorsColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)ContactsContractorsColumn.RegionId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)ContactsContractorsColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)ContactsContractorsColumn.SiteId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)ContactsContractorsColumn.ModifiedByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ContactsContractors entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ContactId = (System.Int32)dataRow["ContactId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.ContactFirstName = (System.String)dataRow["ContactFirstName"];
			entity.ContactLastName = Convert.IsDBNull(dataRow["ContactLastName"]) ? null : (System.String)dataRow["ContactLastName"];
			entity.ContactRole = Convert.IsDBNull(dataRow["ContactRole"]) ? null : (System.String)dataRow["ContactRole"];
			entity.ContactTitle = Convert.IsDBNull(dataRow["ContactTitle"]) ? null : (System.String)dataRow["ContactTitle"];
			entity.ContactEmail = Convert.IsDBNull(dataRow["ContactEmail"]) ? null : (System.String)dataRow["ContactEmail"];
			entity.ContactMobile = Convert.IsDBNull(dataRow["ContactMobile"]) ? null : (System.String)dataRow["ContactMobile"];
			entity.ContactPhone = Convert.IsDBNull(dataRow["ContactPhone"]) ? null : (System.String)dataRow["ContactPhone"];
			entity.RegionId = Convert.IsDBNull(dataRow["RegionId"]) ? null : (System.Int32?)dataRow["RegionId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ContactsContractors"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ContactsContractors Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContactsContractors entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SiteId ?? (int)0);
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, (entity.SiteId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource

			#region RegionIdSource	
			if (CanDeepLoad(entity, "Regions|RegionIdSource", deepLoadType, innerList) 
				&& entity.RegionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.RegionId ?? (int)0);
				Regions tmpEntity = EntityManager.LocateEntity<Regions>(EntityLocator.ConstructKeyFromPkItems(typeof(Regions), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RegionIdSource = tmpEntity;
				else
					entity.RegionIdSource = DataRepository.RegionsProvider.GetByRegionId(transactionManager, (entity.RegionId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RegionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RegionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RegionsProvider.DeepLoad(transactionManager, entity.RegionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RegionIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ContactsContractors object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ContactsContractors instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ContactsContractors Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContactsContractors entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			
			#region RegionIdSource
			if (CanDeepSave(entity, "Regions|RegionIdSource", deepSaveType, innerList) 
				&& entity.RegionIdSource != null)
			{
				DataRepository.RegionsProvider.Save(transactionManager, entity.RegionIdSource);
				entity.RegionId = entity.RegionIdSource.RegionId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ContactsContractorsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ContactsContractors</c>
	///</summary>
	public enum ContactsContractorsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
			
		///<summary>
		/// Composite Property for <c>Regions</c> at RegionIdSource
		///</summary>
		[ChildEntityType(typeof(Regions))]
		Regions,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
		}
	
	#endregion ContactsContractorsChildEntityTypes
	
	#region ContactsContractorsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ContactsContractorsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsFilterBuilder : SqlFilterBuilder<ContactsContractorsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsFilterBuilder class.
		/// </summary>
		public ContactsContractorsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContactsContractorsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContactsContractorsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContactsContractorsFilterBuilder
	
	#region ContactsContractorsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ContactsContractorsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContactsContractorsParameterBuilder : ParameterizedSqlFilterBuilder<ContactsContractorsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsParameterBuilder class.
		/// </summary>
		public ContactsContractorsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContactsContractorsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContactsContractorsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContactsContractorsParameterBuilder
	
	#region ContactsContractorsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ContactsContractorsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContactsContractors"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ContactsContractorsSortBuilder : SqlSortBuilder<ContactsContractorsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContactsContractorsSqlSortBuilder class.
		/// </summary>
		public ContactsContractorsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ContactsContractorsSortBuilder
	
} // end namespace
