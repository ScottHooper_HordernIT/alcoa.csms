<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_SqQuestionnaire_Questionnaire3Verification_4" Codebehind="QuestionnaireVerification_4.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxhe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxwsc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<style type="text/css">
    .transparency
    {
        filter: alpha(opacity=86);
        -moz-opacity: .86;
        opacity: 0.86;
        background-color: #EEE;
    }
</style>

<script language="JavaScript">
    function getStyle(divId) {
        var temp = document.getElementById(divId).style.visibility;

        return temp;
    }

    function switchVisibility(divId) {

        var current = getStyle();

        if (current == "visible") {
            document.getElementById(divId).style.visibility = "hidden";
        }
        else {
            document.getElementById(divId).style.visibility = "visible";
        }
    }

    function setInvisible(divId) {
        if (document.getElementById(divId) != null) {
            document.getElementById(divId).style.visibility = "hidden";
        }
    }
</script>

<%: System.Web.Optimization.Scripts.Render("~/bundles/countdown_timer_scripts") %>

<asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
    <ContentTemplate>
        <asp:Panel ID="pnlTime" runat="server">
            <div style="z-index: 12; position: absolute">
                <table width="100%">
                    <tr>
                        <td width="100%" align="right">
                            <span class="timeClassHeader">Time Remaining (to Save):</span>
                        </td>
                        <td width="100%" align="right">
                            <span id="theTime" class="timeClass"></span>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <table style="width: 890px">
            <tbody>
                <tr>
                    <td style="text-align: center" colspan="3">
                        <span><span style="font-size: 8pt"><span style="color: #000000"></span></span></span>
                        <div style="float: left; position: absolute" align="left">
                            <span style="font-size: 8pt">Section<br />
                            </span><span style="font-size: 11pt"><strong>3 of 7<br />
                            </strong></span>
                        </div>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="text-align: right" width="402">
                                    <div align="right">
                                        <dxe:ASPxButton ID="btnPrevious" Visible="True" OnClick="btnPrevious_Click" runat="server"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                            Text="<< Previous Section (and Save)" Width="202px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            ForeColor="Navy">
                                        </dxe:ASPxButton>
                                        </div>
                                    </td>
                                    <td width="148">
                                        <dxe:ASPxButton ID="btnHome" OnClick="btnHome_Click" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Text="[ Home (and Save) ]" Width="148px" Font-Bold="True"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" ForeColor="Navy">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td style="text-align: left" width="402">
                                        <dxe:ASPxButton ID="btnNext" OnClick="btnNext_Click" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Text="Next Section (and Save) >>" Width="182px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            ForeColor="Navy">
                                        </dxe:ASPxButton>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <strong><span style="font-size: 10pt; color: #ff0000">
                            <asp:Label ID="Label1" runat="server" Text="Warning: You must save your work at least every 30 min (see clock) to ensure data being saved correctly."></asp:Label></span></strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <asp:Panel ID="Panel1" runat="server" Width="890px">
            <br />
            <table style="width: 516px" id="tblContainer0">
                <tbody>
                    <tr>
                        <td style="width: 736px; height: 102px">
                            <h3 style="margin: 6pt 0cm">
                                <span style="font-size: 14pt; color: #0000ff; font-style: normal; font-family: Arial">
                                    People Skills and Training</span></h3>
                            <span style="font-weight: bold; font-size: 11pt; color: blue; font-family: Arial">Criteria</span>
                            <br />
                            <span style="font-size: 11pt; color: black; font-family: Arial">The Contractor will
                                have established Systems and Procedures that ensure all employees are trained to
                                fulfil their roles and responsibilities, and are competent to perform all tasks
                                in a way that is safe and does not adversely impact on themselves, others or the
                                environment.<br />
                            </span><span style="font-size: 11pt; color: black; font-family: Arial"></span><span
                                style="font-weight: bold; font-size: 11pt; color: #0000ff; font-family: Arial">
                                <br />
                                Note</span>
                            <br />
                            <span style="font-size: 11pt; color: #000000; font-family: Arial">You must provide either
                                supporting information or upload a file as detailed in the Evidence.</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 736px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 736px">
                            <br />
                            <span style="font-size: 11pt; color: blue; font-family: Arial"><strong>Questions</strong></span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 736px; height: 341px">
                            <dxrp:ASPxRoundPanel ID="rp1" runat="server" Width="890px" HeaderText="1" EnableDefaultAppearance="False"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                BackColor="#DDECFE" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <b>1</b>
                                            </td>
                                            <td align="right" style="text-align: right; width: 900px;">
                                                <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=1');setInvisible('ctl00_body_ctl00_rp1_div1');" style="color: #ffff00; text-decoration: underline; font-weight: bold">Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <TopEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </TopEdge>
                                <BottomRightCorner Height="9px" Width="9px">
                                </BottomRightCorner>
                                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                                <NoHeaderTopRightCorner Height="9px" Width="9px">
                                </NoHeaderTopRightCorner>
                                <HeaderRightEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderRightEdge>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                                <HeaderLeftEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderLeftEdge>
                                <HeaderStyle BackColor="#7BA4E0">
                                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px">
                                    </Paddings>
                                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                                </HeaderStyle>
                                <TopRightCorner Height="9px" Width="9px">
                                </TopRightCorner>
                                <HeaderContent>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderContent>
                                <DisabledStyle ForeColor="Gray">
                                </DisabledStyle>
                                <NoHeaderTopEdge BackColor="#DDECFE">
                                </NoHeaderTopEdge>
                                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                                </NoHeaderTopLeftCorner>
                                <PanelCollection>
                                    <dxp:PanelContent runat="server">
                                    <div style="z-index: 10; height: 270px; width: 890px" visible="true">
                                            <div id="div1" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 890px; visibility: visible; text-align: center;" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<span align="center" style="text-align: center; color: red">You will be unable to submit the answer to this question until you have clicked on the<br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=1');setInvisible('ctl00_body_ctl00_rp1_div1');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                        <table style="font-size: 10pt; color: black" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                        <span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                            <strong>Has an EHS training needs analysis for all employees been completed and documented?</strong></span></span></span>
                                                    </td>
                                                    <td style="width: 150px; height: 13px; text-align: right">
                                                        <dxe:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" CssPostfix="Office2003Blue"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="100px" ID="rb1"
                                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                                                <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                                            </Items>
                                                            <ValidationSettings Display="Dynamic">
                                                            </ValidationSettings>
                                                        </dxe:ASPxRadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 14px" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Evidence</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="height: 13px" width="100%" colspan="2">
                                                        <em><span style="font-size: 10pt; color: black">
                                                            You are required to upload a document (spreadsheet) demonstrating that all legislative H & S training requirements for employees have been identified and documented in a Training Needs Analysis.
</span></em>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dx:ASPxUploadControl ID="f1" runat="server" Width="100%">
                                                            <ValidationSettings  
                                                                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                                                GeneralErrorText="File Upload Failed."
                                                                MaxFileSize="16252928" 
                                                                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                                                                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="f1e" Visible="False"></asp:Label>
                                                        <dxe:ASPxHyperLink runat="server" ID="f1l">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
<tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Comments (Optional)</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dxe:ASPxMemo runat="server" Height="71px" Width="100%" ID="m1" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                            </ValidationSettings>
                                                        </dxe:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <table border="0" cellpadding="0" cellspacing="0" style="padding-top: 2px;" width="100%">
                                            <tr>
                                                <td style="padding-top: 2px;">
                                                    <dxrp:ASPxRoundPanel ID="rpAssess1" runat="server" HeaderText="Assessor Reviewal & Comments"
                                                        Width="850px" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" 
                                                        CssPostfix="Office2010Silver" EnableDefaultAppearance="False" 
                                                        GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
                                                        SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
                                                        <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
                                                            PaddingTop="10px" />
                                                        <HeaderStyle BackColor="#E9E9E9" Font-Bold="True" HorizontalAlign="Center" ForeColor="#C00000">
                                                            <BorderBottom BorderStyle="None" />
                                                        </HeaderStyle>
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 47px; text-align: right">
                                                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True"
                                                                                ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                                CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                                ID="assess1Approval" SelectedIndex="2">
                                                                                <Items>
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                                                </Items>
                                                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                                                </LoadingPanelImage>
                                                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" Display="Dynamic">
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField IsRequired="False" />
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100px; text-align: left">
                                                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..."
                                                                                Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue"
                                                                                CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess1Comments">
                                                                                <NullTextStyle ForeColor="Gray">
                                                                                </NullTextStyle>
                                                                                <ValidationSettings>
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <TopLeftCorner Height="9px" Width="9px">
                                </TopLeftCorner>
                                <BottomLeftCorner Height="9px" Width="9px">
                                </BottomLeftCorner>
                            </dxrp:ASPxRoundPanel>
                            <br />
                            <dxrp:ASPxRoundPanel ID="rp2" runat="server" Width="890px" HeaderText="2" EnableDefaultAppearance="False"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                BackColor="#DDECFE" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <b>2</b>
                                            </td>
                                            <td align="right" style="text-align: right; width: 900px;">
                                                <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=2');setInvisible('ctl00_body_ctl00_rp2_div2');" style="color: #ffff00; text-decoration: underline; font-weight: bold">Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <TopEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </TopEdge>
                                <BottomRightCorner Height="9px" Width="9px">
                                </BottomRightCorner>
                                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                                <NoHeaderTopRightCorner Height="9px" Width="9px">
                                </NoHeaderTopRightCorner>
                                <HeaderRightEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderRightEdge>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                                <HeaderLeftEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderLeftEdge>
                                <HeaderStyle BackColor="#7BA4E0">
                                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px">
                                    </Paddings>
                                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                                </HeaderStyle>
                                <TopRightCorner Height="9px" Width="9px">
                                </TopRightCorner>
                                <HeaderContent>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderContent>
                                <DisabledStyle ForeColor="Gray">
                                </DisabledStyle>
                                <NoHeaderTopEdge BackColor="#DDECFE">
                                </NoHeaderTopEdge>
                                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                                </NoHeaderTopLeftCorner>
                                <PanelCollection>
                                    <dxp:PanelContent runat="server">
                                    <div style="z-index: 10; height: 270px; width: 890px" visible="true">
                                            <div id="div2" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 890px; visibility: visible; text-align: center;" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<span align="center" style="text-align: center; color: red">You will be unable to submit the answer to this question until you have clicked on the<br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=2');setInvisible('ctl00_body_ctl00_rp2_div2');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                        <table style="font-size: 10pt; color: black" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                        <span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                            <strong><span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                                <strong>Has an H&amp;S Training Plan been developed and documented to address the identified
                                                                    needs?</strong></span></span></span></strong></span></span></span>
                                                    </td>
                                                    <td style="width: 150px; height: 13px; text-align: right">
                                                        <dxe:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" CssPostfix="Office2003Blue"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="100px" ID="rb2"
                                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                                                <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                                            </Items>
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic">
                                                            </ValidationSettings>
                                                        </dxe:ASPxRadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 14px" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Evidence</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="height: 13px" width="100%" colspan="2">
                                                        <em><span style="font-size: 10pt; color: black">
                                                            You are required to upload a Training Plan identifying proposed training derived from the documented H &amp; S Training Needs Analysis that identifies proposed training over a defined period of time for the organisations employees.
</span></em>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dx:ASPxUploadControl ID="f2" runat="server" Width="100%">
                                                            <ValidationSettings  
                                                                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                                                GeneralErrorText="File Upload Failed."
                                                                MaxFileSize="16252928" 
                                                                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                                                                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="f2e" Visible="False"></asp:Label>
                                                        <dxe:ASPxHyperLink runat="server" ID="f2l">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
<tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Comments (Optional)</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dxe:ASPxMemo runat="server" Height="71px" Width="100%" ID="m2" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                            </ValidationSettings>
                                                        </dxe:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <table border="0" cellpadding="0" cellspacing="0" style="padding-top: 2px;" width="100%">
                                            <tr>
                                                <td style="padding-top: 2px;">
                                                    <dxrp:ASPxRoundPanel ID="rpAssess2" runat="server" HeaderText="Assessor Reviewal & Comments"
                                                        Width="850px" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" 
                                                        CssPostfix="Office2010Silver" EnableDefaultAppearance="False" 
                                                        GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
                                                        SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
                                                        <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
                                                            PaddingTop="10px" />
                                                        <HeaderStyle BackColor="#E9E9E9" Font-Bold="True" HorizontalAlign="Center" ForeColor="#C00000">
                                                            <BorderBottom BorderStyle="None" />
                                                        </HeaderStyle>
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 47px; text-align: right">
                                                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True"
                                                                                ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                                CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                                ID="assess2Approval" SelectedIndex="2">
                                                                                <Items>
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                                                </Items>
                                                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                                                </LoadingPanelImage>
                                                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" Display="Dynamic">
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField IsRequired="False" />
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100px; text-align: left">
                                                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..."
                                                                                Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue"
                                                                                CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess2Comments">
                                                                                <NullTextStyle ForeColor="Gray">
                                                                                </NullTextStyle>
                                                                                <ValidationSettings>
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <TopLeftCorner Height="9px" Width="9px">
                                </TopLeftCorner>
                                <BottomLeftCorner Height="9px" Width="9px">
                                </BottomLeftCorner>
                            </dxrp:ASPxRoundPanel>
                            <br />
                            <dxrp:ASPxRoundPanel ID="rp3" runat="server" Width="890px" HeaderText="3" EnableDefaultAppearance="False"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                BackColor="#DDECFE" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <b>3</b>
                                            </td>
                                            <td align="right" style="text-align: right; width: 900px;">
                                                <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=3');setInvisible('ctl00_body_ctl00_rp3_div3');" style="color: #ffff00; text-decoration: underline; font-weight: bold">Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <TopEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </TopEdge>
                                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                                <HeaderRightEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderRightEdge>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                                <HeaderLeftEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderLeftEdge>
                                <HeaderStyle BackColor="#7BA4E0">
                                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px">
                                    </Paddings>
                                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                                </HeaderStyle>
                                <HeaderContent>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderContent>
                                <DisabledStyle ForeColor="Gray">
                                </DisabledStyle>
                                <NoHeaderTopEdge BackColor="#DDECFE">
                                </NoHeaderTopEdge>
                                <PanelCollection>
                                    <dxp:PanelContent runat="server">
                                    <div style="z-index: 10; height: 270px; width: 890px" visible="true">
                                            <div id="div3" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 890px; visibility: visible; text-align: center;" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<span align="center" style="text-align: center; color: red">You will be unable to submit the answer to this question until you have clicked on the<br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=3');setInvisible('ctl00_body_ctl00_rp3_div3');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                        <table style="font-size: 10pt; color: black" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 749px; height: 13px">
                                                        <span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                            <strong><span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                                <strong>Are records of H&amp;S Training accurate and up to date?</strong></span></span></span></strong></span></span></span>
                                                    </td>
                                                    <td style="width: 150px; height: 13px; text-align: right">
                                                        <dxe:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" CssPostfix="Office2003Blue"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="100px" ID="rb3"
                                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                                                <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                                            </Items>
                                                            <ValidationSettings Display="Dynamic">
                                                            </ValidationSettings>
                                                        </dxe:ASPxRadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 14px" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="width: 749px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Evidence</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="height: 13px" width="100%" colspan="2">
                                                        <em><span style="font-size: 10pt; color: black">
                                                            You are required to upload a file that provides evidence of the company procedure that retains currency of H&amp;S training and competency.
</span></em>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 749px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dx:ASPxUploadControl ID="f3" runat="server" Width="100%">
                                                            <ValidationSettings  
                                                                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                                                GeneralErrorText="File Upload Failed."
                                                                MaxFileSize="16252928" 
                                                                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                                                                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="f3e" Visible="False"></asp:Label>
                                                        &nbsp;
                                                        <dxe:ASPxHyperLink runat="server" ID="f3l">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
<tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Comments (Optional)</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dxe:ASPxMemo runat="server" Height="71px" Width="100%" ID="m3" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                            </ValidationSettings>
                                                        </dxe:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <table border="0" cellpadding="0" cellspacing="0" style="padding-top: 2px;" width="100%">
                                            <tr>
                                                <td style="padding-top: 2px;">
                                                    <dxrp:ASPxRoundPanel ID="rpAssess3" runat="server" HeaderText="Assessor Reviewal & Comments"
                                                        Width="850px" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" 
                                                        CssPostfix="Office2010Silver" EnableDefaultAppearance="False" 
                                                        GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
                                                        SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
                                                        <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
                                                            PaddingTop="10px" />
                                                        <HeaderStyle BackColor="#E9E9E9" Font-Bold="True" HorizontalAlign="Center" ForeColor="#C00000">
                                                            <BorderBottom BorderStyle="None" />
                                                        </HeaderStyle>
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 47px; text-align: right">
                                                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True"
                                                                                ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                                CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                                ID="assess3Approval" SelectedIndex="2">
                                                                                <Items>
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                                                </Items>
                                                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                                                </LoadingPanelImage>
                                                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" Display="Dynamic">
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField IsRequired="False" />
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100px; text-align: left">
                                                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..."
                                                                                Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue"
                                                                                CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess3Comments">
                                                                                <NullTextStyle ForeColor="Gray">
                                                                                </NullTextStyle>
                                                                                <ValidationSettings>
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxp:PanelContent>
                                </PanelCollection>
                            </dxrp:ASPxRoundPanel>
                            <br />
                            <dxrp:ASPxRoundPanel ID="rp4" runat="server" Width="890px" HeaderText="4" EnableDefaultAppearance="False"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                BackColor="#DDECFE" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <b>4</b>
                                            </td>
                                            <td align="right" style="text-align: right; width: 900px;">
                                                <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=4');setInvisible('ctl00_body_ctl00_rp4_div4');" style="color: #ffff00; text-decoration: underline; font-weight: bold">Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <TopEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </TopEdge>
                                <BottomRightCorner Height="9px" Width="9px">
                                </BottomRightCorner>
                                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                                <NoHeaderTopRightCorner Height="9px" Width="9px">
                                </NoHeaderTopRightCorner>
                                <HeaderRightEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderRightEdge>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                                <HeaderLeftEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderLeftEdge>
                                <HeaderStyle BackColor="#7BA4E0">
                                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px">
                                    </Paddings>
                                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                                </HeaderStyle>
                                <TopRightCorner Height="9px" Width="9px">
                                </TopRightCorner>
                                <HeaderContent>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderContent>
                                <DisabledStyle ForeColor="Gray">
                                </DisabledStyle>
                                <NoHeaderTopEdge BackColor="#DDECFE">
                                </NoHeaderTopEdge>
                                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                                </NoHeaderTopLeftCorner>
                                <PanelCollection>
                                    <dxp:PanelContent runat="server">
                                    <div style="z-index: 10; height: 270px; width: 890px" visible="true">
                                            <div id="div4" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 890px; visibility: visible; text-align: center;" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<span align="center" style="text-align: center; color: red">You will be unable to submit the answer to this question until you have clicked on the<br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=4');setInvisible('ctl00_body_ctl00_rp4_div4');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                        <table style="font-size: 10pt; color: black" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                        <span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                            <strong><span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                                <strong>Are managers and supervisors trained in H&amp;S management principles and practices
                                                                    relevant to their roles and responsibilities?</strong></span></span></span></strong></span></span></span>
                                                    </td>
                                                    <td style="width: 150px; height: 13px; text-align: right">
                                                        <dxe:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" CssPostfix="Office2003Blue"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="100px" ID="rb4"
                                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                                                <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                                            </Items>
                                                            <ValidationSettings Display="Dynamic">
                                                            </ValidationSettings>
                                                        </dxe:ASPxRadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 14px" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Evidence</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="height: 13px" width="100%" colspan="2">
                                                        <em><span style="font-size: 10pt; color: black">
                                                            You are required to upload an H&amp;S Training Needs Analysis or a H&amp;S Training Plan for Managers and Supervisors within your Company that documents the H&amp;S training needs and planned implementation of identified training.
</span></em>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dx:ASPxUploadControl ID="f4" runat="server" Width="100%">
                                                            <ValidationSettings  
                                                                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                                                GeneralErrorText="File Upload Failed."
                                                                MaxFileSize="16252928" 
                                                                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                                                                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="f4e" Visible="False"></asp:Label>
                                                        &nbsp;
                                                        <dxe:ASPxHyperLink runat="server" ID="f4l">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
<tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Comments (Optional)</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dxe:ASPxMemo runat="server" Height="71px" Width="100%" ID="m4" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                            </ValidationSettings>
                                                        </dxe:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <table border="0" cellpadding="0" cellspacing="0" style="padding-top: 2px;" width="100%">
                                            <tr>
                                                <td style="padding-top: 2px;">
                                                    <dxrp:ASPxRoundPanel ID="rpAssess4" runat="server" HeaderText="Assessor Reviewal & Comments"
                                                        Width="850px" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" 
                                                        CssPostfix="Office2010Silver" EnableDefaultAppearance="False" 
                                                        GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
                                                        SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
                                                        <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
                                                            PaddingTop="10px" />
                                                        <HeaderStyle BackColor="#E9E9E9" Font-Bold="True" HorizontalAlign="Center" ForeColor="#C00000">
                                                            <BorderBottom BorderStyle="None" />
                                                        </HeaderStyle>
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 47px; text-align: right">
                                                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True"
                                                                                ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                                CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                                ID="assess4Approval" SelectedIndex="2">
                                                                                <Items>
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                                                </Items>
                                                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                                                </LoadingPanelImage>
                                                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" Display="Dynamic">
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField IsRequired="False" />
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100px; text-align: left">
                                                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..."
                                                                                Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue"
                                                                                CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess4Comments">
                                                                                <NullTextStyle ForeColor="Gray">
                                                                                </NullTextStyle>
                                                                                <ValidationSettings>
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <TopLeftCorner Height="9px" Width="9px">
                                </TopLeftCorner>
                                <BottomLeftCorner Height="9px" Width="9px">
                                </BottomLeftCorner>
                            </dxrp:ASPxRoundPanel>
                            <br />
                            <dxrp:ASPxRoundPanel ID="rp5" runat="server" Width="890px" HeaderText="5" EnableDefaultAppearance="False"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                BackColor="#DDECFE" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 10px">
                                                <b>5</b>
                                            </td>
                                            <td align="right" style="text-align: right; width: 900px;">
                                                <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=5');setInvisible('ctl00_body_ctl00_rp5_div5');" style="color: #ffff00; text-decoration: underline; font-weight: bold">Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <TopEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </TopEdge>
                                <BottomRightCorner Height="9px" Width="9px">
                                </BottomRightCorner>
                                <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                                <NoHeaderTopRightCorner Height="9px" Width="9px">
                                </NoHeaderTopRightCorner>
                                <HeaderRightEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderRightEdge>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                                <HeaderLeftEdge>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderLeftEdge>
                                <HeaderStyle BackColor="#7BA4E0">
                                    <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px">
                                    </Paddings>
                                    <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                                </HeaderStyle>
                                <TopRightCorner Height="9px" Width="9px">
                                </TopRightCorner>
                                <HeaderContent>
                                    <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                        HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                                </HeaderContent>
                                <DisabledStyle ForeColor="Gray">
                                </DisabledStyle>
                                <NoHeaderTopEdge BackColor="#DDECFE">
                                </NoHeaderTopEdge>
                                <NoHeaderTopLeftCorner Height="9px" Width="9px">
                                </NoHeaderTopLeftCorner>
                                <PanelCollection>
                                    <dxp:PanelContent runat="server">
                                    <div style="z-index: 10; height: 270px; width: 890px" visible="true">
                                            <div id="div5" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 890px; visibility: visible; text-align: center;" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<span align="center" style="text-align: center; color: red">You will be unable to submit the answer to this question until you have clicked on the<br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Verification&SubType=Help&SectionId=3&QuestionId=5');setInvisible('ctl00_body_ctl00_rp5_div5');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                        <table style="font-size: 10pt; color: black" cellspacing="0" cellpadding="0" width="870"
                                            border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                        <span style="font-family: Arial"><span style="color: black"><span style="font-size: 11pt">
                                                            <strong>Have employees been evaluated or tested to ensure that they are competent to
                                                                apply the training outcomes?</strong></span></span></span>
                                                    </td>
                                                    <td style="width: 150px; height: 13px; text-align: right">
                                                        <dxe:ASPxRadioButtonList runat="server" RepeatDirection="Horizontal" CssPostfix="Office2003Blue"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="100px" ID="rb5"
                                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                                                <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                                            </Items>
                                                            <ValidationSettings Display="Dynamic">
                                                            </ValidationSettings>
                                                        </dxe:ASPxRadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 14px" colspan="2">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Evidence</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 10pt">
                                                    <td style="height: 13px" width="100%" colspan="2">
                                                        <em><span style="font-size: 10pt; color: black">You are required to upload a copy of your company's verification of H&amp;S competency procedure or process to support your response.
</span></em>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dx:ASPxUploadControl ID="f5" runat="server" Width="100%">
                                                            <ValidationSettings  
                                                                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                                                GeneralErrorText="File Upload Failed."
                                                                MaxFileSize="16252928" 
                                                                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                                                                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                                            </ValidationSettings>
                                                        </dx:ASPxUploadControl>
                                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="f5e" Visible="False"></asp:Label>
                                                        <dxe:ASPxHyperLink runat="server" ID="f5l">
                                                        </dxe:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 750px; height: 13px">
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
<tr style="font-size: 10pt">
                                                    <td style="width: 750px; height: 13px">
                                                        <strong><span style="font-size: 11pt; color: black">Comments (Optional)</span></strong>
                                                    </td>
                                                    <td style="width: 150px; height: 13px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 13px" colspan="2">
                                                        <dxe:ASPxMemo runat="server" Height="71px" Width="100%" ID="m5" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit">
                                                            </ValidationSettings>
                                                        </dxe:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <table border="0" cellpadding="0" cellspacing="0" style="padding-top: 2px;" width="100%">
                                            <tr>
                                                <td style="padding-top: 2px;">
                                                    <dxrp:ASPxRoundPanel ID="rpAssess5" runat="server" HeaderText="Assessor Reviewal & Comments"
                                                        Width="850px" CssFilePath="~/App_Themes/Office2010Silver/{0}/styles.css" 
                                                        CssPostfix="Office2010Silver" EnableDefaultAppearance="False" 
                                                        GroupBoxCaptionOffsetX="6px" GroupBoxCaptionOffsetY="-19px" 
                                                        SpriteCssFilePath="~/App_Themes/Office2010Silver/{0}/sprite.css">
                                                        <ContentPaddings PaddingBottom="10px" PaddingLeft="9px" PaddingRight="11px" 
                                                            PaddingTop="10px" />
                                                        <HeaderStyle BackColor="#E9E9E9" Font-Bold="True" HorizontalAlign="Center" ForeColor="#C00000">
                                                            <BorderBottom BorderStyle="None" />
                                                        </HeaderStyle>
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 47px; text-align: right">
                                                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True"
                                                                                ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                                                                CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                                                                ID="assess5Approval" SelectedIndex="2">
                                                                                <Items>
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                                                </Items>
                                                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                                                </LoadingPanelImage>
                                                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" Display="Dynamic">
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                    <RequiredField IsRequired="False" />
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100px; text-align: left">
                                                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..."
                                                                                Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue"
                                                                                CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess5Comments">
                                                                                <NullTextStyle ForeColor="Gray">
                                                                                </NullTextStyle>
                                                                                <ValidationSettings>
                                                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                                                    </ErrorFrameStyle>
                                                                                </ValidationSettings>
                                                                            </dxe:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxp:PanelContent>
                                </PanelCollection>
                                <TopLeftCorner Height="9px" Width="9px">
                                </TopLeftCorner>
                                <BottomLeftCorner Height="9px" Width="9px">
                                </BottomLeftCorner>
                            </dxrp:ASPxRoundPanel>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <table style="width: 890px">
            <tbody>
                <tr>
                    <td style="text-align: center" colspan="3">
                        <br />
                        <div align="center">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width: 150px">
                                        <dxe:ASPxButton ID="btnPrevious2" OnClick="btnPrevious_Click" runat="server" Width="202px"
                                            Text="<< Previous Section (and Save)" ForeColor="Navy" CssPostfix="Office2003Blue"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td style="width: 85px">
                                        <dxe:ASPxButton ID="btnHome2" OnClick="btnHome_Click" runat="server" Width="148px"
                                            Text="[ Home (and Save) ]" ForeColor="Navy" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            Font-Bold="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td style="width: 150px">
                                        <dxe:ASPxButton ID="btnNext2" OnClick="btnNext_Click" runat="server" Width="182px"
                                            Text="Next Section (and Save) >>" ForeColor="Navy" CssPostfix="Office2003Blue"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxButton>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        <strong><span style="font-size: 10pt; color: #ff0000">
                            <asp:Label ID="Label2" runat="server" Text="Warning: You must save your work at least every 30 min (see clock) to ensure data being saved correctly."></asp:Label></span></strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <dxpc:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2"
            runat="server" BackColor="Info" CloseAction="MouseOut" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" EnableHotTrack="False" ForeColor="Black" Modal="True"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" HeaderText="Warning"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <br />
                    <br />
                    <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid;
                        padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px;
                        border-bottom: black 1px solid; background-color: #ffffcc; text-align: center">
                        <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">
                            <asp:Label ID="lblBox1_PopUp" runat="server" Text="Could not save. Error Occured."></asp:Label></span><br />
                        </strong><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana">
                            <asp:Label ID="lblBox2_PopUp" runat="server" Text="Your questionnaire could not be saved as one or more files you intended to upload is incorrectly formatted or sized. Please scroll down and correct this, then try saving again."></asp:Label>
                            <br />
                            <br />
                            <dxe:ASPxButton ID="ASPxButton3" Text="Close" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                UseSubmitBehavior="False" Width="150px" Visible="true" AutoPostBack="false">
                                <ClientSideEvents Click="function(s, e) { ASPxPopupControl2.Hide();
}"></ClientSideEvents>
                            </dxe:ASPxButton>
                            <br />
                        </span></span></span>
                    </div>
                    <br />
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <HeaderStyle>
                <Paddings PaddingRight="6px" />
            </HeaderStyle>
        </dxpc:ASPxPopupControl>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnPrevious" />
        <asp:PostBackTrigger ControlID="btnPrevious2" />
        <asp:PostBackTrigger ControlID="btnHome" />
        <asp:PostBackTrigger ControlID="btnHome2" />
        <asp:PostBackTrigger ControlID="btnNext" />
        <asp:PostBackTrigger ControlID="btnNext2" />
    </Triggers>
</asp:UpdatePanel>
