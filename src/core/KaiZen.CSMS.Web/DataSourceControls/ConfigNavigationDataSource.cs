﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ConfigNavigationProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ConfigNavigationDataSourceDesigner))]
	public class ConfigNavigationDataSource : ProviderDataSource<ConfigNavigation, ConfigNavigationKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationDataSource class.
		/// </summary>
		public ConfigNavigationDataSource() : base(new ConfigNavigationService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ConfigNavigationDataSourceView used by the ConfigNavigationDataSource.
		/// </summary>
		protected ConfigNavigationDataSourceView ConfigNavigationView
		{
			get { return ( View as ConfigNavigationDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ConfigNavigationDataSource control invokes to retrieve data.
		/// </summary>
		public ConfigNavigationSelectMethod SelectMethod
		{
			get
			{
				ConfigNavigationSelectMethod selectMethod = ConfigNavigationSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ConfigNavigationSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ConfigNavigationDataSourceView class that is to be
		/// used by the ConfigNavigationDataSource.
		/// </summary>
		/// <returns>An instance of the ConfigNavigationDataSourceView class.</returns>
		protected override BaseDataSourceView<ConfigNavigation, ConfigNavigationKey> GetNewDataSourceView()
		{
			return new ConfigNavigationDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ConfigNavigationDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ConfigNavigationDataSourceView : ProviderDataSourceView<ConfigNavigation, ConfigNavigationKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ConfigNavigationDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ConfigNavigationDataSourceView(ConfigNavigationDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ConfigNavigationDataSource ConfigNavigationOwner
		{
			get { return Owner as ConfigNavigationDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ConfigNavigationSelectMethod SelectMethod
		{
			get { return ConfigNavigationOwner.SelectMethod; }
			set { ConfigNavigationOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ConfigNavigationService ConfigNavigationProvider
		{
			get { return Provider as ConfigNavigationService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ConfigNavigation> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ConfigNavigation> results = null;
			ConfigNavigation item;
			count = 0;
			
			System.Int32 _configNavigationId;
			System.String _navigateUrl_nullable;

			switch ( SelectMethod )
			{
				case ConfigNavigationSelectMethod.Get:
					ConfigNavigationKey entityKey  = new ConfigNavigationKey();
					entityKey.Load(values);
					item = ConfigNavigationProvider.Get(entityKey);
					results = new TList<ConfigNavigation>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ConfigNavigationSelectMethod.GetAll:
                    results = ConfigNavigationProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ConfigNavigationSelectMethod.GetPaged:
					results = ConfigNavigationProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ConfigNavigationSelectMethod.Find:
					if ( FilterParameters != null )
						results = ConfigNavigationProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ConfigNavigationProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ConfigNavigationSelectMethod.GetByConfigNavigationId:
					_configNavigationId = ( values["ConfigNavigationId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ConfigNavigationId"], typeof(System.Int32)) : (int)0;
					item = ConfigNavigationProvider.GetByConfigNavigationId(_configNavigationId);
					results = new TList<ConfigNavigation>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case ConfigNavigationSelectMethod.GetByNavigateUrl:
					_navigateUrl_nullable = (System.String) EntityUtil.ChangeType(values["NavigateUrl"], typeof(System.String));
					item = ConfigNavigationProvider.GetByNavigateUrl(_navigateUrl_nullable);
					results = new TList<ConfigNavigation>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ConfigNavigationSelectMethod.Get || SelectMethod == ConfigNavigationSelectMethod.GetByConfigNavigationId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ConfigNavigation entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ConfigNavigationProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ConfigNavigation> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ConfigNavigationProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ConfigNavigationDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ConfigNavigationDataSource class.
	/// </summary>
	public class ConfigNavigationDataSourceDesigner : ProviderDataSourceDesigner<ConfigNavigation, ConfigNavigationKey>
	{
		/// <summary>
		/// Initializes a new instance of the ConfigNavigationDataSourceDesigner class.
		/// </summary>
		public ConfigNavigationDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigNavigationSelectMethod SelectMethod
		{
			get { return ((ConfigNavigationDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ConfigNavigationDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ConfigNavigationDataSourceActionList

	/// <summary>
	/// Supports the ConfigNavigationDataSourceDesigner class.
	/// </summary>
	internal class ConfigNavigationDataSourceActionList : DesignerActionList
	{
		private ConfigNavigationDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ConfigNavigationDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ConfigNavigationDataSourceActionList(ConfigNavigationDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigNavigationSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ConfigNavigationDataSourceActionList
	
	#endregion ConfigNavigationDataSourceDesigner
	
	#region ConfigNavigationSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ConfigNavigationDataSource.SelectMethod property.
	/// </summary>
	public enum ConfigNavigationSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByConfigNavigationId method.
		/// </summary>
		GetByConfigNavigationId,
		/// <summary>
		/// Represents the GetByNavigateUrl method.
		/// </summary>
		GetByNavigateUrl
	}
	
	#endregion ConfigNavigationSelectMethod

	#region ConfigNavigationFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationFilter : SqlFilter<ConfigNavigationColumn>
	{
	}
	
	#endregion ConfigNavigationFilter

	#region ConfigNavigationExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationExpressionBuilder : SqlExpressionBuilder<ConfigNavigationColumn>
	{
	}
	
	#endregion ConfigNavigationExpressionBuilder	

	#region ConfigNavigationProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ConfigNavigationChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigNavigation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigNavigationProperty : ChildEntityProperty<ConfigNavigationChildEntityTypes>
	{
	}
	
	#endregion ConfigNavigationProperty
}

