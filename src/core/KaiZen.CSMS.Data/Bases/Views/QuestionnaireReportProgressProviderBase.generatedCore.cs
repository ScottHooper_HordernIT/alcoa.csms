﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportProgressProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportProgressProviderBaseCore : EntityViewProviderBase<QuestionnaireReportProgress>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireReportProgress_FilterCompanies
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterCompanies()
		{
			return FilterCompanies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterCompanies(int start, int pageLength)
		{
			return FilterCompanies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterCompanies(TransactionManager transactionManager)
		{
			return FilterCompanies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet FilterCompanies(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireReportProgress_FilterType
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterType' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterType()
		{
			return FilterType(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterType' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterType(int start, int pageLength)
		{
			return FilterType(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterType' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterType(TransactionManager transactionManager)
		{
			return FilterType(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportProgress_FilterType' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet FilterType(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#endregion

		#region Helper Functions

        /*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportProgress&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportProgress&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportProgress&gt; Fill(DataSet dataSet, VList<QuestionnaireReportProgress> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportProgress>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportProgress&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportProgress>"/></returns>
		protected static VList&lt;QuestionnaireReportProgress&gt; Fill(DataTable dataTable, VList<QuestionnaireReportProgress> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportProgress c = new QuestionnaireReportProgress();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.InitialModifiedDate = (Convert.IsDBNull(row["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialModifiedDate"];
					c.SupplierContact = (Convert.IsDBNull(row["SupplierContact"]))?string.Empty:(System.String)row["SupplierContact"];
					c.SupplierContactEmail = (Convert.IsDBNull(row["SupplierContactEmail"]))?string.Empty:(System.String)row["SupplierContactEmail"];
					c.SupplierContactPhone = (Convert.IsDBNull(row["SupplierContactPhone"]))?string.Empty:(System.String)row["SupplierContactPhone"];
					c.SupplierContactEmailPhone = (Convert.IsDBNull(row["SupplierContactEmailPhone"]))?string.Empty:(System.String)row["SupplierContactEmailPhone"];
					c.Deactivated = (Convert.IsDBNull(row["Deactivated"]))?false:(System.Boolean?)row["Deactivated"];
					c.Deactivated2 = (Convert.IsDBNull(row["Deactivated2"]))?false:(System.Boolean)row["Deactivated2"];
					c.PresentlyWith = (Convert.IsDBNull(row["PresentlyWith"]))?string.Empty:(System.String)row["PresentlyWith"];
					c.PresentlyWithDetails = (Convert.IsDBNull(row["PresentlyWithDetails"]))?string.Empty:(System.String)row["PresentlyWithDetails"];
					c.DaysWith = (Convert.IsDBNull(row["DaysWith"]))?(int)0:(System.Int32?)row["DaysWith"];
					c.QmaLastModified = (Convert.IsDBNull(row["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)row["QMALastModified"];
					c.QvaLastModified = (Convert.IsDBNull(row["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)row["QVALastModified"];
					c.Type = (Convert.IsDBNull(row["Type"]))?string.Empty:(System.String)row["Type"];
					c.InitialSpa = (Convert.IsDBNull(row["InitialSPA"]))?string.Empty:(System.String)row["InitialSPA"];
					c.MainModifiedDate = (Convert.IsDBNull(row["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["MainModifiedDate"];
					c.MainModifiedByUserId = (Convert.IsDBNull(row["MainModifiedByUserId"]))?(int)0:(System.Int32?)row["MainModifiedByUserId"];
					c.MainModifiedByUser = (Convert.IsDBNull(row["MainModifiedByUser"]))?string.Empty:(System.String)row["MainModifiedByUser"];
					c.VerificationModifiedDate = (Convert.IsDBNull(row["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["VerificationModifiedDate"];
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.IsMainRequired = (Convert.IsDBNull(row["IsMainRequired"]))?false:(System.Boolean)row["IsMainRequired"];
					c.IsVerificationRequired = (Convert.IsDBNull(row["IsVerificationRequired"]))?false:(System.Boolean)row["IsVerificationRequired"];
					c.MainAssessmentRiskRating = (Convert.IsDBNull(row["MainAssessmentRiskRating"]))?string.Empty:(System.String)row["MainAssessmentRiskRating"];
                    c.FinalRiskRating = (Convert.IsDBNull(row["FinalRiskRating"]))?string.Empty:(System.String)row["FinalRiskRating"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.MainScorePoverall = (Convert.IsDBNull(row["MainScorePOverall"]))?(int)0:(System.Int32?)row["MainScorePOverall"];
					c.CompanyStatusId = (Convert.IsDBNull(row["CompanyStatusId"]))?(int)0:(System.Int32)row["CompanyStatusId"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.CompanyStatusName = (Convert.IsDBNull(row["CompanyStatusName"]))?string.Empty:(System.String)row["CompanyStatusName"];
					c.CreatedByUser = (Convert.IsDBNull(row["CreatedByUser"]))?string.Empty:(System.String)row["CreatedByUser"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.ModifiedByUser = (Convert.IsDBNull(row["ModifiedByUser"]))?string.Empty:(System.String)row["ModifiedByUser"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.InitialRiskAssessment = (Convert.IsDBNull(row["InitialRiskAssessment"]))?string.Empty:(System.String)row["InitialRiskAssessment"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.SafetyAssessorPhone = (Convert.IsDBNull(row["SafetyAssessorPhone"]))?string.Empty:(System.String)row["SafetyAssessorPhone"];
					c.SafetyAssessorSite = (Convert.IsDBNull(row["SafetyAssessorSite"]))?string.Empty:(System.String)row["SafetyAssessorSite"];
					c.SafetyAssessorRegion = (Convert.IsDBNull(row["SafetyAssessorRegion"]))?string.Empty:(System.String)row["SafetyAssessorRegion"];
					c.ProcurementContact = (Convert.IsDBNull(row["ProcurementContact"]))?string.Empty:(System.String)row["ProcurementContact"];
					c.ProcurementSite = (Convert.IsDBNull(row["ProcurementSite"]))?string.Empty:(System.String)row["ProcurementSite"];
					c.ProcurementRegion = (Convert.IsDBNull(row["ProcurementRegion"]))?string.Empty:(System.String)row["ProcurementRegion"];
					c.ProcurementContactUser = (Convert.IsDBNull(row["ProcurementContactUser"]))?string.Empty:(System.String)row["ProcurementContactUser"];
					c.ContractManager = (Convert.IsDBNull(row["ContractManager"]))?string.Empty:(System.String)row["ContractManager"];
					c.ContractManagerUser = (Convert.IsDBNull(row["ContractManagerUser"]))?string.Empty:(System.String)row["ContractManagerUser"];
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.ProcessNo = (Convert.IsDBNull(row["ProcessNo"]))?(int)0:(System.Int32?)row["ProcessNo"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.ActionName = (Convert.IsDBNull(row["ActionName"]))?string.Empty:(System.String)row["ActionName"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(row["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithSinceDaysCount"];
					c.RequestingCompanyId = (Convert.IsDBNull(row["RequestingCompanyId"]))?(int)0:(System.Int32?)row["RequestingCompanyId"];
					c.RequestingCompanyName = (Convert.IsDBNull(row["RequestingCompanyName"]))?string.Empty:(System.String)row["RequestingCompanyName"];
					c.ReasonContractor = (Convert.IsDBNull(row["ReasonContractor"]))?string.Empty:(System.String)row["ReasonContractor"];
					c.ServicesMain = (Convert.IsDBNull(row["ServicesMain"]))?string.Empty:(System.String)row["ServicesMain"];
					c.ServicesOther = (Convert.IsDBNull(row["ServicesOther"]))?string.Empty:(System.String)row["ServicesOther"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.IsReQualification = (Convert.IsDBNull(row["IsReQualification"]))?false:(System.Boolean)row["IsReQualification"];
					c.MainAssessmentValidTo = (Convert.IsDBNull(row["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentValidTo"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/

        ///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportProgress&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportProgress&gt;"/></returns>
		protected VList<QuestionnaireReportProgress> Fill(IDataReader reader, VList<QuestionnaireReportProgress> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportProgress entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportProgress>("QuestionnaireReportProgress",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportProgress();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportProgressColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.InitialModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.InitialModifiedDate)];
					//entity.InitialModifiedDate = (Convert.IsDBNull(reader["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialModifiedDate"];
					entity.SupplierContact = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContact)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContact)];
					//entity.SupplierContact = (Convert.IsDBNull(reader["SupplierContact"]))?string.Empty:(System.String)reader["SupplierContact"];
					entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContactEmail)];
					//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
					entity.SupplierContactPhone = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContactPhone)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContactPhone)];
					//entity.SupplierContactPhone = (Convert.IsDBNull(reader["SupplierContactPhone"]))?string.Empty:(System.String)reader["SupplierContactPhone"];
					entity.SupplierContactEmailPhone = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContactEmailPhone)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContactEmailPhone)];
					//entity.SupplierContactEmailPhone = (Convert.IsDBNull(reader["SupplierContactEmailPhone"]))?string.Empty:(System.String)reader["SupplierContactEmailPhone"];
					entity.Deactivated = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.Deactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireReportProgressColumn.Deactivated)];
					//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
					entity.Deactivated2 = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.Deactivated2)];
					//entity.Deactivated2 = (Convert.IsDBNull(reader["Deactivated2"]))?false:(System.Boolean)reader["Deactivated2"];
					entity.PresentlyWith = (System.String)reader[((int)QuestionnaireReportProgressColumn.PresentlyWith)];
					//entity.PresentlyWith = (Convert.IsDBNull(reader["PresentlyWith"]))?string.Empty:(System.String)reader["PresentlyWith"];
					entity.PresentlyWithDetails = (System.String)reader[((int)QuestionnaireReportProgressColumn.PresentlyWithDetails)];
					//entity.PresentlyWithDetails = (Convert.IsDBNull(reader["PresentlyWithDetails"]))?string.Empty:(System.String)reader["PresentlyWithDetails"];
					entity.DaysWith = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.DaysWith)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.DaysWith)];
					//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
					entity.QmaLastModified = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QmaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.QmaLastModified)];
					//entity.QmaLastModified = (Convert.IsDBNull(reader["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QMALastModified"];
					entity.QvaLastModified = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QvaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.QvaLastModified)];
					//entity.QvaLastModified = (Convert.IsDBNull(reader["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QVALastModified"];
					entity.Type = (System.String)reader[((int)QuestionnaireReportProgressColumn.Type)];
					//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
					entity.InitialSpa = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.InitialSpa)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.InitialSpa)];
					//entity.InitialSpa = (Convert.IsDBNull(reader["InitialSPA"]))?string.Empty:(System.String)reader["InitialSPA"];
					entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.MainModifiedDate)];
					//entity.MainModifiedDate = (Convert.IsDBNull(reader["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainModifiedDate"];
					entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.MainModifiedByUserId)];
					//entity.MainModifiedByUserId = (Convert.IsDBNull(reader["MainModifiedByUserId"]))?(int)0:(System.Int32?)reader["MainModifiedByUserId"];
					entity.MainModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.MainModifiedByUser)];
					//entity.MainModifiedByUser = (Convert.IsDBNull(reader["MainModifiedByUser"]))?string.Empty:(System.String)reader["MainModifiedByUser"];
					entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.VerificationModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.VerificationModifiedDate)];
					//entity.VerificationModifiedDate = (Convert.IsDBNull(reader["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationModifiedDate"];
					entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.IsMainRequired)];
					//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
					entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.IsVerificationRequired)];
					//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
					entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.MainAssessmentRiskRating)];
					//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
                    entity.FinalRiskRating = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.FinalRiskRating))) ? null : (System.String)reader[((int)QuestionnaireReportProgressColumn.FinalRiskRating)];
                    //entity.FinalRiskRating = (Convert.IsDBNull(reader["FinalRiskRating"]))?string.Empty:(System.String)reader["FinalRiskRating"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportProgressColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.MainScorePoverall)];
					//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
					entity.CompanyStatusId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.CompanyStatusId)];
					//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.CompanyStatusName = (System.String)reader[((int)QuestionnaireReportProgressColumn.CompanyStatusName)];
					//entity.CompanyStatusName = (Convert.IsDBNull(reader["CompanyStatusName"]))?string.Empty:(System.String)reader["CompanyStatusName"];
					entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.CreatedByUser)];
					//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ModifiedByUser)];
					//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.InitialRiskAssessment)];
					//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.SafetyAssessorPhone = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessorPhone)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessorPhone)];
					//entity.SafetyAssessorPhone = (Convert.IsDBNull(reader["SafetyAssessorPhone"]))?string.Empty:(System.String)reader["SafetyAssessorPhone"];
					entity.SafetyAssessorSite = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessorSite)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessorSite)];
					//entity.SafetyAssessorSite = (Convert.IsDBNull(reader["SafetyAssessorSite"]))?string.Empty:(System.String)reader["SafetyAssessorSite"];
					entity.SafetyAssessorRegion = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessorRegion)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessorRegion)];
					//entity.SafetyAssessorRegion = (Convert.IsDBNull(reader["SafetyAssessorRegion"]))?string.Empty:(System.String)reader["SafetyAssessorRegion"];
					entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementContact)];
					//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
					entity.ProcurementSite = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementSite)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementSite)];
					//entity.ProcurementSite = (Convert.IsDBNull(reader["ProcurementSite"]))?string.Empty:(System.String)reader["ProcurementSite"];
					entity.ProcurementRegion = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementRegion)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementRegion)];
					//entity.ProcurementRegion = (Convert.IsDBNull(reader["ProcurementRegion"]))?string.Empty:(System.String)reader["ProcurementRegion"];
					entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementContactUser)];
					//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
					entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ContractManager)];
					//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
					entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ContractManagerUser)];
					//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
					entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.ProcessNo)];
					//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
					entity.UserName = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.ActionName = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ActionName)];
					//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSinceDaysCount)];
					//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
					entity.RequestingCompanyId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.RequestingCompanyId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.RequestingCompanyId)];
					//entity.RequestingCompanyId = (Convert.IsDBNull(reader["RequestingCompanyId"]))?(int)0:(System.Int32?)reader["RequestingCompanyId"];
					entity.RequestingCompanyName = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.RequestingCompanyName)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.RequestingCompanyName)];
					//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
					entity.ReasonContractor = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ReasonContractor)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ReasonContractor)];
					//entity.ReasonContractor = (Convert.IsDBNull(reader["ReasonContractor"]))?string.Empty:(System.String)reader["ReasonContractor"];
					entity.ServicesMain = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ServicesMain)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ServicesMain)];
					//entity.ServicesMain = (Convert.IsDBNull(reader["ServicesMain"]))?string.Empty:(System.String)reader["ServicesMain"];
					entity.ServicesOther = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ServicesOther)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ServicesOther)];
					//entity.ServicesOther = (Convert.IsDBNull(reader["ServicesOther"]))?string.Empty:(System.String)reader["ServicesOther"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportProgressColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.IsReQualification)];
					//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
					entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.MainAssessmentValidTo)];
					//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportProgress"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportProgress"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportProgress entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportProgressColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.InitialModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.InitialModifiedDate)];
			//entity.InitialModifiedDate = (Convert.IsDBNull(reader["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialModifiedDate"];
			entity.SupplierContact = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContact)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContact)];
			//entity.SupplierContact = (Convert.IsDBNull(reader["SupplierContact"]))?string.Empty:(System.String)reader["SupplierContact"];
			entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContactEmail)];
			//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
			entity.SupplierContactPhone = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContactPhone)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContactPhone)];
			//entity.SupplierContactPhone = (Convert.IsDBNull(reader["SupplierContactPhone"]))?string.Empty:(System.String)reader["SupplierContactPhone"];
			entity.SupplierContactEmailPhone = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SupplierContactEmailPhone)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SupplierContactEmailPhone)];
			//entity.SupplierContactEmailPhone = (Convert.IsDBNull(reader["SupplierContactEmailPhone"]))?string.Empty:(System.String)reader["SupplierContactEmailPhone"];
			entity.Deactivated = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.Deactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireReportProgressColumn.Deactivated)];
			//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
			entity.Deactivated2 = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.Deactivated2)];
			//entity.Deactivated2 = (Convert.IsDBNull(reader["Deactivated2"]))?false:(System.Boolean)reader["Deactivated2"];
			entity.PresentlyWith = (System.String)reader[((int)QuestionnaireReportProgressColumn.PresentlyWith)];
			//entity.PresentlyWith = (Convert.IsDBNull(reader["PresentlyWith"]))?string.Empty:(System.String)reader["PresentlyWith"];
			entity.PresentlyWithDetails = (System.String)reader[((int)QuestionnaireReportProgressColumn.PresentlyWithDetails)];
			//entity.PresentlyWithDetails = (Convert.IsDBNull(reader["PresentlyWithDetails"]))?string.Empty:(System.String)reader["PresentlyWithDetails"];
			entity.DaysWith = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.DaysWith)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.DaysWith)];
			//entity.DaysWith = (Convert.IsDBNull(reader["DaysWith"]))?(int)0:(System.Int32?)reader["DaysWith"];
			entity.QmaLastModified = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QmaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.QmaLastModified)];
			//entity.QmaLastModified = (Convert.IsDBNull(reader["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QMALastModified"];
			entity.QvaLastModified = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QvaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.QvaLastModified)];
			//entity.QvaLastModified = (Convert.IsDBNull(reader["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QVALastModified"];
			entity.Type = (System.String)reader[((int)QuestionnaireReportProgressColumn.Type)];
			//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
			entity.InitialSpa = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.InitialSpa)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.InitialSpa)];
			//entity.InitialSpa = (Convert.IsDBNull(reader["InitialSPA"]))?string.Empty:(System.String)reader["InitialSPA"];
			entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.MainModifiedDate)];
			//entity.MainModifiedDate = (Convert.IsDBNull(reader["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainModifiedDate"];
			entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.MainModifiedByUserId)];
			//entity.MainModifiedByUserId = (Convert.IsDBNull(reader["MainModifiedByUserId"]))?(int)0:(System.Int32?)reader["MainModifiedByUserId"];
			entity.MainModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.MainModifiedByUser)];
			//entity.MainModifiedByUser = (Convert.IsDBNull(reader["MainModifiedByUser"]))?string.Empty:(System.String)reader["MainModifiedByUser"];
			entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.VerificationModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.VerificationModifiedDate)];
			//entity.VerificationModifiedDate = (Convert.IsDBNull(reader["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationModifiedDate"];
			entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.IsMainRequired)];
			//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
			entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.IsVerificationRequired)];
			//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
			entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.MainAssessmentRiskRating)];
			//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
            entity.FinalRiskRating = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.FinalRiskRating))) ? null : (System.String)reader[((int)QuestionnaireReportProgressColumn.FinalRiskRating)];
            //entity.FinalRiskRating = (Convert.IsDBNull(reader["FinalRiskRating"]))?string.Empty:(System.String)reader["FinalRiskRating"];
            entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportProgressColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.MainScorePoverall)];
			//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
			entity.CompanyStatusId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.CompanyStatusId)];
			//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.CompanyStatusName = (System.String)reader[((int)QuestionnaireReportProgressColumn.CompanyStatusName)];
			//entity.CompanyStatusName = (Convert.IsDBNull(reader["CompanyStatusName"]))?string.Empty:(System.String)reader["CompanyStatusName"];
			entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.CreatedByUser)];
			//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ModifiedByUser)];
			//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportProgressColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.InitialRiskAssessment)];
			//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.SafetyAssessorPhone = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessorPhone)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessorPhone)];
			//entity.SafetyAssessorPhone = (Convert.IsDBNull(reader["SafetyAssessorPhone"]))?string.Empty:(System.String)reader["SafetyAssessorPhone"];
			entity.SafetyAssessorSite = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessorSite)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessorSite)];
			//entity.SafetyAssessorSite = (Convert.IsDBNull(reader["SafetyAssessorSite"]))?string.Empty:(System.String)reader["SafetyAssessorSite"];
			entity.SafetyAssessorRegion = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.SafetyAssessorRegion)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.SafetyAssessorRegion)];
			//entity.SafetyAssessorRegion = (Convert.IsDBNull(reader["SafetyAssessorRegion"]))?string.Empty:(System.String)reader["SafetyAssessorRegion"];
			entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementContact)];
			//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
			entity.ProcurementSite = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementSite)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementSite)];
			//entity.ProcurementSite = (Convert.IsDBNull(reader["ProcurementSite"]))?string.Empty:(System.String)reader["ProcurementSite"];
			entity.ProcurementRegion = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementRegion)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementRegion)];
			//entity.ProcurementRegion = (Convert.IsDBNull(reader["ProcurementRegion"]))?string.Empty:(System.String)reader["ProcurementRegion"];
			entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ProcurementContactUser)];
			//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
			entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ContractManager)];
			//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
			entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ContractManagerUser)];
			//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.ProcessNo)];
			//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
			entity.UserName = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.ActionName = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ActionName)];
			//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.QuestionnairePresentlyWithSinceDaysCount)];
			//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
			entity.RequestingCompanyId = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.RequestingCompanyId)))?null:(System.Int32?)reader[((int)QuestionnaireReportProgressColumn.RequestingCompanyId)];
			//entity.RequestingCompanyId = (Convert.IsDBNull(reader["RequestingCompanyId"]))?(int)0:(System.Int32?)reader["RequestingCompanyId"];
			entity.RequestingCompanyName = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.RequestingCompanyName)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.RequestingCompanyName)];
			//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
			entity.ReasonContractor = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ReasonContractor)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ReasonContractor)];
			//entity.ReasonContractor = (Convert.IsDBNull(reader["ReasonContractor"]))?string.Empty:(System.String)reader["ReasonContractor"];
			entity.ServicesMain = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ServicesMain)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ServicesMain)];
			//entity.ServicesMain = (Convert.IsDBNull(reader["ServicesMain"]))?string.Empty:(System.String)reader["ServicesMain"];
			entity.ServicesOther = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.ServicesOther)))?null:(System.String)reader[((int)QuestionnaireReportProgressColumn.ServicesOther)];
			//entity.ServicesOther = (Convert.IsDBNull(reader["ServicesOther"]))?string.Empty:(System.String)reader["ServicesOther"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportProgressColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireReportProgressColumn.IsReQualification)];
			//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireReportProgressColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireReportProgressColumn.MainAssessmentValidTo)];
			//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
			reader.Close();
	
			entity.AcceptChanges();
		}

        /*
        /// <summary>
        /// Refreshes the <see cref="QuestionnaireReportProgress"/> object from the <see cref="DataSet"/>.
        /// </summary>
        /// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
        /// <param name="entity">The <see cref="QuestionnaireReportProgress"/> object.</param>
        protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportProgress entity)
        {
            DataRow dataRow = dataSet.Tables[0].Rows[0];
			
            entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
            entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
            entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
            entity.InitialModifiedDate = (Convert.IsDBNull(dataRow["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialModifiedDate"];
            entity.SupplierContact = (Convert.IsDBNull(dataRow["SupplierContact"]))?string.Empty:(System.String)dataRow["SupplierContact"];
            entity.SupplierContactEmail = (Convert.IsDBNull(dataRow["SupplierContactEmail"]))?string.Empty:(System.String)dataRow["SupplierContactEmail"];
            entity.SupplierContactPhone = (Convert.IsDBNull(dataRow["SupplierContactPhone"]))?string.Empty:(System.String)dataRow["SupplierContactPhone"];
            entity.SupplierContactEmailPhone = (Convert.IsDBNull(dataRow["SupplierContactEmailPhone"]))?string.Empty:(System.String)dataRow["SupplierContactEmailPhone"];
            entity.Deactivated = (Convert.IsDBNull(dataRow["Deactivated"]))?false:(System.Boolean?)dataRow["Deactivated"];
            entity.Deactivated2 = (Convert.IsDBNull(dataRow["Deactivated2"]))?false:(System.Boolean)dataRow["Deactivated2"];
            entity.PresentlyWith = (Convert.IsDBNull(dataRow["PresentlyWith"]))?string.Empty:(System.String)dataRow["PresentlyWith"];
            entity.PresentlyWithDetails = (Convert.IsDBNull(dataRow["PresentlyWithDetails"]))?string.Empty:(System.String)dataRow["PresentlyWithDetails"];
            entity.DaysWith = (Convert.IsDBNull(dataRow["DaysWith"]))?(int)0:(System.Int32?)dataRow["DaysWith"];
            entity.QmaLastModified = (Convert.IsDBNull(dataRow["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)dataRow["QMALastModified"];
            entity.QvaLastModified = (Convert.IsDBNull(dataRow["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)dataRow["QVALastModified"];
            entity.Type = (Convert.IsDBNull(dataRow["Type"]))?string.Empty:(System.String)dataRow["Type"];
            entity.InitialSpa = (Convert.IsDBNull(dataRow["InitialSPA"]))?string.Empty:(System.String)dataRow["InitialSPA"];
            entity.MainModifiedDate = (Convert.IsDBNull(dataRow["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainModifiedDate"];
            entity.MainModifiedByUserId = (Convert.IsDBNull(dataRow["MainModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["MainModifiedByUserId"];
            entity.MainModifiedByUser = (Convert.IsDBNull(dataRow["MainModifiedByUser"]))?string.Empty:(System.String)dataRow["MainModifiedByUser"];
            entity.VerificationModifiedDate = (Convert.IsDBNull(dataRow["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["VerificationModifiedDate"];
            entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
            entity.IsMainRequired = (Convert.IsDBNull(dataRow["IsMainRequired"]))?false:(System.Boolean)dataRow["IsMainRequired"];
            entity.IsVerificationRequired = (Convert.IsDBNull(dataRow["IsVerificationRequired"]))?false:(System.Boolean)dataRow["IsVerificationRequired"];
            entity.MainAssessmentRiskRating = (Convert.IsDBNull(dataRow["MainAssessmentRiskRating"]))?string.Empty:(System.String)dataRow["MainAssessmentRiskRating"];
            entity.FinalRiskRating = (Convert.IsDBNull(dataRow["FinalRiskRating"]))?string.Empty:(System.String)dataRow["FinalRiskRating"];
            entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
            entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
            entity.MainScorePoverall = (Convert.IsDBNull(dataRow["MainScorePOverall"]))?(int)0:(System.Int32?)dataRow["MainScorePOverall"];
            entity.CompanyStatusId = (Convert.IsDBNull(dataRow["CompanyStatusId"]))?(int)0:(System.Int32)dataRow["CompanyStatusId"];
            entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
            entity.CompanyStatusName = (Convert.IsDBNull(dataRow["CompanyStatusName"]))?string.Empty:(System.String)dataRow["CompanyStatusName"];
            entity.CreatedByUser = (Convert.IsDBNull(dataRow["CreatedByUser"]))?string.Empty:(System.String)dataRow["CreatedByUser"];
            entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
            entity.ModifiedByUser = (Convert.IsDBNull(dataRow["ModifiedByUser"]))?string.Empty:(System.String)dataRow["ModifiedByUser"];
            entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
            entity.InitialRiskAssessment = (Convert.IsDBNull(dataRow["InitialRiskAssessment"]))?string.Empty:(System.String)dataRow["InitialRiskAssessment"];
            entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
            entity.SafetyAssessorPhone = (Convert.IsDBNull(dataRow["SafetyAssessorPhone"]))?string.Empty:(System.String)dataRow["SafetyAssessorPhone"];
            entity.SafetyAssessorSite = (Convert.IsDBNull(dataRow["SafetyAssessorSite"]))?string.Empty:(System.String)dataRow["SafetyAssessorSite"];
            entity.SafetyAssessorRegion = (Convert.IsDBNull(dataRow["SafetyAssessorRegion"]))?string.Empty:(System.String)dataRow["SafetyAssessorRegion"];
            entity.ProcurementContact = (Convert.IsDBNull(dataRow["ProcurementContact"]))?string.Empty:(System.String)dataRow["ProcurementContact"];
            entity.ProcurementSite = (Convert.IsDBNull(dataRow["ProcurementSite"]))?string.Empty:(System.String)dataRow["ProcurementSite"];
            entity.ProcurementRegion = (Convert.IsDBNull(dataRow["ProcurementRegion"]))?string.Empty:(System.String)dataRow["ProcurementRegion"];
            entity.ProcurementContactUser = (Convert.IsDBNull(dataRow["ProcurementContactUser"]))?string.Empty:(System.String)dataRow["ProcurementContactUser"];
            entity.ContractManager = (Convert.IsDBNull(dataRow["ContractManager"]))?string.Empty:(System.String)dataRow["ContractManager"];
            entity.ContractManagerUser = (Convert.IsDBNull(dataRow["ContractManagerUser"]))?string.Empty:(System.String)dataRow["ContractManagerUser"];
            entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
            entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
            entity.ProcessNo = (Convert.IsDBNull(dataRow["ProcessNo"]))?(int)0:(System.Int32?)dataRow["ProcessNo"];
            entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
            entity.ActionName = (Convert.IsDBNull(dataRow["ActionName"]))?string.Empty:(System.String)dataRow["ActionName"];
            entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
            entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
            entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithSinceDaysCount"];
            entity.RequestingCompanyId = (Convert.IsDBNull(dataRow["RequestingCompanyId"]))?(int)0:(System.Int32?)dataRow["RequestingCompanyId"];
            entity.RequestingCompanyName = (Convert.IsDBNull(dataRow["RequestingCompanyName"]))?string.Empty:(System.String)dataRow["RequestingCompanyName"];
            entity.ReasonContractor = (Convert.IsDBNull(dataRow["ReasonContractor"]))?string.Empty:(System.String)dataRow["ReasonContractor"];
            entity.ServicesMain = (Convert.IsDBNull(dataRow["ServicesMain"]))?string.Empty:(System.String)dataRow["ServicesMain"];
            entity.ServicesOther = (Convert.IsDBNull(dataRow["ServicesOther"]))?string.Empty:(System.String)dataRow["ServicesOther"];
            entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
            entity.IsReQualification = (Convert.IsDBNull(dataRow["IsReQualification"]))?false:(System.Boolean)dataRow["IsReQualification"];
            entity.MainAssessmentValidTo = (Convert.IsDBNull(dataRow["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentValidTo"];
            entity.AcceptChanges();
        }
        */

        #endregion Helper Functions
    }//end class

	#region QuestionnaireReportProgressFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportProgressFilterBuilder : SqlFilterBuilder<QuestionnaireReportProgressColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressFilterBuilder class.
		/// </summary>
		public QuestionnaireReportProgressFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportProgressFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportProgressFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportProgressFilterBuilder

	#region QuestionnaireReportProgressParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportProgressParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportProgressColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressParameterBuilder class.
		/// </summary>
		public QuestionnaireReportProgressParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportProgressParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportProgressParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportProgressParameterBuilder
	
	#region QuestionnaireReportProgressSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportProgress"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportProgressSortBuilder : SqlSortBuilder<QuestionnaireReportProgressColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportProgressSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportProgressSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportProgressSortBuilder

} // end namespace
