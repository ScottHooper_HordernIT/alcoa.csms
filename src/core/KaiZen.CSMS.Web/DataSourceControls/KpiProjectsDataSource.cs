﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiProjectsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(KpiProjectsDataSourceDesigner))]
	public class KpiProjectsDataSource : ProviderDataSource<KpiProjects, KpiProjectsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsDataSource class.
		/// </summary>
		public KpiProjectsDataSource() : base(new KpiProjectsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiProjectsDataSourceView used by the KpiProjectsDataSource.
		/// </summary>
		protected KpiProjectsDataSourceView KpiProjectsView
		{
			get { return ( View as KpiProjectsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiProjectsDataSource control invokes to retrieve data.
		/// </summary>
		public KpiProjectsSelectMethod SelectMethod
		{
			get
			{
				KpiProjectsSelectMethod selectMethod = KpiProjectsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiProjectsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiProjectsDataSourceView class that is to be
		/// used by the KpiProjectsDataSource.
		/// </summary>
		/// <returns>An instance of the KpiProjectsDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiProjects, KpiProjectsKey> GetNewDataSourceView()
		{
			return new KpiProjectsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiProjectsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiProjectsDataSourceView : ProviderDataSourceView<KpiProjects, KpiProjectsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiProjectsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiProjectsDataSourceView(KpiProjectsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiProjectsDataSource KpiProjectsOwner
		{
			get { return Owner as KpiProjectsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal KpiProjectsSelectMethod SelectMethod
		{
			get { return KpiProjectsOwner.SelectMethod; }
			set { KpiProjectsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiProjectsService KpiProjectsProvider
		{
			get { return Provider as KpiProjectsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<KpiProjects> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<KpiProjects> results = null;
			KpiProjects item;
			count = 0;
			
			System.Int32 _kpiProjectId;
			System.Int32 _kpiId;
			System.String _projectTitle;

			switch ( SelectMethod )
			{
				case KpiProjectsSelectMethod.Get:
					KpiProjectsKey entityKey  = new KpiProjectsKey();
					entityKey.Load(values);
					item = KpiProjectsProvider.Get(entityKey);
					results = new TList<KpiProjects>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiProjectsSelectMethod.GetAll:
                    results = KpiProjectsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiProjectsSelectMethod.GetPaged:
					results = KpiProjectsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case KpiProjectsSelectMethod.Find:
					if ( FilterParameters != null )
						results = KpiProjectsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = KpiProjectsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case KpiProjectsSelectMethod.GetByKpiProjectId:
					_kpiProjectId = ( values["KpiProjectId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiProjectId"], typeof(System.Int32)) : (int)0;
					item = KpiProjectsProvider.GetByKpiProjectId(_kpiProjectId);
					results = new TList<KpiProjects>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case KpiProjectsSelectMethod.GetByKpiIdProjectTitle:
					_kpiId = ( values["KpiId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiId"], typeof(System.Int32)) : (int)0;
					_projectTitle = ( values["ProjectTitle"] != null ) ? (System.String) EntityUtil.ChangeType(values["ProjectTitle"], typeof(System.String)) : string.Empty;
					results = KpiProjectsProvider.GetByKpiIdProjectTitle(_kpiId, _projectTitle, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case KpiProjectsSelectMethod.GetByKpiId:
					_kpiId = ( values["KpiId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiId"], typeof(System.Int32)) : (int)0;
					results = KpiProjectsProvider.GetByKpiId(_kpiId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == KpiProjectsSelectMethod.Get || SelectMethod == KpiProjectsSelectMethod.GetByKpiProjectId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				KpiProjects entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					KpiProjectsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<KpiProjects> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			KpiProjectsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region KpiProjectsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiProjectsDataSource class.
	/// </summary>
	public class KpiProjectsDataSourceDesigner : ProviderDataSourceDesigner<KpiProjects, KpiProjectsKey>
	{
		/// <summary>
		/// Initializes a new instance of the KpiProjectsDataSourceDesigner class.
		/// </summary>
		public KpiProjectsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiProjectsSelectMethod SelectMethod
		{
			get { return ((KpiProjectsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new KpiProjectsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region KpiProjectsDataSourceActionList

	/// <summary>
	/// Supports the KpiProjectsDataSourceDesigner class.
	/// </summary>
	internal class KpiProjectsDataSourceActionList : DesignerActionList
	{
		private KpiProjectsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the KpiProjectsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public KpiProjectsDataSourceActionList(KpiProjectsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiProjectsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion KpiProjectsDataSourceActionList
	
	#endregion KpiProjectsDataSourceDesigner
	
	#region KpiProjectsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the KpiProjectsDataSource.SelectMethod property.
	/// </summary>
	public enum KpiProjectsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByKpiProjectId method.
		/// </summary>
		GetByKpiProjectId,
		/// <summary>
		/// Represents the GetByKpiIdProjectTitle method.
		/// </summary>
		GetByKpiIdProjectTitle,
		/// <summary>
		/// Represents the GetByKpiId method.
		/// </summary>
		GetByKpiId
	}
	
	#endregion KpiProjectsSelectMethod

	#region KpiProjectsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsFilter : SqlFilter<KpiProjectsColumn>
	{
	}
	
	#endregion KpiProjectsFilter

	#region KpiProjectsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsExpressionBuilder : SqlExpressionBuilder<KpiProjectsColumn>
	{
	}
	
	#endregion KpiProjectsExpressionBuilder	

	#region KpiProjectsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;KpiProjectsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjects"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectsProperty : ChildEntityProperty<KpiProjectsChildEntityTypes>
	{
	}
	
	#endregion KpiProjectsProperty
}

