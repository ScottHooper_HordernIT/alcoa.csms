﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_QuestionnaireRationale" Codebehind="QuestionnaireRationale.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">  
         function doClose(e) // note: takes the event as an arg (IE doesn't)  
         {  
             if (!e) e = window.event; // fix IE  

             if (e.keyCode) // IE  
             {  
                 if (e.keyCode == "27") window.close();  
             }  
             else if (e.charCode) // Netscape/Firefox/Opera  
             {  
                 if (e.keyCode == "27") window.close();  
             } 
         }
         document.onkeydown = doClose;  
 </script> 

    <div align="center">
        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" Text="Close (ESC)"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        </dx:ASPxButton>
        <br />
    </div>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
</asp:Content>
