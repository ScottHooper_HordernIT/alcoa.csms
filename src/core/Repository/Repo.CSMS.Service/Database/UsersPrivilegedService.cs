﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersPrivilegedService
    {
        UsersPrivileged Get(Expression<Func<UsersPrivileged, bool>> where, List<Expression<Func<UsersPrivileged, object>>> includes);
        List<UsersPrivileged> GetMany(int? top, Expression<Func<UsersPrivileged, bool>> where, Func<IQueryable<UsersPrivileged>, IOrderedQueryable<UsersPrivileged>> orderBy, List<Expression<Func<UsersPrivileged, object>>> includes);
        List<UsersPrivileged> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersPrivileged, bool>> where, Func<IQueryable<UsersPrivileged>, IOrderedQueryable<UsersPrivileged>> orderBy, List<Expression<Func<UsersPrivileged, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsersPrivileged, bool>> where, List<Expression<Func<UsersPrivileged, object>>> includes);
        void Insert(UsersPrivileged item, bool saveChanges = true);
        void Update(UsersPrivileged item, bool saveChanges = true);
        void Delete(UsersPrivileged item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersPrivilegedService : IUsersPrivilegedService
    {
        private readonly IRepository<UsersPrivileged> _repository;

        public UsersPrivilegedService(IRepository<UsersPrivileged> repository)
        {
            this._repository = repository;
        }

        public UsersPrivileged Get(Expression<Func<UsersPrivileged, bool>> where, List<Expression<Func<UsersPrivileged, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersPrivileged> GetMany(int? top, Expression<Func<UsersPrivileged, bool>> where, Func<IQueryable<UsersPrivileged>, IOrderedQueryable<UsersPrivileged>> orderBy, List<Expression<Func<UsersPrivileged, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersPrivileged> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersPrivileged, bool>> where, Func<IQueryable<UsersPrivileged>, IOrderedQueryable<UsersPrivileged>> orderBy, List<Expression<Func<UsersPrivileged, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersPrivileged, bool>> where, List<Expression<Func<UsersPrivileged, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersPrivileged item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersPrivileged");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersPrivileged item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersPrivileged");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersPrivileged item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersPrivileged");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
