﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(KpiDataSourceDesigner))]
	public class KpiDataSource : ProviderDataSource<Kpi, KpiKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiDataSource class.
		/// </summary>
		public KpiDataSource() : base(new KpiService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiDataSourceView used by the KpiDataSource.
		/// </summary>
		protected KpiDataSourceView KpiView
		{
			get { return ( View as KpiDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiDataSource control invokes to retrieve data.
		/// </summary>
		public KpiSelectMethod SelectMethod
		{
			get
			{
				KpiSelectMethod selectMethod = KpiSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiDataSourceView class that is to be
		/// used by the KpiDataSource.
		/// </summary>
		/// <returns>An instance of the KpiDataSourceView class.</returns>
		protected override BaseDataSourceView<Kpi, KpiKey> GetNewDataSourceView()
		{
			return new KpiDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiDataSourceView : ProviderDataSourceView<Kpi, KpiKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiDataSourceView(KpiDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiDataSource KpiOwner
		{
			get { return Owner as KpiDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal KpiSelectMethod SelectMethod
		{
			get { return KpiOwner.SelectMethod; }
			set { KpiOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiService KpiProvider
		{
			get { return Provider as KpiService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Kpi> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Kpi> results = null;
			Kpi item;
			count = 0;
			
			System.Int32 _kpiId;
			System.Int32 _companyId;
			System.Int32 _siteId;
			System.DateTime _kpiDateTime;
			System.Int32 _modifiedbyUserId;

			switch ( SelectMethod )
			{
				case KpiSelectMethod.Get:
					KpiKey entityKey  = new KpiKey();
					entityKey.Load(values);
					item = KpiProvider.Get(entityKey);
					results = new TList<Kpi>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiSelectMethod.GetAll:
                    results = KpiProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiSelectMethod.GetPaged:
					results = KpiProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case KpiSelectMethod.Find:
					if ( FilterParameters != null )
						results = KpiProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = KpiProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case KpiSelectMethod.GetByKpiId:
					_kpiId = ( values["KpiId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiId"], typeof(System.Int32)) : (int)0;
					item = KpiProvider.GetByKpiId(_kpiId);
					results = new TList<Kpi>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case KpiSelectMethod.GetByCompanyIdSiteIdKpiDateTime:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_kpiDateTime = ( values["KpiDateTime"] != null ) ? (System.DateTime) EntityUtil.ChangeType(values["KpiDateTime"], typeof(System.DateTime)) : DateTime.MinValue;
					item = KpiProvider.GetByCompanyIdSiteIdKpiDateTime(_companyId, _siteId, _kpiDateTime);
					results = new TList<Kpi>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiSelectMethod.GetByCompanyIdSiteId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = KpiProvider.GetByCompanyIdSiteId(_companyId, _siteId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case KpiSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = KpiProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case KpiSelectMethod.GetByModifiedbyUserId:
					_modifiedbyUserId = ( values["ModifiedbyUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedbyUserId"], typeof(System.Int32)) : (int)0;
					results = KpiProvider.GetByModifiedbyUserId(_modifiedbyUserId, this.StartIndex, this.PageSize, out count);
					break;
				case KpiSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = KpiProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == KpiSelectMethod.Get || SelectMethod == KpiSelectMethod.GetByKpiId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Kpi entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					KpiProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Kpi> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			KpiProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region KpiDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiDataSource class.
	/// </summary>
	public class KpiDataSourceDesigner : ProviderDataSourceDesigner<Kpi, KpiKey>
	{
		/// <summary>
		/// Initializes a new instance of the KpiDataSourceDesigner class.
		/// </summary>
		public KpiDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiSelectMethod SelectMethod
		{
			get { return ((KpiDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new KpiDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region KpiDataSourceActionList

	/// <summary>
	/// Supports the KpiDataSourceDesigner class.
	/// </summary>
	internal class KpiDataSourceActionList : DesignerActionList
	{
		private KpiDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the KpiDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public KpiDataSourceActionList(KpiDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion KpiDataSourceActionList
	
	#endregion KpiDataSourceDesigner
	
	#region KpiSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the KpiDataSource.SelectMethod property.
	/// </summary>
	public enum KpiSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByKpiId method.
		/// </summary>
		GetByKpiId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdKpiDateTime method.
		/// </summary>
		GetByCompanyIdSiteIdKpiDateTime,
		/// <summary>
		/// Represents the GetByCompanyIdSiteId method.
		/// </summary>
		GetByCompanyIdSiteId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByModifiedbyUserId method.
		/// </summary>
		GetByModifiedbyUserId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion KpiSelectMethod

	#region KpiFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiFilter : SqlFilter<KpiColumn>
	{
	}
	
	#endregion KpiFilter

	#region KpiExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiExpressionBuilder : SqlExpressionBuilder<KpiColumn>
	{
	}
	
	#endregion KpiExpressionBuilder	

	#region KpiProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;KpiChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Kpi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProperty : ChildEntityProperty<KpiChildEntityTypes>
	{
	}
	
	#endregion KpiProperty
}

