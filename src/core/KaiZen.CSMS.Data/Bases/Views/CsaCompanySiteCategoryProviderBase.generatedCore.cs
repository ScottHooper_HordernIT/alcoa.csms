﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsaCompanySiteCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CsaCompanySiteCategoryProviderBaseCore : EntityViewProviderBase<CsaCompanySiteCategory>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CsaCompanySiteCategory&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CsaCompanySiteCategory&gt;"/></returns>
		protected static VList&lt;CsaCompanySiteCategory&gt; Fill(DataSet dataSet, VList<CsaCompanySiteCategory> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CsaCompanySiteCategory>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CsaCompanySiteCategory&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CsaCompanySiteCategory>"/></returns>
		protected static VList&lt;CsaCompanySiteCategory&gt; Fill(DataTable dataTable, VList<CsaCompanySiteCategory> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CsaCompanySiteCategory c = new CsaCompanySiteCategory();
					c.CsaId = (Convert.IsDBNull(row["CSAId"]))?(int)0:(System.Int32)row["CSAId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.DateAdded = (Convert.IsDBNull(row["DateAdded"]))?DateTime.MinValue:(System.DateTime)row["DateAdded"];
					c.DateModified = (Convert.IsDBNull(row["DateModified"]))?DateTime.MinValue:(System.DateTime)row["DateModified"];
					c.QtrId = (Convert.IsDBNull(row["QtrId"]))?(int)0:(System.Int32)row["QtrId"];
					c.Year = (Convert.IsDBNull(row["Year"]))?(long)0:(System.Int64)row["Year"];
					c.TotalPossibleScore = (Convert.IsDBNull(row["TotalPossibleScore"]))?(int)0:(System.Int32?)row["TotalPossibleScore"];
					c.TotalActualScore = (Convert.IsDBNull(row["TotalActualScore"]))?(int)0:(System.Int32?)row["TotalActualScore"];
					c.TotalRating = (Convert.IsDBNull(row["TotalRating"]))?(int)0:(System.Int32?)row["TotalRating"];
					c.CompanySiteCategoryId = (Convert.IsDBNull(row["CompanySiteCategoryId"]))?(int)0:(System.Int32?)row["CompanySiteCategoryId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CsaCompanySiteCategory&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CsaCompanySiteCategory&gt;"/></returns>
		protected VList<CsaCompanySiteCategory> Fill(IDataReader reader, VList<CsaCompanySiteCategory> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CsaCompanySiteCategory entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CsaCompanySiteCategory>("CsaCompanySiteCategory",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CsaCompanySiteCategory();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CsaId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.CsaId)];
					//entity.CsaId = (Convert.IsDBNull(reader["CSAId"]))?(int)0:(System.Int32)reader["CSAId"];
					entity.CompanyId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.SiteId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.CreatedByUserId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.DateAdded = (System.DateTime)reader[((int)CsaCompanySiteCategoryColumn.DateAdded)];
					//entity.DateAdded = (Convert.IsDBNull(reader["DateAdded"]))?DateTime.MinValue:(System.DateTime)reader["DateAdded"];
					entity.DateModified = (System.DateTime)reader[((int)CsaCompanySiteCategoryColumn.DateModified)];
					//entity.DateModified = (Convert.IsDBNull(reader["DateModified"]))?DateTime.MinValue:(System.DateTime)reader["DateModified"];
					entity.QtrId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.QtrId)];
					//entity.QtrId = (Convert.IsDBNull(reader["QtrId"]))?(int)0:(System.Int32)reader["QtrId"];
					entity.Year = (System.Int64)reader[((int)CsaCompanySiteCategoryColumn.Year)];
					//entity.Year = (Convert.IsDBNull(reader["Year"]))?(long)0:(System.Int64)reader["Year"];
					entity.TotalPossibleScore = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.TotalPossibleScore)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.TotalPossibleScore)];
					//entity.TotalPossibleScore = (Convert.IsDBNull(reader["TotalPossibleScore"]))?(int)0:(System.Int32?)reader["TotalPossibleScore"];
					entity.TotalActualScore = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.TotalActualScore)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.TotalActualScore)];
					//entity.TotalActualScore = (Convert.IsDBNull(reader["TotalActualScore"]))?(int)0:(System.Int32?)reader["TotalActualScore"];
					entity.TotalRating = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.TotalRating)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.TotalRating)];
					//entity.TotalRating = (Convert.IsDBNull(reader["TotalRating"]))?(int)0:(System.Int32?)reader["TotalRating"];
					entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.CompanySiteCategoryId)];
					//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CsaCompanySiteCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CsaCompanySiteCategory"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CsaCompanySiteCategory entity)
		{
			reader.Read();
			entity.CsaId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.CsaId)];
			//entity.CsaId = (Convert.IsDBNull(reader["CSAId"]))?(int)0:(System.Int32)reader["CSAId"];
			entity.CompanyId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.SiteId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.CreatedByUserId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.DateAdded = (System.DateTime)reader[((int)CsaCompanySiteCategoryColumn.DateAdded)];
			//entity.DateAdded = (Convert.IsDBNull(reader["DateAdded"]))?DateTime.MinValue:(System.DateTime)reader["DateAdded"];
			entity.DateModified = (System.DateTime)reader[((int)CsaCompanySiteCategoryColumn.DateModified)];
			//entity.DateModified = (Convert.IsDBNull(reader["DateModified"]))?DateTime.MinValue:(System.DateTime)reader["DateModified"];
			entity.QtrId = (System.Int32)reader[((int)CsaCompanySiteCategoryColumn.QtrId)];
			//entity.QtrId = (Convert.IsDBNull(reader["QtrId"]))?(int)0:(System.Int32)reader["QtrId"];
			entity.Year = (System.Int64)reader[((int)CsaCompanySiteCategoryColumn.Year)];
			//entity.Year = (Convert.IsDBNull(reader["Year"]))?(long)0:(System.Int64)reader["Year"];
			entity.TotalPossibleScore = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.TotalPossibleScore)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.TotalPossibleScore)];
			//entity.TotalPossibleScore = (Convert.IsDBNull(reader["TotalPossibleScore"]))?(int)0:(System.Int32?)reader["TotalPossibleScore"];
			entity.TotalActualScore = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.TotalActualScore)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.TotalActualScore)];
			//entity.TotalActualScore = (Convert.IsDBNull(reader["TotalActualScore"]))?(int)0:(System.Int32?)reader["TotalActualScore"];
			entity.TotalRating = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.TotalRating)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.TotalRating)];
			//entity.TotalRating = (Convert.IsDBNull(reader["TotalRating"]))?(int)0:(System.Int32?)reader["TotalRating"];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CsaCompanySiteCategoryColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CsaCompanySiteCategoryColumn.CompanySiteCategoryId)];
			//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CsaCompanySiteCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CsaCompanySiteCategory"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CsaCompanySiteCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsaId = (Convert.IsDBNull(dataRow["CSAId"]))?(int)0:(System.Int32)dataRow["CSAId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.DateAdded = (Convert.IsDBNull(dataRow["DateAdded"]))?DateTime.MinValue:(System.DateTime)dataRow["DateAdded"];
			entity.DateModified = (Convert.IsDBNull(dataRow["DateModified"]))?DateTime.MinValue:(System.DateTime)dataRow["DateModified"];
			entity.QtrId = (Convert.IsDBNull(dataRow["QtrId"]))?(int)0:(System.Int32)dataRow["QtrId"];
			entity.Year = (Convert.IsDBNull(dataRow["Year"]))?(long)0:(System.Int64)dataRow["Year"];
			entity.TotalPossibleScore = (Convert.IsDBNull(dataRow["TotalPossibleScore"]))?(int)0:(System.Int32?)dataRow["TotalPossibleScore"];
			entity.TotalActualScore = (Convert.IsDBNull(dataRow["TotalActualScore"]))?(int)0:(System.Int32?)dataRow["TotalActualScore"];
			entity.TotalRating = (Convert.IsDBNull(dataRow["TotalRating"]))?(int)0:(System.Int32?)dataRow["TotalRating"];
			entity.CompanySiteCategoryId = (Convert.IsDBNull(dataRow["CompanySiteCategoryId"]))?(int)0:(System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CsaCompanySiteCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaCompanySiteCategoryFilterBuilder : SqlFilterBuilder<CsaCompanySiteCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryFilterBuilder class.
		/// </summary>
		public CsaCompanySiteCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaCompanySiteCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaCompanySiteCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaCompanySiteCategoryFilterBuilder

	#region CsaCompanySiteCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaCompanySiteCategoryParameterBuilder : ParameterizedSqlFilterBuilder<CsaCompanySiteCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryParameterBuilder class.
		/// </summary>
		public CsaCompanySiteCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaCompanySiteCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaCompanySiteCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaCompanySiteCategoryParameterBuilder
	
	#region CsaCompanySiteCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaCompanySiteCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsaCompanySiteCategorySortBuilder : SqlSortBuilder<CsaCompanySiteCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaCompanySiteCategorySqlSortBuilder class.
		/// </summary>
		public CsaCompanySiteCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsaCompanySiteCategorySortBuilder

} // end namespace
