﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CompaniesEhsimsMapSitesEhsimsIhsListDataSourceDesigner))]
	public class CompaniesEhsimsMapSitesEhsimsIhsListDataSource : ReadOnlyDataSource<CompaniesEhsimsMapSitesEhsimsIhsList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListDataSource class.
		/// </summary>
		public CompaniesEhsimsMapSitesEhsimsIhsListDataSource() : base(new CompaniesEhsimsMapSitesEhsimsIhsListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView used by the CompaniesEhsimsMapSitesEhsimsIhsListDataSource.
		/// </summary>
		protected CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView CompaniesEhsimsMapSitesEhsimsIhsListView
		{
			get { return ( View as CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView class that is to be
		/// used by the CompaniesEhsimsMapSitesEhsimsIhsListDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesEhsimsMapSitesEhsimsIhsList, Object> GetNewDataSourceView()
		{
			return new CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesEhsimsMapSitesEhsimsIhsListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView : ReadOnlyDataSourceView<CompaniesEhsimsMapSitesEhsimsIhsList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesEhsimsMapSitesEhsimsIhsListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesEhsimsMapSitesEhsimsIhsListDataSourceView(CompaniesEhsimsMapSitesEhsimsIhsListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesEhsimsMapSitesEhsimsIhsListDataSource CompaniesEhsimsMapSitesEhsimsIhsListOwner
		{
			get { return Owner as CompaniesEhsimsMapSitesEhsimsIhsListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesEhsimsMapSitesEhsimsIhsListService CompaniesEhsimsMapSitesEhsimsIhsListProvider
		{
			get { return Provider as CompaniesEhsimsMapSitesEhsimsIhsListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CompaniesEhsimsMapSitesEhsimsIhsListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesEhsimsMapSitesEhsimsIhsListDataSource class.
	/// </summary>
	public class CompaniesEhsimsMapSitesEhsimsIhsListDataSourceDesigner : ReadOnlyDataSourceDesigner<CompaniesEhsimsMapSitesEhsimsIhsList>
	{
	}

	#endregion CompaniesEhsimsMapSitesEhsimsIhsListDataSourceDesigner

	#region CompaniesEhsimsMapSitesEhsimsIhsListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapSitesEhsimsIhsListFilter : SqlFilter<CompaniesEhsimsMapSitesEhsimsIhsListColumn>
	{
	}

	#endregion CompaniesEhsimsMapSitesEhsimsIhsListFilter

	#region CompaniesEhsimsMapSitesEhsimsIhsListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsimsMapSitesEhsimsIhsListExpressionBuilder : SqlExpressionBuilder<CompaniesEhsimsMapSitesEhsimsIhsListColumn>
	{
	}
	
	#endregion CompaniesEhsimsMapSitesEhsimsIhsListExpressionBuilder		
}

