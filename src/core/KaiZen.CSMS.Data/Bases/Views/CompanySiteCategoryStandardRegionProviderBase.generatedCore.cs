﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryStandardRegionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompanySiteCategoryStandardRegionProviderBaseCore : EntityViewProviderBase<CompanySiteCategoryStandardRegion>
	{
		#region Custom Methods
		
		
		#region _CompanySiteCategoryStandardRegion_GetByRegionIdCompanySiteCategoryId
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByRegionIdCompanySiteCategoryId(System.Int32? regionId, System.Int32? companySiteCategoryId)
		{
			return GetByRegionIdCompanySiteCategoryId(null, 0, int.MaxValue , regionId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByRegionIdCompanySiteCategoryId(int start, int pageLength, System.Int32? regionId, System.Int32? companySiteCategoryId)
		{
			return GetByRegionIdCompanySiteCategoryId(null, start, pageLength , regionId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByRegionIdCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? regionId, System.Int32? companySiteCategoryId)
		{
			return GetByRegionIdCompanySiteCategoryId(transactionManager, 0, int.MaxValue , regionId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetByRegionIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByRegionIdCompanySiteCategoryId(TransactionManager transactionManager, int start, int pageLength, System.Int32? regionId, System.Int32? companySiteCategoryId);
		
		#endregion

		
		#region _CompanySiteCategoryStandardRegion_NonComplianceTable
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_NonComplianceTable' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NonComplianceTable(System.Int32? companyId, System.Int32? year)
		{
			return NonComplianceTable(null, 0, int.MaxValue , companyId, year);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_NonComplianceTable' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NonComplianceTable(int start, int pageLength, System.Int32? companyId, System.Int32? year)
		{
			return NonComplianceTable(null, start, pageLength , companyId, year);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_NonComplianceTable' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NonComplianceTable(TransactionManager transactionManager, System.Int32? companyId, System.Int32? year)
		{
			return NonComplianceTable(transactionManager, 0, int.MaxValue , companyId, year);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_NonComplianceTable' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet NonComplianceTable(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? year);
		
		#endregion

		
		#region _CompanySiteCategoryStandardRegion_GetBySiteIdCompanySiteCategoryId
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetBySiteIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteIdCompanySiteCategoryId(System.Int32? siteId, System.Int32? companySiteCategoryId)
		{
			return GetBySiteIdCompanySiteCategoryId(null, 0, int.MaxValue , siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetBySiteIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteIdCompanySiteCategoryId(int start, int pageLength, System.Int32? siteId, System.Int32? companySiteCategoryId)
		{
			return GetBySiteIdCompanySiteCategoryId(null, start, pageLength , siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetBySiteIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetBySiteIdCompanySiteCategoryId(TransactionManager transactionManager, System.Int32? siteId, System.Int32? companySiteCategoryId)
		{
			return GetBySiteIdCompanySiteCategoryId(transactionManager, 0, int.MaxValue , siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CompanySiteCategoryStandardRegion_GetBySiteIdCompanySiteCategoryId' stored procedure. 
		/// </summary>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetBySiteIdCompanySiteCategoryId(TransactionManager transactionManager, int start, int pageLength, System.Int32? siteId, System.Int32? companySiteCategoryId);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompanySiteCategoryStandardRegion&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompanySiteCategoryStandardRegion&gt;"/></returns>
		protected static VList&lt;CompanySiteCategoryStandardRegion&gt; Fill(DataSet dataSet, VList<CompanySiteCategoryStandardRegion> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompanySiteCategoryStandardRegion>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompanySiteCategoryStandardRegion&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompanySiteCategoryStandardRegion>"/></returns>
		protected static VList&lt;CompanySiteCategoryStandardRegion&gt; Fill(DataTable dataTable, VList<CompanySiteCategoryStandardRegion> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompanySiteCategoryStandardRegion c = new CompanySiteCategoryStandardRegion();
					c.CompanySiteCategoryStandardId = (Convert.IsDBNull(row["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)row["CompanySiteCategoryStandardId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.CompanySiteCategoryId = (Convert.IsDBNull(row["CompanySiteCategoryId"]))?(int)0:(System.Int32?)row["CompanySiteCategoryId"];
					c.LocationSponsorUserId = (Convert.IsDBNull(row["LocationSponsorUserId"]))?(int)0:(System.Int32?)row["LocationSponsorUserId"];
					c.Approved = (Convert.IsDBNull(row["Approved"]))?false:(System.Boolean?)row["Approved"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.RegionId = (Convert.IsDBNull(row["RegionId"]))?(int)0:(System.Int32)row["RegionId"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.SiteAbbrev = (Convert.IsDBNull(row["SiteAbbrev"]))?string.Empty:(System.String)row["SiteAbbrev"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompanySiteCategoryStandardRegion&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompanySiteCategoryStandardRegion&gt;"/></returns>
		protected VList<CompanySiteCategoryStandardRegion> Fill(IDataReader reader, VList<CompanySiteCategoryStandardRegion> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompanySiteCategoryStandardRegion entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompanySiteCategoryStandardRegion>("CompanySiteCategoryStandardRegion",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompanySiteCategoryStandardRegion();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryStandardId)];
					//entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(reader["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)reader["CompanySiteCategoryStandardId"];
					entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryId)];
					//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
					entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.LocationSponsorUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardRegionColumn.LocationSponsorUserId)];
					//entity.LocationSponsorUserId = (Convert.IsDBNull(reader["LocationSponsorUserId"]))?(int)0:(System.Int32?)reader["LocationSponsorUserId"];
					entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.Approved)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardRegionColumn.Approved)];
					//entity.Approved = (Convert.IsDBNull(reader["Approved"]))?false:(System.Boolean?)reader["Approved"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardRegionColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardRegionColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedDate = (System.DateTime)reader[((int)CompanySiteCategoryStandardRegionColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.RegionId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.RegionId)];
					//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32)reader["RegionId"];
					entity.SiteName = (System.String)reader[((int)CompanySiteCategoryStandardRegionColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.SiteAbbrev = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompanySiteCategoryStandardRegionColumn.SiteAbbrev)];
					//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompanySiteCategoryStandardRegion"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompanySiteCategoryStandardRegion"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompanySiteCategoryStandardRegion entity)
		{
			reader.Read();
			entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryStandardId)];
			//entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(reader["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)reader["CompanySiteCategoryStandardId"];
			entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardRegionColumn.CompanySiteCategoryId)];
			//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
			entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.LocationSponsorUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardRegionColumn.LocationSponsorUserId)];
			//entity.LocationSponsorUserId = (Convert.IsDBNull(reader["LocationSponsorUserId"]))?(int)0:(System.Int32?)reader["LocationSponsorUserId"];
			entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.Approved)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardRegionColumn.Approved)];
			//entity.Approved = (Convert.IsDBNull(reader["Approved"]))?false:(System.Boolean?)reader["Approved"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardRegionColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardRegionColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)reader[((int)CompanySiteCategoryStandardRegionColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.RegionId = (System.Int32)reader[((int)CompanySiteCategoryStandardRegionColumn.RegionId)];
			//entity.RegionId = (Convert.IsDBNull(reader["RegionId"]))?(int)0:(System.Int32)reader["RegionId"];
			entity.SiteName = (System.String)reader[((int)CompanySiteCategoryStandardRegionColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.SiteAbbrev = (reader.IsDBNull(((int)CompanySiteCategoryStandardRegionColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompanySiteCategoryStandardRegionColumn.SiteAbbrev)];
			//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompanySiteCategoryStandardRegion"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompanySiteCategoryStandardRegion"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompanySiteCategoryStandardRegion entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(dataRow["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)dataRow["CompanySiteCategoryStandardId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.CompanySiteCategoryId = (Convert.IsDBNull(dataRow["CompanySiteCategoryId"]))?(int)0:(System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.LocationSponsorUserId = (Convert.IsDBNull(dataRow["LocationSponsorUserId"]))?(int)0:(System.Int32?)dataRow["LocationSponsorUserId"];
			entity.Approved = (Convert.IsDBNull(dataRow["Approved"]))?false:(System.Boolean?)dataRow["Approved"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.RegionId = (Convert.IsDBNull(dataRow["RegionId"]))?(int)0:(System.Int32)dataRow["RegionId"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.SiteAbbrev = (Convert.IsDBNull(dataRow["SiteAbbrev"]))?string.Empty:(System.String)dataRow["SiteAbbrev"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompanySiteCategoryStandardRegionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardRegionFilterBuilder : SqlFilterBuilder<CompanySiteCategoryStandardRegionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionFilterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardRegionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardRegionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardRegionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardRegionFilterBuilder

	#region CompanySiteCategoryStandardRegionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardRegionParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryStandardRegionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardRegionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardRegionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardRegionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardRegionParameterBuilder
	
	#region CompanySiteCategoryStandardRegionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardRegion"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategoryStandardRegionSortBuilder : SqlSortBuilder<CompanySiteCategoryStandardRegionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardRegionSqlSortBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardRegionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategoryStandardRegionSortBuilder

} // end namespace
