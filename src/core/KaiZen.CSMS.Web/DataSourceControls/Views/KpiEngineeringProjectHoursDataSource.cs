﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiEngineeringProjectHoursProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(KpiEngineeringProjectHoursDataSourceDesigner))]
	public class KpiEngineeringProjectHoursDataSource : ReadOnlyDataSource<KpiEngineeringProjectHours>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursDataSource class.
		/// </summary>
		public KpiEngineeringProjectHoursDataSource() : base(new KpiEngineeringProjectHoursService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiEngineeringProjectHoursDataSourceView used by the KpiEngineeringProjectHoursDataSource.
		/// </summary>
		protected KpiEngineeringProjectHoursDataSourceView KpiEngineeringProjectHoursView
		{
			get { return ( View as KpiEngineeringProjectHoursDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiEngineeringProjectHoursDataSourceView class that is to be
		/// used by the KpiEngineeringProjectHoursDataSource.
		/// </summary>
		/// <returns>An instance of the KpiEngineeringProjectHoursDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiEngineeringProjectHours, Object> GetNewDataSourceView()
		{
			return new KpiEngineeringProjectHoursDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiEngineeringProjectHoursDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiEngineeringProjectHoursDataSourceView : ReadOnlyDataSourceView<KpiEngineeringProjectHours>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiEngineeringProjectHoursDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiEngineeringProjectHoursDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiEngineeringProjectHoursDataSourceView(KpiEngineeringProjectHoursDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiEngineeringProjectHoursDataSource KpiEngineeringProjectHoursOwner
		{
			get { return Owner as KpiEngineeringProjectHoursDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiEngineeringProjectHoursService KpiEngineeringProjectHoursProvider
		{
			get { return Provider as KpiEngineeringProjectHoursService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region KpiEngineeringProjectHoursDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiEngineeringProjectHoursDataSource class.
	/// </summary>
	public class KpiEngineeringProjectHoursDataSourceDesigner : ReadOnlyDataSourceDesigner<KpiEngineeringProjectHours>
	{
	}

	#endregion KpiEngineeringProjectHoursDataSourceDesigner

	#region KpiEngineeringProjectHoursFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiEngineeringProjectHoursFilter : SqlFilter<KpiEngineeringProjectHoursColumn>
	{
	}

	#endregion KpiEngineeringProjectHoursFilter

	#region KpiEngineeringProjectHoursExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiEngineeringProjectHours"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiEngineeringProjectHoursExpressionBuilder : SqlExpressionBuilder<KpiEngineeringProjectHoursColumn>
	{
	}
	
	#endregion KpiEngineeringProjectHoursExpressionBuilder		
}

