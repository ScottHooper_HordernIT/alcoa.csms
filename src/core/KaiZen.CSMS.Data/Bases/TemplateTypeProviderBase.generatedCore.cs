﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TemplateTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TemplateTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TemplateType, KaiZen.CSMS.Entities.TemplateTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TemplateTypeKey key)
		{
			return Delete(transactionManager, key.TemplateTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_templateTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _templateTypeId)
		{
			return Delete(null, _templateTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _templateTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TemplateType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TemplateTypeKey key, int start, int pageLength)
		{
			return GetByTemplateTypeId(transactionManager, key.TemplateTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TemplateType index.
		/// </summary>
		/// <param name="_templateTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeId(System.Int32 _templateTypeId)
		{
			int count = -1;
			return GetByTemplateTypeId(null,_templateTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TemplateType index.
		/// </summary>
		/// <param name="_templateTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeId(System.Int32 _templateTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByTemplateTypeId(null, _templateTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TemplateType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeId(TransactionManager transactionManager, System.Int32 _templateTypeId)
		{
			int count = -1;
			return GetByTemplateTypeId(transactionManager, _templateTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TemplateType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeId(TransactionManager transactionManager, System.Int32 _templateTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByTemplateTypeId(transactionManager, _templateTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TemplateType index.
		/// </summary>
		/// <param name="_templateTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeId(System.Int32 _templateTypeId, int start, int pageLength, out int count)
		{
			return GetByTemplateTypeId(null, _templateTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TemplateType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeId(TransactionManager transactionManager, System.Int32 _templateTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_TemplateTypeName index.
		/// </summary>
		/// <param name="_templateTypeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeName(System.String _templateTypeName)
		{
			int count = -1;
			return GetByTemplateTypeName(null,_templateTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TemplateTypeName index.
		/// </summary>
		/// <param name="_templateTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeName(System.String _templateTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByTemplateTypeName(null, _templateTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TemplateTypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeName(TransactionManager transactionManager, System.String _templateTypeName)
		{
			int count = -1;
			return GetByTemplateTypeName(transactionManager, _templateTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TemplateTypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeName(TransactionManager transactionManager, System.String _templateTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByTemplateTypeName(transactionManager, _templateTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TemplateTypeName index.
		/// </summary>
		/// <param name="_templateTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeName(System.String _templateTypeName, int start, int pageLength, out int count)
		{
			return GetByTemplateTypeName(null, _templateTypeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TemplateTypeName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_templateTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TemplateType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TemplateType GetByTemplateTypeName(TransactionManager transactionManager, System.String _templateTypeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TemplateType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TemplateType&gt;"/></returns>
		public static TList<TemplateType> Fill(IDataReader reader, TList<TemplateType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TemplateType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TemplateType")
					.Append("|").Append((System.Int32)reader[((int)TemplateTypeColumn.TemplateTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TemplateType>(
					key.ToString(), // EntityTrackingKey
					"TemplateType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TemplateType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TemplateTypeId = (System.Int32)reader[((int)TemplateTypeColumn.TemplateTypeId - 1)];
					c.TemplateTypeName = (System.String)reader[((int)TemplateTypeColumn.TemplateTypeName - 1)];
					c.TemplateTypeDesc = (System.String)reader[((int)TemplateTypeColumn.TemplateTypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TemplateType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TemplateType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TemplateType entity)
		{
			if (!reader.Read()) return;
			
			entity.TemplateTypeId = (System.Int32)reader[((int)TemplateTypeColumn.TemplateTypeId - 1)];
			entity.TemplateTypeName = (System.String)reader[((int)TemplateTypeColumn.TemplateTypeName - 1)];
			entity.TemplateTypeDesc = (System.String)reader[((int)TemplateTypeColumn.TemplateTypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TemplateType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TemplateType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TemplateType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TemplateTypeId = (System.Int32)dataRow["TemplateTypeId"];
			entity.TemplateTypeName = (System.String)dataRow["TemplateTypeName"];
			entity.TemplateTypeDesc = (System.String)dataRow["TemplateTypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TemplateType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TemplateType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TemplateType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TemplateType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TemplateType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TemplateType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TemplateType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TemplateTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TemplateType</c>
	///</summary>
	public enum TemplateTypeChildEntityTypes
	{
	}
	
	#endregion TemplateTypeChildEntityTypes
	
	#region TemplateTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TemplateTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeFilterBuilder : SqlFilterBuilder<TemplateTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeFilterBuilder class.
		/// </summary>
		public TemplateTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TemplateTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TemplateTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TemplateTypeFilterBuilder
	
	#region TemplateTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TemplateTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeParameterBuilder : ParameterizedSqlFilterBuilder<TemplateTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeParameterBuilder class.
		/// </summary>
		public TemplateTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TemplateTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TemplateTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TemplateTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TemplateTypeParameterBuilder
	
	#region TemplateTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TemplateTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TemplateTypeSortBuilder : SqlSortBuilder<TemplateTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeSqlSortBuilder class.
		/// </summary>
		public TemplateTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TemplateTypeSortBuilder
	
} // end namespace
