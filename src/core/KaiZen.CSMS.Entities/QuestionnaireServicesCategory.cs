﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireServicesCategory' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireServicesCategory : QuestionnaireServicesCategoryBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireServicesCategory"/> instance.
		///</summary>
		public QuestionnaireServicesCategory():base(){}	
		
		#endregion
	}
}
