using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxCallbackPanel;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Collections.Generic;
using System.Web.Configuration;
using System.ComponentModel;

public struct gcTL_SQ
{
    public static object Value { get; set; }
    static gcTL_SQ()
    {
        
    }
}
public partial class UserControls_Tiny_Reports_DropDownListPicker2 : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    private enum TrafficLightColor
    {
        [Description("0")]
        Grey = 0,
        [Description("1")]
        Red = 1,
        [Description("2")]
        Yellow = 2,
        [Description("3")]
        Green = 3,
    }

    // Delegate declaration
    public delegate void OnButtonClick(string strValue);

    // Event declaration
    public event OnButtonClick btnHandler;

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    int intTryPrase = 0;



    private void moduleInit(bool postBack)
    {
        
        if (!postBack)
        {
            ddlCompanies.DataSourceID = "sqldsCompaniesList";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();
            ddlCompanies.Items.Add("-----------------------------", 0);
            if (SessionHandler.spVar_Page == "ComplianceReport")
            {
                ddlCompanies.Items.Add("< Select Company >", -1);
            }
            else
            {
                ddlCompanies.Items.Add("All", -1);
            }
            ddlCompanies.Value = -1;

            ddlMonth.DataSourceID = "dsMonths";
            ddlMonth.TextField = "MonthName";
            ddlMonth.ValueField = "MonthId";
            ddlMonth.DataBind();
            ddlMonth.Items.Add("-----------------------------", -1);
            ddlMonth.Items.Add("All/YTD", 0);

            ddlYear.DataSourceID = "sqldsKpi_GetAllYearsSubmitted";
            ddlYear.TextField = "Year";
            ddlYear.ValueField = "Year";
            ddlYear.DataBind();

            DataTable dt = ((DataView)sqldsKpi_GetAllYearsSubmitted.Select(DataSourceSelectArguments.Empty)).ToTable();
            int r = dt.Rows.Count - 1;
            int latestYear = 0;
            Int32.TryParse(dt.Rows[r].ItemArray[0].ToString(), out latestYear);

            if (latestYear != 0)
            {
                ddlYear.Value = latestYear;
            }
            else
            {
                ddlYear.Value = DateTime.Now.Year;
            }

            if (DateTime.Now.Month == 1)
            {
                ddlYear.Value = DateTime.Now.Year - 1;
                SessionHandler.spVar_Month = "1";
                SessionHandler.spVar_MonthPrevious = "12";
            }
            else
            {
                if (String.IsNullOrEmpty(SessionHandler.spVar_Month)) SessionHandler.spVar_Month = DateTime.Now.Month.ToString();
                if (String.IsNullOrEmpty(SessionHandler.spVar_MonthPrevious)) SessionHandler.spVar_MonthPrevious = (DateTime.Now.Month - 1).ToString();
            }

            if (String.IsNullOrEmpty(SessionHandler.spVar_MonthId)) SessionHandler.spVar_MonthId = "0";

            ddlMonth.Value = Convert.ToInt32(SessionHandler.spVar_MonthId);

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    ddlCompanies.Enabled = true;
                    FillSitesCombo("-1");
                    break;
                case ((int)RoleList.Contractor):
                    ddlCompanies.Value = auth.CompanyId;
                    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    FillSitesCombo(auth.CompanyId.ToString());
                    break;
                case ((int)RoleList.Administrator):
                    ddlCompanies.Enabled = true;
                    FillSitesCombo("-1");
                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            if (String.IsNullOrEmpty(SessionHandler.spVar_CompanyId))
            {
                ddlCompanies.Text = "< Select Company >";
            }
            else
            {
                if (Int32.TryParse(SessionHandler.spVar_CompanyId, out intTryPrase))
                {
                    ddlCompanies.Value = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                    FillSitesCombo(ddlCompanies.Value.ToString());
                }
            }
            if (!String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
            {
                if (Int32.TryParse(SessionHandler.spVar_SiteId, out intTryPrase))
                {
                    ddlSites.Value = Convert.ToInt32(SessionHandler.spVar_SiteId);
                }
            }


            if (!String.IsNullOrEmpty(SessionHandler.spVar_Year))
            {
                if (Int32.TryParse(SessionHandler.spVar_Year, out intTryPrase))
                {
                    ddlYear.Value =SessionHandler.spVar_Year;

                }
            }
        }
    }
    protected void cmbSites_Callback(object source, CallbackEventArgsBase e)
    {
        ddlSites.SelectedIndex = 0;
        FillSitesCombo(e.Parameter);
    }
    protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString());
    }

    //protected void FillCategoriesCombo(string companyId, string siteId)
    //{
    //    if (string.IsNullOrEmpty(companyId) || string.IsNullOrEmpty(siteId)) return;

    //    int _companyId;
    //    bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
    //    if (!companyId_isInt) return;
    //    int _siteId;
    //    bool siteId_isInt = Int32.TryParse(siteId, out _siteId);
    //    if (!siteId_isInt) return;


    //}

    protected void FillSitesCombo(string companyId)
    {
        int _i = 0;
        if (string.IsNullOrEmpty(companyId)) return;

        int _companyId;
        bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
        if(!companyId_isInt) return;

        if (_companyId > 0) //Specific Company
        {
            SessionHandler.spVar_CompanyId = companyId;

            SitesService sService = new SitesService();
            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
            TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_companyId);

            ddlSites.Items.Clear();

            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (cscs.CompanySiteCategoryId != null)
                            {
                                if (s.SiteId == cscs.SiteId)
                                {
                                    ddlSites.Items.Add(s.SiteName, cscs.SiteId);
                                }
                            }
                        }
                    }

                    ddlSites.Items.Add("----------------", 0);

                    //Add Regions
                    RegionsService rService = new RegionsService();
                    DataSet dsRegion = rService.GetByCompanyId(_companyId);
                    if (dsRegion.Tables[0] != null)
                    {
                        if (dsRegion.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in dsRegion.Tables[0].Rows)
                            {
                                if (dr[2].ToString() != "AU") ddlSites.Items.Add(dr[0].ToString(), (-1 * Convert.ToInt32(dr[1].ToString())));
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (_companyId == -1) // All Companies
            {
                ddlSites.Items.Clear();
                SitesFilters sitesFilters = new SitesFilters();
                sitesFilters.Append(SitesColumn.IsVisible, "1");
                TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                foreach (Sites s in sitesList)
                {
                    ddlSites.Items.Add(s.SiteName, s.SiteId);
                }
                ddlSites.Items.Add("----------------", 0);

                RegionsFilters regionsFilters = new RegionsFilters();
                regionsFilters.Append(RegionsColumn.IsVisible, "1");
                TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                foreach (Regions r in regionsList)
                {
                    ddlSites.Items.Add(r.RegionName, (-1 * r.RegionId));
                }
            }
            else if (_companyId == -2) //Selected "----" invalid option, so lets hide all sites.
            {
                ddlSites.Items.Clear();
                ddlSites.DataSourceID = "";
                ddlSites.DataBind();
            }
        }

    }

    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSearchGo_Click(object sender, EventArgs e)
    {
        if (btnHandler != null)
            btnHandler(string.Empty);


    }

    }


