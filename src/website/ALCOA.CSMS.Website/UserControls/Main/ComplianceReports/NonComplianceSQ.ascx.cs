using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Data.Filtering;
using System.Text.RegularExpressions;


public partial class UserControls_NonComplianceSQ : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {


            SessionHandler.spVar_Year = DateTime.Now.Year.ToString();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    grid.Settings.ShowFilterRow = false;
                    grid.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);

                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            //sqldsNonComplianceReportYearCSA.DataBind();
            //grid.DataBind();

            // Export
            String exportFileName = @"ALCOA CSMS - Non Compliance Report - Safety Qualification"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        grid.DataBind();
    }

    

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblExpiresIn") as Label;
        if (label != null)//change by Debashis for deployment issue
        {
            if (grid.GetRowValues(e.VisibleIndex, "ExpiresIn") != DBNull.Value)
            {
                int ExpiresIn = Convert.ToInt32(grid.GetRowValues(e.VisibleIndex, "ExpiresIn"));
                label.Text = String.Format("{0} Days", ExpiresIn);

                if (grid.GetRowValues(e.VisibleIndex, "Status") != null && grid.GetRowValues(e.VisibleIndex, "QuestionnaireId") != null)
                {
                    if (ExpiresIn < 0)
                    {
                        label.ForeColor = Color.Red;
                        label.Font.Bold = true;
                        label.Text = String.Format("Expired {0} Days ago", (ExpiresIn.ToString()).Replace("-", ""));
                    }
                }
            }
            else
            {
                label.Text = "n/a";
            }
        }

        //if(grid.GetRowValues(e.VisibleIndex, "QuestionnaireId") != DBNull.Value)
        //{
        //    System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage1")
        //                               as System.Web.UI.WebControls.Image;
        //    img.Visible = true;
        //    img.ImageUrl = "~/Images/greentick.gif";
        //}

        if (grid.GetRowValues(e.VisibleIndex, "Recommended") != DBNull.Value)
        {
            System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
                                       as System.Web.UI.WebControls.Image;
            bool recommended = (bool)grid.GetRowValues(e.VisibleIndex, "Recommended");
            if (img != null && recommended !=null) //change by Debashis for Deployment issue
            {
                if (recommended)
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/greentick.gif";
                }
                else
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/redcross.gif";
                }
            }
        }
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
    {
        if (e.Column.FieldName == "TotalTime" || e.Column.FieldName == "ExpiresIn")
        {
            ComboBoxProperties combo = new ComboBoxProperties();
            e.EditorProperties = combo;
        }
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "Status" || e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1)}";
        }
        else if (e.Column.FieldName == "ExpiresIn")
        {
            ASPxComboBox combo = e.Editor as ASPxComboBox;
            combo.ValueType = typeof(string);

            combo.Items.Add("(All)");
            combo.Items.Add("Expired 1000 Days ago");
            combo.Items.Add("Expired 50 Days ago");
            combo.Items.Add("7 Days");
            combo.Items.Add("90 Days");
            combo.Items.Add("300 Days");
            combo.Items.Add("n/a");
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName != "ExpiresIn") return;
        if (e.Kind == GridViewAutoFilterEventKind.ExtractDisplayText)
        {
            e.Value = (Session["value"] != null ? Session["value"].ToString() : "");
            return;
        }
        else
        {
            int start = Int32.MinValue, end = Int32.MaxValue;
            string value = e.Value;

            if (value == "n/a")
            {
                e.Criteria = new GroupOperator(GroupOperatorType.And,
                    new BinaryOperator(e.Column.FieldName, DBNull.Value, BinaryOperatorType.Equal));
            }
            else
            {
                if (value == "Expired 1000 Days ago")
                    end = -1000;
                else if (value == "Expired 50 Days ago")
                {
                    end = -50;
                }
                else if (value == "7 Days")
                {
                    start = 7;
                }
                else if (value == "90 Days")
                {
                    start = 90;
                }
                else if (value == "300 Days")
                {
                    start = 300;
                }

                Session["value"] = value;
                e.Criteria = new GroupOperator(GroupOperatorType.And,
                    new BinaryOperator(e.Column.FieldName, start, BinaryOperatorType.GreaterOrEqual),
                    new BinaryOperator(e.Column.FieldName, end, BinaryOperatorType.Less));
            }

        }
    }

    protected void grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
    {

        if (e.Column.FieldName == "ExpiresIn")
        {
            e.Handled = true;
            
            string s1, s2;
            

            if (e.Value1 == null)
            {
                s1 = long.MaxValue.ToString();
            }
            else
            {
                s1 = e.Value1.ToString();
            }
            if (e.Value2 == null)
            {
                s2 = long.MaxValue.ToString();
            }
            else
            {
                s2 = e.Value2.ToString();
            }
            
            //long value1 = Convert.ToInt64(Regex.Match(s1, @"\d+").Value);
            //long value2 = Convert.ToInt64(Regex.Match(s2, @"\d+").Value);
            long value1 = Convert.ToInt64(s1);
            long value2 = Convert.ToInt64(s2);
            //if (s1.Contains("Expired"))
            //{
            //    value1 = value1 * -1;
            //}
            //if (s2.Contains("Expired"))
            //{
            //    value2 = value2 * -1;
            //}
            //if (s1.Contains("n/a"))
            //{
            //    value1 = long.MaxValue;
            //}
            //if (s2.Contains("n/a"))
            //{
            //    value2 = long.MaxValue;
            //}
            
            if (value1 > value2)
                e.Result = 1;
            else
                if (value1 == value2)
                    e.Result = Comparer.Default.Compare(value1, value2);
                else
                    e.Result = -1;
        }
    }




}
