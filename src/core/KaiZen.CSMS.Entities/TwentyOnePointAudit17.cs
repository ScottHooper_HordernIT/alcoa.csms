﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TwentyOnePointAudit_17' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TwentyOnePointAudit17 : TwentyOnePointAudit17Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TwentyOnePointAudit17"/> instance.
		///</summary>
		public TwentyOnePointAudit17():base(){}	
		
		#endregion
	}
}
