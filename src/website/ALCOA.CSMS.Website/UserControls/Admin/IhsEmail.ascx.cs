﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.Net.Mail;
using System.IO;

using System.Security;
using System.Security.Principal;

using System.Text;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class IhsEmail : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            { //first time load
                if (auth.RoleId == (int)RoleList.Administrator)
                {
                    rblDataSource.Value = 0;
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
            Prefill();
        }

        protected void Prefill()
        {
            ASPxMemo1.Text = "";

            WindowsImpersonationContext impContext = null;
            try
            {
                impContext = Helper.ImpersonateWAOCSMUser();
            }
            catch (ApplicationException ex)
            {
                throw new Exception("Error Occured: " + ex.Message);
            }
           
            try
            {
                if (null != impContext)
                {
                    using (impContext)
                    {
                        tbTo.Text = "ihshelp@alcoa.com";
                        Configuration configuration = new Configuration();
                        tbCc.Text = configuration.GetValue(ConfigList.ContactEmail);

                        if ((int)rblDataSource.SelectedItem.Value == 0)
                        {
                            heBody.Html = "Please find attached the Contractor Hours (last month) for import into IHS.";

                            string path = configuration.GetValue(ConfigList.AiImportFilesFilePath);

                            DateTime dtLastMonth = DateTime.Now.AddMonths(-1);

                            ReadErrorLog(path + "month\\" + dtLastMonth.Year + "-" + dtLastMonth.Month + "\\Errors.txt");

                            ListAttachments(path + "month\\" + dtLastMonth.Year + "-" + dtLastMonth.Month + "\\");
                        }
                        else
                        {
                            heBody.Html = "Please find attached the Contractor Hours for this year for import into IHS. Please ensure to delete all entries and replace with the attached.";

                            string path = configuration.GetValue(ConfigList.AiImportFilesFilePath);

                            ReadErrorLog(path + "Errors.txt");

                            ListAttachments(path);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured: " + ex.Message);
            }
        }

        protected void ReadErrorLog(string filepath)
        {
            if (!File.Exists(filepath))
            {
                Console.WriteLine("{0} does not exist.", filepath);
                return;
            }

            lblCreatedAt.Text = File.GetLastWriteTime(filepath).ToString();

            using (StreamReader sr = new StreamReader(filepath))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    ASPxMemo1.Text += line + "\n";
                }
            }
        }

        protected void ListAttachments(string path)
        {
            int count = 1;

            hlShowFolder.NavigateUrl = @"file:///" + path;

            foreach (string filename in Directory.GetFiles(path))
            {
                if (filename.Contains(".csv"))
                {
                    FileInfo fileInfo = new FileInfo(filename);

                    string fileDetails = String.Format("{0} ({1})", filename.Replace(path, ""), FileUtilities.Display.FileSize(Convert.ToInt32(fileInfo.Length)));
                    string fileDetails2 = "<a href=" + "\"" + @"file://" + path + filename.Replace(path, "") + "\">" + fileDetails + "</a>";

                    if (count < Directory.GetFiles(path).Length)
                    {
                        if (count == 1)
                        {
                            lblAttachments.Text = fileDetails2 + "<br />";
                        }
                        else
                        {
                            lblAttachments.Text += fileDetails2 + "<br />";
                        }
                    }
                    else
                    {
                        lblAttachments.Text += fileDetails2;
                    }
                    count++;
                }
            }
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    Configuration config = new Configuration();
                    //------------------------------------
                    //modified by Ashley Goldstraw 11/9/2015 to use new mail sender.  The impersonation was removed as all is needed to be passed is a folder and the mail sender will pick up the files.
                    //------------------------------------
                   /* mail.From = new MailAddress(config.GetValue(ConfigList.ContactEmail), "AUA Alcoa Contractor Services Team");
                    mail.To.Add(tbTo.Text);
                    mail.CC.Add(tbCc.Text);
                    mail.IsBodyHtml = true;
                    mail.Body = heBody.Html;
                    mail.Subject = "Monthly Contractors Hours for IHS Import";

                    mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                    mail.Headers.Add("Disposition-Notification-To", config.GetValue(ConfigList.ContactEmail));
                    */
                    //attachments
                    //WindowsImpersonationContext impContext = null;
                    //impContext = Helper.ImpersonateWAOCSMUser();

                    //if (null == impContext) throw new Exception("Could not use service account.");

                    string path = null;
                    //using (impContext)
                    //{
                        path = config.GetValue(ConfigList.AiImportFilesFilePath);
                        if ((int)rblDataSource.SelectedItem.Value == 0)
                        {
                            DateTime dtLastMonth = DateTime.Now.AddMonths(-1);

                            path = path + "month\\" + dtLastMonth.Year + "-" + dtLastMonth.Month + "\\";
                        }
                        //else
                        //{
                        //    path = config.GetValue(ConfigList.AiImportFilesFilePath);
                        //}

                        /*foreach (string filename in Directory.GetFiles(path))
                        {
                            if (filename.Contains(".csv"))
                            {
                                mail.Attachments.Add(new Attachment(filename));
                            }
                        }*/
                   // }

                    //SmtpClient smtp = new SmtpClient();
                    //smtp.Send(mail);

                    string[] iTo = {tbTo.Text};
                    string[] iCc = {tbCc.Text};
                    string from = config.GetValue(ConfigList.ContactEmail);
                    emailLogService.Insert(from, iTo, iCc, null, "Monthly Contractors Hours for IHS Import", heBody.Html, "IhsEmail", true, path, ".csv");

                    //Removed by AG as the sender automatically logs.
                   /* EmailLogService elService = new EmailLogService();
                    EmailLog el = new EmailLog();
                    el.EmailLogTypeId = (int)EmailLogTypeList.IhsEmail;
                    el.EmailDateTime = DateTime.Now;
                    el.EmailFrom = config.GetValue(ConfigList.ContactEmail);
                    el.EmailTo = tbTo.Text;
                    el.EmailCc = tbCc.Text;
                    el.EmailBcc = null;
                    el.EmailLogMessageSubject = mail.Subject;
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    el.EmailLogMessageBody = (byte[])encoding.GetBytes(heBody.Html);
                    el.SentByUserId = auth.UserId;

                    elService.Save(el);*/
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void rblDataSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Prefill();
            }
        }
    }
}