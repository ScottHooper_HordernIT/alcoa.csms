﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class KpiAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.KpiAudit, KaiZen.CSMS.Entities.KpiAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.KpiAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_KPIAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPIAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPIAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPIAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPIAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KPIAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.KpiAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;KpiAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;KpiAudit&gt;"/></returns>
		public static TList<KpiAudit> Fill(IDataReader reader, TList<KpiAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.KpiAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("KpiAudit")
					.Append("|").Append((System.Int32)reader[((int)KpiAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<KpiAudit>(
					key.ToString(), // EntityTrackingKey
					"KpiAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.KpiAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)KpiAuditColumn.AuditId - 1)];
					c.KpiId = (reader.IsDBNull(((int)KpiAuditColumn.KpiId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.KpiId - 1)];
					c.SiteId = (reader.IsDBNull(((int)KpiAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.SiteId - 1)];
					c.CreatedbyUserId = (reader.IsDBNull(((int)KpiAuditColumn.CreatedbyUserId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.CreatedbyUserId - 1)];
					c.ModifiedbyUserId = (reader.IsDBNull(((int)KpiAuditColumn.ModifiedbyUserId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.ModifiedbyUserId - 1)];
					c.DateAdded = (reader.IsDBNull(((int)KpiAuditColumn.DateAdded - 1)))?null:(System.DateTime?)reader[((int)KpiAuditColumn.DateAdded - 1)];
					c.Datemodified = (reader.IsDBNull(((int)KpiAuditColumn.Datemodified - 1)))?null:(System.DateTime?)reader[((int)KpiAuditColumn.Datemodified - 1)];
					c.CompanyId = (reader.IsDBNull(((int)KpiAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.CompanyId - 1)];
					c.KpiDateTime = (reader.IsDBNull(((int)KpiAuditColumn.KpiDateTime - 1)))?null:(System.DateTime?)reader[((int)KpiAuditColumn.KpiDateTime - 1)];
					c.KpiGeneral = (reader.IsDBNull(((int)KpiAuditColumn.KpiGeneral - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiGeneral - 1)];
					c.KpiCalcinerExpense = (reader.IsDBNull(((int)KpiAuditColumn.KpiCalcinerExpense - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiCalcinerExpense - 1)];
					c.KpiCalcinerCapital = (reader.IsDBNull(((int)KpiAuditColumn.KpiCalcinerCapital - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiCalcinerCapital - 1)];
					c.KpiResidue = (reader.IsDBNull(((int)KpiAuditColumn.KpiResidue - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiResidue - 1)];
					c.ProjectCapital1Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital1Title - 1)];
					c.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital1Hours - 1)];
					c.ProjectCapital2Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital2Title - 1)];
					c.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital2Hours - 1)];
					c.ProjectCapital3Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital3Title - 1)];
					c.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital3Hours - 1)];
					c.ProjectCapital4Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital4Title - 1)];
					c.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital4Hours - 1)];
					c.ProjectCapital5Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital5Title - 1)];
					c.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital5Hours - 1)];
					c.AheaRefineryWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaRefineryWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaRefineryWork - 1)];
					c.AheaResidueWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaResidueWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaResidueWork - 1)];
					c.AheaSmeltingWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaSmeltingWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaSmeltingWork - 1)];
					c.AheaPowerGenerationWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaPowerGenerationWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaPowerGenerationWork - 1)];
					c.AheaTotalManHours = (reader.IsDBNull(((int)KpiAuditColumn.AheaTotalManHours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaTotalManHours - 1)];
					c.AheaPeakNopplSiteWeek = (reader.IsDBNull(((int)KpiAuditColumn.AheaPeakNopplSiteWeek - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.AheaPeakNopplSiteWeek - 1)];
					c.AheaAvgNopplSiteMonth = (reader.IsDBNull(((int)KpiAuditColumn.AheaAvgNopplSiteMonth - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.AheaAvgNopplSiteMonth - 1)];
					c.AheaNoEmpExcessMonth = (reader.IsDBNull(((int)KpiAuditColumn.AheaNoEmpExcessMonth - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.AheaNoEmpExcessMonth - 1)];
					c.IpFati = (reader.IsDBNull(((int)KpiAuditColumn.IpFati - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpFati - 1)];
					c.IpMti = (reader.IsDBNull(((int)KpiAuditColumn.IpMti - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpMti - 1)];
					c.IpRdi = (reader.IsDBNull(((int)KpiAuditColumn.IpRdi - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpRdi - 1)];
					c.IpLti = (reader.IsDBNull(((int)KpiAuditColumn.IpLti - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpLti - 1)];
					c.IpIfe = (reader.IsDBNull(((int)KpiAuditColumn.IpIfe - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpIfe - 1)];
					c.EhspNoLwd = (reader.IsDBNull(((int)KpiAuditColumn.EhspNoLwd - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.EhspNoLwd - 1)];
					c.EhspNoRd = (reader.IsDBNull(((int)KpiAuditColumn.EhspNoRd - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.EhspNoRd - 1)];
					c.EhsAudits = (reader.IsDBNull(((int)KpiAuditColumn.EhsAudits - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.EhsAudits - 1)];
					c.EhsCorrective = (reader.IsDBNull(((int)KpiAuditColumn.EhsCorrective - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.EhsCorrective - 1)];
					c.JsaAudits = (reader.IsDBNull(((int)KpiAuditColumn.JsaAudits - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.JsaAudits - 1)];
					c.IWsc = (reader.IsDBNull(((int)KpiAuditColumn.IWsc - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IWsc - 1)];
					c.ONoHswc = (reader.IsDBNull(((int)KpiAuditColumn.ONoHswc - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.ONoHswc - 1)];
					c.ONoBsp = (reader.IsDBNull(((int)KpiAuditColumn.ONoBsp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.ONoBsp - 1)];
					c.QQas = (reader.IsDBNull(((int)KpiAuditColumn.QQas - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.QQas - 1)];
					c.QNoNci = (reader.IsDBNull(((int)KpiAuditColumn.QNoNci - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.QNoNci - 1)];
					c.MTbmpm = (reader.IsDBNull(((int)KpiAuditColumn.MTbmpm - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MTbmpm - 1)];
					c.MAwcm = (reader.IsDBNull(((int)KpiAuditColumn.MAwcm - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MAwcm - 1)];
					c.MAmcm = (reader.IsDBNull(((int)KpiAuditColumn.MAmcm - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MAmcm - 1)];
					c.MFatality = (reader.IsDBNull(((int)KpiAuditColumn.MFatality - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MFatality - 1)];
					c.Training = (reader.IsDBNull(((int)KpiAuditColumn.Training - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.Training - 1)];
					c.MtTolo = (reader.IsDBNull(((int)KpiAuditColumn.MtTolo - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtTolo - 1)];
					c.MtFp = (reader.IsDBNull(((int)KpiAuditColumn.MtFp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtFp - 1)];
					c.MtElec = (reader.IsDBNull(((int)KpiAuditColumn.MtElec - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtElec - 1)];
					c.MtMe = (reader.IsDBNull(((int)KpiAuditColumn.MtMe - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtMe - 1)];
					c.MtCs = (reader.IsDBNull(((int)KpiAuditColumn.MtCs - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtCs - 1)];
					c.MtCb = (reader.IsDBNull(((int)KpiAuditColumn.MtCb - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtCb - 1)];
					c.MtErgo = (reader.IsDBNull(((int)KpiAuditColumn.MtErgo - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtErgo - 1)];
					c.MtRa = (reader.IsDBNull(((int)KpiAuditColumn.MtRa - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtRa - 1)];
					c.MtHs = (reader.IsDBNull(((int)KpiAuditColumn.MtHs - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtHs - 1)];
					c.MtSp = (reader.IsDBNull(((int)KpiAuditColumn.MtSp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtSp - 1)];
					c.MtIf = (reader.IsDBNull(((int)KpiAuditColumn.MtIf - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtIf - 1)];
					c.MtHp = (reader.IsDBNull(((int)KpiAuditColumn.MtHp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtHp - 1)];
					c.MtRp = (reader.IsDBNull(((int)KpiAuditColumn.MtRp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtRp - 1)];
					c.MtEnginfo81t = (reader.IsDBNull(((int)KpiAuditColumn.MtEnginfo81t - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtEnginfo81t - 1)];
					c.MtOthers = (reader.IsDBNull(((int)KpiAuditColumn.MtOthers - 1)))?null:(System.String)reader[((int)KpiAuditColumn.MtOthers - 1)];
					c.SafetyPlansSubmitted = (reader.IsDBNull(((int)KpiAuditColumn.SafetyPlansSubmitted - 1)))?null:(System.Boolean?)reader[((int)KpiAuditColumn.SafetyPlansSubmitted - 1)];
					c.EbiOnSite = (reader.IsDBNull(((int)KpiAuditColumn.EbiOnSite - 1)))?null:(System.Boolean?)reader[((int)KpiAuditColumn.EbiOnSite - 1)];
					c.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital10Hours - 1)];
					c.ProjectCapital10Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital10Title - 1)];
					c.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital11Hours - 1)];
					c.ProjectCapital11Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital11Title - 1)];
					c.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital12Hours - 1)];
					c.ProjectCapital12Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital12Title - 1)];
					c.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital13Hours - 1)];
					c.ProjectCapital13Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital13Title - 1)];
					c.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital14Hours - 1)];
					c.ProjectCapital14Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital14Title - 1)];
					c.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital15Hours - 1)];
					c.ProjectCapital15Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital15Title - 1)];
					c.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital6Hours - 1)];
					c.ProjectCapital6Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital6Title - 1)];
					c.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital7Hours - 1)];
					c.ProjectCapital7Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital7Title - 1)];
					c.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital8Hours - 1)];
					c.ProjectCapital8Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital8Title - 1)];
					c.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital9Hours - 1)];
					c.ProjectCapital9Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital9Title - 1)];
					c.IsSystemEdit = (reader.IsDBNull(((int)KpiAuditColumn.IsSystemEdit - 1)))?null:(System.Boolean?)reader[((int)KpiAuditColumn.IsSystemEdit - 1)];
					c.ProjectCapital1Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital1Po - 1)];
					c.ProjectCapital1Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital1Line - 1)];
					c.ProjectCapital2Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital2Po - 1)];
					c.ProjectCapital2Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital2Line - 1)];
					c.ProjectCapital3Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital3Po - 1)];
					c.ProjectCapital3Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital3Line - 1)];
					c.ProjectCapital4Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital4Po - 1)];
					c.ProjectCapital4Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital4Line - 1)];
					c.ProjectCapital5Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital5Po - 1)];
					c.ProjectCapital5Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital5Line - 1)];
					c.ProjectCapital6Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital6Po - 1)];
					c.ProjectCapital6Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital6Line - 1)];
					c.ProjectCapital7Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital7Po - 1)];
					c.ProjectCapital7Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital7Line - 1)];
					c.ProjectCapital8Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital8Po - 1)];
					c.ProjectCapital8Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital8Line - 1)];
					c.ProjectCapital9Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital9Po - 1)];
					c.ProjectCapital9Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital9Line - 1)];
					c.ProjectCapital10Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital10Po - 1)];
					c.ProjectCapital10Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital10Line - 1)];
					c.ProjectCapital11Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital11Po - 1)];
					c.ProjectCapital11Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital11Line - 1)];
					c.ProjectCapital12Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital12Po - 1)];
					c.ProjectCapital12Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital12Line - 1)];
					c.ProjectCapital13Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital13Po - 1)];
					c.ProjectCapital13Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital13Line - 1)];
					c.ProjectCapital14Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital14Po - 1)];
					c.ProjectCapital14Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital14Line - 1)];
					c.ProjectCapital15Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital15Po - 1)];
					c.ProjectCapital15Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital15Line - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)KpiAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)KpiAuditColumn.AuditEventId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.KpiAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)KpiAuditColumn.AuditId - 1)];
			entity.KpiId = (reader.IsDBNull(((int)KpiAuditColumn.KpiId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.KpiId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)KpiAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.SiteId - 1)];
			entity.CreatedbyUserId = (reader.IsDBNull(((int)KpiAuditColumn.CreatedbyUserId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.CreatedbyUserId - 1)];
			entity.ModifiedbyUserId = (reader.IsDBNull(((int)KpiAuditColumn.ModifiedbyUserId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.ModifiedbyUserId - 1)];
			entity.DateAdded = (reader.IsDBNull(((int)KpiAuditColumn.DateAdded - 1)))?null:(System.DateTime?)reader[((int)KpiAuditColumn.DateAdded - 1)];
			entity.Datemodified = (reader.IsDBNull(((int)KpiAuditColumn.Datemodified - 1)))?null:(System.DateTime?)reader[((int)KpiAuditColumn.Datemodified - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)KpiAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.CompanyId - 1)];
			entity.KpiDateTime = (reader.IsDBNull(((int)KpiAuditColumn.KpiDateTime - 1)))?null:(System.DateTime?)reader[((int)KpiAuditColumn.KpiDateTime - 1)];
			entity.KpiGeneral = (reader.IsDBNull(((int)KpiAuditColumn.KpiGeneral - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiGeneral - 1)];
			entity.KpiCalcinerExpense = (reader.IsDBNull(((int)KpiAuditColumn.KpiCalcinerExpense - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiCalcinerExpense - 1)];
			entity.KpiCalcinerCapital = (reader.IsDBNull(((int)KpiAuditColumn.KpiCalcinerCapital - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiCalcinerCapital - 1)];
			entity.KpiResidue = (reader.IsDBNull(((int)KpiAuditColumn.KpiResidue - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.KpiResidue - 1)];
			entity.ProjectCapital1Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital1Title - 1)];
			entity.ProjectCapital1Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital1Hours - 1)];
			entity.ProjectCapital2Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital2Title - 1)];
			entity.ProjectCapital2Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital2Hours - 1)];
			entity.ProjectCapital3Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital3Title - 1)];
			entity.ProjectCapital3Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital3Hours - 1)];
			entity.ProjectCapital4Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital4Title - 1)];
			entity.ProjectCapital4Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital4Hours - 1)];
			entity.ProjectCapital5Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital5Title - 1)];
			entity.ProjectCapital5Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital5Hours - 1)];
			entity.AheaRefineryWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaRefineryWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaRefineryWork - 1)];
			entity.AheaResidueWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaResidueWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaResidueWork - 1)];
			entity.AheaSmeltingWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaSmeltingWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaSmeltingWork - 1)];
			entity.AheaPowerGenerationWork = (reader.IsDBNull(((int)KpiAuditColumn.AheaPowerGenerationWork - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaPowerGenerationWork - 1)];
			entity.AheaTotalManHours = (reader.IsDBNull(((int)KpiAuditColumn.AheaTotalManHours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.AheaTotalManHours - 1)];
			entity.AheaPeakNopplSiteWeek = (reader.IsDBNull(((int)KpiAuditColumn.AheaPeakNopplSiteWeek - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.AheaPeakNopplSiteWeek - 1)];
			entity.AheaAvgNopplSiteMonth = (reader.IsDBNull(((int)KpiAuditColumn.AheaAvgNopplSiteMonth - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.AheaAvgNopplSiteMonth - 1)];
			entity.AheaNoEmpExcessMonth = (reader.IsDBNull(((int)KpiAuditColumn.AheaNoEmpExcessMonth - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.AheaNoEmpExcessMonth - 1)];
			entity.IpFati = (reader.IsDBNull(((int)KpiAuditColumn.IpFati - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpFati - 1)];
			entity.IpMti = (reader.IsDBNull(((int)KpiAuditColumn.IpMti - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpMti - 1)];
			entity.IpRdi = (reader.IsDBNull(((int)KpiAuditColumn.IpRdi - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpRdi - 1)];
			entity.IpLti = (reader.IsDBNull(((int)KpiAuditColumn.IpLti - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpLti - 1)];
			entity.IpIfe = (reader.IsDBNull(((int)KpiAuditColumn.IpIfe - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IpIfe - 1)];
			entity.EhspNoLwd = (reader.IsDBNull(((int)KpiAuditColumn.EhspNoLwd - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.EhspNoLwd - 1)];
			entity.EhspNoRd = (reader.IsDBNull(((int)KpiAuditColumn.EhspNoRd - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.EhspNoRd - 1)];
			entity.EhsAudits = (reader.IsDBNull(((int)KpiAuditColumn.EhsAudits - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.EhsAudits - 1)];
			entity.EhsCorrective = (reader.IsDBNull(((int)KpiAuditColumn.EhsCorrective - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.EhsCorrective - 1)];
			entity.JsaAudits = (reader.IsDBNull(((int)KpiAuditColumn.JsaAudits - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.JsaAudits - 1)];
			entity.IWsc = (reader.IsDBNull(((int)KpiAuditColumn.IWsc - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.IWsc - 1)];
			entity.ONoHswc = (reader.IsDBNull(((int)KpiAuditColumn.ONoHswc - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.ONoHswc - 1)];
			entity.ONoBsp = (reader.IsDBNull(((int)KpiAuditColumn.ONoBsp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.ONoBsp - 1)];
			entity.QQas = (reader.IsDBNull(((int)KpiAuditColumn.QQas - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.QQas - 1)];
			entity.QNoNci = (reader.IsDBNull(((int)KpiAuditColumn.QNoNci - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.QNoNci - 1)];
			entity.MTbmpm = (reader.IsDBNull(((int)KpiAuditColumn.MTbmpm - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MTbmpm - 1)];
			entity.MAwcm = (reader.IsDBNull(((int)KpiAuditColumn.MAwcm - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MAwcm - 1)];
			entity.MAmcm = (reader.IsDBNull(((int)KpiAuditColumn.MAmcm - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MAmcm - 1)];
			entity.MFatality = (reader.IsDBNull(((int)KpiAuditColumn.MFatality - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MFatality - 1)];
			entity.Training = (reader.IsDBNull(((int)KpiAuditColumn.Training - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.Training - 1)];
			entity.MtTolo = (reader.IsDBNull(((int)KpiAuditColumn.MtTolo - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtTolo - 1)];
			entity.MtFp = (reader.IsDBNull(((int)KpiAuditColumn.MtFp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtFp - 1)];
			entity.MtElec = (reader.IsDBNull(((int)KpiAuditColumn.MtElec - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtElec - 1)];
			entity.MtMe = (reader.IsDBNull(((int)KpiAuditColumn.MtMe - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtMe - 1)];
			entity.MtCs = (reader.IsDBNull(((int)KpiAuditColumn.MtCs - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtCs - 1)];
			entity.MtCb = (reader.IsDBNull(((int)KpiAuditColumn.MtCb - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtCb - 1)];
			entity.MtErgo = (reader.IsDBNull(((int)KpiAuditColumn.MtErgo - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtErgo - 1)];
			entity.MtRa = (reader.IsDBNull(((int)KpiAuditColumn.MtRa - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtRa - 1)];
			entity.MtHs = (reader.IsDBNull(((int)KpiAuditColumn.MtHs - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtHs - 1)];
			entity.MtSp = (reader.IsDBNull(((int)KpiAuditColumn.MtSp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtSp - 1)];
			entity.MtIf = (reader.IsDBNull(((int)KpiAuditColumn.MtIf - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtIf - 1)];
			entity.MtHp = (reader.IsDBNull(((int)KpiAuditColumn.MtHp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtHp - 1)];
			entity.MtRp = (reader.IsDBNull(((int)KpiAuditColumn.MtRp - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtRp - 1)];
			entity.MtEnginfo81t = (reader.IsDBNull(((int)KpiAuditColumn.MtEnginfo81t - 1)))?null:(System.Int32?)reader[((int)KpiAuditColumn.MtEnginfo81t - 1)];
			entity.MtOthers = (reader.IsDBNull(((int)KpiAuditColumn.MtOthers - 1)))?null:(System.String)reader[((int)KpiAuditColumn.MtOthers - 1)];
			entity.SafetyPlansSubmitted = (reader.IsDBNull(((int)KpiAuditColumn.SafetyPlansSubmitted - 1)))?null:(System.Boolean?)reader[((int)KpiAuditColumn.SafetyPlansSubmitted - 1)];
			entity.EbiOnSite = (reader.IsDBNull(((int)KpiAuditColumn.EbiOnSite - 1)))?null:(System.Boolean?)reader[((int)KpiAuditColumn.EbiOnSite - 1)];
			entity.ProjectCapital10Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital10Hours - 1)];
			entity.ProjectCapital10Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital10Title - 1)];
			entity.ProjectCapital11Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital11Hours - 1)];
			entity.ProjectCapital11Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital11Title - 1)];
			entity.ProjectCapital12Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital12Hours - 1)];
			entity.ProjectCapital12Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital12Title - 1)];
			entity.ProjectCapital13Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital13Hours - 1)];
			entity.ProjectCapital13Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital13Title - 1)];
			entity.ProjectCapital14Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital14Hours - 1)];
			entity.ProjectCapital14Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital14Title - 1)];
			entity.ProjectCapital15Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital15Hours - 1)];
			entity.ProjectCapital15Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital15Title - 1)];
			entity.ProjectCapital6Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital6Hours - 1)];
			entity.ProjectCapital6Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital6Title - 1)];
			entity.ProjectCapital7Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital7Hours - 1)];
			entity.ProjectCapital7Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital7Title - 1)];
			entity.ProjectCapital8Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital8Hours - 1)];
			entity.ProjectCapital8Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital8Title - 1)];
			entity.ProjectCapital9Hours = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Hours - 1)))?null:(System.Decimal?)reader[((int)KpiAuditColumn.ProjectCapital9Hours - 1)];
			entity.ProjectCapital9Title = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Title - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital9Title - 1)];
			entity.IsSystemEdit = (reader.IsDBNull(((int)KpiAuditColumn.IsSystemEdit - 1)))?null:(System.Boolean?)reader[((int)KpiAuditColumn.IsSystemEdit - 1)];
			entity.ProjectCapital1Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital1Po - 1)];
			entity.ProjectCapital1Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital1Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital1Line - 1)];
			entity.ProjectCapital2Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital2Po - 1)];
			entity.ProjectCapital2Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital2Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital2Line - 1)];
			entity.ProjectCapital3Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital3Po - 1)];
			entity.ProjectCapital3Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital3Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital3Line - 1)];
			entity.ProjectCapital4Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital4Po - 1)];
			entity.ProjectCapital4Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital4Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital4Line - 1)];
			entity.ProjectCapital5Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital5Po - 1)];
			entity.ProjectCapital5Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital5Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital5Line - 1)];
			entity.ProjectCapital6Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital6Po - 1)];
			entity.ProjectCapital6Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital6Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital6Line - 1)];
			entity.ProjectCapital7Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital7Po - 1)];
			entity.ProjectCapital7Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital7Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital7Line - 1)];
			entity.ProjectCapital8Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital8Po - 1)];
			entity.ProjectCapital8Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital8Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital8Line - 1)];
			entity.ProjectCapital9Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital9Po - 1)];
			entity.ProjectCapital9Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital9Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital9Line - 1)];
			entity.ProjectCapital10Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital10Po - 1)];
			entity.ProjectCapital10Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital10Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital10Line - 1)];
			entity.ProjectCapital11Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital11Po - 1)];
			entity.ProjectCapital11Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital11Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital11Line - 1)];
			entity.ProjectCapital12Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital12Po - 1)];
			entity.ProjectCapital12Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital12Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital12Line - 1)];
			entity.ProjectCapital13Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital13Po - 1)];
			entity.ProjectCapital13Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital13Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital13Line - 1)];
			entity.ProjectCapital14Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital14Po - 1)];
			entity.ProjectCapital14Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital14Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital14Line - 1)];
			entity.ProjectCapital15Po = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Po - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital15Po - 1)];
			entity.ProjectCapital15Line = (reader.IsDBNull(((int)KpiAuditColumn.ProjectCapital15Line - 1)))?null:(System.String)reader[((int)KpiAuditColumn.ProjectCapital15Line - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)KpiAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)KpiAuditColumn.AuditEventId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.KpiAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.KpiId = Convert.IsDBNull(dataRow["KpiId"]) ? null : (System.Int32?)dataRow["KpiId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.CreatedbyUserId = Convert.IsDBNull(dataRow["CreatedbyUserId"]) ? null : (System.Int32?)dataRow["CreatedbyUserId"];
			entity.ModifiedbyUserId = Convert.IsDBNull(dataRow["ModifiedbyUserId"]) ? null : (System.Int32?)dataRow["ModifiedbyUserId"];
			entity.DateAdded = Convert.IsDBNull(dataRow["DateAdded"]) ? null : (System.DateTime?)dataRow["DateAdded"];
			entity.Datemodified = Convert.IsDBNull(dataRow["Datemodified"]) ? null : (System.DateTime?)dataRow["Datemodified"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.KpiDateTime = Convert.IsDBNull(dataRow["kpiDateTime"]) ? null : (System.DateTime?)dataRow["kpiDateTime"];
			entity.KpiGeneral = Convert.IsDBNull(dataRow["kpiGeneral"]) ? null : (System.Decimal?)dataRow["kpiGeneral"];
			entity.KpiCalcinerExpense = Convert.IsDBNull(dataRow["kpiCalcinerExpense"]) ? null : (System.Decimal?)dataRow["kpiCalcinerExpense"];
			entity.KpiCalcinerCapital = Convert.IsDBNull(dataRow["kpiCalcinerCapital"]) ? null : (System.Decimal?)dataRow["kpiCalcinerCapital"];
			entity.KpiResidue = Convert.IsDBNull(dataRow["kpiResidue"]) ? null : (System.Decimal?)dataRow["kpiResidue"];
			entity.ProjectCapital1Title = Convert.IsDBNull(dataRow["projectCapital1Title"]) ? null : (System.String)dataRow["projectCapital1Title"];
			entity.ProjectCapital1Hours = Convert.IsDBNull(dataRow["projectCapital1Hours"]) ? null : (System.Decimal?)dataRow["projectCapital1Hours"];
			entity.ProjectCapital2Title = Convert.IsDBNull(dataRow["projectCapital2Title"]) ? null : (System.String)dataRow["projectCapital2Title"];
			entity.ProjectCapital2Hours = Convert.IsDBNull(dataRow["projectCapital2Hours"]) ? null : (System.Decimal?)dataRow["projectCapital2Hours"];
			entity.ProjectCapital3Title = Convert.IsDBNull(dataRow["projectCapital3Title"]) ? null : (System.String)dataRow["projectCapital3Title"];
			entity.ProjectCapital3Hours = Convert.IsDBNull(dataRow["projectCapital3Hours"]) ? null : (System.Decimal?)dataRow["projectCapital3Hours"];
			entity.ProjectCapital4Title = Convert.IsDBNull(dataRow["projectCapital4Title"]) ? null : (System.String)dataRow["projectCapital4Title"];
			entity.ProjectCapital4Hours = Convert.IsDBNull(dataRow["projectCapital4Hours"]) ? null : (System.Decimal?)dataRow["projectCapital4Hours"];
			entity.ProjectCapital5Title = Convert.IsDBNull(dataRow["projectCapital5Title"]) ? null : (System.String)dataRow["projectCapital5Title"];
			entity.ProjectCapital5Hours = Convert.IsDBNull(dataRow["projectCapital5Hours"]) ? null : (System.Decimal?)dataRow["projectCapital5Hours"];
			entity.AheaRefineryWork = Convert.IsDBNull(dataRow["aheaRefineryWork"]) ? null : (System.Decimal?)dataRow["aheaRefineryWork"];
			entity.AheaResidueWork = Convert.IsDBNull(dataRow["aheaResidueWork"]) ? null : (System.Decimal?)dataRow["aheaResidueWork"];
			entity.AheaSmeltingWork = Convert.IsDBNull(dataRow["aheaSmeltingWork"]) ? null : (System.Decimal?)dataRow["aheaSmeltingWork"];
			entity.AheaPowerGenerationWork = Convert.IsDBNull(dataRow["aheaPowerGenerationWork"]) ? null : (System.Decimal?)dataRow["aheaPowerGenerationWork"];
			entity.AheaTotalManHours = Convert.IsDBNull(dataRow["aheaTotalManHours"]) ? null : (System.Decimal?)dataRow["aheaTotalManHours"];
			entity.AheaPeakNopplSiteWeek = Convert.IsDBNull(dataRow["aheaPeakNopplSiteWeek"]) ? null : (System.Int32?)dataRow["aheaPeakNopplSiteWeek"];
			entity.AheaAvgNopplSiteMonth = Convert.IsDBNull(dataRow["aheaAvgNopplSiteMonth"]) ? null : (System.Int32?)dataRow["aheaAvgNopplSiteMonth"];
			entity.AheaNoEmpExcessMonth = Convert.IsDBNull(dataRow["aheaNoEmpExcessMonth"]) ? null : (System.Int32?)dataRow["aheaNoEmpExcessMonth"];
			entity.IpFati = Convert.IsDBNull(dataRow["ipFATI"]) ? null : (System.Int32?)dataRow["ipFATI"];
			entity.IpMti = Convert.IsDBNull(dataRow["ipMTI"]) ? null : (System.Int32?)dataRow["ipMTI"];
			entity.IpRdi = Convert.IsDBNull(dataRow["ipRDI"]) ? null : (System.Int32?)dataRow["ipRDI"];
			entity.IpLti = Convert.IsDBNull(dataRow["ipLTI"]) ? null : (System.Int32?)dataRow["ipLTI"];
			entity.IpIfe = Convert.IsDBNull(dataRow["ipIFE"]) ? null : (System.Int32?)dataRow["ipIFE"];
			entity.EhspNoLwd = Convert.IsDBNull(dataRow["ehspNoLWD"]) ? null : (System.Decimal?)dataRow["ehspNoLWD"];
			entity.EhspNoRd = Convert.IsDBNull(dataRow["ehspNoRD"]) ? null : (System.Decimal?)dataRow["ehspNoRD"];
			entity.EhsAudits = Convert.IsDBNull(dataRow["ehsAudits"]) ? null : (System.Int32?)dataRow["ehsAudits"];
			entity.EhsCorrective = Convert.IsDBNull(dataRow["ehsCorrective"]) ? null : (System.Int32?)dataRow["ehsCorrective"];
			entity.JsaAudits = Convert.IsDBNull(dataRow["JSAAudits"]) ? null : (System.Int32?)dataRow["JSAAudits"];
			entity.IWsc = Convert.IsDBNull(dataRow["iWSC"]) ? null : (System.Int32?)dataRow["iWSC"];
			entity.ONoHswc = Convert.IsDBNull(dataRow["oNoHSWC"]) ? null : (System.Int32?)dataRow["oNoHSWC"];
			entity.ONoBsp = Convert.IsDBNull(dataRow["oNoBSP"]) ? null : (System.Int32?)dataRow["oNoBSP"];
			entity.QQas = Convert.IsDBNull(dataRow["qQAS"]) ? null : (System.Decimal?)dataRow["qQAS"];
			entity.QNoNci = Convert.IsDBNull(dataRow["qNoNCI"]) ? null : (System.Int32?)dataRow["qNoNCI"];
			entity.MTbmpm = Convert.IsDBNull(dataRow["mTbmpm"]) ? null : (System.Int32?)dataRow["mTbmpm"];
			entity.MAwcm = Convert.IsDBNull(dataRow["mAwcm"]) ? null : (System.Int32?)dataRow["mAwcm"];
			entity.MAmcm = Convert.IsDBNull(dataRow["mAmcm"]) ? null : (System.Int32?)dataRow["mAmcm"];
			entity.MFatality = Convert.IsDBNull(dataRow["mFatality"]) ? null : (System.Int32?)dataRow["mFatality"];
			entity.Training = Convert.IsDBNull(dataRow["Training"]) ? null : (System.Int32?)dataRow["Training"];
			entity.MtTolo = Convert.IsDBNull(dataRow["mtTolo"]) ? null : (System.Int32?)dataRow["mtTolo"];
			entity.MtFp = Convert.IsDBNull(dataRow["mtFp"]) ? null : (System.Int32?)dataRow["mtFp"];
			entity.MtElec = Convert.IsDBNull(dataRow["mtElec"]) ? null : (System.Int32?)dataRow["mtElec"];
			entity.MtMe = Convert.IsDBNull(dataRow["mtMe"]) ? null : (System.Int32?)dataRow["mtMe"];
			entity.MtCs = Convert.IsDBNull(dataRow["mtCs"]) ? null : (System.Int32?)dataRow["mtCs"];
			entity.MtCb = Convert.IsDBNull(dataRow["mtCb"]) ? null : (System.Int32?)dataRow["mtCb"];
			entity.MtErgo = Convert.IsDBNull(dataRow["mtErgo"]) ? null : (System.Int32?)dataRow["mtErgo"];
			entity.MtRa = Convert.IsDBNull(dataRow["mtRa"]) ? null : (System.Int32?)dataRow["mtRa"];
			entity.MtHs = Convert.IsDBNull(dataRow["mtHs"]) ? null : (System.Int32?)dataRow["mtHs"];
			entity.MtSp = Convert.IsDBNull(dataRow["mtSp"]) ? null : (System.Int32?)dataRow["mtSp"];
			entity.MtIf = Convert.IsDBNull(dataRow["mtIf"]) ? null : (System.Int32?)dataRow["mtIf"];
			entity.MtHp = Convert.IsDBNull(dataRow["mtHp"]) ? null : (System.Int32?)dataRow["mtHp"];
			entity.MtRp = Convert.IsDBNull(dataRow["mtRp"]) ? null : (System.Int32?)dataRow["mtRp"];
			entity.MtEnginfo81t = Convert.IsDBNull(dataRow["mtENGINFO81t"]) ? null : (System.Int32?)dataRow["mtENGINFO81t"];
			entity.MtOthers = Convert.IsDBNull(dataRow["mtOthers"]) ? null : (System.String)dataRow["mtOthers"];
			entity.SafetyPlansSubmitted = Convert.IsDBNull(dataRow["SafetyPlansSubmitted"]) ? null : (System.Boolean?)dataRow["SafetyPlansSubmitted"];
			entity.EbiOnSite = Convert.IsDBNull(dataRow["EbiOnSite"]) ? null : (System.Boolean?)dataRow["EbiOnSite"];
			entity.ProjectCapital10Hours = Convert.IsDBNull(dataRow["projectCapital10Hours"]) ? null : (System.Decimal?)dataRow["projectCapital10Hours"];
			entity.ProjectCapital10Title = Convert.IsDBNull(dataRow["projectCapital10Title"]) ? null : (System.String)dataRow["projectCapital10Title"];
			entity.ProjectCapital11Hours = Convert.IsDBNull(dataRow["projectCapital11Hours"]) ? null : (System.Decimal?)dataRow["projectCapital11Hours"];
			entity.ProjectCapital11Title = Convert.IsDBNull(dataRow["projectCapital11Title"]) ? null : (System.String)dataRow["projectCapital11Title"];
			entity.ProjectCapital12Hours = Convert.IsDBNull(dataRow["projectCapital12Hours"]) ? null : (System.Decimal?)dataRow["projectCapital12Hours"];
			entity.ProjectCapital12Title = Convert.IsDBNull(dataRow["projectCapital12Title"]) ? null : (System.String)dataRow["projectCapital12Title"];
			entity.ProjectCapital13Hours = Convert.IsDBNull(dataRow["projectCapital13Hours"]) ? null : (System.Decimal?)dataRow["projectCapital13Hours"];
			entity.ProjectCapital13Title = Convert.IsDBNull(dataRow["projectCapital13Title"]) ? null : (System.String)dataRow["projectCapital13Title"];
			entity.ProjectCapital14Hours = Convert.IsDBNull(dataRow["projectCapital14Hours"]) ? null : (System.Decimal?)dataRow["projectCapital14Hours"];
			entity.ProjectCapital14Title = Convert.IsDBNull(dataRow["projectCapital14Title"]) ? null : (System.String)dataRow["projectCapital14Title"];
			entity.ProjectCapital15Hours = Convert.IsDBNull(dataRow["projectCapital15Hours"]) ? null : (System.Decimal?)dataRow["projectCapital15Hours"];
			entity.ProjectCapital15Title = Convert.IsDBNull(dataRow["projectCapital15Title"]) ? null : (System.String)dataRow["projectCapital15Title"];
			entity.ProjectCapital6Hours = Convert.IsDBNull(dataRow["projectCapital6Hours"]) ? null : (System.Decimal?)dataRow["projectCapital6Hours"];
			entity.ProjectCapital6Title = Convert.IsDBNull(dataRow["projectCapital6Title"]) ? null : (System.String)dataRow["projectCapital6Title"];
			entity.ProjectCapital7Hours = Convert.IsDBNull(dataRow["projectCapital7Hours"]) ? null : (System.Decimal?)dataRow["projectCapital7Hours"];
			entity.ProjectCapital7Title = Convert.IsDBNull(dataRow["projectCapital7Title"]) ? null : (System.String)dataRow["projectCapital7Title"];
			entity.ProjectCapital8Hours = Convert.IsDBNull(dataRow["projectCapital8Hours"]) ? null : (System.Decimal?)dataRow["projectCapital8Hours"];
			entity.ProjectCapital8Title = Convert.IsDBNull(dataRow["projectCapital8Title"]) ? null : (System.String)dataRow["projectCapital8Title"];
			entity.ProjectCapital9Hours = Convert.IsDBNull(dataRow["projectCapital9Hours"]) ? null : (System.Decimal?)dataRow["projectCapital9Hours"];
			entity.ProjectCapital9Title = Convert.IsDBNull(dataRow["projectCapital9Title"]) ? null : (System.String)dataRow["projectCapital9Title"];
			entity.IsSystemEdit = Convert.IsDBNull(dataRow["IsSystemEdit"]) ? null : (System.Boolean?)dataRow["IsSystemEdit"];
			entity.ProjectCapital1Po = Convert.IsDBNull(dataRow["projectCapital1Po"]) ? null : (System.String)dataRow["projectCapital1Po"];
			entity.ProjectCapital1Line = Convert.IsDBNull(dataRow["projectCapital1Line"]) ? null : (System.String)dataRow["projectCapital1Line"];
			entity.ProjectCapital2Po = Convert.IsDBNull(dataRow["projectCapital2Po"]) ? null : (System.String)dataRow["projectCapital2Po"];
			entity.ProjectCapital2Line = Convert.IsDBNull(dataRow["projectCapital2Line"]) ? null : (System.String)dataRow["projectCapital2Line"];
			entity.ProjectCapital3Po = Convert.IsDBNull(dataRow["projectCapital3Po"]) ? null : (System.String)dataRow["projectCapital3Po"];
			entity.ProjectCapital3Line = Convert.IsDBNull(dataRow["projectCapital3Line"]) ? null : (System.String)dataRow["projectCapital3Line"];
			entity.ProjectCapital4Po = Convert.IsDBNull(dataRow["projectCapital4Po"]) ? null : (System.String)dataRow["projectCapital4Po"];
			entity.ProjectCapital4Line = Convert.IsDBNull(dataRow["projectCapital4Line"]) ? null : (System.String)dataRow["projectCapital4Line"];
			entity.ProjectCapital5Po = Convert.IsDBNull(dataRow["projectCapital5Po"]) ? null : (System.String)dataRow["projectCapital5Po"];
			entity.ProjectCapital5Line = Convert.IsDBNull(dataRow["projectCapital5Line"]) ? null : (System.String)dataRow["projectCapital5Line"];
			entity.ProjectCapital6Po = Convert.IsDBNull(dataRow["projectCapital6Po"]) ? null : (System.String)dataRow["projectCapital6Po"];
			entity.ProjectCapital6Line = Convert.IsDBNull(dataRow["projectCapital6Line"]) ? null : (System.String)dataRow["projectCapital6Line"];
			entity.ProjectCapital7Po = Convert.IsDBNull(dataRow["projectCapital7Po"]) ? null : (System.String)dataRow["projectCapital7Po"];
			entity.ProjectCapital7Line = Convert.IsDBNull(dataRow["projectCapital7Line"]) ? null : (System.String)dataRow["projectCapital7Line"];
			entity.ProjectCapital8Po = Convert.IsDBNull(dataRow["projectCapital8Po"]) ? null : (System.String)dataRow["projectCapital8Po"];
			entity.ProjectCapital8Line = Convert.IsDBNull(dataRow["projectCapital8Line"]) ? null : (System.String)dataRow["projectCapital8Line"];
			entity.ProjectCapital9Po = Convert.IsDBNull(dataRow["projectCapital9Po"]) ? null : (System.String)dataRow["projectCapital9Po"];
			entity.ProjectCapital9Line = Convert.IsDBNull(dataRow["projectCapital9Line"]) ? null : (System.String)dataRow["projectCapital9Line"];
			entity.ProjectCapital10Po = Convert.IsDBNull(dataRow["projectCapital10Po"]) ? null : (System.String)dataRow["projectCapital10Po"];
			entity.ProjectCapital10Line = Convert.IsDBNull(dataRow["projectCapital10Line"]) ? null : (System.String)dataRow["projectCapital10Line"];
			entity.ProjectCapital11Po = Convert.IsDBNull(dataRow["projectCapital11Po"]) ? null : (System.String)dataRow["projectCapital11Po"];
			entity.ProjectCapital11Line = Convert.IsDBNull(dataRow["projectCapital11Line"]) ? null : (System.String)dataRow["projectCapital11Line"];
			entity.ProjectCapital12Po = Convert.IsDBNull(dataRow["projectCapital12Po"]) ? null : (System.String)dataRow["projectCapital12Po"];
			entity.ProjectCapital12Line = Convert.IsDBNull(dataRow["projectCapital12Line"]) ? null : (System.String)dataRow["projectCapital12Line"];
			entity.ProjectCapital13Po = Convert.IsDBNull(dataRow["projectCapital13Po"]) ? null : (System.String)dataRow["projectCapital13Po"];
			entity.ProjectCapital13Line = Convert.IsDBNull(dataRow["projectCapital13Line"]) ? null : (System.String)dataRow["projectCapital13Line"];
			entity.ProjectCapital14Po = Convert.IsDBNull(dataRow["projectCapital14Po"]) ? null : (System.String)dataRow["projectCapital14Po"];
			entity.ProjectCapital14Line = Convert.IsDBNull(dataRow["projectCapital14Line"]) ? null : (System.String)dataRow["projectCapital14Line"];
			entity.ProjectCapital15Po = Convert.IsDBNull(dataRow["projectCapital15Po"]) ? null : (System.String)dataRow["projectCapital15Po"];
			entity.ProjectCapital15Line = Convert.IsDBNull(dataRow["projectCapital15Line"]) ? null : (System.String)dataRow["projectCapital15Line"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.KpiAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.KpiAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region KpiAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.KpiAudit</c>
	///</summary>
	public enum KpiAuditChildEntityTypes
	{
	}
	
	#endregion KpiAuditChildEntityTypes
	
	#region KpiAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;KpiAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditFilterBuilder : SqlFilterBuilder<KpiAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditFilterBuilder class.
		/// </summary>
		public KpiAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiAuditFilterBuilder
	
	#region KpiAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;KpiAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditParameterBuilder : ParameterizedSqlFilterBuilder<KpiAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditParameterBuilder class.
		/// </summary>
		public KpiAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiAuditParameterBuilder
	
	#region KpiAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;KpiAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiAuditSortBuilder : SqlSortBuilder<KpiAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditSqlSortBuilder class.
		/// </summary>
		public KpiAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiAuditSortBuilder
	
} // end namespace
