﻿using ALCOA.CSMS.Batch.Common;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
using repoView = Repo.CSMS.Service.Database.View;


namespace ALCOA.CSMS.BatchJobScripts.Code
{
    public class RadarHelper
    {
        public int YearMinusOneMonth { get; set; }
        public int MonthMinusOneMonth { get; set; }
        public DateTime dtStartOfMonth { get; set; }
        private repo.ICompanySiteCategoryStandardService companySiteCategoryStandardService { get; set; }
        private repo.ICSAService csaService { get; set; }
        private repo.IRegionsSiteService regionSiteService { get; set; }
        private repo.ISiteService siteService { get; set; }
        private repo.IAdhocRadarItemService ahriService { get; set; }
        private repo.ICompanySiteCategoryService cscService { get; set; }
        private repo.ICompanyService companyService { get; set; }
        private repo.IRegionService regionService { get; set; }
        private repo.IAnnualTargetService annualTargetService { get; set; }
        private repo.ICompanySiteCategoryStandardHistoryService companySiteCategoryStandardHistoryService { get; set; }
        private repo.IKpiService kpiService { get; set; }
        private repoView.ISafetyPlansWithoutFileHashService safetyPlansService { get; set; }
        private repoView.IKpiCompanySiteCategoryService kpicscService { get; set; }
        public RadarHelper()
        {
            DependencyConfig.RegisterDependencies(new ContainerBuilder());
            DateTime dtMinusOneMonth = DateTime.Now.AddMonths(-1);  
            //DateTime dtMinusOneMonth = new DateTime(2016,2,1);
            YearMinusOneMonth = dtMinusOneMonth.Year;
            MonthMinusOneMonth = dtMinusOneMonth.Month;
            dtStartOfMonth = new DateTime(YearMinusOneMonth, MonthMinusOneMonth, 1);
            companySiteCategoryStandardService = DependencyResolver.Current.GetService<repo.ICompanySiteCategoryStandardService>();
            csaService = DependencyResolver.Current.GetService<repo.ICSAService>();
            regionSiteService = DependencyResolver.Current.GetService<repo.IRegionsSiteService>();
            siteService = DependencyResolver.Current.GetService<repo.ISiteService>();
            ahriService = DependencyResolver.Current.GetService<repo.IAdhocRadarItemService>();
            cscService = DependencyResolver.Current.GetService<repo.ICompanySiteCategoryService>();
            companyService = DependencyResolver.Current.GetService<repo.ICompanyService>();
            kpicscService = DependencyResolver.Current.GetService<repoView.IKpiCompanySiteCategoryService>();
            regionService = DependencyResolver.Current.GetService<repo.IRegionService>();
            annualTargetService = DependencyResolver.Current.GetService<repo.IAnnualTargetService>();
            kpiService = DependencyResolver.Current.GetService<repo.IKpiService>();
            safetyPlansService = DependencyResolver.Current.GetService<repoView.ISafetyPlansWithoutFileHashService>();
            companySiteCategoryStandardHistoryService = DependencyResolver.Current.GetService<repo.ICompanySiteCategoryStandardHistoryService>();
        }

        public void RecordCompanySiteCategoryStandardHistory()
        {
            //Transfers the companysitecategory for each company into the companySiteCategoryStandardHistoryTable
            //this can only be run for the previous month and cannot be backdated, which is why it gets the current date time
            //the other methods can be back dated.
            List<model.CompanySiteCategoryStandard> cscsList = companySiteCategoryStandardService.GetMany(null, null, null, null);
            DateTime currentDateTimeMinus1 = DateTime.Now.AddMonths(-1);

            foreach (model.CompanySiteCategoryStandard cscs in cscsList)
            {
                model.CompanySiteCategoryStandardHistory cscsh = new model.CompanySiteCategoryStandardHistory();
                cscsh.CompanyId = cscs.CompanyId;
                cscsh.SiteId = cscs.SiteId;
                cscsh.CompanySiteCategoryId = cscs.CompanySiteCategoryId;
                cscsh.RecordDate = new DateTime(currentDateTimeMinus1.Year, currentDateTimeMinus1.Month, 1);
                companySiteCategoryStandardHistoryService.SaveOrUpdate(cscsh);
            }
        }

        public string IsCompanyCSACompliantForPreviousMonth(int companyId, int siteId)
        {
            //model.CompanySiteCategoryStandard cscs = companySiteCategoryStandardService.Get(i => i.CompanyId == companyId && i.SiteId == siteId, null);

            model.CompanySiteCategoryStandardHistory cscsh = companySiteCategoryStandardHistoryService.Get(i => i.CompanyId == companyId && i.SiteId == siteId && i.RecordDate == dtStartOfMonth, null);

            if (cscsh.CompanySiteCategoryId >= 3)
                return "n/a";

            CSAQuarter csaQtr = new CSAQuarter(new DateTime(this.YearMinusOneMonth, this.MonthMinusOneMonth,1),cscsh.CompanySiteCategoryId.Value);
            model.CSA csaCurrent = csaService.Get(r => r.CompanyId == companyId && r.SiteId == siteId && r.Year == csaQtr.CurrentQuarterYear && r.QtrId == csaQtr.CurrentQuarter,null);
            model.CSA csaPrevious = csaService.Get(r => r.CompanyId == companyId && r.SiteId == siteId && r.Year == csaQtr.PreviousQuarterYear && r.QtrId == csaQtr.PreviousQuarter, null);
            if (csaCurrent != null || csaPrevious != null)
                return "true";
            else
                return "false";
        }
        public int GetNumberOfCompaniesCSACompliant(int siteId, int companySiteCategoryId)
        {
            //List<model.CompanySiteCategoryStandard> lst = companySiteCategoryStandardService.GetMany(null, i => i.SiteId == siteId && i.CompanySiteCategoryId == companySiteCategoryId, null, null);

            List<model.CompanySiteCategoryStandardHistory> lsth = companySiteCategoryStandardHistoryService.GetMany(null, i => i.SiteId == siteId && i.CompanySiteCategoryId == companySiteCategoryId && i.RecordDate == dtStartOfMonth, null, null);
            int compliantCount = 0;
            foreach (model.CompanySiteCategoryStandardHistory csc in lsth)
            {
                if (IsCompanyCSACompliantForPreviousMonth(csc.CompanyId,csc.SiteId) == "true")
                  compliantCount++;
            }
            return compliantCount;
        }
        // TODO !####! DEBUG my new version of function with includes set
        public string TEN_FIXED_GetCompaniesNotCSACompliant(int siteId, int companySiteCategoryId)
        {
            List<Expression<Func<model.CompanySiteCategoryStandardHistory, object>>> includes = new List<Expression<Func<model.CompanySiteCategoryStandardHistory, object>>>();
            includes.Add(i => i.Company);

            List<model.CompanySiteCategoryStandardHistory> lsth
                = companySiteCategoryStandardHistoryService.GetMany(null, i => i.SiteId == siteId &&
                                                                          i.CompanySiteCategoryId == companySiteCategoryId &&
                                                                          i.RecordDate == dtStartOfMonth, null, includes);
            string nonCompliantList = "";
            foreach(model.CompanySiteCategoryStandardHistory csc in lsth)
            {
                // TODO !####! DEBUG sort this out once we know more
                ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                    string.Format("DEBUG_INFO: TEN_FIXED_GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  CompanyId:{2}  RecordDate:{3}  CompanyName:{4}",
                    siteId, companySiteCategoryId, csc.CompanyId, csc.RecordDate.ToString("yyyy-MM-dd HH:mm:ss"), csc.Company == null ? "NULL" : csc.Company.CompanyName));
                // TODO !####! DEBUG END

                if(IsCompanyCSACompliantForPreviousMonth(csc.CompanyId, csc.SiteId) == "false")
                {
                    // TODO !####! DEBUG sort this out once we know more
                    if(csc.Company == null)
                    {
                        ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                            string.Format("DEBUG_ERROR: TEN_FIXED_GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  CompanyId:{2}  RecordDate:{3} - csc.Company IS NULL",
                                          siteId, companySiteCategoryId, csc.CompanyId, csc.RecordDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    }
                    else if(csc.Company.CompanyName == null)
                    {
                        ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                            string.Format("DEBUG_ERROR: TEN_FIXED_GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  CompanyId:{2}  RecordDate:{3} - csc.Company.CompanyName IS NULL",
                                          siteId, companySiteCategoryId, csc.CompanyId, csc.RecordDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    }
                    // TODO !####! DEBUG END
                    else
                    {
                        nonCompliantList = nonCompliantList + ", " + csc.Company.CompanyName;
                    }
                }
            }
            if(nonCompliantList.Length > 0)
                nonCompliantList = nonCompliantList.Substring(2, nonCompliantList.Length - 2);

            // TODO !####! DEBUG sort this out once we know more
            ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                string.Format("DEBUG_ERROR: TEN_FIXED_GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  NonCompliantList:{2}", siteId, companySiteCategoryId, nonCompliantList));
            // TODO !####! DEBUG END
            return nonCompliantList;
        }
        // TODO !####! DEBUG END my new version of function with includes set
        public string GetCompaniesNotCSACompliant(int siteId, int companySiteCategoryId)
        {
            // TODO !####! DEBUG sort this out once we know more
            TEN_FIXED_GetCompaniesNotCSACompliant(siteId, companySiteCategoryId);
            // TODO !####! DEBUG END

            List<model.CompanySiteCategoryStandardHistory> lsth
                = companySiteCategoryStandardHistoryService.GetMany(null, i => i.SiteId == siteId &&
                                                                          i.CompanySiteCategoryId == companySiteCategoryId &&
                                                                          i.RecordDate == dtStartOfMonth, null, null);
            string nonCompliantList = "";
            foreach (model.CompanySiteCategoryStandardHistory csc in lsth)
            {
                // TODO !####! DEBUG sort this out once we know more
                ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                    string.Format("DEBUG_INFO: GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  CompanyId:{2}  RecordDate:{3}  CompanyName:{4}",
                                  siteId, companySiteCategoryId, csc.CompanyId, csc.RecordDate.ToString("yyyy-MM-dd HH:mm:ss"), csc.Company == null ? "NULL" : csc.Company.CompanyName));
                // TODO !####! DEBUG END

                if(IsCompanyCSACompliantForPreviousMonth(csc.CompanyId, csc.SiteId) == "false")
                {
                    // TODO !####! DEBUG sort this out once we know more
                    if(csc.Company == null)
                    {
                        ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                            string.Format("DEBUG_ERROR: GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  CompanyId:{2}  RecordDate:{3} - csc.Company IS NULL",
                                          siteId, companySiteCategoryId, csc.CompanyId, csc.RecordDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    }
                    else if(csc.Company.CompanyName == null)
                    {
                        ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                            string.Format("DEBUG_ERROR: GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  CompanyId:{2}  RecordDate:{3} - csc.Company.CompanyName IS NULL",
                                          siteId, companySiteCategoryId, csc.CompanyId, csc.RecordDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    }
                    // TODO !####! DEBUG END
                    else
                    {
                        nonCompliantList = nonCompliantList + ", " + csc.Company.CompanyName;
                    }
                }
            }
            if (nonCompliantList.Length > 0)
                nonCompliantList = nonCompliantList.Substring(2, nonCompliantList.Length - 2);

            // TODO !####! DEBUG sort this out once we know more
            ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                string.Format("DEBUG_ERROR: GetCompaniesNotCSACompliant  SiteId:{0}  CompanySiteCategoryId:{1}  NonCompliantList:{2}", siteId, companySiteCategoryId, nonCompliantList));
            // TODO !####! DEBUG END
            return nonCompliantList;
        }
        public int GetNumberOfCompaniesCSACompliantByRegion(int regionId, int companySiteCategoryId)
        {
            List<model.RegionsSite> regionSites = regionSiteService.GetMany(null, s => s.RegionId == regionId, null, null);
            int countCompanies = 0;
            foreach (model.RegionsSite rs in regionSites)
            {
                countCompanies += GetNumberOfCompaniesCSACompliant(rs.SiteId, companySiteCategoryId);
            }
            return countCompanies;
        }
        public int GetPercentageOfCompaniesCSACompliant(int siteId, int companySiteCategoryId)
        {
            int activeCompanies = GetNumberOfCompanies(siteId, companySiteCategoryId);
            if (activeCompanies == 0)
                return 0;
            else
            {
                int compliantCount = GetNumberOfCompaniesCSACompliant(siteId, companySiteCategoryId);
                int returnValue = 100 * compliantCount / activeCompanies;
                if (returnValue > 100)
                    return 100;
                else
                    return returnValue;
            }
        }
        public int GetPercentageOfCompaniesCSACompliantByRegion(int regionId, int companySiteCategoryId)
        {
            int activeCompanies = GetNumberOfCompaniesByRegion(regionId, companySiteCategoryId);
            if (activeCompanies == 0)
                return 0;
            else
            {
                int compliantCount = GetNumberOfCompaniesCSACompliantByRegion(regionId, companySiteCategoryId);
                int returnValue = 100 * compliantCount / activeCompanies;
                if (returnValue > 100)
                    return 100;
                else
                    return returnValue;
            }
        }

        

        public void UpdatePercentageCSACompliantAllSites()
        {
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            DateTime currentdtMinusOneMonth = DateTime.Now.AddMonths(-1);
            DateTime startOfCurrentPreviousMonth = new DateTime(currentdtMinusOneMonth.Year, currentdtMinusOneMonth.Month, 1);

            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "CSA - Self Audit (Conducted)", null);
                    int numberOfCompanies = this.GetPercentageOfCompaniesCSACompliant(site.SiteId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        //Update the YTD for CSA also if it is being run for the current month
                        if (dtStartOfMonth == startOfCurrentPreviousMonth)
                            ahri.KpiScoreYtd = numberOfCompaniesText;
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }
        public void UpdatePercentageCSACompliantAllRegions()
        {
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            List<model.Region> regions = regionService.GetMany(null, r => r.RegionSiteId != null, null, null);
            DateTime currentdtMinusOneMonth = DateTime.Now.AddMonths(-1);
            DateTime startOfCurrentPreviousMonth = new DateTime(currentdtMinusOneMonth.Year, currentdtMinusOneMonth.Month, 1);
            foreach (model.Region region in regions)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == region.RegionSiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "CSA - Self Audit (Conducted)", null);
                    int numberOfCompanies = this.GetPercentageOfCompaniesCSACompliantByRegion(region.RegionId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        //Update the YTD for CSA also if it is for the current month.
                        if (dtStartOfMonth == startOfCurrentPreviousMonth)
                            ahri.KpiScoreYtd = numberOfCompaniesText;
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }
        public void UpdateCompaniesNotCSACompliantAllSites()
        {
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            DateTime currentdtMinusOneMonth = DateTime.Now.AddMonths(-1);
            DateTime startOfCurrentPreviousMonth = new DateTime(currentdtMinusOneMonth.Year, currentdtMinusOneMonth.Month, 1);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "CSA - Self Audit (Conducted)", null);

                    // TODO !####! DEBUG sort this out once we know more
                    ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogError(
                        string.Format("DEBUG_INFO: UpdateCompaniesNotCSACompliantAllSites  SiteId:{0}  CompanySiteCategoryId:{1}  ModifiedDate:{2}",
                                      site.SiteId, csc.CompanySiteCategoryId, ahri.ModifiedDate.ToString("yyyy-MM-dd HH:mm:ss")));
                    // TODO !####! DEBUG END

                    string companiesNotCompliant = this.GetCompaniesNotCSACompliant(site.SiteId, csc.CompanySiteCategoryId);
                    if (ahri != null)
                    {
                        this.UpdateAhriObjectCompaniesNotCompliantForMonth(ahri, MonthMinusOneMonth, companiesNotCompliant);
                        //Update the YTD for CSA also
                        if (dtStartOfMonth == startOfCurrentPreviousMonth)
                            ahri.CompaniesNotCompliant = companiesNotCompliant;
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }
        //Number of Companies

        public int GetNumberOfCompanies(int siteId, int companySiteCategoryId)
        {
            //List<model.CompanySiteCategoryStandard> lst = companySiteCategoryStandardService.GetMany(null, i => i.SiteId == siteId && i.CompanySiteCategoryId == companySiteCategoryId, null, null);
            List<model.CompanySiteCategoryStandardHistory> lst = companySiteCategoryStandardHistoryService.GetMany(null, i => i.SiteId == siteId && i.CompanySiteCategoryId == companySiteCategoryId && i.RecordDate == dtStartOfMonth, null, null);
            return lst.Count();
        }
        public int GetNumberOfCompaniesByRegion(int regionId, int companySiteCategoryId)
        {
            List<model.RegionsSite> regionSites = regionSiteService.GetMany(null, s=>s.RegionId == regionId,null,null);
            int countCompanies = 0;
            foreach (model.RegionsSite rs in regionSites)
            {
                countCompanies += GetNumberOfCompanies(rs.SiteId, companySiteCategoryId);
            }
            return countCompanies;
        }
        public void UpdateNumberOfCompaniesAllSites()
        {
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "Number Of Companies:", null);
                    int numberOfCompanies = this.GetNumberOfCompanies(site.SiteId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }
        public void UpdateNumberOfCompaniesAllRegions()
        {
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            List<model.Region> regions = regionService.GetMany(null, r => r.RegionSiteId != null, null, null);
            foreach (model.Region region in regions)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == region.RegionSiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "Number Of Companies:", null);
                    int numberOfCompanies = this.GetNumberOfCompaniesByRegion(region.RegionId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        ahriService.Update(ahri, true);
                    }

                }
            }
        }

        //Companies on Site
        public int GetNumberOfCompaniesOnSiteByRegion(int regionId, int companySiteCategoryId)
        {
            List<model.RegionsSite> regionSites = regionSiteService.GetMany(null, s => s.RegionId == regionId, null, null);
            int countCompanies = 0;
            foreach (model.RegionsSite rs in regionSites)
            {
                countCompanies += GetNumberOfCompaniesOnSite(rs.SiteId, companySiteCategoryId);
            }
            return countCompanies;
        }
        
        public int GetNumberOfCompaniesOnSite(int siteId, int companySiteCategoryId)
        {
            DateTime kpiDateTimeLastMonth = new DateTime(YearMinusOneMonth,MonthMinusOneMonth,1);
            List<model.CompanySiteCategoryStandardHistory> cscshList = companySiteCategoryStandardHistoryService.GetMany(null, c => c.SiteId == siteId && c.CompanySiteCategoryId == companySiteCategoryId && c.RecordDate == kpiDateTimeLastMonth, null, null);
            List<int> lstCompanies = cscshList.Select(cs => cs.CompanyId).ToList();
            
            List<model.Kpi> lstKpi = kpiService.GetMany(null,kpi=>lstCompanies.Contains(kpi.CompanyId) && kpi.kpiDateTime == kpiDateTimeLastMonth && kpi.SiteId == siteId,null,null);

            //List<model.KpiCompanySiteCategory> lst = kpicscService.GetMany(null, i => i.SiteId == siteId && i.CompanySiteCategoryId == companySiteCategoryId && i.kpiDateTime == kpiDateTimeLastMonth && (i.aheaTotalManHours != 0 || i.EbiOnSite == true), null, null);
            int activeCompanies = 0;
            foreach (model.Kpi kcsc in lstKpi)
            {
                model.Company company = companyService.Get(ac => ac.CompanyId == kcsc.CompanyId , null);
                if (company != null)
                    activeCompanies++;
            }
            return activeCompanies;
        }

        public void UpdateNumberOfCompaniesOnSiteAllSites()
        {
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "Number of Companies On Site:", null);
                    int numberOfCompanies = this.GetNumberOfCompaniesOnSite(site.SiteId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }
        public void UpdateNumberOfCompaniesOnSiteAllRegions()
        {
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            List<model.Region> regions = regionService.GetMany(null, r => r.RegionSiteId != null, null, null);
            foreach (model.Region region in regions)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == region.RegionSiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "Number of Companies On Site:", null);
                    int numberOfCompanies = this.GetNumberOfCompaniesOnSiteByRegion(region.RegionId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        ahriService.Update(ahri, true);
                    }

                }
            }
        }


        public void UpdatePercentageOfCompliantCompaniesAllRegions(string fieldName, string kpiName, string annualTargetFieldName)
        {
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            List<model.Region> regions = regionService.GetMany(null, r => r.RegionSiteId != null, null, null);
                        
            foreach (model.Region region in regions)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == region.RegionSiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == kpiName, null);
                    int percentageOfCompanies = this.GetPercentageOfCompaniesCompliantByRegion(region.RegionId, csc.CompanySiteCategoryId, fieldName, annualTargetFieldName);
                    string percentageOfCompaniesText = Convert.ToString(percentageOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, percentageOfCompaniesText);
                        //Update the YTD for CSA also if it is for the current month.
                        
                        ahriService.Update(ahri, true);
                    }

                }
            }
        }
        public int GetPercentageOfCompaniesCompliantByRegion(int regionId, int companySiteCategoryId, string fieldName, string annualTargetFieldName)
        {
            int activeCompanies = GetNumberOfCompaniesByRegion(regionId, companySiteCategoryId);
            if (activeCompanies == 0)
                return 0;
            else
            {
                int compliantCount = GetNumberOfCompliantCompaniesByRegion(regionId, companySiteCategoryId, fieldName, annualTargetFieldName);
                int returnValue = 100 * compliantCount / activeCompanies;
                if (returnValue > 100)
                    return 100;
                else
                    return returnValue;
            }
        }
        public void UpdatePercentageOfCompliantCompaniesAllSites(string fieldName, string kpiName, string annualTargetFieldName)
        { 
            //TODO Finish this section
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == kpiName, null);
                    int percentageOfCompaniesCompliant = this.GetPercentageOfCompaniesCompliant(site.SiteId, csc.CompanySiteCategoryId, fieldName, annualTargetFieldName);
                    if (ahri != null)
                    {
                        this.UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, percentageOfCompaniesCompliant.ToString());
                        ahriService.Update(ahri, true);
                    }
                }
            }
            
        }

        public int GetPercentageOfCompaniesCompliant(int siteId, int companySiteCategoryId, string fieldName, string annualTargetFieldName)
        {
            int companies = GetNumberOfCompanies(siteId, companySiteCategoryId);
            if (companies == 0)
                return 0;
            else
            {
                int compliantCount = GetNumberOfCompliantCompanies(siteId, companySiteCategoryId, fieldName,  annualTargetFieldName);
                int returnValue = 100 * compliantCount / companies;
                if (returnValue > 100)
                    return 100;
                else
                    return returnValue;
            }
        }

        public int GetNumberOfCompliantCompanies(int siteId, int companySiteCategoryId, string fieldName, string annualTargetFieldName)
        {
            DateTime dt = new DateTime(YearMinusOneMonth, MonthMinusOneMonth, 1);
            model.AnnualTarget annualTarget = annualTargetService.Get(a => a.Year == YearMinusOneMonth && a.CompanySiteCategoryId == companySiteCategoryId, null);
            if (annualTarget == null)
                annualTarget = annualTargetService.Get(a => a.Year == YearMinusOneMonth - 1 && a.CompanySiteCategoryId == companySiteCategoryId, null);
            var annualTargetText = annualTarget.GetType().GetProperties().Single(at => at.Name.ToLower() == annualTargetFieldName.ToLower()).GetValue(annualTarget, null);
            double annualTargetValue;
            double.TryParse(annualTargetText.ToString(), out annualTargetValue);

            List<model.CompanySiteCategoryStandardHistory> cscshList = companySiteCategoryStandardHistoryService.GetMany(null, c => c.SiteId == siteId && c.CompanySiteCategoryId == companySiteCategoryId && c.RecordDate == dt, null, null);
            List<int> lstCompanies = cscshList.Select(cs => cs.CompanyId).ToList();
            List<model.Kpi> lstKpi = kpiService.GetMany(null, kpi => lstCompanies.Contains(kpi.CompanyId) && kpi.kpiDateTime == dt && kpi.SiteId == siteId, null, null);

            List<model.Company> companyList = companyService.GetMany(null, null, null, null);
            int compliantCount = 0;
            
            foreach (model.Kpi kcsc in lstKpi)
            {
                var score = kcsc.GetType().GetProperties().Single(pi => pi.Name.ToLower() == fieldName.ToLower()).GetValue(kcsc, null);
                if (score != null)
                {
                    double scoreDouble = 0;
                    double.TryParse(score.ToString(), out scoreDouble);
                   
                    if (scoreDouble >= annualTargetValue / 12)
                    {
                        compliantCount++;
                    }
                }
            }
            return compliantCount;
        }

        public int GetNumberOfCompliantCompaniesByRegion(int regionId, int companySiteCategoryId, string fieldName, string annualTargetFieldName)
        {
            List<model.RegionsSite> regionSites = regionSiteService.GetMany(null, s => s.RegionId == regionId, null, null);
            int countCompanies = 0;
            foreach (model.RegionsSite rs in regionSites)
            {
                countCompanies += GetNumberOfCompliantCompanies(rs.SiteId, companySiteCategoryId,fieldName,annualTargetFieldName);
            }
            return countCompanies;
        }


        public string GetNonCompliantCompanies(int siteId, int companySiteCategoryId, string fieldName,string annualTargetFieldName)
        {
            DateTime dt = new DateTime(YearMinusOneMonth, MonthMinusOneMonth, 1);
            model.AnnualTarget annualTarget = annualTargetService.Get(a => a.Year == YearMinusOneMonth && a.CompanySiteCategoryId == companySiteCategoryId, null);
            if (annualTarget == null)
                annualTarget = annualTargetService.Get(a => a.Year == YearMinusOneMonth - 1 && a.CompanySiteCategoryId == companySiteCategoryId, null);
            var annualTargetText = annualTarget.GetType().GetProperties().Single(at => at.Name.ToLower() == annualTargetFieldName.ToLower()).GetValue(annualTarget, null);
            double annualTargetValue;
            double.TryParse(annualTargetText.ToString(), out annualTargetValue);

            List<model.CompanySiteCategoryStandardHistory> cscshList = companySiteCategoryStandardHistoryService.GetMany(null, c => c.SiteId == siteId && c.CompanySiteCategoryId == companySiteCategoryId && c.RecordDate == dt, null, null);
            List<int> lstCompanies = cscshList.Select(cs => cs.CompanyId).ToList();
            List<model.Kpi> lstKpi = kpiService.GetMany(null, kpi => lstCompanies.Contains(kpi.CompanyId) && kpi.kpiDateTime == dt && kpi.SiteId == siteId, null, null);

            List<model.Company> companyList = companyService.GetMany(null, null, null, null);
            string nonCompliantList = "";
            List<string> companyNames = new List<string>();
            foreach (model.Kpi kcsc in lstKpi)
            {
                var score = kcsc.GetType().GetProperties().Single(pi => pi.Name.ToLower() == fieldName.ToLower()).GetValue(kcsc,null);
                if (score == null)
                {
                    companyNames.Add(companyList.FirstOrDefault(c => c.CompanyId == kcsc.CompanyId).CompanyName);
                }
                else
                {
                    double scoreDouble = 0;
                    double.TryParse(score.ToString(), out scoreDouble);
                    //if (value == null || value.ToString().ToLower()== "null" || value.ToString().ToLower() == "false")
                    if (scoreDouble < annualTargetValue / 12)
                    {
                        companyNames.Add(companyList.FirstOrDefault(c => c.CompanyId == kcsc.CompanyId).CompanyName);
                    }
                }
            }
            companyNames.Sort();
            //turn list to comma separated
            foreach (string companyName in companyNames)
            {
                nonCompliantList = nonCompliantList + ", " + companyName;
            }
            if (nonCompliantList.Length > 0)
                nonCompliantList = nonCompliantList.Substring(2, nonCompliantList.Length - 2);
            return nonCompliantList;
        }


        public void UpdateNonCompliantCompaniesAllSites(string fieldName,string kpiName,string annualTargetFieldName)
        {
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == kpiName, null);
                    string companiesNotCompliant = this.GetNonCompliantCompanies(site.SiteId, csc.CompanySiteCategoryId, fieldName, annualTargetFieldName);
                    if (ahri != null)
                    {
                        this.UpdateAhriObjectCompaniesNotCompliantForMonth(ahri, MonthMinusOneMonth, companiesNotCompliant);
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }

        public string GetNonCompliantCompaniesSafetyPlans(int siteId, int companySiteCategoryId)
        {
            DateTime dt = new DateTime(YearMinusOneMonth, MonthMinusOneMonth, 1);
            List<model.CompanySiteCategoryStandardHistory> cscshList = companySiteCategoryStandardHistoryService.GetMany(null, t => t.SiteId == siteId && t.CompanySiteCategoryId == companySiteCategoryId && t.RecordDate == dtStartOfMonth, null, null);
            List<model.SafetyPlansWithoutFileHash> safetyPlans = safetyPlansService.GetMany(null,fd=>fd.Description.Contains(YearMinusOneMonth.ToString()),null,null);
            string nonCompliantList = "";
            List<string> companyNames = new List<string>();
            List<model.Company> companyList = companyService.GetMany(null, null, null, null);
            foreach (model.CompanySiteCategoryStandardHistory cscs in cscshList)
            {
                model.SafetyPlansWithoutFileHash safetyPlan = safetyPlans.FirstOrDefault(f => f.CompanyId == cscs.CompanyId);
                if (safetyPlan == null)
                {
                    companyNames.Add(companyList.FirstOrDefault(c => c.CompanyId == cscs.CompanyId).CompanyName);
                }
            }
            nonCompliantList = CommaStringFromList(companyNames);
            return nonCompliantList;
        }


        public void UpdateNonCompliantCompaniesSafetyPlansAllSites()
        {
            string kpiName = "Safety Plan(s) Submitted";
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == kpiName, null);
                    string companiesNotCompliant = this.GetNonCompliantCompaniesSafetyPlans(site.SiteId, csc.CompanySiteCategoryId);
                    if (ahri != null)
                    {
                        this.UpdateAhriObjectCompaniesNotCompliantForMonth(ahri, MonthMinusOneMonth, companiesNotCompliant);
                        ahriService.Update(ahri, true);
                        //ALCOA.CSMS.Batch.Common.Log.Log4NetService.LogInfo(site.SiteName + " " + csc.CompanySiteCategoryId);
                    }
                }
            }
        }
        public int GetNumberOfCompaniesSafetyPlansCompliant(int siteId, int companySiteCategoryId)
        {
            DateTime dt = new DateTime(YearMinusOneMonth, MonthMinusOneMonth, 1);
            List<model.CompanySiteCategoryStandardHistory> cscshList = companySiteCategoryStandardHistoryService.GetMany(null, t => t.SiteId == siteId && t.CompanySiteCategoryId == companySiteCategoryId && t.RecordDate == dtStartOfMonth, null, null);
            List<model.SafetyPlansWithoutFileHash> safetyPlans = safetyPlansService.GetMany(null,fd=>fd.Description.Contains(YearMinusOneMonth.ToString()),null,null);
            
            List<string> companyNames = new List<string>();
            List<model.Company> companyList = companyService.GetMany(null, null, null, null);

            double compliantCount = 0;
            foreach (model.CompanySiteCategoryStandardHistory cscs in cscshList)
            {
                model.SafetyPlansWithoutFileHash safetyPlan = safetyPlans.FirstOrDefault(f => f.CompanyId == cscs.CompanyId);
                if (safetyPlan != null)
                {
                    compliantCount++;
                }
                
            }
            return Convert.ToInt32(compliantCount);
        }
       
        public int GetNumberOfCompaniesSafetyPlansCompliantByRegion(int regionId, int companySiteCategoryId)
        {
            List<model.RegionsSite> regionSites = regionSiteService.GetMany(null, s => s.RegionId == regionId, null, null);
            int countCompanies = 0;
            foreach (model.RegionsSite rs in regionSites)
            {
                countCompanies += GetNumberOfCompaniesSafetyPlansCompliant(rs.SiteId, companySiteCategoryId);
            }
            return countCompanies;
        }
        

        public int GetPercentageOfCompaniesSafetyPlansCompliant(int siteId, int companySiteCategoryId)
        {
            int activeCompanies = GetNumberOfCompanies(siteId, companySiteCategoryId);
            if (activeCompanies == 0)
                return 0;
            else
            {
                int compliantCount = this.GetNumberOfCompaniesSafetyPlansCompliant(siteId, companySiteCategoryId);
                int returnValue = 100 * compliantCount / activeCompanies;
                if (returnValue > 100)
                    return 100;
                else
                    return returnValue;
            }
        }
        public int GetPercentageOfCompaniesSafetyPlansCompliantByRegion(int regionId, int companySiteCategoryId)
        {
            int activeCompanies = GetNumberOfCompaniesByRegion(regionId, companySiteCategoryId);
            if (activeCompanies == 0)
                return 0;
            else
            {
                int compliantCount = GetNumberOfCompaniesSafetyPlansCompliantByRegion(regionId, companySiteCategoryId);
                int returnValue = 100 * compliantCount / activeCompanies;
                if (returnValue > 100)
                    return 100;
                else
                    return returnValue;
            }
        }
        public void UpdatePercentageSafetyPlansCompliantAllSites()
        {
            List<model.Site> sites = siteService.GetMany(null, i => i.SiteAbbrev.Substring(0, 1) != "-", null, null);
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            foreach (model.Site site in sites)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == site.SiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "Safety Plan(s) Submitted", null);
                    int numberOfCompanies = this.GetPercentageOfCompaniesSafetyPlansCompliant(site.SiteId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        ahriService.Update(ahri, true);
                    }
                }
            }
        }
        public void UpdatePercentageSafetyPlansCompliantAllRegions()
        {
            List<model.CompanySiteCategory> cscs = cscService.GetMany(null, null, null, null);
            List<model.Region> regions = regionService.GetMany(null, r => r.RegionSiteId != null, null, null);
            foreach (model.Region region in regions)
            {
                foreach (model.CompanySiteCategory csc in cscs)
                {
                    model.AdHoc_Radar_Items ahri = ahriService.Get(ra => ra.AdHoc_Radar.CompanySiteCategoryId == csc.CompanySiteCategoryId && ra.SiteId == region.RegionSiteId && ra.Year == YearMinusOneMonth && ra.AdHoc_Radar.KpiName == "Safety Plan(s) Submitted", null);
                    int numberOfCompanies = this.GetPercentageOfCompaniesSafetyPlansCompliantByRegion(region.RegionId, csc.CompanySiteCategoryId);
                    string numberOfCompaniesText = Convert.ToString(numberOfCompanies);
                    if (ahri != null)
                    {
                        UpdateAhriObjectMonthProperty(ahri, MonthMinusOneMonth, numberOfCompaniesText);
                        ahriService.Update(ahri, true);
                    }

                }
            }
        }
        public string CommaStringFromList(List<string> companyNames)
        {
            companyNames.Sort();
            string nonCompliantList = "";
            //turn list to comma separated
            foreach (string companyName in companyNames)
            {
                nonCompliantList = nonCompliantList + ", " + companyName;
            }
            if (nonCompliantList.Length > 0)
                nonCompliantList = nonCompliantList.Substring(2, nonCompliantList.Length - 2);
            return nonCompliantList;
        }

        public string GetMonthNameFromMonthNumber(int monthNumber)
        {
            DateTime d = new DateTime(2000, monthNumber, 1);
            return d.ToString("MMM");
        }
        public void UpdateAhriObjectMonthProperty(model.AdHoc_Radar_Items ahri, int monthNumber, string value)
        {
            switch (monthNumber)
            {
                case 1:
                    ahri.KpiScoreJan = value;
                    break;
                case 2:
                    ahri.KpiScoreFeb = value;
                    break;
                case 3:
                    ahri.KpiScoreMar = value;
                    break;
                case 4:
                    ahri.KpiScoreApr = value;
                    break;
                case 5:
                    ahri.KpiScoreMay = value;
                    break;
                case 6:
                    ahri.KpiScoreJun = value;
                    break;
                case 7:
                    ahri.KpiScoreJul = value;
                    break;
                case 8:
                    ahri.KpiScoreAug = value;
                    break;
                case 9:
                    ahri.KpiScoreSep = value;
                    break;
                case 10:
                    ahri.KpiScoreOct = value;
                    break;
                case 11:
                    ahri.KpiScoreNov = value;
                    break;
                case 12:
                    ahri.KpiScoreDec = value;
                    break;
            }
        }
        public void UpdateAhriObjectCompaniesNotCompliantForMonth(model.AdHoc_Radar_Items ahri, int monthNumber, string companyNames)
        {
            switch (monthNumber)
            {
                case 1:
                    ahri.CompaniesNotCompliantJan = companyNames;
                    break;
                case 2:
                    ahri.CompaniesNotCompliantFeb = companyNames;
                    break;
                case 3:
                    ahri.CompaniesNotCompliantMar = companyNames;
                    break;
                case 4:
                    ahri.CompaniesNotCompliantApr = companyNames;
                    break;
                case 5:
                    ahri.CompaniesNotCompliantMay = companyNames;
                    break;
                case 6:
                    ahri.CompaniesNotCompliantJun = companyNames;
                    break;
                case 7:
                    ahri.CompaniesNotCompliantJul = companyNames;
                    break;
                case 8:
                    ahri.CompaniesNotCompliantAug = companyNames;
                    break;
                case 9:
                    ahri.CompaniesNotCompliantSep = companyNames;
                    break;
                case 10:
                    ahri.CompaniesNotCompliantOct = companyNames;
                    break;
                case 11:
                    ahri.CompaniesNotCompliantNov = companyNames;
                    break;
                case 12:
                    ahri.CompaniesNotCompliantDec = companyNames;
                    break;
            }
        }
    }
    public class CSAQuarter
    {
        public int CurrentQuarterYear { get; set; }
        public int CurrentQuarter { get; set; }
        public int PreviousQuarterYear { get; set; }
        public int PreviousQuarter { get; set; }
        public CSAQuarter(DateTime aDate, int companySiteCategoryId)
        {
            int currentQuarter = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(aDate.Month) / 3));
            if (companySiteCategoryId == 2)
            {
                if (currentQuarter == 4)
                    currentQuarter = 3;
                if (currentQuarter == 2)
                    currentQuarter = 1;
            }
            CurrentQuarter = currentQuarter;
            CurrentQuarterYear = aDate.Year;
            int previousQuarter = currentQuarter - 1;
            int previousQuarterYear = aDate.Year;
            if (previousQuarter == 0)
            {
                previousQuarter = 4;
                previousQuarterYear = previousQuarterYear - 1;
            }
            if (companySiteCategoryId == 2)
            {
                if (previousQuarter == 4)
                    previousQuarter = 3;
                if (previousQuarter == 2)
                    previousQuarter = 1;
            }
            PreviousQuarterYear = previousQuarterYear;
            PreviousQuarter = previousQuarter;
        }

    }
}
