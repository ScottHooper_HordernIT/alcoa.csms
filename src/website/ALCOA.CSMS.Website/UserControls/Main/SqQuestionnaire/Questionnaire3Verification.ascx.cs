using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Drawing;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;

using System.Text;

public partial class UserControls_SqQuestionnaire_Questionnaire3Verification : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        switch (SessionHandler.spVar_Questionnaire_FrontPageLink)
        {
            case "QuestionnaireReport_Overview":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Qualified Contractors";
                hlQuestionnaire.ToolTip = "Go back to Qualified Contractors Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQOverview.aspx";
                break;
            case "QuestionnaireReport_Expire":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Expiry Date";
                hlQuestionnaire.ToolTip = "Go back to SQ Expiry Date Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQ.aspx?q=expire";
                break;
            case "QuestionnaireReport_Progress":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Status";
                hlQuestionnaire.ToolTip = "Go back to SQ Status Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQStatus.aspx";
                break;
            case "EbiContractorsOnSite":
                hlQuestionnaire.Text = "Reports / Charts > Compliance Reports > Contractors On-Site (EBI)";
                hlQuestionnaire.ToolTip = "Go back to Contractors On-Site (EBI) Page";
                hlQuestionnaire.NavigateUrl = "~/NonComplianceEbiOnSite.aspx";
                break;
            default: //Questionnaire
                hlQuestionnaire.Text = "Australian Safety Qualification Process";
                hlQuestionnaire.ToolTip = "Go back to Safety Qualification Page";
                hlQuestionnaire.NavigateUrl = "~/SafetyPQ_Questionnaire.aspx";
                break;
        }

        if (!postBack)
        {
            LoadToolTips();
            bool print = false;
            string q = "";
            try
            {
                q = Request.QueryString["q"];
                if (Request.QueryString["printlayout"] != null)
                {
                    if (Request.QueryString["printlayout"] == "yes")
                    {
                        print = true;
                    }
                }

                LinkButton1.PostBackUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q));

                if (q != "New")
                {
                    QuestionnaireService _qs = new QuestionnaireService();
                    Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

                    if (_q == null)
                    {
                        throw new Exception("Invalid Questionnaire Id");
                    }
                    else
                    {
                        if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                        {
                            if (_q.CompanyId != auth.CompanyId)
                            {
                                throw new Exception("You are not authorised to view this page.");
                            }
                        }

                        CompaniesService companiesService = new CompaniesService();
                        Companies companiesEntity = companiesService.GetByCompanyId(_q.CompanyId);
                        lblCompanyName.Text = companiesEntity.CompanyName;

                        QuestionnaireStatus qs = new QuestionnaireStatus();
                        QuestionnaireStatusService qss = new QuestionnaireStatusService();
                        qs = qss.GetByQuestionnaireStatusId(_q.VerificationStatus);
                        lblStatus.Text = qs.QuestionnaireStatusDesc;
                        if (qs.QuestionnaireStatusDesc == "Being Assessed") lblStatus.Text = "Complete";

                        //if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed || _q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        //if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed || _q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        //{
                        //    btnSubmit.Enabled = false;
                        //}

                        Load2(q);
                        SubmitButton(q);

                        if (_q.Status == (int)QuestionnaireStatusList.Incomplete && _q.VerificationStatus == (int)QuestionnaireStatusList.Incomplete || _q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete)
                        {
                            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                            TList<QuestionnaireVerificationAssessment> qvaTlist = qvaService.GetByQuestionnaireIdAssessorApproval(_q.QuestionnaireId, false);
                            if (qvaTlist.Count > 0)
                            {
                                ShowApprovedColumn(_q.QuestionnaireId);
                            }
                        }


                        if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed && _q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        {
                            ASPxCheckBox1.Enabled = false;
                            ASPxCheckBox1.Checked = true;
                            btnSubmit.Enabled = false;

                            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                            TList<QuestionnaireVerificationAssessment> qvaTlist_all;
                            qvaTlist_all = qvaService.GetByQuestionnaireId(_q.QuestionnaireId);
                            bool approvednull = false;
                            bool notapproved = false;
                            if (qvaTlist_all.Count == 0)
                            {
                                approvednull = true;
                            }
                            else
                            {
                                foreach (QuestionnaireVerificationAssessment qva in qvaTlist_all)
                                {
                                    if (qva.AssessorApproval == null) approvednull = true;
                                    if (qva.AssessorApproval == false) notapproved = true;
                                }
                            }
                            if (!notapproved && !approvednull && qvaTlist_all.Count == 30) lblSave.Text = "Assessed: Approved";
                            if (notapproved && !approvednull) lblSave.Text = "Assessed: Not Approved";
                            if (approvednull)
                            {
                                lblSave.Text = "Being Assessed";
                            }

                            ShowApprovedColumn(_q.QuestionnaireId);
                        }

                        if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                            TList<QuestionnaireVerificationAssessment> qvaTlist_all;
                            qvaTlist_all = qvaService.GetByQuestionnaireId(_q.QuestionnaireId);
                            if (qvaTlist_all.Count == 34)
                            {
                                ShowApprovedColumn(_q.QuestionnaireId);
                            }
                            else
                            {
                                lblApproval1.Text = "-";
                                lblApproval2.Text = "-";
                                //lblApproval3.Text = "-";
                                lblApproval4.Text = "-";
                                lblApproval5.Text = "-";
                                lblApproval6.Text = "-";
                                lblApproval7.Text = "-";
                                lblApproval8.Text = "-";
                            }
                        }
                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(_q.CompanyId);
                        if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
                        {
                            //Read Only
                            ASPxCheckBox1.Enabled = false;
                            ASPxCheckBox1.Checked = true;
                            btnSubmit.Enabled = false;
                            lblSave.Text = "Read Only";
                        }

                        if (c.Deactivated == true
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                        {
                            //Read Only
                            ASPxCheckBox1.Enabled = false;
                            btnSubmit.Enabled = false;
                            lblSave.Text = "Read Only";
                            if (c.Deactivated == true) lblSave.Text += " - Company De-Activated";
                        }

                        try
                        {
                            if (Request.QueryString["s"].ToString() == "1") lblSave.Text = "Saved Successfully.";
                        }
                        catch { }

                        if (_q.MainStatus == (int)QuestionnaireStatusList.Incomplete)
                        {
                            bool authorised = true; //allow contractors to view
                            //if (auth.RoleId == (int)RoleList.Administrator) authorised = true;
                            //EhsConsultantService _eService = new EhsConsultantService();
                            //EhsConsultant _ehsc = _eService.GetByUserId(auth.UserId);
                            //if (_ehsc != null)
                            //{
                            //    if (_ehsc.Enabled) authorised = true;
                            //}
                            CompaniesService _cService = new CompaniesService();
                            Companies _c = _cService.GetByCompanyId(_q.CompanyId);
                            if (_c != null)
                            {
                                if (authorised)
                                {
                                    ASPxCheckBox1.Enabled = false;
                                    btnSubmit.Enabled = false;
                                }
                            }

                            if (!authorised && !print) throw new Exception("Not allowed here, until Supplier Questionnaire Complete");
                        }
                    }

                }

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        //Readers can do as they wish..
                        //btnSave.Enabled = false;
                        //btnSubmit.Enabled = false;
                        //Load(null);
                        break;
                    case ((int)RoleList.Contractor):
                        //qAnswer3.Value = auth.CompanyId;
                        //qAnswer3.Enabled = false;
                        //AutoFill();
                        break;
                    case ((int)RoleList.Administrator):

                        break;
                    case ((int)RoleList.PreQual):
                        break;
                    default:
                        // RoleList.Disabled or No Role
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                Response.Redirect("AccessDenied.aspx?error=" + ex.Message.ToString());
            }



            if (print)
            {
                //disable save options etc.
                btnHome.Enabled = false;
                ASPxCheckBox1.Enabled = false;
                btnSubmit.Enabled = false;
                lb1.Enabled = false;
                lb1_.Enabled = false;
                lb2.Enabled = false;
                lb2_.Enabled = false;
                lb4.Enabled = false;
                lb4_.Enabled = false;
                lb5.Enabled = false;
                lb5_.Enabled = false;
                lb6.Enabled = false;
                lb6_.Enabled = false;
                lb7.Enabled = false;
                lb7_.Enabled = false;
                lb8.Enabled = false;
                lb8_.Enabled = false;
                LinkButton1.Enabled = false;
                hlQuestionnaire.Enabled = false;
            }
        }
    }
    protected void btnHome_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            string q = Request.QueryString["q"].ToString();
            Response.Redirect("~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + q), true);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            Submit();
        }
    }

    protected void Load2(string q)
    {
        int s = 1;
        while (s <= 8)
        {
            if (s != 3)
            {
                LinkButton lbutton = (LinkButton)this.FindControl("lb" + s);
                LinkButton lbutton2 = (LinkButton)this.FindControl(String.Format("lb{0}_", s));
                if (String.IsNullOrEmpty(q))
                {
                    lbutton.PostBackUrl = "javascript:void(0);";
                    lbutton2.PostBackUrl = "javascript:void(0);";
                }
                else
                {
                    lbutton.PostBackUrl = String.Format("~/SafetyPQ_QuestionnaireVerificationSection.aspx{0}",
                                                        QueryStringModule.Encrypt(String.Format("q={0}&s={1}", q, s)));
                    lbutton2.PostBackUrl = lbutton.PostBackUrl;
                }
            }
            s++;
        }

        QuestionnaireVerificationSectionService qvsService = new QuestionnaireVerificationSectionService();
        QuestionnaireVerificationSection qvs = qvsService.GetByQuestionnaireId(Convert.ToInt32(q));
        if (qvs == null)
        {
            lbl1a.Visible = false;
            lbl2a.Visible = false;
            //lbl3a.Visible = false;
            lbl4a.Visible = false;
            lbl5a.Visible = false;
            lbl6a.Visible = false;
            lbl7a.Visible = false;
            lbl8a.Visible = false;
            lbl1b.Visible = false;
            lbl2b.Visible = false;
            //lbl3b.Visible = false;
            lbl4b.Visible = false;
            lbl5b.Visible = false;
            lbl6b.Visible = false;
            lbl7b.Visible = false;
            lbl8b.Visible = false;
        }
        else
        {
            if (qvs.Section1Complete.HasValue)
            {
                if (qvs.Section1Complete == true) { lbl1a.ForeColor = Color.Green; lbl1a.Text = "Yes"; } else { lbl1a.ForeColor = Color.Red; lbl1a.Text = "No"; lbl1b.Visible = true; }
            }
            else
            {
                lbl1a.Visible = false;
            }

            if (qvs.Section2Complete.HasValue)
            {
                if (qvs.Section2Complete == true) { lbl2a.ForeColor = Color.Green; lbl2a.Text = "Yes"; } else { lbl2a.ForeColor = Color.Red; lbl2a.Text = "No"; lbl2b.Visible = true; }
            }
            else
            {
                lbl2a.Visible = false;
            }

            //20-JUL-2010 Removed and not checked anymore.

            //if (qvs.Section3Complete.HasValue)
            //{
            //    if (qvs.Section3Complete == true) { lbl3a.ForeColor = Color.Green; lbl3a.Text = "Yes"; } else { lbl3a.ForeColor = Color.Red; lbl3a.Text = "No"; lbl3b.Visible = true; }
            //}
            //else
            //{
            //    lbl3a.Visible = false;
            //}

            if (qvs.Section4Complete.HasValue)
            {
                if (qvs.Section4Complete == true) { lbl4a.ForeColor = Color.Green; lbl4a.Text = "Yes"; } else { lbl4a.ForeColor = Color.Red; lbl4a.Text = "No"; lbl4b.Visible = true; }
            }
            else
            {
                lbl4a.Visible = false;
            }

            if (qvs.Section5Complete.HasValue)
            {
                if (qvs.Section5Complete == true) { lbl5a.ForeColor = Color.Green; lbl5a.Text = "Yes"; } else { lbl5a.ForeColor = Color.Red; lbl5a.Text = "No"; lbl5b.Visible = true; }
            }
            else
            {
                lbl5a.Visible = false;
            }

            if (qvs.Section6Complete.HasValue)
            {
                if (qvs.Section6Complete == true) { lbl6a.ForeColor = Color.Green; lbl6a.Text = "Yes"; } else { lbl6a.ForeColor = Color.Red; lbl6a.Text = "No"; lbl6b.Visible = true; }
            }
            else
            {
                lbl6a.Visible = false;
            }

            if (qvs.Section7Complete.HasValue)
            {
                if (qvs.Section7Complete == true) { lbl7a.ForeColor = Color.Green; lbl7a.Text = "Yes"; } else { lbl7a.ForeColor = Color.Red; lbl7a.Text = "No"; lbl7b.Visible = true; }
            }
            else
            {
                lbl7a.Visible = false;
            }

            if (qvs.Section8Complete.HasValue)
            {
                if (qvs.Section8Complete == true) { lbl8a.ForeColor = Color.Green; lbl8a.Text = "Yes"; } else { lbl8a.ForeColor = Color.Red; lbl8a.Text = "No"; lbl8b.Visible = true; }
            }
            else
            {
                lbl8a.Visible = false;
            }
        }
    }

    protected void Submit()
    {
        try
        {
            int q = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire qu = qs.GetByQuestionnaireId(Convert.ToInt32(q));

            qu.VerificationModifiedByUserId = auth.UserId;
            qu.VerificationModifiedDate = DateTime.Now;
            //qu.ModifiedByUserId = auth.UserId;
            //qu.ModifiedDate = DateTime.Now;
            qu.VerificationStatus = (int)QuestionnaireStatusList.BeingAssessed;

            qs.Save(qu);
            SessionHandler.spVar_SafetyPlans_QuestionnaireId = "";
            lblSave.Text = "Submitted Successfully. Click Home to return to the list of Questionnaires.";

            int intSubmitResubmit = (int)QuestionnaireActionList.SafetyQuestionnaireSubmitted;
            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
            TList<QuestionnaireVerificationAssessment> qvaTlist_all;
            qvaTlist_all = qvaService.GetByQuestionnaireId(qu.QuestionnaireId);
            if (qvaTlist_all.Count > 0) intSubmitResubmit = (int)QuestionnaireActionList.SafetyQuestionnaireReSubmitted;

            if (Helper.Questionnaire.ActionLog.Add(intSubmitResubmit,
                                                    qu.QuestionnaireId,                         //QuestionnaireId
                                                    auth.UserId,                                //UserId
                                                    auth.FirstName,                             //FirstName
                                                    auth.LastName,                              //LastName
                                                    auth.RoleId,                                //RoleId
                                                    auth.CompanyId,                             //CompanyId
                                                    auth.CompanyName,                           //CompanyName
                                                    null,                                       //Comments
                                                    null,                                       //OldAssigned
                                                    auth.FirstName + " " + auth.LastName,       //NewAssigned
                                                    null,                                       //NewAssigned2
                                                    null,                                       //NewAssigned3
                                                    null)                                       //NewAssigned4
                                                    == false)
            {
                //for now we do nothing if action log fails.
            }

            Response.Redirect("~/SafetyPQ_QuestionnaireModify.aspx" + QueryStringModule.Encrypt("q=" + qu.QuestionnaireId.ToString()), true);
        }
        catch(Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblSave.Text = "Submit failed. Error: " + ex.Message.ToString();
        }
    }

    protected void SubmitButton(string q)
    {
        int qNo = Convert.ToInt32(q);
        QuestionnaireVerificationSectionService qvsService = new QuestionnaireVerificationSectionService();
        QuestionnaireVerificationSection qvs = qvsService.GetByQuestionnaireId(qNo);
        if (qvs != null)
        {
            //Removed Section 3 Check. 20-JUL-2010
            if (qvs.Section1Complete == true && qvs.Section2Complete == true && qvs.Section4Complete == true &&
               qvs.Section5Complete == true && qvs.Section6Complete == true && qvs.Section7Complete == true && qvs.Section8Complete == true)
            {
                //only allow submit if main submitted.
                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire questionnaire = qService.GetByQuestionnaireId(qNo);
                if (questionnaire.MainStatus > (int)QuestionnaireStatusList.Incomplete)
                {
                    ASPxCheckBox1.Enabled = true;
                    btnSubmit.Enabled = true;
                }
            }
            else
            {
                ASPxCheckBox1.Enabled = false;
                btnSubmit.Enabled = false;
            }
        }
        else
        {
            ASPxCheckBox1.Enabled = false;
            btnSubmit.Enabled = false;
        }
    }

    protected void ShowApprovedColumn(int QuestionnaireId)
    {
        lbl1b.ForeColor = System.Drawing.Color.Black;
        lbl2b.ForeColor = System.Drawing.Color.Black;
        //lbl3b.ForeColor = System.Drawing.Color.Black;
        lbl4b.ForeColor = System.Drawing.Color.Black;
        lbl5b.ForeColor = System.Drawing.Color.Black;
        lbl6b.ForeColor = System.Drawing.Color.Black;
        lbl7b.ForeColor = System.Drawing.Color.Black;
        lbl8b.ForeColor = System.Drawing.Color.Black;

        int approved_count = 0;
        int notapproved_count = 0;
        bool nullapproval = false;

        QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
        TList<QuestionnaireVerificationAssessment> qvaTlist_all;

        //1
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 1);
        if(qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 4)
            {
                lblApproval1.Text = "Yes";
                lblApproval1.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval1.Text = "No";
                    lblApproval1.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;
        //2
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 2);
        if (qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 5)
            {
                lblApproval2.Text = "Yes";
                lblApproval2.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval2.Text = "No";
                    lblApproval2.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;
        //3
        //qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 3);
        //if (qvaTlist_all != null)
        //{
        //    foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
        //    {
        //        if (qa.AssessorApproval == true) approved_count++;
        //        if (qa.AssessorApproval == false) notapproved_count++;
        //        if (qa.AssessorApproval == null) nullapproval = true;
        //    }
        //}
        //if (!nullapproval)
        //{
        //    if (approved_count == 4)
        //    {
        //        lblApproval3.Text = "Yes";
        //        lblApproval3.ForeColor = System.Drawing.Color.Green;
        //    }
        //    else
        //    {
        //        if (notapproved_count > 0)
        //        {
        //            lblApproval3.Text = "No";
        //            lblApproval3.ForeColor = System.Drawing.Color.Red;
        //        }
        //    }
        //}
        //qvaTlist_all = null;
        //notapproved_count = 0;
        //approved_count = 0;
        //nullapproval = false;
        //4
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 4);
        if (qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 5)
            {
                lblApproval4.Text = "Yes";
                lblApproval4.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval4.Text = "No";
                    lblApproval4.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;
        //5
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 5);
        if (qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 6)
            {
                lblApproval5.Text = "Yes";
                lblApproval5.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval5.Text = "No";
                    lblApproval5.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;
        //6
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 6);
        if (qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 4)
            {
                lblApproval6.Text = "Yes";
                lblApproval6.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval6.Text = "No";
                    lblApproval6.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;
        //7
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 7);
        if (qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 3)
            {
                lblApproval7.Text = "Yes";
                lblApproval7.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval7.Text = "No";
                    lblApproval7.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;
        //8
        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(QuestionnaireId, 8);
        if (qvaTlist_all != null)
        {
            foreach (QuestionnaireVerificationAssessment qa in qvaTlist_all)
            {
                if (qa.AssessorApproval == true) approved_count++;
                if (qa.AssessorApproval == false) notapproved_count++;
                if (qa.AssessorApproval == null) nullapproval = true;
            }
        }
        if (!nullapproval)
        {
            if (approved_count == 3)
            {
                lblApproval8.Text = "Yes";
                lblApproval8.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                if (notapproved_count > 0)
                {
                    lblApproval8.Text = "No";
                    lblApproval8.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        qvaTlist_all = null;
        notapproved_count = 0;
        approved_count = 0;
        nullapproval = false;

    }

    protected void LoadToolTips()
    {
        try
        {
            QuestionnaireVerificationRationaleService qvrService = new QuestionnaireVerificationRationaleService();
            QuestionnaireVerificationRationale q = qvrService.GetBySectionIdQuestionId(1, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb1.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;
            q = qvrService.GetBySectionIdQuestionId(2, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb2.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;
            q = qvrService.GetBySectionIdQuestionId(4, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb4.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;
            q = qvrService.GetBySectionIdQuestionId(5, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb5.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;
            q = qvrService.GetBySectionIdQuestionId(6, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb6.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;
            q = qvrService.GetBySectionIdQuestionId(7, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb7.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;
            q = qvrService.GetBySectionIdQuestionId(8, "Summary");
            if (q != null)
                if (q.Rationale != null)
                    lb8.ToolTip = "Why: " + Encoding.ASCII.GetString(q.Rationale);
            q = null;

            lb1_.ToolTip = lb1.ToolTip;
            lb2_.ToolTip = lb2.ToolTip;
            lb4_.ToolTip = lb4.ToolTip;
            lb5_.ToolTip = lb5.ToolTip;
            lb6_.ToolTip = lb6.ToolTip;
            lb7_.ToolTip = lb7.ToolTip;
            lb8_.ToolTip = lb8.ToolTip;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }

    }
}

