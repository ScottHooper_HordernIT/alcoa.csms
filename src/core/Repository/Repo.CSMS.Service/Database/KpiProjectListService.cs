﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiProjectListService
    {
        KpiProjectList Get(Expression<Func<KpiProjectList, bool>> where, List<Expression<Func<KpiProjectList, object>>> includes);
        List<KpiProjectList> GetMany(int? top, Expression<Func<KpiProjectList, bool>> where, Func<IQueryable<KpiProjectList>, IOrderedQueryable<KpiProjectList>> orderBy, List<Expression<Func<KpiProjectList, object>>> includes);
        List<KpiProjectList> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiProjectList, bool>> where, Func<IQueryable<KpiProjectList>, IOrderedQueryable<KpiProjectList>> orderBy, List<Expression<Func<KpiProjectList, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiProjectList, bool>> where, List<Expression<Func<KpiProjectList, object>>> includes);
        void Insert(KpiProjectList item, bool saveChanges = true);
        void Update(KpiProjectList item, bool saveChanges = true);
        void Delete(KpiProjectList item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiProjectListService : IKpiProjectListService
    {
        private readonly IRepository<KpiProjectList> _repository;

        public KpiProjectListService(IRepository<KpiProjectList> repository)
        {
            this._repository = repository;
        }

        public KpiProjectList Get(Expression<Func<KpiProjectList, bool>> where, List<Expression<Func<KpiProjectList, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiProjectList> GetMany(int? top, Expression<Func<KpiProjectList, bool>> where, Func<IQueryable<KpiProjectList>, IOrderedQueryable<KpiProjectList>> orderBy, List<Expression<Func<KpiProjectList, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiProjectList> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiProjectList, bool>> where, Func<IQueryable<KpiProjectList>, IOrderedQueryable<KpiProjectList>> orderBy, List<Expression<Func<KpiProjectList, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiProjectList, bool>> where, List<Expression<Func<KpiProjectList, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(KpiProjectList item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiProjectList");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(KpiProjectList item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiProjectList");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(KpiProjectList item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiProjectList");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
