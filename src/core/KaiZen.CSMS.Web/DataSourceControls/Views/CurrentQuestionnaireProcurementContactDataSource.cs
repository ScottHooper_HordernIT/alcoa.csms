﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CurrentQuestionnaireProcurementContactProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CurrentQuestionnaireProcurementContactDataSourceDesigner))]
	public class CurrentQuestionnaireProcurementContactDataSource : ReadOnlyDataSource<CurrentQuestionnaireProcurementContact>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactDataSource class.
		/// </summary>
		public CurrentQuestionnaireProcurementContactDataSource() : base(new CurrentQuestionnaireProcurementContactService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CurrentQuestionnaireProcurementContactDataSourceView used by the CurrentQuestionnaireProcurementContactDataSource.
		/// </summary>
		protected CurrentQuestionnaireProcurementContactDataSourceView CurrentQuestionnaireProcurementContactView
		{
			get { return ( View as CurrentQuestionnaireProcurementContactDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CurrentQuestionnaireProcurementContactDataSourceView class that is to be
		/// used by the CurrentQuestionnaireProcurementContactDataSource.
		/// </summary>
		/// <returns>An instance of the CurrentQuestionnaireProcurementContactDataSourceView class.</returns>
		protected override BaseDataSourceView<CurrentQuestionnaireProcurementContact, Object> GetNewDataSourceView()
		{
			return new CurrentQuestionnaireProcurementContactDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CurrentQuestionnaireProcurementContactDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CurrentQuestionnaireProcurementContactDataSourceView : ReadOnlyDataSourceView<CurrentQuestionnaireProcurementContact>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementContactDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CurrentQuestionnaireProcurementContactDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CurrentQuestionnaireProcurementContactDataSourceView(CurrentQuestionnaireProcurementContactDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CurrentQuestionnaireProcurementContactDataSource CurrentQuestionnaireProcurementContactOwner
		{
			get { return Owner as CurrentQuestionnaireProcurementContactDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CurrentQuestionnaireProcurementContactService CurrentQuestionnaireProcurementContactProvider
		{
			get { return Provider as CurrentQuestionnaireProcurementContactService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CurrentQuestionnaireProcurementContactDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CurrentQuestionnaireProcurementContactDataSource class.
	/// </summary>
	public class CurrentQuestionnaireProcurementContactDataSourceDesigner : ReadOnlyDataSourceDesigner<CurrentQuestionnaireProcurementContact>
	{
	}

	#endregion CurrentQuestionnaireProcurementContactDataSourceDesigner

	#region CurrentQuestionnaireProcurementContactFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementContactFilter : SqlFilter<CurrentQuestionnaireProcurementContactColumn>
	{
	}

	#endregion CurrentQuestionnaireProcurementContactFilter

	#region CurrentQuestionnaireProcurementContactExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementContactExpressionBuilder : SqlExpressionBuilder<CurrentQuestionnaireProcurementContactColumn>
	{
	}
	
	#endregion CurrentQuestionnaireProcurementContactExpressionBuilder		
}

