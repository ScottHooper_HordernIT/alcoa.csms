﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ISqExemptionAuditService
    {
        SqExemptionAudit Get(Expression<Func<SqExemptionAudit, bool>> where, List<Expression<Func<SqExemptionAudit, object>>> includes);
        List<SqExemptionAudit> GetMany(int? top, Expression<Func<SqExemptionAudit, bool>> where, Func<IQueryable<SqExemptionAudit>, IOrderedQueryable<SqExemptionAudit>> orderBy, List<Expression<Func<SqExemptionAudit, object>>> includes);
        List<SqExemptionAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SqExemptionAudit, bool>> where, Func<IQueryable<SqExemptionAudit>, IOrderedQueryable<SqExemptionAudit>> orderBy, List<Expression<Func<SqExemptionAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<SqExemptionAudit, bool>> where, List<Expression<Func<SqExemptionAudit, object>>> includes);
        void Insert(SqExemptionAudit item, bool saveChanges = true);
        void Update(SqExemptionAudit item, bool saveChanges = true);
        void Delete(SqExemptionAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class SqExemptionAuditService : ISqExemptionAuditService
    {
        private readonly IRepository<SqExemptionAudit> _repository;

        public SqExemptionAuditService(IRepository<SqExemptionAudit> repository)
        {
            this._repository = repository;
        }

        public SqExemptionAudit Get(Expression<Func<SqExemptionAudit, bool>> where, List<Expression<Func<SqExemptionAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<SqExemptionAudit> GetMany(int? top, Expression<Func<SqExemptionAudit, bool>> where, Func<IQueryable<SqExemptionAudit>, IOrderedQueryable<SqExemptionAudit>> orderBy, List<Expression<Func<SqExemptionAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<SqExemptionAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SqExemptionAudit, bool>> where, Func<IQueryable<SqExemptionAudit>, IOrderedQueryable<SqExemptionAudit>> orderBy, List<Expression<Func<SqExemptionAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<SqExemptionAudit, bool>> where, List<Expression<Func<SqExemptionAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(SqExemptionAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SqExemptionAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(SqExemptionAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SqExemptionAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(SqExemptionAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SqExemptionAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
