USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_EBI_No_InformationNotFound]    Script Date: 31/07/2015 9:54:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_EBI_No_InformationNotFound]                                            
(                                            
 @SiteId int,                                            
 @Month int,                                            
 @Year int                                            
)                                            
AS                                  
DECLARE @Companies VARCHAR(MAX)                             
IF (@SiteId > 0)                                           
	BEGIN                      
	  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                
	  (SELECT  DISTINCT companyName FROM 
		(SELECT DISTINCT companyName, vendor_number FROM EBI A
		WHERE  A.SwipeSite = (SELECT SiteNameEbi FROM Sites WHERE SiteId = @SiteId)                
		AND  YEAR(A.SwipeDateTime) = @Year    
		AND MONTH(A.SwipeDateTime) =  @Month    
		AND A.CompanyName IS NOT NULL                
		AND A.CompanyName <>'' and vendor_Number is not null and vendor_number <> '') T1 inner  Join    
		(select vendor_number FROM CompanyVendor B WHERE cast(B.TAX_REGISTRATION_NUMBER AS CHAR(11)) NOT IN (
		SELECT CompanyAbn FROM Companies C))  T2
		ON T1.Vendor_Number = T2.Vendor_Number) X                     
	END                                    
ELSE                                    
	BEGIN                          
	  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM               
	  (SELECT  DISTINCT companyName FROM 
		(SELECT DISTINCT companyName, vendor_number FROM EBI A
		WHERE  A.SwipeSite IN (SELECT SiteNameEbi FROM Sites WHERE SiteId IN (SELECT SITEID FROM RegionsSites where RegionId = -1 * @SiteId) and sitenameEbi is not null)
		AND  YEAR(A.SwipeDateTime) = @Year    
		AND MONTH(A.SwipeDateTime) =  @Month   
		AND A.CompanyName IS NOT NULL                
		AND A.CompanyName <>'' and vendor_Number is not null and vendor_number <> '') T1 inner  Join    
		(select vendor_number FROM CompanyVendor B WHERE cast(B.TAX_REGISTRATION_NUMBER AS CHAR(11)) NOT IN (
		SELECT CompanyAbn FROM Companies C))  T2
		ON T1.Vendor_Number = T2.Vendor_Number) X                 
	END                 
                 
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                 
 AND [Month] = @Month AND [ItemName] = 'No - Information could not be found')                
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                
 SET [NonCompliantCompanies] = @Companies                
 WHERE [SiteId] = @SiteId                
   AND [Month] = @Month                
   AND [Year] = @Year                
   AND [ItemName] = 'No - Information could not be found'                
ELSE                
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                
           ([SiteId]                
           ,[Month]                
           ,[Year]                
           ,[ItemName]                
           ,[NonCompliantCompanies])                
     VALUES                
           (@SiteId                
           ,@Month                
           ,@Year                
           ,'No - Information could not be found'                
           ,@Companies)  
----------------------------------------  
GO


