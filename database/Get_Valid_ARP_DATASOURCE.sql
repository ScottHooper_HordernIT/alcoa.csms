

/****** Object:  StoredProcedure [dbo].[Get_Valid_ARP_DATASOURCE]    Script Date: 3/08/2015 4:50:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Get_Valid_ARP_DATASOURCE]          
AS        
select distinct(A.Person_id),A.Full_Name              
from HR..XXHR_ARP_CRP_ALL_V AS A              
LEFT OUTER JOIN HR..XXHR_PEOPLE_DETAILS_V AS B              
ON A.PERSON_ID = B.PERSON_ID              
LEFT OUTER JOIN CompanyVendor AS C              
ON C.VENDOR_NUMBER = B.CWK_VENDOR_NO              
LEFT OUTER JOIN Companies AS D              
ON D.CompanyAbn = C.TAX_REGISTRATION_NUMBER              
LEFT OUTER JOIN SITES AS E              
ON E.SITENAMEHR=B.LOCATION               
WHERE A.PER_TYPE_ID = 'E' OR A.PER_TYPE_ID = 'C'      
ORDER BY A.Full_Name
----------------------------------------

GO


