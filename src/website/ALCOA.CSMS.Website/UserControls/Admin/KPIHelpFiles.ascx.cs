﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class KPIHelpFiles : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); 
        }
        private void moduleInit(bool postBack)
        {
            if (!postBack)
            { //first time load
                if (auth.RoleId == (int)RoleList.Administrator)
                {
                    KpiHelpFilesService khfService = new KpiHelpFilesService();
                    TList<KpiHelpFiles> khf = khfService.GetByKpiHelpCaption("Default");
                    if (khf.Count==0)
                    {
                        hlAddNew.Visible = true;
                        grid.Visible = false;

                        if (hlAddNew != null)
                            hlAddNew.NavigateUrl = String.Format("javascript:popUp2('popups/editkpihelpfile.aspx');");
                    }
                    else
                    {
                        hlAddNew.Visible = false;
                        grid.Visible = true;
                    }
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
        }
        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {

        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {

        }
        protected void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            ASPxHyperLink hlEdit = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlEdit") as ASPxHyperLink;
            if (hlEdit != null)
                hlEdit.NavigateUrl = String.Format("javascript:popUp2('popups/editkpihelpfile.aspx?q={0}');",
                                                        (int)grid.GetRowValues(e.VisibleIndex, "KpiHelpFileId"));

            ASPxLabel lblFileSize = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblFileSize") as ASPxLabel;
            if (lblFileSize != null)
                lblFileSize.Text = String.Format("{0:n0}KB", KaiZen.Library.FileUtilities.Convert.BytestoKilobytes(Convert.ToInt64((int)grid.GetRowValues(e.VisibleIndex, "ContentLength"))));


        }
        protected void grid1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            e.NewValues["DocumentType"] = "KH";

            if (e.NewValues["UseDefault"] == null || e.NewValues["UseDefault"].ToString() == "0")
            {
             //do nothing   
            }

            if (!String.IsNullOrEmpty(Convert.ToString(e.NewValues["DocumentFileName"])))
            {
               //do nothing
            }
            else
            {

                e.NewValues["DocumentFileName"] = "Default";
                e.NewValues["DocumentName"] = "KPI Default Document";
            }
            

        }
        protected void grid1_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {

                string fileName = (string)grid1.GetRowValues(e.VisibleIndex, "DocumentFileName");
                Label label = grid1.FindRowCellTemplateControl(e.VisibleIndex, null, "lblRead") as Label;

                if (label != null)
                {


                    if (fileName=="Default")
                    {
                        label.Text = "Yes";
                        label.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        label.Text = "No";
                        label.ForeColor = System.Drawing.Color.Red;
                    }
                }


                ASPxHyperLink hl = grid1.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFileDownload") as ASPxHyperLink;
                if (hl != null)
                {
                    if (fileName == "Default")
                    {
                        hl.Text = fileName;
                        hl.NavigateUrl = "javascript:popUp2('PopUps/KpiHelpFilePopup.aspx?Help=Default');";
                    }
                    else
                    {
                        hl.Text = fileName;
                        hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MR&File=" + fileName));
                    }
                }
            }
           


        }

    }
}