using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data.SqlClient;
using DevExpress.XtraCharts;
using System.Threading;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using System.Drawing;
using System.Text;

// excellent use of oo and programming techniques below... lol
// TODO: write properly when time allows = never? lol
public partial class UserControls_SummaryComplianceBySite : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        Reports_DropDownListPicker1.btnHandler += new
                 UserControls_Tiny_Reports_DropDownListPicker2b.OnButtonClick(DropDownListPicker_btnHandler);

        SessionHandler.spVar_Page = "ComplianceReportBySite";

        ASPxGridView.RegisterBaseScript(this.Page);

        if (Request.QueryString["id"] != null)
        {

            if (!String.IsNullOrEmpty(Request.QueryString["id"].ToString()))
            {

                int i = 0;
                if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_Year)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_CategoryId)) { i++; };
                if (i == 0)
                {
                    if (Convert.ToInt32(SessionHandler.spVar_SiteId) != 0)
                    {
                        if (Convert.ToInt32(SessionHandler.spVar_SiteId) < 0)
                        {
                            SessionHandler.spVar_RegionId = (SessionHandler.spVar_SiteId).Replace("-", "");
                        }
                        DrawPage();
                    }
                    else
                    {
                        PopUpErrorMessage("Please Select All Search Fields.");
                        //error msg :)
                    }
                }
            }
        }
        else
        {
            //SessionHandler.spVar_Page = "ComplianceChart_A_A";
            SessionHandler.spVar_CompanyId = "-1";
            if (auth.RoleId == (int)RoleList.Contractor)
            {
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            }
            SessionHandler.spVar_SiteId = "";
            SessionHandler.spVar_CategoryId = "-1";
            SessionHandler.spVar_CategoryId2 = "%";

            int iYear = DateTime.Now.Year;
            if (DateTime.Now.Month == 1)
            {
                iYear = DateTime.Now.Year - 1;
            }
            SessionHandler.spVar_Year = iYear.ToString();
            //DrawPage();
        }

        if (!postBack)
        {
            Label1.Text = "As at: " + DateTime.Now.ToString("dd/MM/yyyy");

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader): //Reader
                    break;
                case ((int)RoleList.Contractor): //Contributor
                    break;
                case ((int)RoleList.Administrator): //Administrator
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    public void DrawPage()
    {
        if (String.IsNullOrEmpty(SessionHandler.spVar_ComplianceCount))
        {
            SessionHandler.spVar_ComplianceCount = "0";
        }
        Panel1.Visible = true;


        UserControls_Tiny_ComplianceReports_ComplianceReport_KPI uc_KPI =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_KPI)
                    (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_KPI.ascx"));
        uc_KPI.ID = "uc_KPI";
        ph_KPI.Controls.Add(uc_KPI);

        //Added by Sayani for DT2416
        UserControls_Tiny_ComplianceReports_ComplianceReport_SP uc_SP =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_SP)
                   (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_SP.ascx"));
        uc_SP.ID = "uc_SP";
        ph_SP.Controls.Add(uc_SP);

        //UserControls_Tiny_ComplianceReports_ComplianceReport_SP uc_SP = (UserControls_Tiny_ComplianceReports_ComplianceReport_SP)
        //        (LoadControl(String.Format("~/UserControls/Main/ComplianceReports/Summary/{0}_SP.ascx", SessionHandler.spVar_Page)));
        //uc_SP.ID = "uc_SP";

        //ph_SP.Controls.Add(uc_SP);
        //_i = _i + uc_SP.getExpected();

        UserControls_Tiny_ComplianceReports_ComplianceReport_CSA uc_CSA =
                (UserControls_Tiny_ComplianceReports_ComplianceReport_CSA)
                    (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_CSA.ascx"));
        uc_CSA.ID = "uc_CSA";
        ph_CSA.Controls.Add(uc_CSA);

        //if (auth.RoleId == (int)RoleList.Administrator)
        //{
        UserControls_Tiny_ComplianceReports_ComplianceReport_MS uc_MS = (UserControls_Tiny_ComplianceReports_ComplianceReport_MS)
                (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_MS.ascx"));
        uc_MS.ID = "uc_MS";
        ph_MS.Controls.Add(uc_MS);

        //Training Schedule not applicable in Vic Ops.
        int SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
        if (SiteId > 0)
        {
            RegionsService rService = new RegionsService();
            Regions r = rService.GetByRegionInternalName("VICOPS");
            RegionsSitesService rsService = new RegionsSitesService();
            TList<RegionsSites> rsList = rsService.GetBySiteId(SiteId);
            foreach (RegionsSites rs in rsList)
            {
                if (rs.RegionId == r.RegionId)
                {
                    lblTrainingSchedules.Text = "Training Schedules - n/a";
                }
            }
        }
        if (SiteId == -4)
        {
            lblTrainingSchedules.Text = "Training Schedules - n/a";
        }

        if (lblTrainingSchedules.Text != "Training Schedules - n/a")
        {
            UserControls_Tiny_ComplianceReports_ComplianceReport_TS uc_TS = (UserControls_Tiny_ComplianceReports_ComplianceReport_TS)
        (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_TS.ascx"));
            uc_TS.ID = "uc_TS";
            ph_TS.Controls.Add(uc_TS);
        }

        //DT 3190 changes to remove the EBI subreport for the timebeing
        //UserControls_ComplianceReport_EBI uc_EBI = (UserControls_ComplianceReport_EBI)
        //        (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_EBI.ascx"));
        //uc_EBI.ID = "uc_EBI";
        //ph_EBI.Controls.Add(uc_EBI);

        UserControls_Tiny_ComplianceReports_ComplianceReport_KPISQ uc_KPISQ = (UserControls_Tiny_ComplianceReports_ComplianceReport_KPISQ)
                (LoadControl("~/UserControls/Main/ComplianceReports/Summary/ComplianceReport_KPISQ.ascx"));
        uc_KPISQ.ID = "uc_KPISQ";
        ph_KPISQ.Controls.Add(uc_KPISQ);
    }

    void DropDownListPicker_btnHandler(String strvalue)
    {
        //((ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCompanies")).Value = 5;
        //load stuff
        try
        {
            ph_KPI.Controls.Clear();
            ph_CSA.Controls.Clear();

            ASPxComboBox ddlCategory = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCategory");
            ASPxComboBox ddlSites = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlSites");
            ASPxComboBox ddlYear = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlYear");

            string sCategory = string.Empty;
            string sSite = string.Empty;
            string sYear = string.Empty;

            
            sCategory = ddlCategory.SelectedItem.Value.ToString();
           
            
            try
            {
                sSite = ddlSites.SelectedItem.Value.ToString();
            }
            catch (Exception ex)
            {
                if(sCategory=="0")
                throw new Exception("Please select a valid site and a valid residential category");
                else
                    throw new Exception("Please select a valid site");
            }

            if (sCategory == "0")
                throw new Exception("Please select a valid residential category");

            sYear = ddlYear.SelectedItem.Value.ToString();
            
            int i = 0;
            if (String.IsNullOrEmpty(sCategory)) { i++; };
            if (String.IsNullOrEmpty(sSite)) { i++; };
            if (String.IsNullOrEmpty(sYear)) { i++; };
            
            if (i == 0)
            {
                SessionHandler.spVar_CompanyId = "";
                SessionHandler.spVar_SiteId = sSite;
                SessionHandler.spVar_SiteName = ddlSites.SelectedItem.Text;
                SessionHandler.spVar_Year = sYear;
                SessionHandler.spVar_CategoryId = sCategory;
                //temp
                switch (sCategory)
                {
                    case "0":
                        SessionHandler.spVar_CategoryId2 = "0";
                        break;
                    case "-1":
                        SessionHandler.spVar_CategoryId2 = "%";
                        break;
                    default:
                        SessionHandler.spVar_CategoryId2 = sCategory;
                        break;
                }

                if (sSite != "0")
                {
                    Response.Redirect("Reports_SummaryComplianceBySite.aspx?id=go", true);
                }
                else
                {
                    PopUpErrorMessage("Please Select All Search Fields.");
                }

            }

        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            
            PopUpErrorMessage(ex.Message);
        }
    }

    protected void PopUpErrorMessage(string errormsg)
    {
        Panel1.Visible = false;
        PopupWindow pcWindow = new PopupWindow(errormsg);
        pcWindow.FooterText = "";
        pcWindow.ShowOnPageLoad = true;
        pcWindow.Modal = true;
        //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
        ASPxPopupControl1.Windows.Add(pcWindow);
    }

    //protected void btnPdf_Click(object sender, EventArgs e)
    //{

    //    try
    //    {
    //        var pdf = new Byte[0];

    //        var links = new List<object>();

    //        Link pageBreak = new Link();
    //        pageBreak.CreateDetailArea += new CreateAreaEventHandler(pageBreak_CreateDetailArea);

    //        Link header = new Link();
    //        header.CreateDetailArea += new CreateAreaEventHandler(header_CreateDetailArea);

    //        links.Add(header);

    //        //Add kpi pdf data
    //        foreach (var control in ph_KPI.Controls)
    //        {
    //            var uc_KPI = control as UserControls_Tiny_ComplianceReports_ComplianceReport_KPI;
    //            if (uc_KPI != null)
    //            {
    //                links.Add(uc_KPI.ExportToPdf());
    //                links.Add(pageBreak);

    //                break;
    //            }
    //        }

    //        //Add safety management plans pdf data
    //        foreach (var control in ph_SP.Controls)
    //        {
    //            var uc_SP = control as UserControls_Tiny_ComplianceReports_ComplianceReport_SP;
    //            if (uc_SP != null)
    //            {
    //                links.Add(uc_SP.ExportToPdf());
    //                links.Add(pageBreak);

    //                break;
    //            }
    //        }

    //        //Add medical schedules pdf data
    //        foreach (var control in ph_MS.Controls)
    //        {
    //            var uc_MS = control as UserControls_Tiny_ComplianceReports_ComplianceReport_MS;
    //            if (uc_MS != null)
    //            {
    //                links.Add(uc_MS.ExportToPdf());
    //                links.Add(pageBreak);

    //                break;
    //            }
    //        }

    //        //Add training schedules pdf data
    //        foreach (var control in ph_TS.Controls)
    //        {
    //            var uc_TS = control as UserControls_Tiny_ComplianceReports_ComplianceReport_TS;
    //            if (uc_TS != null)
    //            {
    //                links.Add(uc_TS.ExportToPdf());
    //                links.Add(pageBreak);

    //                break;
    //            }
    //        }

    //        //Add Contractor Services Audit - Shows Embedded and Non Embedded 1 Sites Only pdf data
    //        foreach (var control in ph_CSA.Controls)
    //        {
    //            var uc_CSA = control as UserControls_Tiny_ComplianceReports_ComplianceReport_CSA;
    //            if (uc_CSA != null)
    //            {
    //                //byte[] title = System.Text.Encoding.UTF8.GetBytes("Contractor Services Audit - Shows Embedded and Non Embedded 1 Sites Only");
    //                //pdf = pdf.Concat(title);

    //                links.Add(uc_CSA.ExportEmbeddedToPdf());
    //                links.Add(pageBreak);

    //                //links.Add(uc_CSA.ExportNonEmbedded1ToPdf());

    //                break;
    //            }
    //        }

    //        //Add Contractors On Site (KPI) pdf data
    //        foreach (var control in ph_KPISQ.Controls)
    //        {
    //            var uc_KPISQ = control as UserControls_Tiny_ComplianceReports_ComplianceReport_KPISQ;
    //            if (uc_KPISQ != null)
    //            {
    //                links.Add(uc_KPISQ.ExportToPdf());

    //                break;
    //            }
    //        }

    //        CompositeLink compositeLink = new CompositeLink(new PrintingSystem());
    //        compositeLink.Links.AddRange(links.ToArray());

    //        //Add header/footer
    //        string pageNumberColumn = "Pages: [Page # of Pages #]";
    //        PageHeaderFooter phf = compositeLink.PageHeaderFooter as PageHeaderFooter;
    //        phf.Footer.Content.Clear();
    //        phf.Footer.Content.AddRange(new string[] { pageNumberColumn });
    //        phf.Footer.LineAlignment = BrickAlignment.Far;

    //        compositeLink.CreateDocument();

    //        using (MemoryStream stream = new MemoryStream())
    //        {
    //            compositeLink.PrintingSystem.Document.AutoFitToPagesWidth = 1;
    //            compositeLink.PrintingSystem.ExportToPdf(stream);
    //            pdf = stream.ToArray();
    //        }

    //        //Export pdf to browser
    //        Response.ClearContent();
    //        Response.Clear();
            
    //        Response.ContentType = "application/pdf";
    //        Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", "Summary Compliance Report By Site"));
    //        Response.BinaryWrite(pdf.ToArray());
    //    }
    //    catch (Exception ex)
    //    {
    //        //string strMessage = string.Empty;
    //        //if (Session["ImagePath"] != null)
    //        //{
    //        //    strMessage = Session["ImagePath"].ToString();
    //        //}
    //        //Response.Write(ex.Message);
    //        //throw new Exception(ex.Message);
    //    }

    //}

    protected void btnPdf_Click(object sender, EventArgs e)
    {

        try
        {
            //Get pdf template
            string contents = File.ReadAllText(Server.MapPath(@"pdfTemplateSummaryComplianceBySite.htm"));

            //Get filters
            ASPxComboBox ddlCategory = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCategory");
            ASPxComboBox ddlSites = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlSites");
            ASPxComboBox ddlYear = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlYear");

            string sCategory = "";
            string sSite = "";
            string sYear = "";

            try
            {
                sSite = ddlSites.SelectedItem.Text.ToString();
                sCategory = ddlCategory.SelectedItem.Text.ToString();
                sYear = ddlYear.SelectedItem.Text.ToString();
            }
            catch { }

            contents = contents.Replace("[Now]", DateTime.Now.Date.ToShortDateString());
            contents = contents.Replace("[Site]", sSite);
            contents = contents.Replace("[Category]", sCategory);
            contents = contents.Replace("[Year]", sYear);

            //Add kpi pdf data
            foreach (var control in ph_KPI.Controls)
            {
                var uc_KPI = control as UserControls_Tiny_ComplianceReports_ComplianceReport_KPI;
                if (uc_KPI != null)
                {
                    contents = contents.Replace("[KPI]", uc_KPI.ExportToHtml());

                    break;
                }
            }

            //Add safety management plans pdf data
            foreach (var control in ph_SP.Controls)
            {
                var uc_SP = control as UserControls_Tiny_ComplianceReports_ComplianceReport_SP;
                if (uc_SP != null)
                {
                    contents = contents.Replace("[SP]", uc_SP.ExportToHtml());

                    break;
                }
            }

            //Add medical schedules pdf data
            foreach (var control in ph_MS.Controls)
            {
                var uc_MS = control as UserControls_Tiny_ComplianceReports_ComplianceReport_MS;
                if (uc_MS != null)
                {
                    contents = contents.Replace("[MS]", uc_MS.ExportToHtml());

                    break;
                }
            }

            //Add training schedules pdf data
            foreach (var control in ph_TS.Controls)
            {
                var uc_TS = control as UserControls_Tiny_ComplianceReports_ComplianceReport_TS;
                if (uc_TS != null)
                {
                    contents = contents.Replace("[TS]", uc_TS.ExportToHtml());

                    break;
                }
            }

            //Add Contractor Services Audit - Shows Embedded and Non Embedded 1 Sites Only pdf data
            foreach (var control in ph_CSA.Controls)
            {
                var uc_CSA = control as UserControls_Tiny_ComplianceReports_ComplianceReport_CSA;
                if (uc_CSA != null)
                {
                    contents = contents.Replace("[CSA]", uc_CSA.ExportToHtml());

                    break;
                }
            }

            //Add Contractors On Site (KPI) pdf data
            foreach (var control in ph_KPISQ.Controls)
            {
                var uc_KPISQ = control as UserControls_Tiny_ComplianceReports_ComplianceReport_KPISQ;
                if (uc_KPISQ != null)
                {
                    contents = contents.Replace("[KPISQ]", uc_KPISQ.ExportToHtml());

                    break;
                }
            }

            //Get base url path
            string urlPath = string.Empty;
            string[] arr = Request.Url.ToString().Split('/');
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (i > 0) urlPath = urlPath + "/";
                urlPath = urlPath + arr[i];
            }
            contents = contents.Replace("[UrlPath]", urlPath);

            var document = new iTextSharp.text.Document(PageSize.A4.Rotate(), 25, 25, 25, 25);

            MemoryStream output = new MemoryStream();
            Object writer = PdfWriter.GetInstance(document, output);

            document.Open();

            List<IElement> parsedHtmlElements = iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(new StringReader(contents), null);
            foreach (object htmlElement in parsedHtmlElements)
            {
                document.Add((IElement)htmlElement);

            }
            document.Close();

            //Export pdf to browser
            Response.ClearContent();
            Response.Clear();

            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", "Summary Compliance Report By Site"));
            Response.BinaryWrite(output.ToArray());
        }
        catch (Exception ex)
        {
            //string strMessage = string.Empty;
            //if (Session["ImagePath"] != null)
            //{
            //    strMessage = Session["ImagePath"].ToString();
            //}
            //Response.Write(ex.Message);
            //throw new Exception(ex.Message);
        }

    }

    void pageBreak_CreateDetailArea(object sender, CreateAreaEventArgs e)
    {
        Link s = sender as Link;
        s.PrintingSystem.InsertPageBreak(0);
    }

    void header_CreateDetailArea(object sender, CreateAreaEventArgs e)
    {
        //Print pdf header
        string header = string.Format("Quantitative Comparison of Target vs Actual  As at: {0}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
        TextBrick brick1 = e.Graph.DrawString(header, System.Drawing.ColorTranslator.FromHtml("#000000"), new RectangleF(0, 0, 900, 38), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, 18, FontStyle.Regular);

        //Get filters
        ASPxComboBox ddlCategory = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlCategory");
        ASPxComboBox ddlSites = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlSites");
        ASPxComboBox ddlYear = (ASPxComboBox)Reports_DropDownListPicker1.FindControl("ddlYear");

        string sCategory = "";
        string sSite = "";
        string sYear = "";

        try
        {
            sSite = ddlSites.SelectedItem.Text.ToString();
            sCategory = ddlCategory.SelectedItem.Text.ToString();
            sYear = ddlYear.SelectedItem.Text.ToString();
        }
        catch { }

        //Print filters
        string filters = string.Format("Site: {0}; Residential Category: {1}; Year: {2}", sSite, sCategory, sYear);
        TextBrick brick2 = e.Graph.DrawString(filters, System.Drawing.ColorTranslator.FromHtml("#000000"), new RectangleF(0, 38, 900, 30), DevExpress.XtraPrinting.BorderSide.None);
        brick2.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick2.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }
}

