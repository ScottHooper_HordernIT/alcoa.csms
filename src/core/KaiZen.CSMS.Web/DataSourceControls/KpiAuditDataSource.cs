﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(KpiAuditDataSourceDesigner))]
	public class KpiAuditDataSource : ProviderDataSource<KpiAudit, KpiAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditDataSource class.
		/// </summary>
		public KpiAuditDataSource() : base(new KpiAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiAuditDataSourceView used by the KpiAuditDataSource.
		/// </summary>
		protected KpiAuditDataSourceView KpiAuditView
		{
			get { return ( View as KpiAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiAuditDataSource control invokes to retrieve data.
		/// </summary>
		public KpiAuditSelectMethod SelectMethod
		{
			get
			{
				KpiAuditSelectMethod selectMethod = KpiAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiAuditDataSourceView class that is to be
		/// used by the KpiAuditDataSource.
		/// </summary>
		/// <returns>An instance of the KpiAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiAudit, KpiAuditKey> GetNewDataSourceView()
		{
			return new KpiAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiAuditDataSourceView : ProviderDataSourceView<KpiAudit, KpiAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiAuditDataSourceView(KpiAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiAuditDataSource KpiAuditOwner
		{
			get { return Owner as KpiAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal KpiAuditSelectMethod SelectMethod
		{
			get { return KpiAuditOwner.SelectMethod; }
			set { KpiAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiAuditService KpiAuditProvider
		{
			get { return Provider as KpiAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<KpiAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<KpiAudit> results = null;
			KpiAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case KpiAuditSelectMethod.Get:
					KpiAuditKey entityKey  = new KpiAuditKey();
					entityKey.Load(values);
					item = KpiAuditProvider.Get(entityKey);
					results = new TList<KpiAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiAuditSelectMethod.GetAll:
                    results = KpiAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiAuditSelectMethod.GetPaged:
					results = KpiAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case KpiAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = KpiAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = KpiAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case KpiAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = KpiAuditProvider.GetByAuditId(_auditId);
					results = new TList<KpiAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == KpiAuditSelectMethod.Get || SelectMethod == KpiAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				KpiAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					KpiAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<KpiAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			KpiAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region KpiAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiAuditDataSource class.
	/// </summary>
	public class KpiAuditDataSourceDesigner : ProviderDataSourceDesigner<KpiAudit, KpiAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the KpiAuditDataSourceDesigner class.
		/// </summary>
		public KpiAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiAuditSelectMethod SelectMethod
		{
			get { return ((KpiAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new KpiAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region KpiAuditDataSourceActionList

	/// <summary>
	/// Supports the KpiAuditDataSourceDesigner class.
	/// </summary>
	internal class KpiAuditDataSourceActionList : DesignerActionList
	{
		private KpiAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the KpiAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public KpiAuditDataSourceActionList(KpiAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion KpiAuditDataSourceActionList
	
	#endregion KpiAuditDataSourceDesigner
	
	#region KpiAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the KpiAuditDataSource.SelectMethod property.
	/// </summary>
	public enum KpiAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion KpiAuditSelectMethod

	#region KpiAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditFilter : SqlFilter<KpiAuditColumn>
	{
	}
	
	#endregion KpiAuditFilter

	#region KpiAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditExpressionBuilder : SqlExpressionBuilder<KpiAuditColumn>
	{
	}
	
	#endregion KpiAuditExpressionBuilder	

	#region KpiAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;KpiAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="KpiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiAuditProperty : ChildEntityProperty<KpiAuditChildEntityTypes>
	{
	}
	
	#endregion KpiAuditProperty
}

