﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class Error : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Auth auth = new Auth();

            Configuration configuration = new Configuration();
            lblSorry.Text = String.Format("Sorry! An Unexpected error occurred. The CSMS Technical Support Team have been notified. Please try again shortly or contact us via <a href='mailto:{0}' title='Click to E-Mail the CSMS Team'>e-mail</a>.", configuration.GetValue(ConfigList.ContactEmail));

            string error = Request.QueryString["error"];
            if (!String.IsNullOrEmpty(error))
            {
                if (error == "Not Logged In.") throw new Exception("Not Logged In.");

                lblErrorMessage.Text = error;
            }
            else
            {
                lblErrorMessage.Text = "No Error Specified.";
            }
        }
        catch (Exception ex)
        {
            if (ex.Message != "Not Logged In.")
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }

            Response.Redirect("AccessDenied.aspx" + QueryStringModule.Encrypt("error=" + ex.Message));
        }
    }
}
