﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICsmsEmailRecipientTypeService
    {
        CsmsEmailRecipientType Get(Expression<Func<CsmsEmailRecipientType, bool>> where, List<Expression<Func<CsmsEmailRecipientType, object>>> includes);
        List<CsmsEmailRecipientType> GetMany(int? top, Expression<Func<CsmsEmailRecipientType, bool>> where, Func<IQueryable<CsmsEmailRecipientType>, IOrderedQueryable<CsmsEmailRecipientType>> orderBy, List<Expression<Func<CsmsEmailRecipientType, object>>> includes);
        List<CsmsEmailRecipientType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsEmailRecipientType, bool>> where, Func<IQueryable<CsmsEmailRecipientType>, IOrderedQueryable<CsmsEmailRecipientType>> orderBy, List<Expression<Func<CsmsEmailRecipientType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CsmsEmailRecipientType, bool>> where, List<Expression<Func<CsmsEmailRecipientType, object>>> includes);
        void Insert(CsmsEmailRecipientType item, bool saveChanges = true);
        void Update(CsmsEmailRecipientType item, bool saveChanges = true);
        void Delete(CsmsEmailRecipientType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CsmsEmailRecipientTypeService : ICsmsEmailRecipientTypeService
    {
        private readonly IRepository<CsmsEmailRecipientType> _repository;

        public CsmsEmailRecipientTypeService(IRepository<CsmsEmailRecipientType> repository)
        {
            this._repository = repository;
        }

        public CsmsEmailRecipientType Get(Expression<Func<CsmsEmailRecipientType, bool>> where, List<Expression<Func<CsmsEmailRecipientType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CsmsEmailRecipientType> GetMany(int? top, Expression<Func<CsmsEmailRecipientType, bool>> where, Func<IQueryable<CsmsEmailRecipientType>, IOrderedQueryable<CsmsEmailRecipientType>> orderBy, List<Expression<Func<CsmsEmailRecipientType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CsmsEmailRecipientType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsEmailRecipientType, bool>> where, Func<IQueryable<CsmsEmailRecipientType>, IOrderedQueryable<CsmsEmailRecipientType>> orderBy, List<Expression<Func<CsmsEmailRecipientType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CsmsEmailRecipientType, bool>> where, List<Expression<Func<CsmsEmailRecipientType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CsmsEmailRecipientType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsEmailRecipientType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CsmsEmailRecipientType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsEmailRecipientType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CsmsEmailRecipientType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsEmailRecipientType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
