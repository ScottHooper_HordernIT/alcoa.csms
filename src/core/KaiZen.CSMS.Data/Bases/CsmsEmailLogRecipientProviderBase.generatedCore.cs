﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsmsEmailLogRecipientProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsmsEmailLogRecipientProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CsmsEmailLogRecipient, KaiZen.CSMS.Entities.CsmsEmailLogRecipientKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLogRecipientKey key)
		{
			return Delete(transactionManager, key.CsmsEmailRecipientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csmsEmailRecipientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csmsEmailRecipientId)
		{
			return Delete(null, _csmsEmailRecipientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailLog key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailLog Description: 
		/// </summary>
		/// <param name="_csmsEmailLogId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId)
		{
			int count = -1;
			return GetByCsmsEmailLogId(_csmsEmailLogId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailLog key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailLog Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		/// <remarks></remarks>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId)
		{
			int count = -1;
			return GetByCsmsEmailLogId(transactionManager, _csmsEmailLogId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailLog key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailLog Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailLogId(transactionManager, _csmsEmailLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailLog key.
		///		fkCsmsEmailLogRecipientCsmsEmailLog Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsEmailLogId(null, _csmsEmailLogId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailLog key.
		///		fkCsmsEmailLogRecipientCsmsEmailLog Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId, int start, int pageLength,out int count)
		{
			return GetByCsmsEmailLogId(null, _csmsEmailLogId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailLog key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailLog Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public abstract TList<CsmsEmailLogRecipient> GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailRecipientType key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailRecipientType Description: 
		/// </summary>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailRecipientTypeId(System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientTypeId(_csmsEmailRecipientTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailRecipientType key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailRecipientType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		/// <remarks></remarks>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientTypeId(transactionManager, _csmsEmailRecipientTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailRecipientType key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailRecipientType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailRecipientTypeId(transactionManager, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailRecipientType key.
		///		fkCsmsEmailLogRecipientCsmsEmailRecipientType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailRecipientTypeId(System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsEmailRecipientTypeId(null, _csmsEmailRecipientTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailRecipientType key.
		///		fkCsmsEmailLogRecipientCsmsEmailRecipientType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public TList<CsmsEmailLogRecipient> GetByCsmsEmailRecipientTypeId(System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength,out int count)
		{
			return GetByCsmsEmailRecipientTypeId(null, _csmsEmailRecipientTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsEmailLogRecipient_CsmsEmailRecipientType key.
		///		FK_CsmsEmailLogRecipient_CsmsEmailRecipientType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsEmailLogRecipient objects.</returns>
		public abstract TList<CsmsEmailLogRecipient> GetByCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CsmsEmailLogRecipient Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLogRecipientKey key, int start, int pageLength)
		{
			return GetByCsmsEmailRecipientId(transactionManager, key.CsmsEmailRecipientId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CsmsEmailRecipient index.
		/// </summary>
		/// <param name="_csmsEmailRecipientId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLogRecipient GetByCsmsEmailRecipientId(System.Int32 _csmsEmailRecipientId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientId(null,_csmsEmailRecipientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipient index.
		/// </summary>
		/// <param name="_csmsEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLogRecipient GetByCsmsEmailRecipientId(System.Int32 _csmsEmailRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailRecipientId(null, _csmsEmailRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLogRecipient GetByCsmsEmailRecipientId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientId(transactionManager, _csmsEmailRecipientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLogRecipient GetByCsmsEmailRecipientId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailRecipientId(transactionManager, _csmsEmailRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipient index.
		/// </summary>
		/// <param name="_csmsEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLogRecipient GetByCsmsEmailRecipientId(System.Int32 _csmsEmailRecipientId, int start, int pageLength, out int count)
		{
			return GetByCsmsEmailRecipientId(null, _csmsEmailRecipientId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsEmailLogRecipient GetByCsmsEmailRecipientId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CsmsEmailLogRecipient&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CsmsEmailLogRecipient&gt;"/></returns>
		public static TList<CsmsEmailLogRecipient> Fill(IDataReader reader, TList<CsmsEmailLogRecipient> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CsmsEmailLogRecipient c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CsmsEmailLogRecipient")
					.Append("|").Append((System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailRecipientId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CsmsEmailLogRecipient>(
					key.ToString(), // EntityTrackingKey
					"CsmsEmailLogRecipient",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CsmsEmailLogRecipient();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsmsEmailRecipientId = (System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailRecipientId - 1)];
					c.CsmsEmailLogId = (System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailLogId - 1)];
					c.CsmsEmailRecipientTypeId = (System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailRecipientTypeId - 1)];
					c.RecipientAddress = (System.String)reader[((int)CsmsEmailLogRecipientColumn.RecipientAddress - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CsmsEmailLogRecipient entity)
		{
			if (!reader.Read()) return;
			
			entity.CsmsEmailRecipientId = (System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailRecipientId - 1)];
			entity.CsmsEmailLogId = (System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailLogId - 1)];
			entity.CsmsEmailRecipientTypeId = (System.Int32)reader[((int)CsmsEmailLogRecipientColumn.CsmsEmailRecipientTypeId - 1)];
			entity.RecipientAddress = (System.String)reader[((int)CsmsEmailLogRecipientColumn.RecipientAddress - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CsmsEmailLogRecipient entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsmsEmailRecipientId = (System.Int32)dataRow["CsmsEmailRecipientId"];
			entity.CsmsEmailLogId = (System.Int32)dataRow["CsmsEmailLogId"];
			entity.CsmsEmailRecipientTypeId = (System.Int32)dataRow["CsmsEmailRecipientTypeId"];
			entity.RecipientAddress = (System.String)dataRow["RecipientAddress"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailLogRecipient"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsEmailLogRecipient Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLogRecipient entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CsmsEmailLogIdSource	
			if (CanDeepLoad(entity, "CsmsEmailLog|CsmsEmailLogIdSource", deepLoadType, innerList) 
				&& entity.CsmsEmailLogIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsEmailLogId;
				CsmsEmailLog tmpEntity = EntityManager.LocateEntity<CsmsEmailLog>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsEmailLog), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsEmailLogIdSource = tmpEntity;
				else
					entity.CsmsEmailLogIdSource = DataRepository.CsmsEmailLogProvider.GetByCsmsEmailLogId(transactionManager, entity.CsmsEmailLogId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsEmailLogIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsEmailLogIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsEmailLogProvider.DeepLoad(transactionManager, entity.CsmsEmailLogIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsEmailLogIdSource

			#region CsmsEmailRecipientTypeIdSource	
			if (CanDeepLoad(entity, "CsmsEmailRecipientType|CsmsEmailRecipientTypeIdSource", deepLoadType, innerList) 
				&& entity.CsmsEmailRecipientTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsEmailRecipientTypeId;
				CsmsEmailRecipientType tmpEntity = EntityManager.LocateEntity<CsmsEmailRecipientType>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsEmailRecipientType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsEmailRecipientTypeIdSource = tmpEntity;
				else
					entity.CsmsEmailRecipientTypeIdSource = DataRepository.CsmsEmailRecipientTypeProvider.GetByCsmsEmailRecipientEmailTypeId(transactionManager, entity.CsmsEmailRecipientTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsEmailRecipientTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsEmailRecipientTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsEmailRecipientTypeProvider.DeepLoad(transactionManager, entity.CsmsEmailRecipientTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsEmailRecipientTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CsmsEmailLogRecipient object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CsmsEmailLogRecipient instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsEmailLogRecipient Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLogRecipient entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CsmsEmailLogIdSource
			if (CanDeepSave(entity, "CsmsEmailLog|CsmsEmailLogIdSource", deepSaveType, innerList) 
				&& entity.CsmsEmailLogIdSource != null)
			{
				DataRepository.CsmsEmailLogProvider.Save(transactionManager, entity.CsmsEmailLogIdSource);
				entity.CsmsEmailLogId = entity.CsmsEmailLogIdSource.CsmsEmailLogId;
			}
			#endregion 
			
			#region CsmsEmailRecipientTypeIdSource
			if (CanDeepSave(entity, "CsmsEmailRecipientType|CsmsEmailRecipientTypeIdSource", deepSaveType, innerList) 
				&& entity.CsmsEmailRecipientTypeIdSource != null)
			{
				DataRepository.CsmsEmailRecipientTypeProvider.Save(transactionManager, entity.CsmsEmailRecipientTypeIdSource);
				entity.CsmsEmailRecipientTypeId = entity.CsmsEmailRecipientTypeIdSource.CsmsEmailRecipientEmailTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsmsEmailLogRecipientChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CsmsEmailLogRecipient</c>
	///</summary>
	public enum CsmsEmailLogRecipientChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CsmsEmailLog</c> at CsmsEmailLogIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsEmailLog))]
		CsmsEmailLog,
			
		///<summary>
		/// Composite Property for <c>CsmsEmailRecipientType</c> at CsmsEmailRecipientTypeIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsEmailRecipientType))]
		CsmsEmailRecipientType,
		}
	
	#endregion CsmsEmailLogRecipientChildEntityTypes
	
	#region CsmsEmailLogRecipientFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsmsEmailLogRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientFilterBuilder : SqlFilterBuilder<CsmsEmailLogRecipientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientFilterBuilder class.
		/// </summary>
		public CsmsEmailLogRecipientFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogRecipientFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogRecipientFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogRecipientFilterBuilder
	
	#region CsmsEmailLogRecipientParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsmsEmailLogRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogRecipientParameterBuilder : ParameterizedSqlFilterBuilder<CsmsEmailLogRecipientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientParameterBuilder class.
		/// </summary>
		public CsmsEmailLogRecipientParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogRecipientParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogRecipientParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogRecipientParameterBuilder
	
	#region CsmsEmailLogRecipientSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsmsEmailLogRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLogRecipient"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsmsEmailLogRecipientSortBuilder : SqlSortBuilder<CsmsEmailLogRecipientColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogRecipientSqlSortBuilder class.
		/// </summary>
		public CsmsEmailLogRecipientSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsmsEmailLogRecipientSortBuilder
	
} // end namespace
