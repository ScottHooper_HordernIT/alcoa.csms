﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit02Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit02DataSourceDesigner))]
	public class TwentyOnePointAudit02DataSource : ProviderDataSource<TwentyOnePointAudit02, TwentyOnePointAudit02Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02DataSource class.
		/// </summary>
		public TwentyOnePointAudit02DataSource() : base(new TwentyOnePointAudit02Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit02DataSourceView used by the TwentyOnePointAudit02DataSource.
		/// </summary>
		protected TwentyOnePointAudit02DataSourceView TwentyOnePointAudit02View
		{
			get { return ( View as TwentyOnePointAudit02DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit02DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit02SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit02SelectMethod selectMethod = TwentyOnePointAudit02SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit02SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit02DataSourceView class that is to be
		/// used by the TwentyOnePointAudit02DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit02DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit02, TwentyOnePointAudit02Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit02DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit02DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit02DataSourceView : ProviderDataSourceView<TwentyOnePointAudit02, TwentyOnePointAudit02Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit02DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit02DataSourceView(TwentyOnePointAudit02DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit02DataSource TwentyOnePointAudit02Owner
		{
			get { return Owner as TwentyOnePointAudit02DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit02SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit02Owner.SelectMethod; }
			set { TwentyOnePointAudit02Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit02Service TwentyOnePointAudit02Provider
		{
			get { return Provider as TwentyOnePointAudit02Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit02> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit02> results = null;
			TwentyOnePointAudit02 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit02SelectMethod.Get:
					TwentyOnePointAudit02Key entityKey  = new TwentyOnePointAudit02Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit02Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit02>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit02SelectMethod.GetAll:
                    results = TwentyOnePointAudit02Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit02SelectMethod.GetPaged:
					results = TwentyOnePointAudit02Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit02SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit02Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit02Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit02SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit02Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit02>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit02SelectMethod.Get || SelectMethod == TwentyOnePointAudit02SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit02 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit02Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit02> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit02Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit02DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit02DataSource class.
	/// </summary>
	public class TwentyOnePointAudit02DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit02, TwentyOnePointAudit02Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit02DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit02SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit02DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit02DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit02DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit02DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit02DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit02DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit02DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit02DataSourceActionList(TwentyOnePointAudit02DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit02SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit02DataSourceActionList
	
	#endregion TwentyOnePointAudit02DataSourceDesigner
	
	#region TwentyOnePointAudit02SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit02DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit02SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit02SelectMethod

	#region TwentyOnePointAudit02Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit02"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit02Filter : SqlFilter<TwentyOnePointAudit02Column>
	{
	}
	
	#endregion TwentyOnePointAudit02Filter

	#region TwentyOnePointAudit02ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit02"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit02ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit02Column>
	{
	}
	
	#endregion TwentyOnePointAudit02ExpressionBuilder	

	#region TwentyOnePointAudit02Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit02ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit02"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit02Property : ChildEntityProperty<TwentyOnePointAudit02ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit02Property
}

