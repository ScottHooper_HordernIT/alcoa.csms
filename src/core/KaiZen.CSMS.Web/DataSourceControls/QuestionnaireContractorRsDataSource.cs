﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireContractorRsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireContractorRsDataSourceDesigner))]
	public class QuestionnaireContractorRsDataSource : ProviderDataSource<QuestionnaireContractorRs, QuestionnaireContractorRsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsDataSource class.
		/// </summary>
		public QuestionnaireContractorRsDataSource() : base(new QuestionnaireContractorRsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireContractorRsDataSourceView used by the QuestionnaireContractorRsDataSource.
		/// </summary>
		protected QuestionnaireContractorRsDataSourceView QuestionnaireContractorRsView
		{
			get { return ( View as QuestionnaireContractorRsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireContractorRsDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireContractorRsSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireContractorRsSelectMethod selectMethod = QuestionnaireContractorRsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireContractorRsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireContractorRsDataSourceView class that is to be
		/// used by the QuestionnaireContractorRsDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireContractorRsDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireContractorRs, QuestionnaireContractorRsKey> GetNewDataSourceView()
		{
			return new QuestionnaireContractorRsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireContractorRsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireContractorRsDataSourceView : ProviderDataSourceView<QuestionnaireContractorRs, QuestionnaireContractorRsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireContractorRsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireContractorRsDataSourceView(QuestionnaireContractorRsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireContractorRsDataSource QuestionnaireContractorRsOwner
		{
			get { return Owner as QuestionnaireContractorRsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireContractorRsSelectMethod SelectMethod
		{
			get { return QuestionnaireContractorRsOwner.SelectMethod; }
			set { QuestionnaireContractorRsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireContractorRsService QuestionnaireContractorRsProvider
		{
			get { return Provider as QuestionnaireContractorRsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireContractorRs> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireContractorRs> results = null;
			QuestionnaireContractorRs item;
			count = 0;
			
			System.Int32 _questionnaireContractorRsId;
			System.Int32 _contractorCompanyId;
			System.Int32 _subContractorCompanyId;

			switch ( SelectMethod )
			{
				case QuestionnaireContractorRsSelectMethod.Get:
					QuestionnaireContractorRsKey entityKey  = new QuestionnaireContractorRsKey();
					entityKey.Load(values);
					item = QuestionnaireContractorRsProvider.Get(entityKey);
					results = new TList<QuestionnaireContractorRs>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireContractorRsSelectMethod.GetAll:
                    results = QuestionnaireContractorRsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireContractorRsSelectMethod.GetPaged:
					results = QuestionnaireContractorRsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireContractorRsSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireContractorRsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireContractorRsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireContractorRsSelectMethod.GetByQuestionnaireContractorRsId:
					_questionnaireContractorRsId = ( values["QuestionnaireContractorRsId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireContractorRsId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireContractorRsProvider.GetByQuestionnaireContractorRsId(_questionnaireContractorRsId);
					results = new TList<QuestionnaireContractorRs>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireContractorRsSelectMethod.GetByContractorCompanyIdSubContractorCompanyId:
					_contractorCompanyId = ( values["ContractorCompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContractorCompanyId"], typeof(System.Int32)) : (int)0;
					_subContractorCompanyId = ( values["SubContractorCompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SubContractorCompanyId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireContractorRsProvider.GetByContractorCompanyIdSubContractorCompanyId(_contractorCompanyId, _subContractorCompanyId);
					results = new TList<QuestionnaireContractorRs>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case QuestionnaireContractorRsSelectMethod.GetByContractorCompanyId:
					_contractorCompanyId = ( values["ContractorCompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ContractorCompanyId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireContractorRsProvider.GetByContractorCompanyId(_contractorCompanyId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireContractorRsSelectMethod.GetBySubContractorCompanyId:
					_subContractorCompanyId = ( values["SubContractorCompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SubContractorCompanyId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireContractorRsProvider.GetBySubContractorCompanyId(_subContractorCompanyId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireContractorRsSelectMethod.Get || SelectMethod == QuestionnaireContractorRsSelectMethod.GetByQuestionnaireContractorRsId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireContractorRs entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireContractorRsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireContractorRs> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireContractorRsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireContractorRsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireContractorRsDataSource class.
	/// </summary>
	public class QuestionnaireContractorRsDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireContractorRs, QuestionnaireContractorRsKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsDataSourceDesigner class.
		/// </summary>
		public QuestionnaireContractorRsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireContractorRsSelectMethod SelectMethod
		{
			get { return ((QuestionnaireContractorRsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireContractorRsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireContractorRsDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireContractorRsDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireContractorRsDataSourceActionList : DesignerActionList
	{
		private QuestionnaireContractorRsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContractorRsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireContractorRsDataSourceActionList(QuestionnaireContractorRsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireContractorRsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireContractorRsDataSourceActionList
	
	#endregion QuestionnaireContractorRsDataSourceDesigner
	
	#region QuestionnaireContractorRsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireContractorRsDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireContractorRsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireContractorRsId method.
		/// </summary>
		GetByQuestionnaireContractorRsId,
		/// <summary>
		/// Represents the GetByContractorCompanyIdSubContractorCompanyId method.
		/// </summary>
		GetByContractorCompanyIdSubContractorCompanyId,
		/// <summary>
		/// Represents the GetByContractorCompanyId method.
		/// </summary>
		GetByContractorCompanyId,
		/// <summary>
		/// Represents the GetBySubContractorCompanyId method.
		/// </summary>
		GetBySubContractorCompanyId
	}
	
	#endregion QuestionnaireContractorRsSelectMethod

	#region QuestionnaireContractorRsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsFilter : SqlFilter<QuestionnaireContractorRsColumn>
	{
	}
	
	#endregion QuestionnaireContractorRsFilter

	#region QuestionnaireContractorRsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsExpressionBuilder : SqlExpressionBuilder<QuestionnaireContractorRsColumn>
	{
	}
	
	#endregion QuestionnaireContractorRsExpressionBuilder	

	#region QuestionnaireContractorRsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireContractorRsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContractorRs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContractorRsProperty : ChildEntityProperty<QuestionnaireContractorRsChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireContractorRsProperty
}

