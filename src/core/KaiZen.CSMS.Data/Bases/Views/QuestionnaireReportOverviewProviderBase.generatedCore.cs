﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportOverviewProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportOverviewProviderBaseCore : EntityViewProviderBase<QuestionnaireReportOverview>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireReportOverview_FilterCompanies
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterCompanies()
		{
			return FilterCompanies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterCompanies(int start, int pageLength)
		{
			return FilterCompanies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterCompanies(TransactionManager transactionManager)
		{
			return FilterCompanies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet FilterCompanies(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireReportOverview_ListDistinctCompanies
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_ListDistinctCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListDistinctCompanies()
		{
			return ListDistinctCompanies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_ListDistinctCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListDistinctCompanies(int start, int pageLength)
		{
			return ListDistinctCompanies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_ListDistinctCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListDistinctCompanies(TransactionManager transactionManager)
		{
			return ListDistinctCompanies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_ListDistinctCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListDistinctCompanies(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireReportOverview_GetByTypeSiteCompanyService
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_GetByTypeSiteCompanyService' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="typeOfServiceId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;QuestionnaireReportOverview&gt;"/> instance.</returns>
		public VList<QuestionnaireReportOverview> GetByTypeSiteCompanyService(System.String type, System.Int32? siteId, System.Int32? companyId, System.Int32? typeOfServiceId)
		{
			return GetByTypeSiteCompanyService(null, 0, int.MaxValue , type, siteId, companyId, typeOfServiceId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_GetByTypeSiteCompanyService' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="typeOfServiceId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;QuestionnaireReportOverview&gt;"/> instance.</returns>
		public VList<QuestionnaireReportOverview> GetByTypeSiteCompanyService(int start, int pageLength, System.String type, System.Int32? siteId, System.Int32? companyId, System.Int32? typeOfServiceId)
		{
			return GetByTypeSiteCompanyService(null, start, pageLength , type, siteId, companyId, typeOfServiceId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_GetByTypeSiteCompanyService' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="typeOfServiceId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;QuestionnaireReportOverview&gt;"/> instance.</returns>
		public VList<QuestionnaireReportOverview> GetByTypeSiteCompanyService(TransactionManager transactionManager, System.String type, System.Int32? siteId, System.Int32? companyId, System.Int32? typeOfServiceId)
		{
			return GetByTypeSiteCompanyService(transactionManager, 0, int.MaxValue , type, siteId, companyId, typeOfServiceId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_GetByTypeSiteCompanyService' stored procedure. 
		/// </summary>
		/// <param name="type"> A <c>System.String</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="typeOfServiceId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;QuestionnaireReportOverview&gt;"/> instance.</returns>
		public abstract VList<QuestionnaireReportOverview> GetByTypeSiteCompanyService(TransactionManager transactionManager, int start, int pageLength, System.String type, System.Int32? siteId, System.Int32? companyId, System.Int32? typeOfServiceId);
		
		#endregion

		
		#region _QuestionnaireReportOverview_FilterType
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterType' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterType()
		{
			return FilterType(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterType' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterType(int start, int pageLength)
		{
			return FilterType(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterType' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterType(TransactionManager transactionManager)
		{
			return FilterType(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterType' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet FilterType(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireReportOverview_FilterServices
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterServices' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterServices()
		{
			return FilterServices(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterServices' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterServices(int start, int pageLength)
		{
			return FilterServices(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterServices' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet FilterServices(TransactionManager transactionManager)
		{
			return FilterServices(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportOverview_FilterServices' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet FilterServices(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverview&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportOverview&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportOverview&gt; Fill(DataSet dataSet, VList<QuestionnaireReportOverview> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportOverview>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverview&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportOverview>"/></returns>
		protected static VList&lt;QuestionnaireReportOverview&gt; Fill(DataTable dataTable, VList<QuestionnaireReportOverview> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportOverview c = new QuestionnaireReportOverview();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.CompanyAbn = (Convert.IsDBNull(row["CompanyAbn"]))?string.Empty:(System.String)row["CompanyAbn"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.InitialRiskAssessment = (Convert.IsDBNull(row["InitialRiskAssessment"]))?string.Empty:(System.String)row["InitialRiskAssessment"];
					c.MainAssessmentRiskRating = (Convert.IsDBNull(row["MainAssessmentRiskRating"]))?string.Empty:(System.String)row["MainAssessmentRiskRating"];
					c.FinalRiskRating = (Convert.IsDBNull(row["FinalRiskRating"]))?string.Empty:(System.String)row["FinalRiskRating"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.Type = (Convert.IsDBNull(row["Type"]))?string.Empty:(System.String)row["Type"];
					c.DescriptionOfWork = (Convert.IsDBNull(row["DescriptionOfWork"]))?string.Empty:(System.String)row["DescriptionOfWork"];
					c.LevelOfSupervision = (Convert.IsDBNull(row["LevelOfSupervision"]))?string.Empty:(System.String)row["LevelOfSupervision"];
					c.PrimaryContractor = (Convert.IsDBNull(row["PrimaryContractor"]))?string.Empty:(System.String)row["PrimaryContractor"];
					c.ProcurementContactUserId = (Convert.IsDBNull(row["ProcurementContactUserId"]))?(int)0:(System.Int32?)row["ProcurementContactUserId"];
					c.ContractManagerUserId = (Convert.IsDBNull(row["ContractManagerUserId"]))?(int)0:(System.Int32?)row["ContractManagerUserId"];
					c.ProcurementContactUser = (Convert.IsDBNull(row["ProcurementContactUser"]))?string.Empty:(System.String)row["ProcurementContactUser"];
					c.ContractManagerUser = (Convert.IsDBNull(row["ContractManagerUser"]))?string.Empty:(System.String)row["ContractManagerUser"];
					c.TypeOfService = (Convert.IsDBNull(row["TypeOfService"]))?(int)0:(System.Int32?)row["TypeOfService"];
					c.MainAssessmentValidTo = (Convert.IsDBNull(row["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentValidTo"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedByUser = (Convert.IsDBNull(row["ApprovedByUser"]))?string.Empty:(System.String)row["ApprovedByUser"];
					c.IsMainRequired = (Convert.IsDBNull(row["IsMainRequired"]))?false:(System.Boolean)row["IsMainRequired"];
					c.IsVerificationRequired = (Convert.IsDBNull(row["IsVerificationRequired"]))?false:(System.Boolean)row["IsVerificationRequired"];
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.NumberOfPeople = (Convert.IsDBNull(row["NumberOfPeople"]))?string.Empty:(System.String)row["NumberOfPeople"];
					c.LastSpqDate = (Convert.IsDBNull(row["LastSPQDate"]))?DateTime.MinValue:(System.DateTime?)row["LastSPQDate"];
					c.InitialSpqDate = (Convert.IsDBNull(row["InitialSPQDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialSPQDate"];
					c.Active = (Convert.IsDBNull(row["Active"]))?false:(System.Boolean?)row["Active"];
					c.MainScorePoverall = (Convert.IsDBNull(row["MainScorePOverall"]))?(int)0:(System.Int32?)row["MainScorePOverall"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.CreatedByUser = (Convert.IsDBNull(row["CreatedByUser"]))?string.Empty:(System.String)row["CreatedByUser"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedByUser = (Convert.IsDBNull(row["ModifiedByUser"]))?string.Empty:(System.String)row["ModifiedByUser"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.SubContractor = (Convert.IsDBNull(row["SubContractor"]))?false:(System.Boolean?)row["SubContractor"];
					c.Validity = (Convert.IsDBNull(row["Validity"]))?string.Empty:(System.String)row["Validity"];
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.ProcessNo = (Convert.IsDBNull(row["ProcessNo"]))?(int)0:(System.Int32?)row["ProcessNo"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.ActionName = (Convert.IsDBNull(row["ActionName"]))?string.Empty:(System.String)row["ActionName"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(row["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithSinceDaysCount"];
					c.DaysTillExpiry = (Convert.IsDBNull(row["DaysTillExpiry"]))?(int)0:(System.Int32?)row["DaysTillExpiry"];
					c.TrafficLight = (Convert.IsDBNull(row["TrafficLight"]))?string.Empty:(System.String)row["TrafficLight"];
					c.CompanyId1 = (Convert.IsDBNull(row["CompanyId_1"]))?(int)0:(System.Int32)row["CompanyId_1"];
					c.Kwi = (Convert.IsDBNull(row["KWI"]))?string.Empty:(System.String)row["KWI"];
					c.KwiSpaName = (Convert.IsDBNull(row["KWI_Spa_Name"]))?string.Empty:(System.String)row["KWI_Spa_Name"];
					c.KwiSpa = (Convert.IsDBNull(row["KWI_Spa"]))?(int)0:(System.Int32?)row["KWI_Spa"];
					c.KwiSponsorName = (Convert.IsDBNull(row["KWI_Sponsor_Name"]))?string.Empty:(System.String)row["KWI_Sponsor_Name"];
					c.KwiSponsor = (Convert.IsDBNull(row["KWI_Sponsor"]))?(int)0:(System.Int32?)row["KWI_Sponsor"];
					c.KwiCompanySiteCategory = (Convert.IsDBNull(row["KWI_CompanySiteCategory"]))?string.Empty:(System.String)row["KWI_CompanySiteCategory"];
					c.Pin = (Convert.IsDBNull(row["PIN"]))?string.Empty:(System.String)row["PIN"];
					c.PinSpaName = (Convert.IsDBNull(row["PIN_Spa_Name"]))?string.Empty:(System.String)row["PIN_Spa_Name"];
					c.PinSpa = (Convert.IsDBNull(row["PIN_Spa"]))?(int)0:(System.Int32?)row["PIN_Spa"];
					c.PinSponsorName = (Convert.IsDBNull(row["PIN_Sponsor_Name"]))?string.Empty:(System.String)row["PIN_Sponsor_Name"];
					c.PinSponsor = (Convert.IsDBNull(row["PIN_Sponsor"]))?(int)0:(System.Int32?)row["PIN_Sponsor"];
					c.PinCompanySiteCategory = (Convert.IsDBNull(row["PIN_CompanySiteCategory"]))?string.Empty:(System.String)row["PIN_CompanySiteCategory"];
					c.Wgp = (Convert.IsDBNull(row["WGP"]))?string.Empty:(System.String)row["WGP"];
					c.WgpSpaName = (Convert.IsDBNull(row["WGP_Spa_Name"]))?string.Empty:(System.String)row["WGP_Spa_Name"];
					c.WgpSpa = (Convert.IsDBNull(row["WGP_Spa"]))?(int)0:(System.Int32?)row["WGP_Spa"];
					c.WgpSponsorName = (Convert.IsDBNull(row["WGP_Sponsor_Name"]))?string.Empty:(System.String)row["WGP_Sponsor_Name"];
					c.WgpSponsor = (Convert.IsDBNull(row["WGP_Sponsor"]))?(int)0:(System.Int32?)row["WGP_Sponsor"];
					c.WgpCompanySiteCategory = (Convert.IsDBNull(row["WGP_CompanySiteCategory"]))?string.Empty:(System.String)row["WGP_CompanySiteCategory"];
					c.Hun = (Convert.IsDBNull(row["HUN"]))?string.Empty:(System.String)row["HUN"];
					c.HunSpaName = (Convert.IsDBNull(row["HUN_Spa_Name"]))?string.Empty:(System.String)row["HUN_Spa_Name"];
					c.HunSpa = (Convert.IsDBNull(row["HUN_Spa"]))?(int)0:(System.Int32?)row["HUN_Spa"];
					c.HunSponsorName = (Convert.IsDBNull(row["HUN_Sponsor_Name"]))?string.Empty:(System.String)row["HUN_Sponsor_Name"];
					c.HunSponsor = (Convert.IsDBNull(row["HUN_Sponsor"]))?(int)0:(System.Int32?)row["HUN_Sponsor"];
					c.HunCompanySiteCategory = (Convert.IsDBNull(row["HUN_CompanySiteCategory"]))?string.Empty:(System.String)row["HUN_CompanySiteCategory"];
					c.Wdl = (Convert.IsDBNull(row["WDL"]))?string.Empty:(System.String)row["WDL"];
					c.WdlSpaName = (Convert.IsDBNull(row["WDL_Spa_Name"]))?string.Empty:(System.String)row["WDL_Spa_Name"];
					c.WdlSpa = (Convert.IsDBNull(row["WDL_Spa"]))?(int)0:(System.Int32?)row["WDL_Spa"];
					c.WdlSponsorName = (Convert.IsDBNull(row["WDL_Sponsor_Name"]))?string.Empty:(System.String)row["WDL_Sponsor_Name"];
					c.WdlSponsor = (Convert.IsDBNull(row["WDL_Sponsor"]))?(int)0:(System.Int32?)row["WDL_Sponsor"];
					c.WdlCompanySiteCategory = (Convert.IsDBNull(row["WDL_CompanySiteCategory"]))?string.Empty:(System.String)row["WDL_CompanySiteCategory"];
					c.Bun = (Convert.IsDBNull(row["BUN"]))?string.Empty:(System.String)row["BUN"];
					c.BunSpaName = (Convert.IsDBNull(row["BUN_Spa_Name"]))?string.Empty:(System.String)row["BUN_Spa_Name"];
					c.BunSpa = (Convert.IsDBNull(row["BUN_Spa"]))?(int)0:(System.Int32?)row["BUN_Spa"];
					c.BunSponsorName = (Convert.IsDBNull(row["BUN_Sponsor_Name"]))?string.Empty:(System.String)row["BUN_Sponsor_Name"];
					c.BunSponsor = (Convert.IsDBNull(row["BUN_Sponsor"]))?(int)0:(System.Int32?)row["BUN_Sponsor"];
					c.BunCompanySiteCategory = (Convert.IsDBNull(row["BUN_CompanySiteCategory"]))?string.Empty:(System.String)row["BUN_CompanySiteCategory"];
					c.Fml = (Convert.IsDBNull(row["FML"]))?string.Empty:(System.String)row["FML"];
					c.FmlSpaName = (Convert.IsDBNull(row["FML_Spa_Name"]))?string.Empty:(System.String)row["FML_Spa_Name"];
					c.FmlSpa = (Convert.IsDBNull(row["FML_Spa"]))?(int)0:(System.Int32?)row["FML_Spa"];
					c.FmlSponsorName = (Convert.IsDBNull(row["FML_Sponsor_Name"]))?string.Empty:(System.String)row["FML_Sponsor_Name"];
					c.FmlSponsor = (Convert.IsDBNull(row["FML_Sponsor"]))?(int)0:(System.Int32?)row["FML_Sponsor"];
					c.FmlCompanySiteCategory = (Convert.IsDBNull(row["FML_CompanySiteCategory"]))?string.Empty:(System.String)row["FML_CompanySiteCategory"];
					c.Bgn = (Convert.IsDBNull(row["BGN"]))?string.Empty:(System.String)row["BGN"];
					c.BgnSpaName = (Convert.IsDBNull(row["BGN_Spa_Name"]))?string.Empty:(System.String)row["BGN_Spa_Name"];
					c.BgnSpa = (Convert.IsDBNull(row["BGN_Spa"]))?(int)0:(System.Int32?)row["BGN_Spa"];
					c.BgnSponsorName = (Convert.IsDBNull(row["BGN_Sponsor_Name"]))?string.Empty:(System.String)row["BGN_Sponsor_Name"];
					c.BgnSponsor = (Convert.IsDBNull(row["BGN_Sponsor"]))?(int)0:(System.Int32?)row["BGN_Sponsor"];
					c.BgnCompanySiteCategory = (Convert.IsDBNull(row["BGN_CompanySiteCategory"]))?string.Empty:(System.String)row["BGN_CompanySiteCategory"];
					c.Ce = (Convert.IsDBNull(row["CE"]))?string.Empty:(System.String)row["CE"];
					c.CeSpaName = (Convert.IsDBNull(row["CE_Spa_Name"]))?string.Empty:(System.String)row["CE_Spa_Name"];
					c.CeSpa = (Convert.IsDBNull(row["CE_Spa"]))?(int)0:(System.Int32?)row["CE_Spa"];
					c.CeSponsorName = (Convert.IsDBNull(row["CE_Sponsor_Name"]))?string.Empty:(System.String)row["CE_Sponsor_Name"];
					c.CeSponsor = (Convert.IsDBNull(row["CE_Sponsor"]))?(int)0:(System.Int32?)row["CE_Sponsor"];
					c.CeCompanySiteCategory = (Convert.IsDBNull(row["CE_CompanySiteCategory"]))?string.Empty:(System.String)row["CE_CompanySiteCategory"];
					c.Ang = (Convert.IsDBNull(row["ANG"]))?string.Empty:(System.String)row["ANG"];
					c.AngSpaName = (Convert.IsDBNull(row["ANG_Spa_Name"]))?string.Empty:(System.String)row["ANG_Spa_Name"];
					c.AngSpa = (Convert.IsDBNull(row["ANG_Spa"]))?(int)0:(System.Int32?)row["ANG_Spa"];
					c.AngSponsorName = (Convert.IsDBNull(row["ANG_Sponsor_Name"]))?string.Empty:(System.String)row["ANG_Sponsor_Name"];
					c.AngSponsor = (Convert.IsDBNull(row["ANG_Sponsor"]))?(int)0:(System.Int32?)row["ANG_Sponsor"];
					c.AngCompanySiteCategory = (Convert.IsDBNull(row["ANG_CompanySiteCategory"]))?string.Empty:(System.String)row["ANG_CompanySiteCategory"];
					c.Ptl = (Convert.IsDBNull(row["PTL"]))?string.Empty:(System.String)row["PTL"];
					c.PtlSpaName = (Convert.IsDBNull(row["PTL_Spa_Name"]))?string.Empty:(System.String)row["PTL_Spa_Name"];
					c.PtlSpa = (Convert.IsDBNull(row["PTL_Spa"]))?(int)0:(System.Int32?)row["PTL_Spa"];
					c.PtlSponsorName = (Convert.IsDBNull(row["PTL_Sponsor_Name"]))?string.Empty:(System.String)row["PTL_Sponsor_Name"];
					c.PtlSponsor = (Convert.IsDBNull(row["PTL_Sponsor"]))?(int)0:(System.Int32?)row["PTL_Sponsor"];
					c.PtlCompanySiteCategory = (Convert.IsDBNull(row["PTL_CompanySiteCategory"]))?string.Empty:(System.String)row["PTL_CompanySiteCategory"];
					c.Pth = (Convert.IsDBNull(row["PTH"]))?string.Empty:(System.String)row["PTH"];
					c.PthSpaName = (Convert.IsDBNull(row["PTH_Spa_Name"]))?string.Empty:(System.String)row["PTH_Spa_Name"];
					c.PthSpa = (Convert.IsDBNull(row["PTH_Spa"]))?(int)0:(System.Int32?)row["PTH_Spa"];
					c.PthSponsorName = (Convert.IsDBNull(row["PTH_Sponsor_Name"]))?string.Empty:(System.String)row["PTH_Sponsor_Name"];
					c.PthSponsor = (Convert.IsDBNull(row["PTH_Sponsor"]))?(int)0:(System.Int32?)row["PTH_Sponsor"];
					c.PthCompanySiteCategory = (Convert.IsDBNull(row["PTH_CompanySiteCategory"]))?string.Empty:(System.String)row["PTH_CompanySiteCategory"];
					c.Pel = (Convert.IsDBNull(row["PEL"]))?string.Empty:(System.String)row["PEL"];
					c.PelSpaName = (Convert.IsDBNull(row["PEL_Spa_Name"]))?string.Empty:(System.String)row["PEL_Spa_Name"];
					c.PelSpa = (Convert.IsDBNull(row["PEL_Spa"]))?(int)0:(System.Int32?)row["PEL_Spa"];
					c.PelSponsorName = (Convert.IsDBNull(row["PEL_Sponsor_Name"]))?string.Empty:(System.String)row["PEL_Sponsor_Name"];
					c.PelSponsor = (Convert.IsDBNull(row["PEL_Sponsor"]))?(int)0:(System.Int32?)row["PEL_Sponsor"];
					c.PelCompanySiteCategory = (Convert.IsDBNull(row["PEL_CompanySiteCategory"]))?string.Empty:(System.String)row["PEL_CompanySiteCategory"];
					c.Arp = (Convert.IsDBNull(row["ARP"]))?string.Empty:(System.String)row["ARP"];
					c.ArpSpaName = (Convert.IsDBNull(row["ARP_Spa_Name"]))?string.Empty:(System.String)row["ARP_Spa_Name"];
					c.ArpSpa = (Convert.IsDBNull(row["ARP_Spa"]))?(int)0:(System.Int32?)row["ARP_Spa"];
					c.ArpSponsorName = (Convert.IsDBNull(row["ARP_Sponsor_Name"]))?string.Empty:(System.String)row["ARP_Sponsor_Name"];
					c.ArpSponsor = (Convert.IsDBNull(row["ARP_Sponsor"]))?(int)0:(System.Int32?)row["ARP_Sponsor"];
					c.ArpCompanySiteCategory = (Convert.IsDBNull(row["ARP_CompanySiteCategory"]))?string.Empty:(System.String)row["ARP_CompanySiteCategory"];
					c.Yen = (Convert.IsDBNull(row["YEN"]))?string.Empty:(System.String)row["YEN"];
					c.YenSpaName = (Convert.IsDBNull(row["YEN_Spa_Name"]))?string.Empty:(System.String)row["YEN_Spa_Name"];
					c.YenSpa = (Convert.IsDBNull(row["YEN_Spa"]))?(int)0:(System.Int32?)row["YEN_Spa"];
					c.YenSponsorName = (Convert.IsDBNull(row["YEN_Sponsor_Name"]))?string.Empty:(System.String)row["YEN_Sponsor_Name"];
					c.YenSponsor = (Convert.IsDBNull(row["YEN_Sponsor"]))?(int)0:(System.Int32?)row["YEN_Sponsor"];
					c.YenCompanySiteCategory = (Convert.IsDBNull(row["YEN_CompanySiteCategory"]))?string.Empty:(System.String)row["YEN_CompanySiteCategory"];
					c.CompanyId2 = (Convert.IsDBNull(row["CompanyId_2"]))?(int)0:(System.Int32)row["CompanyId_2"];
					c.KwiArpName = (Convert.IsDBNull(row["KWI_ARP_Name"]))?string.Empty:(System.String)row["KWI_ARP_Name"];
					c.KwiCrpName = (Convert.IsDBNull(row["KWI_CRP_Name"]))?string.Empty:(System.String)row["KWI_CRP_Name"];
					c.PinArpName = (Convert.IsDBNull(row["PIN_ARP_Name"]))?string.Empty:(System.String)row["PIN_ARP_Name"];
					c.PinCrpName = (Convert.IsDBNull(row["PIN_CRP_Name"]))?string.Empty:(System.String)row["PIN_CRP_Name"];
					c.WgpArpName = (Convert.IsDBNull(row["WGP_ARP_Name"]))?string.Empty:(System.String)row["WGP_ARP_Name"];
					c.WgpCrpName = (Convert.IsDBNull(row["WGP_CRP_Name"]))?string.Empty:(System.String)row["WGP_CRP_Name"];
					c.HunArpName = (Convert.IsDBNull(row["HUN_ARP_Name"]))?string.Empty:(System.String)row["HUN_ARP_Name"];
					c.HunCrpName = (Convert.IsDBNull(row["HUN_CRP_Name"]))?string.Empty:(System.String)row["HUN_CRP_Name"];
					c.WdlArpName = (Convert.IsDBNull(row["WDL_ARP_Name"]))?string.Empty:(System.String)row["WDL_ARP_Name"];
					c.WdlCrpName = (Convert.IsDBNull(row["WDL_CRP_Name"]))?string.Empty:(System.String)row["WDL_CRP_Name"];
					c.BunArpName = (Convert.IsDBNull(row["BUN_ARP_Name"]))?string.Empty:(System.String)row["BUN_ARP_Name"];
					c.BunCrpName = (Convert.IsDBNull(row["BUN_CRP_Name"]))?string.Empty:(System.String)row["BUN_CRP_Name"];
					c.FmlArpName = (Convert.IsDBNull(row["FML_ARP_Name"]))?string.Empty:(System.String)row["FML_ARP_Name"];
					c.FmlCrpName = (Convert.IsDBNull(row["FML_CRP_Name"]))?string.Empty:(System.String)row["FML_CRP_Name"];
					c.BgnArpName = (Convert.IsDBNull(row["BGN_ARP_Name"]))?string.Empty:(System.String)row["BGN_ARP_Name"];
					c.BgnCrpName = (Convert.IsDBNull(row["BGN_CRP_Name"]))?string.Empty:(System.String)row["BGN_CRP_Name"];
					c.CeArpName = (Convert.IsDBNull(row["CE_ARP_Name"]))?string.Empty:(System.String)row["CE_ARP_Name"];
					c.CeCrpName = (Convert.IsDBNull(row["CE_CRP_Name"]))?string.Empty:(System.String)row["CE_CRP_Name"];
					c.AngArpName = (Convert.IsDBNull(row["ANG_ARP_Name"]))?string.Empty:(System.String)row["ANG_ARP_Name"];
					c.AngCrpName = (Convert.IsDBNull(row["ANG_CRP_Name"]))?string.Empty:(System.String)row["ANG_CRP_Name"];
					c.PtlArpName = (Convert.IsDBNull(row["PTL_ARP_Name"]))?string.Empty:(System.String)row["PTL_ARP_Name"];
					c.PtlCrpName = (Convert.IsDBNull(row["PTL_CRP_Name"]))?string.Empty:(System.String)row["PTL_CRP_Name"];
					c.PthArpName = (Convert.IsDBNull(row["PTH_ARP_Name"]))?string.Empty:(System.String)row["PTH_ARP_Name"];
					c.PthCrpName = (Convert.IsDBNull(row["PTH_CRP_Name"]))?string.Empty:(System.String)row["PTH_CRP_Name"];
					c.PelArpName = (Convert.IsDBNull(row["PEL_ARP_Name"]))?string.Empty:(System.String)row["PEL_ARP_Name"];
					c.PelCrpName = (Convert.IsDBNull(row["PEL_CRP_Name"]))?string.Empty:(System.String)row["PEL_CRP_Name"];
					c.ArpArpName = (Convert.IsDBNull(row["ARP_ARP_Name"]))?string.Empty:(System.String)row["ARP_ARP_Name"];
					c.ArpCrpName = (Convert.IsDBNull(row["ARP_CRP_Name"]))?string.Empty:(System.String)row["ARP_CRP_Name"];
					c.YenArpName = (Convert.IsDBNull(row["YEN_ARP_Name"]))?string.Empty:(System.String)row["YEN_ARP_Name"];
					c.YenCrpName = (Convert.IsDBNull(row["YEN_CRP_Name"]))?string.Empty:(System.String)row["YEN_CRP_Name"];
					c.RequestingCompanyId = (Convert.IsDBNull(row["RequestingCompanyId"]))?(int)0:(System.Int32?)row["RequestingCompanyId"];
					c.RequestingCompanyName = (Convert.IsDBNull(row["RequestingCompanyName"]))?string.Empty:(System.String)row["RequestingCompanyName"];
					c.ReasonContractor = (Convert.IsDBNull(row["ReasonContractor"]))?string.Empty:(System.String)row["ReasonContractor"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportOverview&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportOverview&gt;"/></returns>
		protected VList<QuestionnaireReportOverview> Fill(IDataReader reader, VList<QuestionnaireReportOverview> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportOverview entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportOverview>("QuestionnaireReportOverview",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportOverview();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportOverviewColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.CompanyAbn = (System.String)reader[((int)QuestionnaireReportOverviewColumn.CompanyAbn)];
					//entity.CompanyAbn = (Convert.IsDBNull(reader["CompanyAbn"]))?string.Empty:(System.String)reader["CompanyAbn"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportOverviewColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.InitialRiskAssessment)];
					//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
					entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.MainAssessmentRiskRating)];
					//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
					entity.FinalRiskRating = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FinalRiskRating)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FinalRiskRating)];
					//entity.FinalRiskRating = (Convert.IsDBNull(reader["FinalRiskRating"]))?string.Empty:(System.String)reader["FinalRiskRating"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.Type = (System.String)reader[((int)QuestionnaireReportOverviewColumn.Type)];
					//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
					entity.DescriptionOfWork = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.DescriptionOfWork)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.DescriptionOfWork)];
					//entity.DescriptionOfWork = (Convert.IsDBNull(reader["DescriptionOfWork"]))?string.Empty:(System.String)reader["DescriptionOfWork"];
					entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.LevelOfSupervision)];
					//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
					entity.PrimaryContractor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PrimaryContractor)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PrimaryContractor)];
					//entity.PrimaryContractor = (Convert.IsDBNull(reader["PrimaryContractor"]))?string.Empty:(System.String)reader["PrimaryContractor"];
					entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ProcurementContactUserId)];
					//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
					entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ContractManagerUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ContractManagerUserId)];
					//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?(int)0:(System.Int32?)reader["ContractManagerUserId"];
					entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ProcurementContactUser)];
					//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
					entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ContractManagerUser)];
					//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
					entity.TypeOfService = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.TypeOfService)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.TypeOfService)];
					//entity.TypeOfService = (Convert.IsDBNull(reader["TypeOfService"]))?(int)0:(System.Int32?)reader["TypeOfService"];
					entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.MainAssessmentValidTo)];
					//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedByUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ApprovedByUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ApprovedByUser)];
					//entity.ApprovedByUser = (Convert.IsDBNull(reader["ApprovedByUser"]))?string.Empty:(System.String)reader["ApprovedByUser"];
					entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireReportOverviewColumn.IsMainRequired)];
					//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
					entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireReportOverviewColumn.IsVerificationRequired)];
					//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.NumberOfPeople = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.NumberOfPeople)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.NumberOfPeople)];
					//entity.NumberOfPeople = (Convert.IsDBNull(reader["NumberOfPeople"]))?string.Empty:(System.String)reader["NumberOfPeople"];
					entity.LastSpqDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.LastSpqDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.LastSpqDate)];
					//entity.LastSpqDate = (Convert.IsDBNull(reader["LastSPQDate"]))?DateTime.MinValue:(System.DateTime?)reader["LastSPQDate"];
					entity.InitialSpqDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.InitialSpqDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.InitialSpqDate)];
					//entity.InitialSpqDate = (Convert.IsDBNull(reader["InitialSPQDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialSPQDate"];
					entity.Active = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Active)))?null:(System.Boolean?)reader[((int)QuestionnaireReportOverviewColumn.Active)];
					//entity.Active = (Convert.IsDBNull(reader["Active"]))?false:(System.Boolean?)reader["Active"];
					entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.MainScorePoverall)];
					//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CreatedByUser)];
					//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportOverviewColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ModifiedByUser)];
					//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
					entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportOverviewColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireReportOverviewColumn.SubContractor)];
					//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
					entity.Validity = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Validity)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Validity)];
					//entity.Validity = (Convert.IsDBNull(reader["Validity"]))?string.Empty:(System.String)reader["Validity"];
					entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ProcessNo)];
					//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
					entity.UserName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.ActionName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ActionName)];
					//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSinceDaysCount)];
					//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
					entity.DaysTillExpiry = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.DaysTillExpiry)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.DaysTillExpiry)];
					//entity.DaysTillExpiry = (Convert.IsDBNull(reader["DaysTillExpiry"]))?(int)0:(System.Int32?)reader["DaysTillExpiry"];
					entity.TrafficLight = (System.String)reader[((int)QuestionnaireReportOverviewColumn.TrafficLight)];
					//entity.TrafficLight = (Convert.IsDBNull(reader["TrafficLight"]))?string.Empty:(System.String)reader["TrafficLight"];
					entity.CompanyId1 = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CompanyId1)];
					//entity.CompanyId1 = (Convert.IsDBNull(reader["CompanyId_1"]))?(int)0:(System.Int32)reader["CompanyId_1"];
					entity.Kwi = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Kwi)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Kwi)];
					//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
					entity.KwiSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiSpaName)];
					//entity.KwiSpaName = (Convert.IsDBNull(reader["KWI_Spa_Name"]))?string.Empty:(System.String)reader["KWI_Spa_Name"];
					entity.KwiSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.KwiSpa)];
					//entity.KwiSpa = (Convert.IsDBNull(reader["KWI_Spa"]))?(int)0:(System.Int32?)reader["KWI_Spa"];
					entity.KwiSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiSponsorName)];
					//entity.KwiSponsorName = (Convert.IsDBNull(reader["KWI_Sponsor_Name"]))?string.Empty:(System.String)reader["KWI_Sponsor_Name"];
					entity.KwiSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.KwiSponsor)];
					//entity.KwiSponsor = (Convert.IsDBNull(reader["KWI_Sponsor"]))?(int)0:(System.Int32?)reader["KWI_Sponsor"];
					entity.KwiCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiCompanySiteCategory)];
					//entity.KwiCompanySiteCategory = (Convert.IsDBNull(reader["KWI_CompanySiteCategory"]))?string.Empty:(System.String)reader["KWI_CompanySiteCategory"];
					entity.Pin = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Pin)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Pin)];
					//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
					entity.PinSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinSpaName)];
					//entity.PinSpaName = (Convert.IsDBNull(reader["PIN_Spa_Name"]))?string.Empty:(System.String)reader["PIN_Spa_Name"];
					entity.PinSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PinSpa)];
					//entity.PinSpa = (Convert.IsDBNull(reader["PIN_Spa"]))?(int)0:(System.Int32?)reader["PIN_Spa"];
					entity.PinSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinSponsorName)];
					//entity.PinSponsorName = (Convert.IsDBNull(reader["PIN_Sponsor_Name"]))?string.Empty:(System.String)reader["PIN_Sponsor_Name"];
					entity.PinSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PinSponsor)];
					//entity.PinSponsor = (Convert.IsDBNull(reader["PIN_Sponsor"]))?(int)0:(System.Int32?)reader["PIN_Sponsor"];
					entity.PinCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinCompanySiteCategory)];
					//entity.PinCompanySiteCategory = (Convert.IsDBNull(reader["PIN_CompanySiteCategory"]))?string.Empty:(System.String)reader["PIN_CompanySiteCategory"];
					entity.Wgp = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Wgp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Wgp)];
					//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
					entity.WgpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpSpaName)];
					//entity.WgpSpaName = (Convert.IsDBNull(reader["WGP_Spa_Name"]))?string.Empty:(System.String)reader["WGP_Spa_Name"];
					entity.WgpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WgpSpa)];
					//entity.WgpSpa = (Convert.IsDBNull(reader["WGP_Spa"]))?(int)0:(System.Int32?)reader["WGP_Spa"];
					entity.WgpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpSponsorName)];
					//entity.WgpSponsorName = (Convert.IsDBNull(reader["WGP_Sponsor_Name"]))?string.Empty:(System.String)reader["WGP_Sponsor_Name"];
					entity.WgpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WgpSponsor)];
					//entity.WgpSponsor = (Convert.IsDBNull(reader["WGP_Sponsor"]))?(int)0:(System.Int32?)reader["WGP_Sponsor"];
					entity.WgpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpCompanySiteCategory)];
					//entity.WgpCompanySiteCategory = (Convert.IsDBNull(reader["WGP_CompanySiteCategory"]))?string.Empty:(System.String)reader["WGP_CompanySiteCategory"];
					entity.Hun = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Hun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Hun)];
					//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
					entity.HunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunSpaName)];
					//entity.HunSpaName = (Convert.IsDBNull(reader["HUN_Spa_Name"]))?string.Empty:(System.String)reader["HUN_Spa_Name"];
					entity.HunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.HunSpa)];
					//entity.HunSpa = (Convert.IsDBNull(reader["HUN_Spa"]))?(int)0:(System.Int32?)reader["HUN_Spa"];
					entity.HunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunSponsorName)];
					//entity.HunSponsorName = (Convert.IsDBNull(reader["HUN_Sponsor_Name"]))?string.Empty:(System.String)reader["HUN_Sponsor_Name"];
					entity.HunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.HunSponsor)];
					//entity.HunSponsor = (Convert.IsDBNull(reader["HUN_Sponsor"]))?(int)0:(System.Int32?)reader["HUN_Sponsor"];
					entity.HunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunCompanySiteCategory)];
					//entity.HunCompanySiteCategory = (Convert.IsDBNull(reader["HUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["HUN_CompanySiteCategory"];
					entity.Wdl = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Wdl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Wdl)];
					//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
					entity.WdlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlSpaName)];
					//entity.WdlSpaName = (Convert.IsDBNull(reader["WDL_Spa_Name"]))?string.Empty:(System.String)reader["WDL_Spa_Name"];
					entity.WdlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WdlSpa)];
					//entity.WdlSpa = (Convert.IsDBNull(reader["WDL_Spa"]))?(int)0:(System.Int32?)reader["WDL_Spa"];
					entity.WdlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlSponsorName)];
					//entity.WdlSponsorName = (Convert.IsDBNull(reader["WDL_Sponsor_Name"]))?string.Empty:(System.String)reader["WDL_Sponsor_Name"];
					entity.WdlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WdlSponsor)];
					//entity.WdlSponsor = (Convert.IsDBNull(reader["WDL_Sponsor"]))?(int)0:(System.Int32?)reader["WDL_Sponsor"];
					entity.WdlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlCompanySiteCategory)];
					//entity.WdlCompanySiteCategory = (Convert.IsDBNull(reader["WDL_CompanySiteCategory"]))?string.Empty:(System.String)reader["WDL_CompanySiteCategory"];
					entity.Bun = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Bun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Bun)];
					//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
					entity.BunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunSpaName)];
					//entity.BunSpaName = (Convert.IsDBNull(reader["BUN_Spa_Name"]))?string.Empty:(System.String)reader["BUN_Spa_Name"];
					entity.BunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BunSpa)];
					//entity.BunSpa = (Convert.IsDBNull(reader["BUN_Spa"]))?(int)0:(System.Int32?)reader["BUN_Spa"];
					entity.BunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunSponsorName)];
					//entity.BunSponsorName = (Convert.IsDBNull(reader["BUN_Sponsor_Name"]))?string.Empty:(System.String)reader["BUN_Sponsor_Name"];
					entity.BunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BunSponsor)];
					//entity.BunSponsor = (Convert.IsDBNull(reader["BUN_Sponsor"]))?(int)0:(System.Int32?)reader["BUN_Sponsor"];
					entity.BunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunCompanySiteCategory)];
					//entity.BunCompanySiteCategory = (Convert.IsDBNull(reader["BUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BUN_CompanySiteCategory"];
					entity.Fml = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Fml)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Fml)];
					//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
					entity.FmlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlSpaName)];
					//entity.FmlSpaName = (Convert.IsDBNull(reader["FML_Spa_Name"]))?string.Empty:(System.String)reader["FML_Spa_Name"];
					entity.FmlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.FmlSpa)];
					//entity.FmlSpa = (Convert.IsDBNull(reader["FML_Spa"]))?(int)0:(System.Int32?)reader["FML_Spa"];
					entity.FmlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlSponsorName)];
					//entity.FmlSponsorName = (Convert.IsDBNull(reader["FML_Sponsor_Name"]))?string.Empty:(System.String)reader["FML_Sponsor_Name"];
					entity.FmlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.FmlSponsor)];
					//entity.FmlSponsor = (Convert.IsDBNull(reader["FML_Sponsor"]))?(int)0:(System.Int32?)reader["FML_Sponsor"];
					entity.FmlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlCompanySiteCategory)];
					//entity.FmlCompanySiteCategory = (Convert.IsDBNull(reader["FML_CompanySiteCategory"]))?string.Empty:(System.String)reader["FML_CompanySiteCategory"];
					entity.Bgn = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Bgn)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Bgn)];
					//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
					entity.BgnSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnSpaName)];
					//entity.BgnSpaName = (Convert.IsDBNull(reader["BGN_Spa_Name"]))?string.Empty:(System.String)reader["BGN_Spa_Name"];
					entity.BgnSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BgnSpa)];
					//entity.BgnSpa = (Convert.IsDBNull(reader["BGN_Spa"]))?(int)0:(System.Int32?)reader["BGN_Spa"];
					entity.BgnSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnSponsorName)];
					//entity.BgnSponsorName = (Convert.IsDBNull(reader["BGN_Sponsor_Name"]))?string.Empty:(System.String)reader["BGN_Sponsor_Name"];
					entity.BgnSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BgnSponsor)];
					//entity.BgnSponsor = (Convert.IsDBNull(reader["BGN_Sponsor"]))?(int)0:(System.Int32?)reader["BGN_Sponsor"];
					entity.BgnCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnCompanySiteCategory)];
					//entity.BgnCompanySiteCategory = (Convert.IsDBNull(reader["BGN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BGN_CompanySiteCategory"];
					entity.Ce = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Ce)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Ce)];
					//entity.Ce = (Convert.IsDBNull(reader["CE"]))?string.Empty:(System.String)reader["CE"];
					entity.CeSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeSpaName)];
					//entity.CeSpaName = (Convert.IsDBNull(reader["CE_Spa_Name"]))?string.Empty:(System.String)reader["CE_Spa_Name"];
					entity.CeSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.CeSpa)];
					//entity.CeSpa = (Convert.IsDBNull(reader["CE_Spa"]))?(int)0:(System.Int32?)reader["CE_Spa"];
					entity.CeSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeSponsorName)];
					//entity.CeSponsorName = (Convert.IsDBNull(reader["CE_Sponsor_Name"]))?string.Empty:(System.String)reader["CE_Sponsor_Name"];
					entity.CeSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.CeSponsor)];
					//entity.CeSponsor = (Convert.IsDBNull(reader["CE_Sponsor"]))?(int)0:(System.Int32?)reader["CE_Sponsor"];
					entity.CeCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeCompanySiteCategory)];
					//entity.CeCompanySiteCategory = (Convert.IsDBNull(reader["CE_CompanySiteCategory"]))?string.Empty:(System.String)reader["CE_CompanySiteCategory"];
					entity.Ang = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Ang)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Ang)];
					//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
					entity.AngSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngSpaName)];
					//entity.AngSpaName = (Convert.IsDBNull(reader["ANG_Spa_Name"]))?string.Empty:(System.String)reader["ANG_Spa_Name"];
					entity.AngSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.AngSpa)];
					//entity.AngSpa = (Convert.IsDBNull(reader["ANG_Spa"]))?(int)0:(System.Int32?)reader["ANG_Spa"];
					entity.AngSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngSponsorName)];
					//entity.AngSponsorName = (Convert.IsDBNull(reader["ANG_Sponsor_Name"]))?string.Empty:(System.String)reader["ANG_Sponsor_Name"];
					entity.AngSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.AngSponsor)];
					//entity.AngSponsor = (Convert.IsDBNull(reader["ANG_Sponsor"]))?(int)0:(System.Int32?)reader["ANG_Sponsor"];
					entity.AngCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngCompanySiteCategory)];
					//entity.AngCompanySiteCategory = (Convert.IsDBNull(reader["ANG_CompanySiteCategory"]))?string.Empty:(System.String)reader["ANG_CompanySiteCategory"];
					entity.Ptl = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Ptl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Ptl)];
					//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
					entity.PtlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlSpaName)];
					//entity.PtlSpaName = (Convert.IsDBNull(reader["PTL_Spa_Name"]))?string.Empty:(System.String)reader["PTL_Spa_Name"];
					entity.PtlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PtlSpa)];
					//entity.PtlSpa = (Convert.IsDBNull(reader["PTL_Spa"]))?(int)0:(System.Int32?)reader["PTL_Spa"];
					entity.PtlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlSponsorName)];
					//entity.PtlSponsorName = (Convert.IsDBNull(reader["PTL_Sponsor_Name"]))?string.Empty:(System.String)reader["PTL_Sponsor_Name"];
					entity.PtlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PtlSponsor)];
					//entity.PtlSponsor = (Convert.IsDBNull(reader["PTL_Sponsor"]))?(int)0:(System.Int32?)reader["PTL_Sponsor"];
					entity.PtlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlCompanySiteCategory)];
					//entity.PtlCompanySiteCategory = (Convert.IsDBNull(reader["PTL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTL_CompanySiteCategory"];
					entity.Pth = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Pth)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Pth)];
					//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
					entity.PthSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthSpaName)];
					//entity.PthSpaName = (Convert.IsDBNull(reader["PTH_Spa_Name"]))?string.Empty:(System.String)reader["PTH_Spa_Name"];
					entity.PthSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PthSpa)];
					//entity.PthSpa = (Convert.IsDBNull(reader["PTH_Spa"]))?(int)0:(System.Int32?)reader["PTH_Spa"];
					entity.PthSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthSponsorName)];
					//entity.PthSponsorName = (Convert.IsDBNull(reader["PTH_Sponsor_Name"]))?string.Empty:(System.String)reader["PTH_Sponsor_Name"];
					entity.PthSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PthSponsor)];
					//entity.PthSponsor = (Convert.IsDBNull(reader["PTH_Sponsor"]))?(int)0:(System.Int32?)reader["PTH_Sponsor"];
					entity.PthCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthCompanySiteCategory)];
					//entity.PthCompanySiteCategory = (Convert.IsDBNull(reader["PTH_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTH_CompanySiteCategory"];
					entity.Pel = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Pel)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Pel)];
					//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
					entity.PelSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelSpaName)];
					//entity.PelSpaName = (Convert.IsDBNull(reader["PEL_Spa_Name"]))?string.Empty:(System.String)reader["PEL_Spa_Name"];
					entity.PelSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PelSpa)];
					//entity.PelSpa = (Convert.IsDBNull(reader["PEL_Spa"]))?(int)0:(System.Int32?)reader["PEL_Spa"];
					entity.PelSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelSponsorName)];
					//entity.PelSponsorName = (Convert.IsDBNull(reader["PEL_Sponsor_Name"]))?string.Empty:(System.String)reader["PEL_Sponsor_Name"];
					entity.PelSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PelSponsor)];
					//entity.PelSponsor = (Convert.IsDBNull(reader["PEL_Sponsor"]))?(int)0:(System.Int32?)reader["PEL_Sponsor"];
					entity.PelCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelCompanySiteCategory)];
					//entity.PelCompanySiteCategory = (Convert.IsDBNull(reader["PEL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PEL_CompanySiteCategory"];
					entity.Arp = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Arp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Arp)];
					//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
					entity.ArpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpSpaName)];
					//entity.ArpSpaName = (Convert.IsDBNull(reader["ARP_Spa_Name"]))?string.Empty:(System.String)reader["ARP_Spa_Name"];
					entity.ArpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ArpSpa)];
					//entity.ArpSpa = (Convert.IsDBNull(reader["ARP_Spa"]))?(int)0:(System.Int32?)reader["ARP_Spa"];
					entity.ArpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpSponsorName)];
					//entity.ArpSponsorName = (Convert.IsDBNull(reader["ARP_Sponsor_Name"]))?string.Empty:(System.String)reader["ARP_Sponsor_Name"];
					entity.ArpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ArpSponsor)];
					//entity.ArpSponsor = (Convert.IsDBNull(reader["ARP_Sponsor"]))?(int)0:(System.Int32?)reader["ARP_Sponsor"];
					entity.ArpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpCompanySiteCategory)];
					//entity.ArpCompanySiteCategory = (Convert.IsDBNull(reader["ARP_CompanySiteCategory"]))?string.Empty:(System.String)reader["ARP_CompanySiteCategory"];
					entity.Yen = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Yen)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Yen)];
					//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
					entity.YenSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenSpaName)];
					//entity.YenSpaName = (Convert.IsDBNull(reader["YEN_Spa_Name"]))?string.Empty:(System.String)reader["YEN_Spa_Name"];
					entity.YenSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.YenSpa)];
					//entity.YenSpa = (Convert.IsDBNull(reader["YEN_Spa"]))?(int)0:(System.Int32?)reader["YEN_Spa"];
					entity.YenSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenSponsorName)];
					//entity.YenSponsorName = (Convert.IsDBNull(reader["YEN_Sponsor_Name"]))?string.Empty:(System.String)reader["YEN_Sponsor_Name"];
					entity.YenSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.YenSponsor)];
					//entity.YenSponsor = (Convert.IsDBNull(reader["YEN_Sponsor"]))?(int)0:(System.Int32?)reader["YEN_Sponsor"];
					entity.YenCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenCompanySiteCategory)];
					//entity.YenCompanySiteCategory = (Convert.IsDBNull(reader["YEN_CompanySiteCategory"]))?string.Empty:(System.String)reader["YEN_CompanySiteCategory"];
					entity.CompanyId2 = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CompanyId2)];
					//entity.CompanyId2 = (Convert.IsDBNull(reader["CompanyId_2"]))?(int)0:(System.Int32)reader["CompanyId_2"];
					entity.KwiArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiArpName)];
					//entity.KwiArpName = (Convert.IsDBNull(reader["KWI_ARP_Name"]))?string.Empty:(System.String)reader["KWI_ARP_Name"];
					entity.KwiCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiCrpName)];
					//entity.KwiCrpName = (Convert.IsDBNull(reader["KWI_CRP_Name"]))?string.Empty:(System.String)reader["KWI_CRP_Name"];
					entity.PinArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinArpName)];
					//entity.PinArpName = (Convert.IsDBNull(reader["PIN_ARP_Name"]))?string.Empty:(System.String)reader["PIN_ARP_Name"];
					entity.PinCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinCrpName)];
					//entity.PinCrpName = (Convert.IsDBNull(reader["PIN_CRP_Name"]))?string.Empty:(System.String)reader["PIN_CRP_Name"];
					entity.WgpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpArpName)];
					//entity.WgpArpName = (Convert.IsDBNull(reader["WGP_ARP_Name"]))?string.Empty:(System.String)reader["WGP_ARP_Name"];
					entity.WgpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpCrpName)];
					//entity.WgpCrpName = (Convert.IsDBNull(reader["WGP_CRP_Name"]))?string.Empty:(System.String)reader["WGP_CRP_Name"];
					entity.HunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunArpName)];
					//entity.HunArpName = (Convert.IsDBNull(reader["HUN_ARP_Name"]))?string.Empty:(System.String)reader["HUN_ARP_Name"];
					entity.HunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunCrpName)];
					//entity.HunCrpName = (Convert.IsDBNull(reader["HUN_CRP_Name"]))?string.Empty:(System.String)reader["HUN_CRP_Name"];
					entity.WdlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlArpName)];
					//entity.WdlArpName = (Convert.IsDBNull(reader["WDL_ARP_Name"]))?string.Empty:(System.String)reader["WDL_ARP_Name"];
					entity.WdlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlCrpName)];
					//entity.WdlCrpName = (Convert.IsDBNull(reader["WDL_CRP_Name"]))?string.Empty:(System.String)reader["WDL_CRP_Name"];
					entity.BunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunArpName)];
					//entity.BunArpName = (Convert.IsDBNull(reader["BUN_ARP_Name"]))?string.Empty:(System.String)reader["BUN_ARP_Name"];
					entity.BunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunCrpName)];
					//entity.BunCrpName = (Convert.IsDBNull(reader["BUN_CRP_Name"]))?string.Empty:(System.String)reader["BUN_CRP_Name"];
					entity.FmlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlArpName)];
					//entity.FmlArpName = (Convert.IsDBNull(reader["FML_ARP_Name"]))?string.Empty:(System.String)reader["FML_ARP_Name"];
					entity.FmlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlCrpName)];
					//entity.FmlCrpName = (Convert.IsDBNull(reader["FML_CRP_Name"]))?string.Empty:(System.String)reader["FML_CRP_Name"];
					entity.BgnArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnArpName)];
					//entity.BgnArpName = (Convert.IsDBNull(reader["BGN_ARP_Name"]))?string.Empty:(System.String)reader["BGN_ARP_Name"];
					entity.BgnCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnCrpName)];
					//entity.BgnCrpName = (Convert.IsDBNull(reader["BGN_CRP_Name"]))?string.Empty:(System.String)reader["BGN_CRP_Name"];
					entity.CeArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeArpName)];
					//entity.CeArpName = (Convert.IsDBNull(reader["CE_ARP_Name"]))?string.Empty:(System.String)reader["CE_ARP_Name"];
					entity.CeCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeCrpName)];
					//entity.CeCrpName = (Convert.IsDBNull(reader["CE_CRP_Name"]))?string.Empty:(System.String)reader["CE_CRP_Name"];
					entity.AngArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngArpName)];
					//entity.AngArpName = (Convert.IsDBNull(reader["ANG_ARP_Name"]))?string.Empty:(System.String)reader["ANG_ARP_Name"];
					entity.AngCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngCrpName)];
					//entity.AngCrpName = (Convert.IsDBNull(reader["ANG_CRP_Name"]))?string.Empty:(System.String)reader["ANG_CRP_Name"];
					entity.PtlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlArpName)];
					//entity.PtlArpName = (Convert.IsDBNull(reader["PTL_ARP_Name"]))?string.Empty:(System.String)reader["PTL_ARP_Name"];
					entity.PtlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlCrpName)];
					//entity.PtlCrpName = (Convert.IsDBNull(reader["PTL_CRP_Name"]))?string.Empty:(System.String)reader["PTL_CRP_Name"];
					entity.PthArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthArpName)];
					//entity.PthArpName = (Convert.IsDBNull(reader["PTH_ARP_Name"]))?string.Empty:(System.String)reader["PTH_ARP_Name"];
					entity.PthCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthCrpName)];
					//entity.PthCrpName = (Convert.IsDBNull(reader["PTH_CRP_Name"]))?string.Empty:(System.String)reader["PTH_CRP_Name"];
					entity.PelArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelArpName)];
					//entity.PelArpName = (Convert.IsDBNull(reader["PEL_ARP_Name"]))?string.Empty:(System.String)reader["PEL_ARP_Name"];
					entity.PelCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelCrpName)];
					//entity.PelCrpName = (Convert.IsDBNull(reader["PEL_CRP_Name"]))?string.Empty:(System.String)reader["PEL_CRP_Name"];
					entity.ArpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpArpName)];
					//entity.ArpArpName = (Convert.IsDBNull(reader["ARP_ARP_Name"]))?string.Empty:(System.String)reader["ARP_ARP_Name"];
					entity.ArpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpCrpName)];
					//entity.ArpCrpName = (Convert.IsDBNull(reader["ARP_CRP_Name"]))?string.Empty:(System.String)reader["ARP_CRP_Name"];
					entity.YenArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenArpName)];
					//entity.YenArpName = (Convert.IsDBNull(reader["YEN_ARP_Name"]))?string.Empty:(System.String)reader["YEN_ARP_Name"];
					entity.YenCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenCrpName)];
					//entity.YenCrpName = (Convert.IsDBNull(reader["YEN_CRP_Name"]))?string.Empty:(System.String)reader["YEN_CRP_Name"];
					entity.RequestingCompanyId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.RequestingCompanyId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.RequestingCompanyId)];
					//entity.RequestingCompanyId = (Convert.IsDBNull(reader["RequestingCompanyId"]))?(int)0:(System.Int32?)reader["RequestingCompanyId"];
					entity.RequestingCompanyName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.RequestingCompanyName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.RequestingCompanyName)];
					//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
					entity.ReasonContractor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ReasonContractor)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ReasonContractor)];
					//entity.ReasonContractor = (Convert.IsDBNull(reader["ReasonContractor"]))?string.Empty:(System.String)reader["ReasonContractor"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverview"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverview"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportOverview entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportOverviewColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.CompanyAbn = (System.String)reader[((int)QuestionnaireReportOverviewColumn.CompanyAbn)];
			//entity.CompanyAbn = (Convert.IsDBNull(reader["CompanyAbn"]))?string.Empty:(System.String)reader["CompanyAbn"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportOverviewColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.InitialRiskAssessment)];
			//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
			entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.MainAssessmentRiskRating)];
			//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
			entity.FinalRiskRating = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FinalRiskRating)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FinalRiskRating)];
			//entity.FinalRiskRating = (Convert.IsDBNull(reader["FinalRiskRating"]))?string.Empty:(System.String)reader["FinalRiskRating"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.Type = (System.String)reader[((int)QuestionnaireReportOverviewColumn.Type)];
			//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
			entity.DescriptionOfWork = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.DescriptionOfWork)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.DescriptionOfWork)];
			//entity.DescriptionOfWork = (Convert.IsDBNull(reader["DescriptionOfWork"]))?string.Empty:(System.String)reader["DescriptionOfWork"];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.LevelOfSupervision)];
			//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
			entity.PrimaryContractor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PrimaryContractor)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PrimaryContractor)];
			//entity.PrimaryContractor = (Convert.IsDBNull(reader["PrimaryContractor"]))?string.Empty:(System.String)reader["PrimaryContractor"];
			entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ProcurementContactUserId)];
			//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
			entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ContractManagerUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ContractManagerUserId)];
			//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?(int)0:(System.Int32?)reader["ContractManagerUserId"];
			entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ProcurementContactUser)];
			//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
			entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ContractManagerUser)];
			//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
			entity.TypeOfService = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.TypeOfService)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.TypeOfService)];
			//entity.TypeOfService = (Convert.IsDBNull(reader["TypeOfService"]))?(int)0:(System.Int32?)reader["TypeOfService"];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.MainAssessmentValidTo)];
			//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedByUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ApprovedByUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ApprovedByUser)];
			//entity.ApprovedByUser = (Convert.IsDBNull(reader["ApprovedByUser"]))?string.Empty:(System.String)reader["ApprovedByUser"];
			entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireReportOverviewColumn.IsMainRequired)];
			//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
			entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireReportOverviewColumn.IsVerificationRequired)];
			//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.NumberOfPeople = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.NumberOfPeople)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.NumberOfPeople)];
			//entity.NumberOfPeople = (Convert.IsDBNull(reader["NumberOfPeople"]))?string.Empty:(System.String)reader["NumberOfPeople"];
			entity.LastSpqDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.LastSpqDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.LastSpqDate)];
			//entity.LastSpqDate = (Convert.IsDBNull(reader["LastSPQDate"]))?DateTime.MinValue:(System.DateTime?)reader["LastSPQDate"];
			entity.InitialSpqDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.InitialSpqDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.InitialSpqDate)];
			//entity.InitialSpqDate = (Convert.IsDBNull(reader["InitialSPQDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialSPQDate"];
			entity.Active = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Active)))?null:(System.Boolean?)reader[((int)QuestionnaireReportOverviewColumn.Active)];
			//entity.Active = (Convert.IsDBNull(reader["Active"]))?false:(System.Boolean?)reader["Active"];
			entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.MainScorePoverall)];
			//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.EhsConsultantId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CreatedByUser)];
			//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportOverviewColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ModifiedByUser)];
			//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportOverviewColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireReportOverviewColumn.SubContractor)];
			//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
			entity.Validity = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Validity)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Validity)];
			//entity.Validity = (Convert.IsDBNull(reader["Validity"]))?string.Empty:(System.String)reader["Validity"];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ProcessNo)];
			//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
			entity.UserName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.ActionName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ActionName)];
			//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.QuestionnairePresentlyWithSinceDaysCount)];
			//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
			entity.DaysTillExpiry = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.DaysTillExpiry)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.DaysTillExpiry)];
			//entity.DaysTillExpiry = (Convert.IsDBNull(reader["DaysTillExpiry"]))?(int)0:(System.Int32?)reader["DaysTillExpiry"];
			entity.TrafficLight = (System.String)reader[((int)QuestionnaireReportOverviewColumn.TrafficLight)];
			//entity.TrafficLight = (Convert.IsDBNull(reader["TrafficLight"]))?string.Empty:(System.String)reader["TrafficLight"];
			entity.CompanyId1 = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CompanyId1)];
			//entity.CompanyId1 = (Convert.IsDBNull(reader["CompanyId_1"]))?(int)0:(System.Int32)reader["CompanyId_1"];
			entity.Kwi = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Kwi)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Kwi)];
			//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
			entity.KwiSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiSpaName)];
			//entity.KwiSpaName = (Convert.IsDBNull(reader["KWI_Spa_Name"]))?string.Empty:(System.String)reader["KWI_Spa_Name"];
			entity.KwiSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.KwiSpa)];
			//entity.KwiSpa = (Convert.IsDBNull(reader["KWI_Spa"]))?(int)0:(System.Int32?)reader["KWI_Spa"];
			entity.KwiSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiSponsorName)];
			//entity.KwiSponsorName = (Convert.IsDBNull(reader["KWI_Sponsor_Name"]))?string.Empty:(System.String)reader["KWI_Sponsor_Name"];
			entity.KwiSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.KwiSponsor)];
			//entity.KwiSponsor = (Convert.IsDBNull(reader["KWI_Sponsor"]))?(int)0:(System.Int32?)reader["KWI_Sponsor"];
			entity.KwiCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiCompanySiteCategory)];
			//entity.KwiCompanySiteCategory = (Convert.IsDBNull(reader["KWI_CompanySiteCategory"]))?string.Empty:(System.String)reader["KWI_CompanySiteCategory"];
			entity.Pin = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Pin)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Pin)];
			//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
			entity.PinSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinSpaName)];
			//entity.PinSpaName = (Convert.IsDBNull(reader["PIN_Spa_Name"]))?string.Empty:(System.String)reader["PIN_Spa_Name"];
			entity.PinSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PinSpa)];
			//entity.PinSpa = (Convert.IsDBNull(reader["PIN_Spa"]))?(int)0:(System.Int32?)reader["PIN_Spa"];
			entity.PinSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinSponsorName)];
			//entity.PinSponsorName = (Convert.IsDBNull(reader["PIN_Sponsor_Name"]))?string.Empty:(System.String)reader["PIN_Sponsor_Name"];
			entity.PinSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PinSponsor)];
			//entity.PinSponsor = (Convert.IsDBNull(reader["PIN_Sponsor"]))?(int)0:(System.Int32?)reader["PIN_Sponsor"];
			entity.PinCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinCompanySiteCategory)];
			//entity.PinCompanySiteCategory = (Convert.IsDBNull(reader["PIN_CompanySiteCategory"]))?string.Empty:(System.String)reader["PIN_CompanySiteCategory"];
			entity.Wgp = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Wgp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Wgp)];
			//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
			entity.WgpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpSpaName)];
			//entity.WgpSpaName = (Convert.IsDBNull(reader["WGP_Spa_Name"]))?string.Empty:(System.String)reader["WGP_Spa_Name"];
			entity.WgpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WgpSpa)];
			//entity.WgpSpa = (Convert.IsDBNull(reader["WGP_Spa"]))?(int)0:(System.Int32?)reader["WGP_Spa"];
			entity.WgpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpSponsorName)];
			//entity.WgpSponsorName = (Convert.IsDBNull(reader["WGP_Sponsor_Name"]))?string.Empty:(System.String)reader["WGP_Sponsor_Name"];
			entity.WgpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WgpSponsor)];
			//entity.WgpSponsor = (Convert.IsDBNull(reader["WGP_Sponsor"]))?(int)0:(System.Int32?)reader["WGP_Sponsor"];
			entity.WgpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpCompanySiteCategory)];
			//entity.WgpCompanySiteCategory = (Convert.IsDBNull(reader["WGP_CompanySiteCategory"]))?string.Empty:(System.String)reader["WGP_CompanySiteCategory"];
			entity.Hun = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Hun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Hun)];
			//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
			entity.HunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunSpaName)];
			//entity.HunSpaName = (Convert.IsDBNull(reader["HUN_Spa_Name"]))?string.Empty:(System.String)reader["HUN_Spa_Name"];
			entity.HunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.HunSpa)];
			//entity.HunSpa = (Convert.IsDBNull(reader["HUN_Spa"]))?(int)0:(System.Int32?)reader["HUN_Spa"];
			entity.HunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunSponsorName)];
			//entity.HunSponsorName = (Convert.IsDBNull(reader["HUN_Sponsor_Name"]))?string.Empty:(System.String)reader["HUN_Sponsor_Name"];
			entity.HunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.HunSponsor)];
			//entity.HunSponsor = (Convert.IsDBNull(reader["HUN_Sponsor"]))?(int)0:(System.Int32?)reader["HUN_Sponsor"];
			entity.HunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunCompanySiteCategory)];
			//entity.HunCompanySiteCategory = (Convert.IsDBNull(reader["HUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["HUN_CompanySiteCategory"];
			entity.Wdl = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Wdl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Wdl)];
			//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
			entity.WdlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlSpaName)];
			//entity.WdlSpaName = (Convert.IsDBNull(reader["WDL_Spa_Name"]))?string.Empty:(System.String)reader["WDL_Spa_Name"];
			entity.WdlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WdlSpa)];
			//entity.WdlSpa = (Convert.IsDBNull(reader["WDL_Spa"]))?(int)0:(System.Int32?)reader["WDL_Spa"];
			entity.WdlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlSponsorName)];
			//entity.WdlSponsorName = (Convert.IsDBNull(reader["WDL_Sponsor_Name"]))?string.Empty:(System.String)reader["WDL_Sponsor_Name"];
			entity.WdlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.WdlSponsor)];
			//entity.WdlSponsor = (Convert.IsDBNull(reader["WDL_Sponsor"]))?(int)0:(System.Int32?)reader["WDL_Sponsor"];
			entity.WdlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlCompanySiteCategory)];
			//entity.WdlCompanySiteCategory = (Convert.IsDBNull(reader["WDL_CompanySiteCategory"]))?string.Empty:(System.String)reader["WDL_CompanySiteCategory"];
			entity.Bun = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Bun)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Bun)];
			//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
			entity.BunSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunSpaName)];
			//entity.BunSpaName = (Convert.IsDBNull(reader["BUN_Spa_Name"]))?string.Empty:(System.String)reader["BUN_Spa_Name"];
			entity.BunSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BunSpa)];
			//entity.BunSpa = (Convert.IsDBNull(reader["BUN_Spa"]))?(int)0:(System.Int32?)reader["BUN_Spa"];
			entity.BunSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunSponsorName)];
			//entity.BunSponsorName = (Convert.IsDBNull(reader["BUN_Sponsor_Name"]))?string.Empty:(System.String)reader["BUN_Sponsor_Name"];
			entity.BunSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BunSponsor)];
			//entity.BunSponsor = (Convert.IsDBNull(reader["BUN_Sponsor"]))?(int)0:(System.Int32?)reader["BUN_Sponsor"];
			entity.BunCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunCompanySiteCategory)];
			//entity.BunCompanySiteCategory = (Convert.IsDBNull(reader["BUN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BUN_CompanySiteCategory"];
			entity.Fml = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Fml)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Fml)];
			//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
			entity.FmlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlSpaName)];
			//entity.FmlSpaName = (Convert.IsDBNull(reader["FML_Spa_Name"]))?string.Empty:(System.String)reader["FML_Spa_Name"];
			entity.FmlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.FmlSpa)];
			//entity.FmlSpa = (Convert.IsDBNull(reader["FML_Spa"]))?(int)0:(System.Int32?)reader["FML_Spa"];
			entity.FmlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlSponsorName)];
			//entity.FmlSponsorName = (Convert.IsDBNull(reader["FML_Sponsor_Name"]))?string.Empty:(System.String)reader["FML_Sponsor_Name"];
			entity.FmlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.FmlSponsor)];
			//entity.FmlSponsor = (Convert.IsDBNull(reader["FML_Sponsor"]))?(int)0:(System.Int32?)reader["FML_Sponsor"];
			entity.FmlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlCompanySiteCategory)];
			//entity.FmlCompanySiteCategory = (Convert.IsDBNull(reader["FML_CompanySiteCategory"]))?string.Empty:(System.String)reader["FML_CompanySiteCategory"];
			entity.Bgn = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Bgn)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Bgn)];
			//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
			entity.BgnSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnSpaName)];
			//entity.BgnSpaName = (Convert.IsDBNull(reader["BGN_Spa_Name"]))?string.Empty:(System.String)reader["BGN_Spa_Name"];
			entity.BgnSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BgnSpa)];
			//entity.BgnSpa = (Convert.IsDBNull(reader["BGN_Spa"]))?(int)0:(System.Int32?)reader["BGN_Spa"];
			entity.BgnSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnSponsorName)];
			//entity.BgnSponsorName = (Convert.IsDBNull(reader["BGN_Sponsor_Name"]))?string.Empty:(System.String)reader["BGN_Sponsor_Name"];
			entity.BgnSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.BgnSponsor)];
			//entity.BgnSponsor = (Convert.IsDBNull(reader["BGN_Sponsor"]))?(int)0:(System.Int32?)reader["BGN_Sponsor"];
			entity.BgnCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnCompanySiteCategory)];
			//entity.BgnCompanySiteCategory = (Convert.IsDBNull(reader["BGN_CompanySiteCategory"]))?string.Empty:(System.String)reader["BGN_CompanySiteCategory"];
			entity.Ce = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Ce)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Ce)];
			//entity.Ce = (Convert.IsDBNull(reader["CE"]))?string.Empty:(System.String)reader["CE"];
			entity.CeSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeSpaName)];
			//entity.CeSpaName = (Convert.IsDBNull(reader["CE_Spa_Name"]))?string.Empty:(System.String)reader["CE_Spa_Name"];
			entity.CeSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.CeSpa)];
			//entity.CeSpa = (Convert.IsDBNull(reader["CE_Spa"]))?(int)0:(System.Int32?)reader["CE_Spa"];
			entity.CeSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeSponsorName)];
			//entity.CeSponsorName = (Convert.IsDBNull(reader["CE_Sponsor_Name"]))?string.Empty:(System.String)reader["CE_Sponsor_Name"];
			entity.CeSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.CeSponsor)];
			//entity.CeSponsor = (Convert.IsDBNull(reader["CE_Sponsor"]))?(int)0:(System.Int32?)reader["CE_Sponsor"];
			entity.CeCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeCompanySiteCategory)];
			//entity.CeCompanySiteCategory = (Convert.IsDBNull(reader["CE_CompanySiteCategory"]))?string.Empty:(System.String)reader["CE_CompanySiteCategory"];
			entity.Ang = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Ang)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Ang)];
			//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
			entity.AngSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngSpaName)];
			//entity.AngSpaName = (Convert.IsDBNull(reader["ANG_Spa_Name"]))?string.Empty:(System.String)reader["ANG_Spa_Name"];
			entity.AngSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.AngSpa)];
			//entity.AngSpa = (Convert.IsDBNull(reader["ANG_Spa"]))?(int)0:(System.Int32?)reader["ANG_Spa"];
			entity.AngSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngSponsorName)];
			//entity.AngSponsorName = (Convert.IsDBNull(reader["ANG_Sponsor_Name"]))?string.Empty:(System.String)reader["ANG_Sponsor_Name"];
			entity.AngSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.AngSponsor)];
			//entity.AngSponsor = (Convert.IsDBNull(reader["ANG_Sponsor"]))?(int)0:(System.Int32?)reader["ANG_Sponsor"];
			entity.AngCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngCompanySiteCategory)];
			//entity.AngCompanySiteCategory = (Convert.IsDBNull(reader["ANG_CompanySiteCategory"]))?string.Empty:(System.String)reader["ANG_CompanySiteCategory"];
			entity.Ptl = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Ptl)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Ptl)];
			//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
			entity.PtlSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlSpaName)];
			//entity.PtlSpaName = (Convert.IsDBNull(reader["PTL_Spa_Name"]))?string.Empty:(System.String)reader["PTL_Spa_Name"];
			entity.PtlSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PtlSpa)];
			//entity.PtlSpa = (Convert.IsDBNull(reader["PTL_Spa"]))?(int)0:(System.Int32?)reader["PTL_Spa"];
			entity.PtlSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlSponsorName)];
			//entity.PtlSponsorName = (Convert.IsDBNull(reader["PTL_Sponsor_Name"]))?string.Empty:(System.String)reader["PTL_Sponsor_Name"];
			entity.PtlSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PtlSponsor)];
			//entity.PtlSponsor = (Convert.IsDBNull(reader["PTL_Sponsor"]))?(int)0:(System.Int32?)reader["PTL_Sponsor"];
			entity.PtlCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlCompanySiteCategory)];
			//entity.PtlCompanySiteCategory = (Convert.IsDBNull(reader["PTL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTL_CompanySiteCategory"];
			entity.Pth = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Pth)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Pth)];
			//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
			entity.PthSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthSpaName)];
			//entity.PthSpaName = (Convert.IsDBNull(reader["PTH_Spa_Name"]))?string.Empty:(System.String)reader["PTH_Spa_Name"];
			entity.PthSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PthSpa)];
			//entity.PthSpa = (Convert.IsDBNull(reader["PTH_Spa"]))?(int)0:(System.Int32?)reader["PTH_Spa"];
			entity.PthSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthSponsorName)];
			//entity.PthSponsorName = (Convert.IsDBNull(reader["PTH_Sponsor_Name"]))?string.Empty:(System.String)reader["PTH_Sponsor_Name"];
			entity.PthSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PthSponsor)];
			//entity.PthSponsor = (Convert.IsDBNull(reader["PTH_Sponsor"]))?(int)0:(System.Int32?)reader["PTH_Sponsor"];
			entity.PthCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthCompanySiteCategory)];
			//entity.PthCompanySiteCategory = (Convert.IsDBNull(reader["PTH_CompanySiteCategory"]))?string.Empty:(System.String)reader["PTH_CompanySiteCategory"];
			entity.Pel = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Pel)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Pel)];
			//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
			entity.PelSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelSpaName)];
			//entity.PelSpaName = (Convert.IsDBNull(reader["PEL_Spa_Name"]))?string.Empty:(System.String)reader["PEL_Spa_Name"];
			entity.PelSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PelSpa)];
			//entity.PelSpa = (Convert.IsDBNull(reader["PEL_Spa"]))?(int)0:(System.Int32?)reader["PEL_Spa"];
			entity.PelSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelSponsorName)];
			//entity.PelSponsorName = (Convert.IsDBNull(reader["PEL_Sponsor_Name"]))?string.Empty:(System.String)reader["PEL_Sponsor_Name"];
			entity.PelSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.PelSponsor)];
			//entity.PelSponsor = (Convert.IsDBNull(reader["PEL_Sponsor"]))?(int)0:(System.Int32?)reader["PEL_Sponsor"];
			entity.PelCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelCompanySiteCategory)];
			//entity.PelCompanySiteCategory = (Convert.IsDBNull(reader["PEL_CompanySiteCategory"]))?string.Empty:(System.String)reader["PEL_CompanySiteCategory"];
			entity.Arp = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Arp)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Arp)];
			//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
			entity.ArpSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpSpaName)];
			//entity.ArpSpaName = (Convert.IsDBNull(reader["ARP_Spa_Name"]))?string.Empty:(System.String)reader["ARP_Spa_Name"];
			entity.ArpSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ArpSpa)];
			//entity.ArpSpa = (Convert.IsDBNull(reader["ARP_Spa"]))?(int)0:(System.Int32?)reader["ARP_Spa"];
			entity.ArpSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpSponsorName)];
			//entity.ArpSponsorName = (Convert.IsDBNull(reader["ARP_Sponsor_Name"]))?string.Empty:(System.String)reader["ARP_Sponsor_Name"];
			entity.ArpSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.ArpSponsor)];
			//entity.ArpSponsor = (Convert.IsDBNull(reader["ARP_Sponsor"]))?(int)0:(System.Int32?)reader["ARP_Sponsor"];
			entity.ArpCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpCompanySiteCategory)];
			//entity.ArpCompanySiteCategory = (Convert.IsDBNull(reader["ARP_CompanySiteCategory"]))?string.Empty:(System.String)reader["ARP_CompanySiteCategory"];
			entity.Yen = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.Yen)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.Yen)];
			//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
			entity.YenSpaName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSpaName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenSpaName)];
			//entity.YenSpaName = (Convert.IsDBNull(reader["YEN_Spa_Name"]))?string.Empty:(System.String)reader["YEN_Spa_Name"];
			entity.YenSpa = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSpa)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.YenSpa)];
			//entity.YenSpa = (Convert.IsDBNull(reader["YEN_Spa"]))?(int)0:(System.Int32?)reader["YEN_Spa"];
			entity.YenSponsorName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSponsorName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenSponsorName)];
			//entity.YenSponsorName = (Convert.IsDBNull(reader["YEN_Sponsor_Name"]))?string.Empty:(System.String)reader["YEN_Sponsor_Name"];
			entity.YenSponsor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenSponsor)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.YenSponsor)];
			//entity.YenSponsor = (Convert.IsDBNull(reader["YEN_Sponsor"]))?(int)0:(System.Int32?)reader["YEN_Sponsor"];
			entity.YenCompanySiteCategory = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenCompanySiteCategory)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenCompanySiteCategory)];
			//entity.YenCompanySiteCategory = (Convert.IsDBNull(reader["YEN_CompanySiteCategory"]))?string.Empty:(System.String)reader["YEN_CompanySiteCategory"];
			entity.CompanyId2 = (System.Int32)reader[((int)QuestionnaireReportOverviewColumn.CompanyId2)];
			//entity.CompanyId2 = (Convert.IsDBNull(reader["CompanyId_2"]))?(int)0:(System.Int32)reader["CompanyId_2"];
			entity.KwiArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiArpName)];
			//entity.KwiArpName = (Convert.IsDBNull(reader["KWI_ARP_Name"]))?string.Empty:(System.String)reader["KWI_ARP_Name"];
			entity.KwiCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.KwiCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.KwiCrpName)];
			//entity.KwiCrpName = (Convert.IsDBNull(reader["KWI_CRP_Name"]))?string.Empty:(System.String)reader["KWI_CRP_Name"];
			entity.PinArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinArpName)];
			//entity.PinArpName = (Convert.IsDBNull(reader["PIN_ARP_Name"]))?string.Empty:(System.String)reader["PIN_ARP_Name"];
			entity.PinCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PinCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PinCrpName)];
			//entity.PinCrpName = (Convert.IsDBNull(reader["PIN_CRP_Name"]))?string.Empty:(System.String)reader["PIN_CRP_Name"];
			entity.WgpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpArpName)];
			//entity.WgpArpName = (Convert.IsDBNull(reader["WGP_ARP_Name"]))?string.Empty:(System.String)reader["WGP_ARP_Name"];
			entity.WgpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WgpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WgpCrpName)];
			//entity.WgpCrpName = (Convert.IsDBNull(reader["WGP_CRP_Name"]))?string.Empty:(System.String)reader["WGP_CRP_Name"];
			entity.HunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunArpName)];
			//entity.HunArpName = (Convert.IsDBNull(reader["HUN_ARP_Name"]))?string.Empty:(System.String)reader["HUN_ARP_Name"];
			entity.HunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.HunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.HunCrpName)];
			//entity.HunCrpName = (Convert.IsDBNull(reader["HUN_CRP_Name"]))?string.Empty:(System.String)reader["HUN_CRP_Name"];
			entity.WdlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlArpName)];
			//entity.WdlArpName = (Convert.IsDBNull(reader["WDL_ARP_Name"]))?string.Empty:(System.String)reader["WDL_ARP_Name"];
			entity.WdlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.WdlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.WdlCrpName)];
			//entity.WdlCrpName = (Convert.IsDBNull(reader["WDL_CRP_Name"]))?string.Empty:(System.String)reader["WDL_CRP_Name"];
			entity.BunArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunArpName)];
			//entity.BunArpName = (Convert.IsDBNull(reader["BUN_ARP_Name"]))?string.Empty:(System.String)reader["BUN_ARP_Name"];
			entity.BunCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BunCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BunCrpName)];
			//entity.BunCrpName = (Convert.IsDBNull(reader["BUN_CRP_Name"]))?string.Empty:(System.String)reader["BUN_CRP_Name"];
			entity.FmlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlArpName)];
			//entity.FmlArpName = (Convert.IsDBNull(reader["FML_ARP_Name"]))?string.Empty:(System.String)reader["FML_ARP_Name"];
			entity.FmlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.FmlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.FmlCrpName)];
			//entity.FmlCrpName = (Convert.IsDBNull(reader["FML_CRP_Name"]))?string.Empty:(System.String)reader["FML_CRP_Name"];
			entity.BgnArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnArpName)];
			//entity.BgnArpName = (Convert.IsDBNull(reader["BGN_ARP_Name"]))?string.Empty:(System.String)reader["BGN_ARP_Name"];
			entity.BgnCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.BgnCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.BgnCrpName)];
			//entity.BgnCrpName = (Convert.IsDBNull(reader["BGN_CRP_Name"]))?string.Empty:(System.String)reader["BGN_CRP_Name"];
			entity.CeArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeArpName)];
			//entity.CeArpName = (Convert.IsDBNull(reader["CE_ARP_Name"]))?string.Empty:(System.String)reader["CE_ARP_Name"];
			entity.CeCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.CeCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.CeCrpName)];
			//entity.CeCrpName = (Convert.IsDBNull(reader["CE_CRP_Name"]))?string.Empty:(System.String)reader["CE_CRP_Name"];
			entity.AngArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngArpName)];
			//entity.AngArpName = (Convert.IsDBNull(reader["ANG_ARP_Name"]))?string.Empty:(System.String)reader["ANG_ARP_Name"];
			entity.AngCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.AngCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.AngCrpName)];
			//entity.AngCrpName = (Convert.IsDBNull(reader["ANG_CRP_Name"]))?string.Empty:(System.String)reader["ANG_CRP_Name"];
			entity.PtlArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlArpName)];
			//entity.PtlArpName = (Convert.IsDBNull(reader["PTL_ARP_Name"]))?string.Empty:(System.String)reader["PTL_ARP_Name"];
			entity.PtlCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PtlCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PtlCrpName)];
			//entity.PtlCrpName = (Convert.IsDBNull(reader["PTL_CRP_Name"]))?string.Empty:(System.String)reader["PTL_CRP_Name"];
			entity.PthArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthArpName)];
			//entity.PthArpName = (Convert.IsDBNull(reader["PTH_ARP_Name"]))?string.Empty:(System.String)reader["PTH_ARP_Name"];
			entity.PthCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PthCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PthCrpName)];
			//entity.PthCrpName = (Convert.IsDBNull(reader["PTH_CRP_Name"]))?string.Empty:(System.String)reader["PTH_CRP_Name"];
			entity.PelArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelArpName)];
			//entity.PelArpName = (Convert.IsDBNull(reader["PEL_ARP_Name"]))?string.Empty:(System.String)reader["PEL_ARP_Name"];
			entity.PelCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.PelCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.PelCrpName)];
			//entity.PelCrpName = (Convert.IsDBNull(reader["PEL_CRP_Name"]))?string.Empty:(System.String)reader["PEL_CRP_Name"];
			entity.ArpArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpArpName)];
			//entity.ArpArpName = (Convert.IsDBNull(reader["ARP_ARP_Name"]))?string.Empty:(System.String)reader["ARP_ARP_Name"];
			entity.ArpCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ArpCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ArpCrpName)];
			//entity.ArpCrpName = (Convert.IsDBNull(reader["ARP_CRP_Name"]))?string.Empty:(System.String)reader["ARP_CRP_Name"];
			entity.YenArpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenArpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenArpName)];
			//entity.YenArpName = (Convert.IsDBNull(reader["YEN_ARP_Name"]))?string.Empty:(System.String)reader["YEN_ARP_Name"];
			entity.YenCrpName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.YenCrpName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.YenCrpName)];
			//entity.YenCrpName = (Convert.IsDBNull(reader["YEN_CRP_Name"]))?string.Empty:(System.String)reader["YEN_CRP_Name"];
			entity.RequestingCompanyId = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.RequestingCompanyId)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewColumn.RequestingCompanyId)];
			//entity.RequestingCompanyId = (Convert.IsDBNull(reader["RequestingCompanyId"]))?(int)0:(System.Int32?)reader["RequestingCompanyId"];
			entity.RequestingCompanyName = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.RequestingCompanyName)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.RequestingCompanyName)];
			//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
			entity.ReasonContractor = (reader.IsDBNull(((int)QuestionnaireReportOverviewColumn.ReasonContractor)))?null:(System.String)reader[((int)QuestionnaireReportOverviewColumn.ReasonContractor)];
			//entity.ReasonContractor = (Convert.IsDBNull(reader["ReasonContractor"]))?string.Empty:(System.String)reader["ReasonContractor"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverview"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverview"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportOverview entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.CompanyAbn = (Convert.IsDBNull(dataRow["CompanyAbn"]))?string.Empty:(System.String)dataRow["CompanyAbn"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.InitialRiskAssessment = (Convert.IsDBNull(dataRow["InitialRiskAssessment"]))?string.Empty:(System.String)dataRow["InitialRiskAssessment"];
			entity.MainAssessmentRiskRating = (Convert.IsDBNull(dataRow["MainAssessmentRiskRating"]))?string.Empty:(System.String)dataRow["MainAssessmentRiskRating"];
			entity.FinalRiskRating = (Convert.IsDBNull(dataRow["FinalRiskRating"]))?string.Empty:(System.String)dataRow["FinalRiskRating"];
			entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
			entity.Type = (Convert.IsDBNull(dataRow["Type"]))?string.Empty:(System.String)dataRow["Type"];
			entity.DescriptionOfWork = (Convert.IsDBNull(dataRow["DescriptionOfWork"]))?string.Empty:(System.String)dataRow["DescriptionOfWork"];
			entity.LevelOfSupervision = (Convert.IsDBNull(dataRow["LevelOfSupervision"]))?string.Empty:(System.String)dataRow["LevelOfSupervision"];
			entity.PrimaryContractor = (Convert.IsDBNull(dataRow["PrimaryContractor"]))?string.Empty:(System.String)dataRow["PrimaryContractor"];
			entity.ProcurementContactUserId = (Convert.IsDBNull(dataRow["ProcurementContactUserId"]))?(int)0:(System.Int32?)dataRow["ProcurementContactUserId"];
			entity.ContractManagerUserId = (Convert.IsDBNull(dataRow["ContractManagerUserId"]))?(int)0:(System.Int32?)dataRow["ContractManagerUserId"];
			entity.ProcurementContactUser = (Convert.IsDBNull(dataRow["ProcurementContactUser"]))?string.Empty:(System.String)dataRow["ProcurementContactUser"];
			entity.ContractManagerUser = (Convert.IsDBNull(dataRow["ContractManagerUser"]))?string.Empty:(System.String)dataRow["ContractManagerUser"];
			entity.TypeOfService = (Convert.IsDBNull(dataRow["TypeOfService"]))?(int)0:(System.Int32?)dataRow["TypeOfService"];
			entity.MainAssessmentValidTo = (Convert.IsDBNull(dataRow["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentValidTo"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedByUser = (Convert.IsDBNull(dataRow["ApprovedByUser"]))?string.Empty:(System.String)dataRow["ApprovedByUser"];
			entity.IsMainRequired = (Convert.IsDBNull(dataRow["IsMainRequired"]))?false:(System.Boolean)dataRow["IsMainRequired"];
			entity.IsVerificationRequired = (Convert.IsDBNull(dataRow["IsVerificationRequired"]))?false:(System.Boolean)dataRow["IsVerificationRequired"];
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.NumberOfPeople = (Convert.IsDBNull(dataRow["NumberOfPeople"]))?string.Empty:(System.String)dataRow["NumberOfPeople"];
			entity.LastSpqDate = (Convert.IsDBNull(dataRow["LastSPQDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastSPQDate"];
			entity.InitialSpqDate = (Convert.IsDBNull(dataRow["InitialSPQDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialSPQDate"];
			entity.Active = (Convert.IsDBNull(dataRow["Active"]))?false:(System.Boolean?)dataRow["Active"];
			entity.MainScorePoverall = (Convert.IsDBNull(dataRow["MainScorePOverall"]))?(int)0:(System.Int32?)dataRow["MainScorePOverall"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedByUser = (Convert.IsDBNull(dataRow["CreatedByUser"]))?string.Empty:(System.String)dataRow["CreatedByUser"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedByUser = (Convert.IsDBNull(dataRow["ModifiedByUser"]))?string.Empty:(System.String)dataRow["ModifiedByUser"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.SubContractor = (Convert.IsDBNull(dataRow["SubContractor"]))?false:(System.Boolean?)dataRow["SubContractor"];
			entity.Validity = (Convert.IsDBNull(dataRow["Validity"]))?string.Empty:(System.String)dataRow["Validity"];
			entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.ProcessNo = (Convert.IsDBNull(dataRow["ProcessNo"]))?(int)0:(System.Int32?)dataRow["ProcessNo"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.ActionName = (Convert.IsDBNull(dataRow["ActionName"]))?string.Empty:(System.String)dataRow["ActionName"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithSinceDaysCount"];
			entity.DaysTillExpiry = (Convert.IsDBNull(dataRow["DaysTillExpiry"]))?(int)0:(System.Int32?)dataRow["DaysTillExpiry"];
			entity.TrafficLight = (Convert.IsDBNull(dataRow["TrafficLight"]))?string.Empty:(System.String)dataRow["TrafficLight"];
			entity.CompanyId1 = (Convert.IsDBNull(dataRow["CompanyId_1"]))?(int)0:(System.Int32)dataRow["CompanyId_1"];
			entity.Kwi = (Convert.IsDBNull(dataRow["KWI"]))?string.Empty:(System.String)dataRow["KWI"];
			entity.KwiSpaName = (Convert.IsDBNull(dataRow["KWI_Spa_Name"]))?string.Empty:(System.String)dataRow["KWI_Spa_Name"];
			entity.KwiSpa = (Convert.IsDBNull(dataRow["KWI_Spa"]))?(int)0:(System.Int32?)dataRow["KWI_Spa"];
			entity.KwiSponsorName = (Convert.IsDBNull(dataRow["KWI_Sponsor_Name"]))?string.Empty:(System.String)dataRow["KWI_Sponsor_Name"];
			entity.KwiSponsor = (Convert.IsDBNull(dataRow["KWI_Sponsor"]))?(int)0:(System.Int32?)dataRow["KWI_Sponsor"];
			entity.KwiCompanySiteCategory = (Convert.IsDBNull(dataRow["KWI_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["KWI_CompanySiteCategory"];
			entity.Pin = (Convert.IsDBNull(dataRow["PIN"]))?string.Empty:(System.String)dataRow["PIN"];
			entity.PinSpaName = (Convert.IsDBNull(dataRow["PIN_Spa_Name"]))?string.Empty:(System.String)dataRow["PIN_Spa_Name"];
			entity.PinSpa = (Convert.IsDBNull(dataRow["PIN_Spa"]))?(int)0:(System.Int32?)dataRow["PIN_Spa"];
			entity.PinSponsorName = (Convert.IsDBNull(dataRow["PIN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PIN_Sponsor_Name"];
			entity.PinSponsor = (Convert.IsDBNull(dataRow["PIN_Sponsor"]))?(int)0:(System.Int32?)dataRow["PIN_Sponsor"];
			entity.PinCompanySiteCategory = (Convert.IsDBNull(dataRow["PIN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PIN_CompanySiteCategory"];
			entity.Wgp = (Convert.IsDBNull(dataRow["WGP"]))?string.Empty:(System.String)dataRow["WGP"];
			entity.WgpSpaName = (Convert.IsDBNull(dataRow["WGP_Spa_Name"]))?string.Empty:(System.String)dataRow["WGP_Spa_Name"];
			entity.WgpSpa = (Convert.IsDBNull(dataRow["WGP_Spa"]))?(int)0:(System.Int32?)dataRow["WGP_Spa"];
			entity.WgpSponsorName = (Convert.IsDBNull(dataRow["WGP_Sponsor_Name"]))?string.Empty:(System.String)dataRow["WGP_Sponsor_Name"];
			entity.WgpSponsor = (Convert.IsDBNull(dataRow["WGP_Sponsor"]))?(int)0:(System.Int32?)dataRow["WGP_Sponsor"];
			entity.WgpCompanySiteCategory = (Convert.IsDBNull(dataRow["WGP_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["WGP_CompanySiteCategory"];
			entity.Hun = (Convert.IsDBNull(dataRow["HUN"]))?string.Empty:(System.String)dataRow["HUN"];
			entity.HunSpaName = (Convert.IsDBNull(dataRow["HUN_Spa_Name"]))?string.Empty:(System.String)dataRow["HUN_Spa_Name"];
			entity.HunSpa = (Convert.IsDBNull(dataRow["HUN_Spa"]))?(int)0:(System.Int32?)dataRow["HUN_Spa"];
			entity.HunSponsorName = (Convert.IsDBNull(dataRow["HUN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["HUN_Sponsor_Name"];
			entity.HunSponsor = (Convert.IsDBNull(dataRow["HUN_Sponsor"]))?(int)0:(System.Int32?)dataRow["HUN_Sponsor"];
			entity.HunCompanySiteCategory = (Convert.IsDBNull(dataRow["HUN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["HUN_CompanySiteCategory"];
			entity.Wdl = (Convert.IsDBNull(dataRow["WDL"]))?string.Empty:(System.String)dataRow["WDL"];
			entity.WdlSpaName = (Convert.IsDBNull(dataRow["WDL_Spa_Name"]))?string.Empty:(System.String)dataRow["WDL_Spa_Name"];
			entity.WdlSpa = (Convert.IsDBNull(dataRow["WDL_Spa"]))?(int)0:(System.Int32?)dataRow["WDL_Spa"];
			entity.WdlSponsorName = (Convert.IsDBNull(dataRow["WDL_Sponsor_Name"]))?string.Empty:(System.String)dataRow["WDL_Sponsor_Name"];
			entity.WdlSponsor = (Convert.IsDBNull(dataRow["WDL_Sponsor"]))?(int)0:(System.Int32?)dataRow["WDL_Sponsor"];
			entity.WdlCompanySiteCategory = (Convert.IsDBNull(dataRow["WDL_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["WDL_CompanySiteCategory"];
			entity.Bun = (Convert.IsDBNull(dataRow["BUN"]))?string.Empty:(System.String)dataRow["BUN"];
			entity.BunSpaName = (Convert.IsDBNull(dataRow["BUN_Spa_Name"]))?string.Empty:(System.String)dataRow["BUN_Spa_Name"];
			entity.BunSpa = (Convert.IsDBNull(dataRow["BUN_Spa"]))?(int)0:(System.Int32?)dataRow["BUN_Spa"];
			entity.BunSponsorName = (Convert.IsDBNull(dataRow["BUN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["BUN_Sponsor_Name"];
			entity.BunSponsor = (Convert.IsDBNull(dataRow["BUN_Sponsor"]))?(int)0:(System.Int32?)dataRow["BUN_Sponsor"];
			entity.BunCompanySiteCategory = (Convert.IsDBNull(dataRow["BUN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["BUN_CompanySiteCategory"];
			entity.Fml = (Convert.IsDBNull(dataRow["FML"]))?string.Empty:(System.String)dataRow["FML"];
			entity.FmlSpaName = (Convert.IsDBNull(dataRow["FML_Spa_Name"]))?string.Empty:(System.String)dataRow["FML_Spa_Name"];
			entity.FmlSpa = (Convert.IsDBNull(dataRow["FML_Spa"]))?(int)0:(System.Int32?)dataRow["FML_Spa"];
			entity.FmlSponsorName = (Convert.IsDBNull(dataRow["FML_Sponsor_Name"]))?string.Empty:(System.String)dataRow["FML_Sponsor_Name"];
			entity.FmlSponsor = (Convert.IsDBNull(dataRow["FML_Sponsor"]))?(int)0:(System.Int32?)dataRow["FML_Sponsor"];
			entity.FmlCompanySiteCategory = (Convert.IsDBNull(dataRow["FML_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["FML_CompanySiteCategory"];
			entity.Bgn = (Convert.IsDBNull(dataRow["BGN"]))?string.Empty:(System.String)dataRow["BGN"];
			entity.BgnSpaName = (Convert.IsDBNull(dataRow["BGN_Spa_Name"]))?string.Empty:(System.String)dataRow["BGN_Spa_Name"];
			entity.BgnSpa = (Convert.IsDBNull(dataRow["BGN_Spa"]))?(int)0:(System.Int32?)dataRow["BGN_Spa"];
			entity.BgnSponsorName = (Convert.IsDBNull(dataRow["BGN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["BGN_Sponsor_Name"];
			entity.BgnSponsor = (Convert.IsDBNull(dataRow["BGN_Sponsor"]))?(int)0:(System.Int32?)dataRow["BGN_Sponsor"];
			entity.BgnCompanySiteCategory = (Convert.IsDBNull(dataRow["BGN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["BGN_CompanySiteCategory"];
			entity.Ce = (Convert.IsDBNull(dataRow["CE"]))?string.Empty:(System.String)dataRow["CE"];
			entity.CeSpaName = (Convert.IsDBNull(dataRow["CE_Spa_Name"]))?string.Empty:(System.String)dataRow["CE_Spa_Name"];
			entity.CeSpa = (Convert.IsDBNull(dataRow["CE_Spa"]))?(int)0:(System.Int32?)dataRow["CE_Spa"];
			entity.CeSponsorName = (Convert.IsDBNull(dataRow["CE_Sponsor_Name"]))?string.Empty:(System.String)dataRow["CE_Sponsor_Name"];
			entity.CeSponsor = (Convert.IsDBNull(dataRow["CE_Sponsor"]))?(int)0:(System.Int32?)dataRow["CE_Sponsor"];
			entity.CeCompanySiteCategory = (Convert.IsDBNull(dataRow["CE_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["CE_CompanySiteCategory"];
			entity.Ang = (Convert.IsDBNull(dataRow["ANG"]))?string.Empty:(System.String)dataRow["ANG"];
			entity.AngSpaName = (Convert.IsDBNull(dataRow["ANG_Spa_Name"]))?string.Empty:(System.String)dataRow["ANG_Spa_Name"];
			entity.AngSpa = (Convert.IsDBNull(dataRow["ANG_Spa"]))?(int)0:(System.Int32?)dataRow["ANG_Spa"];
			entity.AngSponsorName = (Convert.IsDBNull(dataRow["ANG_Sponsor_Name"]))?string.Empty:(System.String)dataRow["ANG_Sponsor_Name"];
			entity.AngSponsor = (Convert.IsDBNull(dataRow["ANG_Sponsor"]))?(int)0:(System.Int32?)dataRow["ANG_Sponsor"];
			entity.AngCompanySiteCategory = (Convert.IsDBNull(dataRow["ANG_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["ANG_CompanySiteCategory"];
			entity.Ptl = (Convert.IsDBNull(dataRow["PTL"]))?string.Empty:(System.String)dataRow["PTL"];
			entity.PtlSpaName = (Convert.IsDBNull(dataRow["PTL_Spa_Name"]))?string.Empty:(System.String)dataRow["PTL_Spa_Name"];
			entity.PtlSpa = (Convert.IsDBNull(dataRow["PTL_Spa"]))?(int)0:(System.Int32?)dataRow["PTL_Spa"];
			entity.PtlSponsorName = (Convert.IsDBNull(dataRow["PTL_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PTL_Sponsor_Name"];
			entity.PtlSponsor = (Convert.IsDBNull(dataRow["PTL_Sponsor"]))?(int)0:(System.Int32?)dataRow["PTL_Sponsor"];
			entity.PtlCompanySiteCategory = (Convert.IsDBNull(dataRow["PTL_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PTL_CompanySiteCategory"];
			entity.Pth = (Convert.IsDBNull(dataRow["PTH"]))?string.Empty:(System.String)dataRow["PTH"];
			entity.PthSpaName = (Convert.IsDBNull(dataRow["PTH_Spa_Name"]))?string.Empty:(System.String)dataRow["PTH_Spa_Name"];
			entity.PthSpa = (Convert.IsDBNull(dataRow["PTH_Spa"]))?(int)0:(System.Int32?)dataRow["PTH_Spa"];
			entity.PthSponsorName = (Convert.IsDBNull(dataRow["PTH_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PTH_Sponsor_Name"];
			entity.PthSponsor = (Convert.IsDBNull(dataRow["PTH_Sponsor"]))?(int)0:(System.Int32?)dataRow["PTH_Sponsor"];
			entity.PthCompanySiteCategory = (Convert.IsDBNull(dataRow["PTH_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PTH_CompanySiteCategory"];
			entity.Pel = (Convert.IsDBNull(dataRow["PEL"]))?string.Empty:(System.String)dataRow["PEL"];
			entity.PelSpaName = (Convert.IsDBNull(dataRow["PEL_Spa_Name"]))?string.Empty:(System.String)dataRow["PEL_Spa_Name"];
			entity.PelSpa = (Convert.IsDBNull(dataRow["PEL_Spa"]))?(int)0:(System.Int32?)dataRow["PEL_Spa"];
			entity.PelSponsorName = (Convert.IsDBNull(dataRow["PEL_Sponsor_Name"]))?string.Empty:(System.String)dataRow["PEL_Sponsor_Name"];
			entity.PelSponsor = (Convert.IsDBNull(dataRow["PEL_Sponsor"]))?(int)0:(System.Int32?)dataRow["PEL_Sponsor"];
			entity.PelCompanySiteCategory = (Convert.IsDBNull(dataRow["PEL_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["PEL_CompanySiteCategory"];
			entity.Arp = (Convert.IsDBNull(dataRow["ARP"]))?string.Empty:(System.String)dataRow["ARP"];
			entity.ArpSpaName = (Convert.IsDBNull(dataRow["ARP_Spa_Name"]))?string.Empty:(System.String)dataRow["ARP_Spa_Name"];
			entity.ArpSpa = (Convert.IsDBNull(dataRow["ARP_Spa"]))?(int)0:(System.Int32?)dataRow["ARP_Spa"];
			entity.ArpSponsorName = (Convert.IsDBNull(dataRow["ARP_Sponsor_Name"]))?string.Empty:(System.String)dataRow["ARP_Sponsor_Name"];
			entity.ArpSponsor = (Convert.IsDBNull(dataRow["ARP_Sponsor"]))?(int)0:(System.Int32?)dataRow["ARP_Sponsor"];
			entity.ArpCompanySiteCategory = (Convert.IsDBNull(dataRow["ARP_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["ARP_CompanySiteCategory"];
			entity.Yen = (Convert.IsDBNull(dataRow["YEN"]))?string.Empty:(System.String)dataRow["YEN"];
			entity.YenSpaName = (Convert.IsDBNull(dataRow["YEN_Spa_Name"]))?string.Empty:(System.String)dataRow["YEN_Spa_Name"];
			entity.YenSpa = (Convert.IsDBNull(dataRow["YEN_Spa"]))?(int)0:(System.Int32?)dataRow["YEN_Spa"];
			entity.YenSponsorName = (Convert.IsDBNull(dataRow["YEN_Sponsor_Name"]))?string.Empty:(System.String)dataRow["YEN_Sponsor_Name"];
			entity.YenSponsor = (Convert.IsDBNull(dataRow["YEN_Sponsor"]))?(int)0:(System.Int32?)dataRow["YEN_Sponsor"];
			entity.YenCompanySiteCategory = (Convert.IsDBNull(dataRow["YEN_CompanySiteCategory"]))?string.Empty:(System.String)dataRow["YEN_CompanySiteCategory"];
			entity.CompanyId2 = (Convert.IsDBNull(dataRow["CompanyId_2"]))?(int)0:(System.Int32)dataRow["CompanyId_2"];
			entity.KwiArpName = (Convert.IsDBNull(dataRow["KWI_ARP_Name"]))?string.Empty:(System.String)dataRow["KWI_ARP_Name"];
			entity.KwiCrpName = (Convert.IsDBNull(dataRow["KWI_CRP_Name"]))?string.Empty:(System.String)dataRow["KWI_CRP_Name"];
			entity.PinArpName = (Convert.IsDBNull(dataRow["PIN_ARP_Name"]))?string.Empty:(System.String)dataRow["PIN_ARP_Name"];
			entity.PinCrpName = (Convert.IsDBNull(dataRow["PIN_CRP_Name"]))?string.Empty:(System.String)dataRow["PIN_CRP_Name"];
			entity.WgpArpName = (Convert.IsDBNull(dataRow["WGP_ARP_Name"]))?string.Empty:(System.String)dataRow["WGP_ARP_Name"];
			entity.WgpCrpName = (Convert.IsDBNull(dataRow["WGP_CRP_Name"]))?string.Empty:(System.String)dataRow["WGP_CRP_Name"];
			entity.HunArpName = (Convert.IsDBNull(dataRow["HUN_ARP_Name"]))?string.Empty:(System.String)dataRow["HUN_ARP_Name"];
			entity.HunCrpName = (Convert.IsDBNull(dataRow["HUN_CRP_Name"]))?string.Empty:(System.String)dataRow["HUN_CRP_Name"];
			entity.WdlArpName = (Convert.IsDBNull(dataRow["WDL_ARP_Name"]))?string.Empty:(System.String)dataRow["WDL_ARP_Name"];
			entity.WdlCrpName = (Convert.IsDBNull(dataRow["WDL_CRP_Name"]))?string.Empty:(System.String)dataRow["WDL_CRP_Name"];
			entity.BunArpName = (Convert.IsDBNull(dataRow["BUN_ARP_Name"]))?string.Empty:(System.String)dataRow["BUN_ARP_Name"];
			entity.BunCrpName = (Convert.IsDBNull(dataRow["BUN_CRP_Name"]))?string.Empty:(System.String)dataRow["BUN_CRP_Name"];
			entity.FmlArpName = (Convert.IsDBNull(dataRow["FML_ARP_Name"]))?string.Empty:(System.String)dataRow["FML_ARP_Name"];
			entity.FmlCrpName = (Convert.IsDBNull(dataRow["FML_CRP_Name"]))?string.Empty:(System.String)dataRow["FML_CRP_Name"];
			entity.BgnArpName = (Convert.IsDBNull(dataRow["BGN_ARP_Name"]))?string.Empty:(System.String)dataRow["BGN_ARP_Name"];
			entity.BgnCrpName = (Convert.IsDBNull(dataRow["BGN_CRP_Name"]))?string.Empty:(System.String)dataRow["BGN_CRP_Name"];
			entity.CeArpName = (Convert.IsDBNull(dataRow["CE_ARP_Name"]))?string.Empty:(System.String)dataRow["CE_ARP_Name"];
			entity.CeCrpName = (Convert.IsDBNull(dataRow["CE_CRP_Name"]))?string.Empty:(System.String)dataRow["CE_CRP_Name"];
			entity.AngArpName = (Convert.IsDBNull(dataRow["ANG_ARP_Name"]))?string.Empty:(System.String)dataRow["ANG_ARP_Name"];
			entity.AngCrpName = (Convert.IsDBNull(dataRow["ANG_CRP_Name"]))?string.Empty:(System.String)dataRow["ANG_CRP_Name"];
			entity.PtlArpName = (Convert.IsDBNull(dataRow["PTL_ARP_Name"]))?string.Empty:(System.String)dataRow["PTL_ARP_Name"];
			entity.PtlCrpName = (Convert.IsDBNull(dataRow["PTL_CRP_Name"]))?string.Empty:(System.String)dataRow["PTL_CRP_Name"];
			entity.PthArpName = (Convert.IsDBNull(dataRow["PTH_ARP_Name"]))?string.Empty:(System.String)dataRow["PTH_ARP_Name"];
			entity.PthCrpName = (Convert.IsDBNull(dataRow["PTH_CRP_Name"]))?string.Empty:(System.String)dataRow["PTH_CRP_Name"];
			entity.PelArpName = (Convert.IsDBNull(dataRow["PEL_ARP_Name"]))?string.Empty:(System.String)dataRow["PEL_ARP_Name"];
			entity.PelCrpName = (Convert.IsDBNull(dataRow["PEL_CRP_Name"]))?string.Empty:(System.String)dataRow["PEL_CRP_Name"];
			entity.ArpArpName = (Convert.IsDBNull(dataRow["ARP_ARP_Name"]))?string.Empty:(System.String)dataRow["ARP_ARP_Name"];
			entity.ArpCrpName = (Convert.IsDBNull(dataRow["ARP_CRP_Name"]))?string.Empty:(System.String)dataRow["ARP_CRP_Name"];
			entity.YenArpName = (Convert.IsDBNull(dataRow["YEN_ARP_Name"]))?string.Empty:(System.String)dataRow["YEN_ARP_Name"];
			entity.YenCrpName = (Convert.IsDBNull(dataRow["YEN_CRP_Name"]))?string.Empty:(System.String)dataRow["YEN_CRP_Name"];
			entity.RequestingCompanyId = (Convert.IsDBNull(dataRow["RequestingCompanyId"]))?(int)0:(System.Int32?)dataRow["RequestingCompanyId"];
			entity.RequestingCompanyName = (Convert.IsDBNull(dataRow["RequestingCompanyName"]))?string.Empty:(System.String)dataRow["RequestingCompanyName"];
			entity.ReasonContractor = (Convert.IsDBNull(dataRow["ReasonContractor"]))?string.Empty:(System.String)dataRow["ReasonContractor"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportOverviewFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewFilterBuilder : SqlFilterBuilder<QuestionnaireReportOverviewColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewFilterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewFilterBuilder

	#region QuestionnaireReportOverviewParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportOverviewColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewParameterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewParameterBuilder
	
	#region QuestionnaireReportOverviewSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportOverviewSortBuilder : SqlSortBuilder<QuestionnaireReportOverviewColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportOverviewSortBuilder

} // end namespace
