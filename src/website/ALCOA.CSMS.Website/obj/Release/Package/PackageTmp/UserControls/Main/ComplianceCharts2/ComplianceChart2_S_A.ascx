﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Main_ComplianceCharts2_ComplianceChart2_S_A" Codebehind="ComplianceChart2_S_A.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<div style="width: 900px">
    <div style="text-align: center" align="center">
        <dx:ASPxLabel ID="lblResidential" runat="server" ForeColor="Red" Text="">
        </dx:ASPxLabel>
    </div>
    <br />
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" Width="100%">
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <ContentStyle>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </ContentStyle>
        <TabPages>
            <dx:TabPage Name="tabE" Text="Embedded">
                <TabStyle Font-Bold="True">
                </TabStyle>
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server">
                        <div style="float: right; text-align: right;">
                            <dx:ASPxLabel ID="lblLastViewedAt_E" runat="server" Text="" ForeColor="GrayText" Font-Size="11px">
                            </dx:ASPxLabel>
                        </div>
                        <table width="100%">
                            <tr width="100%">
                                <td style="text-align: center; padding-top: 15px;">
                                    <dxchartsui:WebChartControl ID="wcSpider_E" runat="server" Height="350px" Width="430px"
                                        AppearanceName="Light"  PaletteBaseColorNumber="1"
                                        PaletteName="Custom Green-Red">
                                        <SeriesTemplate  
                                            >
                                            <ViewSerializable>
<cc1:RadarAreaSeriesView HiddenSerializableString="to be serialized" Transparency="0">
                                            </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                            <LabelSerializable>
<cc1:RadarPointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True">
                                                <FillStyle >
                                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                </FillStyle>
                                            </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</PointOptionsSerializable>
                                            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                        </SeriesTemplate>
                                        <DiagramSerializable>
<cc1:RadarDiagram DrawingStyle="Polygon" RotationDirection="Clockwise">
                                            <axisy>
<range auto="False" maxvalueserializable="100" minvalueserializable="0" sidemarginsenabled="True"></range>
</axisy>
                                        </cc1:RadarDiagram>
</DiagramSerializable>
                                        <FillStyle FillMode="Empty">
                                        </FillStyle>
                                        <BorderOptions Visible="False" Color="Black" />
                                        <Legend Visible="False"></Legend>
                                        <SeriesSerializable>
                                            <cc1:Series  Name="Series 1" 
                                                >
                                                <ViewSerializable>
<cc1:RadarAreaSeriesView ColorEach="True" HiddenSerializableString="to be serialized" Transparency="0">
                                                </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                                <LabelSerializable>
<cc1:RadarPointSeriesLabel Antialiasing="True" HiddenSerializableString="to be serialized" LineVisible="True"
                                                    Visible="False" ResolveOverlappingMode="JustifyAllAroundPoint">
                                                    <FillStyle >
                                                        <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                    </FillStyle>
                                                </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                                <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</PointOptionsSerializable>
                                                <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                            </cc1:Series>
                                        </SeriesSerializable>
                                        <PaletteWrappers>
                                            <dxchartsui:PaletteWrapper Name="Custom Green-Red" ScaleMode="Repeat">
                                                <Palette>
                                                    <cc1:PaletteEntry Color="Lime" Color2="Lime" />
                                                </Palette>
                                            </dxchartsui:PaletteWrapper>
                                        </PaletteWrappers>
                                    </dxchartsui:WebChartControl>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="grid_E" runat="server" AutoGenerateColumns="False" 
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" Width="100%">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblGrid2_E" runat="server" Font-Bold="True" ForeColor="Red" 
                                            Text="YTD Averages:">
                                        </dx:ASPxLabel>
                                        <dx:ASPxGridView ID="grid2_E" runat="server" AutoGenerateColumns="False" 
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue" OnHtmlRowCreated="grid2_E_HtmlRowCreated" 
                                            Width="100%">
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <SettingsPager PageSize="30">
                                                <AllButton Visible="True"></AllButton>
                                            </SettingsPager>
                                            <Settings ShowFooter="True" />
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                                CssPostfix="Office2003Blue">
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                        </dx:ASPxGridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Enabled="True" Name="tabNE1" Text="Non-Embedded 1">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <div style="float: right; text-align: right;">
                            <dx:ASPxLabel ID="lblLastViewedAt_NE1" runat="server" Text="" ForeColor="GrayText" Font-Size="11px">
                            </dx:ASPxLabel>
                        </div>
                        <table width="100%">
                            <tr width="100%">
                                <td style="text-align: center; padding-top: 15px;">
                                    <dxchartsui:WebChartControl ID="wcSpider_NE1" runat="server" Height="350px" Width="430px"
                                        AppearanceName="Light"  PaletteBaseColorNumber="1"
                                        PaletteName="Custom Green-Red">
                                        <SeriesTemplate  
                                            >
                                            <ViewSerializable>
<cc1:RadarAreaSeriesView HiddenSerializableString="to be serialized" Transparency="0">
                                            </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                            <LabelSerializable>
<cc1:RadarPointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True">
                                                <FillStyle >
                                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                </FillStyle>
                                            </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</PointOptionsSerializable>
                                            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                        </SeriesTemplate>
                                        <DiagramSerializable>
<cc1:RadarDiagram DrawingStyle="Polygon" RotationDirection="Clockwise">
                                            <axisy>
<range auto="False" maxvalueserializable="100" minvalueserializable="0" sidemarginsenabled="True"></range>
</axisy>
                                        </cc1:RadarDiagram>
</DiagramSerializable>
                                        <FillStyle FillMode="Empty">
                                        </FillStyle>
                                        <BorderOptions Visible="False" Color="Black" />
                                        <Legend Visible="False"></Legend>
                                        <SeriesSerializable>
                                            <cc1:Series  Name="Series 1" 
                                                >
                                                <ViewSerializable>
<cc1:RadarAreaSeriesView ColorEach="True" HiddenSerializableString="to be serialized" Transparency="0">
                                                </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                                <LabelSerializable>
<cc1:RadarPointSeriesLabel Antialiasing="True" HiddenSerializableString="to be serialized" LineVisible="True"
                                                    Visible="False" ResolveOverlappingMode="JustifyAllAroundPoint">
                                                    <FillStyle >
                                                        <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                    </FillStyle>
                                                </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                                <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</PointOptionsSerializable>
                                                <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                            </cc1:Series>
                                        </SeriesSerializable>
                                        <PaletteWrappers>
                                            <dxchartsui:PaletteWrapper Name="Custom Green-Red" ScaleMode="Repeat">
                                                <Palette>
                                                    <cc1:PaletteEntry Color="Lime" Color2="Lime" />
                                                </Palette>
                                            </dxchartsui:PaletteWrapper>
                                        </PaletteWrappers>
                                    </dxchartsui:WebChartControl>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="grid_NE1" runat="server" AutoGenerateColumns="False" 
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" Width="100%">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblGrid2_NE1" runat="server" Font-Bold="True" ForeColor="Red" 
                                            Text="YTD Averages:">
                                        </dx:ASPxLabel>
                                        <dx:ASPxGridView ID="grid2_NE1" runat="server" AutoGenerateColumns="False" 
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue" OnHtmlRowCreated="grid2_NE1_HtmlRowCreated" 
                                            Width="100%">
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <SettingsPager PageSize="30">
                                                <AllButton Visible="True"></AllButton>
                                            </SettingsPager>
                                            <Settings ShowFooter="True" />
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                                CssPostfix="Office2003Blue">
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                        </dx:ASPxGridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Enabled="True" Text="Non-Embedded 2">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                    <div style="float: right; text-align: right;">
                            <dx:ASPxLabel ID="lblLastViewedAt_NE2" runat="server" Text="" ForeColor="GrayText" Font-Size="11px">
                            </dx:ASPxLabel>
                        </div>
                        <table width="100%">
                            <tr width="100%">
                                <td style="text-align: center; padding-top: 15px;">
                                    <dxchartsui:WebChartControl ID="wcSpider_NE2" runat="server" Height="350px" Width="430px"
                                        AppearanceName="Light"  PaletteBaseColorNumber="1"
                                        PaletteName="Custom Green-Red">
                                        <SeriesTemplate  
                                            >
                                            <ViewSerializable>
<cc1:RadarAreaSeriesView HiddenSerializableString="to be serialized" Transparency="0">
                                            </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                            <LabelSerializable>
<cc1:RadarPointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True">
                                                <FillStyle >
                                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                </FillStyle>
                                            </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</PointOptionsSerializable>
                                            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                        </SeriesTemplate>
                                        <DiagramSerializable>
<cc1:RadarDiagram DrawingStyle="Polygon" RotationDirection="Clockwise">
                                            <axisy>
<range auto="False" maxvalueserializable="100" minvalueserializable="0" sidemarginsenabled="True"></range>
</axisy>
                                        </cc1:RadarDiagram>
</DiagramSerializable>
                                        <FillStyle FillMode="Empty">
                                        </FillStyle>
                                        <BorderOptions Visible="False" Color="Black" />
                                        <Legend Visible="False"></Legend>
                                        <SeriesSerializable>
                                            <cc1:Series  Name="Series 1" 
                                                >
                                                <ViewSerializable>
<cc1:RadarAreaSeriesView ColorEach="True" HiddenSerializableString="to be serialized" Transparency="0">
                                                </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                                <LabelSerializable>
<cc1:RadarPointSeriesLabel Antialiasing="True" HiddenSerializableString="to be serialized" LineVisible="True"
                                                    Visible="False" ResolveOverlappingMode="JustifyAllAroundPoint">
                                                    <FillStyle >
                                                        <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                    </FillStyle>
                                                </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                                <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</PointOptionsSerializable>
                                                <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                            </cc1:Series>
                                        </SeriesSerializable>
                                        <PaletteWrappers>
                                            <dxchartsui:PaletteWrapper Name="Custom Green-Red" ScaleMode="Repeat">
                                                <Palette>
                                                    <cc1:PaletteEntry Color="Lime" Color2="Lime" />
                                                </Palette>
                                            </dxchartsui:PaletteWrapper>
                                        </PaletteWrappers>
                                    </dxchartsui:WebChartControl>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="grid_NE2" runat="server" AutoGenerateColumns="False" 
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" Width="100%">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblGrid2_NE2" runat="server" Font-Bold="True" ForeColor="Red" 
                                            Text="YTD Averages:">
                                        </dx:ASPxLabel>
                                        <dx:ASPxGridView ID="grid2_NE2" runat="server" AutoGenerateColumns="False" 
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue" OnHtmlRowCreated="grid2_NE2_HtmlRowCreated" 
                                            Width="100%">
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <SettingsPager PageSize="30">
                                                <AllButton Visible="True"></AllButton>
                                            </SettingsPager>
                                            <Settings ShowFooter="True" />
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                                CssPostfix="Office2003Blue">
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                        </dx:ASPxGridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Enabled="True" Text="Non-Embedded 3">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                    <div style="float: right; text-align: right;">
                            <dx:ASPxLabel ID="lblLastViewedAt_NE3" runat="server" Text="" ForeColor="GrayText" Font-Size="11px">
                            </dx:ASPxLabel>
                        </div>
                        <table width="100%">
                            <tr width="100%">
                                <td style="text-align: center; padding-top: 15px;">
                                    <dxchartsui:WebChartControl ID="wcSpider_NE3" runat="server" Height="350px" Width="430px"
                                        AppearanceName="Light"  PaletteBaseColorNumber="1"
                                        PaletteName="Custom Green-Red">
                                        <SeriesTemplate  
                                            >
                                            <ViewSerializable>
<cc1:RadarAreaSeriesView HiddenSerializableString="to be serialized" Transparency="0">
                                            </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                            <LabelSerializable>
<cc1:RadarPointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True">
                                                <FillStyle >
                                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                </FillStyle>
                                            </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                            <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</PointOptionsSerializable>
                                            <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                            </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                        </SeriesTemplate>
                                        <DiagramSerializable>
<cc1:RadarDiagram DrawingStyle="Polygon" RotationDirection="Clockwise">
                                            <axisy>
<range auto="False" maxvalueserializable="100" minvalueserializable="0" sidemarginsenabled="True"></range>
</axisy>
                                        </cc1:RadarDiagram>
</DiagramSerializable>
                                        <FillStyle FillMode="Empty">
                                        </FillStyle>
                                        <BorderOptions Visible="False" Color="Black" />
                                        <Legend Visible="False"></Legend>
                                        <SeriesSerializable>
                                            <cc1:Series  Name="Series 1" 
                                                >
                                                <ViewSerializable>
<cc1:RadarAreaSeriesView ColorEach="True" HiddenSerializableString="to be serialized" Transparency="0">
                                                </cc1:RadarAreaSeriesView>
</ViewSerializable>
                                                <LabelSerializable>
<cc1:RadarPointSeriesLabel Antialiasing="True" HiddenSerializableString="to be serialized" LineVisible="True"
                                                    Visible="False" ResolveOverlappingMode="JustifyAllAroundPoint">
                                                    <FillStyle >
                                                        <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                                    </FillStyle>
                                                </cc1:RadarPointSeriesLabel>
</LabelSerializable>
                                                <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</PointOptionsSerializable>
                                                <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                                </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                            </cc1:Series>
                                        </SeriesSerializable>
                                        <PaletteWrappers>
                                            <dxchartsui:PaletteWrapper Name="Custom Green-Red" ScaleMode="Repeat">
                                                <Palette>
                                                    <cc1:PaletteEntry Color="Lime" Color2="Lime" />
                                                </Palette>
                                            </dxchartsui:PaletteWrapper>
                                        </PaletteWrappers>
                                    </dxchartsui:WebChartControl>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dx:ASPxGridView ID="grid_NE3" runat="server" AutoGenerateColumns="False" 
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                        CssPostfix="Office2003Blue" Width="100%">
                                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                            </LoadingPanelOnStatusBar>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                            </LoadingPanel>
                                        </Images>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <SettingsPager PageSize="30">
                                            <AllButton Visible="True"></AllButton>
                                        </SettingsPager>
                                        <ImagesFilterControl>
                                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                            </LoadingPanel>
                                        </ImagesFilterControl>
                                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue">
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                            </Header>
                                            <LoadingPanel ImageSpacing="10px">
                                            </LoadingPanel>
                                        </Styles>
                                        <StylesEditors>
                                            <ProgressBar Height="25px">
                                            </ProgressBar>
                                        </StylesEditors>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                                        <dx:ASPxLabel ID="lblGrid2_NE3" runat="server" Font-Bold="True" ForeColor="Red" 
                                            Text="YTD Averages:">
                                        </dx:ASPxLabel>
                                        <dx:ASPxGridView ID="grid2_NE3" runat="server" AutoGenerateColumns="False" 
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                            CssPostfix="Office2003Blue" OnHtmlRowCreated="grid2_NE3_HtmlRowCreated" 
                                            Width="100%">
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                            <SettingsPager PageSize="30">
                                                <AllButton Visible="True"></AllButton>
                                            </SettingsPager>
                                            <Settings ShowFooter="True" />
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                                                CssPostfix="Office2003Blue">
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                        </dx:ASPxGridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <LoadingPanelStyle ImageSpacing="6px">
        </LoadingPanelStyle>
    </dx:ASPxPageControl>
</div>
