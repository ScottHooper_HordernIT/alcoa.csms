﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdHocRadarItemsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdHocRadarItemsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdHocRadarItems, KaiZen.CSMS.Entities.AdHocRadarItemsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItemsKey key)
		{
			return Delete(transactionManager, key.AdHocRadarItemId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adHocRadarItemId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adHocRadarItemId)
		{
			return Delete(null, _adHocRadarItemId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItemId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adHocRadarItemId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetByAdHocRadarId(System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByAdHocRadarId(_adHocRadarId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		/// <remarks></remarks>
		public TList<AdHocRadarItems> GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByAdHocRadarId(transactionManager, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarId(transactionManager, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_AdHoc_Radar key.
		///		fkAdHocRadarItemsAdHocRadar Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adHocRadarId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetByAdHocRadarId(System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdHocRadarId(null, _adHocRadarId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_AdHoc_Radar key.
		///		fkAdHocRadarItemsAdHocRadar Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetByAdHocRadarId(System.Int32 _adHocRadarId, int start, int pageLength,out int count)
		{
			return GetByAdHocRadarId(null, _adHocRadarId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_AdHoc_Radar key.
		///		FK_AdHoc_Radar_Items_AdHoc_Radar Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public abstract TList<AdHocRadarItems> GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_Sites key.
		///		FK_AdHoc_Radar_Items_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_Sites key.
		///		FK_AdHoc_Radar_Items_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		/// <remarks></remarks>
		public TList<AdHocRadarItems> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_Sites key.
		///		FK_AdHoc_Radar_Items_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_Sites key.
		///		fkAdHocRadarItemsSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_Sites key.
		///		fkAdHocRadarItemsSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public TList<AdHocRadarItems> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_Items_Sites key.
		///		FK_AdHoc_Radar_Items_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadarItems objects.</returns>
		public abstract TList<AdHocRadarItems> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdHocRadarItems Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItemsKey key, int start, int pageLength)
		{
			return GetByAdHocRadarItemId(transactionManager, key.AdHocRadarItemId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="_adHocRadarItemId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByAdHocRadarItemId(System.Int32 _adHocRadarItemId)
		{
			int count = -1;
			return GetByAdHocRadarItemId(null,_adHocRadarItemId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="_adHocRadarItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByAdHocRadarItemId(System.Int32 _adHocRadarItemId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarItemId(null, _adHocRadarItemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItemId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByAdHocRadarItemId(TransactionManager transactionManager, System.Int32 _adHocRadarItemId)
		{
			int count = -1;
			return GetByAdHocRadarItemId(transactionManager, _adHocRadarItemId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByAdHocRadarItemId(TransactionManager transactionManager, System.Int32 _adHocRadarItemId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarItemId(transactionManager, _adHocRadarItemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="_adHocRadarItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByAdHocRadarItemId(System.Int32 _adHocRadarItemId, int start, int pageLength, out int count)
		{
			return GetByAdHocRadarItemId(null, _adHocRadarItemId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdHocRadarItems GetByAdHocRadarItemId(TransactionManager transactionManager, System.Int32 _adHocRadarItemId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByYearAdHocRadarIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(null,_year, _adHocRadarId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByYearAdHocRadarIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(null, _year, _adHocRadarId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByYearAdHocRadarIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(transactionManager, _year, _adHocRadarId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByYearAdHocRadarIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarIdSiteId(transactionManager, _year, _adHocRadarId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadarItems GetByYearAdHocRadarIdSiteId(System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByYearAdHocRadarIdSiteId(null, _year, _adHocRadarId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdHocRadarItems GetByYearAdHocRadarIdSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar_Items_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems&gt;"/> class.</returns>
		public TList<AdHocRadarItems> GetByYearAdHocRadarId(System.Int32 _year, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByYearAdHocRadarId(null,_year, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems&gt;"/> class.</returns>
		public TList<AdHocRadarItems> GetByYearAdHocRadarId(System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarId(null, _year, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems&gt;"/> class.</returns>
		public TList<AdHocRadarItems> GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByYearAdHocRadarId(transactionManager, _year, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems&gt;"/> class.</returns>
		public TList<AdHocRadarItems> GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearAdHocRadarId(transactionManager, _year, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems&gt;"/> class.</returns>
		public TList<AdHocRadarItems> GetByYearAdHocRadarId(System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength, out int count)
		{
			return GetByYearAdHocRadarId(null, _year, _adHocRadarId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar_Items_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdHocRadarItems&gt;"/> class.</returns>
		public abstract TList<AdHocRadarItems> GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _adHocRadarId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdHocRadarItems&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdHocRadarItems&gt;"/></returns>
		public static TList<AdHocRadarItems> Fill(IDataReader reader, TList<AdHocRadarItems> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdHocRadarItems c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdHocRadarItems")
					.Append("|").Append((System.Int32)reader[((int)AdHocRadarItemsColumn.AdHocRadarItemId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdHocRadarItems>(
					key.ToString(), // EntityTrackingKey
					"AdHocRadarItems",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdHocRadarItems();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdHocRadarItemId = (System.Int32)reader[((int)AdHocRadarItemsColumn.AdHocRadarItemId - 1)];
					c.AdHocRadarId = (System.Int32)reader[((int)AdHocRadarItemsColumn.AdHocRadarId - 1)];
					c.Year = (System.Int32)reader[((int)AdHocRadarItemsColumn.Year - 1)];
					c.SiteId = (System.Int32)reader[((int)AdHocRadarItemsColumn.SiteId - 1)];
					c.KpiScoreYtd = (System.String)reader[((int)AdHocRadarItemsColumn.KpiScoreYtd - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)AdHocRadarItemsColumn.ModifiedDate - 1)];
					c.CompaniesNotCompliant = (reader.IsDBNull(((int)AdHocRadarItemsColumn.CompaniesNotCompliant - 1)))?null:(System.String)reader[((int)AdHocRadarItemsColumn.CompaniesNotCompliant - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdHocRadarItems entity)
		{
			if (!reader.Read()) return;
			
			entity.AdHocRadarItemId = (System.Int32)reader[((int)AdHocRadarItemsColumn.AdHocRadarItemId - 1)];
			entity.AdHocRadarId = (System.Int32)reader[((int)AdHocRadarItemsColumn.AdHocRadarId - 1)];
			entity.Year = (System.Int32)reader[((int)AdHocRadarItemsColumn.Year - 1)];
			entity.SiteId = (System.Int32)reader[((int)AdHocRadarItemsColumn.SiteId - 1)];
			entity.KpiScoreYtd = (System.String)reader[((int)AdHocRadarItemsColumn.KpiScoreYtd - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)AdHocRadarItemsColumn.ModifiedDate - 1)];
			entity.CompaniesNotCompliant = (reader.IsDBNull(((int)AdHocRadarItemsColumn.CompaniesNotCompliant - 1)))?null:(System.String)reader[((int)AdHocRadarItemsColumn.CompaniesNotCompliant - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdHocRadarItems entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdHocRadarItemId = (System.Int32)dataRow["AdHoc_Radar_ItemId"];
			entity.AdHocRadarId = (System.Int32)dataRow["AdHoc_RadarId"];
			entity.Year = (System.Int32)dataRow["Year"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.KpiScoreYtd = (System.String)dataRow["KpiScoreYtd"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.CompaniesNotCompliant = Convert.IsDBNull(dataRow["CompaniesNotCompliant"]) ? null : (System.String)dataRow["CompaniesNotCompliant"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadarItems"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdHocRadarItems Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItems entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdHocRadarIdSource	
			if (CanDeepLoad(entity, "AdHocRadar|AdHocRadarIdSource", deepLoadType, innerList) 
				&& entity.AdHocRadarIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdHocRadarId;
				AdHocRadar tmpEntity = EntityManager.LocateEntity<AdHocRadar>(EntityLocator.ConstructKeyFromPkItems(typeof(AdHocRadar), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdHocRadarIdSource = tmpEntity;
				else
					entity.AdHocRadarIdSource = DataRepository.AdHocRadarProvider.GetByAdHocRadarId(transactionManager, entity.AdHocRadarId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdHocRadarIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdHocRadarProvider.DeepLoad(transactionManager, entity.AdHocRadarIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdHocRadarIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdHocRadarItems object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdHocRadarItems instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdHocRadarItems Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarItems entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdHocRadarIdSource
			if (CanDeepSave(entity, "AdHocRadar|AdHocRadarIdSource", deepSaveType, innerList) 
				&& entity.AdHocRadarIdSource != null)
			{
				DataRepository.AdHocRadarProvider.Save(transactionManager, entity.AdHocRadarIdSource);
				entity.AdHocRadarId = entity.AdHocRadarIdSource.AdHocRadarId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdHocRadarItemsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdHocRadarItems</c>
	///</summary>
	public enum AdHocRadarItemsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdHocRadar</c> at AdHocRadarIdSource
		///</summary>
		[ChildEntityType(typeof(AdHocRadar))]
		AdHocRadar,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion AdHocRadarItemsChildEntityTypes
	
	#region AdHocRadarItemsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdHocRadarItemsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsFilterBuilder : SqlFilterBuilder<AdHocRadarItemsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsFilterBuilder class.
		/// </summary>
		public AdHocRadarItemsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItemsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItemsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItemsFilterBuilder
	
	#region AdHocRadarItemsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdHocRadarItemsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsParameterBuilder : ParameterizedSqlFilterBuilder<AdHocRadarItemsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsParameterBuilder class.
		/// </summary>
		public AdHocRadarItemsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarItemsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarItemsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarItemsParameterBuilder
	
	#region AdHocRadarItemsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdHocRadarItemsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdHocRadarItemsSortBuilder : SqlSortBuilder<AdHocRadarItemsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsSqlSortBuilder class.
		/// </summary>
		public AdHocRadarItemsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdHocRadarItemsSortBuilder
	
} // end namespace
