﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_UsersFinancialReportingAccess" Codebehind="UsersFinancialReportingAccess.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<strong>Financial Reporting Access</strong>
<br />
- Users in this table are granted privileged
access to confidential Financial Reporting
information.<br />
    
<dxwgv:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    DataSourceID="SqlDataSource1" KeyFieldName="UsersPrivilegedId" 
    Width="900px" OnRowUpdating="grid_RowUpdating" OnRowInserting="grid_RowInserting">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="120px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="true"></ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="UsersPrivilegedId" ReadOnly="True" Visible="False"
            VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="User" FieldName="UserId" VisibleIndex="1" Settings-SortMode="DisplayText" SortOrder="Ascending" SortIndex="0">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" TextField="UserDetails"
                ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditItemTemplate>
            <dxe:ASPxComboBox ID="usersp1" runat="server" DataSourceID="SqlDataSource2"  TextField="UserDetails" ValueField="UserId" ValueType="System.Int32" Value='<%# Eval("UserId")%>' IncrementalFilteringMode="StartsWith" Width="400px"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxComboBox>
            </EditItemTemplate>
        </dxwgv:GridViewDataComboBoxColumn>
    </Columns>
    <SettingsPager NumericButtonCount="5" PageSize="5" AlwaysShowPager="True">
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <Settings ShowFilterRow="true"  />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    DeleteCommand="DELETE FROM [UsersPrivileged] WHERE [UsersPrivilegedId] = @UsersPrivilegedId"
    InsertCommand="INSERT INTO [UsersPrivileged] ([UserId]) VALUES (@UserId)" 
    SelectCommand="SELECT * FROM [UsersPrivileged] where [UserId] in (select [UserId] from [Users] where [RoleId] < 3)"
    UpdateCommand="UPDATE [UsersPrivileged] SET [UserId] = @UserId WHERE [UsersPrivilegedId] = @UsersPrivilegedId">
    <DeleteParameters>
        <asp:Parameter Name="UsersPrivilegedId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
        <asp:Parameter Name="UsersPrivilegedId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
<asp:SqlDataSource
	ID="UsersDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ') (' + dbo.Companies.CompanyName + ')' As UserDetails FROM dbo.Users INNER JOIN dbo.Companies ON dbo.Users.CompanyId = dbo.Companies.CompanyID AND (dbo.Users.RoleId < 3) AND dbo.Users.UserId in (select Distinct UserId from dbo.UsersPrivileged) ORDER BY dbo.Users.LastName">
</asp:SqlDataSource>
<asp:SqlDataSource
	ID="SqlDataSource2"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ') (' + dbo.Companies.CompanyName + ')' As UserDetails FROM dbo.Users INNER JOIN dbo.Companies ON dbo.Users.CompanyId = dbo.Companies.CompanyID AND (dbo.Users.RoleId < 3) ORDER BY dbo.Users.LastName">
</asp:SqlDataSource>