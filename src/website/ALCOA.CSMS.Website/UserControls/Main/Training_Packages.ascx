﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Training_Packages" Codebehind="Training_Packages.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

    <script type="text/javascript">
    function OnUpdateClick()
    {
          var theText = uploader.GetText();
          var theText2 = uploader2.GetText();

          if (theText != "") {
              uploader.UploadFile();
          }
          if (theText2 != "")
          {
              uploader2.UploadFile();
          }

          grid.UpdateEdit();
        }
    </script>

<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <span class="bodycopy"><span class="title">Training<asp:Button ID="btnEdit" runat="server"
                BackColor="White" BorderColor="White" BorderStyle="None" Font-Names="Verdana"
                Font-Size="XX-Small" Font-Underline="True" ForeColor="Red" OnClick="btnEdit_Click"
                Text="(Edit)" /></span><br />
                <span class="date">Packages</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: left; height: 176px;">
            <dxwgv:aspxgridview id="grid" runat="server" autogeneratecolumns="False"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
                datasourceid="DocumentsDataSource" ClientInstanceName="grid"
                OnRowInserting="grid_OnRowInserting"
                OnHtmlRowCreated="grid_RowCreated" OnInitNewRow="grid_InitNewRow"
                OnRowUpdating="grid_RowUpdating" KeyFieldName="DocumentId" Width="900px">

                <Templates>
                    <EditForm>
                        <table border="0" cellpadding="0" cellspacing="0" style="width:700px">
                        <tr>
                            <td width="200">
                                Training Category:</td>
                            <td style="width: 500px">
                                <dxe:ASPxTextBox ID="tbRootNode" runat="server"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Text=<%# Eval("RootNode") %>
                                    CssPostfix="Office2003Blue" Width="500px"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">
                                Training Frequency:</td>
                            <td style="width: 500px">
                                <dxe:ASPxTextBox ID="tbParentNode" runat="server"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Text=<%# Eval("ParentNode") %>
                                    CssPostfix="Office2003Blue" Width="500px"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">
                                Training for...:</td>
                            <td style="width: 500px">
                                <dxe:ASPxTextBox ID="tbChildNode" runat="server"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Text=<%# Eval("ChildNode") %>
                                    CssPostfix="Office2003Blue" Width="500px"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">
                                Training Package Name:</td>
                            <td style="width: 500px">
                                <dxe:ASPxTextBox ID="tbDocumentName" runat="server"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Text=<%# Eval("DocumentName") %>
                                    CssPostfix="Office2003Blue" Width="500px"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">
                                Training Package File Name:</td>
                            <td style="width: 500px">
                                <dxe:ASPxTextBox ID="tbDocumentFileName" runat="server"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Text=<%# Eval("DocumentFileName") %>
                                    CssPostfix="Office2003Blue" Width="500px"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">
                                Upload Questionnaire:</td>
                            <td style="width: 500px;">
                                <dxuc:ASPxUploadControl runat="server" ClientInstanceName="uploader" ID="ASPxUploadControl1"
                                    OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" Width="500px">
                                </dxuc:ASPxUploadControl>
                            </td>
                        </tr>
                        <tr>
                            <td width="200">
                                Upload Answers:</td>
                            <td style="width: 500px;">
                                <dxuc:ASPxUploadControl runat="server" ClientInstanceName="uploader2" ID="ASPxUploadControl2"
                                    OnFileUploadComplete="ASPxUploadControl2_FileUploadComplete" Width="500px">
                                    <ClientSideEvents FileUploadComplete="function(s, e) { if (e.isValid) { grid.UpdateEdit(); }}" />
                                </dxuc:ASPxUploadControl>
                            </td>
                        </tr>
                        </table>
                        <br />
                        <div style="text-align: right; padding: 2px 2px 2px 2px">
                            <a href="#" onclick="OnUpdateClick()">Update</a>
                            <dxwgv:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton" runat="server">
                            </dxwgv:ASPxGridViewTemplateReplacement>
                        </div>
                    </EditForm>
                </Templates>

                <Columns>
                    <dxwgv:GridViewCommandColumn Caption="Action" Visible="False" VisibleIndex="0" Width="50px">
                        <EditButton Visible="True">
                        </EditButton>
                        <NewButton Visible="True">
                        </NewButton>
                        <DeleteButton Visible="True">
                        </DeleteButton>
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentId" ReadOnly="True" Visible="False"
                        VisibleIndex="0">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption=" " FieldName="RootNode" GroupIndex="0" SortIndex="0"
                        SortOrder="Ascending" VisibleIndex="1">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption=" " FieldName="ParentNode" GroupIndex="1" SortIndex="1"
                        SortOrder="Ascending" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption=" " FieldName="ChildNode" GroupIndex="2" SortIndex="2"
                        SortOrder="Ascending" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataHyperLinkColumn Caption="File Name" FieldName="DocumentFileName"
                        ToolTip="Click to download file." VisibleIndex="4" Width="100px">
                        <DataItemTemplate>
                            <asp:HyperLink ID="hlF" runat="server" Text="" Width="100px" ToolTip="Click to download file."></asp:HyperLink>
                        </DataItemTemplate>
                    </dxwgv:GridViewDataHyperLinkColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Training Package Name" FieldName="DocumentName"
                        SortIndex="3" SortOrder="Ascending" VisibleIndex="0">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileDescription" Visible="False"
                        VisibleIndex="4">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentType" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="UploadedFileName" Width="1px" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="UploadedFileName2" Width="1px" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataHyperLinkColumn Caption="Questionnnaire" FieldName="DocumentId" VisibleIndex="5" Width="90px">
                   <DataItemTemplate>
                        <asp:HyperLink ID="hlQ" runat="server" Text="" Width="90px" ToolTip="Click to download file."></asp:HyperLink>
                 </DataItemTemplate>
                 <CellStyle ForeColor="Orange" HorizontalAlign="Left">
                                </CellStyle>
                </dxwgv:GridViewDataHyperLinkColumn>

                <dxwgv:GridViewDataHyperLinkColumn Caption="Answers" FieldName="DocumentId" VisibleIndex="6" Width="90px">
                   <DataItemTemplate>
                        <asp:HyperLink ID="hlA" runat="server" Text="" Width="90px" ToolTip="Click to download file."></asp:HyperLink>
                 </DataItemTemplate>
                 <CellStyle ForeColor="Orange" HorizontalAlign="Left">
                                </CellStyle>
                </dxwgv:GridViewDataHyperLinkColumn>

</Columns>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
</Images>

                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>

<Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header ImageSpacing="5px" SortingImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
    <GroupRow Font-Bold="True" Font-Size="Medium">
    </GroupRow>
</Styles>
                <SettingsPager PageSize="100">
                    <AllButton Visible="True"></AllButton>
                </SettingsPager>
                <SettingsBehavior ConfirmDelete="True" />
                <Settings GroupFormat="{0} {1} {2}" />
                <ClientSideEvents ContextMenu="function(s, e) {
                if (expanded == true)
                {
                    grid.CollapseAll();
                    expanded = false;
                }
                else
                {
	                grid.ExpandAll();
	                expanded = true;
	            }
}" />

</dxwgv:aspxgridview>

    </tr>
</table>

<data:DocumentsDataSource ID="DocumentsDataSource" runat="server" EnableDeepLoad="False"
                    EnableSorting="true" SelectMethod="GetPaged"  Sort="RootNode">
    <Parameters>
       <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
          <Filters>
             <data:DocumentsFilter Column="DocumentType" DefaultValue="TP" />
          </Filters>
       </data:SqlParameter>
    </Parameters>

</data:DocumentsDataSource>
<br />
