﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileDbAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileDbAuditDataSourceDesigner))]
	public class FileDbAuditDataSource : ProviderDataSource<FileDbAudit, FileDbAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditDataSource class.
		/// </summary>
		public FileDbAuditDataSource() : base(new FileDbAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileDbAuditDataSourceView used by the FileDbAuditDataSource.
		/// </summary>
		protected FileDbAuditDataSourceView FileDbAuditView
		{
			get { return ( View as FileDbAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileDbAuditDataSource control invokes to retrieve data.
		/// </summary>
		public FileDbAuditSelectMethod SelectMethod
		{
			get
			{
				FileDbAuditSelectMethod selectMethod = FileDbAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileDbAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileDbAuditDataSourceView class that is to be
		/// used by the FileDbAuditDataSource.
		/// </summary>
		/// <returns>An instance of the FileDbAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<FileDbAudit, FileDbAuditKey> GetNewDataSourceView()
		{
			return new FileDbAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileDbAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileDbAuditDataSourceView : ProviderDataSourceView<FileDbAudit, FileDbAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileDbAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileDbAuditDataSourceView(FileDbAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileDbAuditDataSource FileDbAuditOwner
		{
			get { return Owner as FileDbAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileDbAuditSelectMethod SelectMethod
		{
			get { return FileDbAuditOwner.SelectMethod; }
			set { FileDbAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileDbAuditService FileDbAuditProvider
		{
			get { return Provider as FileDbAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileDbAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileDbAudit> results = null;
			FileDbAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case FileDbAuditSelectMethod.Get:
					FileDbAuditKey entityKey  = new FileDbAuditKey();
					entityKey.Load(values);
					item = FileDbAuditProvider.Get(entityKey);
					results = new TList<FileDbAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileDbAuditSelectMethod.GetAll:
                    results = FileDbAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileDbAuditSelectMethod.GetPaged:
					results = FileDbAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileDbAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = FileDbAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileDbAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileDbAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = FileDbAuditProvider.GetByAuditId(_auditId);
					results = new TList<FileDbAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileDbAuditSelectMethod.Get || SelectMethod == FileDbAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileDbAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileDbAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileDbAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileDbAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileDbAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileDbAuditDataSource class.
	/// </summary>
	public class FileDbAuditDataSourceDesigner : ProviderDataSourceDesigner<FileDbAudit, FileDbAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileDbAuditDataSourceDesigner class.
		/// </summary>
		public FileDbAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbAuditSelectMethod SelectMethod
		{
			get { return ((FileDbAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileDbAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileDbAuditDataSourceActionList

	/// <summary>
	/// Supports the FileDbAuditDataSourceDesigner class.
	/// </summary>
	internal class FileDbAuditDataSourceActionList : DesignerActionList
	{
		private FileDbAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileDbAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileDbAuditDataSourceActionList(FileDbAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileDbAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileDbAuditDataSourceActionList
	
	#endregion FileDbAuditDataSourceDesigner
	
	#region FileDbAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileDbAuditDataSource.SelectMethod property.
	/// </summary>
	public enum FileDbAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion FileDbAuditSelectMethod

	#region FileDbAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditFilter : SqlFilter<FileDbAuditColumn>
	{
	}
	
	#endregion FileDbAuditFilter

	#region FileDbAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditExpressionBuilder : SqlExpressionBuilder<FileDbAuditColumn>
	{
	}
	
	#endregion FileDbAuditExpressionBuilder	

	#region FileDbAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileDbAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditProperty : ChildEntityProperty<FileDbAuditChildEntityTypes>
	{
	}
	
	#endregion FileDbAuditProperty
}

