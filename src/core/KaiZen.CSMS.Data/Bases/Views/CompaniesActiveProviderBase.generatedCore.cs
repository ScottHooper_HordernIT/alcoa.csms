﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesActiveProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompaniesActiveProviderBaseCore : EntityViewProviderBase<CompaniesActive>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompaniesActive&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompaniesActive&gt;"/></returns>
		protected static VList&lt;CompaniesActive&gt; Fill(DataSet dataSet, VList<CompaniesActive> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompaniesActive>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompaniesActive&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompaniesActive>"/></returns>
		protected static VList&lt;CompaniesActive&gt; Fill(DataTable dataTable, VList<CompaniesActive> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompaniesActive c = new CompaniesActive();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.CompanyAbn = (Convert.IsDBNull(row["CompanyAbn"]))?string.Empty:(System.String)row["CompanyAbn"];
					c.PhoneNo = (Convert.IsDBNull(row["PhoneNo"]))?string.Empty:(System.String)row["PhoneNo"];
					c.FaxNo = (Convert.IsDBNull(row["FaxNo"]))?string.Empty:(System.String)row["FaxNo"];
					c.MobNo = (Convert.IsDBNull(row["MobNo"]))?string.Empty:(System.String)row["MobNo"];
					c.AddressBusiness = (Convert.IsDBNull(row["AddressBusiness"]))?string.Empty:(System.String)row["AddressBusiness"];
					c.AddressPostal = (Convert.IsDBNull(row["AddressPostal"]))?string.Empty:(System.String)row["AddressPostal"];
					c.ProcurementSupplierNo = (Convert.IsDBNull(row["ProcurementSupplierNo"]))?string.Empty:(System.String)row["ProcurementSupplierNo"];
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.ModifiedbyUserId = (Convert.IsDBNull(row["ModifiedbyUserId"]))?(int)0:(System.Int32)row["ModifiedbyUserId"];
					c.IprocSupplierNo = (Convert.IsDBNull(row["IprocSupplierNo"]))?string.Empty:(System.String)row["IprocSupplierNo"];
					c.CompanyStatusId = (Convert.IsDBNull(row["CompanyStatusId"]))?(int)0:(System.Int32)row["CompanyStatusId"];
					c.StartDate = (Convert.IsDBNull(row["StartDate"]))?DateTime.MinValue:(System.DateTime?)row["StartDate"];
					c.EndDate = (Convert.IsDBNull(row["EndDate"]))?DateTime.MinValue:(System.DateTime?)row["EndDate"];
					c.CompanyStatus2Id = (Convert.IsDBNull(row["CompanyStatus2Id"]))?(int)0:(System.Int32)row["CompanyStatus2Id"];
					c.Active = (Convert.IsDBNull(row["Active"]))?string.Empty:(System.String)row["Active"];
					c.ExpiresIn = (Convert.IsDBNull(row["ExpiresIn"]))?(int)0:(System.Int32?)row["ExpiresIn"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompaniesActive&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompaniesActive&gt;"/></returns>
		protected VList<CompaniesActive> Fill(IDataReader reader, VList<CompaniesActive> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompaniesActive entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompaniesActive>("CompaniesActive",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompaniesActive();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)CompaniesActiveColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)CompaniesActiveColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.CompanyAbn = (System.String)reader[((int)CompaniesActiveColumn.CompanyAbn)];
					//entity.CompanyAbn = (Convert.IsDBNull(reader["CompanyAbn"]))?string.Empty:(System.String)reader["CompanyAbn"];
					entity.PhoneNo = (reader.IsDBNull(((int)CompaniesActiveColumn.PhoneNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.PhoneNo)];
					//entity.PhoneNo = (Convert.IsDBNull(reader["PhoneNo"]))?string.Empty:(System.String)reader["PhoneNo"];
					entity.FaxNo = (reader.IsDBNull(((int)CompaniesActiveColumn.FaxNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.FaxNo)];
					//entity.FaxNo = (Convert.IsDBNull(reader["FaxNo"]))?string.Empty:(System.String)reader["FaxNo"];
					entity.MobNo = (reader.IsDBNull(((int)CompaniesActiveColumn.MobNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.MobNo)];
					//entity.MobNo = (Convert.IsDBNull(reader["MobNo"]))?string.Empty:(System.String)reader["MobNo"];
					entity.AddressBusiness = (reader.IsDBNull(((int)CompaniesActiveColumn.AddressBusiness)))?null:(System.String)reader[((int)CompaniesActiveColumn.AddressBusiness)];
					//entity.AddressBusiness = (Convert.IsDBNull(reader["AddressBusiness"]))?string.Empty:(System.String)reader["AddressBusiness"];
					entity.AddressPostal = (reader.IsDBNull(((int)CompaniesActiveColumn.AddressPostal)))?null:(System.String)reader[((int)CompaniesActiveColumn.AddressPostal)];
					//entity.AddressPostal = (Convert.IsDBNull(reader["AddressPostal"]))?string.Empty:(System.String)reader["AddressPostal"];
					entity.ProcurementSupplierNo = (reader.IsDBNull(((int)CompaniesActiveColumn.ProcurementSupplierNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.ProcurementSupplierNo)];
					//entity.ProcurementSupplierNo = (Convert.IsDBNull(reader["ProcurementSupplierNo"]))?string.Empty:(System.String)reader["ProcurementSupplierNo"];
					entity.EhsConsultantId = (reader.IsDBNull(((int)CompaniesActiveColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)CompaniesActiveColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.ModifiedbyUserId = (System.Int32)reader[((int)CompaniesActiveColumn.ModifiedbyUserId)];
					//entity.ModifiedbyUserId = (Convert.IsDBNull(reader["ModifiedbyUserId"]))?(int)0:(System.Int32)reader["ModifiedbyUserId"];
					entity.IprocSupplierNo = (reader.IsDBNull(((int)CompaniesActiveColumn.IprocSupplierNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.IprocSupplierNo)];
					//entity.IprocSupplierNo = (Convert.IsDBNull(reader["IprocSupplierNo"]))?string.Empty:(System.String)reader["IprocSupplierNo"];
					entity.CompanyStatusId = (System.Int32)reader[((int)CompaniesActiveColumn.CompanyStatusId)];
					//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
					entity.StartDate = (reader.IsDBNull(((int)CompaniesActiveColumn.StartDate)))?null:(System.DateTime?)reader[((int)CompaniesActiveColumn.StartDate)];
					//entity.StartDate = (Convert.IsDBNull(reader["StartDate"]))?DateTime.MinValue:(System.DateTime?)reader["StartDate"];
					entity.EndDate = (reader.IsDBNull(((int)CompaniesActiveColumn.EndDate)))?null:(System.DateTime?)reader[((int)CompaniesActiveColumn.EndDate)];
					//entity.EndDate = (Convert.IsDBNull(reader["EndDate"]))?DateTime.MinValue:(System.DateTime?)reader["EndDate"];
					entity.CompanyStatus2Id = (System.Int32)reader[((int)CompaniesActiveColumn.CompanyStatus2Id)];
					//entity.CompanyStatus2Id = (Convert.IsDBNull(reader["CompanyStatus2Id"]))?(int)0:(System.Int32)reader["CompanyStatus2Id"];
					entity.Active = (System.String)reader[((int)CompaniesActiveColumn.Active)];
					//entity.Active = (Convert.IsDBNull(reader["Active"]))?string.Empty:(System.String)reader["Active"];
					entity.ExpiresIn = (reader.IsDBNull(((int)CompaniesActiveColumn.ExpiresIn)))?null:(System.Int32?)reader[((int)CompaniesActiveColumn.ExpiresIn)];
					//entity.ExpiresIn = (Convert.IsDBNull(reader["ExpiresIn"]))?(int)0:(System.Int32?)reader["ExpiresIn"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompaniesActive"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesActive"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompaniesActive entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)CompaniesActiveColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)CompaniesActiveColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.CompanyAbn = (System.String)reader[((int)CompaniesActiveColumn.CompanyAbn)];
			//entity.CompanyAbn = (Convert.IsDBNull(reader["CompanyAbn"]))?string.Empty:(System.String)reader["CompanyAbn"];
			entity.PhoneNo = (reader.IsDBNull(((int)CompaniesActiveColumn.PhoneNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.PhoneNo)];
			//entity.PhoneNo = (Convert.IsDBNull(reader["PhoneNo"]))?string.Empty:(System.String)reader["PhoneNo"];
			entity.FaxNo = (reader.IsDBNull(((int)CompaniesActiveColumn.FaxNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.FaxNo)];
			//entity.FaxNo = (Convert.IsDBNull(reader["FaxNo"]))?string.Empty:(System.String)reader["FaxNo"];
			entity.MobNo = (reader.IsDBNull(((int)CompaniesActiveColumn.MobNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.MobNo)];
			//entity.MobNo = (Convert.IsDBNull(reader["MobNo"]))?string.Empty:(System.String)reader["MobNo"];
			entity.AddressBusiness = (reader.IsDBNull(((int)CompaniesActiveColumn.AddressBusiness)))?null:(System.String)reader[((int)CompaniesActiveColumn.AddressBusiness)];
			//entity.AddressBusiness = (Convert.IsDBNull(reader["AddressBusiness"]))?string.Empty:(System.String)reader["AddressBusiness"];
			entity.AddressPostal = (reader.IsDBNull(((int)CompaniesActiveColumn.AddressPostal)))?null:(System.String)reader[((int)CompaniesActiveColumn.AddressPostal)];
			//entity.AddressPostal = (Convert.IsDBNull(reader["AddressPostal"]))?string.Empty:(System.String)reader["AddressPostal"];
			entity.ProcurementSupplierNo = (reader.IsDBNull(((int)CompaniesActiveColumn.ProcurementSupplierNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.ProcurementSupplierNo)];
			//entity.ProcurementSupplierNo = (Convert.IsDBNull(reader["ProcurementSupplierNo"]))?string.Empty:(System.String)reader["ProcurementSupplierNo"];
			entity.EhsConsultantId = (reader.IsDBNull(((int)CompaniesActiveColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)CompaniesActiveColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)CompaniesActiveColumn.ModifiedbyUserId)];
			//entity.ModifiedbyUserId = (Convert.IsDBNull(reader["ModifiedbyUserId"]))?(int)0:(System.Int32)reader["ModifiedbyUserId"];
			entity.IprocSupplierNo = (reader.IsDBNull(((int)CompaniesActiveColumn.IprocSupplierNo)))?null:(System.String)reader[((int)CompaniesActiveColumn.IprocSupplierNo)];
			//entity.IprocSupplierNo = (Convert.IsDBNull(reader["IprocSupplierNo"]))?string.Empty:(System.String)reader["IprocSupplierNo"];
			entity.CompanyStatusId = (System.Int32)reader[((int)CompaniesActiveColumn.CompanyStatusId)];
			//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
			entity.StartDate = (reader.IsDBNull(((int)CompaniesActiveColumn.StartDate)))?null:(System.DateTime?)reader[((int)CompaniesActiveColumn.StartDate)];
			//entity.StartDate = (Convert.IsDBNull(reader["StartDate"]))?DateTime.MinValue:(System.DateTime?)reader["StartDate"];
			entity.EndDate = (reader.IsDBNull(((int)CompaniesActiveColumn.EndDate)))?null:(System.DateTime?)reader[((int)CompaniesActiveColumn.EndDate)];
			//entity.EndDate = (Convert.IsDBNull(reader["EndDate"]))?DateTime.MinValue:(System.DateTime?)reader["EndDate"];
			entity.CompanyStatus2Id = (System.Int32)reader[((int)CompaniesActiveColumn.CompanyStatus2Id)];
			//entity.CompanyStatus2Id = (Convert.IsDBNull(reader["CompanyStatus2Id"]))?(int)0:(System.Int32)reader["CompanyStatus2Id"];
			entity.Active = (System.String)reader[((int)CompaniesActiveColumn.Active)];
			//entity.Active = (Convert.IsDBNull(reader["Active"]))?string.Empty:(System.String)reader["Active"];
			entity.ExpiresIn = (reader.IsDBNull(((int)CompaniesActiveColumn.ExpiresIn)))?null:(System.Int32?)reader[((int)CompaniesActiveColumn.ExpiresIn)];
			//entity.ExpiresIn = (Convert.IsDBNull(reader["ExpiresIn"]))?(int)0:(System.Int32?)reader["ExpiresIn"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompaniesActive"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesActive"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompaniesActive entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.CompanyAbn = (Convert.IsDBNull(dataRow["CompanyAbn"]))?string.Empty:(System.String)dataRow["CompanyAbn"];
			entity.PhoneNo = (Convert.IsDBNull(dataRow["PhoneNo"]))?string.Empty:(System.String)dataRow["PhoneNo"];
			entity.FaxNo = (Convert.IsDBNull(dataRow["FaxNo"]))?string.Empty:(System.String)dataRow["FaxNo"];
			entity.MobNo = (Convert.IsDBNull(dataRow["MobNo"]))?string.Empty:(System.String)dataRow["MobNo"];
			entity.AddressBusiness = (Convert.IsDBNull(dataRow["AddressBusiness"]))?string.Empty:(System.String)dataRow["AddressBusiness"];
			entity.AddressPostal = (Convert.IsDBNull(dataRow["AddressPostal"]))?string.Empty:(System.String)dataRow["AddressPostal"];
			entity.ProcurementSupplierNo = (Convert.IsDBNull(dataRow["ProcurementSupplierNo"]))?string.Empty:(System.String)dataRow["ProcurementSupplierNo"];
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.ModifiedbyUserId = (Convert.IsDBNull(dataRow["ModifiedbyUserId"]))?(int)0:(System.Int32)dataRow["ModifiedbyUserId"];
			entity.IprocSupplierNo = (Convert.IsDBNull(dataRow["IprocSupplierNo"]))?string.Empty:(System.String)dataRow["IprocSupplierNo"];
			entity.CompanyStatusId = (Convert.IsDBNull(dataRow["CompanyStatusId"]))?(int)0:(System.Int32)dataRow["CompanyStatusId"];
			entity.StartDate = (Convert.IsDBNull(dataRow["StartDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["StartDate"];
			entity.EndDate = (Convert.IsDBNull(dataRow["EndDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["EndDate"];
			entity.CompanyStatus2Id = (Convert.IsDBNull(dataRow["CompanyStatus2Id"]))?(int)0:(System.Int32)dataRow["CompanyStatus2Id"];
			entity.Active = (Convert.IsDBNull(dataRow["Active"]))?string.Empty:(System.String)dataRow["Active"];
			entity.ExpiresIn = (Convert.IsDBNull(dataRow["ExpiresIn"]))?(int)0:(System.Int32?)dataRow["ExpiresIn"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompaniesActiveFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesActiveFilterBuilder : SqlFilterBuilder<CompaniesActiveColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveFilterBuilder class.
		/// </summary>
		public CompaniesActiveFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesActiveFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesActiveFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesActiveFilterBuilder

	#region CompaniesActiveParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesActiveParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesActiveColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveParameterBuilder class.
		/// </summary>
		public CompaniesActiveParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesActiveParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesActiveParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesActiveParameterBuilder
	
	#region CompaniesActiveSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesActiveSortBuilder : SqlSortBuilder<CompaniesActiveColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveSqlSortBuilder class.
		/// </summary>
		public CompaniesActiveSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesActiveSortBuilder

} // end namespace
