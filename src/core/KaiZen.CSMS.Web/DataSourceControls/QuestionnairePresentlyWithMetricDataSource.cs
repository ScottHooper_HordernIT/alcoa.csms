﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnairePresentlyWithMetricProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnairePresentlyWithMetricDataSourceDesigner))]
	public class QuestionnairePresentlyWithMetricDataSource : ProviderDataSource<QuestionnairePresentlyWithMetric, QuestionnairePresentlyWithMetricKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricDataSource class.
		/// </summary>
		public QuestionnairePresentlyWithMetricDataSource() : base(new QuestionnairePresentlyWithMetricService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnairePresentlyWithMetricDataSourceView used by the QuestionnairePresentlyWithMetricDataSource.
		/// </summary>
		protected QuestionnairePresentlyWithMetricDataSourceView QuestionnairePresentlyWithMetricView
		{
			get { return ( View as QuestionnairePresentlyWithMetricDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnairePresentlyWithMetricDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnairePresentlyWithMetricSelectMethod SelectMethod
		{
			get
			{
				QuestionnairePresentlyWithMetricSelectMethod selectMethod = QuestionnairePresentlyWithMetricSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnairePresentlyWithMetricSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnairePresentlyWithMetricDataSourceView class that is to be
		/// used by the QuestionnairePresentlyWithMetricDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnairePresentlyWithMetricDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnairePresentlyWithMetric, QuestionnairePresentlyWithMetricKey> GetNewDataSourceView()
		{
			return new QuestionnairePresentlyWithMetricDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnairePresentlyWithMetricDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnairePresentlyWithMetricDataSourceView : ProviderDataSourceView<QuestionnairePresentlyWithMetric, QuestionnairePresentlyWithMetricKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnairePresentlyWithMetricDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnairePresentlyWithMetricDataSourceView(QuestionnairePresentlyWithMetricDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnairePresentlyWithMetricDataSource QuestionnairePresentlyWithMetricOwner
		{
			get { return Owner as QuestionnairePresentlyWithMetricDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnairePresentlyWithMetricSelectMethod SelectMethod
		{
			get { return QuestionnairePresentlyWithMetricOwner.SelectMethod; }
			set { QuestionnairePresentlyWithMetricOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnairePresentlyWithMetricService QuestionnairePresentlyWithMetricProvider
		{
			get { return Provider as QuestionnairePresentlyWithMetricService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnairePresentlyWithMetric> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnairePresentlyWithMetric> results = null;
			QuestionnairePresentlyWithMetric item;
			count = 0;
			
			System.Int32 _questionnairePresentlyWithMetricId;
			System.Int32 _year;
			System.Int32 _weekNo;
			System.Int32? _siteId_nullable;
			System.Int32 _questionnairePresentlyWithActionId;
			System.Int32 _questionnairePresentlyWithUserId;

			switch ( SelectMethod )
			{
				case QuestionnairePresentlyWithMetricSelectMethod.Get:
					QuestionnairePresentlyWithMetricKey entityKey  = new QuestionnairePresentlyWithMetricKey();
					entityKey.Load(values);
					item = QuestionnairePresentlyWithMetricProvider.Get(entityKey);
					results = new TList<QuestionnairePresentlyWithMetric>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetAll:
                    results = QuestionnairePresentlyWithMetricProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetPaged:
					results = QuestionnairePresentlyWithMetricProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnairePresentlyWithMetricProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnairePresentlyWithMetricProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnairePresentlyWithMetricSelectMethod.GetByQuestionnairePresentlyWithMetricId:
					_questionnairePresentlyWithMetricId = ( values["QuestionnairePresentlyWithMetricId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithMetricId"], typeof(System.Int32)) : (int)0;
					item = QuestionnairePresentlyWithMetricProvider.GetByQuestionnairePresentlyWithMetricId(_questionnairePresentlyWithMetricId);
					results = new TList<QuestionnairePresentlyWithMetric>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnairePresentlyWithMetricSelectMethod.GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_weekNo = ( values["WeekNo"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["WeekNo"], typeof(System.Int32)) : (int)0;
					_siteId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					_questionnairePresentlyWithActionId = ( values["QuestionnairePresentlyWithActionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithActionId"], typeof(System.Int32)) : (int)0;
					item = QuestionnairePresentlyWithMetricProvider.GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(_year, _weekNo, _siteId_nullable, _questionnairePresentlyWithActionId);
					results = new TList<QuestionnairePresentlyWithMetric>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_weekNo = ( values["WeekNo"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["WeekNo"], typeof(System.Int32)) : (int)0;
					_siteId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					_questionnairePresentlyWithUserId = ( values["QuestionnairePresentlyWithUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnairePresentlyWithMetricProvider.GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(_year, _weekNo, _siteId_nullable, _questionnairePresentlyWithUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetByYearWeekNoSiteId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_weekNo = ( values["WeekNo"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["WeekNo"], typeof(System.Int32)) : (int)0;
					_siteId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					results = QuestionnairePresentlyWithMetricProvider.GetByYearWeekNoSiteId(_year, _weekNo, _siteId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetByYearWeekNoQuestionnairePresentlyWithActionId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_weekNo = ( values["WeekNo"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["WeekNo"], typeof(System.Int32)) : (int)0;
					_questionnairePresentlyWithActionId = ( values["QuestionnairePresentlyWithActionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithActionId"], typeof(System.Int32)) : (int)0;
					results = QuestionnairePresentlyWithMetricProvider.GetByYearWeekNoQuestionnairePresentlyWithActionId(_year, _weekNo, _questionnairePresentlyWithActionId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnairePresentlyWithMetricSelectMethod.GetBySiteId:
					_siteId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					results = QuestionnairePresentlyWithMetricProvider.GetBySiteId(_siteId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetByQuestionnairePresentlyWithActionId:
					_questionnairePresentlyWithActionId = ( values["QuestionnairePresentlyWithActionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithActionId"], typeof(System.Int32)) : (int)0;
					results = QuestionnairePresentlyWithMetricProvider.GetByQuestionnairePresentlyWithActionId(_questionnairePresentlyWithActionId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnairePresentlyWithMetricSelectMethod.GetByQuestionnairePresentlyWithUserId:
					_questionnairePresentlyWithUserId = ( values["QuestionnairePresentlyWithUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnairePresentlyWithMetricProvider.GetByQuestionnairePresentlyWithUserId(_questionnairePresentlyWithUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnairePresentlyWithMetricSelectMethod.Get || SelectMethod == QuestionnairePresentlyWithMetricSelectMethod.GetByQuestionnairePresentlyWithMetricId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnairePresentlyWithMetric entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnairePresentlyWithMetricProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnairePresentlyWithMetric> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnairePresentlyWithMetricProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnairePresentlyWithMetricDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnairePresentlyWithMetricDataSource class.
	/// </summary>
	public class QuestionnairePresentlyWithMetricDataSourceDesigner : ProviderDataSourceDesigner<QuestionnairePresentlyWithMetric, QuestionnairePresentlyWithMetricKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricDataSourceDesigner class.
		/// </summary>
		public QuestionnairePresentlyWithMetricDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnairePresentlyWithMetricSelectMethod SelectMethod
		{
			get { return ((QuestionnairePresentlyWithMetricDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnairePresentlyWithMetricDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnairePresentlyWithMetricDataSourceActionList

	/// <summary>
	/// Supports the QuestionnairePresentlyWithMetricDataSourceDesigner class.
	/// </summary>
	internal class QuestionnairePresentlyWithMetricDataSourceActionList : DesignerActionList
	{
		private QuestionnairePresentlyWithMetricDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnairePresentlyWithMetricDataSourceActionList(QuestionnairePresentlyWithMetricDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnairePresentlyWithMetricSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnairePresentlyWithMetricDataSourceActionList
	
	#endregion QuestionnairePresentlyWithMetricDataSourceDesigner
	
	#region QuestionnairePresentlyWithMetricSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnairePresentlyWithMetricDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnairePresentlyWithMetricSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithMetricId method.
		/// </summary>
		GetByQuestionnairePresentlyWithMetricId,
		/// <summary>
		/// Represents the GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId method.
		/// </summary>
		GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId,
		/// <summary>
		/// Represents the GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId method.
		/// </summary>
		GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId,
		/// <summary>
		/// Represents the GetByYearWeekNoSiteId method.
		/// </summary>
		GetByYearWeekNoSiteId,
		/// <summary>
		/// Represents the GetByYearWeekNoQuestionnairePresentlyWithActionId method.
		/// </summary>
		GetByYearWeekNoQuestionnairePresentlyWithActionId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithActionId method.
		/// </summary>
		GetByQuestionnairePresentlyWithActionId,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithUserId method.
		/// </summary>
		GetByQuestionnairePresentlyWithUserId
	}
	
	#endregion QuestionnairePresentlyWithMetricSelectMethod

	#region QuestionnairePresentlyWithMetricFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricFilter : SqlFilter<QuestionnairePresentlyWithMetricColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithMetricFilter

	#region QuestionnairePresentlyWithMetricExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricExpressionBuilder : SqlExpressionBuilder<QuestionnairePresentlyWithMetricColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithMetricExpressionBuilder	

	#region QuestionnairePresentlyWithMetricProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnairePresentlyWithMetricChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricProperty : ChildEntityProperty<QuestionnairePresentlyWithMetricChildEntityTypes>
	{
	}
	
	#endregion QuestionnairePresentlyWithMetricProperty
}

