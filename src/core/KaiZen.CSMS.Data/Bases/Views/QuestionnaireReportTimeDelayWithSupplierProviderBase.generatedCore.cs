﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportTimeDelayWithSupplierProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportTimeDelayWithSupplierProviderBaseCore : EntityViewProviderBase<QuestionnaireReportTimeDelayWithSupplier>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireReportTimeDelayWithSupplier_ByMoreThanOrEqualTo
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportTimeDelayWithSupplier_ByMoreThanOrEqualTo' stored procedure. 
		/// </summary>
		/// <param name="moreThanOrEqualTo"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ByMoreThanOrEqualTo(System.Int32? moreThanOrEqualTo)
		{
			return ByMoreThanOrEqualTo(null, 0, int.MaxValue , moreThanOrEqualTo);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportTimeDelayWithSupplier_ByMoreThanOrEqualTo' stored procedure. 
		/// </summary>
		/// <param name="moreThanOrEqualTo"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ByMoreThanOrEqualTo(int start, int pageLength, System.Int32? moreThanOrEqualTo)
		{
			return ByMoreThanOrEqualTo(null, start, pageLength , moreThanOrEqualTo);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireReportTimeDelayWithSupplier_ByMoreThanOrEqualTo' stored procedure. 
		/// </summary>
		/// <param name="moreThanOrEqualTo"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ByMoreThanOrEqualTo(TransactionManager transactionManager, System.Int32? moreThanOrEqualTo)
		{
			return ByMoreThanOrEqualTo(transactionManager, 0, int.MaxValue , moreThanOrEqualTo);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireReportTimeDelayWithSupplier_ByMoreThanOrEqualTo' stored procedure. 
		/// </summary>
		/// <param name="moreThanOrEqualTo"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ByMoreThanOrEqualTo(TransactionManager transactionManager, int start, int pageLength, System.Int32? moreThanOrEqualTo);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt; Fill(DataSet dataSet, VList<QuestionnaireReportTimeDelayWithSupplier> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportTimeDelayWithSupplier>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportTimeDelayWithSupplier>"/></returns>
		protected static VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt; Fill(DataTable dataTable, VList<QuestionnaireReportTimeDelayWithSupplier> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportTimeDelayWithSupplier c = new QuestionnaireReportTimeDelayWithSupplier();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SubContractor = (Convert.IsDBNull(row["SubContractor"]))?false:(System.Boolean?)row["SubContractor"];
					c.SupplierContact = (Convert.IsDBNull(row["SupplierContact"]))?string.Empty:(System.String)row["SupplierContact"];
					c.SupplierContactEmail = (Convert.IsDBNull(row["SupplierContactEmail"]))?string.Empty:(System.String)row["SupplierContactEmail"];
					c.ProcurementContact = (Convert.IsDBNull(row["ProcurementContact"]))?string.Empty:(System.String)row["ProcurementContact"];
					c.ProcurementContactEmail = (Convert.IsDBNull(row["ProcurementContactEmail"]))?string.Empty:(System.String)row["ProcurementContactEmail"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.SafetyAssessorEmail = (Convert.IsDBNull(row["SafetyAssessorEmail"]))?string.Empty:(System.String)row["SafetyAssessorEmail"];
					c.DaysWithSupplier = (Convert.IsDBNull(row["DaysWithSupplier"]))?(int)0:(System.Int32?)row["DaysWithSupplier"];
					c.ProcurementContactUserId = (Convert.IsDBNull(row["ProcurementContactUserId"]))?(int)0:(System.Int32?)row["ProcurementContactUserId"];
					c.SafetyAssessorUserId = (Convert.IsDBNull(row["SafetyAssessorUserId"]))?(int)0:(System.Int32?)row["SafetyAssessorUserId"];
					c.CompanyDeactivated = (Convert.IsDBNull(row["CompanyDeactivated"]))?false:(System.Boolean?)row["CompanyDeactivated"];
					c.NoCompanyUsers = (Convert.IsDBNull(row["NoCompanyUsers"]))?(int)0:(System.Int32?)row["NoCompanyUsers"];
					c.NoCompanyUsersNotDisabled = (Convert.IsDBNull(row["NoCompanyUsersNotDisabled"]))?(int)0:(System.Int32?)row["NoCompanyUsersNotDisabled"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportTimeDelayWithSupplier&gt;"/></returns>
		protected VList<QuestionnaireReportTimeDelayWithSupplier> Fill(IDataReader reader, VList<QuestionnaireReportTimeDelayWithSupplier> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportTimeDelayWithSupplier entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportTimeDelayWithSupplier>("QuestionnaireReportTimeDelayWithSupplier",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportTimeDelayWithSupplier();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SubContractor)];
					//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
					entity.SupplierContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContact)];
					//entity.SupplierContact = (Convert.IsDBNull(reader["SupplierContact"]))?string.Empty:(System.String)reader["SupplierContact"];
					entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContactEmail)];
					//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
					entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContact)];
					//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
					entity.ProcurementContactEmail = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactEmail)];
					//entity.ProcurementContactEmail = (Convert.IsDBNull(reader["ProcurementContactEmail"]))?string.Empty:(System.String)reader["ProcurementContactEmail"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.SafetyAssessorEmail = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorEmail)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorEmail)];
					//entity.SafetyAssessorEmail = (Convert.IsDBNull(reader["SafetyAssessorEmail"]))?string.Empty:(System.String)reader["SafetyAssessorEmail"];
					entity.DaysWithSupplier = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.DaysWithSupplier)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.DaysWithSupplier)];
					//entity.DaysWithSupplier = (Convert.IsDBNull(reader["DaysWithSupplier"]))?(int)0:(System.Int32?)reader["DaysWithSupplier"];
					entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactUserId)];
					//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
					entity.SafetyAssessorUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorUserId)];
					//entity.SafetyAssessorUserId = (Convert.IsDBNull(reader["SafetyAssessorUserId"]))?(int)0:(System.Int32?)reader["SafetyAssessorUserId"];
					entity.CompanyDeactivated = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.CompanyDeactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.CompanyDeactivated)];
					//entity.CompanyDeactivated = (Convert.IsDBNull(reader["CompanyDeactivated"]))?false:(System.Boolean?)reader["CompanyDeactivated"];
					entity.NoCompanyUsers = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsers)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsers)];
					//entity.NoCompanyUsers = (Convert.IsDBNull(reader["NoCompanyUsers"]))?(int)0:(System.Int32?)reader["NoCompanyUsers"];
					entity.NoCompanyUsersNotDisabled = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsersNotDisabled)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsersNotDisabled)];
					//entity.NoCompanyUsersNotDisabled = (Convert.IsDBNull(reader["NoCompanyUsersNotDisabled"]))?(int)0:(System.Int32?)reader["NoCompanyUsersNotDisabled"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportTimeDelayWithSupplier entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SubContractor)];
			//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
			entity.SupplierContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContact)];
			//entity.SupplierContact = (Convert.IsDBNull(reader["SupplierContact"]))?string.Empty:(System.String)reader["SupplierContact"];
			entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SupplierContactEmail)];
			//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
			entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContact)];
			//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
			entity.ProcurementContactEmail = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactEmail)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactEmail)];
			//entity.ProcurementContactEmail = (Convert.IsDBNull(reader["ProcurementContactEmail"]))?string.Empty:(System.String)reader["ProcurementContactEmail"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.SafetyAssessorEmail = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorEmail)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorEmail)];
			//entity.SafetyAssessorEmail = (Convert.IsDBNull(reader["SafetyAssessorEmail"]))?string.Empty:(System.String)reader["SafetyAssessorEmail"];
			entity.DaysWithSupplier = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.DaysWithSupplier)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.DaysWithSupplier)];
			//entity.DaysWithSupplier = (Convert.IsDBNull(reader["DaysWithSupplier"]))?(int)0:(System.Int32?)reader["DaysWithSupplier"];
			entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.ProcurementContactUserId)];
			//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
			entity.SafetyAssessorUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.SafetyAssessorUserId)];
			//entity.SafetyAssessorUserId = (Convert.IsDBNull(reader["SafetyAssessorUserId"]))?(int)0:(System.Int32?)reader["SafetyAssessorUserId"];
			entity.CompanyDeactivated = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.CompanyDeactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.CompanyDeactivated)];
			//entity.CompanyDeactivated = (Convert.IsDBNull(reader["CompanyDeactivated"]))?false:(System.Boolean?)reader["CompanyDeactivated"];
			entity.NoCompanyUsers = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsers)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsers)];
			//entity.NoCompanyUsers = (Convert.IsDBNull(reader["NoCompanyUsers"]))?(int)0:(System.Int32?)reader["NoCompanyUsers"];
			entity.NoCompanyUsersNotDisabled = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsersNotDisabled)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayWithSupplierColumn.NoCompanyUsersNotDisabled)];
			//entity.NoCompanyUsersNotDisabled = (Convert.IsDBNull(reader["NoCompanyUsersNotDisabled"]))?(int)0:(System.Int32?)reader["NoCompanyUsersNotDisabled"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportTimeDelayWithSupplier entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SubContractor = (Convert.IsDBNull(dataRow["SubContractor"]))?false:(System.Boolean?)dataRow["SubContractor"];
			entity.SupplierContact = (Convert.IsDBNull(dataRow["SupplierContact"]))?string.Empty:(System.String)dataRow["SupplierContact"];
			entity.SupplierContactEmail = (Convert.IsDBNull(dataRow["SupplierContactEmail"]))?string.Empty:(System.String)dataRow["SupplierContactEmail"];
			entity.ProcurementContact = (Convert.IsDBNull(dataRow["ProcurementContact"]))?string.Empty:(System.String)dataRow["ProcurementContact"];
			entity.ProcurementContactEmail = (Convert.IsDBNull(dataRow["ProcurementContactEmail"]))?string.Empty:(System.String)dataRow["ProcurementContactEmail"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.SafetyAssessorEmail = (Convert.IsDBNull(dataRow["SafetyAssessorEmail"]))?string.Empty:(System.String)dataRow["SafetyAssessorEmail"];
			entity.DaysWithSupplier = (Convert.IsDBNull(dataRow["DaysWithSupplier"]))?(int)0:(System.Int32?)dataRow["DaysWithSupplier"];
			entity.ProcurementContactUserId = (Convert.IsDBNull(dataRow["ProcurementContactUserId"]))?(int)0:(System.Int32?)dataRow["ProcurementContactUserId"];
			entity.SafetyAssessorUserId = (Convert.IsDBNull(dataRow["SafetyAssessorUserId"]))?(int)0:(System.Int32?)dataRow["SafetyAssessorUserId"];
			entity.CompanyDeactivated = (Convert.IsDBNull(dataRow["CompanyDeactivated"]))?false:(System.Boolean?)dataRow["CompanyDeactivated"];
			entity.NoCompanyUsers = (Convert.IsDBNull(dataRow["NoCompanyUsers"]))?(int)0:(System.Int32?)dataRow["NoCompanyUsers"];
			entity.NoCompanyUsersNotDisabled = (Convert.IsDBNull(dataRow["NoCompanyUsersNotDisabled"]))?(int)0:(System.Int32?)dataRow["NoCompanyUsersNotDisabled"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportTimeDelayWithSupplierFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayWithSupplierFilterBuilder : SqlFilterBuilder<QuestionnaireReportTimeDelayWithSupplierColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierFilterBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayWithSupplierFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayWithSupplierFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayWithSupplierFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayWithSupplierFilterBuilder

	#region QuestionnaireReportTimeDelayWithSupplierParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayWithSupplierParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportTimeDelayWithSupplierColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierParameterBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayWithSupplierParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayWithSupplierParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayWithSupplierParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayWithSupplierParameterBuilder
	
	#region QuestionnaireReportTimeDelayWithSupplierSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayWithSupplier"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportTimeDelayWithSupplierSortBuilder : SqlSortBuilder<QuestionnaireReportTimeDelayWithSupplierColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayWithSupplierSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayWithSupplierSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportTimeDelayWithSupplierSortBuilder

} // end namespace
