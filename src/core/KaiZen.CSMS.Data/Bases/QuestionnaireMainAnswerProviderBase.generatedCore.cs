﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainAnswerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireMainAnswerProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireMainAnswer, KaiZen.CSMS.Entities.QuestionnaireMainAnswerKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAnswerKey key)
		{
			return Delete(transactionManager, key.AnswerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_answerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _answerId)
		{
			return Delete(null, _answerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _answerId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireAnswer_QuestionnaireQuestion key.
		///		FK_QuestionnaireAnswer_QuestionnaireQuestion Description: 
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAnswer objects.</returns>
		public TList<QuestionnaireMainAnswer> GetByQuestionNo(System.String _questionNo)
		{
			int count = -1;
			return GetByQuestionNo(_questionNo, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireAnswer_QuestionnaireQuestion key.
		///		FK_QuestionnaireAnswer_QuestionnaireQuestion Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAnswer objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireMainAnswer> GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo)
		{
			int count = -1;
			return GetByQuestionNo(transactionManager, _questionNo, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireAnswer_QuestionnaireQuestion key.
		///		FK_QuestionnaireAnswer_QuestionnaireQuestion Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAnswer objects.</returns>
		public TList<QuestionnaireMainAnswer> GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNo(transactionManager, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireAnswer_QuestionnaireQuestion key.
		///		fkQuestionnaireAnswerQuestionnaireQuestion Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAnswer objects.</returns>
		public TList<QuestionnaireMainAnswer> GetByQuestionNo(System.String _questionNo, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionNo(null, _questionNo, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireAnswer_QuestionnaireQuestion key.
		///		fkQuestionnaireAnswerQuestionnaireQuestion Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionNo"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAnswer objects.</returns>
		public TList<QuestionnaireMainAnswer> GetByQuestionNo(System.String _questionNo, int start, int pageLength,out int count)
		{
			return GetByQuestionNo(null, _questionNo, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireAnswer_QuestionnaireQuestion key.
		///		FK_QuestionnaireAnswer_QuestionnaireQuestion Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAnswer objects.</returns>
		public abstract TList<QuestionnaireMainAnswer> GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireMainAnswer Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAnswerKey key, int start, int pageLength)
		{
			return GetByAnswerId(transactionManager, key.AnswerId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByAnswerId(System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(null,_answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByAnswerId(System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(null, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByAnswerId(System.Int32 _answerId, int start, int pageLength, out int count)
		{
			return GetByAnswerId(null, _answerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="_answerNo"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByQuestionNoAnswerNo(System.String _questionNo, System.String _answerNo)
		{
			int count = -1;
			return GetByQuestionNoAnswerNo(null,_questionNo, _answerNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="_answerNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByQuestionNoAnswerNo(System.String _questionNo, System.String _answerNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNoAnswerNo(null, _questionNo, _answerNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="_answerNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByQuestionNoAnswerNo(TransactionManager transactionManager, System.String _questionNo, System.String _answerNo)
		{
			int count = -1;
			return GetByQuestionNoAnswerNo(transactionManager, _questionNo, _answerNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="_answerNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByQuestionNoAnswerNo(TransactionManager transactionManager, System.String _questionNo, System.String _answerNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNoAnswerNo(transactionManager, _questionNo, _answerNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="_answerNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByQuestionNoAnswerNo(System.String _questionNo, System.String _answerNo, int start, int pageLength, out int count)
		{
			return GetByQuestionNoAnswerNo(null, _questionNo, _answerNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAnswer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="_answerNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainAnswer GetByQuestionNoAnswerNo(TransactionManager transactionManager, System.String _questionNo, System.String _answerNo, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireMainAnswer&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireMainAnswer&gt;"/></returns>
		public static TList<QuestionnaireMainAnswer> Fill(IDataReader reader, TList<QuestionnaireMainAnswer> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireMainAnswer c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireMainAnswer")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireMainAnswerColumn.AnswerId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireMainAnswer>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireMainAnswer",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireMainAnswer();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AnswerId = (System.Int32)reader[((int)QuestionnaireMainAnswerColumn.AnswerId - 1)];
					c.QuestionNo = (System.String)reader[((int)QuestionnaireMainAnswerColumn.QuestionNo - 1)];
					c.AnswerNo = (reader.IsDBNull(((int)QuestionnaireMainAnswerColumn.AnswerNo - 1)))?null:(System.String)reader[((int)QuestionnaireMainAnswerColumn.AnswerNo - 1)];
					c.AnswerText = (reader.IsDBNull(((int)QuestionnaireMainAnswerColumn.AnswerText - 1)))?null:(System.String)reader[((int)QuestionnaireMainAnswerColumn.AnswerText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireMainAnswer entity)
		{
			if (!reader.Read()) return;
			
			entity.AnswerId = (System.Int32)reader[((int)QuestionnaireMainAnswerColumn.AnswerId - 1)];
			entity.QuestionNo = (System.String)reader[((int)QuestionnaireMainAnswerColumn.QuestionNo - 1)];
			entity.AnswerNo = (reader.IsDBNull(((int)QuestionnaireMainAnswerColumn.AnswerNo - 1)))?null:(System.String)reader[((int)QuestionnaireMainAnswerColumn.AnswerNo - 1)];
			entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireMainAnswerColumn.AnswerText - 1)))?null:(System.String)reader[((int)QuestionnaireMainAnswerColumn.AnswerText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireMainAnswer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerId = (System.Int32)dataRow["AnswerId"];
			entity.QuestionNo = (System.String)dataRow["QuestionNo"];
			entity.AnswerNo = Convert.IsDBNull(dataRow["AnswerNo"]) ? null : (System.String)dataRow["AnswerNo"];
			entity.AnswerText = Convert.IsDBNull(dataRow["AnswerText"]) ? null : (System.String)dataRow["AnswerText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAnswer"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainAnswer Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAnswer entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionNoSource	
			if (CanDeepLoad(entity, "QuestionnaireMainQuestion|QuestionNoSource", deepLoadType, innerList) 
				&& entity.QuestionNoSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionNo;
				QuestionnaireMainQuestion tmpEntity = EntityManager.LocateEntity<QuestionnaireMainQuestion>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireMainQuestion), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionNoSource = tmpEntity;
				else
					entity.QuestionNoSource = DataRepository.QuestionnaireMainQuestionProvider.GetByQuestionNo(transactionManager, entity.QuestionNo);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionNoSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionNoSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireMainQuestionProvider.DeepLoad(transactionManager, entity.QuestionNoSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionNoSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAnswerId methods when available
			
			#region QuestionnaireMainResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainResponse>|QuestionnaireMainResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainResponseCollection = DataRepository.QuestionnaireMainResponseProvider.GetByAnswerId(transactionManager, entity.AnswerId);

				if (deep && entity.QuestionnaireMainResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainResponse>) DataRepository.QuestionnaireMainResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireMainAttachmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainAttachment>|QuestionnaireMainAttachmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainAttachmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainAttachmentCollection = DataRepository.QuestionnaireMainAttachmentProvider.GetByAnswerId(transactionManager, entity.AnswerId);

				if (deep && entity.QuestionnaireMainAttachmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainAttachment>) DataRepository.QuestionnaireMainAttachmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainAttachmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireMainAnswer object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireMainAnswer instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainAnswer Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAnswer entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionNoSource
			if (CanDeepSave(entity, "QuestionnaireMainQuestion|QuestionNoSource", deepSaveType, innerList) 
				&& entity.QuestionNoSource != null)
			{
				DataRepository.QuestionnaireMainQuestionProvider.Save(transactionManager, entity.QuestionNoSource);
				entity.QuestionNo = entity.QuestionNoSource.QuestionNo;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireMainResponse>
				if (CanDeepSave(entity.QuestionnaireMainResponseCollection, "List<QuestionnaireMainResponse>|QuestionnaireMainResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainResponse child in entity.QuestionnaireMainResponseCollection)
					{
						if(child.AnswerIdSource != null)
						{
							child.AnswerId = child.AnswerIdSource.AnswerId;
						}
						else
						{
							child.AnswerId = entity.AnswerId;
						}

					}

					if (entity.QuestionnaireMainResponseCollection.Count > 0 || entity.QuestionnaireMainResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainResponseProvider.Save(transactionManager, entity.QuestionnaireMainResponseCollection);
						
						deepHandles.Add("QuestionnaireMainResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainResponse >) DataRepository.QuestionnaireMainResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireMainAttachment>
				if (CanDeepSave(entity.QuestionnaireMainAttachmentCollection, "List<QuestionnaireMainAttachment>|QuestionnaireMainAttachmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainAttachment child in entity.QuestionnaireMainAttachmentCollection)
					{
						if(child.AnswerIdSource != null)
						{
							child.AnswerId = child.AnswerIdSource.AnswerId;
						}
						else
						{
							child.AnswerId = entity.AnswerId;
						}

					}

					if (entity.QuestionnaireMainAttachmentCollection.Count > 0 || entity.QuestionnaireMainAttachmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainAttachmentProvider.Save(transactionManager, entity.QuestionnaireMainAttachmentCollection);
						
						deepHandles.Add("QuestionnaireMainAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainAttachment >) DataRepository.QuestionnaireMainAttachmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainAttachmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireMainAnswerChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireMainAnswer</c>
	///</summary>
	public enum QuestionnaireMainAnswerChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>QuestionnaireMainQuestion</c> at QuestionNoSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireMainQuestion))]
		QuestionnaireMainQuestion,
	
		///<summary>
		/// Collection of <c>QuestionnaireMainAnswer</c> as OneToMany for QuestionnaireMainResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainResponse>))]
		QuestionnaireMainResponseCollection,

		///<summary>
		/// Collection of <c>QuestionnaireMainAnswer</c> as OneToMany for QuestionnaireMainAttachmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainAttachment>))]
		QuestionnaireMainAttachmentCollection,
	}
	
	#endregion QuestionnaireMainAnswerChildEntityTypes
	
	#region QuestionnaireMainAnswerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerFilterBuilder : SqlFilterBuilder<QuestionnaireMainAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerFilterBuilder class.
		/// </summary>
		public QuestionnaireMainAnswerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAnswerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAnswerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAnswerFilterBuilder
	
	#region QuestionnaireMainAnswerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAnswerParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainAnswerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerParameterBuilder class.
		/// </summary>
		public QuestionnaireMainAnswerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAnswerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAnswerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAnswerParameterBuilder
	
	#region QuestionnaireMainAnswerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainAnswerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAnswer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainAnswerSortBuilder : SqlSortBuilder<QuestionnaireMainAnswerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAnswerSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainAnswerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainAnswerSortBuilder
	
} // end namespace
