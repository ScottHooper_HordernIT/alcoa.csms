﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_header"
    CodeBehind="header.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<a name="top"></a>
<table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="900">
    <tbody>
        <tr>
            <td><img height="1" src="Images/spacer.gif" width="10" /></td>
            <td><img height="1" src="Images/spacer.gif" width="8" /></td>
            <td><img height="1" src="Images/spacer.gif" width="153" /></td>
            <td><img height="1" src="Images/spacer.gif" width="69" /></td>
            <td><img height="1" src="Images/spacer.gif" width="500" /></td>
            <td style="width: 13px"><img height="1" src="Images/spacer.gif" width="10" /></td>
        </tr>
        <tr>
            <td colspan="2" rowspan="2"><img height="15" src="Images/spacer.gif" width="1" /></td>
            <td rowspan="2" valign="bottom">
                <a href="http://www.alcoa.com/global/en/home.asp" target="_blank" onfocus="blur(this)">
<img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" alt="Alcoa" width="153" /></a>
            </td>
            <td rowspan="2" valign="bottom">
                <img height="1" src="Images/spacer.gif" width="10" />
            </td>
            <td align="right" valign="top">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr><td><img height="3" src="Images/spacer.gif" /></td></tr>
                    </tbody>
                </table>
                <span style="color: #003399">Alcoa World Alumina - Australian Operations</span></td>
            <td rowspan="2" style="width: 13px"><img height="1" src="Images/spacer.gif" width="1" /></td>
        </tr>
        <tr>
            <td align="right" valign="bottom">
                <span class="sitetitleblue">
                    <a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">Contractor Services Management System</a></span></td>
        </tr>
        <tr>
            <td colspan="6" style="height: 3px"><img height="3" src="Images/spacer.gif" width="1" /></td>
        </tr>
    </tbody>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700">
    <tbody>
        <tr>
            <td><img height="1" src="Images/spacer.gif" width="9" /></td>
            <td><img height="1" src="Images/spacer.gif" width="1" /></td>
            <td><img height="1" src="Images/spacer.gif" width="700" /></td>
            <td style="width: 201px"><img height="1" src="Images/spacer.gif" width="200" /></td>
            <td><img height="1" src="Images/spacer.gif" width="1" /></td>
            <td><img height="1" src="Images/spacer.gif" width="9" /></td>
        </tr>
        <tr>
            <td colspan="2" style="height: 28px">
                <img height="28" src="Images/spacer.gif" width= "6" />
            </td>
            <td colspan="2" style="height: 28px">
                <dx:ASPxMenu ID="ASPxMenu1" runat="server" CssFilePath="~/App_Themes/AlcoaMenu/{0}/styles1.css"
                    CssPostfix="PlasticBlue" ImageFolder="~/App_Themes/AlcoaMenu/{0}/" ItemSpacing="0px"
                    SeparatorHeight="22px" Width="100%" Cursor="pointer" EnableAnimation="False"
                    EnableViewState="False" GutterWidth="0px">
                    <HorizontalPopOutImage Width="0px" Height="0px"></HorizontalPopOutImage>    <%--Added for Horizontal Drop Down Image in Menu by Debashis--%>
                    <Items>
                        <dx:MenuItem NavigateUrl="~/Default.aspx" Text="HOME">
                        </dx:MenuItem>
                        <dx:MenuItem NavigateUrl="~/SafetyPQ_Questionnaire.aspx" Text="Safety Pre-Qualification"
                            Visible="False">
                        </dx:MenuItem>
                        <dx:MenuItem Text="Health &amp; Safety">
                            <Items>
                                <dx:MenuItem Text="Australian Safety Qualification Process" Visible="False" Name="SQlink"
                                    NavigateUrl="~/SafetyPQ_Questionnaire.aspx" >
                                </dx:MenuItem>
                                <dx:MenuItem Text="Australian Safety Qualification Information" Visible="False" >
                                    <Items>
                                        <dx:MenuItem NavigateUrl="~/Reports_SafetyPQOverview.aspx" Text="Qualified Contractors"
                                            Name="SQ_Database" >
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Reports_SafetyPQ.aspx?q=expire" Text="Expiry Date" Name="SQ_Expiry">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Reports_SafetyPQ.aspx?q=services" Text="Services" Name="SQ_Services" >
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Reports_SafetyPQStatus.aspx" Text="Status" Name="SQ_Status" >
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="~/CSA.aspx" Text="Australian Contractor Services Audit">
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="~/SafetyPQ_Questionnaire_SubC.aspx?sc=1" Text="WA SubContractor Process"
                                    Visible="False" >
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="~/SafetyPlans.aspx" Text="Australian Safety Management Plan Process" >
                                </dx:MenuItem>
                                <dx:MenuItem Name="Safety WAO" Text="WA Health" >
                                    <Items>
                                        <dx:MenuItem BeginGroup="True" Text="Medical" >
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/Medical_Req.aspx" Text="Medical Requirements" >
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Medical_Upload.aspx" Text="Upload or View Medical Schedules" >
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Templates&File=Medical" Name="Template_Medical"
                                                    Text="Download Standard Template" >
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                                <dx:MenuItem Text="Victorian Health">
                                    <Items>
                                        <dx:MenuItem NavigateUrl="~/Medical_Upload.aspx?VicOps=true" Text="Upload or View Medical Schedules">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Templates&File=MedicalVicOps"
                                            Name="Template_MedicalVicOps" Text="Download Standard Template">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>
                        <dx:MenuItem Text="Training">
                            <Items>
                                <dx:MenuItem Name="Training WAO" Text="Western Australian Operations">
                                    <Items>
                                        <dx:MenuItem NavigateUrl="~/Training_Packages.aspx" Text="Training Packages">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Training_Upload.aspx" Text="Upload or View Training Schedules">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Templates&File=Training" Name="Template_Training"
                                            Text="Download Standard Template">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Training_Attendance.aspx" Name="Training_Attendance"
                                            Text="Training Attendance Record">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="javascript:popUp('PopUps/VicTraining.aspx')" Text="Victorian Operations">
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>
                        <dx:MenuItem Text="Library">
                            <Items>
                                <dx:MenuItem Name="Library WAO" Text="Western Australian Operations">
                                    <Items>
                                        <dx:MenuItem NavigateUrl="~/APSS_Top9.aspx" Text="Must Read these...">
                                        </dx:MenuItem>
                                        <dx:MenuItem BeginGroup="True" Text="Documents">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/APSS_MFU.aspx" Text="Most Frequently Used">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/APSS_Search.aspx" Text="Search">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem BeginGroup="True" Text="Templates">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Templates&File=Medical" Name="Template_Medical2"
                                                    Text="Medical Template">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Templates&File=Training" Name="Template_Training2"
                                                    Text="Training Template">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                                <dx:MenuItem Text="Victorian Operations">
                                    <Items>
                                        <dx:MenuItem NavigateUrl="~/APSS_Top9.aspx?Region=VIC" Text="Must Read these...">
                                        </dx:MenuItem>
                                        <dx:MenuItem Visible="False" Text="Documents">
                                        </dx:MenuItem>
                                        <dx:MenuItem Visible="False" Text="Templates">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                                <dx:MenuItem Text="Definitions">
                                    <Items>
                                        <dx:MenuItem NavigateUrl="javascript:popUp3('PopUps/Library_Embedded.aspx');" Text="Embedded / Non-Embedded">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Help&File=KpiEmbedded" Name="Definitions_KpiEmbedded"
                                            Text="KPI required by Residential Category (E, NE1, NE2, NE3)">
                                        </dx:MenuItem>
                                        <dx:MenuItem NavigateUrl="javascript:popUp('PopUps/Library_SFR.aspx');" Text="Safety Frequency Rates">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                                <dx:MenuItem Text="Useful Links" Name="UsefulLinks">
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="" Name="SqExemptionForm" Text="Safety Qualification Exemption Form">
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>
                        <dx:MenuItem Text="Performance Management">
                            <Items>
                                <dx:MenuItem NavigateUrl="~/KPI.aspx" Text="Monthly KPI Recording">
                                </dx:MenuItem>
                                <dx:MenuItem Text="Reports/Charts">
                                    <Items>
                                        <dx:MenuItem NavigateUrl="~/CreateYourOwnReports.aspx" Text="Create Your Own KPI Report">
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Monthly KPI Summary">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/Reports_MonthlyReports_Australia.aspx" Text="Australia"
                                                    Visible="False">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Reports_MonthlyReports.aspx?Group=VICOPS" Text="Victorian Operations">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Reports_MonthlyReports.aspx?Group=WAO" Text="Western Australian Operations">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Compliance Reports">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/ComplianceChart2.aspx" Text="Safety KPI Compliance Chart (Radar)"
                                                    Visible="true">
                                                </dx:MenuItem>
                                                <dx:MenuItem BeginGroup="True" NavigateUrl="~/Reports_SummaryCompliance.aspx" Text="Summary Compliance Report By Company">
                                                </dx:MenuItem>
                                                <dx:MenuItem BeginGroup="False" NavigateUrl="~/Reports_SummaryComplianceBySite.aspx"
                                                    Text="Summary Compliance Report By Site">
                                                </dx:MenuItem>

                                                <dx:MenuItem BeginGroup="true" NavigateUrl="~/NonComplianceSQ.aspx" Text="Safety Qualification Report">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceSP.aspx" Text="Safety Management Plan">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceEbiOnSite.aspx" Name="Ebi" Text="Contractors on site (EBI)"
                                                    Visible="false">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceKPISQ.aspx" Text="Contractors on site (KPI)"
                                                    Visible="false">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceKPI.aspx" Text="KPI submitted">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceMT.aspx" Text="Medical Schedules submitted"
                                                    Visible="true">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceMT.aspx?q=TS" Text="Training Schedules submitted"
                                                    Visible="true">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/NonComplianceCSA.aspx" Text="Contractor Services Audit">
                                                </dx:MenuItem>
                                                <%--Added by Vikas for item#33--%>
                                                 <dx:MenuItem NavigateUrl="~/ContactDetails.aspx" Text="Contact Details">
                                                </dx:MenuItem>
                                                <%--End--%>
                                                <dx:MenuItem NavigateUrl="~/AuditDownloads.aspx" Text="Document History">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/21PointAudit.aspx" Text="CSA - 2008" Visible="False">
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="Audit S8.12" Visible="False">
                                                    <Items>
                                                        <dx:MenuItem NavigateUrl="~/Reports_AuditSection8_12.aspx" Text="Audit Report" Visible="True">
                                                        </dx:MenuItem>
                                                        <%--<dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Help&File=Sa812Evidence" Text="Evidence Folder">
                                                        </dx:MenuItem>--%>
                                                        <dx:MenuItem NavigateUrl="~/AuditS812EvidenceFolder.aspx" Text="Evidence Folder">
                                                        </dx:MenuItem>
                                                    </Items>
                                                </dx:MenuItem>

                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Validation Reports" Visible="False">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/Reports_Variation.aspx" Text="Incident Variation Report (EHSIMS &lt;&gt; CSMS)">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Reports_iProc.aspx" Text="iProc Claims Report">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Financial Reports" Visible="False">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/Admin_Ebi.aspx" Target="_self" Text="Attendance Recording">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/FinancialReport.aspx" Target="_self" Text="Contractor Details - Hours">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/FinancialReport2.aspx" Target="_self" Text="Contractor Summary - FTE">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Work Levelling Tool" Visible="False">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/Reports_EhsWorkload_Smp.aspx" Name="EHS WorkLoad SMP"
                                                    Text="Safety Management Plans">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Reports_EhsWorkload_Sqq.aspx" Name="EHS WorkLoad SQQ"
                                                    Text="Safety Qualification Questionnaires">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/CompanyChangeApprovalList.aspx" Name="RegionalManagerApprovals"
                                                    Text="Manager - Procurement Operations : Approvals">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="Contract Reviews &amp; Presentations" Visible="False">
                                            <Items>
                                                <dx:MenuItem NavigateUrl="~/ComplianceChart2p.aspx?internal=0" Name="Bicycle_External"
                                                    Text="External Alcoa Presentation">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/ComplianceChart2p.aspx?internal=1" Name="Bicycle_Internal"
                                                    Text="Internal Alcoa Presentation">
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Text="CMT Shop Front Tools" Name="PerformanceManagement_Tools" Visible="false">
                                            <Items>
                                            <%--//Added By Sayani--%>
                                                <dx:MenuItem NavigateUrl="~/ContractorManagementSafetyDashboard.aspx" Text="Contractor Management Safety Dashboard">
                                                </dx:MenuItem>
                                                <%--End--%>
                                                <dx:MenuItem NavigateUrl="~/SafetyCategoryUpdateManager.aspx" Text="Safety Categories, Sponsors, ARP, CRP, Approvals">
                                                </dx:MenuItem>

                                                <%-- Added by Sayani Sil for Task# 16--%>
                                                <dx:MenuItem NavigateUrl="~/SafetyRecognitionProgram.aspx" Text="Safety Recognition Program">
                                                </dx:MenuItem>
                                                <%--End--%>
                                                <dx:MenuItem NavigateUrl="~/Reports_Recordables.aspx" Text="Incident Report" Visible="False">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/Reports_LastIncident.aspx" Text="Injury Free Days Report"
                                                    Visible="False">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/KPIVariance.aspx" Text="KPI Hours Variance Report"
                                                    Visible="True">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/EbiMetrics.aspx" Text="EBI Metrics"
                                                    Visible="True">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/ContractReviewsRegister.aspx" Text="Contract Reviews Register"
                                                    Visible="True">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/RiskNotificationsCorrectiveActionsOpenClosed.aspx" Text="RN/CA Open-Closed Metrics"
                                                    Visible="True">
                                                </dx:MenuItem>
                                                <dx:MenuItem NavigateUrl="~/FatigueManagement.aspx" Text="Fatigue Management"
                                                    Visible="True">
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="People Management –Training">
                                                    <Items>
                                                        <dx:MenuItem NavigateUrl="~/WorkPlanningFormSummary.aspx"
                                                            Text="Work Planning Form Summary">
                                                        </dx:MenuItem>
                                                        <dx:MenuItem NavigateUrl="~/Validity.aspx"
                                                            Text="Validity">
                                                        </dx:MenuItem>
                                                        <dx:MenuItem NavigateUrl="~/TrainingMetrics.aspx"
                                                            Text="Training Metrics">
                                                        </dx:MenuItem>
                                                    </Items>
                                                </dx:MenuItem>
                                            </Items>
                                        </dx:MenuItem>
                                        <dx:MenuItem Name="KpiEngineeringProjectHours" NavigateUrl="~/KpiEngineeringProjectHours.aspx"
                                            Text="Engineering Project Hours" Visible="false">
                                        </dx:MenuItem>
                                        <dx:MenuItem Name="Cpms" NavigateUrl="http://cpms.aua.alcoa.com"
                                            Text="Construction Package Management System" Visible="false">
                                        </dx:MenuItem>
                                        <dx:MenuItem Name="Cprs" NavigateUrl="http://cprs.aua.alcoa.com"
                                            Text="Contractor Performance Reporting System" Visible="false">
                                        </dx:MenuItem>
                                    </Items>
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>
                        <dx:MenuItem Text="Contact Details">
                            <Items>
                                <dx:MenuItem NavigateUrl="~/cContacts.aspx" Text="Contractors">
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="~/Common/GetFile.aspx?Type=Templates&File=cContactsInfo"
                                    Text="Contractor Contact Template" Target="_blank"> <%--Changed by Jolly for Spec # 72--%>
                                </dx:MenuItem>
                                <dx:MenuItem NavigateUrl="~/aContacts.aspx" Text="Alcoa">
                                </dx:MenuItem>
                                <dx:MenuItem BeginGroup="True" Name="Users" NavigateUrl="~/cUsers.aspx" Text="Who Has Access">
                                </dx:MenuItem>
                                <dx:MenuItem BeginGroup="True" NavigateUrl="~/CompanyContractorReview.aspx" Text="Company Contractor Review">
                                </dx:MenuItem>
                                <dx:MenuItem BeginGroup="True" NavigateUrl="~/ContractorDataManagement.aspx" Text="Add/Maintain Contractor Person Record">
                                </dx:MenuItem>
                            </Items>
                        </dx:MenuItem>
                    </Items>
                    <SubMenuStyle GutterWidth="0px" />
                    <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                    <SeparatorBackgroundImage ImageUrl="~/App_Themes/AlcoaMenu/Web/mSeparator.gif" />
                    <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                    <BackgroundImage ImageUrl="~/App_Themes/AlcoaMenu/Web/bckgrn_blue.gif" Repeat="RepeatX" />
                    <ItemStyle Cursor="auto" HorizontalAlign="Center" ImageSpacing="0px" VerticalAlign="Middle"
                        Wrap="False" ForeColor="White" Font-Bold="True">
                        <HoverStyle BackColor="#CCDDFF" ForeColor="Black">
                            <Border BorderColor="#003399" BorderStyle="Solid" BorderWidth="1px" />
                        </HoverStyle>
                        <SelectedStyle BackColor="#CCDDFF" ForeColor="Black">
                            <Border BorderColor="#003399" BorderStyle="Double" BorderWidth="1px" />
                        </SelectedStyle>
                        <BackgroundImage Repeat="NoRepeat" />
                    </ItemStyle>
                    <SubMenuItemStyle HorizontalAlign="Left">
                    </SubMenuItemStyle>
                </dx:ASPxMenu>
            </td>
            <td style="height: 28px">
            </td>
        </tr>
    </tbody>
</table>
