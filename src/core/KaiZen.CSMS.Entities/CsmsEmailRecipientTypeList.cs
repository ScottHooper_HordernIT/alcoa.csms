﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'CsmsEmailRecipientType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table CsmsEmailRecipientType</remark>
	[Serializable]
	public enum CsmsEmailRecipientTypeList
	{
		/// <summary> 
		/// To
		/// </summary>
		[EnumTextValue(@"To")]
		To = 1, 

		/// <summary> 
		/// Cc
		/// </summary>
		[EnumTextValue(@"Cc")]
		Cc = 2, 

		/// <summary> 
		/// Bcc
		/// </summary>
		[EnumTextValue(@"Bcc")]
		Bcc = 3

	}
}
