﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'ConfigSa812' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ConfigSa812 : ConfigSa812Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ConfigSa812"/> instance.
		///</summary>
		public ConfigSa812():base(){}	
		
		#endregion
	}
}
