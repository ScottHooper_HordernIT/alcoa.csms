﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'CompanyStatus2' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CompanyStatus2 : CompanyStatus2Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CompanyStatus2"/> instance.
		///</summary>
		public CompanyStatus2():base(){}	
		
		#endregion
	}
}
