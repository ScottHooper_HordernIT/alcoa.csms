﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'SafetyPlans_SE_Responses' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SafetyPlansSeResponses : SafetyPlansSeResponsesBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SafetyPlansSeResponses"/> instance.
		///</summary>
		public SafetyPlansSeResponses():base(){}	
		
		#endregion
	}
}
