using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS;

using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;

using System.Text;
using System.Drawing;

public partial class UserControls_CSA : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    string[] section01 = new String[] { "1a", "1b", "1c", "1d", "1e", "1f", "1g", "1h", "1i", "1j", "1k", "1l", "1m", "1n", "1o", "1p", "1q", "1r" };
    string[] section02 = new String[] { "2a", "2b", "2c", "2d", "2e", "2f", "2g", "2h" };
    string[] section03 = new String[] { "3a", "3b", "3c", "3d", "3e", "3f", "3g", "3h", "3i", "3j", "3k", "3l", "3m", "3n", "3o" };
    string[] section04 = new String[] { "4a", "4b", "4c", "4d", "4e", "4f", "4g", "4h", "4i", "4j", "4k", "4l", "4m", "4n", "4o" };
    string[] section05 = new String[] { "5a", "5b", "5c", "5d", "5e", "5f" };
    string[] section06 = new String[] { "6a", "6b", "6c", "6d", "6e", "6f", "6g", "6h", "6i" };

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        cbQtr.ValidationSettings.Display = Display.Dynamic;
        if (!postBack)
        {
            cbYear.Items.Clear();
            if (DateTime.Now.Year > 2008)
            {
                cbYear.Items.Add(DateTime.Now.Year.ToString());
                cbYear.SelectedIndex = 0;
            }
            if (DateTime.Now.Year > 2009)
            {
                cbYear.Items.Clear();
                cbYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString());
                cbYear.Items.Add(DateTime.Now.Year.ToString());
                cbYear.SelectedIndex = 1;
            }
            if (DateTime.Now.Year > 2010)
            {
                cbYear.Items.Clear();
                cbYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString());
                cbYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString());
                cbYear.Items.Add(DateTime.Now.Year.ToString());
                cbYear.SelectedIndex = 2;
            }
            if (DateTime.Now.Year > 2011)
            {
                cbYear.Items.Clear();
                cbYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 3).ToString());
                cbYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 2).ToString());
                cbYear.Items.Add((Convert.ToInt32(DateTime.Now.Year) - 1).ToString());
                cbYear.Items.Add(DateTime.Now.Year.ToString());
                cbYear.SelectedIndex = 3;
            }

            //cbQtr.DataSourceID = "topaQtr";
            //cbQtr.TextField = "QtrName";
            //cbQtr.ValueField = "QtrId";
            //cbQtr.DataBind();

            //if (DateTime.Now.Month >= 1) //Qtr 1 - 1-3
            //{
            //    cbQtr.SelectedIndex = 0;
            //}
            //if (DateTime.Now.Month >= 4) //Qtr 2 - 4-6
            //{
            //    cbQtr.SelectedIndex = 1;
            //}
            //if (DateTime.Now.Month >= 7) //Qtr 3 - 7-9
            //{
            //    cbQtr.SelectedIndex = 2;
            //}
            //if (DateTime.Now.Month >= 10) //Qtr 4 - 10-12
            //{
            //    cbQtr.SelectedIndex = 3;
            //}


            ddlCompanies.Text = "< Select Company >";
            ddlCompanies.DataSourceID = "sqldsCompaniesList";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();

            //SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            //FillSitesCombo(SessionHandler.spVar_CompanyId);

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    ////btnSave.Visible = true;
                    ////btnSave.Enabled = true;
                    //btnSearchGo.Text = "View";
                    break;
                case ((int)RoleList.Contractor):
                    ddlCompanies.Value = auth.CompanyId;
                    ddlCompanies.Enabled = false;
                    FillSitesCombo(Convert.ToString(auth.CompanyId));
                    //cbQtr.Items.Remove(cbQtr.Items.FindByValue("5")); //remove 'Alcoa Annual Review'
                    break;
                case ((int)RoleList.Administrator):

                    break;
                default: // RoleList.Disabled or No Role
                    break;
            }
        }
    }

    //protected void ddlSites_Callback(object source, CallbackEventArgsBase e)
    //{
    //    FillSitesCombo(e.Parameter);
    //    //ddlSites.SelectedIndex = 0;
    //}
    //pankaj
    //protected void cbQtr_Callback(object source, CallbackEventArgsBase e)
    //{
    //    //if (ddlSites.SelectedItem != null)
    //    FillPeriodCombo(e.Parameter, ddlCompanies.SelectedItem.Value.ToString());
    //}
    // internal
    protected void FillSitesCombo(string companyId)
    {
        if (string.IsNullOrEmpty(companyId)) return;
        SessionHandler.spVar_CompanyId = companyId;

        SitesService sService = new SitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(companyId));

        ddlSites.Items.Clear();
        int _i = 0;

        if (Convert.ToInt32(companyId) > 0)
        {
            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    ddlSites.Items.Add("< Select Site >", "0");
                    foreach (Sites s in sitesList)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (s.SiteId == cscs.SiteId)
                            {
                                ddlSites.Items.Add(s.SiteName, cscs.SiteId);
                            }
                        }
                    }
                }
            }
        }
        ddlSites.SelectedIndex = 0;
        cbQtr.Items.Clear();
        cbQtr.SelectedItem = null;
        CleanUp_Load();
    }

    private void FillPeriodCombo(string siteId, string companyId)
    {
        if (string.IsNullOrEmpty(siteId) || string.IsNullOrEmpty(companyId))
        {
            CleanUp_Load();
            cbQtr.Items.Clear();
        }
        else
        {
            string companySiteCategory = GetCompanySiteCategoryId(siteId, companyId);

            cbQtr.Items.Clear();

            cbQtr.DataSourceID = "topaQtr";
            cbQtr.TextField = "QtrName";
            cbQtr.ValueField = "QtrId";
            cbQtr.DataBind();

            if (companySiteCategory == "2")
            {
                cbQtr.Items.RemoveAt(1);
                cbQtr.Items.RemoveAt(2);
                cbQtr.Items.FindByText("Quarter 1").Text = "Half 1";
                cbQtr.Items.FindByText("Quarter 3").Text = "Half 2";

                if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 6)
                {
                    cbQtr.SelectedIndex = 0;
                }
                else if (DateTime.Now.Month >= 7 && DateTime.Now.Month <= 12)
                {
                    cbQtr.SelectedIndex = 1;
                }
            }
            else if (companySiteCategory == "1")
            {

                if (DateTime.Now.Month >= 1) //Qtr 1 - 1-3
                {
                    cbQtr.SelectedIndex = 0;
                }
                if (DateTime.Now.Month >= 4) //Qtr 2 - 4-6
                {
                    cbQtr.SelectedIndex = 1;
                }
                if (DateTime.Now.Month >= 7) //Qtr 3 - 7-9
                {
                    cbQtr.SelectedIndex = 2;
                }
                if (DateTime.Now.Month >= 10) //Qtr 4 - 10-12
                {
                    cbQtr.SelectedIndex = 3;
                }
            }
            else
            {
                cbQtr.Items.Clear();
                cbQtr.SelectedItem = null;
            }
            if (cbQtr.Items.Count > 0)
            {
                cbQtr.ValidationSettings.Display = Display.None;
            }
        }
    }
    private string GetCompanySiteCategoryId(string siteId, string companyId)
    {
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(companyId));
        string strCategory = string.Empty;

        foreach (CompanySiteCategoryStandard cscsCat in cscsTlist)
        {
            if (cscsCat.SiteId.ToString() == siteId)
            {
                strCategory = cscsCat.CompanySiteCategoryId.ToString();
                break;
            }
        }
        return strCategory;
    }
    protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString());
        //ddlSites.SelectedIndex = 0;
    }
    //pankaj
    protected void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSites.SelectedItem != null)
            if (ddlSites.SelectedIndex == 0)
            {
                cbQtr.Items.Clear();
                cbQtr.SelectedItem = null;
            }
            else
            {
                FillPeriodCombo(ddlSites.SelectedItem.Value.ToString(), ddlCompanies.SelectedItem.Value.ToString());
            }
    }

    protected void Button2_Click1(object sender, EventArgs e)
    {
        int errorCount = 0;
        int errorCountPeriod = 0;
        try
        {
            if (String.IsNullOrEmpty(ddlCompanies.SelectedItem.Text)) { errorCount++; };
            if (String.IsNullOrEmpty(ddlSites.SelectedItem.Text)) { errorCount++; };
            if (String.IsNullOrEmpty(cbQtr.SelectedItem.Text)) { errorCountPeriod++; };
        }
        catch (Exception ex)
        {
            //ToDo: Handle Exception
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            errorCount = 1;
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }

        if (errorCount > 0)
        {
            PopupWindow pcWindow = new PopupWindow("You haven't selected a company and/or site. If no site exists for your company when it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }
        else if (errorCountPeriod > 0)
        {
            PopupWindow pcWindow = new PopupWindow("No period exists for the selected company and site combination. If you think it should, please contact the AUA Contractor Services Team.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
        }
        else
        {
            CSA_Init();
        }
    } // View Button

    private void CSA_Init()
    {
        switch (auth.RoleId)
        {
            case 1: //Reader - Read Only
                CSA_ReadOnly(true);
                CleanUp_Load();
                CSA_Load((int)ddlCompanies.SelectedItem.Value, (int)ddlSites.SelectedItem.Value, (int)cbQtr.SelectedItem.Value, (int)cbYear.SelectedItem.Value);
                if (cbQtr.SelectedItem.Text != "Alcoa Annual Review")
                {
                    btnSave.Visible = true;
                    btnSave.Enabled = false;
                    //TODO: Show Read Only Message to Readers.
                }
                else
                {
                    btnSave.Visible = true;
                    btnSave.Enabled = true;
                }
                break;
            case 2: //Contractor - Edit Own - Can't edit previous quarters
                CleanUp_Load();
                CSA_Load((int)ddlCompanies.SelectedItem.Value, (int)ddlSites.SelectedItem.Value, (int)cbQtr.SelectedItem.Value, (int)cbYear.SelectedItem.Value);

                btnContractorEmployeeSkillsTraining.Visible = btnAdministrativeControls.Visible = btnKpiCompliance.Visible = btnHsCommunication.Visible = btnOfficesYardCribFacilities.Visible = btnAlcoaMandatoryHsTraining.Visible = true;
                if (cbQtr.SelectedItem.Text == "Alcoa Annual Review")
                {
                    btnContractorEmployeeSkillsTraining.Attributes.Add("disabled", "disabled");
                    btnAdministrativeControls.Attributes.Add("disabled", "disabled");
                    btnKpiCompliance.Attributes.Add("disabled", "disabled");
                    btnHsCommunication.Attributes.Add("disabled", "disabled");
                    btnOfficesYardCribFacilities.Attributes.Add("disabled", "disabled");
                    btnAlcoaMandatoryHsTraining.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    btnContractorEmployeeSkillsTraining.Attributes.Remove("disabled");
                    btnAdministrativeControls.Attributes.Remove("disabled");
                    btnKpiCompliance.Attributes.Remove("disabled");
                    btnHsCommunication.Attributes.Remove("disabled");
                    btnOfficesYardCribFacilities.Attributes.Remove("disabled");
                    btnAlcoaMandatoryHsTraining.Attributes.Remove("disabled");
                }

                break;
            case 3: //Administrator - Full Access
                try
                {
                    CleanUp_Load();
                    CSA_Load((int)ddlCompanies.SelectedItem.Value, (int)ddlSites.SelectedItem.Value, (int)cbQtr.SelectedItem.Value, (int)cbYear.SelectedItem.Value);
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    Response.Write(ex.Message);
                }
                break;
            default: //Access Denied.
                break;
        }
    }

    private void CSA_Load(int CompanyId, int SiteId, int QtrId, int Year)
    {
        //System.Threading.Thread.Sleep(500);
        CleanUp_Load();
        SessionHandler.spVar_CompanyId = CompanyId.ToString();
        SessionHandler.spVar_SiteId = SiteId.ToString();
        SessionHandler.spVar_QtrId = QtrId.ToString();
        SessionHandler.spVar_Year = Year.ToString();
        SessionHandler.spVar_TopaId = "";

        CsaService csaService = new CsaService();
        //pankaj
        Csa csa = new Csa();
        if (cbQtr.SelectedItem.Text == "Half1")
        {
            csa = csaService.GetByCompanyIdSiteIdForHalfYear(CompanyId, SiteId, 1, 2, Convert.ToInt64(Year));
        }
        else if (cbQtr.SelectedItem.Text == "Half2")
        {
            csa = csaService.GetByCompanyIdSiteIdForHalfYear(CompanyId, SiteId, 3, 4, Convert.ToInt64(Year));
        }
        else
        {
            csa = csaService.GetByCompanyIdSiteIdQtrIdYear(CompanyId, SiteId, QtrId, Convert.ToInt64(Year));
        }

        if (csa != null) //load values
        {
            UsersService usersService = new UsersService();
            Users user = usersService.GetByUserId(csa.CreatedByUserId);
            if (user != null)
            {
                lblCreatedBy.Text = String.Format("{0}, {1} ({2}) at {3}", user.LastName, user.FirstName, user.UserLogon, csa.DateAdded);
            }
            else
            {
                lblCreatedBy.Text = String.Format("- at {0}", csa.DateAdded);
            }
            Users user2 = usersService.GetByUserId(csa.ModifiedByUserId);
            if (user2 != null)
            {
                lblModifiedBy.Text = String.Format("{0}, {1} ({2}) at {3}", user2.LastName, user2.FirstName, user2.UserLogon, csa.DateModified);
            }
            else
            {
                lblModifiedBy.Text = String.Format("- at {0}", csa.DateModified);
            }

            int csaId = csa.CsaId;

            foreach (string str in section01) { LoadVars(csaId, str); }
            foreach (string str in section02) { LoadVars(csaId, str); }
            foreach (string str in section03) { LoadVars(csaId, str); }
            foreach (string str in section04) { LoadVars(csaId, str); }
            foreach (string str in section05) { LoadVars(csaId, str); }
            foreach (string str in section06) { LoadVars(csaId, str); }

            SessionHandler.spVar_TopaState = "Update"; // Update
            btnSave.Text = "Update Record";
            lblSave.Text = "";
            panelCSA.Visible = true;
            CalcScores_Load(false);
        }
        else //new
        {
            SessionHandler.spVar_TopaState = "New"; //New
            btnSave.Text = "Save New Record";
            lblSave.Text = "No Existing Data Found. Please enter and save.";
            panelCSA.Visible = true;
            CalcScores_Load(true);
        }

    }

    private void LoadVars(int csaId, string q)
    {
        CsaAnswersService csaAnswersService = new CsaAnswersService();
        CsaAnswers csaAnswers = csaAnswersService.GetByCsaIdQuestionNo(csaId, q);

        DropDownList cb = (DropDownList)FindControl("cb" + q);
        try
        {
            if (csaAnswers != null)
            {
                if (csaAnswers.AnswerValue == null)
                {
                    cb.SelectedValue = "-";
                }
                else
                {
                    cb.SelectedValue = csaAnswers.AnswerValue.ToString();
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //ToDo: Handle Exception

            cb.SelectedValue = null;
        }

        TextBox tb = (TextBox)FindControl("o" + q);
        try
        {
            if (tb != null)
            {
                if (csaAnswers != null)
                {
                    if (csaAnswers.Comment != null)
                    {
                        tb.Text = Encoding.ASCII.GetString(csaAnswers.Comment);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //ToDo: Handle Exception
        }

    }

    private void CSA_ReadOnly(bool enable)
    {
        //TODO: Set everything to read only.

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        CSA_Save();
    }

    private void CSA_Save()
    {
        //System.Threading.Thread.Sleep(500);

        CsaService csaService = new CsaService();
        Csa csa = new Csa();
        CsaAnswersService csaAnswersService = new CsaAnswersService();
        CsaAnswers csaAnswers = new CsaAnswers();

        try
        {
            
            if (SessionHandler.spVar_TopaState == "New")
            {
                //DT 3137 Changes 
                //Validation and exception part                
                if (SessionHandler.spVar_CompanyId.ToString().Trim() != "")
                {
                    csa.CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                }
                else 
                {
                    throw new Exception("It appears that the Session has been expired.CompanyId is Null from the session variable.Please try again");
                }
                if (SessionHandler.spVar_SiteId.ToString().Trim() != "")
                {
                    csa.SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);
                }
                else
                {
                    throw new Exception("It appears that the Session has been expired.SiteId is Null from the session variable.Please try again");
                }
                if (SessionHandler.spVar_QtrId.ToString().Trim() != "")
                {
                    csa.QtrId = Convert.ToInt32(SessionHandler.spVar_QtrId);
                }
                else
                {
                    throw new Exception("It appears that the Session has been expired.QtrId is Null from the session variable.Please try again");
                }
                if (SessionHandler.spVar_Year.ToString().Trim() != "")
                {
                    csa.Year = Convert.ToInt32(SessionHandler.spVar_Year);
                }
                else
                {
                    throw new Exception("It appears that the Session has been expired.Year is Null from the session variable.Please try again");
                }
                csa.DateAdded = DateTime.Now;
                csa.DateModified = DateTime.Now;
                csa.CreatedByUserId = auth.UserId;
                csa.ModifiedByUserId = auth.UserId;
                CalcTotals();
                if (scoreTotal_a.Text.ToString().Trim() != "")
                {
                    csa.TotalPossibleScore = Convert.ToInt32(scoreTotal_a.Text); // rename this to TotalApplicableScore
                }
                else
                {
                    throw new Exception("It appears that some mandatory fields has not been filled.Please try again");
                }
                if (scoreTotal_b.Text.ToString().Trim() != "")
                {
                    csa.TotalActualScore = Convert.ToInt32(scoreTotal_b.Text);
                }
                else
                {
                    throw new Exception("It appears that some mandatory fields has not been filled.Please try again");
                }
                if (scoreTotal_c.Text.ToString().Trim() != "")
                {
                    csa.TotalRating = Convert.ToInt32(scoreTotal_c.Text.Replace("%", ""));
                }
                else
                {
                    throw new Exception("It appears that some mandatory fields has not been filled.Please try again");
                }
                csaService.Save(csa);
            }
            if (SessionHandler.spVar_TopaState == "Update")
            {
                //DT 3137 Changes 
                //Validation and exception part
                if (SessionHandler.spVar_CompanyId.ToString().Trim() != "" && SessionHandler.spVar_SiteId.ToString().Trim() != "" && SessionHandler.spVar_QtrId.ToString().Trim() != "" && SessionHandler.spVar_Year.ToString().Trim() != "")
                {
                    csa = csaService.GetByCompanyIdSiteIdQtrIdYear(Convert.ToInt32(SessionHandler.spVar_CompanyId), Convert.ToInt32(SessionHandler.spVar_SiteId), Convert.ToInt32(SessionHandler.spVar_QtrId), Convert.ToInt32(SessionHandler.spVar_Year));
                }
                else
                {
                    throw new Exception("It appears that the Session has been expired. Some mandatory values from session variables are null.Please try again");
                }
                csa.DateModified = DateTime.Now;
                csa.ModifiedByUserId = auth.UserId;
                CalcTotals();
                if (scoreTotal_a.Text.ToString().Trim() != "")
                {
                    csa.TotalPossibleScore = Convert.ToInt32(scoreTotal_a.Text); // rename this to TotalApplicableScore
                }
                else
                {
                    throw new Exception("It appears that some mandatory fields has not been filled.Please try again");
                }
                if (scoreTotal_b.Text.ToString().Trim() != "")
                {
                    csa.TotalActualScore = Convert.ToInt32(scoreTotal_b.Text);
                }
                else
                {
                    throw new Exception("It appears that some mandatory fields has not been filled.Please try again");
                }
                if (scoreTotal_c.Text.ToString().Trim() != "")
                {
                    csa.TotalRating = Convert.ToInt32(scoreTotal_c.Text.Replace("%", ""));
                }
                else
                {
                    throw new Exception("It appears that some mandatory fields has not been filled.Please try again");
                }
                if (csa != null)
                {
                    csaService.Save(csa);
                }
            }

            if (csa.CsaId > 0) //valid
            {

                string[] allSections = new string[section01.Length + section02.Length + section03.Length +
                                                  section04.Length + section05.Length + section06.Length];
                section01.CopyTo(allSections, 0);
                section02.CopyTo(allSections, section01.Length);
                section03.CopyTo(allSections, section01.Length + section02.Length);
                section04.CopyTo(allSections, section01.Length + section02.Length + section03.Length);
                section05.CopyTo(allSections, section01.Length + section02.Length + section03.Length +
                                                section04.Length);
                section06.CopyTo(allSections, section01.Length + section02.Length + section03.Length +
                                                section04.Length + section05.Length);

                foreach (string str in allSections)
                {
                    if (GetCBValue(str) != -1)
                    {
                        csaAnswers = new CsaAnswers();
                        if (SessionHandler.spVar_TopaState == "Update")
                        {
                            csaAnswers = csaAnswersService.GetByCsaIdQuestionNo(csa.CsaId, str);
                            if (csaAnswers == null) csaAnswers = new CsaAnswers();
                        }
                        csaAnswers.CsaId = csa.CsaId;
                        csaAnswers.QuestionNo = str;
                        csaAnswers.AnswerValue = GetCBValue(str);
                        csaAnswers.Comment = GetCommentValue(str);
                        csaAnswersService.Save(csaAnswers);
                    }
                    else
                    {
                        if (SessionHandler.spVar_TopaState == "Update")
                        {
                            csaAnswers = csaAnswersService.GetByCsaIdQuestionNo(csa.CsaId, str);
                        }
                        if (csaAnswers != null)
                        {
                            //csaAnswersService.Delete(csaAnswers);
                        }
                    }
                }
            }
            if (SessionHandler.spVar_TopaState == "New")
            {
                SessionHandler.spVar_TopaState = "Update";
                //CSA_Load(Convert.ToInt32(SessionHandler.spVar_CompanyId),
                //         Convert.ToInt32(SessionHandler.spVar_SiteId),
                //         Convert.ToInt32(SessionHandler.spVar_QtrId),
                //         Convert.ToInt32(SessionHandler.spVar_Year));
                lblSave.Text = "Record Created and Saved successfully.";
                btnSave.Text = "Update Record";

            }
            if (SessionHandler.spVar_TopaState == "Update")
            {
                //CSA_Load(Convert.ToInt32(SessionHandler.spVar_CompanyId),
                //         Convert.ToInt32(SessionHandler.spVar_SiteId),
                //         Convert.ToInt32(SessionHandler.spVar_QtrId),
                //         Convert.ToInt32(SessionHandler.spVar_Year));
                lblSave.Text = "Record Updated and Saved successfully.";
                btnSave.Text = "Update Record";
            }

        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblSave.Text = "Not Saved, Please Try Again. Error Occured: " + ex.Message;
        }

    }
    protected int CalcTotalScoreA(string id)
    {
        return 1;
    }
    protected int CalcTotalScoreB(int id)
    {
        string Achieved = ""; //Textbox
        double total = 0;

        switch (id)
        {
            case 01:
                foreach (string str in section01)
                {
                    DropDownList oAchievedCtl;

                    Control oCtlAchieved = FindControl("cb" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.SelectedValue;

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                break;
            case 02:
                foreach (string str in section02)
                {
                    DropDownList oAchievedCtl;

                    Control oCtlAchieved = FindControl("cb" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.SelectedValue;

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                break;
            case 03:
                foreach (string str in section03)
                {
                    DropDownList oAchievedCtl;

                    Control oCtlAchieved = FindControl("cb" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.SelectedValue;

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                break;
            case 04:
                foreach (string str in section04)
                {
                    DropDownList oAchievedCtl;

                    Control oCtlAchieved = FindControl("cb" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.SelectedValue;

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                break;
            case 05:
                foreach (string str in section05)
                {
                    DropDownList oAchievedCtl;

                    Control oCtlAchieved = FindControl("cb" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.SelectedValue; ;

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                break;
            case 06:
                foreach (string str in section06)
                {
                    DropDownList oAchievedCtl;

                    Control oCtlAchieved = FindControl("cb" + str);
                    oAchievedCtl = (DropDownList)oCtlAchieved;
                    Achieved = oAchievedCtl.SelectedValue;

                    double temp = 0.0;
                    try
                    {
                        if (!String.IsNullOrEmpty(Achieved))
                        { temp = Convert.ToInt32(Achieved); }
                        else
                        { temp = 0; }
                    }
                    catch { temp = 0; }
                    total += temp;
                }
                break;
            default:
                break;
        }
        return Convert.ToInt32(total);
    }
    protected void CalcScore(string id)
    {
        string js = "";
        DropDownList cb;
        Label score;
        Control oCtlcb = FindControl("cb" + id);
        cb = (DropDownList)oCtlcb;

        Control oCtlScore = FindControl("score" + id);
        score = (Label)oCtlScore;
        js = String.Format("(document.getElementById('ctl00_contentBody_CSA1_score{0}').innerText = ((document.getElementById('ctl00_contentBody_CSA1_cb{0}').value))); return false;", id);
        cb.Attributes.Add("onBlur", "Javascript:" + js);

        score.Text = cb.SelectedValue;
    }
    protected void CalcScores_Load(bool new_)
    {
        foreach (string str in section01) { CalcScore(str); }
        foreach (string str in section02) { CalcScore(str); }
        foreach (string str in section03) { CalcScore(str); }
        foreach (string str in section04) { CalcScore(str); }
        foreach (string str in section05) { CalcScore(str); }
        foreach (string str in section06) { CalcScore(str); }

        if (new_ == false) CalcTotals();
    }

    protected int? GetCBValue(string id)
    {
        Control oCtlcb = this.FindControl("cb" + id);
        //cb = (ASPxComboBox)oCtlcb;
        DropDownList cb = (DropDownList)oCtlcb;
        int? i = -1;
        try
        {

            if (cb.SelectedValue == "-" || String.IsNullOrEmpty(cb.SelectedValue))
            {
                i = null;
            }
            else
            {
                i = Convert.ToInt32(cb.SelectedValue);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //ToDo: Handle Exception
        }

        if (String.IsNullOrEmpty(cb.Text)) { i = -1; };
        return i;
    }

    protected byte[] GetCommentValue(string id)
    {
        Control oCtlcb = this.FindControl("o" + id);
        TextBox tb = (TextBox)oCtlcb;
        ASCIIEncoding encoding = new ASCIIEncoding();
        return (byte[])encoding.GetBytes(tb.Text);
    }
    protected void CleanUp_Load()
    {
        foreach (string str in section01) { CleanUp(str); }
        foreach (string str in section02) { CleanUp(str); }
        foreach (string str in section03) { CleanUp(str); }
        foreach (string str in section04) { CleanUp(str); }
        foreach (string str in section05) { CleanUp(str); }
        foreach (string str in section06) { CleanUp(str); }
        lblSave.Text = "";
        panelCSA.Visible = false;

        lblCreatedBy.Text = "-";
        lblModifiedBy.Text = "-";

        score1_a.Text = "";
        score2_a.Text = "";
        score3_a.Text = "";
        score4_a.Text = "";
        score5_a.Text = "";
        score6_a.Text = "";
        score1_b.Text = "";
        score2_b.Text = "";
        score3_b.Text = "";
        score4_b.Text = "";
        score5_b.Text = "";
        score6_b.Text = "";
        score1_c.Text = "";
        score2_c.Text = "";
        score3_c.Text = "";
        score4_c.Text = "";
        score5_c.Text = "";
        score6_c.Text = "";

        score1_a_.Text = "";
        score2_a_.Text = "";
        score3_a_.Text = "";
        score4_a_.Text = "";
        score5_a_.Text = "";
        score6_a_.Text = "";
        score1_b_.Text = "";
        score2_b_.Text = "";
        score3_b_.Text = "";
        score4_b_.Text = "";
        score5_b_.Text = "";
        score6_b_.Text = "";
        score1_c_.Text = "";
        score2_c_.Text = "";
        score3_c_.Text = "";
        score4_c_.Text = "";
        score5_c_.Text = "";
        score6_c_.Text = "";

        scoreTotal_a.Text = "";
        scoreTotal_b.Text = "";
        scoreTotal_c.Text = "";

    }
    protected void CleanUp(string id)
    {
        DropDownList oAchievedCtl;
        Label oScoreCtl;
        TextBox oObservationCtl;

        Control oCtlAchieved = FindControl("cb" + id);
        oAchievedCtl = (DropDownList)oCtlAchieved;
        oAchievedCtl.Text = "";
        oAchievedCtl.SelectedValue = null;

        Control oCtlScore = FindControl("score" + id);
        oScoreCtl = (Label)oCtlScore;
        oScoreCtl.Text = "";

        Control oCtlObservation = FindControl("o" + id);
        oObservationCtl = (TextBox)oCtlObservation;
        oObservationCtl.Text = "";
    }

    protected void CalcTotals()
    {
        CalcTotal(section01, 1);
        CalcTotal(section02, 2);
        CalcTotal(section03, 3);
        CalcTotal(section04, 4);
        CalcTotal(section05, 5);
        CalcTotal(section06, 6);

        int Total_applicable = 0;
        int Total_score = 0;
        int Total_perc = 0;

        Total_applicable = Convert.ToInt32(score1_a_.Text) + Convert.ToInt32(score2_a_.Text) +
                            Convert.ToInt32(score3_a_.Text) + Convert.ToInt32(score4_a_.Text) +
                            Convert.ToInt32(score5_a_.Text) + Convert.ToInt32(score6_a_.Text);
        Total_score = Convert.ToInt32(score1_b_.Text) + Convert.ToInt32(score2_b_.Text) +
                        Convert.ToInt32(score3_b_.Text) + Convert.ToInt32(score4_b_.Text) +
                        Convert.ToInt32(score5_b_.Text) + Convert.ToInt32(score6_b_.Text);
        if (Total_applicable > 0 && Total_score > 0)
        {
            Total_perc = Convert.ToInt32((Convert.ToDouble(Total_score) /
                                    Convert.ToDouble(Total_applicable)) * 100);
        }

        scoreTotal_a.Text = Total_applicable.ToString();
        scoreTotal_b.Text = Total_score.ToString();
        scoreTotal_c.Text = Total_perc.ToString() + "%";

        if (Total_perc < 80 && Total_applicable > 0)
        {
            scoreTotal_c.ForeColor = Color.Red;
            scoreTotal_c.Font.Bold = true;
        }
        else
        {
            scoreTotal_c.ForeColor = Color.Black;
            scoreTotal_c.Font.Bold = false;
        }
    }

    protected void CalcTotal(string[] section, int no)
    {
        int section_applicable = 0;
        int section_score = 0;
        int section_perc = 0;
        int number = Convert.ToInt32(no);
        foreach (string str in section)
        {
            DropDownList cb;
            Control oCtlcb = FindControl("cb" + str);
            cb = (DropDownList)oCtlcb;
            if (cb.SelectedValue != "-")
            {
                section_applicable += 5;
                if (!String.IsNullOrEmpty(cb.SelectedValue))
                {
                    section_score += Convert.ToInt32(cb.SelectedValue);
                }
            }
        }
        if (section_applicable > 0 && section_score > 0)
        {
            section_perc = Convert.ToInt32((Convert.ToDouble(section_score) /
                                Convert.ToDouble(section_applicable)) * 100);
        }
        Control oCtlScore_a = FindControl(String.Format("score{0}_a", number));
        Label score_a = (Label)oCtlScore_a;
        Control oCtlScore_b = FindControl(String.Format("score{0}_b", number));
        Label score_b = (Label)oCtlScore_b;
        Control oCtlScore_c = FindControl(String.Format("score{0}_c", number));
        Label score_c = (Label)oCtlScore_c;
        Control oCtlScore_a_ = FindControl(String.Format("score{0}_a_", number));
        Label score_a_ = (Label)oCtlScore_a_;
        Control oCtlScore_b_ = FindControl(String.Format("score{0}_b_", number));
        Label score_b_ = (Label)oCtlScore_b_;
        Control oCtlScore_c_ = FindControl(String.Format("score{0}_c_", number));
        Label score_c_ = (Label)oCtlScore_c_;

        score_a.Text = section_applicable.ToString();
        score_b.Text = section_score.ToString();
        score_c.Text = section_perc.ToString() + "%";

        score_a_.Text = score_a.Text;
        score_b_.Text = score_b.Text;
        score_c_.Text = score_c.Text;
        if (section_perc < 80 && section_applicable > 0)
        {
            score_c_.ForeColor = System.Drawing.Color.Red;
            score_c_.Font.Bold = true;
        }
        else
        {
            score_c.ForeColor = System.Drawing.Color.Black;
            score_c_.Font.Bold = false;
        }
    }










    //// internal
    //protected void FillSitesCombocbQtr(string companyId)
    //{
    //    if (string.IsNullOrEmpty(companyId)) return;
    //    SessionHandler.spVar_CompanyId = companyId;

    //    SitesService sService = new SitesService();
    //    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
    //    TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(companyId));

    //    ddlSites.Items.Clear();
    //    int _i = 0;

    //    if (Convert.ToInt32(companyId) > 0)
    //    {
    //        if (cscsTlist != null)
    //        {
    //            if (cscsTlist.Count > 0)
    //            {
    //                SitesFilters sitesFilters = new SitesFilters();
    //                sitesFilters.Append(SitesColumn.IsVisible, "1");
    //                TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
    //                foreach (Sites s in sitesList)
    //                {
    //                    foreach (CompanySiteCategoryStandard cscs in cscsTlist)
    //                    {
    //                        if (s.SiteId == cscs.SiteId)
    //                        {
    //                            ddlSites.Items.Add(s.SiteName, cscs.SiteId);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }



    //}
}