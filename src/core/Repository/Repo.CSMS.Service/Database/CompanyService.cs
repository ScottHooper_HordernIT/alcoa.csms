﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity.Core.Objects;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyService
    {
        Company Get(Expression<Func<Company, bool>> where, List<Expression<Func<Company, object>>> includes);
        List<Company> GetMany(int? top, Expression<Func<Company, bool>> where, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy, List<Expression<Func<Company, object>>> includes);
        List<Company> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Company, bool>> where, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy, List<Expression<Func<Company, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Company, bool>> where, List<Expression<Func<Company, object>>> includes);
        void Insert(Company item, bool saveChanges = true);
        void Update(Company item, bool saveChanges = true);
        void Delete(Company item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyService : ICompanyService
    {
        private readonly IRepository<Company> _repository;

        public CompanyService(IRepository<Company> repository)
        {
            this._repository = repository;
        }

        public Company Get(Expression<Func<Company, bool>> where, List<Expression<Func<Company, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Company> GetMany(int? top, Expression<Func<Company, bool>> where, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy, List<Expression<Func<Company, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Company> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Company, bool>> where, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy, List<Expression<Func<Company, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Company, bool>> where, List<Expression<Func<Company, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Company item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Company");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Company item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Company");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Company item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Company");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
