using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using System.Text;

public partial class Adminv2_UserControls_News : System.Web.UI.UserControl
{

    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                News2Service n2Service = new News2Service();
                News2 n2 = n2Service.GetByNews2Id(1);
                if (n2 == null)
                {
                    n2 = new News2();
                    n2.News2Id = 1;
                    n2Service.Save(n2);
                }
                if (n2.Content != null)
                {
                    ASPxHtmlEditor1.Html = Encoding.ASCII.GetString(n2.Content);
                }
                else
                {
                    ASPxHtmlEditor1.Html = "";
                }
                lblNewsSaveStatus.Text = "Loaded. Not Yet Saved";
            }
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        News2Service n2Service = new News2Service();
        News2 n2 = n2Service.GetByNews2Id(1);
        if (n2 == null)
        {
            n2 = new News2();
            n2.News2Id = 1;
            n2Service.Save(n2);
        }
        else
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            n2.Content = (byte[])encoding.GetBytes(ASPxHtmlEditor1.Html);
            n2Service.Save(n2);
            lblNewsSaveStatus.Text = "Saved at: " + DateTime.Now.ToString();
        }
    }
}
