﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersCompaniesActiveContractorsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersCompaniesActiveContractorsProviderBaseCore : EntityViewProviderBase<UsersCompaniesActiveContractors>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersCompaniesActiveContractors&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersCompaniesActiveContractors&gt;"/></returns>
		protected static VList&lt;UsersCompaniesActiveContractors&gt; Fill(DataSet dataSet, VList<UsersCompaniesActiveContractors> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersCompaniesActiveContractors>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersCompaniesActiveContractors&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersCompaniesActiveContractors>"/></returns>
		protected static VList&lt;UsersCompaniesActiveContractors&gt; Fill(DataTable dataTable, VList<UsersCompaniesActiveContractors> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersCompaniesActiveContractors c = new UsersCompaniesActiveContractors();
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.UserFullNameLogonCompany = (Convert.IsDBNull(row["UserFullNameLogonCompany"]))?string.Empty:(System.String)row["UserFullNameLogonCompany"];
					c.CompanyUserFullNameLogon = (Convert.IsDBNull(row["CompanyUserFullNameLogon"]))?string.Empty:(System.String)row["CompanyUserFullNameLogon"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersCompaniesActiveContractors&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersCompaniesActiveContractors&gt;"/></returns>
		protected VList<UsersCompaniesActiveContractors> Fill(IDataReader reader, VList<UsersCompaniesActiveContractors> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersCompaniesActiveContractors entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersCompaniesActiveContractors>("UsersCompaniesActiveContractors",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersCompaniesActiveContractors();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UserId = (System.Int32)reader[((int)UsersCompaniesActiveContractorsColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveContractorsColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.UserFullNameLogonCompany = (System.String)reader[((int)UsersCompaniesActiveContractorsColumn.UserFullNameLogonCompany)];
					//entity.UserFullNameLogonCompany = (Convert.IsDBNull(reader["UserFullNameLogonCompany"]))?string.Empty:(System.String)reader["UserFullNameLogonCompany"];
					entity.CompanyUserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveContractorsColumn.CompanyUserFullNameLogon)];
					//entity.CompanyUserFullNameLogon = (Convert.IsDBNull(reader["CompanyUserFullNameLogon"]))?string.Empty:(System.String)reader["CompanyUserFullNameLogon"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersCompaniesActiveContractors"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersCompaniesActiveContractors"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersCompaniesActiveContractors entity)
		{
			reader.Read();
			entity.UserId = (System.Int32)reader[((int)UsersCompaniesActiveContractorsColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveContractorsColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.UserFullNameLogonCompany = (System.String)reader[((int)UsersCompaniesActiveContractorsColumn.UserFullNameLogonCompany)];
			//entity.UserFullNameLogonCompany = (Convert.IsDBNull(reader["UserFullNameLogonCompany"]))?string.Empty:(System.String)reader["UserFullNameLogonCompany"];
			entity.CompanyUserFullNameLogon = (System.String)reader[((int)UsersCompaniesActiveContractorsColumn.CompanyUserFullNameLogon)];
			//entity.CompanyUserFullNameLogon = (Convert.IsDBNull(reader["CompanyUserFullNameLogon"]))?string.Empty:(System.String)reader["CompanyUserFullNameLogon"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersCompaniesActiveContractors"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersCompaniesActiveContractors"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersCompaniesActiveContractors entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.UserFullNameLogonCompany = (Convert.IsDBNull(dataRow["UserFullNameLogonCompany"]))?string.Empty:(System.String)dataRow["UserFullNameLogonCompany"];
			entity.CompanyUserFullNameLogon = (Convert.IsDBNull(dataRow["CompanyUserFullNameLogon"]))?string.Empty:(System.String)dataRow["CompanyUserFullNameLogon"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersCompaniesActiveContractorsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveContractorsFilterBuilder : SqlFilterBuilder<UsersCompaniesActiveContractorsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsFilterBuilder class.
		/// </summary>
		public UsersCompaniesActiveContractorsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveContractorsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveContractorsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveContractorsFilterBuilder

	#region UsersCompaniesActiveContractorsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveContractorsParameterBuilder : ParameterizedSqlFilterBuilder<UsersCompaniesActiveContractorsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsParameterBuilder class.
		/// </summary>
		public UsersCompaniesActiveContractorsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersCompaniesActiveContractorsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersCompaniesActiveContractorsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersCompaniesActiveContractorsParameterBuilder
	
	#region UsersCompaniesActiveContractorsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersCompaniesActiveContractorsSortBuilder : SqlSortBuilder<UsersCompaniesActiveContractorsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsSqlSortBuilder class.
		/// </summary>
		public UsersCompaniesActiveContractorsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersCompaniesActiveContractorsSortBuilder

} // end namespace
