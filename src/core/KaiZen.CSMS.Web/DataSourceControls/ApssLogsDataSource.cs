﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ApssLogsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ApssLogsDataSourceDesigner))]
	public class ApssLogsDataSource : ProviderDataSource<ApssLogs, ApssLogsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsDataSource class.
		/// </summary>
		public ApssLogsDataSource() : base(new ApssLogsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ApssLogsDataSourceView used by the ApssLogsDataSource.
		/// </summary>
		protected ApssLogsDataSourceView ApssLogsView
		{
			get { return ( View as ApssLogsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ApssLogsDataSource control invokes to retrieve data.
		/// </summary>
		public ApssLogsSelectMethod SelectMethod
		{
			get
			{
				ApssLogsSelectMethod selectMethod = ApssLogsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ApssLogsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ApssLogsDataSourceView class that is to be
		/// used by the ApssLogsDataSource.
		/// </summary>
		/// <returns>An instance of the ApssLogsDataSourceView class.</returns>
		protected override BaseDataSourceView<ApssLogs, ApssLogsKey> GetNewDataSourceView()
		{
			return new ApssLogsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ApssLogsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ApssLogsDataSourceView : ProviderDataSourceView<ApssLogs, ApssLogsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ApssLogsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ApssLogsDataSourceView(ApssLogsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ApssLogsDataSource ApssLogsOwner
		{
			get { return Owner as ApssLogsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ApssLogsSelectMethod SelectMethod
		{
			get { return ApssLogsOwner.SelectMethod; }
			set { ApssLogsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ApssLogsService ApssLogsProvider
		{
			get { return Provider as ApssLogsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ApssLogs> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ApssLogs> results = null;
			ApssLogs item;
			count = 0;
			
			System.Int32 _entryId;
			System.DateTime _timeStamp;
			System.String _fileName;
			System.String _fileTag;

			switch ( SelectMethod )
			{
				case ApssLogsSelectMethod.Get:
					ApssLogsKey entityKey  = new ApssLogsKey();
					entityKey.Load(values);
					item = ApssLogsProvider.Get(entityKey);
					results = new TList<ApssLogs>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ApssLogsSelectMethod.GetAll:
                    results = ApssLogsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ApssLogsSelectMethod.GetPaged:
					results = ApssLogsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ApssLogsSelectMethod.Find:
					if ( FilterParameters != null )
						results = ApssLogsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ApssLogsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ApssLogsSelectMethod.GetByEntryId:
					_entryId = ( values["EntryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EntryId"], typeof(System.Int32)) : (int)0;
					item = ApssLogsProvider.GetByEntryId(_entryId);
					results = new TList<ApssLogs>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case ApssLogsSelectMethod.GetByTimeStampFileNameFileTag:
					_timeStamp = ( values["TimeStamp"] != null ) ? (System.DateTime) EntityUtil.ChangeType(values["TimeStamp"], typeof(System.DateTime)) : DateTime.MinValue;
					_fileName = ( values["FileName"] != null ) ? (System.String) EntityUtil.ChangeType(values["FileName"], typeof(System.String)) : string.Empty;
					_fileTag = ( values["FileTag"] != null ) ? (System.String) EntityUtil.ChangeType(values["FileTag"], typeof(System.String)) : string.Empty;
					item = ApssLogsProvider.GetByTimeStampFileNameFileTag(_timeStamp, _fileName, _fileTag);
					results = new TList<ApssLogs>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ApssLogsSelectMethod.Get || SelectMethod == ApssLogsSelectMethod.GetByEntryId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ApssLogs entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ApssLogsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ApssLogs> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ApssLogsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ApssLogsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ApssLogsDataSource class.
	/// </summary>
	public class ApssLogsDataSourceDesigner : ProviderDataSourceDesigner<ApssLogs, ApssLogsKey>
	{
		/// <summary>
		/// Initializes a new instance of the ApssLogsDataSourceDesigner class.
		/// </summary>
		public ApssLogsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ApssLogsSelectMethod SelectMethod
		{
			get { return ((ApssLogsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ApssLogsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ApssLogsDataSourceActionList

	/// <summary>
	/// Supports the ApssLogsDataSourceDesigner class.
	/// </summary>
	internal class ApssLogsDataSourceActionList : DesignerActionList
	{
		private ApssLogsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ApssLogsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ApssLogsDataSourceActionList(ApssLogsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ApssLogsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ApssLogsDataSourceActionList
	
	#endregion ApssLogsDataSourceDesigner
	
	#region ApssLogsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ApssLogsDataSource.SelectMethod property.
	/// </summary>
	public enum ApssLogsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEntryId method.
		/// </summary>
		GetByEntryId,
		/// <summary>
		/// Represents the GetByTimeStampFileNameFileTag method.
		/// </summary>
		GetByTimeStampFileNameFileTag
	}
	
	#endregion ApssLogsSelectMethod

	#region ApssLogsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsFilter : SqlFilter<ApssLogsColumn>
	{
	}
	
	#endregion ApssLogsFilter

	#region ApssLogsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsExpressionBuilder : SqlExpressionBuilder<ApssLogsColumn>
	{
	}
	
	#endregion ApssLogsExpressionBuilder	

	#region ApssLogsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ApssLogsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsProperty : ChildEntityProperty<ApssLogsChildEntityTypes>
	{
	}
	
	#endregion ApssLogsProperty
}

