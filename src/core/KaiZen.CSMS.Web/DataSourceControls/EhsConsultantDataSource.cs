﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EhsConsultantProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EhsConsultantDataSourceDesigner))]
	public class EhsConsultantDataSource : ProviderDataSource<EhsConsultant, EhsConsultantKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantDataSource class.
		/// </summary>
		public EhsConsultantDataSource() : base(new EhsConsultantService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EhsConsultantDataSourceView used by the EhsConsultantDataSource.
		/// </summary>
		protected EhsConsultantDataSourceView EhsConsultantView
		{
			get { return ( View as EhsConsultantDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EhsConsultantDataSource control invokes to retrieve data.
		/// </summary>
		public EhsConsultantSelectMethod SelectMethod
		{
			get
			{
				EhsConsultantSelectMethod selectMethod = EhsConsultantSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EhsConsultantSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EhsConsultantDataSourceView class that is to be
		/// used by the EhsConsultantDataSource.
		/// </summary>
		/// <returns>An instance of the EhsConsultantDataSourceView class.</returns>
		protected override BaseDataSourceView<EhsConsultant, EhsConsultantKey> GetNewDataSourceView()
		{
			return new EhsConsultantDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EhsConsultantDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EhsConsultantDataSourceView : ProviderDataSourceView<EhsConsultant, EhsConsultantKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EhsConsultantDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EhsConsultantDataSourceView(EhsConsultantDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EhsConsultantDataSource EhsConsultantOwner
		{
			get { return Owner as EhsConsultantDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EhsConsultantSelectMethod SelectMethod
		{
			get { return EhsConsultantOwner.SelectMethod; }
			set { EhsConsultantOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EhsConsultantService EhsConsultantProvider
		{
			get { return Provider as EhsConsultantService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EhsConsultant> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EhsConsultant> results = null;
			EhsConsultant item;
			count = 0;
			
			System.Int32 _ehsConsultantId;
			System.Int32 _userId;
			System.Int32? _supervisorUserId_nullable;

			switch ( SelectMethod )
			{
				case EhsConsultantSelectMethod.Get:
					EhsConsultantKey entityKey  = new EhsConsultantKey();
					entityKey.Load(values);
					item = EhsConsultantProvider.Get(entityKey);
					results = new TList<EhsConsultant>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EhsConsultantSelectMethod.GetAll:
                    results = EhsConsultantProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EhsConsultantSelectMethod.GetPaged:
					results = EhsConsultantProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EhsConsultantSelectMethod.Find:
					if ( FilterParameters != null )
						results = EhsConsultantProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EhsConsultantProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EhsConsultantSelectMethod.GetByEhsConsultantId:
					_ehsConsultantId = ( values["EhsConsultantId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EhsConsultantId"], typeof(System.Int32)) : (int)0;
					item = EhsConsultantProvider.GetByEhsConsultantId(_ehsConsultantId);
					results = new TList<EhsConsultant>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EhsConsultantSelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					item = EhsConsultantProvider.GetByUserId(_userId);
					results = new TList<EhsConsultant>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case EhsConsultantSelectMethod.GetBySupervisorUserId:
					_supervisorUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["SupervisorUserId"], typeof(System.Int32?));
					results = EhsConsultantProvider.GetBySupervisorUserId(_supervisorUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EhsConsultantSelectMethod.Get || SelectMethod == EhsConsultantSelectMethod.GetByEhsConsultantId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EhsConsultant entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EhsConsultantProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EhsConsultant> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EhsConsultantProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EhsConsultantDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EhsConsultantDataSource class.
	/// </summary>
	public class EhsConsultantDataSourceDesigner : ProviderDataSourceDesigner<EhsConsultant, EhsConsultantKey>
	{
		/// <summary>
		/// Initializes a new instance of the EhsConsultantDataSourceDesigner class.
		/// </summary>
		public EhsConsultantDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EhsConsultantSelectMethod SelectMethod
		{
			get { return ((EhsConsultantDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EhsConsultantDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EhsConsultantDataSourceActionList

	/// <summary>
	/// Supports the EhsConsultantDataSourceDesigner class.
	/// </summary>
	internal class EhsConsultantDataSourceActionList : DesignerActionList
	{
		private EhsConsultantDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EhsConsultantDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EhsConsultantDataSourceActionList(EhsConsultantDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EhsConsultantSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EhsConsultantDataSourceActionList
	
	#endregion EhsConsultantDataSourceDesigner
	
	#region EhsConsultantSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EhsConsultantDataSource.SelectMethod property.
	/// </summary>
	public enum EhsConsultantSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEhsConsultantId method.
		/// </summary>
		GetByEhsConsultantId,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId,
		/// <summary>
		/// Represents the GetBySupervisorUserId method.
		/// </summary>
		GetBySupervisorUserId
	}
	
	#endregion EhsConsultantSelectMethod

	#region EhsConsultantFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantFilter : SqlFilter<EhsConsultantColumn>
	{
	}
	
	#endregion EhsConsultantFilter

	#region EhsConsultantExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantExpressionBuilder : SqlExpressionBuilder<EhsConsultantColumn>
	{
	}
	
	#endregion EhsConsultantExpressionBuilder	

	#region EhsConsultantProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EhsConsultantChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantProperty : ChildEntityProperty<EhsConsultantChildEntityTypes>
	{
	}
	
	#endregion EhsConsultantProperty
}

