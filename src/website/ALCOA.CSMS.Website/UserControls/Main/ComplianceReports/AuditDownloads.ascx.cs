using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;

public partial class UserControls_AuditDownloads : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    grid.DataSourceID = "dsDocumentsDownloadLog_Report";
                    grid.DataBind();
                    //Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("User of Reader role can not access this module."));
                    break;
                case ((int)RoleList.Contractor):
                    grid.DataSourceID = "dsDocumentsDownloadLog_Report_ByCompany";
                    grid.DataBind();

                    break;
                case ((int)RoleList.Administrator):
                    grid.DataSourceID = "dsDocumentsDownloadLog_Report";
                    //grid.DataSourceID = "sqldsAll";
                    grid.DataBind();
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Non Compliance Report - Document Access History"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
    }

    protected void ddlPager_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPager.SelectedValue != "All")
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = Convert.ToInt32(ddlPager.SelectedValue);
        }
        else
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
        }
        grid.DataBind();
    }
}
