﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailTemplateListWithAttachmentsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class AdminTaskEmailTemplateListWithAttachmentsProviderBaseCore : EntityViewProviderBase<AdminTaskEmailTemplateListWithAttachments>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;AdminTaskEmailTemplateListWithAttachments&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;AdminTaskEmailTemplateListWithAttachments&gt;"/></returns>
		protected static VList&lt;AdminTaskEmailTemplateListWithAttachments&gt; Fill(DataSet dataSet, VList<AdminTaskEmailTemplateListWithAttachments> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<AdminTaskEmailTemplateListWithAttachments>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;AdminTaskEmailTemplateListWithAttachments&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<AdminTaskEmailTemplateListWithAttachments>"/></returns>
		protected static VList&lt;AdminTaskEmailTemplateListWithAttachments&gt; Fill(DataTable dataTable, VList<AdminTaskEmailTemplateListWithAttachments> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					AdminTaskEmailTemplateListWithAttachments c = new AdminTaskEmailTemplateListWithAttachments();
					c.AdminTaskEmailTemplateId = (Convert.IsDBNull(row["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)row["AdminTaskEmailTemplateId"];
					c.AdminTaskTypeId = (Convert.IsDBNull(row["AdminTaskTypeId"]))?(int)0:(System.Int32)row["AdminTaskTypeId"];
					c.EmailSubject = (Convert.IsDBNull(row["EmailSubject"]))?string.Empty:(System.String)row["EmailSubject"];
					c.EmailBodyAlcoaLan = (Convert.IsDBNull(row["EmailBodyAlcoaLan"]))?new byte[] {}:(System.Byte[])row["EmailBodyAlcoaLan"];
					c.EmailBodyAlcoaDirectNewUser = (Convert.IsDBNull(row["EmailBodyAlcoaDirectNewUser"]))?new byte[] {}:(System.Byte[])row["EmailBodyAlcoaDirectNewUser"];
					c.EmailBodyAlcoaDirectExistingUser = (Convert.IsDBNull(row["EmailBodyAlcoaDirectExistingUser"]))?new byte[] {}:(System.Byte[])row["EmailBodyAlcoaDirectExistingUser"];
					c.CsmsFileId = (Convert.IsDBNull(row["CsmsFileId"]))?(int)0:(System.Int32)row["CsmsFileId"];
					c.CsmsFileTypeId = (Convert.IsDBNull(row["CsmsFileTypeId"]))?(int)0:(System.Int32)row["CsmsFileTypeId"];
					c.FileName = (Convert.IsDBNull(row["FileName"]))?string.Empty:(System.String)row["FileName"];
					c.Description = (Convert.IsDBNull(row["Description"]))?string.Empty:(System.String)row["Description"];
					c.FileHash = (Convert.IsDBNull(row["FileHash"]))?new byte[] {}:(System.Byte[])row["FileHash"];
					c.ContentLength = (Convert.IsDBNull(row["ContentLength"]))?(int)0:(System.Int32)row["ContentLength"];
					c.IsVisible = (Convert.IsDBNull(row["IsVisible"]))?false:(System.Boolean)row["IsVisible"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;AdminTaskEmailTemplateListWithAttachments&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;AdminTaskEmailTemplateListWithAttachments&gt;"/></returns>
		protected VList<AdminTaskEmailTemplateListWithAttachments> Fill(IDataReader reader, VList<AdminTaskEmailTemplateListWithAttachments> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					AdminTaskEmailTemplateListWithAttachments entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<AdminTaskEmailTemplateListWithAttachments>("AdminTaskEmailTemplateListWithAttachments",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new AdminTaskEmailTemplateListWithAttachments();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.AdminTaskEmailTemplateId)];
					//entity.AdminTaskEmailTemplateId = (Convert.IsDBNull(reader["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)reader["AdminTaskEmailTemplateId"];
					entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.AdminTaskTypeId)];
					//entity.AdminTaskTypeId = (Convert.IsDBNull(reader["AdminTaskTypeId"]))?(int)0:(System.Int32)reader["AdminTaskTypeId"];
					entity.EmailSubject = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailSubject)))?null:(System.String)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailSubject)];
					//entity.EmailSubject = (Convert.IsDBNull(reader["EmailSubject"]))?string.Empty:(System.String)reader["EmailSubject"];
					entity.EmailBodyAlcoaLan = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaLan)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaLan)];
					//entity.EmailBodyAlcoaLan = (Convert.IsDBNull(reader["EmailBodyAlcoaLan"]))?new byte[] {}:(System.Byte[])reader["EmailBodyAlcoaLan"];
					entity.EmailBodyAlcoaDirectNewUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectNewUser)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectNewUser)];
					//entity.EmailBodyAlcoaDirectNewUser = (Convert.IsDBNull(reader["EmailBodyAlcoaDirectNewUser"]))?new byte[] {}:(System.Byte[])reader["EmailBodyAlcoaDirectNewUser"];
					entity.EmailBodyAlcoaDirectExistingUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectExistingUser)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectExistingUser)];
					//entity.EmailBodyAlcoaDirectExistingUser = (Convert.IsDBNull(reader["EmailBodyAlcoaDirectExistingUser"]))?new byte[] {}:(System.Byte[])reader["EmailBodyAlcoaDirectExistingUser"];
					entity.CsmsFileId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.CsmsFileId)];
					//entity.CsmsFileId = (Convert.IsDBNull(reader["CsmsFileId"]))?(int)0:(System.Int32)reader["CsmsFileId"];
					entity.CsmsFileTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.CsmsFileTypeId)];
					//entity.CsmsFileTypeId = (Convert.IsDBNull(reader["CsmsFileTypeId"]))?(int)0:(System.Int32)reader["CsmsFileTypeId"];
					entity.FileName = (System.String)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.FileName)];
					//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
					entity.Description = (System.String)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.Description)];
					//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
					entity.FileHash = (System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.FileHash)];
					//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
					entity.ContentLength = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.ContentLength)];
					//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
					entity.IsVisible = (System.Boolean)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.IsVisible)];
					//entity.IsVisible = (Convert.IsDBNull(reader["IsVisible"]))?false:(System.Boolean)reader["IsVisible"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="AdminTaskEmailTemplateListWithAttachments"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="AdminTaskEmailTemplateListWithAttachments"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, AdminTaskEmailTemplateListWithAttachments entity)
		{
			reader.Read();
			entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.AdminTaskEmailTemplateId)];
			//entity.AdminTaskEmailTemplateId = (Convert.IsDBNull(reader["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)reader["AdminTaskEmailTemplateId"];
			entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.AdminTaskTypeId)];
			//entity.AdminTaskTypeId = (Convert.IsDBNull(reader["AdminTaskTypeId"]))?(int)0:(System.Int32)reader["AdminTaskTypeId"];
			entity.EmailSubject = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailSubject)))?null:(System.String)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailSubject)];
			//entity.EmailSubject = (Convert.IsDBNull(reader["EmailSubject"]))?string.Empty:(System.String)reader["EmailSubject"];
			entity.EmailBodyAlcoaLan = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaLan)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaLan)];
			//entity.EmailBodyAlcoaLan = (Convert.IsDBNull(reader["EmailBodyAlcoaLan"]))?new byte[] {}:(System.Byte[])reader["EmailBodyAlcoaLan"];
			entity.EmailBodyAlcoaDirectNewUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectNewUser)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectNewUser)];
			//entity.EmailBodyAlcoaDirectNewUser = (Convert.IsDBNull(reader["EmailBodyAlcoaDirectNewUser"]))?new byte[] {}:(System.Byte[])reader["EmailBodyAlcoaDirectNewUser"];
			entity.EmailBodyAlcoaDirectExistingUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectExistingUser)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.EmailBodyAlcoaDirectExistingUser)];
			//entity.EmailBodyAlcoaDirectExistingUser = (Convert.IsDBNull(reader["EmailBodyAlcoaDirectExistingUser"]))?new byte[] {}:(System.Byte[])reader["EmailBodyAlcoaDirectExistingUser"];
			entity.CsmsFileId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.CsmsFileId)];
			//entity.CsmsFileId = (Convert.IsDBNull(reader["CsmsFileId"]))?(int)0:(System.Int32)reader["CsmsFileId"];
			entity.CsmsFileTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.CsmsFileTypeId)];
			//entity.CsmsFileTypeId = (Convert.IsDBNull(reader["CsmsFileTypeId"]))?(int)0:(System.Int32)reader["CsmsFileTypeId"];
			entity.FileName = (System.String)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.FileName)];
			//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
			entity.Description = (System.String)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.Description)];
			//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
			entity.FileHash = (System.Byte[])reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.FileHash)];
			//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
			entity.ContentLength = (System.Int32)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.ContentLength)];
			//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
			entity.IsVisible = (System.Boolean)reader[((int)AdminTaskEmailTemplateListWithAttachmentsColumn.IsVisible)];
			//entity.IsVisible = (Convert.IsDBNull(reader["IsVisible"]))?false:(System.Boolean)reader["IsVisible"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="AdminTaskEmailTemplateListWithAttachments"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, AdminTaskEmailTemplateListWithAttachments entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailTemplateId = (Convert.IsDBNull(dataRow["AdminTaskEmailTemplateId"]))?(int)0:(System.Int32)dataRow["AdminTaskEmailTemplateId"];
			entity.AdminTaskTypeId = (Convert.IsDBNull(dataRow["AdminTaskTypeId"]))?(int)0:(System.Int32)dataRow["AdminTaskTypeId"];
			entity.EmailSubject = (Convert.IsDBNull(dataRow["EmailSubject"]))?string.Empty:(System.String)dataRow["EmailSubject"];
			entity.EmailBodyAlcoaLan = (Convert.IsDBNull(dataRow["EmailBodyAlcoaLan"]))?new byte[] {}:(System.Byte[])dataRow["EmailBodyAlcoaLan"];
			entity.EmailBodyAlcoaDirectNewUser = (Convert.IsDBNull(dataRow["EmailBodyAlcoaDirectNewUser"]))?new byte[] {}:(System.Byte[])dataRow["EmailBodyAlcoaDirectNewUser"];
			entity.EmailBodyAlcoaDirectExistingUser = (Convert.IsDBNull(dataRow["EmailBodyAlcoaDirectExistingUser"]))?new byte[] {}:(System.Byte[])dataRow["EmailBodyAlcoaDirectExistingUser"];
			entity.CsmsFileId = (Convert.IsDBNull(dataRow["CsmsFileId"]))?(int)0:(System.Int32)dataRow["CsmsFileId"];
			entity.CsmsFileTypeId = (Convert.IsDBNull(dataRow["CsmsFileTypeId"]))?(int)0:(System.Int32)dataRow["CsmsFileTypeId"];
			entity.FileName = (Convert.IsDBNull(dataRow["FileName"]))?string.Empty:(System.String)dataRow["FileName"];
			entity.Description = (Convert.IsDBNull(dataRow["Description"]))?string.Empty:(System.String)dataRow["Description"];
			entity.FileHash = (Convert.IsDBNull(dataRow["FileHash"]))?new byte[] {}:(System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (Convert.IsDBNull(dataRow["ContentLength"]))?(int)0:(System.Int32)dataRow["ContentLength"];
			entity.IsVisible = (Convert.IsDBNull(dataRow["IsVisible"]))?false:(System.Boolean)dataRow["IsVisible"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region AdminTaskEmailTemplateListWithAttachmentsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListWithAttachmentsFilterBuilder : SqlFilterBuilder<AdminTaskEmailTemplateListWithAttachmentsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsFilterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateListWithAttachmentsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListWithAttachmentsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListWithAttachmentsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListWithAttachmentsFilterBuilder

	#region AdminTaskEmailTemplateListWithAttachmentsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListWithAttachmentsParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailTemplateListWithAttachmentsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsParameterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateListWithAttachmentsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateListWithAttachmentsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateListWithAttachmentsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateListWithAttachmentsParameterBuilder
	
	#region AdminTaskEmailTemplateListWithAttachmentsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailTemplateListWithAttachmentsSortBuilder : SqlSortBuilder<AdminTaskEmailTemplateListWithAttachmentsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateListWithAttachmentsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailTemplateListWithAttachmentsSortBuilder

} // end namespace
