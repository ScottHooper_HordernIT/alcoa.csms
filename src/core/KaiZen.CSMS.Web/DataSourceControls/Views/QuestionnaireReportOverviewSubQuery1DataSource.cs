﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportOverviewSubQuery1Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportOverviewSubQuery1DataSourceDesigner))]
	public class QuestionnaireReportOverviewSubQuery1DataSource : ReadOnlyDataSource<QuestionnaireReportOverviewSubQuery1>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1DataSource class.
		/// </summary>
		public QuestionnaireReportOverviewSubQuery1DataSource() : base(new QuestionnaireReportOverviewSubQuery1Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportOverviewSubQuery1DataSourceView used by the QuestionnaireReportOverviewSubQuery1DataSource.
		/// </summary>
		protected QuestionnaireReportOverviewSubQuery1DataSourceView QuestionnaireReportOverviewSubQuery1View
		{
			get { return ( View as QuestionnaireReportOverviewSubQuery1DataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportOverviewSubQuery1DataSourceView class that is to be
		/// used by the QuestionnaireReportOverviewSubQuery1DataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportOverviewSubQuery1DataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportOverviewSubQuery1, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportOverviewSubQuery1DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportOverviewSubQuery1DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportOverviewSubQuery1DataSourceView : ReadOnlyDataSourceView<QuestionnaireReportOverviewSubQuery1>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubQuery1DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportOverviewSubQuery1DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportOverviewSubQuery1DataSourceView(QuestionnaireReportOverviewSubQuery1DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportOverviewSubQuery1DataSource QuestionnaireReportOverviewSubQuery1Owner
		{
			get { return Owner as QuestionnaireReportOverviewSubQuery1DataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportOverviewSubQuery1Service QuestionnaireReportOverviewSubQuery1Provider
		{
			get { return Provider as QuestionnaireReportOverviewSubQuery1Service; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportOverviewSubQuery1DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportOverviewSubQuery1DataSource class.
	/// </summary>
	public class QuestionnaireReportOverviewSubQuery1DataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportOverviewSubQuery1>
	{
	}

	#endregion QuestionnaireReportOverviewSubQuery1DataSourceDesigner

	#region QuestionnaireReportOverviewSubQuery1Filter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery1Filter : SqlFilter<QuestionnaireReportOverviewSubQuery1Column>
	{
	}

	#endregion QuestionnaireReportOverviewSubQuery1Filter

	#region QuestionnaireReportOverviewSubQuery1ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubQuery1"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubQuery1ExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportOverviewSubQuery1Column>
	{
	}
	
	#endregion QuestionnaireReportOverviewSubQuery1ExpressionBuilder		
}

