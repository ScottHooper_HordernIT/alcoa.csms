﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileVaultSubCategoryService
    {
        FileVaultSubCategory Get(Expression<Func<FileVaultSubCategory, bool>> where, List<Expression<Func<FileVaultSubCategory, object>>> includes);
        List<FileVaultSubCategory> GetMany(int? top, Expression<Func<FileVaultSubCategory, bool>> where, Func<IQueryable<FileVaultSubCategory>, IOrderedQueryable<FileVaultSubCategory>> orderBy, List<Expression<Func<FileVaultSubCategory, object>>> includes);
        List<FileVaultSubCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultSubCategory, bool>> where, Func<IQueryable<FileVaultSubCategory>, IOrderedQueryable<FileVaultSubCategory>> orderBy, List<Expression<Func<FileVaultSubCategory, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileVaultSubCategory, bool>> where, List<Expression<Func<FileVaultSubCategory, object>>> includes);
        void Insert(FileVaultSubCategory item, bool saveChanges = true);
        void Update(FileVaultSubCategory item, bool saveChanges = true);
        void Delete(FileVaultSubCategory item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileVaultSubCategoryService : IFileVaultSubCategoryService
    {
        private readonly IRepository<FileVaultSubCategory> _repository;

        public FileVaultSubCategoryService(IRepository<FileVaultSubCategory> repository)
        {
            this._repository = repository;
        }

        public FileVaultSubCategory Get(Expression<Func<FileVaultSubCategory, bool>> where, List<Expression<Func<FileVaultSubCategory, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileVaultSubCategory> GetMany(int? top, Expression<Func<FileVaultSubCategory, bool>> where, Func<IQueryable<FileVaultSubCategory>, IOrderedQueryable<FileVaultSubCategory>> orderBy, List<Expression<Func<FileVaultSubCategory, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileVaultSubCategory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVaultSubCategory, bool>> where, Func<IQueryable<FileVaultSubCategory>, IOrderedQueryable<FileVaultSubCategory>> orderBy, List<Expression<Func<FileVaultSubCategory, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileVaultSubCategory, bool>> where, List<Expression<Func<FileVaultSubCategory, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileVaultSubCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultSubCategory");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileVaultSubCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultSubCategory");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileVaultSubCategory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVaultSubCategory");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
