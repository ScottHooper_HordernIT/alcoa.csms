﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BatchJobEmailTemplates.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Admin.BatchJobEmailTemplates" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<table>
    <tr>
        <td>
            <dx:ASPxComboBox ID="cbTemplate" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" DataSourceID="TemplateTypeDataSource1"
                TextField="TemplateTypeDesc" ValueField="TemplateTypeId" Width="400px" IncrementalFilteringMode="Contains">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings Display="Dynamic" ValidationGroup="Load">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
        <td>
            <dx:ASPxButton ID="btnLoad" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                Text="Load" OnClick="btnLoad_Click" ValidationGroup="Load">
            </dx:ASPxButton>
        </td>
    </tr>
</table>
<asp:Panel ID="Panel1" runat="server">
    <table>
        <tr>
            <td>
                <div align="left" style="float: left;">
                    <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Save" OnClick="btnSave_Click">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
                <div align="left" style="float: left;">
                    <dx:ASPxButton ID="btnSaveClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Save & Close" OnClick="btnSaveAndClose_Click">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
                <div align="left" style="float: left;">
                    <dx:ASPxButton ID="btnClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Close" OnClick="btnClose_Click">
                    </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <strong>Body:</strong><br />
    <dx:ASPxHtmlEditor ID="heBody" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" Height="500px" Visible="False" Width="876px">
        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
            <ViewArea>
                <Border BorderColor="#002D96" />
            </ViewArea>
        </Styles>
        <SettingsImageUpload>
            <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
            </ValidationSettings>
        </SettingsImageUpload>
        <SettingsImageSelector>
            <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
        </SettingsImageSelector>
        <SettingsDocumentSelector>
            <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
        </SettingsDocumentSelector>
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
            </LoadingPanel>
        </Images>
        <ImagesFileManager>
            <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
            </FolderContainerNodeLoadingPanel>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </LoadingPanel>
        </ImagesFileManager>
    </dx:ASPxHtmlEditor>
</asp:Panel>
<data:TemplateTypeDataSource ID="TemplateTypeDataSource1" runat="server" Filter="TemplateTypeId != 1">
</data:TemplateTypeDataSource>
