﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'QuestionnaireStatus' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table QuestionnaireStatus</remark>
	[Serializable]
	public enum QuestionnaireStatusList
	{
		/// <summary> 
		/// Incomplete
		/// </summary>
		[EnumTextValue(@"Incomplete")]
		Incomplete = 1, 

		/// <summary> 
		/// Being Assessed
		/// </summary>
		[EnumTextValue(@"Being Assessed")]
		BeingAssessed = 2, 

		/// <summary> 
		/// Assessment Complete
		/// </summary>
		[EnumTextValue(@"Assessment Complete")]
		AssessmentComplete = 3

	}
}
