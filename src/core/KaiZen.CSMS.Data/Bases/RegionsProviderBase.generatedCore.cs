﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RegionsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RegionsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Regions, KaiZen.CSMS.Entities.RegionsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.RegionsKey key)
		{
			return Delete(transactionManager, key.RegionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_regionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _regionId)
		{
			return Delete(null, _regionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _regionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Regions Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.RegionsKey key, int start, int pageLength)
		{
			return GetByRegionId(transactionManager, key.RegionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Region index.
		/// </summary>
		/// <param name="_regionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionId(System.Int32 _regionId)
		{
			int count = -1;
			return GetByRegionId(null,_regionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Region index.
		/// </summary>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionId(System.Int32 _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionId(null, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Region index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Region index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Region index.
		/// </summary>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionId(System.Int32 _regionId, int start, int pageLength, out int count)
		{
			return GetByRegionId(null, _regionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Region index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Regions GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Regions_RegionInternalName index.
		/// </summary>
		/// <param name="_regionInternalName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionInternalName(System.String _regionInternalName)
		{
			int count = -1;
			return GetByRegionInternalName(null,_regionInternalName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionInternalName index.
		/// </summary>
		/// <param name="_regionInternalName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionInternalName(System.String _regionInternalName, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionInternalName(null, _regionInternalName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionInternalName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionInternalName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionInternalName(TransactionManager transactionManager, System.String _regionInternalName)
		{
			int count = -1;
			return GetByRegionInternalName(transactionManager, _regionInternalName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionInternalName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionInternalName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionInternalName(TransactionManager transactionManager, System.String _regionInternalName, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionInternalName(transactionManager, _regionInternalName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionInternalName index.
		/// </summary>
		/// <param name="_regionInternalName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionInternalName(System.String _regionInternalName, int start, int pageLength, out int count)
		{
			return GetByRegionInternalName(null, _regionInternalName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionInternalName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionInternalName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Regions GetByRegionInternalName(TransactionManager transactionManager, System.String _regionInternalName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Regions_RegionName index.
		/// </summary>
		/// <param name="_regionName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionName(System.String _regionName)
		{
			int count = -1;
			return GetByRegionName(null,_regionName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionName index.
		/// </summary>
		/// <param name="_regionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionName(System.String _regionName, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionName(null, _regionName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionName(TransactionManager transactionManager, System.String _regionName)
		{
			int count = -1;
			return GetByRegionName(transactionManager, _regionName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionName(TransactionManager transactionManager, System.String _regionName, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionName(transactionManager, _regionName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionName index.
		/// </summary>
		/// <param name="_regionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionName(System.String _regionName, int start, int pageLength, out int count)
		{
			return GetByRegionName(null, _regionName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Regions GetByRegionName(TransactionManager transactionManager, System.String _regionName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Regions_RegionNameAbbrev index.
		/// </summary>
		/// <param name="_regionNameAbbrev"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Regions&gt;"/> class.</returns>
		public TList<Regions> GetByRegionNameAbbrev(System.String _regionNameAbbrev)
		{
			int count = -1;
			return GetByRegionNameAbbrev(null,_regionNameAbbrev, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionNameAbbrev index.
		/// </summary>
		/// <param name="_regionNameAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Regions&gt;"/> class.</returns>
		public TList<Regions> GetByRegionNameAbbrev(System.String _regionNameAbbrev, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionNameAbbrev(null, _regionNameAbbrev, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionNameAbbrev index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionNameAbbrev"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Regions&gt;"/> class.</returns>
		public TList<Regions> GetByRegionNameAbbrev(TransactionManager transactionManager, System.String _regionNameAbbrev)
		{
			int count = -1;
			return GetByRegionNameAbbrev(transactionManager, _regionNameAbbrev, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionNameAbbrev index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionNameAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Regions&gt;"/> class.</returns>
		public TList<Regions> GetByRegionNameAbbrev(TransactionManager transactionManager, System.String _regionNameAbbrev, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionNameAbbrev(transactionManager, _regionNameAbbrev, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionNameAbbrev index.
		/// </summary>
		/// <param name="_regionNameAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Regions&gt;"/> class.</returns>
		public TList<Regions> GetByRegionNameAbbrev(System.String _regionNameAbbrev, int start, int pageLength, out int count)
		{
			return GetByRegionNameAbbrev(null, _regionNameAbbrev, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionNameAbbrev index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionNameAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Regions&gt;"/> class.</returns>
		public abstract TList<Regions> GetByRegionNameAbbrev(TransactionManager transactionManager, System.String _regionNameAbbrev, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Regions_RegionValue index.
		/// </summary>
		/// <param name="_regionValue"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionValue(System.Int32 _regionValue)
		{
			int count = -1;
			return GetByRegionValue(null,_regionValue, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionValue index.
		/// </summary>
		/// <param name="_regionValue"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionValue(System.Int32 _regionValue, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionValue(null, _regionValue, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionValue"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionValue(TransactionManager transactionManager, System.Int32 _regionValue)
		{
			int count = -1;
			return GetByRegionValue(transactionManager, _regionValue, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionValue"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionValue(TransactionManager transactionManager, System.Int32 _regionValue, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionValue(transactionManager, _regionValue, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionValue index.
		/// </summary>
		/// <param name="_regionValue"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public KaiZen.CSMS.Entities.Regions GetByRegionValue(System.Int32 _regionValue, int start, int pageLength, out int count)
		{
			return GetByRegionValue(null, _regionValue, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Regions_RegionValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionValue"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Regions"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Regions GetByRegionValue(TransactionManager transactionManager, System.Int32 _regionValue, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Regions_GetWAOVIC 
		
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetWAOVIC()
		{
			return GetWAOVIC(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetWAOVIC(int start, int pageLength)
		{
			return GetWAOVIC(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetWAOVIC(TransactionManager transactionManager)
		{
			return GetWAOVIC(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetWAOVIC(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Regions_GetByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Regions_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(System.Int32? companyId)
		{
			return GetByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Regions_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Regions_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Regions_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Regions_GetWAOVIC_ByCompany 
		
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetWAOVIC_ByCompany(System.Int32? companyId)
		{
			return GetWAOVIC_ByCompany(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetWAOVIC_ByCompany(int start, int pageLength, System.Int32? companyId)
		{
			return GetWAOVIC_ByCompany(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetWAOVIC_ByCompany(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetWAOVIC_ByCompany(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Regions_GetWAOVIC_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetWAOVIC_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Regions&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Regions&gt;"/></returns>
		public static TList<Regions> Fill(IDataReader reader, TList<Regions> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Regions c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Regions")
					.Append("|").Append((System.Int32)reader[((int)RegionsColumn.RegionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Regions>(
					key.ToString(), // EntityTrackingKey
					"Regions",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Regions();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RegionId = (System.Int32)reader[((int)RegionsColumn.RegionId - 1)];
					c.RegionName = (System.String)reader[((int)RegionsColumn.RegionName - 1)];
					c.RegionInternalName = (reader.IsDBNull(((int)RegionsColumn.RegionInternalName - 1)))?null:(System.String)reader[((int)RegionsColumn.RegionInternalName - 1)];
					c.RegionNameAbbrev = (reader.IsDBNull(((int)RegionsColumn.RegionNameAbbrev - 1)))?null:(System.String)reader[((int)RegionsColumn.RegionNameAbbrev - 1)];
					c.RegionDescription = (reader.IsDBNull(((int)RegionsColumn.RegionDescription - 1)))?null:(System.String)reader[((int)RegionsColumn.RegionDescription - 1)];
					c.RegionValue = (System.Int32)reader[((int)RegionsColumn.RegionValue - 1)];
					c.IprocCode = (reader.IsDBNull(((int)RegionsColumn.IprocCode - 1)))?null:(System.Int32?)reader[((int)RegionsColumn.IprocCode - 1)];
					c.IsVisible = (System.Boolean)reader[((int)RegionsColumn.IsVisible - 1)];
					c.Ordinal = (System.Int32)reader[((int)RegionsColumn.Ordinal - 1)];
					c.Level = (System.Int32)reader[((int)RegionsColumn.Level - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Regions"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Regions"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Regions entity)
		{
			if (!reader.Read()) return;
			
			entity.RegionId = (System.Int32)reader[((int)RegionsColumn.RegionId - 1)];
			entity.RegionName = (System.String)reader[((int)RegionsColumn.RegionName - 1)];
			entity.RegionInternalName = (reader.IsDBNull(((int)RegionsColumn.RegionInternalName - 1)))?null:(System.String)reader[((int)RegionsColumn.RegionInternalName - 1)];
			entity.RegionNameAbbrev = (reader.IsDBNull(((int)RegionsColumn.RegionNameAbbrev - 1)))?null:(System.String)reader[((int)RegionsColumn.RegionNameAbbrev - 1)];
			entity.RegionDescription = (reader.IsDBNull(((int)RegionsColumn.RegionDescription - 1)))?null:(System.String)reader[((int)RegionsColumn.RegionDescription - 1)];
			entity.RegionValue = (System.Int32)reader[((int)RegionsColumn.RegionValue - 1)];
			entity.IprocCode = (reader.IsDBNull(((int)RegionsColumn.IprocCode - 1)))?null:(System.Int32?)reader[((int)RegionsColumn.IprocCode - 1)];
			entity.IsVisible = (System.Boolean)reader[((int)RegionsColumn.IsVisible - 1)];
			entity.Ordinal = (System.Int32)reader[((int)RegionsColumn.Ordinal - 1)];
			entity.Level = (System.Int32)reader[((int)RegionsColumn.Level - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Regions"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Regions"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Regions entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RegionId = (System.Int32)dataRow["RegionId"];
			entity.RegionName = (System.String)dataRow["RegionName"];
			entity.RegionInternalName = Convert.IsDBNull(dataRow["RegionInternalName"]) ? null : (System.String)dataRow["RegionInternalName"];
			entity.RegionNameAbbrev = Convert.IsDBNull(dataRow["RegionNameAbbrev"]) ? null : (System.String)dataRow["RegionNameAbbrev"];
			entity.RegionDescription = Convert.IsDBNull(dataRow["RegionDescription"]) ? null : (System.String)dataRow["RegionDescription"];
			entity.RegionValue = (System.Int32)dataRow["RegionValue"];
			entity.IprocCode = Convert.IsDBNull(dataRow["IprocCode"]) ? null : (System.Int32?)dataRow["IprocCode"];
			entity.IsVisible = (System.Boolean)dataRow["IsVisible"];
			entity.Ordinal = (System.Int32)dataRow["Ordinal"];
			entity.Level = (System.Int32)dataRow["Level"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Regions"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Regions Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Regions entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByRegionId methods when available
			
			#region DocumentsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Documents>|DocumentsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DocumentsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.DocumentsCollection = DataRepository.DocumentsProvider.GetByRegionId(transactionManager, entity.RegionId);

				if (deep && entity.DocumentsCollection.Count > 0)
				{
					deepHandles.Add("DocumentsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Documents>) DataRepository.DocumentsProvider.DeepLoad,
						new object[] { transactionManager, entity.DocumentsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ContactsContractorsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContactsContractors>|ContactsContractorsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactsContractorsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContactsContractorsCollection = DataRepository.ContactsContractorsProvider.GetByRegionId(transactionManager, entity.RegionId);

				if (deep && entity.ContactsContractorsCollection.Count > 0)
				{
					deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContactsContractors>) DataRepository.ContactsContractorsProvider.DeepLoad,
						new object[] { transactionManager, entity.ContactsContractorsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FileDbMedicalTrainingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileDbMedicalTraining>|FileDbMedicalTrainingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileDbMedicalTrainingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileDbMedicalTrainingCollection = DataRepository.FileDbMedicalTrainingProvider.GetByRegionId(transactionManager, entity.RegionId);

				if (deep && entity.FileDbMedicalTrainingCollection.Count > 0)
				{
					deepHandles.Add("FileDbMedicalTrainingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileDbMedicalTraining>) DataRepository.FileDbMedicalTrainingProvider.DeepLoad,
						new object[] { transactionManager, entity.FileDbMedicalTrainingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region RegionsSitesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<RegionsSites>|RegionsSitesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RegionsSitesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RegionsSitesCollection = DataRepository.RegionsSitesProvider.GetByRegionId(transactionManager, entity.RegionId);

				if (deep && entity.RegionsSitesCollection.Count > 0)
				{
					deepHandles.Add("RegionsSitesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<RegionsSites>) DataRepository.RegionsSitesProvider.DeepLoad,
						new object[] { transactionManager, entity.RegionsSitesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Regions object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Regions instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Regions Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Regions entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Documents>
				if (CanDeepSave(entity.DocumentsCollection, "List<Documents>|DocumentsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Documents child in entity.DocumentsCollection)
					{
						if(child.RegionIdSource != null)
						{
							child.RegionId = child.RegionIdSource.RegionId;
						}
						else
						{
							child.RegionId = entity.RegionId;
						}

					}

					if (entity.DocumentsCollection.Count > 0 || entity.DocumentsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.DocumentsProvider.Save(transactionManager, entity.DocumentsCollection);
						
						deepHandles.Add("DocumentsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Documents >) DataRepository.DocumentsProvider.DeepSave,
							new object[] { transactionManager, entity.DocumentsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContactsContractors>
				if (CanDeepSave(entity.ContactsContractorsCollection, "List<ContactsContractors>|ContactsContractorsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContactsContractors child in entity.ContactsContractorsCollection)
					{
						if(child.RegionIdSource != null)
						{
							child.RegionId = child.RegionIdSource.RegionId;
						}
						else
						{
							child.RegionId = entity.RegionId;
						}

					}

					if (entity.ContactsContractorsCollection.Count > 0 || entity.ContactsContractorsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContactsContractorsProvider.Save(transactionManager, entity.ContactsContractorsCollection);
						
						deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContactsContractors >) DataRepository.ContactsContractorsProvider.DeepSave,
							new object[] { transactionManager, entity.ContactsContractorsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileDbMedicalTraining>
				if (CanDeepSave(entity.FileDbMedicalTrainingCollection, "List<FileDbMedicalTraining>|FileDbMedicalTrainingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileDbMedicalTraining child in entity.FileDbMedicalTrainingCollection)
					{
						if(child.RegionIdSource != null)
						{
							child.RegionId = child.RegionIdSource.RegionId;
						}
						else
						{
							child.RegionId = entity.RegionId;
						}

					}

					if (entity.FileDbMedicalTrainingCollection.Count > 0 || entity.FileDbMedicalTrainingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileDbMedicalTrainingProvider.Save(transactionManager, entity.FileDbMedicalTrainingCollection);
						
						deepHandles.Add("FileDbMedicalTrainingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileDbMedicalTraining >) DataRepository.FileDbMedicalTrainingProvider.DeepSave,
							new object[] { transactionManager, entity.FileDbMedicalTrainingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<RegionsSites>
				if (CanDeepSave(entity.RegionsSitesCollection, "List<RegionsSites>|RegionsSitesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(RegionsSites child in entity.RegionsSitesCollection)
					{
						if(child.RegionIdSource != null)
						{
							child.RegionId = child.RegionIdSource.RegionId;
						}
						else
						{
							child.RegionId = entity.RegionId;
						}

					}

					if (entity.RegionsSitesCollection.Count > 0 || entity.RegionsSitesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RegionsSitesProvider.Save(transactionManager, entity.RegionsSitesCollection);
						
						deepHandles.Add("RegionsSitesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< RegionsSites >) DataRepository.RegionsSitesProvider.DeepSave,
							new object[] { transactionManager, entity.RegionsSitesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RegionsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Regions</c>
	///</summary>
	public enum RegionsChildEntityTypes
	{

		///<summary>
		/// Collection of <c>Regions</c> as OneToMany for DocumentsCollection
		///</summary>
		[ChildEntityType(typeof(TList<Documents>))]
		DocumentsCollection,

		///<summary>
		/// Collection of <c>Regions</c> as OneToMany for ContactsContractorsCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContactsContractors>))]
		ContactsContractorsCollection,

		///<summary>
		/// Collection of <c>Regions</c> as OneToMany for FileDbMedicalTrainingCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileDbMedicalTraining>))]
		FileDbMedicalTrainingCollection,

		///<summary>
		/// Collection of <c>Regions</c> as OneToMany for RegionsSitesCollection
		///</summary>
		[ChildEntityType(typeof(TList<RegionsSites>))]
		RegionsSitesCollection,
	}
	
	#endregion RegionsChildEntityTypes
	
	#region RegionsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RegionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsFilterBuilder : SqlFilterBuilder<RegionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsFilterBuilder class.
		/// </summary>
		public RegionsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsFilterBuilder
	
	#region RegionsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RegionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsParameterBuilder : ParameterizedSqlFilterBuilder<RegionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsParameterBuilder class.
		/// </summary>
		public RegionsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsParameterBuilder
	
	#region RegionsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RegionsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Regions"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RegionsSortBuilder : SqlSortBuilder<RegionsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSqlSortBuilder class.
		/// </summary>
		public RegionsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RegionsSortBuilder
	
} // end namespace
