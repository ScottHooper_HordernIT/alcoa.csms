﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesHrCrpDataListAllProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompaniesHrCrpDataListAllProviderBaseCore : EntityViewProviderBase<CompaniesHrCrpDataListAll>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompaniesHrCrpDataListAll&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompaniesHrCrpDataListAll&gt;"/></returns>
		protected static VList&lt;CompaniesHrCrpDataListAll&gt; Fill(DataSet dataSet, VList<CompaniesHrCrpDataListAll> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompaniesHrCrpDataListAll>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompaniesHrCrpDataListAll&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompaniesHrCrpDataListAll>"/></returns>
		protected static VList&lt;CompaniesHrCrpDataListAll&gt; Fill(DataTable dataTable, VList<CompaniesHrCrpDataListAll> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompaniesHrCrpDataListAll c = new CompaniesHrCrpDataListAll();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.CrpName = (Convert.IsDBNull(row["CrpName"]))?string.Empty:(System.String)row["CrpName"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32?)row["CompanyId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32?)row["SiteId"];
					c.SiteAbbrev = (Convert.IsDBNull(row["SiteAbbrev"]))?string.Empty:(System.String)row["SiteAbbrev"];
					c.CompanyNameHr = (Convert.IsDBNull(row["CompanyNameHr"]))?(int)0:(System.Int32?)row["CompanyNameHr"];
					c.SiteNameHr = (Convert.IsDBNull(row["SiteNameHr"]))?string.Empty:(System.String)row["SiteNameHr"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompaniesHrCrpDataListAll&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompaniesHrCrpDataListAll&gt;"/></returns>
		protected VList<CompaniesHrCrpDataListAll> Fill(IDataReader reader, VList<CompaniesHrCrpDataListAll> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompaniesHrCrpDataListAll entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompaniesHrCrpDataListAll>("CompaniesHrCrpDataListAll",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompaniesHrCrpDataListAll();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.CompanyName)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SiteName = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteName)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.CrpName = (System.String)reader[((int)CompaniesHrCrpDataListAllColumn.CrpName)];
					//entity.CrpName = (Convert.IsDBNull(reader["CrpName"]))?string.Empty:(System.String)reader["CrpName"];
					entity.CompanyId = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.CompanyId)))?null:(System.Int32?)reader[((int)CompaniesHrCrpDataListAllColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32?)reader["CompanyId"];
					entity.SiteId = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteId)))?null:(System.Int32?)reader[((int)CompaniesHrCrpDataListAllColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
					entity.SiteAbbrev = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.SiteAbbrev)];
					//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
					entity.CompanyNameHr = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.CompanyNameHr)))?null:(System.Int32?)reader[((int)CompaniesHrCrpDataListAllColumn.CompanyNameHr)];
					//entity.CompanyNameHr = (Convert.IsDBNull(reader["CompanyNameHr"]))?(int)0:(System.Int32?)reader["CompanyNameHr"];
					entity.SiteNameHr = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteNameHr)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.SiteNameHr)];
					//entity.SiteNameHr = (Convert.IsDBNull(reader["SiteNameHr"]))?string.Empty:(System.String)reader["SiteNameHr"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompaniesHrCrpDataListAll"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesHrCrpDataListAll"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompaniesHrCrpDataListAll entity)
		{
			reader.Read();
			entity.CompanyName = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.CompanyName)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SiteName = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteName)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.CrpName = (System.String)reader[((int)CompaniesHrCrpDataListAllColumn.CrpName)];
			//entity.CrpName = (Convert.IsDBNull(reader["CrpName"]))?string.Empty:(System.String)reader["CrpName"];
			entity.CompanyId = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.CompanyId)))?null:(System.Int32?)reader[((int)CompaniesHrCrpDataListAllColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32?)reader["CompanyId"];
			entity.SiteId = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteId)))?null:(System.Int32?)reader[((int)CompaniesHrCrpDataListAllColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32?)reader["SiteId"];
			entity.SiteAbbrev = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.SiteAbbrev)];
			//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
			entity.CompanyNameHr = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.CompanyNameHr)))?null:(System.Int32?)reader[((int)CompaniesHrCrpDataListAllColumn.CompanyNameHr)];
			//entity.CompanyNameHr = (Convert.IsDBNull(reader["CompanyNameHr"]))?(int)0:(System.Int32?)reader["CompanyNameHr"];
			entity.SiteNameHr = (reader.IsDBNull(((int)CompaniesHrCrpDataListAllColumn.SiteNameHr)))?null:(System.String)reader[((int)CompaniesHrCrpDataListAllColumn.SiteNameHr)];
			//entity.SiteNameHr = (Convert.IsDBNull(reader["SiteNameHr"]))?string.Empty:(System.String)reader["SiteNameHr"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompaniesHrCrpDataListAll"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompaniesHrCrpDataListAll"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompaniesHrCrpDataListAll entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.CrpName = (Convert.IsDBNull(dataRow["CrpName"]))?string.Empty:(System.String)dataRow["CrpName"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32?)dataRow["CompanyId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32?)dataRow["SiteId"];
			entity.SiteAbbrev = (Convert.IsDBNull(dataRow["SiteAbbrev"]))?string.Empty:(System.String)dataRow["SiteAbbrev"];
			entity.CompanyNameHr = (Convert.IsDBNull(dataRow["CompanyNameHr"]))?(int)0:(System.Int32?)dataRow["CompanyNameHr"];
			entity.SiteNameHr = (Convert.IsDBNull(dataRow["SiteNameHr"]))?string.Empty:(System.String)dataRow["SiteNameHr"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompaniesHrCrpDataListAllFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataListAllFilterBuilder : SqlFilterBuilder<CompaniesHrCrpDataListAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllFilterBuilder class.
		/// </summary>
		public CompaniesHrCrpDataListAllFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataListAllFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataListAllFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataListAllFilterBuilder

	#region CompaniesHrCrpDataListAllParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrCrpDataListAllParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesHrCrpDataListAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllParameterBuilder class.
		/// </summary>
		public CompaniesHrCrpDataListAllParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrCrpDataListAllParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrCrpDataListAllParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrCrpDataListAllParameterBuilder
	
	#region CompaniesHrCrpDataListAllSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrCrpDataListAll"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesHrCrpDataListAllSortBuilder : SqlSortBuilder<CompaniesHrCrpDataListAllColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrCrpDataListAllSqlSortBuilder class.
		/// </summary>
		public CompaniesHrCrpDataListAllSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesHrCrpDataListAllSortBuilder

} // end namespace
