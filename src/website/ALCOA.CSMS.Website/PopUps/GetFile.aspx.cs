﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.IO;
using System.Security;
using System.Security.Principal;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class GetFile : System.Web.UI.Page
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                try
                {
                    if (Request.QueryString["q"] != null)
                    {
                        int FileVaultId = 0;
                        Int32.TryParse(Request.QueryString["q"], out FileVaultId);
                        if(FileVaultId == 0) throw new Exception("Invalid Link");

                        FileVaultService fvService = new FileVaultService();
                        using (FileVault fv = fvService.GetByFileVaultId(FileVaultId))
                        {
                            if (fv == null)
                                throw new Exception("Invalid Link");
                            SendFile(fv.FileName, fv.Content, fv.ContentLength);
                        }
                    }
                    else
                    {
                        throw new Exception("Invalid Link");
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    Response.Write("Error: " + ex.Message); //TODO: Friendly Error Message Handling.
                }
            }
        }

        protected void SendFile(string FileName, byte[] FileContent, int FileLength)
        {
            Response.AppendHeader("Content-Type", "application/download");

            string ContentDisposition;
            String userAgent = Request.Headers.Get("User-Agent");
            if (userAgent.Contains("MSIE 7.0"))
            {
                ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
            }
            else
            {
                ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
            }
            Response.AppendHeader("Content-Disposition", ContentDisposition);
            Response.AppendHeader("Content-Length", FileLength.ToString());

            //Response.BinaryWrite(d.UploadedFile);
            Response.OutputStream.Write(FileContent, 0, FileLength);
            //Response.End();
            Response.Flush();
        }
    }
}