﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IPrivilegeService
    {
        Privilege Get(Expression<Func<Privilege, bool>> where, List<Expression<Func<Privilege, object>>> includes);
        List<Privilege> GetMany(int? top, Expression<Func<Privilege, bool>> where, Func<IQueryable<Privilege>, IOrderedQueryable<Privilege>> orderBy, List<Expression<Func<Privilege, object>>> includes);
        List<Privilege> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Privilege, bool>> where, Func<IQueryable<Privilege>, IOrderedQueryable<Privilege>> orderBy, List<Expression<Func<Privilege, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Privilege, bool>> where, List<Expression<Func<Privilege, object>>> includes);
        void Insert(Privilege item, bool saveChanges = true);
        void Update(Privilege item, bool saveChanges = true);
        void Delete(Privilege item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class PrivilegeService : IPrivilegeService
    {
        private readonly IRepository<Privilege> _repository;

        public PrivilegeService(IRepository<Privilege> repository)
        {
            this._repository = repository;
        }

        public Privilege Get(Expression<Func<Privilege, bool>> where, List<Expression<Func<Privilege, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Privilege> GetMany(int? top, Expression<Func<Privilege, bool>> where, Func<IQueryable<Privilege>, IOrderedQueryable<Privilege>> orderBy, List<Expression<Func<Privilege, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Privilege> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Privilege, bool>> where, Func<IQueryable<Privilege>, IOrderedQueryable<Privilege>> orderBy, List<Expression<Func<Privilege, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Privilege, bool>> where, List<Expression<Func<Privilege, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Privilege item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Privilege");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Privilege item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Privilege");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Privilege item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Privilege");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
