﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit09Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit09RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit09RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit09RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit09Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit09Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit09Repeater z = (TwentyOnePointAudit09Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit09Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit09RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit09Repeater runat=\"server\"></{0}:TwentyOnePointAudit09Repeater>")]
	public class TwentyOnePointAudit09Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit09Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit09Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit09Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit09Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit09Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit09Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit09Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit09 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit09;
						TwentyOnePointAudit09Item container = new TwentyOnePointAudit09Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit09Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit09 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit09Item"/> class.
        /// </summary>
		public TwentyOnePointAudit09Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit09Item"/> class.
        /// </summary>
		public TwentyOnePointAudit09Item(KaiZen.CSMS.Entities.TwentyOnePointAudit09 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved9a
        /// </summary>
        /// <value>The Achieved9a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9a
		{
			get { return _entity.Achieved9a; }
		}
        /// <summary>
        /// Gets the Achieved9b
        /// </summary>
        /// <value>The Achieved9b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9b
		{
			get { return _entity.Achieved9b; }
		}
        /// <summary>
        /// Gets the Achieved9c
        /// </summary>
        /// <value>The Achieved9c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9c
		{
			get { return _entity.Achieved9c; }
		}
        /// <summary>
        /// Gets the Achieved9d
        /// </summary>
        /// <value>The Achieved9d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9d
		{
			get { return _entity.Achieved9d; }
		}
        /// <summary>
        /// Gets the Achieved9e
        /// </summary>
        /// <value>The Achieved9e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9e
		{
			get { return _entity.Achieved9e; }
		}
        /// <summary>
        /// Gets the Achieved9f
        /// </summary>
        /// <value>The Achieved9f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9f
		{
			get { return _entity.Achieved9f; }
		}
        /// <summary>
        /// Gets the Achieved9g
        /// </summary>
        /// <value>The Achieved9g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9g
		{
			get { return _entity.Achieved9g; }
		}
        /// <summary>
        /// Gets the Achieved9h
        /// </summary>
        /// <value>The Achieved9h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved9h
		{
			get { return _entity.Achieved9h; }
		}
        /// <summary>
        /// Gets the Observation9a
        /// </summary>
        /// <value>The Observation9a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9a
		{
			get { return _entity.Observation9a; }
		}
        /// <summary>
        /// Gets the Observation9b
        /// </summary>
        /// <value>The Observation9b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9b
		{
			get { return _entity.Observation9b; }
		}
        /// <summary>
        /// Gets the Observation9c
        /// </summary>
        /// <value>The Observation9c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9c
		{
			get { return _entity.Observation9c; }
		}
        /// <summary>
        /// Gets the Observation9d
        /// </summary>
        /// <value>The Observation9d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9d
		{
			get { return _entity.Observation9d; }
		}
        /// <summary>
        /// Gets the Observation9e
        /// </summary>
        /// <value>The Observation9e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9e
		{
			get { return _entity.Observation9e; }
		}
        /// <summary>
        /// Gets the Observation9f
        /// </summary>
        /// <value>The Observation9f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9f
		{
			get { return _entity.Observation9f; }
		}
        /// <summary>
        /// Gets the Observation9g
        /// </summary>
        /// <value>The Observation9g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9g
		{
			get { return _entity.Observation9g; }
		}
        /// <summary>
        /// Gets the Observation9h
        /// </summary>
        /// <value>The Observation9h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation9h
		{
			get { return _entity.Observation9h; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit09"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit09 Entity
        {
            get { return _entity; }
        }
	}
}
