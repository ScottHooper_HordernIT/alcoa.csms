

/****** Object:  StoredProcedure [dbo].[_NonEmbeded_CmpCompliant_DashBoard]    Script Date: 3/08/2015 1:41:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_NonEmbeded_CmpCompliant_DashBoard]                            
(                            
 @SiteId int,                            
 @Month int,                            
 @Year int                            
)                            
AS                  
              
      
DECLARE @SiteIdN int       
      
--IF (@SiteId < 0)                             
-- BEGIN        
--  SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)        
-- END        
--ELSE        
-- BEGIN        
--  SET @SiteIdN = @SiteId        
-- END    
 SELECT @SiteIdN = [dbo].[GetSiteIdByComboRegionId](@SiteId)  
                      
 BEGIN                
  SELECT DISTINCT               
  ----          
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 26)[NE_Number of Companies:],               
  ------             
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 27)[NE_Contractor Services Audit],            
  ------                  
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 29)[NE_Workplace Safety Compliance],               
  -----                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 30)[NE_Management Health Safety Work Contacts],               
  -----                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 31)[NE_Behavioural Observations],             
  ------                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 32)[NE_JSA Field Audit Verifications],              
  -----                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 33)[NE_Toolbox Meetings],            
  -----                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 34)[NE_Alcoa Weekly Contractors Meeting],               
  -----                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 35)[NE_Alcoa Monthly Contractors Meeting],              
  ----                
  (SELECT A.KpiScoreYtd FROM dbo.AdHoc_Radar_Items A LEFT OUTER JOIN dbo.AdHoc_Radar B                
  ON A.AdHoc_RadarId = B.AdHoc_RadarId WHERE A.SiteId = C.SiteId                
  AND  A.[Year] = C.[Year]                
  --AND MONTH(A.ModifiedDate) = MONTH(C.ModifiedDate)                
  AND B.CompanySiteCategoryId = 2               
  AND B.AdHoc_RadarId = 36)[NE_Mandated Training]              
  ----                
  FROM AdHoc_Radar_Items C WHERE C.SiteId = @SiteIdN         
  AND  C.[Year] = @Year                
  --AND MONTH(C.ModifiedDate) = @Month               
 END                    
----------------------------------------

GO


