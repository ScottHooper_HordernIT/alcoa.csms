﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialContactStatusProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialContactStatusDataSourceDesigner))]
	public class QuestionnaireInitialContactStatusDataSource : ProviderDataSource<QuestionnaireInitialContactStatus, QuestionnaireInitialContactStatusKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusDataSource class.
		/// </summary>
		public QuestionnaireInitialContactStatusDataSource() : base(new QuestionnaireInitialContactStatusService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialContactStatusDataSourceView used by the QuestionnaireInitialContactStatusDataSource.
		/// </summary>
		protected QuestionnaireInitialContactStatusDataSourceView QuestionnaireInitialContactStatusView
		{
			get { return ( View as QuestionnaireInitialContactStatusDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialContactStatusDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialContactStatusSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialContactStatusSelectMethod selectMethod = QuestionnaireInitialContactStatusSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialContactStatusSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialContactStatusDataSourceView class that is to be
		/// used by the QuestionnaireInitialContactStatusDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialContactStatusDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialContactStatus, QuestionnaireInitialContactStatusKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialContactStatusDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialContactStatusDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialContactStatusDataSourceView : ProviderDataSourceView<QuestionnaireInitialContactStatus, QuestionnaireInitialContactStatusKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialContactStatusDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialContactStatusDataSourceView(QuestionnaireInitialContactStatusDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialContactStatusDataSource QuestionnaireInitialContactStatusOwner
		{
			get { return Owner as QuestionnaireInitialContactStatusDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialContactStatusSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialContactStatusOwner.SelectMethod; }
			set { QuestionnaireInitialContactStatusOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialContactStatusService QuestionnaireInitialContactStatusProvider
		{
			get { return Provider as QuestionnaireInitialContactStatusService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialContactStatus> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialContactStatus> results = null;
			QuestionnaireInitialContactStatus item;
			count = 0;
			
			System.Int32 _questionnaireInitialContactStatusId;
			System.String _statusName;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialContactStatusSelectMethod.Get:
					QuestionnaireInitialContactStatusKey entityKey  = new QuestionnaireInitialContactStatusKey();
					entityKey.Load(values);
					item = QuestionnaireInitialContactStatusProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialContactStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialContactStatusSelectMethod.GetAll:
                    results = QuestionnaireInitialContactStatusProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialContactStatusSelectMethod.GetPaged:
					results = QuestionnaireInitialContactStatusProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialContactStatusSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialContactStatusProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialContactStatusProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialContactStatusSelectMethod.GetByQuestionnaireInitialContactStatusId:
					_questionnaireInitialContactStatusId = ( values["QuestionnaireInitialContactStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireInitialContactStatusId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialContactStatusProvider.GetByQuestionnaireInitialContactStatusId(_questionnaireInitialContactStatusId);
					results = new TList<QuestionnaireInitialContactStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialContactStatusSelectMethod.GetByStatusName:
					_statusName = ( values["StatusName"] != null ) ? (System.String) EntityUtil.ChangeType(values["StatusName"], typeof(System.String)) : string.Empty;
					item = QuestionnaireInitialContactStatusProvider.GetByStatusName(_statusName);
					results = new TList<QuestionnaireInitialContactStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialContactStatusSelectMethod.Get || SelectMethod == QuestionnaireInitialContactStatusSelectMethod.GetByQuestionnaireInitialContactStatusId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialContactStatus entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialContactStatusProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialContactStatus> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialContactStatusProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialContactStatusDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialContactStatusDataSource class.
	/// </summary>
	public class QuestionnaireInitialContactStatusDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialContactStatus, QuestionnaireInitialContactStatusKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialContactStatusDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactStatusSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialContactStatusDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialContactStatusDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialContactStatusDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialContactStatusDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialContactStatusDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialContactStatusDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialContactStatusDataSourceActionList(QuestionnaireInitialContactStatusDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactStatusSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialContactStatusDataSourceActionList
	
	#endregion QuestionnaireInitialContactStatusDataSourceDesigner
	
	#region QuestionnaireInitialContactStatusSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialContactStatusDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialContactStatusSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireInitialContactStatusId method.
		/// </summary>
		GetByQuestionnaireInitialContactStatusId,
		/// <summary>
		/// Represents the GetByStatusName method.
		/// </summary>
		GetByStatusName
	}
	
	#endregion QuestionnaireInitialContactStatusSelectMethod

	#region QuestionnaireInitialContactStatusFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusFilter : SqlFilter<QuestionnaireInitialContactStatusColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactStatusFilter

	#region QuestionnaireInitialContactStatusExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialContactStatusColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactStatusExpressionBuilder	

	#region QuestionnaireInitialContactStatusProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialContactStatusChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusProperty : ChildEntityProperty<QuestionnaireInitialContactStatusChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialContactStatusProperty
}

