﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repo.CSMS.DAL.EntityModels;
using System.Web.Mvc;
using Repo.CSMS.Service.Database;
using ALCOA.CSMS.Batch.Common.Log;
using ALCOA.CSMS.Batch.Common;
using Autofac;

namespace ALCOA.CSMS.JobWatch
{
    class Program
    {
        static void Main(string[] args)
        {
            Log4NetService.LogInfo("Job Watch Program Started");

            //Setup dependencies
            DependencyConfig.RegisterDependencies(new ContainerBuilder());

            var resolver = DependencyResolver.Current.GetService<IEmailLogService>();

            var jobScheduleService = DependencyResolver.Current.GetService<IJobScheduleService>();
            //List<JobSchedule> jobSchedules = jobScheduleService.GetMany(null, p => p.IsOverdue == 1 && p.EmailSent == false, null, null); //this only sent an email once, perhaps we always want an email sent
            List<JobSchedule> jobSchedules = jobScheduleService.GetMany(null, p => p.IsOverdue == 1, null, null);


            StringBuilder sbNotRunningJobs = new StringBuilder();

            foreach (JobSchedule js in jobSchedules)
            {
                TimeSpan ts = DateTime.Now - js.LastStartedDateTime;

                sbNotRunningJobs.AppendLine(string.Format("{0}, last ran {1}, scheduled every {2} hours. It is {3}:{4} hours overdue.{5}",  
                    js.Name, js.LastStartedDateTime, Math.Round(js.SLAHours, 1), Math.Floor(ts.TotalHours), ts.Minutes.ToString("00") , Environment.NewLine));

                js.EmailSent = true;
                jobScheduleService.Update(js, false); // don't update the database until we've actually sent it
            }

            //call log4NetService.LogError with the details
            if (jobSchedules.Count > 0)
            {
                Log4NetService.LogError("Jobs that have not run on schedule" + Environment.NewLine + sbNotRunningJobs.ToString());
                jobScheduleService.SaveChanges(); //now update the database
            }
        }
    }
}
