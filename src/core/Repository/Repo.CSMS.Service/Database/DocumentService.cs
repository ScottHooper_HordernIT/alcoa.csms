﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IDocumentService
    {
        Document Get(Expression<Func<Document, bool>> where, List<Expression<Func<Document, object>>> includes);
        List<Document> GetMany(int? top, Expression<Func<Document, bool>> where, Func<IQueryable<Document>, IOrderedQueryable<Document>> orderBy, List<Expression<Func<Document, object>>> includes);
        List<Document> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Document, bool>> where, Func<IQueryable<Document>, IOrderedQueryable<Document>> orderBy, List<Expression<Func<Document, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Document, bool>> where, List<Expression<Func<Document, object>>> includes);
        void Insert(Document item, bool saveChanges = true);
        void Update(Document item, bool saveChanges = true);
        void Delete(Document item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class DocumentService : IDocumentService
    {
        private readonly IRepository<Document> _repository;

        public DocumentService(IRepository<Document> repository)
        {
            this._repository = repository;
        }

        public Document Get(Expression<Func<Document, bool>> where, List<Expression<Func<Document, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Document> GetMany(int? top, Expression<Func<Document, bool>> where, Func<IQueryable<Document>, IOrderedQueryable<Document>> orderBy, List<Expression<Func<Document, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Document> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Document, bool>> where, Func<IQueryable<Document>, IOrderedQueryable<Document>> orderBy, List<Expression<Func<Document, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Document, bool>> where, List<Expression<Func<Document, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Document item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Document");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Document item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Document");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Document item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Document");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
