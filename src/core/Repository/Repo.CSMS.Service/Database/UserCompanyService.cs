﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUserCompanyService
    {
        UserCompany Get(Expression<Func<UserCompany, bool>> where, List<Expression<Func<UserCompany, object>>> includes);
        List<UserCompany> GetMany(int? top, Expression<Func<UserCompany, bool>> where, Func<IQueryable<UserCompany>, IOrderedQueryable<UserCompany>> orderBy, List<Expression<Func<UserCompany, object>>> includes);
        List<UserCompany> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UserCompany, bool>> where, Func<IQueryable<UserCompany>, IOrderedQueryable<UserCompany>> orderBy, List<Expression<Func<UserCompany, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UserCompany, bool>> where, List<Expression<Func<UserCompany, object>>> includes);
        void Insert(UserCompany item, bool saveChanges = true);
        void Update(UserCompany item, bool saveChanges = true);
        void Delete(UserCompany item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UserCompanyService : IUserCompanyService
    {
        private readonly IRepository<UserCompany> _repository;

        public UserCompanyService(IRepository<UserCompany> repository)
        {
            this._repository = repository;
        }

        public UserCompany Get(Expression<Func<UserCompany, bool>> where, List<Expression<Func<UserCompany, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UserCompany> GetMany(int? top, Expression<Func<UserCompany, bool>> where, Func<IQueryable<UserCompany>, IOrderedQueryable<UserCompany>> orderBy, List<Expression<Func<UserCompany, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UserCompany> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UserCompany, bool>> where, Func<IQueryable<UserCompany>, IOrderedQueryable<UserCompany>> orderBy, List<Expression<Func<UserCompany, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UserCompany, bool>> where, List<Expression<Func<UserCompany, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UserCompany item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UserCompany");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UserCompany item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UserCompany");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UserCompany item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UserCompany");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
