﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Services;
using System.Data;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxGridView;
using System.Configuration;
using System.Data.SqlClient;

namespace ALCOA.CSMS.Website.UserControls.Main.PeopleManagementTraining
{
    public partial class TrainingMetrics : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);

            if (!Page.IsPostBack)
            {
                ViewState["CompanyDropdown"] = null; 
                ViewState["SiteDropdown"] = null;
                Helper.ExportGrid.Settings(ucExportButtons2, "People Management – Training – Training Metrics", "grid");

                FillCompanyDropdown(cmbCompanies);

                int startIndex = 0;

                FillYearCombo();

                FillCourseDropdown();

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):

                        cmbCompanies.Enabled = true;
                        FillSitesCombo("-1", cmbSites);
                        startIndex = cmbSites.Items.IndexOfText("Australia");
                        cmbSites.SelectedIndex = startIndex;
                        break;
                    case ((int)RoleList.Contractor):
                        cmbCompanies.Value = auth.CompanyId;
                        cmbCompanies.Enabled = false;
                        FillSitesCombo(auth.CompanyId.ToString(), cmbSites);

                        SqlCompanyDataSource1.SelectCommand = "select CompanyId,CompanyName from Companies where CompanyId="+ auth.CompanyId;
                        SqlCompanyDataSource1.DataBind();
                        break;
                    case ((int)RoleList.Administrator):
                        cmbCompanies.Enabled = true;
                        FillSitesCombo("-1", cmbSites);
                        startIndex = cmbSites.Items.IndexOfText("Australia");
                        cmbSites.SelectedIndex = startIndex;
                        break;
                    default:
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }

            }
            else
            {
                if (ViewState["SiteDropdown"] != null)
                {
                    if (auth.RoleId == Convert.ToInt32(RoleList.Contractor))
                    {
                        SqlCompanyDataSource1.SelectCommand = "select CompanyId,CompanyName from Companies where CompanyId=" + auth.CompanyId;
                        SqlCompanyDataSource1.DataBind();
                    }

                    DataSet ds = new DataSet();
                    try
                    {

                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                        {

                            using (SqlCommand cmd = new SqlCommand("TrainingMetrics_Select2", conn))
                            {
                                cmd.CommandTimeout = 60000;
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@COURSE_NAME", cmbTrainingPkg.Text));
                                cmd.Parameters.Add(new SqlParameter("@CompanyId", cmbCompanies.Value));
                                cmd.Parameters.Add(new SqlParameter("@SiteId", ViewState["SiteDropdown"]));
                                cmd.Parameters.Add(new SqlParameter("@Year", cmbYear.Value));
                                SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                                sqlda.Fill(ds);

                                //conn.Open();
                                //cmd.ExecuteNonQuery();
                                //conn.Close();
                                // Console.WriteLine("Deleted..");
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        
                        throw;
                    }
                    //SqlDataSourceTrainingMetics.SelectParameters["Course_Name"].DefaultValue = cmbTrainingPkg.Value.ToString();
                    //SqlDataSourceTrainingMetics.SelectParameters["CompanyId"].DefaultValue = "-1";
                    ////SqlDataSourceTrainingMetics.SelectParameters["SiteId"].DefaultValue = cmbSites.Value.ToString();
                    //SqlDataSourceTrainingMetics.SelectParameters["SiteId"].DefaultValue = ViewState["SiteDropdown"].ToString();
                    //SqlDataSourceTrainingMetics.SelectParameters["Year"].DefaultValue = cmbYear.Value.ToString();
                    ////grid.DataSourceID = "SqlDataSourceTrainingMetics";
                    ////grid.DataBind();
                    //if (ViewState["CompanyDropdown"] != null)
                    //{
                    //    if (Convert.ToInt32(ViewState["CompanyDropdown"]) > -1)
                    //    {
                    //        DataView dv = (DataView)SqlDataSourceTrainingMetics.Select(DataSourceSelectArguments.Empty);

                    //        dv.RowFilter = "CompanyId=" + ViewState["CompanyDropdown"].ToString();

                    //        grid.DataSource = dv.ToTable();
                           
                    //    }
                    //    else
                    //    {
                    //        DataView dv = (DataView)SqlDataSourceTrainingMetics.Select(DataSourceSelectArguments.Empty);
                    //        grid.DataSource = dv.ToTable();
                           
                    //    }
                    //}
                    //else
                    //{
                    //    DataView dv = (DataView)SqlDataSourceTrainingMetics.Select(DataSourceSelectArguments.Empty);
                    //    grid.DataSource = dv.ToTable();
                        
                    //}
                    grid.DataSource = ds;
                    grid.DataBind();
                    
                    if (ViewState["SiteDropdown"] != null)
                    {
                        if (Convert.ToInt32(ViewState["SiteDropdown"]) < 0)
                        {
                            string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";
                            SqlDataSite1.SelectCommand = strQuery;
                            grid.Columns["SiteId"].Visible = true;

                        }
                        else
                        {
                            string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";
                   
                            SqlDataSite1.SelectCommand = strQuery;
                            grid.Columns["SiteId"].Visible = false;
                        }
                    }
                    
                    //grid.DataBind();
                }

            }
        }

        protected void FillYearCombo()
        {
            int yearToStart = (DateTime.Now.Year)-8;
            int currentYear = DateTime.Now.Year;

            while (currentYear >= yearToStart)
            {
                cmbYear.Items.Add(currentYear.ToString(), currentYear);
                currentYear--;
            }
            cmbYear.Value = DateTime.Now.Year;
        }



        protected void cmbSites_Callback(object source, CallbackEventArgsBase e)
        {
            cmbSites.SelectedIndex = 0;
            FillSitesCombo(e.Parameter, cmbSites);
        }


        protected void FillSitesCombo(string companyId, ASPxComboBox CmbSite)
        {
            int _i = 0;
            if (string.IsNullOrEmpty(companyId)) return;

            int _companyId;
            bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
            if (!companyId_isInt) return;



            if (_companyId > 0) //Specific Company
            {

                SitesService sService = new SitesService();
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_companyId);

                CmbSite.Items.Clear();

                if (cscsTlist != null)
                {
                    if (cscsTlist.Count > 0)
                    {
                        SitesFilters sitesFilters = new SitesFilters();
                        sitesFilters.Append(SitesColumn.IsVisible, "1");
                        TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                        foreach (Sites s in sitesList)
                        {
                            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                            {
                                if (cscs.CompanySiteCategoryId != null)
                                {
                                    if (s.SiteId == cscs.SiteId)
                                    {
                                        CmbSite.Items.Add(s.SiteName, cscs.SiteId);
                                    }
                                }
                            }
                        }

                        cmbSites.Items.Add("----------------", 0);

                        //Add Regions
                        RegionsService rService = new RegionsService();
                        DataSet dsRegion = rService.GetByCompanyId(_companyId);
                        if (dsRegion.Tables[0] != null)
                        {
                            if (dsRegion.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in dsRegion.Tables[0].Rows)
                                {
                                    if (dr[2].ToString() != "AU") CmbSite.Items.Add(dr[0].ToString(), (-1 * Convert.ToInt32(dr[1].ToString())));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (_companyId == -1) // All Companies
                {
                    CmbSite.Items.Clear();
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        CmbSite.Items.Add(s.SiteName, s.SiteId);
                    }
                    CmbSite.Items.Add("----------------", 0);

                    RegionsFilters regionsFilters = new RegionsFilters();
                    regionsFilters.Append(RegionsColumn.IsVisible, "1");
                    TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                    foreach (Regions r in regionsList)
                    {
                        CmbSite.Items.Add(r.RegionName, (-1 * r.RegionId));
                    }
                }
                else if (_companyId == -2) //Selected "----" invalid option, so lets hide all sites.
                {
                    CmbSite.Items.Clear();
                    CmbSite.DataSourceID = "";
                    CmbSite.DataBind();
                }
            }

        }

       

        protected void FillCourseDropdown()
        {
            cmbTrainingPkg.DataSourceID = "sqldaCourseName";
            cmbTrainingPkg.TextField = "Course_Name";
            cmbTrainingPkg.ValueField = "Course_Name";
            cmbTrainingPkg.DataBind();
            cmbTrainingPkg.Items.Add("-----------------------------", 0);
            cmbTrainingPkg.Items.Add("All", "All");
            cmbTrainingPkg.Value = "All";
        }

        protected void FillCompanyDropdown(ASPxComboBox CmbCompany)
        {
            CmbCompany.DataSourceID = "CompaniesDataSource3";
            CmbCompany.TextField = "CompanyName";
            CmbCompany.ValueField = "CompanyId";
            CmbCompany.DataBind();
            CmbCompany.Items.Add("-----------------------------", 0);
            CmbCompany.Items.Add("All", -1);
            CmbCompany.Value = -1;
        }


        protected void cmbCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSitesCombo(cmbCompanies.SelectedItem.Value.ToString(), cmbSites);

        }

        protected void btnGoFilter_click(object sender, EventArgs e)
        {
            grid.Visible = true;
            ucExportButtons2.Visible = true;
            //if (cmbTrainingPkg.Value.ToString() == "-1")
            //{
            //    SqlDataSourceTrainingMetics.SelectParameters["Course_Name"].DefaultValue = "All";  
            //}
            DataSet ds = new DataSet();
            try
            {
               
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                using (SqlConnection conn = new SqlConnection(conString.ConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand("TrainingMetrics_Select2", conn))
                    {
                        cmd.CommandTimeout = 60000;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@COURSE_NAME", cmbTrainingPkg.Text));
                        cmd.Parameters.Add(new SqlParameter("@CompanyId", cmbCompanies.Value));
                        cmd.Parameters.Add(new SqlParameter("@SiteId", cmbSites.Value));
                        cmd.Parameters.Add(new SqlParameter("@Year", cmbYear.Value));
                        SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                        sqlda.Fill(ds);

                        //conn.Open();
                        //cmd.ExecuteNonQuery();
                        //conn.Close();
                        // Console.WriteLine("Deleted..");
                    }
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred while updating Polling Table for XXHR_CWK_HISTORY");
                throw;
            }

            //SqlDataSourceTrainingMetics.SelectParameters["Course_Name"].DefaultValue = cmbTrainingPkg.Value.ToString();
            //SqlDataSourceTrainingMetics.SelectParameters["CompanyId"].DefaultValue = "-1";
            //SqlDataSourceTrainingMetics.SelectParameters["SiteId"].DefaultValue = cmbSites.Value.ToString();
            //SqlDataSourceTrainingMetics.SelectParameters["Year"].DefaultValue = cmbYear.Value.ToString();


            if (auth.RoleId == Convert.ToInt32(RoleList.Contractor))
            {
                SqlCompanyDataSource1.SelectCommand = "select CompanyId,CompanyName from Companies where CompanyId=" + auth.CompanyId;
                SqlCompanyDataSource1.DataBind();
            }

            //grid.DataSourceID = "SqlDataSourceTrainingMetics";
            //grid.DataBind();

            //if (Convert.ToInt32(cmbCompanies.Value) > -1)
            //{
            //    DataView dv = (DataView)SqlDataSourceTrainingMetics.Select(DataSourceSelectArguments.Empty);

            //    dv.RowFilter = "CompanyId=" + cmbCompanies.Value.ToString();

            //    grid.DataSource = dv.ToTable();
            //    grid.DataBind();
            //    ViewState["CompanyDropdown"] = cmbCompanies.Value.ToString();
            //    ViewState["SiteDropdown"] = cmbSites.Value.ToString();
            //}
            //else
            //{
            //    DataView dv = (DataView)SqlDataSourceTrainingMetics.Select(DataSourceSelectArguments.Empty);
            //    grid.DataSource = dv.ToTable();
            //    grid.DataBind();
            //    ViewState["CompanyDropdown"] = null;
            //    ViewState["SiteDropdown"] = cmbSites.Value.ToString();
            //}

            grid.DataSource = ds;
            grid.DataBind();
            ViewState["CompanyDropdown"] = cmbCompanies.Value.ToString();
            ViewState["SiteDropdown"] = cmbSites.Value.ToString();
            if (cmbSites.Value != null)
            {
                if (Convert.ToInt32(cmbSites.Value) < 0)
                {
                    string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";
                    SqlDataSite1.SelectCommand = strQuery;
                    grid.Columns["SiteId"].Visible = true;

                }
                else
                {
                    string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";
                   
                    SqlDataSite1.SelectCommand = strQuery;
                    grid.Columns["SiteId"].Visible = false;
                }
            }

            


        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {

            if (e.RowType == GridViewRowType.Data)
            {
                //ASPxHyperLink hlCompany = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlCompany") as ASPxHyperLink;

               int companyId= Convert.ToInt32(grid.GetRowValues(e.VisibleIndex, "CompanyId"));
                int siteId= Convert.ToInt32(grid.GetRowValues(e.VisibleIndex, "SiteId"));

                

                if (e.RowType != GridViewRowType.Data) return;
                ASPxHyperLink hlCompany = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlCompany") as ASPxHyperLink;
                if (hlCompany != null)
                    hlCompany.NavigateUrl = String.Format("javascript:popUp('popups/TrainingMetricsPopup.aspx?C={0}&S={1}')",companyId,siteId);

                CompaniesService comSer = new CompaniesService();
                Companies comp = comSer.GetByCompanyId(companyId);
                if (comp != null && hlCompany!=null) //change by Debashis
                {
                    hlCompany.Text = comp.CompanyName;
                } 

                //e.Row.Cells[0].Attributes.Add("onClick", String.Format("javascript:popUp('PopUps/TrainingMetricsPopup.aspx?kpi={0}&site={1}')", 1, 2 ));

                

            }
        }
    }
}