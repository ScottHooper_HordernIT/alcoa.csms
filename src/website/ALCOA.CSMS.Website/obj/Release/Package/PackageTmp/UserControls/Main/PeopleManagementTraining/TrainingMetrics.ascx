﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrainingMetrics.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.PeopleManagementTraining.TrainingMetrics" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
    <%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

 <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">People Management – Training</span><br />
                <span class="date">Training Metrics</span><br />
                 <img src="images/grfc_dottedline.gif" width="24" height="1"><br />
            </span>
            
            </td>
    </tr>

    <tr>
        <td style="width:100%; padding-top:15px; text-align:left; vertical-align:top">
            <div style="font-weight:bold; float:left; padding:5px 0px 0px 0px">Training Package:</div>
            <div style="float:left; padding:2px 0px 0px 3px">
                <dxe:ASPxComboBox id="cmbTrainingPkg" runat="server" Width="150px"
                      clientinstancename="cmbTrainingPkg"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                     csspostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"                     
                     valuetype="System.String" DataSourceID="sqldaCourseName" TextField="Course_Name" ValueField="Course_Name"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic">
                        </ValidationSettings>
                    </dxe:ASPxComboBox>
            </div>
            

            <div style="font-weight:bold; float:left;  padding:5px 0px 0px 8px">Company:</div>
            <div style="float:left;  padding:2px 0px 0px 3px">
               <dx:ASPxComboBox ID="cmbCompanies" Width="145px"
                       ClientInstanceName="cmbCompanies" runat="server" IncrementalFilteringMode="StartsWith"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                          OnSelectedIndexChanged="cmbCompanies_SelectedIndexChanged"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic" ValidationGroup="ShowData"  ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a Company!" ErrorImage-AlternateText="Please select a Company!">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a Company!" />
                                                    </ValidationSettings>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    cmbSites.PerformCallback(s.GetValue());
                                                                    }" />
                    </dx:ASPxComboBox>
            </div>

            <div style="font-weight:bold; float:left; padding:5px 0px 0px 8px">Site:</div>
            <div style="float:left; padding:2px 0px 0px 3px">
                <dx:ASPxComboBox ID="cmbSites"  ClientInstanceName="cmbSites" Width="145px"
                       runat="server" EnableSynchronization="False" OnCallback="cmbSites_Callback"
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic" ValidationGroup="ShowData"  ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a Site!" ErrorImage-AlternateText="Please select a Site!">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a Site!" />
                                                    </ValidationSettings>
                    </dx:ASPxComboBox>
            </div>


            <div style="font-weight:bold; float:left; padding:5px 0px 0px 5px">Year:</div>
            <div style="float:left; padding:2px 15px 0px 3px">
                <dxe:ASPxComboBox id="cmbYear" runat="server" Width="55px"
                      clientinstancename="cmbYear"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                     csspostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"                     
                     valuetype="System.String" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                       
                    </dxe:ASPxComboBox>
            </div>
            
            <dx:ASPxButton ID="btnGoFilter" runat="server" OnClick="btnGoFilter_click" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Go / Refresh" ValidationGroup="ShowData">
                        
                    </dx:ASPxButton>

        </td>
    </tr>
    <tr>
        <td style="width:100%; text-align:left; vertical-align:top">
             
        </td>
    </tr>
    <tr>
        <td style="width:100%; text-align:left; vertical-align:top">
             <dxwgv:ASPxGridView Id="grid" Width="100%" Visible="false"
                ClientInstanceName="grid" OnHtmlRowCreated="grid_RowCreated"
                runat="server"
                AutoGenerateColumns="False"                  
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue">
                <Settings ShowFilterRow="true" ShowHeaderFilterButton="false" ShowFilterBar="Visible" />
                <SettingsPager AllButton-Visible="true"></SettingsPager>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ExpandedButton Height="12px" Width="11px" />
                    <LoadingPanelOnStatusBar Url= "~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px" Width="11px" />
                    <DetailCollapsedButton Height="12px" Width="11px" />
                    <DetailExpandedButton Height="12px" Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
              
                    <Columns>

                        <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="1" Caption="Company Name" FieldName="CompanyId" Name="CompanyBox" SortIndex="2" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="SqlCompanyDataSource1" DropDownHeight="130px" TextField="CompanyName"
                            ValueField="CompanyId"  ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <DataItemTemplate>
                             <table cellpadding="0" cellspacing="0"><tr>
                             <td style="width:100%">
                                 <dx:ASPxHyperLink Width="100%" ID="hlCompany" runat="server" Text="">
                                 </dx:ASPxHyperLink>
                             </td>
                             </tr></table>
                        </DataItemTemplate>
                        <EditFormSettings Visible="True"/>
                        <Settings SortMode="DisplayText"/>
                    </dxwgv:GridViewDataComboBoxColumn>


                    <dxwgv:GridViewDataComboBoxColumn Visible="false" ReadOnly="true" VisibleIndex="2" Caption="Site Name" FieldName="SiteId" Name="CompanyBox">
                        <PropertiesComboBox DataSourceID="SqlDataSite1" DropDownHeight="130px" TextField="SiteName"
                            ValueField="SiteId"  ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>                        
                        <Settings SortMode="DisplayText"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewBandColumn Caption="Number of People with Training Recorded" VisibleIndex="3">
                    <HeaderStyle HorizontalAlign="Center" />
                    <Columns>
                    <dxwgv:GridViewDataTextColumn FieldName="January" Width="10px" Name="January" Caption="Jan" VisibleIndex="1">
                   </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="February" Width="10px" Name="February" Caption="Feb" VisibleIndex="2"> 
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="March" Width="10px" Name="March" Caption="Mar" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="April" Width="10px" Name="April" Caption="Apr" VisibleIndex="4">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="May" Width="10px" Name="May" Caption="May" VisibleIndex="5">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="June" Width="10px" Name="June" Caption="Jun" VisibleIndex="6">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="July" Width="10px" Name="July" Caption="Jul" VisibleIndex="7">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="August" Width="10px" Name="August" Caption="Aug" VisibleIndex="8">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="September"  Width="10px" Name="September" Caption="Sep" VisibleIndex="9">
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="October" Width="10px" Name="October" Caption="Oct" VisibleIndex="10">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="November" Width="10px" Name="November" Caption="Nov" VisibleIndex="11">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="December" Width="10px" Name="December" Caption="Dec" VisibleIndex="12">
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="Total"  Width="15px" Name="Total" Caption="Year" VisibleIndex="13">
                    </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    
                    </dxwgv:GridViewBandColumn>
                     
                    
                    </Columns>
                  </dxwgv:ASPxGridView>
                   <table width="900px">
                                                        <tr align="right">
                                                            <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
                                                                width: 100%" align="right">
                                                                <div align="right">
                                                                    <uc1:ExportButtons ID="ucExportButtons2" Visible="false" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
        </td>
    </tr>
    </table>
    <br />



    <data:CompaniesDataSource ID="CompaniesDataSource3" runat="server" Sort="CompanyName ASC"></data:CompaniesDataSource>

    <asp:SqlDataSource ID="SqlCompanyDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select CompanyId,CompanyName from Companies order by CompanyName"
     SelectCommandType="Text">    
</asp:SqlDataSource>

 <asp:SqlDataSource ID="SqlDataSourceTrainingMetics" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="TrainingMetrics_Select2" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter Name="Course_Name" Type="String" />        
        <asp:Parameter Name="CompanyId" Type="Int32" />
        <asp:Parameter Name="SiteId" Type="Int32" />
        <asp:Parameter Name="Year" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSourceTrainingMetics1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                        SelectCommand="TrainingMetrics_Select2" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="cmbTrainingPkg" Name="Course_Name" PropertyName="Value" Type="String" />
                            <asp:ControlParameter ControlID="cmbCompanies" Name="CompanyId" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbSites" Name="SiteId" PropertyName="Value" Type="Int32" />
                            <asp:ControlParameter ControlID="cmbYear" Name="Year" PropertyName="Value" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataFinal" runat="server">
    
</asp:SqlDataSource>

<data:SitesDataSource ID="SitesDataSource1" runat="server" Sort="SiteName ASC"></data:SitesDataSource>

<asp:SqlDataSource ID="SqlDataSite1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select SiteId,SiteName from Sites order by SiteName"
     SelectCommandType="Text">    
</asp:SqlDataSource>


<asp:SqlDataSource ID="sqldaCourseName" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct Course_Name from HR.XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP order by Course_Name asc"
     SelectCommandType="Text">    
</asp:SqlDataSource>


<asp:SqlDataSource ID="SqldaYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select  distinct (YEAR(COURSE_END_DATE)) as Year1 from HR.XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP order by Year1 desc"
     SelectCommandType="Text">    
</asp:SqlDataSource>
