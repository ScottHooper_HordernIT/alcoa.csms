﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Usage" Codebehind="Usage.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxwpg" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
    
    <%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc2" %>

<dxtc:ASPxPageControl ID="ASPxPageControl2" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" Width="880px">
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
    <TabPages>
        <dxtc:TabPage Text="Login Activity Logs">
            <ContentCollection>
                <dxw:ContentControl runat="server">
<dxwgv:ASPxGridView ID="grid"
            runat="server" AutoGenerateColumns="False"
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" DataSourceID="sqldsUsage" KeyFieldName="LogId" 
                        Width="870px" ClientInstanceName="grid" 
                        OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter"
            >
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                </LoadingPanelOnStatusBar>
                <CollapsedButton Height="12px"
                    Width="11px" />
                <ExpandedButton Height="12px"
                    Width="11px" />
                <DetailCollapsedButton Height="12px"
                    Width="11px" />
                <DetailExpandedButton Height="12px"
                    Width="11px" />
                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="LogId" ReadOnly="True" SortIndex="1" SortOrder="Ascending"
                    Visible="False" VisibleIndex="0">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataDateColumn FieldName="LogDateTime" VisibleIndex="0" SortIndex="0" SortOrder="Descending" Width="125px">
                    <PropertiesDateEdit DisplayFormatString="yyyy-MM-dd HH:mm:ss">
                    </PropertiesDateEdit>

<Settings AutoFilterCondition="Contains"></Settings>
                </dxwgv:GridViewDataDateColumn>
                

                <dxwgv:GridViewDataTextColumn FieldName="UserId" Visible="False" VisibleIndex="2">
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="UserLogon" VisibleIndex="1" Caption="User Logon">
                                               
                                                <PropertiesComboBox DataSourceID="UsersDataSource2" TextField="UserLogon"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                 <dxwgv:GridViewDataComboBoxColumn FieldName="LastName" VisibleIndex="2" Caption="LastName">
                                               
                                                <PropertiesComboBox DataSourceID="UsersDataSource3" TextField="LastName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="FirstName" VisibleIndex="3" Caption="FirstName">
                                               
                                                <PropertiesComboBox DataSourceID="UsersDataSource4" TextField="FirstName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="Title" VisibleIndex="4" Caption="Title">
                                               
                                                <PropertiesComboBox DataSourceID="UsersDataSource5" TextField="Title"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
               
                <dxwgv:GridViewDataComboBoxColumn FieldName="Email" VisibleIndex="5" Caption="Email">
                                               
                                                <PropertiesComboBox DataSourceID="EmailDS" TextField="Email"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                <dxwgv:GridViewDataTextColumn FieldName="RoleId" VisibleIndex="6" Visible="False">
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="RoleId" VisibleIndex="6" Caption="Role">
                                               
                                                <PropertiesComboBox DataSourceID="RoleDataSource1" TextField="Role"
                                                    ValueField="RoleId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                <%--<dxwgv:GridViewDataTextColumn FieldName="CompanyName" VisibleIndex="7">
                </dxwgv:GridViewDataTextColumn>--%>
                <dxwgv:GridViewDataComboBoxColumn FieldName="CompanyName" VisibleIndex="7" Caption="CompanyName">
                                               
                                                <PropertiesComboBox DataSourceID="CompaniesDataSource1" TextField="CompanyName"
                                                    IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
            </Columns>
            <Settings ShowGroupPanel="True" ShowFilterRow="True" ShowFilterBar="Visible" />
            <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
            <TotalSummary>
                <dxwgv:ASPxSummaryItem FieldName="UserLogon" ShowInColumn="User Logon" ShowInGroupFooterColumn="User Logon"
                    SummaryType="Count" />
            </TotalSummary>
            <SettingsCustomizationWindow Enabled="True" />
            <SettingsPager>
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
        </dxwgv:ASPxGridView>
<div style="width: 870px; text-align: right">
            Number of Rows to show per page:<asp:DropDownList ID="DropDownList1" runat="server"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"><asp:ListItem>10</asp:ListItem>
<asp:ListItem>20</asp:ListItem>
<asp:ListItem>50</asp:ListItem>
<asp:ListItem>All</asp:ListItem>
</asp:DropDownList>
    <br />
    </div>
    <div style="width: 870px; text-align: right; padding-top: 2px">
    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
        <dxtc:TabPage Text="Usage Charts">
            <ContentCollection>
                <dxw:ContentControl runat="server">
                    <div align="center">
<dxchartsui:WebChartControl ID="WebChartControl1" runat="server" DataSourceID="sqldsDayWeek"
            Height="262px" Width="415px"  AppearanceName="Northern Lights">
<FillStyle >
<OptionsSerializable>
<cc2:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
</FillStyle>

<SeriesSerializable>
<cc2:Series ArgumentScaleType="Numerical" 
                    Name="Day of Week"  >
<ViewSerializable>
<cc2:SideBySideBarSeriesView coloreach="True" hiddenserializablestring="to be serialized">
<FinancialIndicators><TrendLines>
<cc2:TrendLine Name="Trendline 1">
<LineStyle DashStyle="Dash"></LineStyle>

<Point1 ArgumentSerializable="0"></Point1>

<Point2 ArgumentSerializable="4"></Point2>
</cc2:TrendLine>
</TrendLines>
</FinancialIndicators>
</cc2:SideBySideBarSeriesView>
</ViewSerializable>

<PointOptionsSerializable>
<cc2:PointOptions hiddenserializablestring="to be serialized"></cc2:PointOptions>
</PointOptionsSerializable>

<LegendPointOptionsSerializable>
<cc2:PointOptions hiddenserializablestring="to be serialized"></cc2:PointOptions>
</LegendPointOptionsSerializable>
</cc2:Series>
</SeriesSerializable>

<SeriesTemplate  
                >
<ViewSerializable>
<cc2:SideBySideBarSeriesView HiddenSerializableString="to be serialized"></cc2:SideBySideBarSeriesView>
</ViewSerializable>


<PointOptionsSerializable>
<cc2:PointOptions HiddenSerializableString="to be serialized"></cc2:PointOptions>
</PointOptionsSerializable>

<LegendPointOptionsSerializable>
<cc2:PointOptions HiddenSerializableString="to be serialized"></cc2:PointOptions>
</LegendPointOptionsSerializable>
</SeriesTemplate>
<DiagramSerializable>
<cc2:XYDiagram Rotated="True">
<axisx visibleinpanesserializable="-1">
<Range SideMarginsEnabled="True"></Range>
</axisx>

<axisy visibleinpanesserializable="-1" visible="False">
<Label Visible="False"></Label>

<Range SideMarginsEnabled="True"></Range>
</axisy>
</cc2:XYDiagram>
</DiagramSerializable>

<Legend Visible="False"></Legend>
<Titles>
<cc2:ChartTitle Text="Total Logins by Day"></cc2:ChartTitle>
</Titles>
</dxchartsui:WebChartControl>
&nbsp;&nbsp;<dxchartsui:WebChartControl ID="WebChartControl3" runat="server" DataSourceID="sqldsTimeDay"
            Height="262px" Width="415px"  AppearanceName="Northern Lights">
<SeriesSerializable>
<cc2:Series ArgumentScaleType="Numerical"  Name="Hour"
                             >
<ViewSerializable>
<cc2:SplineAreaSeriesView coloreach="True" hiddenserializablestring="to be serialized" transparency="0">
<FinancialIndicators><TrendLines>
<cc2:TrendLine Name="Trendline 1">
<LineStyle DashStyle="Dash"></LineStyle>

<Point1 ArgumentSerializable="0"></Point1>

<Point2 ArgumentSerializable="4"></Point2>
</cc2:TrendLine>
</TrendLines>
</FinancialIndicators>
</cc2:SplineAreaSeriesView>
</ViewSerializable>


<PointOptionsSerializable>
<cc2:PointOptions hiddenserializablestring="to be serialized"></cc2:PointOptions>
</PointOptionsSerializable>

<LegendPointOptionsSerializable>
<cc2:PointOptions hiddenserializablestring="to be serialized"></cc2:PointOptions>
</LegendPointOptionsSerializable>
</cc2:Series>
</SeriesSerializable>

<SeriesTemplate  
                >
<ViewSerializable>
<cc2:SplineAreaSeriesView HiddenSerializableString="to be serialized"></cc2:SplineAreaSeriesView>
</ViewSerializable>

<PointOptionsSerializable>
<cc2:PointOptions HiddenSerializableString="to be serialized"></cc2:PointOptions>
</PointOptionsSerializable>

<LegendPointOptionsSerializable>
<cc2:PointOptions HiddenSerializableString="to be serialized"></cc2:PointOptions>
</LegendPointOptionsSerializable>
</SeriesTemplate>
<DiagramSerializable>
<cc2:XYDiagram>
<axisx visibleinpanesserializable="-1">
<Range SideMarginsEnabled="False"></Range>
</axisx>

<axisy visible="False" visibleinpanesserializable="-1">
<Label Visible="False"></Label>

<Range SideMarginsEnabled="True"></Range>
</axisy>
</cc2:XYDiagram>
</DiagramSerializable>

<FillStyle >
<OptionsSerializable>
<cc2:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
</FillStyle>

<Legend Visible="False"></Legend>
<Titles>
<cc2:ChartTitle Text="Total Logins by Hour"></cc2:ChartTitle>
</Titles>
</dxchartsui:WebChartControl>
</div>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
    </TabPages>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
</dxtc:ASPxPageControl>

<asp:SqlDataSource ID="sqldsTimeDay" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
            SelectCommand="_UsageStats_HourOfDay" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>


<asp:SqlDataSource ID="sqldsDayWeek" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
            SelectCommand="_UsageStats_DayofWeek" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>

<asp:SqlDataSource
    Id="sqldsUsage"
    runat="server"
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_AccessLogs_Summary"
    SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<data:UsersDataSource ID="UsersDataSource1" runat="server">
</data:UsersDataSource>
<data:UsersDataSource ID="UsersDataSource2" runat="server" Sort="UserLogon ASC">
</data:UsersDataSource>
<data:UsersDataSource ID="UsersDataSource3" runat="server" Sort="LastName ASC">
</data:UsersDataSource>
<data:UsersDataSource ID="UsersDataSource4" runat="server" Sort="FirstName ASC">
</data:UsersDataSource>
<data:UsersDataSource ID="UsersDataSource5" runat="server" Sort="Title DESC">
</data:UsersDataSource>
<asp:SqlDataSource
	ID="EmailDS"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct dbo.users.Email from dbo.Users">
</asp:SqlDataSource>
<data:RoleDataSource ID="RoleDataSource1" runat="server" Sort="Role ASC"></data:RoleDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" Sort="CompanyName ASC"></data:CompaniesDataSource>
