﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.Text;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class BatchJobEmailTemplates : System.Web.UI.UserControl
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                Hide(true);
            }
            //BindRecipients();
        }

        protected void Load()
        {
            int TemplateTypeId = (int)cbTemplate.SelectedItem.Value;
            TemplateService tService = new TemplateService();
            Template t = tService.GetByTemplateId(TemplateTypeId);
            if (t != null)
            {
                heBody.Html = "";
                if (t.TemplateContent != null)
                {
                    heBody.Html = Encoding.ASCII.GetString(t.TemplateContent);
                }
            }
            Hide(false);
        }
        protected void Save(bool close)
        {
            int TemplateTypeId = (int)cbTemplate.SelectedItem.Value;
            TemplateService tService = new TemplateService();
            Template t = tService.GetByTemplateId(TemplateTypeId);
            if (t == null)
            {
                t = new Template();
                t.TemplateTypeId = TemplateTypeId;
            }

            ASCIIEncoding encoding = new ASCIIEncoding();
            t.TemplateContent = (byte[])encoding.GetBytes(heBody.Html);

            tService.Save(t);

            if (close) Close();
        }
        protected void Close()
        {
            heBody.Html = "";
            btnLoad.Enabled = true;
            cbTemplate.Enabled = true;

            Hide(true);
        }
        protected void Hide(bool hide)
        {
            if (hide)
            {
                btnSave.Enabled = false;
                btnSaveClose.Enabled = false;

                heBody.Visible = false;

                cbTemplate.Enabled = true;
                btnLoad.Enabled = true;
                Panel1.Visible = false;
            }
            else
            {
                btnSave.Enabled = true;
                btnSaveClose.Enabled = true;

                heBody.Visible = true;

                cbTemplate.Enabled = false;
                btnLoad.Enabled = false;

                Panel1.Visible = true;
            }
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            Load();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save(false);
        }
        protected void btnSaveAndClose_Click(object sender, EventArgs e)
        {
            Save(true);
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}