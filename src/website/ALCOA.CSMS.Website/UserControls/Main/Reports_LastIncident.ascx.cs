using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraPrinting;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;

using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Security;
using System.Security.Principal;

public partial class UserControls_Reports_LastIncident : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            //string group = "AU";
            //if (Request.QueryString["Group"] != null) { group = Request.QueryString["Group"].ToString(); }

            //RegionsService rService = new RegionsService();
            //TList<Regions> rList = rService.GetByRegionInternalName(group);
            //cbRegion.Value = rList[0].RegionId;

            if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Administrator)
            {
                SessionHandler.spVar_CompanyId = "-1";
            }
            else
            {
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                //ASPxGridView1.Columns["CompanyName"].Visible = false;
                grid.GroupBy((GridViewDataColumn)grid.Columns["LocCode"], 1);
                grid.ExpandAll();
            }
            Calc();

            // Export
            String exportFileName = @"ALCOA CSMS - Last Incident(s) Report"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtGrid"];
            //grid.DataBind(); //Commented this line for DT2843
        }
        //grid.ExpandAll(); //Commented this line for DT2843
    }

    public void Calc()
    {
        //1. Read EHSIMS data from csv flat file into datatable
        //string csvFilePath = "C:\\TempRead\\Last Incident Date by Contractor Company.csv";
        Configuration configuration = new Configuration();
        string csvFilePath = @configuration.GetValue(ConfigList.ContractorLastIncidentDateFilePath);
        const string startHeader = "Location Code";

        //2.1 Read from the actual Header of the CSV file. (Ignore junk at start of CSV file)
        string myFile = "";
        bool start = false;

        //IMPERSONATION!
        WindowsImpersonationContext impContext = null;
        try
        {
            impContext = Helper.ImpersonateWAOCSMUser();
        }
        catch (ApplicationException ex)
        {
            Trace.Write(ex.Message);
        }

        try
        {
            if (null != impContext)
            {
                using (impContext)
                {

                    try
                    {
                        using (FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                        {
                            StreamReader logFileReader = new StreamReader(logFileStream);
                            while (!logFileReader.EndOfStream)
                            {
                                string line = logFileReader.ReadLine();
                                if (line.Contains(startHeader))
                                    start = true;
                                ; ;
                                if (start == true)
                                    myFile += line + "\n";
                            }
                            logFileReader.Close();
                            logFileStream.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                        Response.Write(ex.Message);
                    }

                    if (!String.IsNullOrEmpty(myFile))
                    {
                        //2.1 Create the DataTable which will store the variation report
                        DataTable dtGrid = new DataTable();
                        dtGrid.Columns.Add("CompanyName", typeof(string));
                        dtGrid.Columns.Add("SiteName", typeof(string));
                        dtGrid.Columns.Add("LastIncidentDate", typeof(DateTime));
                        dtGrid.Columns.Add("InjuryFreeDays", typeof(int));

                        //2.3 Read Filtered CSV File and insert into DataTable
                        using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();

                            while (csv.ReadNextRecord())
                            {
                                DataRow dr = dtGrid.NewRow();

                                string CompanyName = "";

                                DateTime date = (DateTime.ParseExact(csv["Last Incident Date"].ToString(),
                                                            "d-MMM-yyyy",
                                                            System.Globalization.CultureInfo.InvariantCulture));
                                dr["LastIncidentDate"] = date;

                                //EHSIMS & IHS
                                
                                //using (DataSet ds = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByContCompanyCodeSiteNameIhs(
                                //                        csv["Cont Company Code"], csv["Location Code"]))
                                //{
                                //    if ((ds != null))
                                //    {
                                //        if (ds.Tables[0].Rows.Count == 1)
                                //        {
                                //            //0 - EhsimsMapId
                                //            //1 - CompanyId
                                //            //2 - ContCompanyCode
                                //            //3 - SiteId
                                //            //4 - IHS Site Name
                                //            //5 - EHSIMS Loc Code
                                //            //6 - EHSIMS Loc Code - IHS Site Name
                                //            //7 - Site Name - EHSIMS Loc Code - IHS Site Name
                                //            //8 - Site Name
                                //            //9 - Site Abbrev
                                //            //10- Company Name
                                //            if (ds.Tables[0].Rows[0].ItemArray[0] != null)
                                //            {
                                //                CompanyName = ds.Tables[0].Rows[0].ItemArray[10].ToString(); //ContCompanyCode
                                //                dr["SiteName"] = ds.Tables[0].Rows[0].ItemArray[8].ToString(); //SiteName
                                //            }
                                //        }
                                //        else
                                //        {
                                //            //Exception e = new Exception("Not Found or Multiple CompanyCode/LocCode Found: " +
                                //            //    csv["Cont Company Code"] + " - " + csv["Location Code"]);
                                //            //Elmah.ErrorSignal.FromContext(Context).Raise(e);
                                //        }
                                //    }
                                //}
                                
                                CompaniesService cService = new CompaniesService();
                                Companies cs = cService.GetByCompanyAbn(csv["Cont Company Code"]);
                                if (cs != null)
                                {
                                    CompanyName = cs.CompanyName;
                                    dr["CompanyName"] = cs.CompanyName;
                                }

                                SitesService sService = new SitesService();
                                TList<Sites> st = sService.GetBySiteNameIhs(csv["Location Code"]);
                                if (st.Count > 0)
                                    dr["SiteName"] = st[0].SiteName;
                                
                                dr["InjuryFreeDays"] = csv["Injury Free Days"];

                                //Check if CompanyName + Loc exists in WAOCSMS. If Yes then add row. Else ignore
                                if (!String.IsNullOrEmpty(CompanyName)) //quick check for now.. TODO: proper!
                                {
                                    if (auth.RoleId < (int)RoleList.Administrator)
                                    {
                                        if (auth.CompanyName == dr["CompanyName"].ToString())
                                        {
                                            dtGrid.Rows.Add(dr);
                                        }
                                    }
                                    else
                                    {
                                        dtGrid.Rows.Add(dr);
                                    }
                                }
                            }
                        }

                        Session["dtGrid"] = dtGrid;
                        grid.DataSource = (DataTable)Session["dtGrid"];
                        grid.SortBy((GridViewDataColumn)grid.Columns["CompanyName"], 0);
                        grid.SortBy((GridViewDataColumn)grid.Columns["SiteName"], 1);
                        grid.SortBy((GridViewDataColumn)grid.Columns["LastIncidentDate"], 2);
                        grid.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ToDo: Handle Exception
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Server Configuration Account Error. Contact Administrator");
        }
        finally
        {
            //if (impContext != null) impContext.Undo(); //changed by Debashis for 'Safe Handle closed' issue in deployment
        }
    }

    //protected void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    SessionHandler.spVar_RegionId = cbRegion.Value.ToString();
    //}
}
