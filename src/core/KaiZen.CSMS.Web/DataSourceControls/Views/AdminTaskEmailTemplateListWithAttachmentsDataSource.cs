﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailTemplateListWithAttachmentsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(AdminTaskEmailTemplateListWithAttachmentsDataSourceDesigner))]
	public class AdminTaskEmailTemplateListWithAttachmentsDataSource : ReadOnlyDataSource<AdminTaskEmailTemplateListWithAttachments>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsDataSource class.
		/// </summary>
		public AdminTaskEmailTemplateListWithAttachmentsDataSource() : base(new AdminTaskEmailTemplateListWithAttachmentsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailTemplateListWithAttachmentsDataSourceView used by the AdminTaskEmailTemplateListWithAttachmentsDataSource.
		/// </summary>
		protected AdminTaskEmailTemplateListWithAttachmentsDataSourceView AdminTaskEmailTemplateListWithAttachmentsView
		{
			get { return ( View as AdminTaskEmailTemplateListWithAttachmentsDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailTemplateListWithAttachmentsDataSourceView class that is to be
		/// used by the AdminTaskEmailTemplateListWithAttachmentsDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailTemplateListWithAttachmentsDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailTemplateListWithAttachments, Object> GetNewDataSourceView()
		{
			return new AdminTaskEmailTemplateListWithAttachmentsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailTemplateListWithAttachmentsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailTemplateListWithAttachmentsDataSourceView : ReadOnlyDataSourceView<AdminTaskEmailTemplateListWithAttachments>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListWithAttachmentsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailTemplateListWithAttachmentsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailTemplateListWithAttachmentsDataSourceView(AdminTaskEmailTemplateListWithAttachmentsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailTemplateListWithAttachmentsDataSource AdminTaskEmailTemplateListWithAttachmentsOwner
		{
			get { return Owner as AdminTaskEmailTemplateListWithAttachmentsDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailTemplateListWithAttachmentsService AdminTaskEmailTemplateListWithAttachmentsProvider
		{
			get { return Provider as AdminTaskEmailTemplateListWithAttachmentsService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region AdminTaskEmailTemplateListWithAttachmentsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailTemplateListWithAttachmentsDataSource class.
	/// </summary>
	public class AdminTaskEmailTemplateListWithAttachmentsDataSourceDesigner : ReadOnlyDataSourceDesigner<AdminTaskEmailTemplateListWithAttachments>
	{
	}

	#endregion AdminTaskEmailTemplateListWithAttachmentsDataSourceDesigner

	#region AdminTaskEmailTemplateListWithAttachmentsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListWithAttachmentsFilter : SqlFilter<AdminTaskEmailTemplateListWithAttachmentsColumn>
	{
	}

	#endregion AdminTaskEmailTemplateListWithAttachmentsFilter

	#region AdminTaskEmailTemplateListWithAttachmentsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateListWithAttachments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListWithAttachmentsExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailTemplateListWithAttachmentsColumn>
	{
	}
	
	#endregion AdminTaskEmailTemplateListWithAttachmentsExpressionBuilder		
}

