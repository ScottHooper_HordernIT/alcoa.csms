using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using System.Collections.Generic;

public partial class UserControls_cContacts : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            grid.Visible = false;
            btnShowAll.Visible = false;
            btnEdit.Visible = false;
            SessionHandler.ViewMode = "View";

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    Companies_All(true);
                    grid.Visible = true;
                    btnShowAll.Visible = false;
                    btnEdit.Visible = false;

                    break;
                case ((int)RoleList.Contractor):
                    Companies_All(false);
                    grid.Visible = true;
                    report_missingdetails(auth.CompanyId);

                    btnEdit.Visible = true;
                    checkContactsUpToDate(auth.CompanyId);
                    grid.ExpandAll();
                    break;
                case ((int)RoleList.Administrator):
                    Companies_All(true);
                    grid.Visible = true;
                    btnShowAll.Visible = false;
                    btnEdit.Visible = true;

                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            string exportFileName = @"ALCOA CSMS - Contractor Contacts"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                ) 
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, null, true);
        }
    }


    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        e.NewValues["CompanyId"] = auth.CompanyId;
        e.NewValues["ModifiedByUserId"] = auth.UserId;
    }
    protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
    {
        if (e.NewValues["CompanyId"] == null) { e.Errors[grid.Columns["CompanyId"]] = "Value can't be null."; }
        if (e.NewValues["ContactEmail"] == null) { e.Errors[grid.Columns["ContactEmail"]] = "Value can't be null."; }
        if (e.NewValues["ContactFirstName"] == null) { e.Errors[grid.Columns["ContactFirstName"]] = "Value can't be null."; }
        if (e.NewValues["ContactLastName"] == null) { e.Errors[grid.Columns["ContactLastName"]] = "Value can't be null."; }
        if (e.NewValues["ContactRole"] == null) { e.Errors[grid.Columns["ContactRole"]] = "Value can't be null."; }
        if (e.NewValues["ContactPhone"] == null) { e.Errors[grid.Columns["ContactPhone"]] = "Value can't be null."; }

        if (auth.RoleId <= 2)
        {
            if (Convert.ToInt32(e.NewValues["CompanyId"].ToString()) != auth.CompanyId)
            {
                e.Errors[grid.Columns["CompanyId"]] = "You can only edit your own company";
            }
        }

        if (e.Errors.Count > 0) e.RowError = "Errors Encountered. Please recheck form and correct.";

        e.NewValues["ModifiedByUserId"] = auth.UserId;
    }
    protected void grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        if (!grid.IsNewRowEditing) grid.DoRowValidation();
    }
    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        try
        {
            ContactsContractorsService ccService = new ContactsContractorsService();
            ContactsContractors cc = ccService.GetByContactId(Convert.ToInt32(e.Keys["ContactId"].ToString()));
            if (cc != null)
            {
                if (e.NewValues["CompanyId"] != null) cc.CompanyId = Convert.ToInt32(e.NewValues["CompanyId"].ToString());
                if (e.NewValues["SiteId"] != null) cc.SiteId = Convert.ToInt32(e.NewValues["SiteId"].ToString());
                if (e.NewValues["ContactEmail"] != null) cc.ContactEmail = e.NewValues["ContactEmail"].ToString();
                if (e.NewValues["ContactFirstName"] != null) cc.ContactFirstName = e.NewValues["ContactFirstName"].ToString();
                if (e.NewValues["ContactLastName"] != null) cc.ContactLastName = e.NewValues["ContactLastName"].ToString();
                if (e.NewValues["ContactMobile"] != null) cc.ContactMobile = e.NewValues["ContactMobile"].ToString();
                if (e.NewValues["ContactPhone"] != null) cc.ContactPhone = e.NewValues["ContactPhone"].ToString();
                if (e.NewValues["ContactRole"] != null) cc.ContactRole = e.NewValues["ContactRole"].ToString();
                if (e.NewValues["ContactTitle"] != null) cc.ContactTitle = e.NewValues["ContactTitle"].ToString();
                cc.ModifiedByUserId = auth.UserId;
            }
            else
            {
                throw new Exception("Error Deleting. Please Try Again and/or contact your System Administrator.");
            }

            ccService.Save(cc);
            e.NewValues["ModifiedByUserId"] = auth.UserId;

            e.Cancel = true;
            grid.CancelEdit();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void grid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        try
        {
            ContactsContractorsService ccService = new ContactsContractorsService();
            ContactsContractors cc = ccService.GetByContactId(Convert.ToInt32(e.Keys["ContactId"].ToString()));

            if (cc != null)
            {
                ccService.Delete(cc);
            }
            else
            {
                throw new Exception("Error Deleting. Please Try Again and/or contact your System Administrator.");
            }

            e.Cancel = true;
            grid.CancelEdit();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        if (SessionHandler.ViewMode == "View")
        {
            grid.Columns[0].Visible = true;
            SessionHandler.ViewMode = "Edit";
            btnEdit.Text = "(View)";
        }
        else if (SessionHandler.ViewMode == "Edit")
        {
            grid.Columns[0].Visible = false;
            SessionHandler.ViewMode = "View";
            btnEdit.Text = "(New/Edit/Delete)";
        }
    }

    protected void btnEmailSelected_Click(object sender, EventArgs e)
    {
        List<object> keyValues = grid.GetSelectedFieldValues("ContactEmail");
        string mailTo = "";
        Hashtable ht = new Hashtable();
        foreach (string key in keyValues)
        {
            if (!String.IsNullOrEmpty(key))
            {
                if (!ht.ContainsKey(key))
                {
                    ht[key] = 0;
                    mailTo += ";" + key;
                }
            }
        }
        Response.Redirect("mailto:" + mailTo); //TODO: Bug Fix. After Click. Loading Panel Stays Active.
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        grid.DataSourceID = "odsContractorContactsDS";
        grid.DataBind();
        ASPxcbCompanies.Text = " < Select Company... >";
    }

    protected void ASPxcbCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch ((int)ASPxcbCompanies.SelectedItem.Value)
        {
            case (-1):
                BindData();
                
                break;
            case (-2): //Do Nothing if select '---'
                break;
            default:
                BindData((int)ASPxcbCompanies.SelectedItem.Value);
                report_missingdetails((int)ASPxcbCompanies.SelectedItem.Value);
                checkContactsUpToDate((int)ASPxcbCompanies.SelectedItem.Value);
                break;
        }
    }

    protected void checkContactsUpToDate(int CompanyId)
    {
        DateTime nowMinus3 = DateTime.Now.AddMonths(-3);
        CompaniesService cService = new CompaniesService();
        Companies c = cService.GetByCompanyId(CompanyId);
        if (c.ContactsLastUpdated != null)
        {
            if (c.ContactsLastUpdated <= nowMinus3)
            {
                ASPxRoundPanel1.Visible = true;
            }
        }
        else
        {
            ASPxRoundPanel1.Visible = true;
        }
    }

    protected void Companies_All(bool all)
    {
        if (all == true)
        {
            ASPxcbCompanies.DataSourceID = "sqldsCompaniesList";
            ASPxcbCompanies.TextField = "CompanyName";
            ASPxcbCompanies.ValueField = "CompanyId";
            ASPxcbCompanies.DataBind();
            ASPxcbCompanies.Items.Add("-----------------------------", "-2");
            ASPxcbCompanies.Items.Add("All Companies", "-1");
            ASPxcbCompanies.Text = "All Companies";
        }
        else if (all == false)
        {
            CompaniesDataSource.SelectMethod = KaiZen.CSMS.Web.Data.CompaniesSelectMethod.GetByCompanyId;
            CompaniesDataSource.Parameters.Clear();
            CompaniesDataSource.Parameters.Add("CompanyId", auth.CompanyId.ToString());
            CompaniesDataSource.DataBind();

            ASPxcbCompanies.DataSourceID = "CompaniesDataSource";
            ASPxcbCompanies.TextField = "CompanyName";
            ASPxcbCompanies.ValueField = "CompanyId";
            ASPxcbCompanies.DataBind();
            ASPxcbCompanies.Text = auth.CompanyName;
            ASPxcbCompanies.Enabled = false;

            BindData(auth.CompanyId);
        }
    }
    protected void BindData()
    {
        ContactsContractorsDataSource.SelectMethod = KaiZen.CSMS.Web.Data.ContactsContractorsSelectMethod.GetPaged;
        ContactsContractorsDataSource.Parameters.Clear();
        ContactsContractorsDataSource.DataBind();
        grid.Columns["Company"].Visible = true;

        grid.DataBind();
    }
    protected void BindData(int selectedValue)
    {
        ContactsContractorsDataSource.SelectMethod = KaiZen.CSMS.Web.Data.ContactsContractorsSelectMethod.GetByCompanyId;
        ContactsContractorsDataSource.Parameters.Clear();
        ContactsContractorsDataSource.Parameters.Add("CompanyId", selectedValue.ToString());
        ContactsContractorsDataSource.DataBind();
        grid.Columns["Company"].Visible = false;

        grid.DataBind();
    }

    protected bool missingdetails(int CompanyId, string role)
    {
        int count = 0;
        ContactsContractorsFilters querycContacts = new ContactsContractorsFilters();
        querycContacts.Append(ContactsContractorsColumn.CompanyId, CompanyId.ToString());
        querycContacts.Append(ContactsContractorsColumn.ContactRole, role);
        TList<ContactsContractors> cContactsList = DataRepository.ContactsContractorsProvider.GetPaged(querycContacts.ToString(), null, 0, 100, out count);
        if (count > 0) { return false; } else { return true; };
    }

    protected void report_missingdetails(int CompanyId)
    {
        string strMissingDetails = "";
        int count = 0;
        if (missingdetails(CompanyId, "KPI%Administration%Contact") == true)
        {
            strMissingDetails += "KPI Administration Contact";
            count++;
        };
        if (missingdetails(CompanyId, "Contract%Manager") == true)
        {
            if (count > 0) { strMissingDetails += ", Contract Manager"; }
            else { strMissingDetails = "Contract Manager"; };
            count++;
        };
        if (missingdetails(CompanyId, "Principal") == true)
        {
            if (count > 0) { strMissingDetails += ", Principal"; }
            else { strMissingDetails = "Principal"; };
            count++;
        };
        if (missingdetails(CompanyId, "Operation%Manager") == true)
        {
            if (count > 0) { strMissingDetails += ", Operation Manager"; }
            else { strMissingDetails = "Operation Manager"; };
            count++;
        };
        lblRestriction.Text = "Alcoa requires that you have at minimum added your '<b>KPI Administration Contact</b>', '<b>Contract Manager</b>', '<b>Principal</b>' and '<b>Operation Manager</b>' contacts to the list below.";

        if (count > 0)
        {
            lblRestriction.Text += String.Format("<font color='red'> You have not entered in the following contacts: <b>{0}</b>.</font>", strMissingDetails);
        }
        else
        {
            lblRestriction.Text += "<font color='green'> Your company has entered in all required contacts.</font>";
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        int CompanyId = auth.CompanyId;

        if (auth.RoleId != (int)RoleList.Contractor)
        {
            if (ASPxcbCompanies.SelectedItem.Value != null)
            {
                if ((int)ASPxcbCompanies.SelectedItem.Value > 0)
                {
                    CompanyId = (int)ASPxcbCompanies.SelectedItem.Value;
                }
            }
        }

        CompaniesService cService = new CompaniesService();
        Companies c = cService.GetByCompanyId(CompanyId);
        c.ContactsLastUpdated = DateTime.Now;
        cService.Update(c);
        ASPxRoundPanel1.Visible = false;
    }
}
