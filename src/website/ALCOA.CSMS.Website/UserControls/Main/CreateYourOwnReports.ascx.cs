using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Text;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using DevExpress.XtraPrinting;

public partial class UserControls_CreateYourOwnReports : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            grid.Visible = false;

            ddlCompaniesMain.DataSourceID = "sqldsCompaniesList";
            ddlCompaniesMain.DataTextField = "CompanyName";
            ddlCompaniesMain.DataValueField = "CompanyId";
            ddlCompaniesMain.DataBind();
            ddlCompaniesMain.Items.Add(new ListItem("-----------------------------", "-2"));
            ddlCompaniesMain.Items.Add(new ListItem("All Companies", "-3"));
            lblLayout.Text = "(Default)";

            DateTime PreviousMonth = DateTime.Now.AddMonths(-1);
            int Month = PreviousMonth.Month;
            string _Month = Month.ToString();
            if(Month < 10) _Month = "0" + Month;
            grid.FilterExpression = String.Format("kpiDateTime LIKE '01/{0}/{1}'", _Month, PreviousMonth.Year);
            grid.DataBind();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    grid.Visible = true;
                    ddlCompaniesMain.Visible = true;
                    ddlCompaniesMain.Enabled = true;

                    GridViewDataComboBoxColumn combo0 = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo0.PropertiesComboBox.DataSourceID = "sqldsCompaniesList";
                    ddlCompaniesMain.SelectedValue = "-3";

                    break;
                case ((int)RoleList.Contractor):
                    grid.Visible = true;
                    ddlCompaniesMain.SelectedValue = auth.CompanyId.ToString();
                    grid.DataSourceID = "sqldsCustomReport_GetByCompanyId";
                    grid.DataBind();
                    GridViewDataComboBoxColumn combo = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo.Visible = false;
                    combo.ShowInCustomizationForm = false;
                    ddlCompaniesMain.Enabled = false;
                    grid.Columns["CompanyId"].Visible = false;

                    break;
                case ((int)RoleList.Administrator):
                    grid.Visible = true;
                    ddlCompaniesMain.Visible = true;
                    ddlCompaniesMain.Enabled = true;

                    GridViewDataComboBoxColumn combo2 = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo2.PropertiesComboBox.DataSourceID = "sqldsCompaniesList";
                    ddlCompaniesMain.SelectedValue = "-3";

                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));

                    break;
            }
            if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.Administrator)
            {
                if (Request.QueryString["ID"] != null)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        LoadLayout(Convert.ToInt32(Request.QueryString["ID"]));
                    }
                }
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Create Your Own Report"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true);
        }
    }

    public static int CalculateMonthDifference(DateTime startDate, DateTime endDate)
    {
        int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
        return Math.Abs(monthsApart);
    }

    protected void ASPxcbCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(2000);
        grid.DataSourceID = "odsKPIGetByCompanyId";
        grid.DataBind();
    }


    protected void btnSaveLayout_Click(object sender, EventArgs e)
    {
        LoadSaveLayout("save");
    }
    protected void btnLoadLayout_Click(object sender, EventArgs e)
    {
        LoadSaveLayout("load");
    }

    protected void LoadSaveLayout(string action)
    {
        if (action == "save" || action == "load")
        {
            try
            {
                CustomReportingLayoutsService crlService = new CustomReportingLayoutsService();
                TList<CustomReportingLayouts> crlList = crlService.GetAll();
                TList<CustomReportingLayouts> crlList2 = crlList.FindAll(
                    delegate(CustomReportingLayouts c2)
                    {
                        return
                            c2.Area == "CYOR" &&
                            c2.ReportName == "My Report #1" &&
                            c2.ModifiedByUserId == auth.UserId;
                    }
                );
                if (action == "save")
                {
                    using (CustomReportingLayouts crl = new CustomReportingLayouts())
                    {
                        crl.Area = "CYOR";
                        crl.ReportName = "My Report #1";

                        if (!String.IsNullOrEmpty(ddlCompaniesMain.SelectedValue))
                            crl.ReportDescription = ddlCompaniesMain.SelectedValue;
                        else
                            crl.ReportDescription = "";

                        crl.ReportLayout = grid.SaveClientLayout();
                        crl.ModifiedByUserId = auth.UserId;
                        crl.ModifiedDate = DateTime.Now;
                        if (crlList2.Count != 0)
                        {
                            foreach (CustomReportingLayouts crl2 in crlList2)
                                crl.ReportId = crl2.ReportId;
                            crlService.Update(crl);
                            lblLayout.Text = String.Format("{0} | Report Updated.", DateTime.Now);
                        }
                        else
                        {
                            crlService.Insert(crl);
                            lblLayout.Text = String.Format("{0} | Report Updated.", DateTime.Now);
                        }
                    }
                }

                if (action == "load")
                {
                    if (crlList2.Count != 0)
                    {
                        foreach (CustomReportingLayouts crl in crlList2)
                        {
                            grid.LoadClientLayout(crl.ReportLayout);

                            if (!String.IsNullOrEmpty(crl.ReportDescription))
                            {
                                int selectedCompanyId = Convert.ToInt32(crl.ReportDescription);
                                //ASPxcbCompanies.SelectedItem.Value = (object)crl.ReportDescription.ToString();
                                
                                ddlCompaniesMain.SelectedValue = selectedCompanyId.ToString();
                                if (selectedCompanyId > 0)
                                {
                                    SessionHandler.spVar_CompanyId = ddlCompaniesMain.SelectedValue;
                                    grid.Columns["CompanyId"].Visible = false;
                                    grid.DataSourceID = "sqldsCustomReport_GetByCompanyId";
                                    grid.DataBind();
                                }
                                if (selectedCompanyId == -3)
                                {
                                    grid.Columns["CompanyId"].Visible = true;
                                    grid.DataSourceID = "sqldsCustomReport";
                                    grid.DataBind();
                                }
                            }

                            lblLayout.Text = "Report loaded.";
                        }
                    }
                    else
                    {
                        lblLayout.Text = "Report could not be loaded.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                lblLayout.Text = ex.Message;
            }
        }
    }
    protected void LoadLayout(int ReportId)
    {
        CustomReportingLayoutsService crlService = new CustomReportingLayoutsService();
        TList<CustomReportingLayouts> crlList = crlService.GetAll();
        TList<CustomReportingLayouts> crlList2 = crlList.FindAll(
            delegate(CustomReportingLayouts c2)
            {
                return
                    c2.ReportId == ReportId;
            }
        );
        string ReportName = "";
        if (crlList2.Count != 0)
        {
            foreach (CustomReportingLayouts crl in crlList2)
            {
                grid.LoadClientLayout(crl.ReportLayout);

                if (!String.IsNullOrEmpty(crl.ReportDescription))
                {
                    ReportName = crl.ReportName;
                    int selectedCompanyId = Convert.ToInt32(crl.ReportDescription);
                    //ASPxcbCompanies.SelectedItem.Value = (object)crl.ReportDescription.ToString();

                    ddlCompaniesMain.SelectedValue = selectedCompanyId.ToString();
                    if (selectedCompanyId > 0)
                    {
                        SessionHandler.spVar_CompanyId = ddlCompaniesMain.SelectedValue;
                        grid.Columns["CompanyId"].Visible = false;
                        grid.DataSourceID = "sqldsCustomReport_GetByCompanyId";
                        grid.DataBind();
                    }
                    if (selectedCompanyId == -3)
                    {
                        grid.Columns["CompanyId"].Visible = true;
                        grid.DataSourceID = "sqldsCustomReport";
                        grid.DataBind();
                    }
                }

                lblLayout.Text = ReportName;
            }
        }
        else
        {
            lblLayout.Text = "(Default) | Selected Layout could not be loaded.";
        }
    }
    protected void ddlCompaniesMain_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCompaniesMain.SelectedItem.ToString() == "All Companies")
        {
            grid.Columns["CompanyId"].Visible = true;
            grid.DataSourceID = "sqldsCustomReport";
            grid.DataBind();

        }
        else
        {
            if (ddlCompaniesMain.SelectedItem.ToString() != "-----------------------------")
            {
                SessionHandler.spVar_CompanyId = ddlCompaniesMain.SelectedValue;
                grid.Columns["CompanyId"].Visible = false;
                grid.DataSourceID = "sqldsCustomReport_GetByCompanyId";
                grid.DataBind();
            }
        }
    }

    protected void grid2_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["Area"] = "CYOR";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        if (!String.IsNullOrEmpty(ddlCompaniesMain.SelectedValue))
        {
            e.NewValues["ReportDescription"] = ddlCompaniesMain.SelectedValue;
        }
        else
        {
            e.NewValues["ReportDescription"] = "";
        }
    }
    protected void grid2_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        e.NewValues["Area"] = "CYOR";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        if (!String.IsNullOrEmpty(ddlCompaniesMain.SelectedValue))
        {
            e.NewValues["ReportDescription"] = ddlCompaniesMain.SelectedValue;
        }
        else
        {
            e.NewValues["ReportDescription"] = "";
        }
    }
    protected void grid2_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["Area"] = "CYOR";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        if (!String.IsNullOrEmpty(ddlCompaniesMain.SelectedValue))
        {
            e.NewValues["ReportDescription"] = ddlCompaniesMain.SelectedValue;
        }
        else
        {
            e.NewValues["ReportDescription"] = "";
        }
    }
    protected void grid2_RowValidating(object sender, ASPxDataValidationEventArgs e)
    {
        //REQUIRED FIELDS (EVERYTHING ELSE IS DEFAULT TO 0)
        if (e.NewValues["ReportName"] == null) { e.Errors[ASPxGridView1.Columns["ReportName"]] = "Required Field."; }

        //int temp = 0;

        if (e.Errors.Count > 0) e.RowError = "Report Name can not be empty.";
    }
    protected void grid2_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        if (!ASPxGridView1.IsNewRowEditing)
        {
            ASPxGridView1.DoRowValidation();
        }
    }
    protected void grid2_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        int ReportId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "ReportId");
        ASPxHyperLink hl = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "hlLoad") as ASPxHyperLink;
        if (hl != null)
        {
            hl.NavigateUrl = String.Format("~/CreateYourOwnReports.aspx{0}", QueryStringModule.Encrypt(String.Format("ID={0}", ReportId)));
        }
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "SiteId" || e.Column.FieldName == "CompanySiteCategoryId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1)}";
        }
    }


}