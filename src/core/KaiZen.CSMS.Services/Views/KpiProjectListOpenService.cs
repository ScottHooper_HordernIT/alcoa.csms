﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An component type implementation of the 'KpiProjectList_Open' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class KpiProjectListOpenService : KaiZen.CSMS.Services.KpiProjectListOpenServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenService class.
		/// </summary>
		public KpiProjectListOpenService() : base()
		{
		}
		
	}//End Class


} // end namespace
