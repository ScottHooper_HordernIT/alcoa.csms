﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="Error" Codebehind="Error.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="scriptmanager1" runat="Server" EnablePageMethods="true" EnablePartialRendering="true" AsyncPostBackTimeout="600" LoadScriptsBeforeUI="true">
    </asp:ScriptManager>
    <div align="center" style="text-align: center">
        <img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" width="153" /><br />
        <table id="Table1" align="center" border="0" cellpadding="0" cellspacing="0" width="900">
            <tr>
                <td align="right" style="text-align: center" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                    <img height="3" src="Images/spacer.gif" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <span style="color: #003399">Australian Operations</span></td>
            </tr>
            <tr>
                <td align="right" style="text-align: center" valign="bottom">
                    <span class="sitetitleblue"><a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">
                        Contractor Services Management System</a></span>
                </td>
            </tr>
        </table>
        <br />
        <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" CssFilePath="~/App_Themes/RedGlass/{0}/styles.css"
            CssPostfix="RedGlass" ShowHeader="True" Width="460px" Style="text-align: center"
            HeaderText="An Application Error Occurred" HeaderStyle-HorizontalAlign="Center">
            <TopEdge BackColor="#EA9CA8">
            </TopEdge>
            <BottomRightCorner Height="2px" Url="~/App_Themes/RedGlass/Web/rpBottomRightCorner.png"
                Width="2px">
            </BottomRightCorner>
            <ContentPaddings Padding="9px" />
            <HeaderRightEdge BackColor="#D01F3C">
                <BackgroundImage ImageUrl="~/App_Themes/RedGlass/Web/rpHeader.png" Repeat="RepeatX" />
            </HeaderRightEdge>
            <Border BorderColor="#959595" />
            <HeaderLeftEdge BackColor="#D01F3C">
                <BackgroundImage ImageUrl="~/App_Themes/RedGlass/Web/rpHeader.png" Repeat="RepeatX" />
            </HeaderLeftEdge>
            <HeaderStyle>
                <Paddings PaddingBottom="5px" PaddingLeft="7px" PaddingTop="5px" />
            </HeaderStyle>
            <TopRightCorner Height="2px" Url="~/App_Themes/RedGlass/Web/rpTopRightCorner.png"
                Width="2px">
            </TopRightCorner>
            <HeaderContent BackColor="#D01F3C">
                <BackgroundImage ImageUrl="~/App_Themes/RedGlass/Web/rpHeader.png" Repeat="RepeatX" />
            </HeaderContent>
            <NoHeaderTopEdge BackColor="#F7F7F7">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <div align="center" style="text-align: center">
                        <dx:ASPxLabel ID="lblSorry" runat="server" Text="" EncodeHtml="false">
                        </dx:ASPxLabel>
                        <br />
                        <br />
                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                            TargetControlID="ContentPanel" ExpandControlID="TitlePanel" Collapsed="true"
                            TextLabelID="TextToControl" ExpandedText="" CollapsedText="" ImageControlID="ImageToControl"
                            ExpandedImage="~/Images/gvExpandedButton.png" CollapsedImage="~/Images/gvCollapsedButton.png"
                            SuppressPostBack="False" CollapseControlID="TitlePanel">
                        </asp:CollapsiblePanelExtender>
                        <!-- Collapsible Panel Title Bar-->
                        <div id="Div1" class="collapsePanel">
                            <asp:Panel ID="TitlePanel" runat="server" Height="23px" Width="100%">
                                <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                    <div style="float: left; vertical-align: middle;">
                                        <asp:Image ID="ImageToControl" runat="server" ImageUrl="~/Images/gvExpandedButton.png" />
                                    </div>
                                    <div style="float: left; font-size: 10pt">
                                        <font color="red">Additional Error Information Available</font></div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Collapsible Panel Target -->
                        <div id="Div2" class="collapseTargetPanel" style="text-align: left" align="left">
                            <asp:Panel ID="ContentPanel" runat="server" Height="50px" Width="100%">
                            
                                <dx:ASPxLabel ID="lblErrorMessage" runat="server" Text="">
                                </dx:ASPxLabel>
                            </asp:Panel>
                        </div>
                        
                    </div>
                </dx:PanelContent>
            </PanelCollection>
            <TopLeftCorner Height="2px" Url="~/App_Themes/RedGlass/Web/rpTopLeftCorner.png" Width="2px">
            </TopLeftCorner>
            <BottomLeftCorner Height="2px" Url="~/App_Themes/RedGlass/Web/rpBottomLeftCorner.png"
                Width="2px">
            </BottomLeftCorner>
        </dx:ASPxRoundPanel>
    </div>
    <br />
    <br />
</asp:Content>
