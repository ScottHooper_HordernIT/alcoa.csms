﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'CsmsAccess' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table CsmsAccess</remark>
	[Serializable, Flags]
	public enum CsmsAccessList
	{
		/// <summary> 
		/// Alcoa (Internal)
		/// </summary>
		[EnumTextValue(@"Alcoa (Internal)")]
		AlcoaLan = 1, 

		/// <summary> 
		/// AlcoaDirect (External)
		/// </summary>
		[EnumTextValue(@"AlcoaDirect (External)")]
		AlcoaDirect = 2

	}
}
