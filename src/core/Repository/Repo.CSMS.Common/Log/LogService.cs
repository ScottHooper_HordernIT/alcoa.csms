﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Web;

namespace Repo.CSMS.Common.Log
{
    public class LogService : ILogService
    {
        private ILog logger;
        private bool isConfigured = false;

        public LogService()
        {
            if (!isConfigured)
            {
                logger = LogManager.GetLogger(typeof(LogService));
                log4net.Config.XmlConfigurator.Configure();
            }
        }

        public ILog Logger()
        {
            return logger;
        }

        #region Log Error

        public void LogError(string message)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Error(message);
        }

        public void LogError(string message, string user)
        {
            MDC.Set("user", user);
            this.logger.Error(message);
        }

        public void LogError(string message, Exception ex, string user)
        {
            MDC.Set("user", user);
            this.logger.Error(message, ex);
        }

        public void LogError(Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Error("", ex);
        }

        public void LogError(string message, Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Error(message, ex);
        }

        #endregion

        #region Log Warning

        public void LogWarning(string message)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Warn(message);
        }

        public void LogWarning(string message, string user)
        {
            MDC.Set("user", user);
            this.logger.Warn(message);
        }

        public void LogWarning(string message, Exception ex, string user)
        {
            MDC.Set("user", user);
            this.logger.Warn(message, ex);
        }

        public void LogWarning(Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Warn("", ex);
        }

        public void LogWarning(string message, Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Warn(message, ex);
        }

        #endregion

        #region Log Info

        public void LogInfo(string message)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Info(message);
        }

        public void LogInfo(string message, string user)
        {
            MDC.Set("user", user);
            this.logger.Info(message);
        }

        public void LogInfo(string message, Exception ex, string user)
        {
            MDC.Set("user", user);
            this.logger.Info(message, ex);
        }

        public void LogInfo(Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Info("", ex);
        }

        public void LogInfo(string message, Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Info(message, ex);
        }

        #endregion

        #region Log Debug

        public void LogDebug(string message)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Debug(message);
        }

        public void LogDebug(string message, string user)
        {
            MDC.Set("user", user);
            this.logger.Debug(message);
        }

        public void LogDebug(string message, Exception ex, string user)
        {
            MDC.Set("user", user);
            this.logger.Debug(message, ex);
        }

        public void LogDebug(Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Debug("", ex);
        }

        public void LogDebug(string message, Exception ex)
        {
            string user = "";
            try
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                    user = Common.Mvc.Utils.NameWithoutDomain(HttpContext.Current.User.Identity.Name);
            }
            catch { }

            MDC.Set("user", user);
            this.logger.Debug(message, ex);
        }

        #endregion
    }
}
