﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminTaskClose.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.AdminTaskClose" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<p>
    &nbsp;<a href="AdminTasks.aspx">Admin Tasks Worklist</a> &gt; <strong>Task Number:</strong>
    <dx:ASPxLabel ID="lblTaskNo" runat="server" Text="(NEW)">
    </dx:ASPxLabel>
</p>
<p>
    Please review the below information and enter in any task comments (if 
    applicable) before closing this task.</p>
<table class="style1">
    <tr>
        <td class="style8">
            Source:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbSource" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="AdminTaskSourceDataSource1"
                TextField="AdminTaskSourceDesc" ValueField="AdminTaskSourceId">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Type:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="AdminTaskTypeDataSource1"
                TextField="AdminTaskTypeName" ValueField="AdminTaskTypeId" Enabled="False">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Access:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbAccess" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" 
                DataSourceID="CsmsAccessDataSource1" TextField="AccessDesc"
                ValueField="CsmsAccessId" Enabled="False">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style8">
            Login:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbLogin" runat="server" Style="margin-bottom: 8px" Width="270px"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            E-Mail Address:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbEmail" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RegularExpression ErrorText="Not a Valid E-Mail." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style8">
            Company:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="CompaniesDataSource1" TextField="CompanyName"
                ValueField="CompanyId" Enabled="False">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            First Name:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbFirstName" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Last Name:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbLastName" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style8">
            Position Held:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbJobTitle" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Mobile Number:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbMobileNumber" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Telephone Number:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbTelephoneNo" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Fax Number:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbFaxNo" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="False">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
</table>
<p>
    <data:CompaniesDataSource ID="CompaniesDataSource1" runat="server">
    </data:CompaniesDataSource>
    <data:AdminTaskTypeDataSource ID="AdminTaskTypeDataSource1" runat="server">
    </data:AdminTaskTypeDataSource>
    <data:AdminTaskSourceDataSource ID="AdminTaskSourceDataSource1" runat="server">
    </data:AdminTaskSourceDataSource>
    <data:CsmsAccessDataSource ID="CsmsAccessDataSource1" runat="server">
    </data:CsmsAccessDataSource>
</p>
<strong>Task Comments</strong> i.e. Reasons for Task Closure (Stored against task - Not sent to User):<br />
            <dx:ASPxMemo ID="mTaskComments" runat="server" Height="71px" Width="900px">
            </dx:ASPxMemo><br />
            <dx:ASPxButton ID="btnCloseTask" runat="server" Text="Close Task"
                Width="290px" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                OnClick="btnCloseTask_Click" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dx:ASPxButton>

