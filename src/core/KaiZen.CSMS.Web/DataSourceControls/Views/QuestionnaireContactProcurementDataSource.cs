﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireContactProcurementProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireContactProcurementDataSourceDesigner))]
	public class QuestionnaireContactProcurementDataSource : ReadOnlyDataSource<QuestionnaireContactProcurement>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementDataSource class.
		/// </summary>
		public QuestionnaireContactProcurementDataSource() : base(new QuestionnaireContactProcurementService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireContactProcurementDataSourceView used by the QuestionnaireContactProcurementDataSource.
		/// </summary>
		protected QuestionnaireContactProcurementDataSourceView QuestionnaireContactProcurementView
		{
			get { return ( View as QuestionnaireContactProcurementDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireContactProcurementDataSourceView class that is to be
		/// used by the QuestionnaireContactProcurementDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireContactProcurementDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireContactProcurement, Object> GetNewDataSourceView()
		{
			return new QuestionnaireContactProcurementDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireContactProcurementDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireContactProcurementDataSourceView : ReadOnlyDataSourceView<QuestionnaireContactProcurement>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireContactProcurementDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireContactProcurementDataSourceView(QuestionnaireContactProcurementDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireContactProcurementDataSource QuestionnaireContactProcurementOwner
		{
			get { return Owner as QuestionnaireContactProcurementDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireContactProcurementService QuestionnaireContactProcurementProvider
		{
			get { return Provider as QuestionnaireContactProcurementService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireContactProcurementDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireContactProcurementDataSource class.
	/// </summary>
	public class QuestionnaireContactProcurementDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireContactProcurement>
	{
	}

	#endregion QuestionnaireContactProcurementDataSourceDesigner

	#region QuestionnaireContactProcurementFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContactProcurementFilter : SqlFilter<QuestionnaireContactProcurementColumn>
	{
	}

	#endregion QuestionnaireContactProcurementFilter

	#region QuestionnaireContactProcurementExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContactProcurementExpressionBuilder : SqlExpressionBuilder<QuestionnaireContactProcurementColumn>
	{
	}
	
	#endregion QuestionnaireContactProcurementExpressionBuilder		
}

