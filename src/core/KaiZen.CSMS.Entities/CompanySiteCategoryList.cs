﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'CompanySiteCategory' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table CompanySiteCategory</remark>
	[Serializable]
	public enum CompanySiteCategoryList
	{
		/// <summary> 
		/// Embedded
		/// </summary>
		[EnumTextValue(@"Embedded")]
		E = 1, 

		/// <summary> 
		/// Non-Embedded 1
		/// </summary>
		[EnumTextValue(@"Non-Embedded 1")]
		NE1 = 2, 

		/// <summary> 
		/// Non-Embedded 2
		/// </summary>
		[EnumTextValue(@"Non-Embedded 2")]
		NE2 = 3, 

		/// <summary> 
		/// Non-Embedded 3
		/// </summary>
		[EnumTextValue(@"Non-Embedded 3")]
		NE3 = 4

	}
}
