﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_CSA" Codebehind="CSA.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
    
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
    
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server" RenderMode="Inline"
    Visible="True">
    <ContentTemplate>
<table border="0" cellpadding="2" cellspacing="0" width="900">
    <tr>
        <td class="pageName" colspan="8" style="height: 44px; width: 796px;">
            <span class="title">Safety</span><br />
            <span class="date">Contractor Services Audit</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left; width: 796px;">
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="bodyText" style="height: 20px; text-align: left" valign="top">
                        <span style="font-size: 12pt; color: navy"><strong>Select Company/Timeframe</strong></span>
                    </td>
                </tr>
                <tr>
                    <td class="bodyText" style="height: 17px; text-align: left" valign="top">
                        <dxcp:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
                            <PanelCollection>
                                <dxp:PanelContent runat="server">
                                    <table border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 110px; text-align: right">
                                                    <strong>Company Name:</strong>
                                                         </td>
                                                <td style="text-align: left" colspan="2">
                                                    <%--<dxe:ASPxComboBox runat="server" EnableSynchronization="False" ValueType="System.Int32"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ClientInstanceName="ddlCompanies" Width="350px" ID="ddlCompanies" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged">
                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                        ddlSites.PerformCallback(s.GetValue());
                                    }"></ClientSideEvents>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px" Cursor="pointer">
                                                        </ButtonStyle>
                                                    </dxe:ASPxComboBox>--%>
                                                    <dxe:ASPxComboBox runat="server" AutoPostBack="true" EnableSynchronization="False" ValueType="System.Int32"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ClientInstanceName="ddlCompanies" Width="350px" ID="ddlCompanies" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px" Cursor="pointer">
                                                        </ButtonStyle>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                                <td style="width: 150px; text-align: right" colspan="2" rowspan="3">
                                                    <dxe:ASPxButton runat="server" Wrap="True" Text="View/Enter/Update" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Width="140px" Height="63px" Font-Bold="True" ForeColor="Navy" ID="btnSearchGo"
                                                        OnClick="Button2_Click1" ValidationGroup="Go">
                                                        <ClientSideEvents Click="function(s, e) {
                                            needToConfirm = true;
                                        }"></ClientSideEvents>
                                                        <Paddings Padding="0px" PaddingLeft="0px" PaddingTop="0px" PaddingRight="0px" PaddingBottom="0px">
                                                        </Paddings>
                                                    </dxe:ASPxButton>
                                                </td>
                                                <td style="width: 150px; text-align: right" colspan="1" rowspan="3">
                                                    <button style="font-weight: bold; width: 140px; height: 63px" onclick="javascript:print();"
                                                        name="btnPrint" type="button">
                                                        Print</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; height: 15px; text-align: right">
                                                    <span style="font-size: 10pt"><strong>Site:</strong></span>
                                                         </td>
                                                <td style="font-size: 10pt; height: 15px; text-align: left" colspan="2">
                                                    <%--<dxe:ASPxComboBox runat="server" EnableSynchronization="False" ValueType="System.Int32"
                                                        TextField="SiteName" ValueField="SiteId"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ClientInstanceName="ddlSites" ID="ddlSites" OnCallback="ddlSites_Callback">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px" Cursor="pointer">
                                                        </ButtonStyle>
                                                        <ValidationSettings CausesValidation="True" SetFocusOnError="True" ValidationGroup="Go">
                                                            <RequiredField IsRequired="True" />
                                                        </ValidationSettings>
                                                    </dxe:ASPxComboBox>--%>
                                                    <dxe:ASPxComboBox runat="server" AutoPostBack="true" EnableSynchronization="False" ValueType="System.Int32"
                                                        TextField="SiteName" ValueField="SiteId"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                        IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ClientInstanceName="ddlSites" ID="ddlSites" OnSelectedIndexChanged="ddlSites_SelectedIndexChanged">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px" Cursor="pointer">
                                                        </ButtonStyle>
                                                        <%--<ValidationSettings CausesValidation="True" SetFocusOnError="True" ValidationGroup="Go">
                                                            <RequiredField IsRequired="True" />
                                                        <RequiredField IsRequired="True" /></ValidationSettings>--%>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; height: 15px; text-align: right">
                                                    <strong>Period:</strong>
                                                         </td>
                                                <td style="width: 147px; height: 15px; text-align: left">
                                                    <dxe:ASPxComboBox ID="cbQtr" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        Width="145px" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                        <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip"
                                                            SetFocusOnError="True" ValidationGroup="Go">
                                                            <RequiredField IsRequired="True" />
                                                        <RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /></ValidationSettings>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                                <td style="width: 333px; height: 15px; text-align: left">
                                                    <dxe:ASPxComboBox ID="cbYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        ValueType="System.Int32" Width="60px" IncrementalFilteringMode="StartsWith">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                        <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True"
                                                            ValidationGroup="Go">
                                                            <RequiredField IsRequired="True" />
                                                        <RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /><RequiredField IsRequired="True" /></ValidationSettings>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </dxp:PanelContent>
                            </PanelCollection>
                        </dxcp:ASPxCallbackPanel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />

    <asp:Panel ID="panelCSA" runat="server" visible="false">
<TABLE cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="WIDTH: 100px; HEIGHT: 1044px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
        <tr>
            <td style="width: 900px; height: 13px">
                <strong>Created By: </strong>
                <asp:Label ID="lblCreatedBy" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 900px">
                <strong>Modified By: </strong>
                <asp:Label ID="lblModifiedBy" runat="server"></asp:Label></td>
        </tr>
    </table>
    <br />
    <TABLE class="csa-table" style="BORDER-RIGHT: #4f93e3 1px solid; TABLE-LAYOUT: auto; BORDER-TOP: #4f93e3 1px solid; FONT: 11px Tahoma; OVERFLOW: hidden; BORDER-LEFT: #4f93e3 1px solid; COLOR: black; BORDER-BOTTOM: #4f93e3 1px solid; BACKGROUND-COLOR: transparent" width=900><TBODY><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD style="WIDTH: 623px; HEIGHT: 15px"><STRONG class="group-title">1.0 Administrative Controls</STRONG><input type="button" class="group-button-na" id="btnAdministrativeControls" runat="server" value="Set Section to 'N/A'" /></TD><TD style="WIDTH: 100px; HEIGHT: 15px"><STRONG>Achieved</STRONG></TD><TD style="WIDTH: 41px; HEIGHT: 15px"><STRONG>Score</STRONG></TD><TD style="WIDTH: 253px; HEIGHT: 15px"><STRONG>If No, describe Corrective Action,<br />who and by when. <br />If Best Practice, comments.</STRONG></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">a) H&amp;S Plan has been submitted electronically to the Alcoa Portal.</SPAN></TD><TD style="FONT-SIZE: 7pt; WIDTH: 100px">
                        <asp:DropDownList ID="cb1a" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList>
                        
                            </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center;"><asp:Label id="score1a" runat="server"></asp:Label></TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o1a" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN><SPAN style="FONT-SIZE: 8pt; BACKGROUND-COLOR: #edf5ff">b) H &amp; S activiites matrix in place (or 2 week look ahead).</SPAN></SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px">
                            <asp:DropDownList ID="cb1b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList>
                             </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1b" runat="server"></asp:Label> </TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o1b" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">c) Company and Alcoa H&amp;S policy statements displayed in offices and crib rooms.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1c" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList></TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1c" runat="server"></asp:Label><SPAN style="FONT-SIZE: 8pt"> </SPAN></TD><TD style="FONT-SIZE: 8pt" width=253><asp:TextBox id="o1c" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">d) Contractor employees meet the current medical assessment requirements.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1d" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1d" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o1d" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">e) Applicable Contractor employees have current Mobile Equipment Medicals.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1e" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1e" runat="server"></asp:Label><SPAN style="FONT-SIZE: 8pt"> </SPAN></TD><TD style="FONT-SIZE: 8pt" width=253><asp:TextBox id="o1e" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">f) Are H&amp;S resources available at the Location.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1f" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1f" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o1f" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">g) All incidents are investigated within 24 hours and data input to the Alcoa system.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1g" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1g" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1g" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="FONT-WEIGHT: bold; HEIGHT: 2px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"></TD><TD width=253></TD></TR><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; FONT-WEIGHT: bold; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD colSpan=4>1.1 A Register is available, and information current for - </TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px; HEIGHT: 13px">h) Portable Electrical Tools &amp; Equipment</TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb1h" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score1h" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold; HEIGHT: 13px" width=253><asp:TextBox id="o1h" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">i) Lifting Gear</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px"><asp:DropDownList ID="cb1i" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value=" " Text=" "></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1i" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1i" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">j) Fall Arrest Equipment</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1j" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1j" runat="server"></asp:Label></TD><TD style="FONT-WEIGHT: bold" width=253><asp:TextBox id="o1j" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">k) Employee Certificates of Competency and Driver’s Licence</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1k" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1k" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1k" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">l) Hazardous Substances</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1l" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1l" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1l" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">m) Mobile Telephone Approvals</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1m" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1m" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1m" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">n) Camera Approvals</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1n" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1n" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1n" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">o) Fire Extinguishers (including in vehicles)</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1o" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1o" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1o" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">p) Sub contractor Permits</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1p" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1p" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1p" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px; HEIGHT: 13px">q) Standard Work Instructions / Work Method Statements</TD><TD style="WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb1q" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score1q" runat="server"></asp:Label></TD><TD style="HEIGHT: 13px" width=253><asp:TextBox id="o1q" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">r) Hot Work Equipment, Gauges, flash arrestors etc</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb1r" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score1r" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o1r" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Total Possible Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><SPAN style="COLOR: #ff0000">90</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px"></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Actual Applicable Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><asp:Label id="score1_a" Width="40px" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; COLOR: #000000; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score1_b" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; text-align: center;"><asp:Label id="score1_c" runat="server" Text="0%"></asp:Label></TD></TR></TBODY></TABLE><BR /><TABLE class="csa-table" style="BORDER-RIGHT: #4f93e3 1px solid; TABLE-LAYOUT: auto; BORDER-TOP: #4f93e3 1px solid; FONT: 11px Tahoma; OVERFLOW: hidden; BORDER-LEFT: #4f93e3 1px solid; COLOR: black; BORDER-BOTTOM: #4f93e3 1px solid; BACKGROUND-COLOR: transparent" width=900><TBODY><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD style="WIDTH: 623px; HEIGHT: 15px"><STRONG class="group-title">2.0 KPI Compliance</STRONG><input type="button" class="group-button-na" id="btnKpiCompliance" runat="server" value="Set Section to 'N/A'" /></TD><TD style="WIDTH: 100px; HEIGHT: 15px"><STRONG>Achieved</STRONG></TD><TD style="WIDTH: 41px; HEIGHT: 15px"><STRONG>Score</STRONG></TD><TD style="WIDTH: 253px; HEIGHT: 15px"><STRONG>If No, describe Corrective Action,<br />who and by when. <br />If Best Practice, comments.</STRONG></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">a) Contractor Services Audit completed quarterly by the Contractor at the Location.</SPAN></TD><TD style="FONT-SIZE: 7pt; WIDTH: 100px"><asp:DropDownList ID="cb2a" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center;"><asp:Label id="score2a" runat="server"></asp:Label></TD><TD style="COLOR: #000000;" width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o2a" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN><SPAN style="FONT-SIZE: 8pt; BACKGROUND-COLOR: #edf5ff">b) JSA Field Audit Verifications completed using the Alcoa approved Template.</SPAN></SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2b" runat="server"></asp:Label> </TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o2b" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">c) Workplace Safety &amp; Compliance Inspections completed using a Template that has been approved by the Location Contractor Services Supervisor/Safety Manager Mining.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2c" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2c" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o2c" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">d) Management Safety Work Contacts have been completed by representatives of the Contractor Management Team, using the Alcoa approved Template.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2d" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2d" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o2d" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">e) Behavioural Observations have been completed by the Contractor employees who have received training in the Program being used at the Location.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2e" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2e" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o2e" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">f) Weekly Toolbox Meetings conducted - this meeting is for a brief, informal information process that the supervisor and their direct work team can discuss and agree upon H&S issues. This is not a daily Prestart meeting.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2f" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2f" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o2f" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">g) Alcoa Weekly Contractors Meeting attendance - arranged by the Location Contractor Services Supervisor and Attendance is recorded in the Meeting Minutes.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2g" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2g" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o2g" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">h) Alcoa Mandatory Training Program - Number of employees trained per month is equal to the average number of employees on site for the same month.</SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb2h" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score2h" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o2h" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Total Possible Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><SPAN style="COLOR: #ff0000">40</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px"></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Actual Applicable Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><asp:Label id="score2_a" Width="40px" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; COLOR: #000000; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score2_b" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; text-align: center;"><asp:Label id="score2_c" runat="server" Text="0%"></asp:Label></TD></TR></TBODY></TABLE><BR /><TABLE class="csa-table" style="BORDER-RIGHT: #4f93e3 1px solid; TABLE-LAYOUT: auto; BORDER-TOP: #4f93e3 1px solid; FONT: 11px Tahoma; OVERFLOW: hidden; BORDER-LEFT: #4f93e3 1px solid; COLOR: black; BORDER-BOTTOM: #4f93e3 1px solid; BACKGROUND-COLOR: transparent" width=900><TBODY><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD style="WIDTH: 623px; HEIGHT: 15px"><STRONG class="group-title">3.0 H&amp;S Communication</STRONG><input type="button" class="group-button-na" id="btnHsCommunication" runat="server" value="Set Section to 'N/A'" /></TD><TD style="WIDTH: 100px; HEIGHT: 15px"><STRONG>Achieved</STRONG></TD><TD style="WIDTH: 41px; HEIGHT: 15px"><STRONG>Score</STRONG></TD><TD style="WIDTH: 253px; HEIGHT: 15px"><STRONG>If No, describe Corrective Action,<br />who and by when.<br />If Best Practice, comments.</STRONG></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">a) Pre-Shift briefings are held. Evidence that they address H&amp;S and are documented.</SPAN></TD><TD style="FONT-SIZE: 7pt; WIDTH: 100px"><asp:DropDownList ID="cb3a" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center;"><asp:Label id="score3a" runat="server"></asp:Label></TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o3a" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN><SPAN style="FONT-SIZE: 8pt; BACKGROUND-COLOR: #edf5ff">b) Close-Out briefings are held. Evidence that they address H&amp;S and any incidents from the shift are documented. </SPAN></SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3b" runat="server"></asp:Label> </TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o3b" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">c) Attendance at Pre-Shift briefings and Close-Out briefings is recorded.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3c" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3c" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o3c" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">d) Safety Meeting and Toolbox Meeting Minutes demonstrate that items are closed out and that any H&S suggestions are recorded.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3d" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3d" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3d" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">e) H&amp;S Training Toolbox Meetings are held at least monthly.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3e" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3e" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o3e" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">f) Attendance at Safety Meetings and Toolbox Meetings is recorded. </TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3f" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3f" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o3f" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">g) The Contractor has identified their Top 3 potential Fatality issues for discussion. </TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3g" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3g" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3g" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">h) A Fatality Prevention presentation and discussion is completed each month.</SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3h" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3h" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3h" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px; HEIGHT: 13px">i) Fatality Prevention action plans are established, presented and being addressed.</TD><TD style="WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb3i" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score3i" runat="server"></asp:Label></TD><TD style="HEIGHT: 13px" width=253><asp:TextBox id="o3i" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px; HEIGHT: 13px">j) The Contractor has forwarded a copy of their Top 3 Fatality Prevention issues to the Location Contractor Services Supervisor/Safety Manager Mining.</TD><TD style="WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb3j" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score3j" runat="server"></asp:Label></TD><TD style="HEIGHT: 13px" width=253><asp:TextBox id="o3j" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">k) Notice Board provided in Offices and Crib Room.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3k" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3k" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3k" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">l) Alcoa Safety Alerts and Incident Reports are on display and are current.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3l" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3l" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3l" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">m) Emergency Procedures and contact phone numbers displayed.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3m" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3m" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3m" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">n) Evacuation Procedures and Muster Points are known by the Work Group.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3n" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3n" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3n" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">o) An Emergency Evacuation Drill involving the Work Group has been conducted within the last twelve months.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb3o" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score3o" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o3o" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Total Possible Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><SPAN style="COLOR: #ff0000">75</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px"></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Actual Applicable Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><asp:Label id="score3_a" Width="40px" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; COLOR: #000000; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score3_b" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; text-align: center;"><asp:Label id="score3_c" runat="server" Text="0%"></asp:Label></TD></TR></TBODY></TABLE><BR /><TABLE class="csa-table" style="BORDER-RIGHT: #4f93e3 1px solid; TABLE-LAYOUT: auto; BORDER-TOP: #4f93e3 1px solid; FONT: 11px Tahoma; OVERFLOW: hidden; BORDER-LEFT: #4f93e3 1px solid; COLOR: black; BORDER-BOTTOM: #4f93e3 1px solid; BACKGROUND-COLOR: transparent" width=900><TBODY><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD style="WIDTH: 623px; HEIGHT: 15px"><STRONG class="group-title">4.0 Offices, Yard and Crib Facilities</STRONG><input type="button" class="group-button-na" id="btnOfficesYardCribFacilities" runat="server" value="Set Section to 'N/A'" /></TD><TD style="WIDTH: 100px; HEIGHT: 15px"><STRONG>Achieved</STRONG></TD><TD style="WIDTH: 41px; HEIGHT: 15px"><STRONG>Score</STRONG></TD><TD style="WIDTH: 253px; HEIGHT: 15px"><STRONG>If No, describe Corrective Action,<br />who and by when. <br />If Best Practice, comments.</STRONG></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">a) Access and Egress clearly marked – emergency exit signage.</SPAN></TD><TD style="FONT-SIZE: 7pt; WIDTH: 100px"><asp:DropDownList ID="cb4a" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center;"><asp:Label id="score4a" runat="server"></asp:Label></TD><TD style="COLOR: #000000;" width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o4a" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN><SPAN style="FONT-SIZE: 8pt; BACKGROUND-COLOR: #edf5ff">b) Offices, Yard and Crib Facilities are kept clean. </SPAN></SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4b" runat="server"></asp:Label><SPAN style="BACKGROUND-COLOR: #edf5ff"> </SPAN></TD><TD style="BACKGROUND-COLOR: #edf5ff" width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o4b" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">c) Housekeeping schedules being met.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4c" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4c" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o4c" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">d) There is a Waste Management program.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4d" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4d" runat="server"></asp:Label><SPAN style="BACKGROUND-COLOR: #edf5ff"> </SPAN></TD><TD style="BACKGROUND-COLOR: #edf5ff" width=253><asp:TextBox id="o4d" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">e) Waste Bin locations prominent, adequate, emptied regularly, and recycling compliance is high.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4e" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4e" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o4e" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">f) Materials, food and office equipment are stored correctly.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4f" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4f" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4f" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">g) Hazardous Materials are not stored in or adjacent to Office and Crib facilities. </TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4g" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4g" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4g" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">h) Hazardous Materials are segregated and bunded to standard.</SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4h" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4h" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4h" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px; HEIGHT: 13px">i) All hazardous substance containers are labelled.</TD><TD style="WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb4i" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score4i" runat="server"></asp:Label></TD><TD style="HEIGHT: 13px" width=253><asp:TextBox id="o4i" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px; HEIGHT: 13px">j) Procedures clearly visible in case of fire or spills. </TD><TD style="WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb4j" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score4j" runat="server"></asp:Label></TD><TD style="HEIGHT: 13px" width=253><asp:TextBox id="o4j" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">k) Fire extinguishers and Emergency equipment accessible and sign posted.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4k" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4k" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4k" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR>
                        <TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; FONT-WEIGHT: bold; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD colSpan=4>Traffic Management applied to your work area or facilities</TD></TR>
                            <TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">l) Areas delineated for vehicle parking.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4l" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4l" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4l" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">m) Laydown areas are clearly defined.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4m" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4m" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4m" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">n) Signs, road markings complied with.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4n" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4n" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4n" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">o) Pedestrians are segregated from mobile equipment travel ways.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb4o" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score4o" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o4o" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Total Possible Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; height: 13px; text-align: center;"><SPAN style="COLOR: #ff0000">75</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; height: 13px;"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; height: 13px;"></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Actual Applicable Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><asp:Label id="score4_a" Width="40px" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; COLOR: #000000; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score4_b" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; text-align: center;"><asp:Label id="score4_c" runat="server" Text="0%"></asp:Label></TD></TR></TBODY></TABLE><BR /><TABLE class="csa-table" style="BORDER-RIGHT: #4f93e3 1px solid; TABLE-LAYOUT: auto; BORDER-TOP: #4f93e3 1px solid; FONT: 11px Tahoma; OVERFLOW: hidden; BORDER-LEFT: #4f93e3 1px solid; COLOR: black; BORDER-BOTTOM: #4f93e3 1px solid; BACKGROUND-COLOR: transparent" width=900><TBODY><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD style="WIDTH: 623px; HEIGHT: 15px"><STRONG class="group-title">5.0 Alcoa Mandatory H&amp;S Training</STRONG><input type="button" class="group-button-na" id="btnAlcoaMandatoryHsTraining" runat="server" value="Set Section to 'N/A'" /></TD><TD style="WIDTH: 100px; HEIGHT: 15px"><STRONG>Achieved</STRONG></TD><TD style="WIDTH: 41px; HEIGHT: 15px"><STRONG>Score</STRONG></TD><TD style="WIDTH: 253px; HEIGHT: 15px"><STRONG>If No, describe Corrective Action,<br />who and by when. <br />If Best Practice, comments.</STRONG></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD colspan="4">
                            <span style="color: maroon">Annual Mandatory Training has been completed by all Contractor employees who are, or have been working at the Location: </span>
                        </TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">a) Fatality Prevention</SPAN></TD><TD style="FONT-SIZE: 7pt; WIDTH: 100px"><asp:DropDownList ID="cb5a" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center;"><asp:Label id="score5a" runat="server"></asp:Label></TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o5a" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN><SPAN style="FONT-SIZE: 8pt; BACKGROUND-COLOR: #edf5ff">b) Hazardous Materials</SPAN></SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb5b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score5b" runat="server"></asp:Label> </TD><TD width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o5b" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">c) Hearing Conservation</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb5c" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score5c" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o5c" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">d) Respiratory Protection</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb5d" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score5d" runat="server"></asp:Label><SPAN style="FONT-SIZE: 8pt"> </SPAN></TD><TD style="FONT-SIZE: 8pt" width=253><asp:TextBox id="o5d" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="FONT-SIZE: 8pt; HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">e) All Mandatory Training is recorded.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb5e" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score5e" runat="server"></asp:Label><SPAN style="COLOR: #ff0000"> </SPAN></TD><TD style="COLOR: #ff0000" width=253><asp:TextBox id="o5e" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">f) The company Contractor Responsible Person (CRP) training has been completed.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb5f" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score5f" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o5f" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Total Possible Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><SPAN style="COLOR: #ff0000">30</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px"></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Actual Applicable Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><asp:Label id="score5_a" Width="40px" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; COLOR: #000000; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score5_b" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; text-align: center;"><asp:Label id="score5_c" runat="server" Text="0%"></asp:Label></TD></TR></TBODY></TABLE><BR /><TABLE class="csa-table" style="BORDER-RIGHT: #4f93e3 1px solid; TABLE-LAYOUT: auto; BORDER-TOP: #4f93e3 1px solid; FONT: 11px Tahoma; OVERFLOW: hidden; BORDER-LEFT: #4f93e3 1px solid; COLOR: black; BORDER-BOTTOM: #4f93e3 1px solid; BACKGROUND-COLOR: transparent" width=900><TBODY><TR style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; BACKGROUND: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top; PADDING-BOTTOM: 8px; COLOR: black; PADDING-TOP: 8px; BORDER-BOTTOM: #4f93e3 1px solid" height=16><TD style="WIDTH: 623px; HEIGHT: 15px"><STRONG class="group-title">6.0 Contractor Employee Skills and Training </STRONG><input type="button" class="group-button-na" id="btnContractorEmployeeSkillsTraining" runat="server" value="Set Section to 'N/A'" /></TD><TD style="WIDTH: 100px; HEIGHT: 15px"><STRONG>Achieved</STRONG></TD><TD style="WIDTH: 41px; HEIGHT: 15px"><STRONG>Score</STRONG></TD><TD style="WIDTH: 253px; HEIGHT: 15px"><STRONG>If No, describe Corrective Action,<br />who and by when. <br />If Best Practice, comments.</STRONG></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">a) There is a documented Employee Skills Matrix for employees at this Location.</SPAN></TD><TD style="FONT-SIZE: 7pt; WIDTH: 100px"><asp:DropDownList ID="cb6a" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center;"><asp:Label id="score6a" runat="server"></asp:Label></TD><TD style="COLOR: #000000;" width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o6a" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN><SPAN style="FONT-SIZE: 8pt; BACKGROUND-COLOR: #edf5ff">b) There is a documented Training Needs Analysis for the scope of work at this Location.</SPAN></SPAN></TD><TD style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; WIDTH: 100px"><asp:DropDownList ID="cb6b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6b" runat="server"></asp:Label><SPAN style="BACKGROUND-COLOR: #edf5ff"> </SPAN></TD><TD style="BACKGROUND-COLOR: #edf5ff" width=253><SPAN style="FONT-SIZE: 7pt"><asp:TextBox id="o6b" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></SPAN></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">c) A Training Plan based on the identified training requirements has been developed and implemented.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb6c" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6c" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o6c" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">d) There is a documented Program or Schedule for employee retraining and competency assessment.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb6d" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6d" runat="server"></asp:Label><SPAN style="BACKGROUND-COLOR: #edf5ff"> </SPAN></TD><TD style="BACKGROUND-COLOR: #edf5ff" width=253><asp:TextBox id="o6d" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">e) Records confirm that Certificates of Competency are current.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb6e" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6e" runat="server"></asp:Label> </TD><TD width=253><asp:TextBox id="o6e" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px">f) Records confirm that relevant employees have a current National Licence for performing high risk work.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb6f" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6f" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o6f" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px">g) There is a documented induction process for all new, re-assigned or transferred employees.</TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb6g" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6g" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o6g" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: #edf5ff"><TD style="WIDTH: 623px"><SPAN style="FONT-SIZE: 8pt">h) Applicable employees are trained in Incident Investigation and Recording techniques.</SPAN></TD><TD style="WIDTH: 100px"><asp:DropDownList ID="cb6h" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; TEXT-ALIGN: center"><asp:Label id="score6h" runat="server"></asp:Label></TD><TD width=253><asp:TextBox id="o6h" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="HEIGHT: 13px; BACKGROUND-COLOR: white"><TD style="WIDTH: 623px; HEIGHT: 13px">i) Training and Assessment is delivered by persons with appropriate knowledge, skills and experience.</TD><TD style="WIDTH: 100px; HEIGHT: 13px"><asp:DropDownList ID="cb6i" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="-" Text="N/A"></asp:ListItem>
                            <asp:ListItem Value="0" Text="No"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Yes"></asp:ListItem>
                            <asp:ListItem Value="5" Text="Best Practice"></asp:ListItem>
                        </asp:DropDownList> </TD><TD style="WIDTH: 41px; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score6i" runat="server"></asp:Label></TD><TD style="HEIGHT: 13px" width=253><asp:TextBox id="o6i" Width="244px" runat="server" TextMode="MultiLine"></asp:TextBox></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Total Possible Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><SPAN style="COLOR: #ff0000">45</SPAN></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px"></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px"></TD></TR><TR style="COLOR: #000000; HEIGHT: 13px; BACKGROUND-COLOR: palegoldenrod"><TD style="WIDTH: 623px; HEIGHT: 13px; TEXT-ALIGN: right"><STRONG>Actual Applicable Score:</STRONG></TD><TD style="FONT-SIZE: 8pt; WIDTH: 100px; text-align: center;"><asp:Label id="score6_a" Width="40px" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 41px; COLOR: #000000; HEIGHT: 13px; TEXT-ALIGN: center"><asp:Label id="score6_b" runat="server" Text="0"></asp:Label></TD><TD style="FONT-SIZE: 8pt; WIDTH: 253px; text-align: center;"><asp:Label id="score6_c" runat="server" Text="0%"></asp:Label></TD></TR></TBODY></TABLE></TD></TR>
    <tr>
        <td style="width: 100px">
        <br />
        <br />
            <table style="border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid;
                font: 11px Tahoma; overflow: hidden; border-left: #4f93e3 1px solid; color: black;
                border-bottom: #4f93e3 1px solid; background-color: transparent" width="900">
                <tr height="16" style="padding-right: 4px; padding-left: 4px; background: url(./Images/gvGradient.gif) #94b6e8 repeat-x center top;
                    padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid">
                    <td colspan="5" style="height: 15px; text-align: center">
                        <strong>SUMMARY</strong></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 469px">
                        <strong>Item / Area</strong></td>
                    <td style="width: 50px; text-align: center">
                    </td>
                    <td style="width: 73px; text-align: center">
                        <strong>Applicable Score</strong></td>
                    <td style="width: 44px; text-align: center">
                        <strong>Actual Score</strong></td>
                    <td style="width: 253px; text-align: center">
                        <strong>Rating %</strong></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        1.0 Administrative Controls</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="score1_a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="score1_b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="score1_c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        2.0 KPI Compliance</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="score2_a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="score2_b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="score2_c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        3.0 H&amp;S Communication</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="score3_a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="score3_b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="score3_c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        4.0 Offices, Yard and Crib Facilities</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="score4_a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="score4_b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="score4_c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        5.0 Alcoa Mandatory H&amp;S Training</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="score5_a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="score5_b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="score5_c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        6.0 Contractor Employee Skills and Training</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="score6_a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="score6_b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="score6_c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                    </td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                        <strong>Total:</strong></td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="scoreTotal_a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="scoreTotal_b" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="scoreTotal_c" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: right">
                    </td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 253px; height: 13px; text-align: center">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <TR><TD style="WIDTH: 900px; HEIGHT: 96px; TEXT-ALIGN: right">
                            <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                                <tr>
                                    <td style="width: 650px; text-align: center">
                                        <asp:Label ID="lblSave" runat="server" Font-Bold="True" Font-Size="16pt" ForeColor="Red"
                                            Text=""></asp:Label></td>
                                    <td style="width: 100px">
                                        <dxe:ASPxButton id="btnSave" onclick="btnSave_Click" Width="167px" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" ForeColor="Navy" Font-Bold="True" Height="77px" Text="Save New Record" Wrap="True" HorizontalAlign="Center">
                    <Paddings Padding="0px" PaddingLeft="0px" PaddingTop="0px" PaddingRight="0px" PaddingBottom="0px"></Paddings>

                    <ClientSideEvents Click="function(s, e) {
                        needToConfirm = false;
	                    e.processOnServer = confirm('Are you sure you wish to save your changes?');
                    }"></ClientSideEvents>
                    </dxe:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </TD></TR></TBODY></TABLE>
</asp:Panel>
<data:SitesDataSource ID="SitesDataSource" runat="server" EnableDeepLoad="False"
    EnableSorting="true" SelectMethod="GetAll">
</data:SitesDataSource>
<data:TwentyOnePointAuditQtrDataSource ID="topaQtr" runat="server" EnableDeepLoad="False"
    EnableSorting="true" SelectMethod="GetAll">
</data:TwentyOnePointAuditQtrDataSource>
<data:CompaniesDataSource ID="CompaniesDataSource" runat="server" EnableDeepLoad="False"
    EnableSorting="true" SelectMethod="GetAll">
</data:CompaniesDataSource>
    </ContentTemplate>
</asp:UpdatePanel>
<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    EnableHotTrack="False" HeaderText="Warning" Height="111px" Modal="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" Width="439px">
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
            SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" ID="ASPxLabel1" Font-Size="14px" Text="You can not enter monthly KPI information for a future month."
                Height="30px">
                <Border BorderWidth="10px" BorderColor="White"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
