﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportTimeDelayAssessmentCompleteProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportTimeDelayAssessmentCompleteProviderBaseCore : EntityViewProviderBase<QuestionnaireReportTimeDelayAssessmentComplete>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt; Fill(DataSet dataSet, VList<QuestionnaireReportTimeDelayAssessmentComplete> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportTimeDelayAssessmentComplete>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportTimeDelayAssessmentComplete>"/></returns>
		protected static VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt; Fill(DataTable dataTable, VList<QuestionnaireReportTimeDelayAssessmentComplete> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportTimeDelayAssessmentComplete c = new QuestionnaireReportTimeDelayAssessmentComplete();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.ProcurementContact = (Convert.IsDBNull(row["ProcurementContact"]))?string.Empty:(System.String)row["ProcurementContact"];
					c.ContractManager = (Convert.IsDBNull(row["ContractManager"]))?string.Empty:(System.String)row["ContractManager"];
					c.ProcurementContactUserId = (Convert.IsDBNull(row["ProcurementContactUserId"]))?string.Empty:(System.String)row["ProcurementContactUserId"];
					c.ContractManagerUserId = (Convert.IsDBNull(row["ContractManagerUserId"]))?string.Empty:(System.String)row["ContractManagerUserId"];
					c.DaysToApprove = (Convert.IsDBNull(row["DaysToApprove"]))?(int)0:(System.Int32?)row["DaysToApprove"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedBy = (Convert.IsDBNull(row["ApprovedBy"]))?string.Empty:(System.String)row["ApprovedBy"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportTimeDelayAssessmentComplete&gt;"/></returns>
		protected VList<QuestionnaireReportTimeDelayAssessmentComplete> Fill(IDataReader reader, VList<QuestionnaireReportTimeDelayAssessmentComplete> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportTimeDelayAssessmentComplete entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportTimeDelayAssessmentComplete>("QuestionnaireReportTimeDelayAssessmentComplete",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportTimeDelayAssessmentComplete();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContact)];
					//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
					entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManager)];
					//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
					entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContactUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContactUserId)];
					//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?string.Empty:(System.String)reader["ProcurementContactUserId"];
					entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManagerUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManagerUserId)];
					//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?string.Empty:(System.String)reader["ContractManagerUserId"];
					entity.DaysToApprove = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.DaysToApprove)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.DaysToApprove)];
					//entity.DaysToApprove = (Convert.IsDBNull(reader["DaysToApprove"]))?(int)0:(System.Int32?)reader["DaysToApprove"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedBy = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedBy)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedBy)];
					//entity.ApprovedBy = (Convert.IsDBNull(reader["ApprovedBy"]))?string.Empty:(System.String)reader["ApprovedBy"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportTimeDelayAssessmentComplete entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContact)];
			//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
			entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManager)];
			//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
			entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContactUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ProcurementContactUserId)];
			//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?string.Empty:(System.String)reader["ProcurementContactUserId"];
			entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManagerUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ContractManagerUserId)];
			//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?string.Empty:(System.String)reader["ContractManagerUserId"];
			entity.DaysToApprove = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.DaysToApprove)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.DaysToApprove)];
			//entity.DaysToApprove = (Convert.IsDBNull(reader["DaysToApprove"]))?(int)0:(System.Int32?)reader["DaysToApprove"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedBy = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedBy)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.ApprovedBy)];
			//entity.ApprovedBy = (Convert.IsDBNull(reader["ApprovedBy"]))?string.Empty:(System.String)reader["ApprovedBy"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayAssessmentCompleteColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportTimeDelayAssessmentComplete entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.ProcurementContact = (Convert.IsDBNull(dataRow["ProcurementContact"]))?string.Empty:(System.String)dataRow["ProcurementContact"];
			entity.ContractManager = (Convert.IsDBNull(dataRow["ContractManager"]))?string.Empty:(System.String)dataRow["ContractManager"];
			entity.ProcurementContactUserId = (Convert.IsDBNull(dataRow["ProcurementContactUserId"]))?string.Empty:(System.String)dataRow["ProcurementContactUserId"];
			entity.ContractManagerUserId = (Convert.IsDBNull(dataRow["ContractManagerUserId"]))?string.Empty:(System.String)dataRow["ContractManagerUserId"];
			entity.DaysToApprove = (Convert.IsDBNull(dataRow["DaysToApprove"]))?(int)0:(System.Int32?)dataRow["DaysToApprove"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedBy = (Convert.IsDBNull(dataRow["ApprovedBy"]))?string.Empty:(System.String)dataRow["ApprovedBy"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder : SqlFilterBuilder<QuestionnaireReportTimeDelayAssessmentCompleteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayAssessmentCompleteFilterBuilder

	#region QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportTimeDelayAssessmentCompleteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayAssessmentCompleteParameterBuilder
	
	#region QuestionnaireReportTimeDelayAssessmentCompleteSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportTimeDelayAssessmentCompleteSortBuilder : SqlSortBuilder<QuestionnaireReportTimeDelayAssessmentCompleteColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayAssessmentCompleteSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayAssessmentCompleteSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportTimeDelayAssessmentCompleteSortBuilder

} // end namespace
