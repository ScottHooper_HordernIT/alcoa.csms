﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit16Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit16DataSourceDesigner))]
	public class TwentyOnePointAudit16DataSource : ProviderDataSource<TwentyOnePointAudit16, TwentyOnePointAudit16Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16DataSource class.
		/// </summary>
		public TwentyOnePointAudit16DataSource() : base(new TwentyOnePointAudit16Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit16DataSourceView used by the TwentyOnePointAudit16DataSource.
		/// </summary>
		protected TwentyOnePointAudit16DataSourceView TwentyOnePointAudit16View
		{
			get { return ( View as TwentyOnePointAudit16DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit16DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit16SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit16SelectMethod selectMethod = TwentyOnePointAudit16SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit16SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit16DataSourceView class that is to be
		/// used by the TwentyOnePointAudit16DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit16DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit16, TwentyOnePointAudit16Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit16DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit16DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit16DataSourceView : ProviderDataSourceView<TwentyOnePointAudit16, TwentyOnePointAudit16Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit16DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit16DataSourceView(TwentyOnePointAudit16DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit16DataSource TwentyOnePointAudit16Owner
		{
			get { return Owner as TwentyOnePointAudit16DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit16SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit16Owner.SelectMethod; }
			set { TwentyOnePointAudit16Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit16Service TwentyOnePointAudit16Provider
		{
			get { return Provider as TwentyOnePointAudit16Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit16> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit16> results = null;
			TwentyOnePointAudit16 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit16SelectMethod.Get:
					TwentyOnePointAudit16Key entityKey  = new TwentyOnePointAudit16Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit16Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit16>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit16SelectMethod.GetAll:
                    results = TwentyOnePointAudit16Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit16SelectMethod.GetPaged:
					results = TwentyOnePointAudit16Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit16SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit16Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit16Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit16SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit16Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit16>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit16SelectMethod.Get || SelectMethod == TwentyOnePointAudit16SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit16 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit16Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit16> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit16Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit16DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit16DataSource class.
	/// </summary>
	public class TwentyOnePointAudit16DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit16, TwentyOnePointAudit16Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit16DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit16SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit16DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit16DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit16DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit16DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit16DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit16DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit16DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit16DataSourceActionList(TwentyOnePointAudit16DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit16SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit16DataSourceActionList
	
	#endregion TwentyOnePointAudit16DataSourceDesigner
	
	#region TwentyOnePointAudit16SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit16DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit16SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit16SelectMethod

	#region TwentyOnePointAudit16Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit16"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit16Filter : SqlFilter<TwentyOnePointAudit16Column>
	{
	}
	
	#endregion TwentyOnePointAudit16Filter

	#region TwentyOnePointAudit16ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit16"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit16ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit16Column>
	{
	}
	
	#endregion TwentyOnePointAudit16ExpressionBuilder	

	#region TwentyOnePointAudit16Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit16ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit16"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit16Property : ChildEntityProperty<TwentyOnePointAudit16ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit16Property
}

