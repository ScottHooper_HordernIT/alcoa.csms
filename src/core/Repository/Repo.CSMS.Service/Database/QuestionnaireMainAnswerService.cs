﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainAnswerService
    {
        QuestionnaireMainAnswer Get(Expression<Func<QuestionnaireMainAnswer, bool>> where, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes);
        List<QuestionnaireMainAnswer> GetMany(int? top, Expression<Func<QuestionnaireMainAnswer, bool>> where, Func<IQueryable<QuestionnaireMainAnswer>, IOrderedQueryable<QuestionnaireMainAnswer>> orderBy, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes);
        List<QuestionnaireMainAnswer> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAnswer, bool>> where, Func<IQueryable<QuestionnaireMainAnswer>, IOrderedQueryable<QuestionnaireMainAnswer>> orderBy, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainAnswer, bool>> where, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes);
        void Insert(QuestionnaireMainAnswer item, bool saveChanges = true);
        void Update(QuestionnaireMainAnswer item, bool saveChanges = true);
        void Delete(QuestionnaireMainAnswer item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainAnswerService : IQuestionnaireMainAnswerService
    {
        private readonly IRepository<QuestionnaireMainAnswer> _repository;

        public QuestionnaireMainAnswerService(IRepository<QuestionnaireMainAnswer> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainAnswer Get(Expression<Func<QuestionnaireMainAnswer, bool>> where, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainAnswer> GetMany(int? top, Expression<Func<QuestionnaireMainAnswer, bool>> where, Func<IQueryable<QuestionnaireMainAnswer>, IOrderedQueryable<QuestionnaireMainAnswer>> orderBy, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainAnswer> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainAnswer, bool>> where, Func<IQueryable<QuestionnaireMainAnswer>, IOrderedQueryable<QuestionnaireMainAnswer>> orderBy, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainAnswer, bool>> where, List<Expression<Func<QuestionnaireMainAnswer, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainAnswer item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAnswer");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainAnswer item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAnswer");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainAnswer item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainAnswer");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
