﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.Data;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using OfficeOpenXml;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using dal = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContractorDataManagement : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        bool isAdmin;
        repo.Hr.IXxhrCwkHistoryService cwkHistorySvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrCwkHistoryService>();
        repo.Hr.IXxhrAddTrngService addTrainingSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrAddTrngService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);
            moduleInit(Page.IsPostBack);

            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.imgBtnExcelAll);
            scriptManager.RegisterPostBackControl(this.imgBtnImport);
        }

        private void moduleInit(bool postBack)
        {
            isAdmin = auth.RoleId == (int)RoleList.Administrator;

            if(!postBack)
            {
                switch (auth.RoleId)
                {
                    case (int)RoleList.CWKMaintainer:
                    case (int)RoleList.Administrator:
                        //Can access
                        break;

                    case (int)RoleList.CWKEnquiry:
                        //Can access limited
                        this.trExport.Visible = false;
                        this.ucExportButtons.Visible = false;
                        this.trImport.Visible = false;
                        this.AddNewButton.Visible = false;
                        break;

                    default:
                        Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised user")));
                        break;
                }

                var year = DateTime.Now.Year;
                for (int y = year; y > (year - 20); y--)
                {
                    this.ExportYearDropDownList.Items.Add(y.ToString());
                }
            }
            else
            {

            }
        }

        protected void PageSizeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PageSizeDropDownList.SelectedValue.ToString() != "All")
            {
                grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                grid.SettingsPager.PageSize = Convert.ToInt32(PageSizeDropDownList.SelectedValue.ToString());
            }
            else
            {
                grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            }
            grid.DataBind();
        }

        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
        {
            //if (e.NewValues["ModifiedByUserId"] == null) { e.NewValues["ModifiedByUserId"] = auth.UserId; }
            //e.NewValues["UserLogon"] = "AUSTRALIA_ASIA\\";
        }

        protected void imgBtnExcelAll_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
        {
            try
            {
                var year = this.ExportYearDropDownList.SelectedValue;

                DataSet ds = new DataSet();
                string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;
                string sql = string.Format("SELECT * FROM [HR].[XXHR_CWK_HISTORY] WHERE EFFECTIVE_START_DATE BETWEEN '{0}-01-01' AND '{0}-12-31 23:59:59';SELECT * FROM [HR].[XXHR_ADD_TRNG] WHERE COURSE_END_DATE BETWEEN '{0}-01-01' AND '{0}-12-31 23:59:59';", year);

                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    SqlCommand cmdDel = new SqlCommand(sql, cnDel);
                    cmdDel.CommandTimeout = 7000;
                    cmdDel.CommandType = CommandType.Text;
                    //cmdDel.Parameters.AddWithValue("@Year", Convert.ToInt32(cbByMonth_Year.Value));
                    //cmdDel.Parameters.AddWithValue("@Month", Convert.ToInt32(cbByMonth_Month.Value));
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmdDel);
                    sqlda.Fill(ds);
                    cnDel.Close();
                }

                if (ds.Tables.Count == 2)
                {
                    string fileName = String.Format("csms_contractor_data_{0}.xlsx", year);

                    var ms = new System.IO.MemoryStream();

                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        ExcelWorksheet ws_c = pck.Workbook.Worksheets.Add("Contractors");
                        ws_c.Cells["A1"].LoadFromDataTable(ds.Tables[0], true);

                        var cPos = new List<int>();
                        for (var i = 0; i < ds.Tables[0].Columns.Count; i++)
                            if (ds.Tables[0].Columns[i].DataType.Name.Equals("DateTime"))
                                cPos.Add(i);
                        foreach (var pos in cPos)
                        {
                            ws_c.Column(pos + 1).Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss AM/PM";
                        }

                        ExcelWorksheet ws_t = pck.Workbook.Worksheets.Add("Training");
                        ws_t.Cells["A1"].LoadFromDataTable(ds.Tables[1], true);

                        var tPos = new List<int>();
                        for (var i = 0; i < ds.Tables[1].Columns.Count; i++)
                            if (ds.Tables[1].Columns[i].DataType.Name.Equals("DateTime"))
                                tPos.Add(i);
                        foreach (var pos in tPos)
                        {
                            ws_t.Column(pos + 1).Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss AM/PM";
                        }

                        pck.SaveAs(ms);
                    }

                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));

                    ms.WriteTo(Response.OutputStream);

                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errorMsg = ex.InnerException.Message;
                }

                //Nothing yet
                //ScriptManager.RegisterClientScriptBlock(this, typeof(System.Web.UI.Page), "msg", "alert('" + ex.Message + "');", true);
                PopUpErrorMessage(errorMsg);
            }
        }

        protected void imgBtnImport_Click(object sender, EventArgs e) //import excel spreadsheet format 
        {
            try
            {
                if (this.ContractorMaintenanceFileUpload.HasFile && Path.GetExtension(this.ContractorMaintenanceFileUpload.FileName) == ".xlsx")
                {
                    using (var excel = new ExcelPackage(this.ContractorMaintenanceFileUpload.PostedFile.InputStream))
                    {
                        var dt = new DataTable();
                        var ws = excel.Workbook.Worksheets.First();
                        var hasHeader = true;  // adjust accordingly
                        // add DataColumns to DataTable
                        foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                            dt.Columns.Add(hasHeader ? firstRowCell.Value.ToString()
                                : String.Format("Column {0}", firstRowCell.Start.Column));

                        // add DataRows to DataTable
                        int startRow = hasHeader ? 2 : 1;
                        for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                        {
                            var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                            DataRow row = dt.NewRow();
                            foreach (var cell in wsRow)
                                row[cell.Start.Column - 1] = cell.Value;
                            dt.Rows.Add(row);
                        }

                        int success = 0, failed = 0;
                        foreach (DataRow dataRow in dt.Rows)
                        {
                            try
                            {
                                string EMPL_ID = dataRow["EMPL_ID"].ToString();
                                string COURSE_NAME = dataRow["COURSE_NAME"].ToString();
                                string TRAINING_TITLE = dataRow["TRAINING_TITLE"].ToString();
                                DateTime COURSE_END_DATE = DateTime.FromOADate(Convert.ToDouble(dataRow["COURSE_END_DATE"]));

                                string FULL_NAME = (dataRow["FULL_NAME"] ?? "").ToString();
                                string EXPIRATION_DATE = (dataRow["EXPIRATION_DATE"] ?? "").ToString();
                                string DURATION = (dataRow["DURATION"] ?? "").ToString();
                                string DURATION_UNITS = (dataRow["DURATION_UNITS"] ?? "").ToString();
                                string PER_TYPE_ID = (dataRow["PER_TYPE_ID"] ?? "").ToString();
                                string RECORD_TYPE = (dataRow["RECORD_TYPE"] ?? "").ToString();

                                DateTime LAST_UPDATE_DATE = DateTime.Now;

                                XXHR_ADD_TRNG training = this.addTrainingSvc.Get(i => i.EMPL_ID == EMPL_ID && i.COURSE_NAME == COURSE_NAME && i.TRAINING_TITLE == TRAINING_TITLE && i.COURSE_END_DATE == COURSE_END_DATE, null);
                                if (training != null)
                                {
                                    //Update
                                    training.FULL_NAME = FULL_NAME;
                                    training.EXPIRATION_DATE = EXPIRATION_DATE;
                                    training.DURATION = DURATION;
                                    training.DURATION_UNITS = DURATION_UNITS;
                                    training.PER_TYPE_ID = PER_TYPE_ID;
                                    training.RECORD_TYPE = RECORD_TYPE;
                                    training.LAST_UPDATE_DATE = LAST_UPDATE_DATE;

                                    this.addTrainingSvc.Update(training, false);
                                }
                                else
                                {
                                    //Insert
                                    training = new XXHR_ADD_TRNG();

                                    training.EMPL_ID = EMPL_ID;
                                    training.COURSE_NAME = COURSE_NAME;
                                    training.TRAINING_TITLE = TRAINING_TITLE;
                                    training.COURSE_END_DATE = COURSE_END_DATE;

                                    training.FULL_NAME = FULL_NAME;
                                    training.EXPIRATION_DATE = EXPIRATION_DATE;
                                    training.DURATION = DURATION;
                                    training.DURATION_UNITS = DURATION_UNITS;
                                    training.PER_TYPE_ID = PER_TYPE_ID;
                                    training.RECORD_TYPE = RECORD_TYPE;
                                    training.LAST_UPDATE_DATE = LAST_UPDATE_DATE;

                                    this.addTrainingSvc.Insert(training, false);
                                }

                                success += 1;
                            }
                            catch
                            {
                                //Skip any rows that are invalid
                                failed += 1;
                            }
                        }

                        this.addTrainingSvc.SaveChanges();
                        
                        var msg = String.Format("Successfully read from excel-file. Column-count: {0} Row-count: {1}<br/>", dt.Columns.Count, dt.Rows.Count);
                        msg += String.Format("Successfully saved {0} rows to database, {1} failed.", success, failed);

                        //ScriptManager.RegisterClientScriptBlock(this, typeof(System.Web.UI.Page), "msg", "alert('" + msg + "');", true);
                        PopUpErrorMessage(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                if (ex.InnerException != null)
                {
                    errorMsg = ex.InnerException.Message;
                }
                //Nothing yet
                //ScriptManager.RegisterClientScriptBlock(this, typeof(System.Web.UI.Page), "msg", "alert('" + errorMsg + "');", true);
                PopUpErrorMessage(errorMsg);
            }
        }

        protected void PopUpErrorMessage(string errormsg)
        {
            //ASPxGridView1.Visible = false;
            PopupWindow pcWindow = new PopupWindow(errormsg);
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
            ASPxPopupControl1.Windows.Add(pcWindow);
            //WebChartControl1.Visible = false;
        }
    }
}
