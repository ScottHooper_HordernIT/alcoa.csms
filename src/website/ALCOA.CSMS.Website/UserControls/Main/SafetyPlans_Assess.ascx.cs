using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using KaiZen.Library;
using System.Drawing;
using System.Text;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;

public partial class UserControls_SafetyPlans_Assess : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    repo.IEmailTemplateService emailTemplateService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailTemplateService>();
    repo.IEmailLogService emailLogService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEmailLogService>();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Contractor)
            {
                btnSave.Text = "Close";
                btnSave2.Text = "Close";
            }
            bool isEhsAdmin = false;
            //Check if User is an EHS Consultant
            int count;
            EhsConsultantFilters queryEhsConsultant = new EhsConsultantFilters();
            queryEhsConsultant.Append(EhsConsultantColumn.UserId, auth.UserId.ToString());
            TList<EhsConsultant> EhsConsultantList = DataRepository.EhsConsultantProvider.GetPaged(queryEhsConsultant.ToString(), null, 0, 100, out count);
            if (count > 0) { isEhsAdmin = true; };

            bool go = false;
            if(isEhsAdmin) go = true;
            if (auth.RoleId == (int)RoleList.Administrator) go = true;

            if(go == false)
            {
                btnSave.Text = "Close";
                btnSave.ClientSideEvents.Click = "function(s, e) { needToConfirm = false; }";
                cb1.Enabled = false;
                cb2.Enabled = false;
                cb3.Enabled = false;
                cb4.Enabled = false;
                cb5.Enabled = false;
                cb6.Enabled = false;
                cb7.Enabled = false;
                cb8.Enabled = false;
                cb9.Enabled = false;
                cb10.Enabled = false;
                memoAssessorComments1.ReadOnly = true;
                memoAssessorComments2.ReadOnly = true;
                memoAssessorComments3.ReadOnly = true;
                memoAssessorComments4.ReadOnly = true;
                memoAssessorComments5.ReadOnly = true;
                memoAssessorComments6.ReadOnly = true;
                memoAssessorComments7.ReadOnly = true;
                memoAssessorComments8.ReadOnly = true;
                memoAssessorComments9.ReadOnly = true;
                memoAssessorComments10.ReadOnly = true;

                htmlAssessorComments.Settings.AllowHtmlView = false;
                htmlAssessorComments.Settings.AllowDesignView = false;
                htmlAssessorComments.Settings.AllowPreview = true;
                //htmlAssessorComments.Html = false;
            }

            int r = 0;

            LoadQ();

            r = Convert.ToInt32(Request.QueryString["R"]);
            SessionHandler.spVar_SafetyPlans_ResponseId = r.ToString();
            LoadA(r);
        }
    }

    protected void LoadQ()
    {
        SafetyPlansSeQuestionsService seq = new SafetyPlansSeQuestionsService();
        TList<SafetyPlansSeQuestions> seqList = seq.GetAll();
        foreach (SafetyPlansSeQuestions s in seqList)
        {
            switch (s.QuestionId)
            {
                case 1:
                    lblQuestion1.Text = s.Question;
                    break;
                case 2:
                    lblQuestion2.Text = s.Question;
                    break;
                case 3:
                    lblQuestion3.Text = s.Question;
                    break;
                case 4:
                    lblQuestion4.Text = s.Question;
                    break;
                case 5:
                    lblQuestion5.Text = s.Question;
                    break;
                case 6:
                    lblQuestion6.Text = s.Question;
                    break;
                case 7:
                    lblQuestion7.Text = s.Question;
                    break;
                case 8:
                    lblQuestion8.Text = s.Question;
                    break;
                case 9:
                    lblQuestion9.Text = s.Question;
                    break;
                case 10:
                    lblQuestion10.Text = s.Question;
                    break;

                default:
                    break;
            }
        }
    }
    protected void LoadA(int r)
    {
        string sComment = "";
        string a = "";
        SafetyPlansSeAnswersService sea = new SafetyPlansSeAnswersService();
        TList<SafetyPlansSeAnswers> seaList = sea.GetByResponseId(r);
        

        foreach (SafetyPlansSeAnswers s in seaList)
        {
            switch (s.QuestionId)
            {
              
                case 1:
                    if (s.Answer == true) { lblAnswer1.Text = "Yes"; lblAnswer1.ForeColor = Color.Green; }
                    else { lblAnswer1.Text = "No"; lblAnswer1.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments1.Text = sComment;
                    }
                    else
                    {
                        lblComments1.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments1.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb1.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb1.Text = "Approved";
                        if (s.AssessorApproval == false) cb1.Text = "Not Approved";
                    }

                    break;
                case 2:
                    if (s.Answer == true) { lblAnswer2.Text = "Yes"; lblAnswer2.ForeColor = Color.Green; }
                    else { lblAnswer2.Text = "No"; lblAnswer2.ForeColor = Color.Red; }


                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments2.Text = sComment;
                    }
                    else
                    {
                        lblComments2.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments2.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb2.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb2.Text = "Approved";
                        if (s.AssessorApproval == false) cb2.Text = "Not Approved";
                    }

                    break;
                case 3:
                    if (s.Answer == true) { lblAnswer3.Text = "Yes"; lblAnswer3.ForeColor = Color.Green; }
                    else { lblAnswer3.Text = "No"; lblAnswer3.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments3.Text = sComment;
                    }
                    else
                    {
                        lblComments3.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments3.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb3.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb3.Text = "Approved";
                        if (s.AssessorApproval == false) cb3.Text = "Not Approved";
                    }

                    break;
                case 4:
                    if (s.Answer == true) { lblAnswer4.Text = "Yes"; lblAnswer4.ForeColor = Color.Green; }
                    else { lblAnswer4.Text = "No"; lblAnswer4.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments4.Text = sComment;
                    }
                    else
                    {
                        lblComments4.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments4.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb4.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb4.Text = "Approved";
                        if (s.AssessorApproval == false) cb4.Text = "Not Approved";
                    }

                    break;
                case 5:
                    if (s.Answer == true) { lblAnswer5.Text = "Yes"; lblAnswer5.ForeColor = Color.Green; }
                    else { lblAnswer5.Text = "No"; lblAnswer5.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments5.Text = sComment;
                    }
                    else
                    {
                        lblComments5.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments5.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb5.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb5.Text = "Approved";
                        if (s.AssessorApproval == false) cb5.Text = "Not Approved";
                    }

                    break;
                case 6:
                    if (s.Answer == true) { lblAnswer6.Text = "Yes"; lblAnswer6.ForeColor = Color.Green; }
                    else { lblAnswer6.Text = "No"; lblAnswer6.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments6.Text = sComment;
                    }
                    else
                    {
                        lblComments6.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments6.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb6.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb6.Text = "Approved";
                        if (s.AssessorApproval == false) cb6.Text = "Not Approved";
                    }

                    break;
                case 7:
                    if (s.Answer == true) { lblAnswer7.Text = "Yes"; lblAnswer7.ForeColor = Color.Green; }
                    else { lblAnswer7.Text = "No"; lblAnswer7.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments7.Text = sComment;
                    }
                    else
                    {
                        lblComments7.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments7.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb7.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb7.Text = "Approved";
                        if (s.AssessorApproval == false) cb7.Text = "Not Approved";
                    }

                    break;
                case 8:
                    if (s.Answer == true) { lblAnswer8.Text = "Yes"; lblAnswer8.ForeColor = Color.Green; }
                    else { lblAnswer8.Text = "No"; lblAnswer8.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments8.Text = sComment;
                    }
                    else
                    {
                        lblComments8.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments8.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb8.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb8.Text = "Approved";
                        if (s.AssessorApproval == false) cb8.Text = "Not Approved";
                    }

                    break;
                case 9:
                    if (s.Answer == true) { lblAnswer9.Text = "Yes"; lblAnswer9.ForeColor = Color.Green; }
                    else { lblAnswer9.Text = "No"; lblAnswer9.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments9.Text = sComment;
                    }
                    else
                    {
                        lblComments9.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments9.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }

                    cb9.Text = "";
                    if (s.AssessorApproval != null)
                    {
                        if (s.AssessorApproval == true) cb9.Text = "Approved";
                        if (s.AssessorApproval == false) cb9.Text = "Not Approved";
                    }

                    break;
                case 10:
                    if (s.Answer == true) { lblAnswer10.Text = "Yes"; lblAnswer10.ForeColor = Color.Green; }
                    else { lblAnswer10.Text = "No"; lblAnswer10.ForeColor = Color.Red; }

                    if (s.Comment != null) sComment = Encoding.ASCII.GetString(s.Comment);
                    if (!String.IsNullOrEmpty(sComment))
                    {
                        lblComments10.Text = sComment;
                    }
                    else
                    {
                        lblComments10.Text = "None.";
                    }

                    if (s.AssessorComment != null)
                    {
                        memoAssessorComments10.Text = Encoding.ASCII.GetString(s.AssessorComment);
                    }


                    if (s.AssessorApproval == true) a = "Approved";
                    if (s.AssessorApproval == false) a = "Not Approved";
                    if (s.AssessorApproval == null) a = "";

                    cb10.Text = a;
                    a = "";

                    break;
                default:
                    break;
            }
        }
        SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();
        SafetyPlansSeResponses ser = sers.GetByResponseId(r);

        if (ser.AsessorComment != null)
        {
            htmlAssessorComments.Html = Encoding.ASCII.GetString(ser.AsessorComment);
        }
    }


    /// <summary>
    /// SendEmail By Bishwajit Sahoo
    /// </summary>
    /// <param name="r"></param>
    protected bool SendEmail(int r)
    {
        bool isSent = false;
        try
        {
            SafetyPlansSeResponsesService ser = new SafetyPlansSeResponsesService();
            SafetyPlansSeResponses serList = ser.GetByResponseId(r);
            int modifiedUserId = serList.ModifiedByUserId;
            UsersService users = new UsersService();
            Users user = users.GetByUserId(modifiedUserId);
            string[] ToEmailId = { user.Email };
            string ToMailName = user.FirstName + " " + user.LastName;
            string[] ccEmaild = { auth.Email };
            user.Dispose();
            user = users.GetByUserId(auth.UserId);
            string phoneNumber = user.PhoneNo;
            string ccEmailName = user.FirstName + " " + user.LastName;
            int companyId = serList.CompanyId;
            CompaniesService compServies = new CompaniesService();
            Companies company = compServies.GetByCompanyId(companyId);
            string companyName = company.CompanyName;

            model.EmailTemplate atemt = emailTemplateService.Get(i => i.EmailTemplateName == "Safety Management Plan assessed as INCOMPLETE", null);
            string subject = atemt.EmailTemplateSubject;
            subject = subject.Replace("{CompanyName}", companyName);
            subject = subject.Replace("{SafetyAssessorName}", ccEmailName);
            subject = subject.Replace("{SafetyAssessorEmail}", ccEmaild[0]);
            subject = subject.Replace("{SafetyAssessorPhoneNumber}", phoneNumber);

            //string subject = "Safety Management Plan assessed as INCOMPLETE for " + companyName;
            byte[] bodyByte = atemt.EmailBody;
            string mailbody = System.Text.Encoding.ASCII.GetString(bodyByte);

            mailbody = mailbody.Replace("{SafetyAssessorName}", ccEmailName);
            mailbody = mailbody.Replace("{SafetyAssessorEmail}", ccEmaild[0]);
            mailbody = mailbody.Replace("{SafetyAssessorPhoneNumber}", phoneNumber);
            mailbody = mailbody.Replace("{CompanyName}", companyName);
            

            /*string mailbody = "Your Safety Management Plan has been assessed and found to be incomplete.\n\n" +
                            "You may now go back into the Alcoa Contractor Services Management System and alter/supply the \n" +
                            "relevant information.\n\n" +
                            "Your H&S Assessor is " + ccEmailName + " and is contactable via e-mail " + ccEmaild[0] +
                            " or PHONE " + phoneNumber;
            */
            emailLogService.Insert(ToEmailId, ccEmaild, null, subject, mailbody, "SafetyPlans", true, null, null);
            //Helper.Email.sendEmail(ToEmailId, ccEmaild, null, subject, mailbody, null);
            //emailLogService.Insert();

            //Configuration configuration = new Configuration();

            ////To - AUACSMS Team To Removed 9.Mar.11, Replaced with Default EHS Consultant
            //ConfigService _cService = new ConfigService();
            //Config _c = _cService.GetByKey("DefaultEhsConsultant_WAO");
            //if (!String.IsNullOrEmpty(_c.Value))
            //{
            //    string[] defaultehs = { _c.Value };
            //    Helper.Email.sendEmail(defaultehs, ccEmaild, null, subject, mailbody, null);

            //}
            //else
            //{
            //    string[] csmteam = { configuration.GetValue(ConfigList.ContactEmail) };
            //    Helper.Email.sendEmail(csmteam, ccEmaild, null, subject, mailbody, null);
            //}
            isSent = true;

        }
        catch (Exception ex)
        {

            isSent = false;
        }
        return isSent;


    }

    protected void Save(int r)
    {
        try
        {
            SafetyPlansSeResponsesService Sers = new SafetyPlansSeResponsesService();
            SafetyPlansSeResponses Ser = Sers.GetByResponseId(r);
            //Ser.CompanyId
          //auth.FirstName
            //auth.LastName
           // auth.RoleId
            ASCIIEncoding encoding = new ASCIIEncoding();
            if (CompareUtilities.Strings(Ser.AsessorComment, htmlAssessorComments.Html)) Ser.AsessorComment = (byte[])encoding.GetBytes(htmlAssessorComments.Html);

            if(Ser.EntityState != EntityState.Unchanged) Sers.Save(Ser);
            SafetyPlansSeAnswersService Seas = new SafetyPlansSeAnswersService();
            SafetyPlansSeAnswers Sea1 = Seas.GetByQuestionIdResponseId(1, r);
            SafetyPlansSeAnswers Sea2 = Seas.GetByQuestionIdResponseId(2, r);
            SafetyPlansSeAnswers Sea3 = Seas.GetByQuestionIdResponseId(3, r);
            SafetyPlansSeAnswers Sea4 = Seas.GetByQuestionIdResponseId(4, r);
            SafetyPlansSeAnswers Sea5 = Seas.GetByQuestionIdResponseId(5, r);
            SafetyPlansSeAnswers Sea6 = Seas.GetByQuestionIdResponseId(6, r);
            SafetyPlansSeAnswers Sea7 = Seas.GetByQuestionIdResponseId(7, r);
            SafetyPlansSeAnswers Sea8 = Seas.GetByQuestionIdResponseId(8, r);
            SafetyPlansSeAnswers Sea9 = Seas.GetByQuestionIdResponseId(9, r);
            SafetyPlansSeAnswers Sea10 = Seas.GetByQuestionIdResponseId(10, r);

            if (CompareUtilities.Strings(Sea1.AssessorComment, memoAssessorComments1.Text)) Sea1.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments1.Text);
            if (CompareUtilities.Strings(Sea2.AssessorComment, memoAssessorComments2.Text)) Sea2.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments2.Text);
            if (CompareUtilities.Strings(Sea3.AssessorComment, memoAssessorComments3.Text)) Sea3.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments3.Text);
            if (CompareUtilities.Strings(Sea4.AssessorComment, memoAssessorComments4.Text)) Sea4.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments4.Text);
            if (CompareUtilities.Strings(Sea5.AssessorComment, memoAssessorComments5.Text)) Sea5.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments5.Text);
            if (CompareUtilities.Strings(Sea6.AssessorComment, memoAssessorComments6.Text)) Sea6.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments6.Text);
            if (CompareUtilities.Strings(Sea7.AssessorComment, memoAssessorComments7.Text)) Sea7.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments7.Text);
            if (CompareUtilities.Strings(Sea8.AssessorComment, memoAssessorComments8.Text)) Sea8.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments8.Text);
            if (CompareUtilities.Strings(Sea9.AssessorComment, memoAssessorComments9.Text)) Sea9.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments9.Text);
            if (CompareUtilities.Strings(Sea10.AssessorComment, memoAssessorComments10.Text)) Sea10.AssessorComment = (byte[])encoding.GetBytes(memoAssessorComments10.Text);

            int count0 = 0;
            int count1 = 0;
            if (cb1.SelectedItem != null)
            {
                if (cb1.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea1.AssessorApproval != false) Sea1.AssessorApproval = false;
                    count0++;
                }
                if (cb1.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea1.AssessorApproval != true) Sea1.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb2.SelectedItem != null)
            {
                if (cb2.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea2.AssessorApproval != false) Sea2.AssessorApproval = false;
                    count0++;
                }
                if (cb2.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea2.AssessorApproval != true) Sea2.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb3.SelectedItem != null)
            {
                if (cb3.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea3.AssessorApproval != false) Sea3.AssessorApproval = false;
                    count0++;
                }
                if (cb3.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea3.AssessorApproval != true) Sea3.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb4.SelectedItem != null)
            {
                if (cb4.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea4.AssessorApproval != false) Sea4.AssessorApproval = false;
                    count0++;
                }
                if (cb4.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea4.AssessorApproval != true) Sea4.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb5.SelectedItem != null)
            {
                if (cb5.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea5.AssessorApproval != false) Sea5.AssessorApproval = false;
                    count0++;
                }
                if (cb5.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea5.AssessorApproval != true) Sea5.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb6.SelectedItem != null)
            {
                if (cb6.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea6.AssessorApproval != false) Sea6.AssessorApproval = false;
                    count0++;
                }
                if (cb6.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea6.AssessorApproval != true) Sea6.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb7.SelectedItem != null)
            {
                if (cb7.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea7.AssessorApproval != false) Sea7.AssessorApproval = false;
                    count0++;
                }
                if (cb7.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea7.AssessorApproval != true) Sea7.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb8.SelectedItem != null)
            {
                if (cb8.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea8.AssessorApproval != false) Sea8.AssessorApproval = false;
                    count0++;
                }
                if (cb8.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea8.AssessorApproval != true) Sea8.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb9.SelectedItem != null)
            {
                if (cb9.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea9.AssessorApproval != false) Sea9.AssessorApproval = false;
                    count0++;
                }
                if (cb9.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea9.AssessorApproval != true) Sea9.AssessorApproval = true;
                    count1++;
                }
            }
            if (cb10.SelectedItem != null)
            {
                if (cb10.SelectedItem.Value.ToString() == "0")
                {
                    if (Sea10.AssessorApproval != false) Sea10.AssessorApproval = false;
                    count0++;
                }
                if (cb10.SelectedItem.Value.ToString() == "1")
                {
                    if (Sea10.AssessorApproval != true) Sea10.AssessorApproval = true;
                    count1++;
                }
            }
            int FileId = -1;
            FileDbService fdbs = new FileDbService();
            bool isSent = false;
            DataSet dbsf = fdbs.GetFileIdByResponseId(r);
            foreach (DataRow drow in dbsf.Tables[0].Rows)
            {
                FileId = Convert.ToInt32(drow[0].ToString());
            }
            //

            string accessor = auth.Email;


            int count = count0 + count1;
            
            if (count == 10)
            {
                FileDb f = fdbs.GetByFileId(FileId);
                if (count0 >= 1)
                {
                    //DT307 21/10/15 Cindi Thornton - only send an email if the status was previously not approved, as if it is already not approved, they've already been sent an email.
                    //Previously, an email was sent irrespective of the current status
                    if (f.StatusId != (int)SafetyPlanStatusList.Not_Approved)
                    {
                        f.StatusId = (int)SafetyPlanStatusList.Not_Approved;
                        isSent = SendEmail(r);
                    }
                }
                else
                {
                    if (f.StatusId != (int)SafetyPlanStatusList.Approved) f.StatusId = (int)SafetyPlanStatusList.Approved;
                }
                if (f.EntityState != EntityState.Unchanged)
                {
                    f.StatusModifiedByUserId = auth.UserId;
                    f.StatusModifiedDate = DateTime.Now;
                    fdbs.Save(f);
                }              
                f.Dispose();
            }
            else
            {
                if (count >= 1)
                {
                    FileDb f = fdbs.GetByFileId(FileId);
                    if (f.StatusId != (int)SafetyPlanStatusList.Being_Assessed) f.StatusId = (int)SafetyPlanStatusList.Being_Assessed;
                    if (f.EntityState != EntityState.Unchanged)
                    {
                        f.StatusModifiedByUserId = auth.UserId;
                        f.StatusModifiedDate = DateTime.Now;
                        fdbs.Save(f);
                        //isSent=SendEmail(r); DT307 21/10/15 Cindi Thornton - we only want to send the INCOMPLETE email once all questions have been reviewed.
                    }
                    f.Dispose();
                }
            }

            if(Sea1.EntityState != EntityState.Unchanged) Seas.Save(Sea1);
            if (Sea2.EntityState != EntityState.Unchanged) Seas.Save(Sea2);
            if (Sea3.EntityState != EntityState.Unchanged) Seas.Save(Sea3);
            if (Sea4.EntityState != EntityState.Unchanged) Seas.Save(Sea4);
            if (Sea5.EntityState != EntityState.Unchanged) Seas.Save(Sea5);
            if (Sea6.EntityState != EntityState.Unchanged) Seas.Save(Sea6);
            if (Sea7.EntityState != EntityState.Unchanged) Seas.Save(Sea7);
            if (Sea8.EntityState != EntityState.Unchanged) Seas.Save(Sea8);
            if (Sea9.EntityState != EntityState.Unchanged) Seas.Save(Sea9);
            if (Sea10.EntityState != EntityState.Unchanged) Seas.Save(Sea10);

            Sea1.Dispose();
            Sea2.Dispose();
            Sea3.Dispose();
            Sea4.Dispose();
            Sea5.Dispose();
            Sea5.Dispose();
            Sea6.Dispose();
            Sea7.Dispose();
            Sea8.Dispose();
            Sea9.Dispose();
            Sea10.Dispose();

            Label1.Text = "Save Successful.";
            Response.Redirect("~/SafetyPlans_SelfAssessment.aspx?R=" + Request.QueryString["R"]);
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Label1.ForeColor = Color.Red;
            Label1.Text = "Save Failed: " + ex.Message;
        }

    }
    protected void btnHome_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        Response.Redirect("~/SafetyPlans_SelfAssessment.aspx?R=" + Request.QueryString["R"],true);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Helper.General.isEhsConsultant(auth.UserId) || (auth.RoleId == (int)RoleList.Administrator))
        {
            Save(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId));
        }
        else
        {
            Response.Redirect("~/SafetyPlans_SelfAssessment.aspx?R=" + Request.QueryString["R"], true);
        }
        
    }
}