﻿
/*
	File generated by NetTiers templates [www.NetTiers.com]
	Important: Do not modify this file. Edit the file QuestionnairePresentlyWithUsersActions.cs instead.
*/

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Security;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;
using Entities = KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;


using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion 

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An object representation of the 'QuestionnairePresentlyWithUsersActions' View.
	///</summary>
	/// <remarks>
	/// IMPORTANT!!! You should not modify this partial  class, modify the QuestionnairePresentlyWithUsersActions.cs file instead.
	/// All custom implementations should be done in the <see cref="QuestionnairePresentlyWithUsersActions"/> class.
	/// </remarks>
	[DataObject]
	public partial class QuestionnairePresentlyWithUsersActionsServiceBase : ServiceViewBase<QuestionnairePresentlyWithUsersActions>
	{

		#region Constructors
		///<summary>
		/// Creates a new <see cref="QuestionnairePresentlyWithUsersActions"/> instance .
		///</summary>
		public QuestionnairePresentlyWithUsersActionsServiceBase() : base()
		{
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="QuestionnairePresentlyWithUsersActions"/> instance.
		///</summary>
		///<param name="_questionnairePresentlyWithActionId"></param>
		///<param name="_questionnairePresentlyWithUserId"></param>
		///<param name="_processNo"></param>
		///<param name="_userName"></param>
		///<param name="_actionName"></param>
		///<param name="_userDescription"></param>
		///<param name="_actionDescription"></param>
		public static QuestionnairePresentlyWithUsersActions CreateQuestionnairePresentlyWithUsersActions(System.Int32 _questionnairePresentlyWithActionId, System.Int32 _questionnairePresentlyWithUserId, System.Int32? _processNo, System.String _userName, System.String _actionName, System.String _userDescription, System.String _actionDescription)
		{
			QuestionnairePresentlyWithUsersActions newEntityQuestionnairePresentlyWithUsersActions = new QuestionnairePresentlyWithUsersActions();
			newEntityQuestionnairePresentlyWithUsersActions.QuestionnairePresentlyWithActionId  = _questionnairePresentlyWithActionId;
			newEntityQuestionnairePresentlyWithUsersActions.QuestionnairePresentlyWithUserId  = _questionnairePresentlyWithUserId;
			newEntityQuestionnairePresentlyWithUsersActions.ProcessNo  = _processNo;
			newEntityQuestionnairePresentlyWithUsersActions.UserName  = _userName;
			newEntityQuestionnairePresentlyWithUsersActions.ActionName  = _actionName;
			newEntityQuestionnairePresentlyWithUsersActions.UserDescription  = _userDescription;
			newEntityQuestionnairePresentlyWithUsersActions.ActionDescription  = _actionDescription;
			return newEntityQuestionnairePresentlyWithUsersActions;
		}
		#endregion Constructors

		#region Fields
		//private static SecurityContext<QuestionnairePresentlyWithUsersActions> securityContext = new SecurityContext<QuestionnairePresentlyWithUsersActions>();
		private static readonly string layerExceptionPolicy = "ServiceLayerExceptionPolicy";
		private static readonly bool noTranByDefault = false;
		private static readonly int defaultMaxRecords = 10000;
		#endregion 
		
		#region Data Access Methods
			
		#region Get 
		/// <summary>
		/// Attempts to do a parameterized version of a simple whereclause. 
		/// Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
        /// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <remarks>Does NOT Support Advanced Operations such as SubSelects.  See GetPaged for that functionality.</remarks>
		/// <returns>Returns a typed collection of Entity objects.</returns>
		public override VList<QuestionnairePresentlyWithUsersActions> Get(string whereClause, string orderBy)
		{
			int totalCount = -1;
			return Get(whereClause, orderBy, 0, defaultMaxRecords, out totalCount);
		}

		/// <summary>
		/// Returns rows meeting the whereClause condition from the DataSource.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
        /// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">out parameter to get total records for query</param>
		/// <remarks>Does NOT Support Advanced Operations such as SubSelects.  See GetPaged for that functionality.</remarks>
		/// <returns>Returns a typed collection TList{QuestionnairePresentlyWithUsersActions} of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public override VList<QuestionnairePresentlyWithUsersActions> Get(string whereClause, string orderBy, int start, int pageLength, out int totalCount)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("Get");
								
			// get this data
			VList<QuestionnairePresentlyWithUsersActions> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.Get(transactionManager, whereClause, orderBy, start, pageLength, out totalCount);
				
				//if borrowed tran, leave open for next call
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;
		}
		
		#endregion Get Methods
		
		#region GetAll
		/// <summary>
		/// Get a complete collection of <see cref="QuestionnairePresentlyWithUsersActions" /> entities.
		/// </summary>
		/// <returns></returns>
		public virtual VList<QuestionnairePresentlyWithUsersActions> GetAll() 
		{
			int totalCount = -1;
			return GetAll(0, defaultMaxRecords, out totalCount);
		}

       
		/// <summary>
		/// Get a set portion of a complete list of <see cref="QuestionnairePresentlyWithUsersActions" /> entities
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">out parameter, number of total rows in given query.</param>
		/// <returns>a <see cref="TList{QuestionnairePresentlyWithUsersActions}"/> </returns>
		public override VList<QuestionnairePresentlyWithUsersActions> GetAll(int start, int pageLength, out int totalCount) 
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetAll");
			
			// get this data
			VList<QuestionnairePresentlyWithUsersActions> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;					

				//Access repository
				list = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.GetAll(transactionManager, start, pageLength, out totalCount);	
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;
		}
		#endregion GetAll

		#region GetPaged
		/// <summary>
		/// Gets a page of <see cref="TList{QuestionnairePresentlyWithUsersActions}" /> rows from the DataSource.
		/// </summary>
		/// <param name="totalCount">Out Parameter, Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public virtual VList<QuestionnairePresentlyWithUsersActions> GetPaged(out int totalCount)
		{
			return GetPaged(null, null, 0, defaultMaxRecords, out totalCount);
		}
		
		/// <summary>
		/// Gets a page of <see cref="TList{QuestionnairePresentlyWithUsersActions}" /> rows from the DataSource.
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public virtual VList<QuestionnairePresentlyWithUsersActions> GetPaged(int start, int pageLength, out int totalCount)
		{
			return GetPaged(null, null, start, pageLength, out totalCount);
		}

		/// <summary>
		/// Gets a page of entity rows with a <see cref="TList{QuestionnairePresentlyWithUsersActions}" /> from the DataSource with a where clause and order by clause.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC).</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="totalCount">Out Parameter, Number of rows in the DataSource.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public override VList<QuestionnairePresentlyWithUsersActions> GetPaged(string whereClause,string orderBy, int start, int pageLength, out int totalCount)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("GetPaged");
			
			// get this data
			VList<QuestionnairePresentlyWithUsersActions> list = null;
			totalCount = -1;
			TransactionManager transactionManager = null; 

			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.GetPaged(transactionManager, whereClause, orderBy, start, pageLength, out totalCount);
				
				//if borrowed tran, leave open for next call
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			return list;			
		}
		
		/// <summary>
		/// Gets the number of rows in the DataSource that match the specified whereClause.
		/// This method is only provided as a workaround for the ObjectDataSource's need to 
		/// execute another method to discover the total count instead of using another param, like our out param.  
		/// This method should be avoided if using the ObjectDataSource or another method.
		/// </summary>
		/// <param name="whereClause">Specifies the condition for the rows returned by a query (Name='John Doe', Name='John Doe' AND Id='1', Name='John Doe' OR Id='1').</param>
		/// <param name="totalCount">Number of rows in the DataSource.</param>
		/// <returns>Returns the number of rows.</returns>
		public int GetTotalItems(string whereClause, out int totalCount)
		{
			GetPaged(whereClause, null, 0, defaultMaxRecords, out totalCount);
			return totalCount;
		}
		#endregion GetPaged	

		#region Find Methods

		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <returns>Returns a typed collection of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public virtual VList<QuestionnairePresentlyWithUsersActions> Find(IFilterParameterCollection parameters)
		{
			return Find(parameters, null);
		}
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <returns>Returns a typed collection of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public virtual VList<QuestionnairePresentlyWithUsersActions> Find(IFilterParameterCollection parameters, string orderBy)
		{
			int count = 0;
			return Find(parameters, orderBy, 0, defaultMaxRecords, out count);
		}
		
		/// <summary>
		/// 	Returns rows from the DataSource that meet the parameter conditions.
		/// </summary>
		/// <param name="parameters">A collection of <see cref="SqlFilterParameter"/> objects.</param>
		/// <param name="orderBy">Specifies the sort criteria for the rows in the DataSource (Name ASC; BirthDay DESC, Name ASC);</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out. The number of rows that match this query.</param>
		/// <returns>Returns a typed collection of <c>QuestionnairePresentlyWithUsersActions</c> objects.</returns>
		public override VList<QuestionnairePresentlyWithUsersActions> Find(IFilterParameterCollection parameters, string orderBy, int start, int pageLength, out int count)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("Find");
								
			// get this data
			TransactionManager transactionManager = null; 
			VList<QuestionnairePresentlyWithUsersActions> list = null;
			count = -1;
			
			try
            {	
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
					
				//Access repository
				list = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.Find(transactionManager, parameters, orderBy, start, pageLength, out count);
			}
            catch (Exception exc)
            {
				//if open, rollback, it's possible this is part of a larger commit
                if (transactionManager != null && transactionManager.IsOpen) 
					transactionManager.Rollback();
				
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
			}
			
			return list;
		}
		
		#endregion Find Methods
		
		#region Custom Methods
		
		#region _QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId' stored procedure. 
		/// </summary>
		/// <param name="actionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet OrderByProcessNo_ByActionId(System.Int32? actionId)
		{
			return OrderByProcessNo_ByActionId( actionId, 0, defaultMaxRecords );
		}
	
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId' stored procedure. 
		/// </summary>
		/// <param name="actionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet OrderByProcessNo_ByActionId(System.Int32? actionId, int start, int pageLength)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("OrderByProcessNo_ByActionId");
			
		
			DataSet result = null; 
			TransactionManager transactionManager = null; 
			
			try
            {
				bool isBorrowedTransaction = ConnectionScope.Current.HasTransaction;				
				
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
                
				//Call Custom Procedure from Repository
				result = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.OrderByProcessNo_ByActionId(transactionManager, start, pageLength , actionId);
	        
            	
			}
            catch (Exception exc)
            {
				//if open, rollback
                if (transactionManager != null && transactionManager.IsOpen)
                        transactionManager.Rollback();
                    
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
            }
			
			return result;
		}
		#endregion 
		
		#region _QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet OrderByProcessNo_ByUserId(System.Int32? userId)
		{
			return OrderByProcessNo_ByUserId( userId, 0, defaultMaxRecords );
		}
	
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet OrderByProcessNo_ByUserId(System.Int32? userId, int start, int pageLength)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("OrderByProcessNo_ByUserId");
			
		
			DataSet result = null; 
			TransactionManager transactionManager = null; 
			
			try
            {
				bool isBorrowedTransaction = ConnectionScope.Current.HasTransaction;				
				
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
                
				//Call Custom Procedure from Repository
				result = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.OrderByProcessNo_ByUserId(transactionManager, start, pageLength , userId);
	        
            	
			}
            catch (Exception exc)
            {
				//if open, rollback
                if (transactionManager != null && transactionManager.IsOpen)
                        transactionManager.Rollback();
                    
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
            }
			
			return result;
		}
		#endregion 
		
		#region _QuestionnairePresentlyWithUsersActions_OrderByProcessNo
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet OrderByProcessNo()
		{
			return OrderByProcessNo( 0, defaultMaxRecords );
		}
	
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public virtual DataSet OrderByProcessNo(int start, int pageLength)
		{
			// throws security exception if not authorized
			//SecurityContext.IsAuthorized("OrderByProcessNo");
			
		
			DataSet result = null; 
			TransactionManager transactionManager = null; 
			
			try
            {
				bool isBorrowedTransaction = ConnectionScope.Current.HasTransaction;				
				
				//since this is a read operation, don't create a tran by default, only use tran if provided to us for custom isolation level
				transactionManager = ConnectionScope.ValidateOrCreateTransaction(noTranByDefault);
				NetTiersProvider dataProvider = ConnectionScope.Current.DataProvider;
                
				//Call Custom Procedure from Repository
				result = dataProvider.QuestionnairePresentlyWithUsersActionsProvider.OrderByProcessNo(transactionManager, start, pageLength );
	        
            	
			}
            catch (Exception exc)
            {
				//if open, rollback
                if (transactionManager != null && transactionManager.IsOpen)
                        transactionManager.Rollback();
                    
				//Handle exception based on policy
                if (DomainUtil.HandleException(exc, layerExceptionPolicy)) 
					throw;
            }
			
			return result;
		}
		#endregion 
		#endregion
		
		#endregion Data Access Methods
		
	
	}//End Class
} // end namespace



