﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;
using Repo.CSMS.Service.Model;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanySiteCategoryStandardHistoryService
    {
        CompanySiteCategoryStandardHistory Get(Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes);
        List<CompanySiteCategoryStandardHistory> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, Func<IQueryable<CompanySiteCategoryStandardHistory>, IOrderedQueryable<CompanySiteCategoryStandardHistory>> orderBy, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes);
        List<CompanySiteCategoryStandardHistory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, Func<IQueryable<CompanySiteCategoryStandardHistory>, IOrderedQueryable<CompanySiteCategoryStandardHistory>> orderBy, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes);
        List<GroupCount> GetGroupCount(Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, Func<CompanySiteCategoryStandardHistory, object> selector);
        void Insert(CompanySiteCategoryStandardHistory item, bool saveChanges = true);
        void Update(CompanySiteCategoryStandardHistory item, bool saveChanges = true);
        void Delete(CompanySiteCategoryStandardHistory item, bool saveChanges = true);
        void SaveOrUpdate(CompanySiteCategoryStandardHistory item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryStandardHistoryService : ICompanySiteCategoryStandardHistoryService
    {
        private readonly IRepository<CompanySiteCategoryStandardHistory> _repository;

        public CompanySiteCategoryStandardHistoryService(IRepository<CompanySiteCategoryStandardHistory> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryStandardHistory Get(Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryStandardHistory> GetMany(int? top, Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, Func<IQueryable<CompanySiteCategoryStandardHistory>, IOrderedQueryable<CompanySiteCategoryStandardHistory>> orderBy, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryStandardHistory> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, Func<IQueryable<CompanySiteCategoryStandardHistory>, IOrderedQueryable<CompanySiteCategoryStandardHistory>> orderBy, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, List<Expression<Func<CompanySiteCategoryStandardHistory, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public List<GroupCount> GetGroupCount(Expression<Func<CompanySiteCategoryStandardHistory, bool>> where, Func<CompanySiteCategoryStandardHistory, object> selector)
        {
            return _repository.GetMany(where).GroupBy(selector).Select(g => new GroupCount { Name = g.Key, Count = g.Count() }).ToList();
        }

        public void Insert(CompanySiteCategoryStandardHistory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandardHistory");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanySiteCategoryStandardHistory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandardHistory");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanySiteCategoryStandardHistory item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryStandardHistory");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }


        public void SaveOrUpdate(CompanySiteCategoryStandardHistory item, bool saveChanges = true)
        {
            CompanySiteCategoryStandardHistory x = _repository.Get(i => i.RecordDate == item.RecordDate && i.CompanyId == item.CompanyId && i.SiteId == item.SiteId);
            if (x != null)
            {
                x.CompanySiteCategoryId = item.CompanySiteCategoryId;
                this.Update(x);
            }
            else
                this.Insert(item);

        }
    }

    #endregion
}
