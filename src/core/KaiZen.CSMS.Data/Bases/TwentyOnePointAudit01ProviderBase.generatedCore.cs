﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit01ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit01ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit01, KaiZen.CSMS.Entities.TwentyOnePointAudit01Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit01Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit01 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit01Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_01 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit01 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_01 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit01 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_01 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit01 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_01 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit01 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_01 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit01 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_01 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit01 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit01&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit01&gt;"/></returns>
		public static TList<TwentyOnePointAudit01> Fill(IDataReader reader, TList<TwentyOnePointAudit01> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit01 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit01")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit01Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit01>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit01",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit01();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit01Column.Id - 1)];
					c.Achieved1a = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1a - 1)];
					c.Achieved1b = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1b - 1)];
					c.Achieved1c = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c - 1)];
					c.Achieved1c1 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c1 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c1 - 1)];
					c.Achieved1c2 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c2 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c2 - 1)];
					c.Achieved1c3 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c3 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c3 - 1)];
					c.Achieved1c4 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c4 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c4 - 1)];
					c.Achieved1c5 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c5 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c5 - 1)];
					c.Achieved1c6 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c6 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c6 - 1)];
					c.Achieved1c7 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c7 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c7 - 1)];
					c.Achieved1c8 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c8 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c8 - 1)];
					c.Achieved1c9 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c9 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c9 - 1)];
					c.Achieved1c10 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c10 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c10 - 1)];
					c.Achieved1c11 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c11 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c11 - 1)];
					c.Achieved1d = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1d - 1)];
					c.Achieved1e = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1e - 1)];
					c.Achieved1f = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1f - 1)];
					c.Achieved1g = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1g - 1)];
					c.Achieved1h = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1h - 1)];
					c.Achieved1i = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1i - 1)];
					c.Achieved1j = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1j - 1)];
					c.Achieved1k = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1k - 1)];
					c.Achieved1l = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1l - 1)];
					c.Achieved1m = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1m - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1m - 1)];
					c.Achieved1n = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1n - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1n - 1)];
					c.Achieved1o = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1o - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1o - 1)];
					c.Achieved1p = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1p - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1p - 1)];
					c.Achieved1q = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1q - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1q - 1)];
					c.Achieved1r = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1r - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1r - 1)];
					c.Achieved1s = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1s - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1s - 1)];
					c.Achieved1t = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1t - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1t - 1)];
					c.Achieved1u = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1u - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1u - 1)];
					c.Achieved1v = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1v - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1v - 1)];
					c.Achieved1w = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1w - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1w - 1)];
					c.Achieved1x = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1x - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1x - 1)];
					c.Achieved1y = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1y - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1y - 1)];
					c.Achieved1z = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1z - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1z - 1)];
					c.Observation1a = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1a - 1)];
					c.Observation1b = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1b - 1)];
					c.Observation1c = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c - 1)];
					c.Observation1c1 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c1 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c1 - 1)];
					c.Observation1c2 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c2 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c2 - 1)];
					c.Observation1c3 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c3 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c3 - 1)];
					c.Observation1c4 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c4 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c4 - 1)];
					c.Observation1c5 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c5 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c5 - 1)];
					c.Observation1c6 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c6 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c6 - 1)];
					c.Observation1c7 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c7 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c7 - 1)];
					c.Observation1c8 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c8 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c8 - 1)];
					c.Observation1c9 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c9 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c9 - 1)];
					c.Observation1c10 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c10 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c10 - 1)];
					c.Observation1c11 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c11 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c11 - 1)];
					c.Observation1d = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1d - 1)];
					c.Observation1e = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1e - 1)];
					c.Observation1f = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1f - 1)];
					c.Observation1g = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1g - 1)];
					c.Observation1h = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1h - 1)];
					c.Observation1i = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1i - 1)];
					c.Observation1j = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1j - 1)];
					c.Observation1k = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1k - 1)];
					c.Observation1l = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1l - 1)];
					c.Observation1m = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1m - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1m - 1)];
					c.Observation1n = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1n - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1n - 1)];
					c.Observation1o = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1o - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1o - 1)];
					c.Observation1p = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1p - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1p - 1)];
					c.Observation1q = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1q - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1q - 1)];
					c.Observation1r = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1r - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1r - 1)];
					c.Observation1s = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1s - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1s - 1)];
					c.Observation1t = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1t - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1t - 1)];
					c.Observation1u = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1u - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1u - 1)];
					c.Observation1v = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1v - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1v - 1)];
					c.Observation1w = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1w - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1w - 1)];
					c.Observation1x = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1x - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1x - 1)];
					c.Observation1y = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1y - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1y - 1)];
					c.Observation1z = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1z - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1z - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit01 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit01Column.Id - 1)];
			entity.Achieved1a = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1a - 1)];
			entity.Achieved1b = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1b - 1)];
			entity.Achieved1c = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c - 1)];
			entity.Achieved1c1 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c1 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c1 - 1)];
			entity.Achieved1c2 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c2 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c2 - 1)];
			entity.Achieved1c3 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c3 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c3 - 1)];
			entity.Achieved1c4 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c4 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c4 - 1)];
			entity.Achieved1c5 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c5 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c5 - 1)];
			entity.Achieved1c6 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c6 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c6 - 1)];
			entity.Achieved1c7 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c7 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c7 - 1)];
			entity.Achieved1c8 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c8 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c8 - 1)];
			entity.Achieved1c9 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c9 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c9 - 1)];
			entity.Achieved1c10 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c10 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c10 - 1)];
			entity.Achieved1c11 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1c11 - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1c11 - 1)];
			entity.Achieved1d = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1d - 1)];
			entity.Achieved1e = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1e - 1)];
			entity.Achieved1f = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1f - 1)];
			entity.Achieved1g = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1g - 1)];
			entity.Achieved1h = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1h - 1)];
			entity.Achieved1i = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1i - 1)];
			entity.Achieved1j = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1j - 1)];
			entity.Achieved1k = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1k - 1)];
			entity.Achieved1l = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1l - 1)];
			entity.Achieved1m = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1m - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1m - 1)];
			entity.Achieved1n = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1n - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1n - 1)];
			entity.Achieved1o = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1o - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1o - 1)];
			entity.Achieved1p = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1p - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1p - 1)];
			entity.Achieved1q = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1q - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1q - 1)];
			entity.Achieved1r = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1r - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1r - 1)];
			entity.Achieved1s = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1s - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1s - 1)];
			entity.Achieved1t = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1t - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1t - 1)];
			entity.Achieved1u = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1u - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1u - 1)];
			entity.Achieved1v = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1v - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1v - 1)];
			entity.Achieved1w = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1w - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1w - 1)];
			entity.Achieved1x = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1x - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1x - 1)];
			entity.Achieved1y = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1y - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1y - 1)];
			entity.Achieved1z = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Achieved1z - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.Achieved1z - 1)];
			entity.Observation1a = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1a - 1)];
			entity.Observation1b = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1b - 1)];
			entity.Observation1c = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c - 1)];
			entity.Observation1c1 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c1 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c1 - 1)];
			entity.Observation1c2 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c2 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c2 - 1)];
			entity.Observation1c3 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c3 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c3 - 1)];
			entity.Observation1c4 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c4 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c4 - 1)];
			entity.Observation1c5 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c5 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c5 - 1)];
			entity.Observation1c6 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c6 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c6 - 1)];
			entity.Observation1c7 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c7 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c7 - 1)];
			entity.Observation1c8 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c8 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c8 - 1)];
			entity.Observation1c9 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c9 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c9 - 1)];
			entity.Observation1c10 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c10 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c10 - 1)];
			entity.Observation1c11 = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1c11 - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1c11 - 1)];
			entity.Observation1d = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1d - 1)];
			entity.Observation1e = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1e - 1)];
			entity.Observation1f = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1f - 1)];
			entity.Observation1g = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1g - 1)];
			entity.Observation1h = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1h - 1)];
			entity.Observation1i = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1i - 1)];
			entity.Observation1j = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1j - 1)];
			entity.Observation1k = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1k - 1)];
			entity.Observation1l = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1l - 1)];
			entity.Observation1m = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1m - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1m - 1)];
			entity.Observation1n = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1n - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1n - 1)];
			entity.Observation1o = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1o - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1o - 1)];
			entity.Observation1p = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1p - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1p - 1)];
			entity.Observation1q = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1q - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1q - 1)];
			entity.Observation1r = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1r - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1r - 1)];
			entity.Observation1s = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1s - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1s - 1)];
			entity.Observation1t = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1t - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1t - 1)];
			entity.Observation1u = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1u - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1u - 1)];
			entity.Observation1v = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1v - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1v - 1)];
			entity.Observation1w = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1w - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1w - 1)];
			entity.Observation1x = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1x - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1x - 1)];
			entity.Observation1y = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1y - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1y - 1)];
			entity.Observation1z = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.Observation1z - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit01Column.Observation1z - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit01Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit01Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit01 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved1a = Convert.IsDBNull(dataRow["Achieved1a"]) ? null : (System.Int32?)dataRow["Achieved1a"];
			entity.Achieved1b = Convert.IsDBNull(dataRow["Achieved1b"]) ? null : (System.Int32?)dataRow["Achieved1b"];
			entity.Achieved1c = Convert.IsDBNull(dataRow["Achieved1c"]) ? null : (System.Int32?)dataRow["Achieved1c"];
			entity.Achieved1c1 = Convert.IsDBNull(dataRow["Achieved1c1"]) ? null : (System.Int32?)dataRow["Achieved1c1"];
			entity.Achieved1c2 = Convert.IsDBNull(dataRow["Achieved1c2"]) ? null : (System.Int32?)dataRow["Achieved1c2"];
			entity.Achieved1c3 = Convert.IsDBNull(dataRow["Achieved1c3"]) ? null : (System.Int32?)dataRow["Achieved1c3"];
			entity.Achieved1c4 = Convert.IsDBNull(dataRow["Achieved1c4"]) ? null : (System.Int32?)dataRow["Achieved1c4"];
			entity.Achieved1c5 = Convert.IsDBNull(dataRow["Achieved1c5"]) ? null : (System.Int32?)dataRow["Achieved1c5"];
			entity.Achieved1c6 = Convert.IsDBNull(dataRow["Achieved1c6"]) ? null : (System.Int32?)dataRow["Achieved1c6"];
			entity.Achieved1c7 = Convert.IsDBNull(dataRow["Achieved1c7"]) ? null : (System.Int32?)dataRow["Achieved1c7"];
			entity.Achieved1c8 = Convert.IsDBNull(dataRow["Achieved1c8"]) ? null : (System.Int32?)dataRow["Achieved1c8"];
			entity.Achieved1c9 = Convert.IsDBNull(dataRow["Achieved1c9"]) ? null : (System.Int32?)dataRow["Achieved1c9"];
			entity.Achieved1c10 = Convert.IsDBNull(dataRow["Achieved1c10"]) ? null : (System.Int32?)dataRow["Achieved1c10"];
			entity.Achieved1c11 = Convert.IsDBNull(dataRow["Achieved1c11"]) ? null : (System.Int32?)dataRow["Achieved1c11"];
			entity.Achieved1d = Convert.IsDBNull(dataRow["Achieved1d"]) ? null : (System.Int32?)dataRow["Achieved1d"];
			entity.Achieved1e = Convert.IsDBNull(dataRow["Achieved1e"]) ? null : (System.Int32?)dataRow["Achieved1e"];
			entity.Achieved1f = Convert.IsDBNull(dataRow["Achieved1f"]) ? null : (System.Int32?)dataRow["Achieved1f"];
			entity.Achieved1g = Convert.IsDBNull(dataRow["Achieved1g"]) ? null : (System.Int32?)dataRow["Achieved1g"];
			entity.Achieved1h = Convert.IsDBNull(dataRow["Achieved1h"]) ? null : (System.Int32?)dataRow["Achieved1h"];
			entity.Achieved1i = Convert.IsDBNull(dataRow["Achieved1i"]) ? null : (System.Int32?)dataRow["Achieved1i"];
			entity.Achieved1j = Convert.IsDBNull(dataRow["Achieved1j"]) ? null : (System.Int32?)dataRow["Achieved1j"];
			entity.Achieved1k = Convert.IsDBNull(dataRow["Achieved1k"]) ? null : (System.Int32?)dataRow["Achieved1k"];
			entity.Achieved1l = Convert.IsDBNull(dataRow["Achieved1l"]) ? null : (System.Int32?)dataRow["Achieved1l"];
			entity.Achieved1m = Convert.IsDBNull(dataRow["Achieved1m"]) ? null : (System.Int32?)dataRow["Achieved1m"];
			entity.Achieved1n = Convert.IsDBNull(dataRow["Achieved1n"]) ? null : (System.Int32?)dataRow["Achieved1n"];
			entity.Achieved1o = Convert.IsDBNull(dataRow["Achieved1o"]) ? null : (System.Int32?)dataRow["Achieved1o"];
			entity.Achieved1p = Convert.IsDBNull(dataRow["Achieved1p"]) ? null : (System.Int32?)dataRow["Achieved1p"];
			entity.Achieved1q = Convert.IsDBNull(dataRow["Achieved1q"]) ? null : (System.Int32?)dataRow["Achieved1q"];
			entity.Achieved1r = Convert.IsDBNull(dataRow["Achieved1r"]) ? null : (System.Int32?)dataRow["Achieved1r"];
			entity.Achieved1s = Convert.IsDBNull(dataRow["Achieved1s"]) ? null : (System.Int32?)dataRow["Achieved1s"];
			entity.Achieved1t = Convert.IsDBNull(dataRow["Achieved1t"]) ? null : (System.Int32?)dataRow["Achieved1t"];
			entity.Achieved1u = Convert.IsDBNull(dataRow["Achieved1u"]) ? null : (System.Int32?)dataRow["Achieved1u"];
			entity.Achieved1v = Convert.IsDBNull(dataRow["Achieved1v"]) ? null : (System.Int32?)dataRow["Achieved1v"];
			entity.Achieved1w = Convert.IsDBNull(dataRow["Achieved1w"]) ? null : (System.Int32?)dataRow["Achieved1w"];
			entity.Achieved1x = Convert.IsDBNull(dataRow["Achieved1x"]) ? null : (System.Int32?)dataRow["Achieved1x"];
			entity.Achieved1y = Convert.IsDBNull(dataRow["Achieved1y"]) ? null : (System.Int32?)dataRow["Achieved1y"];
			entity.Achieved1z = Convert.IsDBNull(dataRow["Achieved1z"]) ? null : (System.Int32?)dataRow["Achieved1z"];
			entity.Observation1a = Convert.IsDBNull(dataRow["Observation1a"]) ? null : (System.String)dataRow["Observation1a"];
			entity.Observation1b = Convert.IsDBNull(dataRow["Observation1b"]) ? null : (System.String)dataRow["Observation1b"];
			entity.Observation1c = Convert.IsDBNull(dataRow["Observation1c"]) ? null : (System.String)dataRow["Observation1c"];
			entity.Observation1c1 = Convert.IsDBNull(dataRow["Observation1c1"]) ? null : (System.String)dataRow["Observation1c1"];
			entity.Observation1c2 = Convert.IsDBNull(dataRow["Observation1c2"]) ? null : (System.String)dataRow["Observation1c2"];
			entity.Observation1c3 = Convert.IsDBNull(dataRow["Observation1c3"]) ? null : (System.String)dataRow["Observation1c3"];
			entity.Observation1c4 = Convert.IsDBNull(dataRow["Observation1c4"]) ? null : (System.String)dataRow["Observation1c4"];
			entity.Observation1c5 = Convert.IsDBNull(dataRow["Observation1c5"]) ? null : (System.String)dataRow["Observation1c5"];
			entity.Observation1c6 = Convert.IsDBNull(dataRow["Observation1c6"]) ? null : (System.String)dataRow["Observation1c6"];
			entity.Observation1c7 = Convert.IsDBNull(dataRow["Observation1c7"]) ? null : (System.String)dataRow["Observation1c7"];
			entity.Observation1c8 = Convert.IsDBNull(dataRow["Observation1c8"]) ? null : (System.String)dataRow["Observation1c8"];
			entity.Observation1c9 = Convert.IsDBNull(dataRow["Observation1c9"]) ? null : (System.String)dataRow["Observation1c9"];
			entity.Observation1c10 = Convert.IsDBNull(dataRow["Observation1c10"]) ? null : (System.String)dataRow["Observation1c10"];
			entity.Observation1c11 = Convert.IsDBNull(dataRow["Observation1c11"]) ? null : (System.String)dataRow["Observation1c11"];
			entity.Observation1d = Convert.IsDBNull(dataRow["Observation1d"]) ? null : (System.String)dataRow["Observation1d"];
			entity.Observation1e = Convert.IsDBNull(dataRow["Observation1e"]) ? null : (System.String)dataRow["Observation1e"];
			entity.Observation1f = Convert.IsDBNull(dataRow["Observation1f"]) ? null : (System.String)dataRow["Observation1f"];
			entity.Observation1g = Convert.IsDBNull(dataRow["Observation1g"]) ? null : (System.String)dataRow["Observation1g"];
			entity.Observation1h = Convert.IsDBNull(dataRow["Observation1h"]) ? null : (System.String)dataRow["Observation1h"];
			entity.Observation1i = Convert.IsDBNull(dataRow["Observation1i"]) ? null : (System.String)dataRow["Observation1i"];
			entity.Observation1j = Convert.IsDBNull(dataRow["Observation1j"]) ? null : (System.String)dataRow["Observation1j"];
			entity.Observation1k = Convert.IsDBNull(dataRow["Observation1k"]) ? null : (System.String)dataRow["Observation1k"];
			entity.Observation1l = Convert.IsDBNull(dataRow["Observation1l"]) ? null : (System.String)dataRow["Observation1l"];
			entity.Observation1m = Convert.IsDBNull(dataRow["Observation1m"]) ? null : (System.String)dataRow["Observation1m"];
			entity.Observation1n = Convert.IsDBNull(dataRow["Observation1n"]) ? null : (System.String)dataRow["Observation1n"];
			entity.Observation1o = Convert.IsDBNull(dataRow["Observation1o"]) ? null : (System.String)dataRow["Observation1o"];
			entity.Observation1p = Convert.IsDBNull(dataRow["Observation1p"]) ? null : (System.String)dataRow["Observation1p"];
			entity.Observation1q = Convert.IsDBNull(dataRow["Observation1q"]) ? null : (System.String)dataRow["Observation1q"];
			entity.Observation1r = Convert.IsDBNull(dataRow["Observation1r"]) ? null : (System.String)dataRow["Observation1r"];
			entity.Observation1s = Convert.IsDBNull(dataRow["Observation1s"]) ? null : (System.String)dataRow["Observation1s"];
			entity.Observation1t = Convert.IsDBNull(dataRow["Observation1t"]) ? null : (System.String)dataRow["Observation1t"];
			entity.Observation1u = Convert.IsDBNull(dataRow["Observation1u"]) ? null : (System.String)dataRow["Observation1u"];
			entity.Observation1v = Convert.IsDBNull(dataRow["Observation1v"]) ? null : (System.String)dataRow["Observation1v"];
			entity.Observation1w = Convert.IsDBNull(dataRow["Observation1w"]) ? null : (System.String)dataRow["Observation1w"];
			entity.Observation1x = Convert.IsDBNull(dataRow["Observation1x"]) ? null : (System.String)dataRow["Observation1x"];
			entity.Observation1y = Convert.IsDBNull(dataRow["Observation1y"]) ? null : (System.String)dataRow["Observation1y"];
			entity.Observation1z = Convert.IsDBNull(dataRow["Observation1z"]) ? null : (System.String)dataRow["Observation1z"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit01"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit01 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit01 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint01Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit01 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit01 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit01 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit01 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point01IdSource != null)
						{
							child.Point01Id = child.Point01IdSource.Id;
						}
						else
						{
							child.Point01Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit01ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit01</c>
	///</summary>
	public enum TwentyOnePointAudit01ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit01</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit01ChildEntityTypes
	
	#region TwentyOnePointAudit01FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit01Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit01Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit01FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit01FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit01FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit01FilterBuilder
	
	#region TwentyOnePointAudit01ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit01Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit01ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit01Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit01ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit01ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit01ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit01ParameterBuilder
	
	#region TwentyOnePointAudit01SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit01Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit01"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit01SortBuilder : SqlSortBuilder<TwentyOnePointAudit01Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit01SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit01SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit01SortBuilder
	
} // end namespace
