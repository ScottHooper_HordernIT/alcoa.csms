﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using EntityFramework.Extensions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsefulLinkService
    {
        UsefulLink Get(Expression<Func<UsefulLink, bool>> where, List<Expression<Func<UsefulLink, object>>> includes);
        List<UsefulLink> GetAll();
        List<UsefulLink> GetMany(int? top, Expression<Func<UsefulLink, bool>> where, Func<IQueryable<UsefulLink>, IOrderedQueryable<UsefulLink>> orderBy, List<Expression<Func<UsefulLink, object>>> includes, bool isEntityTracking = true);
        List<UsefulLink> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsefulLink, bool>> where, Func<IQueryable<UsefulLink>, IOrderedQueryable<UsefulLink>> orderBy, List<Expression<Func<UsefulLink, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsefulLink, bool>> where, List<Expression<Func<UsefulLink, object>>> includes);
        void Insert(UsefulLink item);
        void Insert(UsefulLink item, bool saveChanges = true);
        void Update(UsefulLink item);
        void Update(UsefulLink item, bool saveChanges = true);
        void Delete(int UsefulLinkId, bool saveChanges);
        void Delete(UsefulLink item);
        void Delete(UsefulLink item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsefulLinkService : IUsefulLinkService
    {
        private readonly IRepository<UsefulLink> _repository;

        public UsefulLinkService(IRepository<UsefulLink> repository)
        {
            this._repository = repository;
        }

        public UsefulLink Get(Expression<Func<UsefulLink, bool>> where, List<Expression<Func<UsefulLink, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsefulLink> GetAll()
        {
            return _repository.GetAll(null, o => o.OrderBy(b => b.Ordinal), null).ToList();
        }

        public List<UsefulLink> GetMany(int? top, Expression<Func<UsefulLink, bool>> where, Func<IQueryable<UsefulLink>, IOrderedQueryable<UsefulLink>> orderBy, List<Expression<Func<UsefulLink, object>>> includes, bool isEntityTracking = true)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes, isEntityTracking).ToList();
            else
                return _repository.GetAll(top, orderBy, includes, isEntityTracking).ToList();
        }

        public List<UsefulLink> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsefulLink, bool>> where, Func<IQueryable<UsefulLink>, IOrderedQueryable<UsefulLink>> orderBy, List<Expression<Func<UsefulLink, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsefulLink, bool>> where, List<Expression<Func<UsefulLink, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsefulLink item)
        {
            Insert(item, true);
        }

        public void Insert(UsefulLink item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsefulLink");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsefulLink item)
        {
            Update(item, true);
        }

        public void Update(UsefulLink item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsefulLink");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsefulLink item)
        {
            if (item == null)
                throw new ArgumentNullException("UsefulLink");

            Delete(item.UsefulLinkId, true);
        }

        public void Delete(UsefulLink item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsefulLink");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(int UsefulLinkId, bool saveChanges = true)
        {
            _repository.Delete(e => e.UsefulLinkId == UsefulLinkId);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
