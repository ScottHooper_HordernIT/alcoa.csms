//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuestionnairePresentlyWithAction
    {
        public QuestionnairePresentlyWithAction()
        {
            this.QuestionnairePresentlyWithMetrics = new HashSet<QuestionnairePresentlyWithMetric>();
        }
    
        public int QuestionnairePresentlyWithActionId { get; set; }
        public string ActionName { get; set; }
        public string ActionDescription { get; set; }
        public int QuestionnairePresentlyWithUserId { get; set; }
        public Nullable<int> ProcessNo { get; set; }
    
        public virtual ICollection<QuestionnairePresentlyWithMetric> QuestionnairePresentlyWithMetrics { get; set; }
    }
}
