﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TwentyOnePointAudit_12' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TwentyOnePointAudit12 : TwentyOnePointAudit12Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TwentyOnePointAudit12"/> instance.
		///</summary>
		public TwentyOnePointAudit12():base(){}	
		
		#endregion
	}
}
