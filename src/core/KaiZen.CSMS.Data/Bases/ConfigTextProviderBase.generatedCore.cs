﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ConfigTextProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ConfigTextProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ConfigText, KaiZen.CSMS.Entities.ConfigTextKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigTextKey key)
		{
			return Delete(transactionManager, key.ConfigTextId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_configTextId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _configTextId)
		{
			return Delete(null, _configTextId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _configTextId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText_ConfigTextType key.
		///		FK_ConfigText_ConfigTextType Description: 
		/// </summary>
		/// <param name="_configTextTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText objects.</returns>
		public TList<ConfigText> GetByConfigTextTypeId(System.Int32 _configTextTypeId)
		{
			int count = -1;
			return GetByConfigTextTypeId(_configTextTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText_ConfigTextType key.
		///		FK_ConfigText_ConfigTextType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText objects.</returns>
		/// <remarks></remarks>
		public TList<ConfigText> GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId)
		{
			int count = -1;
			return GetByConfigTextTypeId(transactionManager, _configTextTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText_ConfigTextType key.
		///		FK_ConfigText_ConfigTextType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText objects.</returns>
		public TList<ConfigText> GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigTextTypeId(transactionManager, _configTextTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText_ConfigTextType key.
		///		fkConfigTextConfigTextType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_configTextTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText objects.</returns>
		public TList<ConfigText> GetByConfigTextTypeId(System.Int32 _configTextTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByConfigTextTypeId(null, _configTextTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText_ConfigTextType key.
		///		fkConfigTextConfigTextType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText objects.</returns>
		public TList<ConfigText> GetByConfigTextTypeId(System.Int32 _configTextTypeId, int start, int pageLength,out int count)
		{
			return GetByConfigTextTypeId(null, _configTextTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ConfigText_ConfigTextType key.
		///		FK_ConfigText_ConfigTextType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.ConfigText objects.</returns>
		public abstract TList<ConfigText> GetByConfigTextTypeId(TransactionManager transactionManager, System.Int32 _configTextTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ConfigText Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigTextKey key, int start, int pageLength)
		{
			return GetByConfigTextId(transactionManager, key.ConfigTextId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ConfigText index.
		/// </summary>
		/// <param name="_configTextId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText GetByConfigTextId(System.Int32 _configTextId)
		{
			int count = -1;
			return GetByConfigTextId(null,_configTextId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText index.
		/// </summary>
		/// <param name="_configTextId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText GetByConfigTextId(System.Int32 _configTextId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigTextId(null, _configTextId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText GetByConfigTextId(TransactionManager transactionManager, System.Int32 _configTextId)
		{
			int count = -1;
			return GetByConfigTextId(transactionManager, _configTextId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText GetByConfigTextId(TransactionManager transactionManager, System.Int32 _configTextId, int start, int pageLength)
		{
			int count = -1;
			return GetByConfigTextId(transactionManager, _configTextId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText index.
		/// </summary>
		/// <param name="_configTextId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText"/> class.</returns>
		public KaiZen.CSMS.Entities.ConfigText GetByConfigTextId(System.Int32 _configTextId, int start, int pageLength, out int count)
		{
			return GetByConfigTextId(null, _configTextId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ConfigText index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_configTextId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ConfigText"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ConfigText GetByConfigTextId(TransactionManager transactionManager, System.Int32 _configTextId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _ConfigText_GetRandom 
		
		/// <summary>
		///	This method wrap the '_ConfigText_GetRandom' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetRandom()
		{
			return GetRandom(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_ConfigText_GetRandom' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetRandom(int start, int pageLength)
		{
			return GetRandom(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_ConfigText_GetRandom' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetRandom(TransactionManager transactionManager)
		{
			return GetRandom(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_ConfigText_GetRandom' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetRandom(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ConfigText&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ConfigText&gt;"/></returns>
		public static TList<ConfigText> Fill(IDataReader reader, TList<ConfigText> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ConfigText c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ConfigText")
					.Append("|").Append((System.Int32)reader[((int)ConfigTextColumn.ConfigTextId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ConfigText>(
					key.ToString(), // EntityTrackingKey
					"ConfigText",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ConfigText();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ConfigTextId = (System.Int32)reader[((int)ConfigTextColumn.ConfigTextId - 1)];
					c.ConfigTextTypeId = (System.Int32)reader[((int)ConfigTextColumn.ConfigTextTypeId - 1)];
					c.ConfigText = (System.String)reader[((int)ConfigTextColumn.ConfigText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigText"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigText"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ConfigText entity)
		{
			if (!reader.Read()) return;
			
			entity.ConfigTextId = (System.Int32)reader[((int)ConfigTextColumn.ConfigTextId - 1)];
			entity.ConfigTextTypeId = (System.Int32)reader[((int)ConfigTextColumn.ConfigTextTypeId - 1)];
			entity.ConfigText = (System.String)reader[((int)ConfigTextColumn.ConfigText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ConfigText"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigText"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ConfigText entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ConfigTextId = (System.Int32)dataRow["ConfigTextId"];
			entity.ConfigTextTypeId = (System.Int32)dataRow["ConfigTextTypeId"];
			entity.ConfigText = (System.String)dataRow["ConfigText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ConfigText"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigText Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigText entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ConfigTextTypeIdSource	
			if (CanDeepLoad(entity, "ConfigTextType|ConfigTextTypeIdSource", deepLoadType, innerList) 
				&& entity.ConfigTextTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ConfigTextTypeId;
				ConfigTextType tmpEntity = EntityManager.LocateEntity<ConfigTextType>(EntityLocator.ConstructKeyFromPkItems(typeof(ConfigTextType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ConfigTextTypeIdSource = tmpEntity;
				else
					entity.ConfigTextTypeIdSource = DataRepository.ConfigTextTypeProvider.GetByConfigTextTypeId(transactionManager, entity.ConfigTextTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigTextTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ConfigTextTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ConfigTextTypeProvider.DeepLoad(transactionManager, entity.ConfigTextTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ConfigTextTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ConfigText object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ConfigText instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ConfigText Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ConfigText entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ConfigTextTypeIdSource
			if (CanDeepSave(entity, "ConfigTextType|ConfigTextTypeIdSource", deepSaveType, innerList) 
				&& entity.ConfigTextTypeIdSource != null)
			{
				DataRepository.ConfigTextTypeProvider.Save(transactionManager, entity.ConfigTextTypeIdSource);
				entity.ConfigTextTypeId = entity.ConfigTextTypeIdSource.ConfigTextTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ConfigTextChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ConfigText</c>
	///</summary>
	public enum ConfigTextChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ConfigTextType</c> at ConfigTextTypeIdSource
		///</summary>
		[ChildEntityType(typeof(ConfigTextType))]
		ConfigTextType,
		}
	
	#endregion ConfigTextChildEntityTypes
	
	#region ConfigTextFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ConfigTextColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextFilterBuilder : SqlFilterBuilder<ConfigTextColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextFilterBuilder class.
		/// </summary>
		public ConfigTextFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextFilterBuilder
	
	#region ConfigTextParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ConfigTextColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigTextParameterBuilder : ParameterizedSqlFilterBuilder<ConfigTextColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextParameterBuilder class.
		/// </summary>
		public ConfigTextParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ConfigTextParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ConfigTextParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ConfigTextParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ConfigTextParameterBuilder
	
	#region ConfigTextSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ConfigTextColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ConfigTextSortBuilder : SqlSortBuilder<ConfigTextColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigTextSqlSortBuilder class.
		/// </summary>
		public ConfigTextSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ConfigTextSortBuilder
	
} // end namespace
