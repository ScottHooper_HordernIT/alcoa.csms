﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryStandardAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanySiteCategoryStandardAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanySiteCategoryStandardAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompanySiteCategoryStandardAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanySiteCategoryStandardAudit&gt;"/></returns>
		public static TList<CompanySiteCategoryStandardAudit> Fill(IDataReader reader, TList<CompanySiteCategoryStandardAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanySiteCategoryStandardAudit")
					.Append("|").Append((System.Int32)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanySiteCategoryStandardAudit>(
					key.ToString(), // EntityTrackingKey
					"CompanySiteCategoryStandardAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditEventId - 1)];
					c.CompanySiteCategoryStandardId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryStandardId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryStandardId - 1)];
					c.CompanyId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.CompanyId - 1)];
					c.SiteId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.SiteId - 1)];
					c.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryId - 1)];
					c.LocationSpaUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.LocationSpaUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.LocationSpaUserId - 1)];
					c.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.LocationSponsorUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.LocationSponsorUserId - 1)];
					c.ArpUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ArpUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.ArpUserId - 1)];
					c.Area = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.Area - 1)))?null:(System.String)reader[((int)CompanySiteCategoryStandardAuditColumn.Area - 1)];
					c.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardAuditColumn.Approved - 1)];
					c.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.ApprovedByUserId - 1)];
					c.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardAuditColumn.ApprovedDate - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardAuditColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)CompanySiteCategoryStandardAuditColumn.AuditEventId - 1)];
			entity.CompanySiteCategoryStandardId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryStandardId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryStandardId - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.CompanyId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.SiteId - 1)];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.CompanySiteCategoryId - 1)];
			entity.LocationSpaUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.LocationSpaUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.LocationSpaUserId - 1)];
			entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.LocationSponsorUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.LocationSponsorUserId - 1)];
			entity.ArpUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ArpUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.ArpUserId - 1)];
			entity.Area = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.Area - 1)))?null:(System.String)reader[((int)CompanySiteCategoryStandardAuditColumn.Area - 1)];
			entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.Approved - 1)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardAuditColumn.Approved - 1)];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.ApprovedByUserId - 1)];
			entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardAuditColumn.ApprovedDate - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardAuditColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.CompanySiteCategoryStandardId = Convert.IsDBNull(dataRow["CompanySiteCategoryStandardId"]) ? null : (System.Int32?)dataRow["CompanySiteCategoryStandardId"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.CompanySiteCategoryId = Convert.IsDBNull(dataRow["CompanySiteCategoryId"]) ? null : (System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.LocationSpaUserId = Convert.IsDBNull(dataRow["LocationSpaUserId"]) ? null : (System.Int32?)dataRow["LocationSpaUserId"];
			entity.LocationSponsorUserId = Convert.IsDBNull(dataRow["LocationSponsorUserId"]) ? null : (System.Int32?)dataRow["LocationSponsorUserId"];
			entity.ArpUserId = Convert.IsDBNull(dataRow["ArpUserId"]) ? null : (System.Int32?)dataRow["ArpUserId"];
			entity.Area = Convert.IsDBNull(dataRow["Area"]) ? null : (System.String)dataRow["Area"];
			entity.Approved = Convert.IsDBNull(dataRow["Approved"]) ? null : (System.Boolean?)dataRow["Approved"];
			entity.ApprovedByUserId = Convert.IsDBNull(dataRow["ApprovedByUserId"]) ? null : (System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = Convert.IsDBNull(dataRow["ApprovedDate"]) ? null : (System.DateTime?)dataRow["ApprovedDate"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanySiteCategoryStandardAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanySiteCategoryStandardAudit</c>
	///</summary>
	public enum CompanySiteCategoryStandardAuditChildEntityTypes
	{
	}
	
	#endregion CompanySiteCategoryStandardAuditChildEntityTypes
	
	#region CompanySiteCategoryStandardAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanySiteCategoryStandardAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditFilterBuilder : SqlFilterBuilder<CompanySiteCategoryStandardAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditFilterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardAuditFilterBuilder
	
	#region CompanySiteCategoryStandardAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanySiteCategoryStandardAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardAuditParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryStandardAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardAuditParameterBuilder
	
	#region CompanySiteCategoryStandardAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanySiteCategoryStandardAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategoryStandardAuditSortBuilder : SqlSortBuilder<CompanySiteCategoryStandardAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardAuditSqlSortBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategoryStandardAuditSortBuilder
	
} // end namespace
