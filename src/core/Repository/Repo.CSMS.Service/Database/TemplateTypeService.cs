﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ITemplateTypeService
    {
        TemplateType Get(Expression<Func<TemplateType, bool>> where, List<Expression<Func<TemplateType, object>>> includes);
        List<TemplateType> GetMany(int? top, Expression<Func<TemplateType, bool>> where, Func<IQueryable<TemplateType>, IOrderedQueryable<TemplateType>> orderBy, List<Expression<Func<TemplateType, object>>> includes);
        List<TemplateType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<TemplateType, bool>> where, Func<IQueryable<TemplateType>, IOrderedQueryable<TemplateType>> orderBy, List<Expression<Func<TemplateType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<TemplateType, bool>> where, List<Expression<Func<TemplateType, object>>> includes);
        void Insert(TemplateType item, bool saveChanges = true);
        void Update(TemplateType item, bool saveChanges = true);
        void Delete(TemplateType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class TemplateTypeService : ITemplateTypeService
    {
        private readonly IRepository<TemplateType> _repository;

        public TemplateTypeService(IRepository<TemplateType> repository)
        {
            this._repository = repository;
        }

        public TemplateType Get(Expression<Func<TemplateType, bool>> where, List<Expression<Func<TemplateType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<TemplateType> GetMany(int? top, Expression<Func<TemplateType, bool>> where, Func<IQueryable<TemplateType>, IOrderedQueryable<TemplateType>> orderBy, List<Expression<Func<TemplateType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<TemplateType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<TemplateType, bool>> where, Func<IQueryable<TemplateType>, IOrderedQueryable<TemplateType>> orderBy, List<Expression<Func<TemplateType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<TemplateType, bool>> where, List<Expression<Func<TemplateType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(TemplateType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("TemplateType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(TemplateType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("TemplateType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(TemplateType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("TemplateType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
