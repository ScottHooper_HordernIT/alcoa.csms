using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

using System.IO;

using LumenWorks.Framework.IO.Csv;

namespace ALCOA.CSMS.Scripts2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string csvFilePath = Directory.GetCurrentDirectory() + "/alcoaproc.csv";
            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    if (!String.IsNullOrEmpty(csv["E-Mail Address"]))
                    {
                        if (!String.IsNullOrEmpty(csv["UserLogon"]))
                        {
                            try
                            {
                                CompaniesService cs = new CompaniesService();
                                Companies c = cs.GetByCompanyName("Alcoa");
                                UsersService us = new UsersService();
                                Users u = new Users();
                                u.UserLogon = "AUSTRALIA_ASIA\\" + csv["UserLogon"].ToString();

                                if (!String.IsNullOrEmpty(csv["Last Name"]))  u.LastName = csv["Last Name"].ToString();
                                if (!String.IsNullOrEmpty(csv["First Name"])) u.FirstName = csv["First Name"].ToString();
                                u.CompanyId = c.CompanyId;
                                if (!String.IsNullOrEmpty(csv["Position"])) u.Title = csv["Position"].ToString();
                                if (!String.IsNullOrEmpty(csv["Telephone Number"])) u.PhoneNo = csv["Telephone Number"].ToString();
                                if (!String.IsNullOrEmpty(csv["Mobile Number"])) u.MobNo = csv["Mobile Number"].ToString();
                                if (!String.IsNullOrEmpty(csv["E-Mail Address"])) u.Email = csv["E-Mail Address"].ToString();
                                u.RoleId = (int)RoleList.Reader;
                                u.ModifiedByUserId = 0;
                                us.Insert(u);

                                UsersProcurementService ups = new UsersProcurementService();
                                UsersProcurement up = new UsersProcurement();
                                up.UserId = u.UserId;
                                up.Enabled = true;
                                ups.Insert(up);
                            }
                            catch (Exception ex)
                            {
                                memoEdit1.Text += "\n" + "Error at:" + csv["UserLogon"] + ": " + ex.Message.ToString();
                            }
                        }
                    }
                }
            }
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            string csvFilePath = Directory.GetCurrentDirectory() + "/alcoaContacts.csv";
            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    try
                    {
                        ContactsAlcoaService cas = new ContactsAlcoaService();
                        ContactsAlcoa ca = new ContactsAlcoa();
                        ca.SiteId = Convert.ToInt32(csv["Site"].ToString());
                        ca.ContactFirstName = csv["First Name"].ToString();
                        ca.ContactLastName = csv["Last Name"].ToString();
                        ca.ContactTitlePosition = csv["Position/Role"].ToString();
                        ca.ContactEmail = csv["Email"].ToString();
                        ca.ContactMainNo = csv["Phone No"].ToString();
                        cas.Insert(ca);
                    }
                    catch (Exception ex)
                    {
                        memoEdit1.Text += "\n" + "Error at:" + csv["UserLogon"] + ": " + ex.Message.ToString();
                    }

                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            string csvFilePath = Directory.GetCurrentDirectory() + "/contractors.csv";
            using (CsvReader csv =
                   new CsvReader(new StreamReader(csvFilePath), true))
            {
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();

                while (csv.ReadNextRecord())
                {
                    try
                    {
                        CompaniesService cs = new CompaniesService();
                        Companies c = cs.GetByCompanyName(csv["Company Name"].ToString());
                        if (c == null)
                        {
                            c.CompanyName = csv["Company Name"].ToString();
                            c.CompanyAbn = csv["Company ABN"].ToString();
                            c.ModifiedbyUserId = 0;
                            cs.Insert(c);
                        }

                        if(!String.IsNullOrEmpty(csv["Email"].ToString()) && !String.IsNullOrEmpty(c.CompanyId.ToString()))
                        {
                            UsersService us = new UsersService();
                            Users u = us.GetByUserLogon(csv["Email"].ToString());
                            bool exists = true;
                            if (u == null)
                            {
                                u = new Users();
                                u.UserLogon = csv["Email"].ToString();
                                exists = false;
                            }

                            if (!String.IsNullOrEmpty(csv["Last Name"])) u.LastName = csv["Last Name"].ToString();
                            if (!String.IsNullOrEmpty(csv["First Name"])) u.FirstName = csv["First Name"].ToString();
                            u.CompanyId = c.CompanyId;
                            if (!String.IsNullOrEmpty(csv["Title"])) u.Title = csv["Title"].ToString();
                            if (!String.IsNullOrEmpty(csv["Telephone Number"])) u.PhoneNo = csv["Telephone Number"].ToString();
                            if (!String.IsNullOrEmpty(csv["Mobile Number"])) u.MobNo = csv["Mobile Number"].ToString();
                            if (!String.IsNullOrEmpty(csv["Email"])) u.Email = csv["Email"].ToString();
                            u.RoleId = (int)RoleList.Contractor;
                            u.ModifiedByUserId = 0;
                            if (exists == false)
                            {
                                us.Insert(u);
                            }
                            else
                            {
                                us.Save(u);
                            }

                            SitesService ss = new SitesService();
                            Sites s = ss.GetBySiteName(csv["Responsible For"].ToString());
                            if (s != null)
                            {
                                ContactsContractorsService ccs= new ContactsContractorsService();
                                ContactsContractors cc = new ContactsContractors();
                                cc = ccs.GetByContactLastNameContactFirstNameCompanyIdContactRoleContactTitleContactPhoneContactMobileContactEmailSiteId(csv["Last Name"].ToString(), csv["First Name"].ToString(), c.CompanyId, csv["Position/Role"].ToString(), csv["Title"].ToString(), csv["Telephone Number"].ToString(), "", csv["Email"].ToString(), s.SiteId);
                                bool exist = false;
                                if (cc != null) exist = true;
                                if (cc == null) cc = new ContactsContractors();
                                int? _siteid;
                                _siteid = s.SiteId;
                                cc.SiteId = _siteid;
                                cc.CompanyId = c.CompanyId;
                                cc.ContactFirstName = csv["First Name"].ToString();
                                cc.ContactLastName = csv["Last Name"].ToString();
                                cc.ContactRole = csv["Position/Role"].ToString();
                                cc.ContactTitle = csv["Title"].ToString();
                                cc.ContactEmail = csv["Email"].ToString();
                                cc.ContactPhone = csv["Telephone Number"].ToString();
                                cc.ContactMobile = csv["Mobile Number"].ToString();

                                if (exist == false)
                                {
                                    ccs.Insert(cc);
                                }
                                else
                                {
                                    ccs.Save(cc);
                                }
                                
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        memoEdit1.Text += "\n" + "Error at:" + csv["Email"] + ": " + ex.Message.ToString();
                    }

                }
            }
        }
    }
}