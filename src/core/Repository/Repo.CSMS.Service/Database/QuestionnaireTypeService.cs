﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireTypeService
    {
        QuestionnaireType Get(Expression<Func<QuestionnaireType, bool>> where, List<Expression<Func<QuestionnaireType, object>>> includes);
        List<QuestionnaireType> GetMany(int? top, Expression<Func<QuestionnaireType, bool>> where, Func<IQueryable<QuestionnaireType>, IOrderedQueryable<QuestionnaireType>> orderBy, List<Expression<Func<QuestionnaireType, object>>> includes);
        List<QuestionnaireType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireType, bool>> where, Func<IQueryable<QuestionnaireType>, IOrderedQueryable<QuestionnaireType>> orderBy, List<Expression<Func<QuestionnaireType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireType, bool>> where, List<Expression<Func<QuestionnaireType, object>>> includes);
        void Insert(QuestionnaireType item, bool saveChanges = true);
        void Update(QuestionnaireType item, bool saveChanges = true);
        void Delete(QuestionnaireType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireTypeService : IQuestionnaireTypeService
    {
        private readonly IRepository<QuestionnaireType> _repository;

        public QuestionnaireTypeService(IRepository<QuestionnaireType> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireType Get(Expression<Func<QuestionnaireType, bool>> where, List<Expression<Func<QuestionnaireType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireType> GetMany(int? top, Expression<Func<QuestionnaireType, bool>> where, Func<IQueryable<QuestionnaireType>, IOrderedQueryable<QuestionnaireType>> orderBy, List<Expression<Func<QuestionnaireType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireType, bool>> where, Func<IQueryable<QuestionnaireType>, IOrderedQueryable<QuestionnaireType>> orderBy, List<Expression<Func<QuestionnaireType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireType, bool>> where, List<Expression<Func<QuestionnaireType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
