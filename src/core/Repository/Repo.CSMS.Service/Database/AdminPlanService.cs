﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminPlanService
    {
        AdminPlan Get(Expression<Func<AdminPlan, bool>> where, List<Expression<Func<AdminPlan, object>>> includes);
        List<AdminPlan> GetMany(int? top, Expression<Func<AdminPlan, bool>> where, Func<IQueryable<AdminPlan>, IOrderedQueryable<AdminPlan>> orderBy, List<Expression<Func<AdminPlan, object>>> includes);
        List<AdminPlan> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminPlan, bool>> where, Func<IQueryable<AdminPlan>, IOrderedQueryable<AdminPlan>> orderBy, List<Expression<Func<AdminPlan, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminPlan, bool>> where, List<Expression<Func<AdminPlan, object>>> includes);
        void Insert(AdminPlan item, bool saveChanges = true);
        void Update(AdminPlan item, bool saveChanges = true);
        void Delete(AdminPlan item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminPlanService : IAdminPlanService
    {
        private readonly IRepository<AdminPlan> _repository;

        public AdminPlanService(IRepository<AdminPlan> repository)
        {
            this._repository = repository;
        }

        public AdminPlan Get(Expression<Func<AdminPlan, bool>> where, List<Expression<Func<AdminPlan, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminPlan> GetMany(int? top, Expression<Func<AdminPlan, bool>> where, Func<IQueryable<AdminPlan>, IOrderedQueryable<AdminPlan>> orderBy, List<Expression<Func<AdminPlan, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminPlan> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminPlan, bool>> where, Func<IQueryable<AdminPlan>, IOrderedQueryable<AdminPlan>> orderBy, List<Expression<Func<AdminPlan, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminPlan, bool>> where, List<Expression<Func<AdminPlan, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminPlan item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminPlan");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminPlan item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminPlan");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminPlan item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminPlan");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
