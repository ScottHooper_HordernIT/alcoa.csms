﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="YearlyTargetsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class YearlyTargetsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.YearlyTargets, KaiZen.CSMS.Entities.YearlyTargetsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.YearlyTargetsKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.YearlyTargets Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.YearlyTargetsKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_YearlyTargets index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.YearlyTargets GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_YearlyTargets index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.YearlyTargets GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_YearlyTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.YearlyTargets GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_YearlyTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.YearlyTargets GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_YearlyTargets index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> class.</returns>
		public KaiZen.CSMS.Entities.YearlyTargets GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_YearlyTargets index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.YearlyTargets GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;YearlyTargets&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;YearlyTargets&gt;"/></returns>
		public static TList<YearlyTargets> Fill(IDataReader reader, TList<YearlyTargets> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.YearlyTargets c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("YearlyTargets")
					.Append("|").Append((System.Int32)reader[((int)YearlyTargetsColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<YearlyTargets>(
					key.ToString(), // EntityTrackingKey
					"YearlyTargets",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.YearlyTargets();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)YearlyTargetsColumn.Id - 1)];
					c.Year = (System.Int32)reader[((int)YearlyTargetsColumn.Year - 1)];
					c.EhsAudits = (reader.IsDBNull(((int)YearlyTargetsColumn.EhsAudits - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.EhsAudits - 1)];
					c.WorkplaceSafetyCompliance = (reader.IsDBNull(((int)YearlyTargetsColumn.WorkplaceSafetyCompliance - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.WorkplaceSafetyCompliance - 1)];
					c.HealthSafetyWorkContacts = (reader.IsDBNull(((int)YearlyTargetsColumn.HealthSafetyWorkContacts - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.HealthSafetyWorkContacts - 1)];
					c.BehaviouralSafetyProgram = (reader.IsDBNull(((int)YearlyTargetsColumn.BehaviouralSafetyProgram - 1)))?null:(System.Decimal?)reader[((int)YearlyTargetsColumn.BehaviouralSafetyProgram - 1)];
					c.QuarterlyAuditScore = (reader.IsDBNull(((int)YearlyTargetsColumn.QuarterlyAuditScore - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.QuarterlyAuditScore - 1)];
					c.ToolboxMeetingsPerMonth = (reader.IsDBNull(((int)YearlyTargetsColumn.ToolboxMeetingsPerMonth - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.ToolboxMeetingsPerMonth - 1)];
					c.AlcoaWeeklyContractorsMeeting = (reader.IsDBNull(((int)YearlyTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)];
					c.AlcoaMonthlyContractorsMeeting = (reader.IsDBNull(((int)YearlyTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)];
					c.SafetyPlan = (reader.IsDBNull(((int)YearlyTargetsColumn.SafetyPlan - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.SafetyPlan - 1)];
					c.JsaScore = (reader.IsDBNull(((int)YearlyTargetsColumn.JsaScore - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.JsaScore - 1)];
					c.FatalityPrevention = (reader.IsDBNull(((int)YearlyTargetsColumn.FatalityPrevention - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.FatalityPrevention - 1)];
					c.Trifr = (reader.IsDBNull(((int)YearlyTargetsColumn.Trifr - 1)))?null:(System.Decimal?)reader[((int)YearlyTargetsColumn.Trifr - 1)];
					c.Aifr = (reader.IsDBNull(((int)YearlyTargetsColumn.Aifr - 1)))?null:(System.Decimal?)reader[((int)YearlyTargetsColumn.Aifr - 1)];
					c.Training = (reader.IsDBNull(((int)YearlyTargetsColumn.Training - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.Training - 1)];
					c.MedicalSchedule = (reader.IsDBNull(((int)YearlyTargetsColumn.MedicalSchedule - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.MedicalSchedule - 1)];
					c.TrainingSchedule = (reader.IsDBNull(((int)YearlyTargetsColumn.TrainingSchedule - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.TrainingSchedule - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.YearlyTargets entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)YearlyTargetsColumn.Id - 1)];
			entity.Year = (System.Int32)reader[((int)YearlyTargetsColumn.Year - 1)];
			entity.EhsAudits = (reader.IsDBNull(((int)YearlyTargetsColumn.EhsAudits - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.EhsAudits - 1)];
			entity.WorkplaceSafetyCompliance = (reader.IsDBNull(((int)YearlyTargetsColumn.WorkplaceSafetyCompliance - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.WorkplaceSafetyCompliance - 1)];
			entity.HealthSafetyWorkContacts = (reader.IsDBNull(((int)YearlyTargetsColumn.HealthSafetyWorkContacts - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.HealthSafetyWorkContacts - 1)];
			entity.BehaviouralSafetyProgram = (reader.IsDBNull(((int)YearlyTargetsColumn.BehaviouralSafetyProgram - 1)))?null:(System.Decimal?)reader[((int)YearlyTargetsColumn.BehaviouralSafetyProgram - 1)];
			entity.QuarterlyAuditScore = (reader.IsDBNull(((int)YearlyTargetsColumn.QuarterlyAuditScore - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.QuarterlyAuditScore - 1)];
			entity.ToolboxMeetingsPerMonth = (reader.IsDBNull(((int)YearlyTargetsColumn.ToolboxMeetingsPerMonth - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.ToolboxMeetingsPerMonth - 1)];
			entity.AlcoaWeeklyContractorsMeeting = (reader.IsDBNull(((int)YearlyTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.AlcoaWeeklyContractorsMeeting - 1)];
			entity.AlcoaMonthlyContractorsMeeting = (reader.IsDBNull(((int)YearlyTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.AlcoaMonthlyContractorsMeeting - 1)];
			entity.SafetyPlan = (reader.IsDBNull(((int)YearlyTargetsColumn.SafetyPlan - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.SafetyPlan - 1)];
			entity.JsaScore = (reader.IsDBNull(((int)YearlyTargetsColumn.JsaScore - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.JsaScore - 1)];
			entity.FatalityPrevention = (reader.IsDBNull(((int)YearlyTargetsColumn.FatalityPrevention - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.FatalityPrevention - 1)];
			entity.Trifr = (reader.IsDBNull(((int)YearlyTargetsColumn.Trifr - 1)))?null:(System.Decimal?)reader[((int)YearlyTargetsColumn.Trifr - 1)];
			entity.Aifr = (reader.IsDBNull(((int)YearlyTargetsColumn.Aifr - 1)))?null:(System.Decimal?)reader[((int)YearlyTargetsColumn.Aifr - 1)];
			entity.Training = (reader.IsDBNull(((int)YearlyTargetsColumn.Training - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.Training - 1)];
			entity.MedicalSchedule = (reader.IsDBNull(((int)YearlyTargetsColumn.MedicalSchedule - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.MedicalSchedule - 1)];
			entity.TrainingSchedule = (reader.IsDBNull(((int)YearlyTargetsColumn.TrainingSchedule - 1)))?null:(System.Int32?)reader[((int)YearlyTargetsColumn.TrainingSchedule - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.YearlyTargets entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Year = (System.Int32)dataRow["Year"];
			entity.EhsAudits = Convert.IsDBNull(dataRow["EHSAudits"]) ? null : (System.Int32?)dataRow["EHSAudits"];
			entity.WorkplaceSafetyCompliance = Convert.IsDBNull(dataRow["WorkplaceSafetyCompliance"]) ? null : (System.Int32?)dataRow["WorkplaceSafetyCompliance"];
			entity.HealthSafetyWorkContacts = Convert.IsDBNull(dataRow["HealthSafetyWorkContacts"]) ? null : (System.Int32?)dataRow["HealthSafetyWorkContacts"];
			entity.BehaviouralSafetyProgram = Convert.IsDBNull(dataRow["BehaviouralSafetyProgram"]) ? null : (System.Decimal?)dataRow["BehaviouralSafetyProgram"];
			entity.QuarterlyAuditScore = Convert.IsDBNull(dataRow["QuarterlyAuditScore"]) ? null : (System.Int32?)dataRow["QuarterlyAuditScore"];
			entity.ToolboxMeetingsPerMonth = Convert.IsDBNull(dataRow["ToolboxMeetingsPerMonth"]) ? null : (System.Int32?)dataRow["ToolboxMeetingsPerMonth"];
			entity.AlcoaWeeklyContractorsMeeting = Convert.IsDBNull(dataRow["AlcoaWeeklyContractorsMeeting"]) ? null : (System.Int32?)dataRow["AlcoaWeeklyContractorsMeeting"];
			entity.AlcoaMonthlyContractorsMeeting = Convert.IsDBNull(dataRow["AlcoaMonthlyContractorsMeeting"]) ? null : (System.Int32?)dataRow["AlcoaMonthlyContractorsMeeting"];
			entity.SafetyPlan = Convert.IsDBNull(dataRow["SafetyPlan"]) ? null : (System.Int32?)dataRow["SafetyPlan"];
			entity.JsaScore = Convert.IsDBNull(dataRow["JSAScore"]) ? null : (System.Int32?)dataRow["JSAScore"];
			entity.FatalityPrevention = Convert.IsDBNull(dataRow["FatalityPrevention"]) ? null : (System.Int32?)dataRow["FatalityPrevention"];
			entity.Trifr = Convert.IsDBNull(dataRow["TRIFR"]) ? null : (System.Decimal?)dataRow["TRIFR"];
			entity.Aifr = Convert.IsDBNull(dataRow["AIFR"]) ? null : (System.Decimal?)dataRow["AIFR"];
			entity.Training = Convert.IsDBNull(dataRow["Training"]) ? null : (System.Int32?)dataRow["Training"];
			entity.MedicalSchedule = Convert.IsDBNull(dataRow["MedicalSchedule"]) ? null : (System.Int32?)dataRow["MedicalSchedule"];
			entity.TrainingSchedule = Convert.IsDBNull(dataRow["TrainingSchedule"]) ? null : (System.Int32?)dataRow["TrainingSchedule"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.YearlyTargets"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.YearlyTargets Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.YearlyTargets entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.YearlyTargets object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.YearlyTargets instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.YearlyTargets Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.YearlyTargets entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region YearlyTargetsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.YearlyTargets</c>
	///</summary>
	public enum YearlyTargetsChildEntityTypes
	{
	}
	
	#endregion YearlyTargetsChildEntityTypes
	
	#region YearlyTargetsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;YearlyTargetsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsFilterBuilder : SqlFilterBuilder<YearlyTargetsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsFilterBuilder class.
		/// </summary>
		public YearlyTargetsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public YearlyTargetsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public YearlyTargetsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion YearlyTargetsFilterBuilder
	
	#region YearlyTargetsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;YearlyTargetsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsParameterBuilder : ParameterizedSqlFilterBuilder<YearlyTargetsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsParameterBuilder class.
		/// </summary>
		public YearlyTargetsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public YearlyTargetsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public YearlyTargetsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion YearlyTargetsParameterBuilder
	
	#region YearlyTargetsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;YearlyTargetsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class YearlyTargetsSortBuilder : SqlSortBuilder<YearlyTargetsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsSqlSortBuilder class.
		/// </summary>
		public YearlyTargetsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion YearlyTargetsSortBuilder
	
} // end namespace
