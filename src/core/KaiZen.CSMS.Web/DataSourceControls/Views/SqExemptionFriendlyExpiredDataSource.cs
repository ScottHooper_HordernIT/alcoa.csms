﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SqExemptionFriendlyExpiredProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(SqExemptionFriendlyExpiredDataSourceDesigner))]
	public class SqExemptionFriendlyExpiredDataSource : ReadOnlyDataSource<SqExemptionFriendlyExpired>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredDataSource class.
		/// </summary>
		public SqExemptionFriendlyExpiredDataSource() : base(new SqExemptionFriendlyExpiredService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SqExemptionFriendlyExpiredDataSourceView used by the SqExemptionFriendlyExpiredDataSource.
		/// </summary>
		protected SqExemptionFriendlyExpiredDataSourceView SqExemptionFriendlyExpiredView
		{
			get { return ( View as SqExemptionFriendlyExpiredDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SqExemptionFriendlyExpiredDataSourceView class that is to be
		/// used by the SqExemptionFriendlyExpiredDataSource.
		/// </summary>
		/// <returns>An instance of the SqExemptionFriendlyExpiredDataSourceView class.</returns>
		protected override BaseDataSourceView<SqExemptionFriendlyExpired, Object> GetNewDataSourceView()
		{
			return new SqExemptionFriendlyExpiredDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SqExemptionFriendlyExpiredDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SqExemptionFriendlyExpiredDataSourceView : ReadOnlyDataSourceView<SqExemptionFriendlyExpired>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyExpiredDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SqExemptionFriendlyExpiredDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SqExemptionFriendlyExpiredDataSourceView(SqExemptionFriendlyExpiredDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SqExemptionFriendlyExpiredDataSource SqExemptionFriendlyExpiredOwner
		{
			get { return Owner as SqExemptionFriendlyExpiredDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SqExemptionFriendlyExpiredService SqExemptionFriendlyExpiredProvider
		{
			get { return Provider as SqExemptionFriendlyExpiredService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region SqExemptionFriendlyExpiredDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SqExemptionFriendlyExpiredDataSource class.
	/// </summary>
	public class SqExemptionFriendlyExpiredDataSourceDesigner : ReadOnlyDataSourceDesigner<SqExemptionFriendlyExpired>
	{
	}

	#endregion SqExemptionFriendlyExpiredDataSourceDesigner

	#region SqExemptionFriendlyExpiredFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyExpired"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyExpiredFilter : SqlFilter<SqExemptionFriendlyExpiredColumn>
	{
	}

	#endregion SqExemptionFriendlyExpiredFilter

	#region SqExemptionFriendlyExpiredExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyExpired"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyExpiredExpressionBuilder : SqlExpressionBuilder<SqExemptionFriendlyExpiredColumn>
	{
	}
	
	#endregion SqExemptionFriendlyExpiredExpressionBuilder		
}

