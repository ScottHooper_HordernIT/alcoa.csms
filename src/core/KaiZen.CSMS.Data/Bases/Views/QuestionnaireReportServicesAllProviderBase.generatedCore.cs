﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportServicesAllProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportServicesAllProviderBaseCore : EntityViewProviderBase<QuestionnaireReportServicesAll>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesAll&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportServicesAll&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportServicesAll&gt; Fill(DataSet dataSet, VList<QuestionnaireReportServicesAll> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportServicesAll>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportServicesAll&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportServicesAll>"/></returns>
		protected static VList&lt;QuestionnaireReportServicesAll&gt; Fill(DataTable dataTable, VList<QuestionnaireReportServicesAll> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportServicesAll c = new QuestionnaireReportServicesAll();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.CategoryText = (Convert.IsDBNull(row["CategoryText"]))?string.Empty:(System.String)row["CategoryText"];
					c.QuestionnaireTypeDesc = (Convert.IsDBNull(row["QuestionnaireTypeDesc"]))?string.Empty:(System.String)row["QuestionnaireTypeDesc"];
					c.LevelOfSupervision = (Convert.IsDBNull(row["LevelOfSupervision"]))?string.Empty:(System.String)row["LevelOfSupervision"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.Type = (Convert.IsDBNull(row["Type"]))?string.Empty:(System.String)row["Type"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportServicesAll&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportServicesAll&gt;"/></returns>
		protected VList<QuestionnaireReportServicesAll> Fill(IDataReader reader, VList<QuestionnaireReportServicesAll> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportServicesAll entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportServicesAll>("QuestionnaireReportServicesAll",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportServicesAll();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportServicesAllColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportServicesAllColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportServicesAllColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.CategoryText = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.CategoryText)];
					//entity.CategoryText = (Convert.IsDBNull(reader["CategoryText"]))?string.Empty:(System.String)reader["CategoryText"];
					entity.QuestionnaireTypeDesc = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.QuestionnaireTypeDesc)];
					//entity.QuestionnaireTypeDesc = (Convert.IsDBNull(reader["QuestionnaireTypeDesc"]))?string.Empty:(System.String)reader["QuestionnaireTypeDesc"];
					entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireReportServicesAllColumn.LevelOfSupervision)];
					//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportServicesAllColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesAllColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesAllColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.Type = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.Type)];
					//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesAll"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesAll"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportServicesAll entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportServicesAllColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportServicesAllColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportServicesAllColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.CategoryText = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.CategoryText)];
			//entity.CategoryText = (Convert.IsDBNull(reader["CategoryText"]))?string.Empty:(System.String)reader["CategoryText"];
			entity.QuestionnaireTypeDesc = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.QuestionnaireTypeDesc)];
			//entity.QuestionnaireTypeDesc = (Convert.IsDBNull(reader["QuestionnaireTypeDesc"]))?string.Empty:(System.String)reader["QuestionnaireTypeDesc"];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireReportServicesAllColumn.LevelOfSupervision)];
			//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportServicesAllColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportServicesAllColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesAllColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireReportServicesAllColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireReportServicesAllColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.Type = (System.String)reader[((int)QuestionnaireReportServicesAllColumn.Type)];
			//entity.Type = (Convert.IsDBNull(reader["Type"]))?string.Empty:(System.String)reader["Type"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportServicesAll"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportServicesAll"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportServicesAll entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.CategoryText = (Convert.IsDBNull(dataRow["CategoryText"]))?string.Empty:(System.String)dataRow["CategoryText"];
			entity.QuestionnaireTypeDesc = (Convert.IsDBNull(dataRow["QuestionnaireTypeDesc"]))?string.Empty:(System.String)dataRow["QuestionnaireTypeDesc"];
			entity.LevelOfSupervision = (Convert.IsDBNull(dataRow["LevelOfSupervision"]))?string.Empty:(System.String)dataRow["LevelOfSupervision"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.Type = (Convert.IsDBNull(dataRow["Type"]))?string.Empty:(System.String)dataRow["Type"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportServicesAllFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesAllFilterBuilder : SqlFilterBuilder<QuestionnaireReportServicesAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllFilterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesAllFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesAllFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesAllFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesAllFilterBuilder

	#region QuestionnaireReportServicesAllParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesAllParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportServicesAllColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllParameterBuilder class.
		/// </summary>
		public QuestionnaireReportServicesAllParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportServicesAllParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportServicesAllParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportServicesAllParameterBuilder
	
	#region QuestionnaireReportServicesAllSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesAll"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportServicesAllSortBuilder : SqlSortBuilder<QuestionnaireReportServicesAllColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesAllSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportServicesAllSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportServicesAllSortBuilder

} // end namespace
