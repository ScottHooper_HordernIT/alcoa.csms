﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsaProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsaProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Csa, KaiZen.CSMS.Entities.CsaKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsaKey key)
		{
			return Delete(transactionManager, key.CsaId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csaId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csaId)
		{
			return Delete(null, _csaId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csaId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Created key.
		///		FK_CSA_Users_Created Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Created key.
		///		FK_CSA_Users_Created Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		/// <remarks></remarks>
		public TList<Csa> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Created key.
		///		FK_CSA_Users_Created Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Created key.
		///		fkCsaUsersCreated Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Created key.
		///		fkCsaUsersCreated Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Created key.
		///		FK_CSA_Users_Created Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public abstract TList<Csa> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Modified key.
		///		FK_CSA_Users_Modified Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Modified key.
		///		FK_CSA_Users_Modified Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		/// <remarks></remarks>
		public TList<Csa> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Modified key.
		///		FK_CSA_Users_Modified Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Modified key.
		///		fkCsaUsersModified Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Modified key.
		///		fkCsaUsersModified Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Users_Modified key.
		///		FK_CSA_Users_Modified Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public abstract TList<Csa> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Companies key.
		///		FK_CSA_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Companies key.
		///		FK_CSA_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		/// <remarks></remarks>
		public TList<Csa> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Companies key.
		///		FK_CSA_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Companies key.
		///		fkCsaCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Companies key.
		///		fkCsaCompanies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Companies key.
		///		FK_CSA_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public abstract TList<Csa> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Sites key.
		///		FK_CSA_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Sites key.
		///		FK_CSA_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		/// <remarks></remarks>
		public TList<Csa> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Sites key.
		///		FK_CSA_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Sites key.
		///		fkCsaSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Sites key.
		///		fkCsaSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public TList<Csa> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Sites key.
		///		FK_CSA_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Csa objects.</returns>
		public abstract TList<Csa> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Csa Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsaKey key, int start, int pageLength)
		{
			return GetByCsaId(transactionManager, key.CsaId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CSA index.
		/// </summary>
		/// <param name="_csaId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCsaId(System.Int32 _csaId)
		{
			int count = -1;
			return GetByCsaId(null,_csaId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA index.
		/// </summary>
		/// <param name="_csaId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCsaId(System.Int32 _csaId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaId(null, _csaId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCsaId(TransactionManager transactionManager, System.Int32 _csaId)
		{
			int count = -1;
			return GetByCsaId(transactionManager, _csaId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCsaId(TransactionManager transactionManager, System.Int32 _csaId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaId(transactionManager, _csaId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA index.
		/// </summary>
		/// <param name="_csaId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCsaId(System.Int32 _csaId, int start, int pageLength, out int count)
		{
			return GetByCsaId(null, _csaId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Csa GetByCsaId(TransactionManager transactionManager, System.Int32 _csaId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CSA index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdQtrIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(null,_companyId, _siteId, _qtrId, _year, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdQtrIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(null, _companyId, _siteId, _qtrId, _year, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdQtrIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(transactionManager, _companyId, _siteId, _qtrId, _year, 0, int.MaxValue, out count);
		}

        //Added by Pankaj Dikshit for CSMS Enhancement item# 5
        /// <summary>
        /// 	Gets rows from the datasource based on the IX_CSA index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_companyId"></param>
        /// <param name="_siteId"></param>
        /// <param name="_qtrId"></param>
        /// <param name="_year"></param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
        public KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdForHalfYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr1Id, System.Int32 _qtr2Id, System.Int64 _year)
        {
            int count = -1;
            return GetByCompanyIdSiteIdForHalfYear(transactionManager, _companyId, _siteId, _qtr1Id, _qtr2Id, _year, 0, int.MaxValue, out count);
        }

		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdQtrIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtrIdYear(transactionManager, _companyId, _siteId, _qtrId, _year, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdQtrIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteIdQtrIdYear(null, _companyId, _siteId, _qtrId, _year, start, pageLength, out count);
		}

        //Added by Pankaj Dikshit for CSMS Enhancement item# 5
        /// <summary>
        /// 	Gets rows from the datasource based on the IX_CSA index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_companyId"></param>
        /// <param name="_siteId"></param>
        /// <param name="_qtrId"></param>
        /// <param name="_year"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
        public abstract KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdForHalfYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr1Id, System.Int32 _qtr2Id, System.Int64 _year, int start, int pageLength, out int count);		
		
        /// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtrId"></param>
		/// <param name="_year"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Csa"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Csa GetByCompanyIdSiteIdQtrIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtrId, System.Int64 _year, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _CSA_ComplianceReport_BySite 
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(null, 0, int.MaxValue , sYear, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(int start, int pageLength, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(null, start, pageLength , sYear, siteId, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_BySite(TransactionManager transactionManager, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
		{
			return ComplianceReport_BySite(transactionManager, 0, int.MaxValue , sYear, siteId, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_BySite' stored procedure. 
		/// </summary>
		/// <param name="sYear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_BySite(TransactionManager transactionManager, int start, int pageLength , System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId);
		
		#endregion
		
		#region _CSA_ComplianceReport 
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport(null, 0, int.MaxValue , currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(int start, int pageLength, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport(null, start, pageLength , currentyear, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(TransactionManager transactionManager, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport(transactionManager, 0, int.MaxValue , currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport(TransactionManager transactionManager, int start, int pageLength , System.Int32? currentyear, System.Int32? siteId);
		
		#endregion
		
		#region _CSA_GetReport_NonCompliance 
		
		/// <summary>
		///	This method wrap the '_CSA_GetReport_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="status"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_NonCompliance(System.Int32? currentyear, System.Int32? regionId, System.String status)
		{
			return GetReport_NonCompliance(null, 0, int.MaxValue , currentyear, regionId, status);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_GetReport_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="status"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_NonCompliance(int start, int pageLength, System.Int32? currentyear, System.Int32? regionId, System.String status)
		{
			return GetReport_NonCompliance(null, start, pageLength , currentyear, regionId, status);
		}
				
		/// <summary>
		///	This method wrap the '_CSA_GetReport_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="status"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_NonCompliance(TransactionManager transactionManager, System.Int32? currentyear, System.Int32? regionId, System.String status)
		{
			return GetReport_NonCompliance(transactionManager, 0, int.MaxValue , currentyear, regionId, status);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_GetReport_NonCompliance' stored procedure. 
		/// </summary>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="status"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_NonCompliance(TransactionManager transactionManager, int start, int pageLength , System.Int32? currentyear, System.Int32? regionId, System.String status);
		
		#endregion
		
		#region _CSA_ComplianceReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport_ByCompany(null, 0, int.MaxValue , companyId, currentyear, siteId);
		}
		


        //Added By Sayani
        public DataSet ComplianceReport_ByCompany_Embeded(System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
        {
            return ComplianceReport_ByCompany_Embeded(null, 0, int.MaxValue, companyId, currentyear, siteId);
        }

        public abstract DataSet ComplianceReport_ByCompany_Embeded(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId);
		
        public DataSet ComplianceReport_ByCompany_NonEmbeded(System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
        {
            return ComplianceReport_ByCompany_NonEmbeded(null, 0, int.MaxValue, companyId, currentyear, siteId);
        }

        public abstract DataSet ComplianceReport_ByCompany_NonEmbeded(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId);
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(int start, int pageLength, System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport_ByCompany(null, start, pageLength , companyId, currentyear, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId)
		{
			return ComplianceReport_ByCompany(transactionManager, 0, int.MaxValue , companyId, currentyear, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_CSA_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="currentyear"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? currentyear, System.Int32? siteId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Csa&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Csa&gt;"/></returns>
		public static TList<Csa> Fill(IDataReader reader, TList<Csa> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Csa c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Csa")
					.Append("|").Append((System.Int32)reader[((int)CsaColumn.CsaId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Csa>(
					key.ToString(), // EntityTrackingKey
					"Csa",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Csa();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsaId = (System.Int32)reader[((int)CsaColumn.CsaId - 1)];
					c.CompanyId = (System.Int32)reader[((int)CsaColumn.CompanyId - 1)];
					c.SiteId = (System.Int32)reader[((int)CsaColumn.SiteId - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)CsaColumn.CreatedByUserId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)CsaColumn.ModifiedByUserId - 1)];
					c.DateAdded = (System.DateTime)reader[((int)CsaColumn.DateAdded - 1)];
					c.DateModified = (System.DateTime)reader[((int)CsaColumn.DateModified - 1)];
					c.QtrId = (System.Int32)reader[((int)CsaColumn.QtrId - 1)];
					c.Year = (System.Int64)reader[((int)CsaColumn.Year - 1)];
					c.TotalPossibleScore = (reader.IsDBNull(((int)CsaColumn.TotalPossibleScore - 1)))?null:(System.Int32?)reader[((int)CsaColumn.TotalPossibleScore - 1)];
					c.TotalActualScore = (reader.IsDBNull(((int)CsaColumn.TotalActualScore - 1)))?null:(System.Int32?)reader[((int)CsaColumn.TotalActualScore - 1)];
					c.TotalRating = (reader.IsDBNull(((int)CsaColumn.TotalRating - 1)))?null:(System.Int32?)reader[((int)CsaColumn.TotalRating - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Csa"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Csa"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Csa entity)
		{
			if (!reader.Read()) return;
			
			entity.CsaId = (System.Int32)reader[((int)CsaColumn.CsaId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CsaColumn.CompanyId - 1)];
			entity.SiteId = (System.Int32)reader[((int)CsaColumn.SiteId - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)CsaColumn.CreatedByUserId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CsaColumn.ModifiedByUserId - 1)];
			entity.DateAdded = (System.DateTime)reader[((int)CsaColumn.DateAdded - 1)];
			entity.DateModified = (System.DateTime)reader[((int)CsaColumn.DateModified - 1)];
			entity.QtrId = (System.Int32)reader[((int)CsaColumn.QtrId - 1)];
			entity.Year = (System.Int64)reader[((int)CsaColumn.Year - 1)];
			entity.TotalPossibleScore = (reader.IsDBNull(((int)CsaColumn.TotalPossibleScore - 1)))?null:(System.Int32?)reader[((int)CsaColumn.TotalPossibleScore - 1)];
			entity.TotalActualScore = (reader.IsDBNull(((int)CsaColumn.TotalActualScore - 1)))?null:(System.Int32?)reader[((int)CsaColumn.TotalActualScore - 1)];
			entity.TotalRating = (reader.IsDBNull(((int)CsaColumn.TotalRating - 1)))?null:(System.Int32?)reader[((int)CsaColumn.TotalRating - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Csa"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Csa"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Csa entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsaId = (System.Int32)dataRow["CSAId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.DateAdded = (System.DateTime)dataRow["DateAdded"];
			entity.DateModified = (System.DateTime)dataRow["DateModified"];
			entity.QtrId = (System.Int32)dataRow["QtrId"];
			entity.Year = (System.Int64)dataRow["Year"];
			entity.TotalPossibleScore = Convert.IsDBNull(dataRow["TotalPossibleScore"]) ? null : (System.Int32?)dataRow["TotalPossibleScore"];
			entity.TotalActualScore = Convert.IsDBNull(dataRow["TotalActualScore"]) ? null : (System.Int32?)dataRow["TotalActualScore"];
			entity.TotalRating = Convert.IsDBNull(dataRow["TotalRating"]) ? null : (System.Int32?)dataRow["TotalRating"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Csa"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Csa Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Csa entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCsaId methods when available
			
			#region CsaAnswersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CsaAnswers>|CsaAnswersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsaAnswersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsaAnswersCollection = DataRepository.CsaAnswersProvider.GetByCsaId(transactionManager, entity.CsaId);

				if (deep && entity.CsaAnswersCollection.Count > 0)
				{
					deepHandles.Add("CsaAnswersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CsaAnswers>) DataRepository.CsaAnswersProvider.DeepLoad,
						new object[] { transactionManager, entity.CsaAnswersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Csa object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Csa instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Csa Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Csa entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CsaAnswers>
				if (CanDeepSave(entity.CsaAnswersCollection, "List<CsaAnswers>|CsaAnswersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CsaAnswers child in entity.CsaAnswersCollection)
					{
						if(child.CsaIdSource != null)
						{
							child.CsaId = child.CsaIdSource.CsaId;
						}
						else
						{
							child.CsaId = entity.CsaId;
						}

					}

					if (entity.CsaAnswersCollection.Count > 0 || entity.CsaAnswersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CsaAnswersProvider.Save(transactionManager, entity.CsaAnswersCollection);
						
						deepHandles.Add("CsaAnswersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CsaAnswers >) DataRepository.CsaAnswersProvider.DeepSave,
							new object[] { transactionManager, entity.CsaAnswersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsaChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Csa</c>
	///</summary>
	public enum CsaChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
	
		///<summary>
		/// Collection of <c>Csa</c> as OneToMany for CsaAnswersCollection
		///</summary>
		[ChildEntityType(typeof(TList<CsaAnswers>))]
		CsaAnswersCollection,
	}
	
	#endregion CsaChildEntityTypes
	
	#region CsaFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsaColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaFilterBuilder : SqlFilterBuilder<CsaColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaFilterBuilder class.
		/// </summary>
		public CsaFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaFilterBuilder
	
	#region CsaParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsaColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaParameterBuilder : ParameterizedSqlFilterBuilder<CsaColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaParameterBuilder class.
		/// </summary>
		public CsaParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaParameterBuilder
	
	#region CsaSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsaColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Csa"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsaSortBuilder : SqlSortBuilder<CsaColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaSqlSortBuilder class.
		/// </summary>
		public CsaSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsaSortBuilder
	
} // end namespace
