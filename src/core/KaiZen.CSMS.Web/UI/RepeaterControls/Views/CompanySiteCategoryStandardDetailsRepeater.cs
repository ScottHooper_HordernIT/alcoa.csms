﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>CompanySiteCategoryStandardDetailsRepeater</c>
    /// </summary>
	public class CompanySiteCategoryStandardDetailsRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanySiteCategoryStandardDetailsRepeaterDesigner"/> class.
        /// </summary>
		public CompanySiteCategoryStandardDetailsRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is CompanySiteCategoryStandardDetailsRepeater))
			{ 
				throw new ArgumentException("Component is not a CompanySiteCategoryStandardDetailsRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			CompanySiteCategoryStandardDetailsRepeater z = (CompanySiteCategoryStandardDetailsRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="CompanySiteCategoryStandardDetailsRepeater"/> Type.
    /// </summary>
	[Designer(typeof(CompanySiteCategoryStandardDetailsRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:CompanySiteCategoryStandardDetailsRepeater runat=\"server\"></{0}:CompanySiteCategoryStandardDetailsRepeater>")]
	public class CompanySiteCategoryStandardDetailsRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanySiteCategoryStandardDetailsRepeater"/> class.
        /// </summary>
		public CompanySiteCategoryStandardDetailsRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanySiteCategoryStandardDetailsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanySiteCategoryStandardDetailsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(CompanySiteCategoryStandardDetailsItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanySiteCategoryStandardDetailsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanySiteCategoryStandardDetailsItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.CompanySiteCategoryStandardDetails entity = o as KaiZen.CSMS.Entities.CompanySiteCategoryStandardDetails;
						CompanySiteCategoryStandardDetailsItem container = new CompanySiteCategoryStandardDetailsItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class CompanySiteCategoryStandardDetailsItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.CompanySiteCategoryStandardDetails _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanySiteCategoryStandardDetailsItem"/> class.
        /// </summary>
		public CompanySiteCategoryStandardDetailsItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanySiteCategoryStandardDetailsItem"/> class.
        /// </summary>
		public CompanySiteCategoryStandardDetailsItem(KaiZen.CSMS.Entities.CompanySiteCategoryStandardDetails entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanySiteCategoryStandardId
        /// </summary>
        /// <value>The CompanySiteCategoryStandardId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanySiteCategoryStandardId
		{
			get { return _entity.CompanySiteCategoryStandardId; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the SiteId
        /// </summary>
        /// <value>The SiteId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 SiteId
		{
			get { return _entity.SiteId; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryId
        /// </summary>
        /// <value>The CompanySiteCategoryId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CompanySiteCategoryId
		{
			get { return _entity.CompanySiteCategoryId; }
		}
        /// <summary>
        /// Gets the LocationSpaUserId
        /// </summary>
        /// <value>The LocationSpaUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? LocationSpaUserId
		{
			get { return _entity.LocationSpaUserId; }
		}
        /// <summary>
        /// Gets the LocationSponsorUserId
        /// </summary>
        /// <value>The LocationSponsorUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? LocationSponsorUserId
		{
			get { return _entity.LocationSponsorUserId; }
		}
        /// <summary>
        /// Gets the ArpUserId
        /// </summary>
        /// <value>The ArpUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ArpUserId
		{
			get { return _entity.ArpUserId; }
		}
        /// <summary>
        /// Gets the Area
        /// </summary>
        /// <value>The Area.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Area
		{
			get { return _entity.Area; }
		}
        /// <summary>
        /// Gets the Approved
        /// </summary>
        /// <value>The Approved.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Approved
		{
			get { return _entity.Approved; }
		}
        /// <summary>
        /// Gets the ApprovedByUserId
        /// </summary>
        /// <value>The ApprovedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ApprovedByUserId
		{
			get { return _entity.ApprovedByUserId; }
		}
        /// <summary>
        /// Gets the ApprovedDate
        /// </summary>
        /// <value>The ApprovedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? ApprovedDate
		{
			get { return _entity.ApprovedDate; }
		}
        /// <summary>
        /// Gets the ModifiedByUserId
        /// </summary>
        /// <value>The ModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedByUserId
		{
			get { return _entity.ModifiedByUserId; }
		}
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the SiteName
        /// </summary>
        /// <value>The SiteName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SiteName
		{
			get { return _entity.SiteName; }
		}
        /// <summary>
        /// Gets the SiteAbbrev
        /// </summary>
        /// <value>The SiteAbbrev.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SiteAbbrev
		{
			get { return _entity.SiteAbbrev; }
		}
        /// <summary>
        /// Gets the CategoryName
        /// </summary>
        /// <value>The CategoryName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CategoryName
		{
			get { return _entity.CategoryName; }
		}
        /// <summary>
        /// Gets the CategoryDesc
        /// </summary>
        /// <value>The CategoryDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CategoryDesc
		{
			get { return _entity.CategoryDesc; }
		}
        /// <summary>
        /// Gets the ApprovalText
        /// </summary>
        /// <value>The ApprovalText.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ApprovalText
		{
			get { return _entity.ApprovalText; }
		}
        /// <summary>
        /// Gets the LocationSpaUserFullName
        /// </summary>
        /// <value>The LocationSpaUserFullName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String LocationSpaUserFullName
		{
			get { return _entity.LocationSpaUserFullName; }
		}
        /// <summary>
        /// Gets the LocationSponsorUserFullName
        /// </summary>
        /// <value>The LocationSponsorUserFullName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String LocationSponsorUserFullName
		{
			get { return _entity.LocationSponsorUserFullName; }
		}
        /// <summary>
        /// Gets the ArpUserFullName
        /// </summary>
        /// <value>The ArpUserFullName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpUserFullName
		{
			get { return _entity.ArpUserFullName; }
		}
        /// <summary>
        /// Gets the CrpUserFullNames
        /// </summary>
        /// <value>The CrpUserFullNames.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CrpUserFullNames
		{
			get { return _entity.CrpUserFullNames; }
		}
        /// <summary>
        /// Gets the ApprovedByUserFullName
        /// </summary>
        /// <value>The ApprovedByUserFullName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ApprovedByUserFullName
		{
			get { return _entity.ApprovedByUserFullName; }
		}

	}
}
