﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CustomReportingLayoutsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CustomReportingLayoutsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CustomReportingLayouts, KaiZen.CSMS.Entities.CustomReportingLayoutsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CustomReportingLayoutsKey key)
		{
			return Delete(transactionManager, key.ReportId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_reportId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _reportId)
		{
			return Delete(null, _reportId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reportId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _reportId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CustomReportingLayouts Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CustomReportingLayoutsKey key, int start, int pageLength)
		{
			return GetByReportId(transactionManager, key.ReportId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CustomReportingLayouts index.
		/// </summary>
		/// <param name="_reportId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByReportId(System.Int32 _reportId)
		{
			int count = -1;
			return GetByReportId(null,_reportId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CustomReportingLayouts index.
		/// </summary>
		/// <param name="_reportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByReportId(System.Int32 _reportId, int start, int pageLength)
		{
			int count = -1;
			return GetByReportId(null, _reportId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CustomReportingLayouts index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reportId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByReportId(TransactionManager transactionManager, System.Int32 _reportId)
		{
			int count = -1;
			return GetByReportId(transactionManager, _reportId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CustomReportingLayouts index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByReportId(TransactionManager transactionManager, System.Int32 _reportId, int start, int pageLength)
		{
			int count = -1;
			return GetByReportId(transactionManager, _reportId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CustomReportingLayouts index.
		/// </summary>
		/// <param name="_reportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByReportId(System.Int32 _reportId, int start, int pageLength, out int count)
		{
			return GetByReportId(null, _reportId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CustomReportingLayouts index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CustomReportingLayouts GetByReportId(TransactionManager transactionManager, System.Int32 _reportId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CustomReportingLayouts index.
		/// </summary>
		/// <param name="_area"></param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="_reportName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByAreaModifiedByUserIdReportName(System.String _area, System.Int32 _modifiedByUserId, System.String _reportName)
		{
			int count = -1;
			return GetByAreaModifiedByUserIdReportName(null,_area, _modifiedByUserId, _reportName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts index.
		/// </summary>
		/// <param name="_area"></param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="_reportName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByAreaModifiedByUserIdReportName(System.String _area, System.Int32 _modifiedByUserId, System.String _reportName, int start, int pageLength)
		{
			int count = -1;
			return GetByAreaModifiedByUserIdReportName(null, _area, _modifiedByUserId, _reportName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_area"></param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="_reportName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByAreaModifiedByUserIdReportName(TransactionManager transactionManager, System.String _area, System.Int32 _modifiedByUserId, System.String _reportName)
		{
			int count = -1;
			return GetByAreaModifiedByUserIdReportName(transactionManager, _area, _modifiedByUserId, _reportName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_area"></param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="_reportName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByAreaModifiedByUserIdReportName(TransactionManager transactionManager, System.String _area, System.Int32 _modifiedByUserId, System.String _reportName, int start, int pageLength)
		{
			int count = -1;
			return GetByAreaModifiedByUserIdReportName(transactionManager, _area, _modifiedByUserId, _reportName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts index.
		/// </summary>
		/// <param name="_area"></param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="_reportName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public KaiZen.CSMS.Entities.CustomReportingLayouts GetByAreaModifiedByUserIdReportName(System.String _area, System.Int32 _modifiedByUserId, System.String _reportName, int start, int pageLength, out int count)
		{
			return GetByAreaModifiedByUserIdReportName(null, _area, _modifiedByUserId, _reportName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_area"></param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="_reportName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CustomReportingLayouts GetByAreaModifiedByUserIdReportName(TransactionManager transactionManager, System.String _area, System.Int32 _modifiedByUserId, System.String _reportName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CustomReportingLayouts_ModifiedByUserId index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomReportingLayouts&gt;"/> class.</returns>
		public TList<CustomReportingLayouts> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(null,_modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts_ModifiedByUserId index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomReportingLayouts&gt;"/> class.</returns>
		public TList<CustomReportingLayouts> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts_ModifiedByUserId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomReportingLayouts&gt;"/> class.</returns>
		public TList<CustomReportingLayouts> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts_ModifiedByUserId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomReportingLayouts&gt;"/> class.</returns>
		public TList<CustomReportingLayouts> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts_ModifiedByUserId index.
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomReportingLayouts&gt;"/> class.</returns>
		public TList<CustomReportingLayouts> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength, out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CustomReportingLayouts_ModifiedByUserId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomReportingLayouts&gt;"/> class.</returns>
		public abstract TList<CustomReportingLayouts> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CustomReportingLayouts&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CustomReportingLayouts&gt;"/></returns>
		public static TList<CustomReportingLayouts> Fill(IDataReader reader, TList<CustomReportingLayouts> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CustomReportingLayouts c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CustomReportingLayouts")
					.Append("|").Append((System.Int32)reader[((int)CustomReportingLayoutsColumn.ReportId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CustomReportingLayouts>(
					key.ToString(), // EntityTrackingKey
					"CustomReportingLayouts",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CustomReportingLayouts();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ReportId = (System.Int32)reader[((int)CustomReportingLayoutsColumn.ReportId - 1)];
					c.Area = (System.String)reader[((int)CustomReportingLayoutsColumn.Area - 1)];
					c.ReportName = (System.String)reader[((int)CustomReportingLayoutsColumn.ReportName - 1)];
					c.ReportDescription = (reader.IsDBNull(((int)CustomReportingLayoutsColumn.ReportDescription - 1)))?null:(System.String)reader[((int)CustomReportingLayoutsColumn.ReportDescription - 1)];
					c.ReportLayout = (System.String)reader[((int)CustomReportingLayoutsColumn.ReportLayout - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)CustomReportingLayoutsColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)CustomReportingLayoutsColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CustomReportingLayouts entity)
		{
			if (!reader.Read()) return;
			
			entity.ReportId = (System.Int32)reader[((int)CustomReportingLayoutsColumn.ReportId - 1)];
			entity.Area = (System.String)reader[((int)CustomReportingLayoutsColumn.Area - 1)];
			entity.ReportName = (System.String)reader[((int)CustomReportingLayoutsColumn.ReportName - 1)];
			entity.ReportDescription = (reader.IsDBNull(((int)CustomReportingLayoutsColumn.ReportDescription - 1)))?null:(System.String)reader[((int)CustomReportingLayoutsColumn.ReportDescription - 1)];
			entity.ReportLayout = (System.String)reader[((int)CustomReportingLayoutsColumn.ReportLayout - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CustomReportingLayoutsColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)CustomReportingLayoutsColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CustomReportingLayouts entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ReportId = (System.Int32)dataRow["ReportId"];
			entity.Area = (System.String)dataRow["Area"];
			entity.ReportName = (System.String)dataRow["ReportName"];
			entity.ReportDescription = Convert.IsDBNull(dataRow["ReportDescription"]) ? null : (System.String)dataRow["ReportDescription"];
			entity.ReportLayout = (System.String)dataRow["ReportLayout"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CustomReportingLayouts"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CustomReportingLayouts Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CustomReportingLayouts entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CustomReportingLayouts object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CustomReportingLayouts instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CustomReportingLayouts Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CustomReportingLayouts entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CustomReportingLayoutsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CustomReportingLayouts</c>
	///</summary>
	public enum CustomReportingLayoutsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion CustomReportingLayoutsChildEntityTypes
	
	#region CustomReportingLayoutsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CustomReportingLayoutsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsFilterBuilder : SqlFilterBuilder<CustomReportingLayoutsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsFilterBuilder class.
		/// </summary>
		public CustomReportingLayoutsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomReportingLayoutsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomReportingLayoutsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomReportingLayoutsFilterBuilder
	
	#region CustomReportingLayoutsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CustomReportingLayoutsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsParameterBuilder : ParameterizedSqlFilterBuilder<CustomReportingLayoutsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsParameterBuilder class.
		/// </summary>
		public CustomReportingLayoutsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomReportingLayoutsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomReportingLayoutsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomReportingLayoutsParameterBuilder
	
	#region CustomReportingLayoutsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CustomReportingLayoutsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CustomReportingLayoutsSortBuilder : SqlSortBuilder<CustomReportingLayoutsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsSqlSortBuilder class.
		/// </summary>
		public CustomReportingLayoutsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CustomReportingLayoutsSortBuilder
	
} // end namespace
