﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireServicesSelectedAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireServicesSelectedAuditDataSourceDesigner))]
	public class QuestionnaireServicesSelectedAuditDataSource : ProviderDataSource<QuestionnaireServicesSelectedAudit, QuestionnaireServicesSelectedAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditDataSource class.
		/// </summary>
		public QuestionnaireServicesSelectedAuditDataSource() : base(new QuestionnaireServicesSelectedAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireServicesSelectedAuditDataSourceView used by the QuestionnaireServicesSelectedAuditDataSource.
		/// </summary>
		protected QuestionnaireServicesSelectedAuditDataSourceView QuestionnaireServicesSelectedAuditView
		{
			get { return ( View as QuestionnaireServicesSelectedAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireServicesSelectedAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireServicesSelectedAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireServicesSelectedAuditSelectMethod selectMethod = QuestionnaireServicesSelectedAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireServicesSelectedAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireServicesSelectedAuditDataSourceView class that is to be
		/// used by the QuestionnaireServicesSelectedAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireServicesSelectedAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireServicesSelectedAudit, QuestionnaireServicesSelectedAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireServicesSelectedAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireServicesSelectedAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireServicesSelectedAuditDataSourceView : ProviderDataSourceView<QuestionnaireServicesSelectedAudit, QuestionnaireServicesSelectedAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireServicesSelectedAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireServicesSelectedAuditDataSourceView(QuestionnaireServicesSelectedAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireServicesSelectedAuditDataSource QuestionnaireServicesSelectedAuditOwner
		{
			get { return Owner as QuestionnaireServicesSelectedAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireServicesSelectedAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireServicesSelectedAuditOwner.SelectMethod; }
			set { QuestionnaireServicesSelectedAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireServicesSelectedAuditService QuestionnaireServicesSelectedAuditProvider
		{
			get { return Provider as QuestionnaireServicesSelectedAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireServicesSelectedAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireServicesSelectedAudit> results = null;
			QuestionnaireServicesSelectedAudit item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32? _questionnaireServiceId_nullable;
			System.Int32? _questionnaireId_nullable;
			System.Int32? _modifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireServicesSelectedAuditSelectMethod.Get:
					QuestionnaireServicesSelectedAuditKey entityKey  = new QuestionnaireServicesSelectedAuditKey();
					entityKey.Load(values);
					item = QuestionnaireServicesSelectedAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireServicesSelectedAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireServicesSelectedAuditSelectMethod.GetAll:
                    results = QuestionnaireServicesSelectedAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireServicesSelectedAuditSelectMethod.GetPaged:
					results = QuestionnaireServicesSelectedAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireServicesSelectedAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireServicesSelectedAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireServicesSelectedAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireServicesSelectedAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireServicesSelectedAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireServicesSelectedAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireServicesSelectedAuditSelectMethod.GetByQuestionnaireServiceIdQuestionnaireId:
					_questionnaireServiceId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnaireServiceId"], typeof(System.Int32?));
					_questionnaireId_nullable = (System.Int32?) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32?));
					results = QuestionnaireServicesSelectedAuditProvider.GetByQuestionnaireServiceIdQuestionnaireId(_questionnaireServiceId_nullable, _questionnaireId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireServicesSelectedAuditSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = QuestionnaireServicesSelectedAuditProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireServicesSelectedAuditSelectMethod.Get || SelectMethod == QuestionnaireServicesSelectedAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireServicesSelectedAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireServicesSelectedAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireServicesSelectedAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireServicesSelectedAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireServicesSelectedAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireServicesSelectedAuditDataSource class.
	/// </summary>
	public class QuestionnaireServicesSelectedAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireServicesSelectedAudit, QuestionnaireServicesSelectedAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireServicesSelectedAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireServicesSelectedAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireServicesSelectedAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireServicesSelectedAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireServicesSelectedAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireServicesSelectedAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireServicesSelectedAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireServicesSelectedAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireServicesSelectedAuditDataSourceActionList(QuestionnaireServicesSelectedAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireServicesSelectedAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireServicesSelectedAuditDataSourceActionList
	
	#endregion QuestionnaireServicesSelectedAuditDataSourceDesigner
	
	#region QuestionnaireServicesSelectedAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireServicesSelectedAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireServicesSelectedAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByQuestionnaireServiceIdQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireServiceIdQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireServicesSelectedAuditSelectMethod

	#region QuestionnaireServicesSelectedAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelectedAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedAuditFilter : SqlFilter<QuestionnaireServicesSelectedAuditColumn>
	{
	}
	
	#endregion QuestionnaireServicesSelectedAuditFilter

	#region QuestionnaireServicesSelectedAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelectedAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireServicesSelectedAuditColumn>
	{
	}
	
	#endregion QuestionnaireServicesSelectedAuditExpressionBuilder	

	#region QuestionnaireServicesSelectedAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireServicesSelectedAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelectedAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedAuditProperty : ChildEntityProperty<QuestionnaireServicesSelectedAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireServicesSelectedAuditProperty
}

