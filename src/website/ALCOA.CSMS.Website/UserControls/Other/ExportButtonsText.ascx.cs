using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;

using System.IO;

/// <summary>
/// Last Updated: 11/09/2009
/// Description: User control with 3 buttons to export an aspxgridview to rtf(doc)/pdf/xls
/// </summary>
public partial class UserControls_Tiny_ExportButtonsText : System.Web.UI.UserControl
{
    bool forceDownload = true;

    protected void imgBtnWord_Click(object sender, ImageClickEventArgs e) //export to word document format 
    {
        Response.Clear();
        RtfExportOptions opt = new RtfExportOptions();
        opt.ExportWatermarks = true;
        gridExporter.WriteRtfToResponse(opt);
        Response.Close();
    }
    protected void imgBtnPdf_Click(object sender, ImageClickEventArgs e) //export to adobe acrobat pdf format 
    {
        Response.Clear();
        PdfExportOptions opt = new PdfExportOptions();
        opt.Compressed = true;
        opt.DocumentOptions.Title = gridExporter.FileName;
        opt.DocumentOptions.Author = "Alcoa Inc.";
        gridExporter.WritePdfToResponse(opt);
        Response.Close();
    }
    protected void response_write(MemoryStream iStream, string FileName)
    {
        // Buffer to read 10K bytes in chunk:
        byte[] buffer = new Byte[10000];

        // Length of the file:
        int length;

        // Total bytes to read:
        long dataToRead;

        try
        {
            dataToRead = iStream.Length;
            if (forceDownload)
            {
                Response.ContentType = "application/download";
            }
            else
            {
                Response.ContentType = "application/octet-stream";
            }

            string ContentDisposition = String.Format(@"attachment; filename={0}", Server.UrlEncode(FileName));
            String userAgent = Request.Headers.Get("User-Agent");
            if (userAgent.Contains("MSIE 7.0"))
            {
                ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
            }
            else
            {
                ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
            }
            Response.AppendHeader("Content-Disposition", ContentDisposition);

            // Read the bytes.
            while (dataToRead > 0)
            {
                // Verify that the client is connected.
                if (Response.IsClientConnected)
                {
                    // Read the data in buffer.
                    length = iStream.Read(buffer, 0, 10000);

                    // Write the data to the current output stream.
                    Response.OutputStream.Write(buffer, 0, length);

                    // Flush the data to the HTML output.
                    Response.Flush();

                    buffer = new Byte[10000];
                    dataToRead = dataToRead - length;
                }
                else
                {
                    //prevent infinite loop if user disconnects
                    dataToRead = -1;
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Error: " + ex.Message);
        }
        finally
        {
            iStream.Close();
            Response.Close();
        }
    }
    protected void imgBtnExcel_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
    {
        //using (MemoryStream stream = new MemoryStream())
        //{
       
            Response.Clear();
            XlsExportOptions opt = new XlsExportOptions();
            opt.SheetName = gridExporter.FileName;
            opt.ShowGridLines = true;
            //opt.TextExportMode = TextExportMode.Value;
            opt.TextExportMode = TextExportMode.Text;
            //opt.UseNativeFormat = true;
            gridExporter.WriteXlsToResponse(opt);
            Response.Close();
        //    gridExporter.WriteXls(stream, opt);
        //    response_write(stream, gridExporter.FileName);
        //}
    }

    //protected void OnButtonClick(object sender, EventArgs e)
    //{
    //    Response.Clear();
    //    XlsExportOptions opt = new XlsExportOptions();
    //    opt.SheetName = gridExporter.FileName;
    //    opt.ShowGridLines = true;
    //    opt.TextExportMode = TextExportMode.Value;
    //    //opt.UseNativeFormat = true;
    //    gridExporter.WriteXlsToResponse(opt);
    //    Response.Close();
    //}
}
