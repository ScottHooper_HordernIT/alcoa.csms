<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_EbiContractorsOnSite" Codebehind="EbiContractorsOnSite.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register src="../SqExemptionList.ascx" tagname="SqExemptionList" tagprefix="uc2" %>
<script language="javascript" type="text/javascript">
    function popUpClosed() {
        window.location.reload();
    }
</script>
<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" style="height: 16px">
            <span class="bodycopy"><span class="title">Reports</span><br />
                <span class="date">Contractors on site (EBI)</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
    <tr>
        <td class="pageName" style="height: 17px">
            <dx:ASPxLabel runat="server" ID="lblLive" Text="nb. Data Shows Contractors On Site Today (Since Midnight until 9am).">
            </dx:ASPxLabel>
            &nbsp;<dx:ASPxHyperLink ID="hlLive" runat="server" Text="Click Here to show the Live Version"
                NavigateUrl="EbiContractorsOnSite.aspx?q=live">
            </dx:ASPxHyperLink>
            <br />
        </td>
    </tr>
    <tr>
        <td class="pageName" style="height: 17px; text-align: right">
            <dx:ASPxButton ID="btnReset" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" OnClick="btnReset_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                Text="Reset Layout" ToolTip="Customised the grid layout below and everything's confusing? Click this button to reset to the default view.">
            </dx:ASPxButton>
        </td>
    </tr>
    <tr>
        <td style="width: 874px; padding-top: 2px; text-align: center" class="pageName" align="right">
            <div align="right">
                <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                    Customize</a>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                OnHtmlRowCreated="grid_RowCreated" OnCellEditorInitialize="grid_CellEditorInitialize"
                Width="900px">
                <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Columns>
                    <dx:GridViewDataTextColumn FieldName="Site" VisibleIndex="0" Width="78px" GroupIndex="1"
                        SortIndex="0" SortOrder="Ascending">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Company Name (EBI)" FieldName="CompanyName" VisibleIndex="1"
                        SortIndex="2" SortOrder="Ascending" Width="113px">
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="hlebi" runat="server" Text="CompanyName" NavigateUrl="" Visible="false"></asp:HyperLink><asp:Label
                                            ID="lblebi" runat="server" Text="CompanyName" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle Wrap="true" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Company Name (CSMS)" FieldName="CompanyName-CSMS"
                        VisibleIndex="2" Width="108px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle Wrap="true" />
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="hlq" runat="server" Text="CompanyName" NavigateUrl="" Visible="false"></asp:HyperLink><asp:Label
                                            ID="lbq" runat="server" Text="CompanyName" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Type" FieldName="Type" VisibleIndex="3" Width="105px">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Valid" VisibleIndex="4" Width="70px" SortIndex="1"
                        SortOrder="Ascending">
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Image ID="changeImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                            GenerateEmptyAlternateText="True" />
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Action Required" FieldName="Valid" VisibleIndex="5" Width="170px"
                        SortOrder="Ascending">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Procurement Contact" FieldName="ProcurementContact"
                        Visible="false" Width="150px" ShowInCustomizationForm="true">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" Wrap="true">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="H&S Assessor" FieldName="HSAssessor"
                        Visible="false" Width="150px" ShowInCustomizationForm="true">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" Wrap="true">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Approved For Site" FieldName="ApprovedOnSite"
                        VisibleIndex="5" Width="81px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Center" Wrap="true">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Validity" VisibleIndex="6" Width="83px" Visible="false">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Company SQ Status" FieldName="CompanySQStatus"
                        VisibleIndex="7" Width="160px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="SQ Valid Till" FieldName="SqValidTill" VisibleIndex="8"
                        Width="80px">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="QuestionnaireId" Visible="False" ShowInCustomizationForm="false">
                        <HeaderStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                </Columns>
                <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
                <SettingsPager AlwaysShowPager="True" Mode="ShowAllRecords">
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <%--<SettingsBehavior ColumnResizeMode="NextColumn" />--%>
                <SettingsCookies CookiesID="gridEbi" Version="0.5" Enabled="True" />
            </dx:ASPxGridView>
        </td>
    </tr>
    <tr align="right">
        <td style="height: 35px; text-align: right; text-align: -moz-right;"
            align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
    <tr>
        <td>
        <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                        style="width: 100%; border-collapse: collapse; empty-cells: show">
                        <tbody>
                            <tr>
                                <td class="dxgvHeader_Office2003_Blue" colspan="2" style="border-top-width: 0px;
                                    font-weight: bold; border-left-width: 0px; white-space: normal; height: 13px;
                                    text-align: left;">
                                    <span style="color: maroon">What does all these icons mean? (LEGEND)</span>
                                </td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlGreen.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    1) Yes - Safety Qualification Current / Access to Site Granted.</td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlYellow.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    2) Yes - Safety Qualification Current (expiring in 60 days) / Access to Site Granted.<br />
                                    3) Yes - Safety Qualification Current (currently Re-Qualifying) / Access to Site Granted.</td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlRed.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    4) No - Current Safety Qualification not found / Access to Site Not Granted.<br />
                                    5) No - Safety Qualification Expired / Access to Site Not Granted.<br />
                                    6) No - Access to Site Not Granted.</td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/redcross.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    7) No - Safety Qualification not found.<br />
                                    8) No - Company information could not be found (Check that Company exists in CSMS).</td>
                            </tr>
                            <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 30px; height: 20px; text-align: center">
                                    <img src="Images/TrafficLights/tlPurple.gif" />
                                </td>
                                <td class="dxgv" style="width: 972px; height: 20px; text-align: left">
                                    9) This company has a valid Safety Qualification exemption for this site.</td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
        </td>
    </tr>
    <tr align="left">
        <td class="pageName" style="height: 16px">
            <span class="bodycopy"><span class="date">
            Contractors granted exemption from completing Safety Qualification (Granted Authority to Access Site)</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
    <tr>
        <td>
            <uc2:SqExemptionList ID="SqExemptionList1" runat="server" />
        </td>
    </tr>
</table>
<br />