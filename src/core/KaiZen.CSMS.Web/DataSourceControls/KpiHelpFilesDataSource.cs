﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(KpiHelpFilesDataSourceDesigner))]
	public class KpiHelpFilesDataSource : ProviderDataSource<KpiHelpFiles, KpiHelpFilesKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultDataSource class.
		/// </summary>
		public KpiHelpFilesDataSource() : base(new KpiHelpFilesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultDataSourceView used by the FileVaultDataSource.
		/// </summary>
		protected KpiHelpFilesDataSourceView KpiHelpFilesView
		{
			get { return ( View as KpiHelpFilesDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiHelpFilesDataSource control invokes to retrieve data.
		/// </summary>
		public KpiHelpFilesSelectMethod SelectMethod
		{
			get
			{
				KpiHelpFilesSelectMethod selectMethod = KpiHelpFilesSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiHelpFilesSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiHelpFilesDataSourceView class that is to be
		/// used by the KpiHelpFilesDataSource.
		/// </summary>
		/// <returns>An instance of the KpiHelpFilesDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiHelpFiles, KpiHelpFilesKey> GetNewDataSourceView()
		{
			return new KpiHelpFilesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiHelpFilesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiHelpFilesDataSourceView : ProviderDataSourceView<KpiHelpFiles, KpiHelpFilesKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiHelpFilesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiHelpFilesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiHelpFilesDataSourceView(KpiHelpFilesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiHelpFilesDataSource KpiHelpFilesOwner
		{
			get { return Owner as KpiHelpFilesDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal KpiHelpFilesSelectMethod SelectMethod
		{
			get { return KpiHelpFilesOwner.SelectMethod; }
			set { KpiHelpFilesOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiHelpFilesService KpiHelpFilesProvider
		{
			get { return Provider as KpiHelpFilesService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<KpiHelpFiles> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<KpiHelpFiles> results = null;
			KpiHelpFiles item;
			count = 0;

            System.Int32 _kpiHelpFileId;
            System.String _kpiHelpCaption;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case KpiHelpFilesSelectMethod.Get:
					KpiHelpFilesKey entityKey  = new KpiHelpFilesKey();
					entityKey.Load(values);
					item = KpiHelpFilesProvider.Get(entityKey);
					results = new TList<KpiHelpFiles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiHelpFilesSelectMethod.GetAll:
                    results = KpiHelpFilesProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiHelpFilesSelectMethod.GetPaged:
					results = KpiHelpFilesProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case KpiHelpFilesSelectMethod.Find:
					if ( FilterParameters != null )
						results = KpiHelpFilesProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = KpiHelpFilesProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case KpiHelpFilesSelectMethod.GetByKpiHelpFileId:
					_kpiHelpFileId = ( values["KpiHelpFileId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiHelpFileId"], typeof(System.Int32)) : (int)0;
					item = KpiHelpFilesProvider.GetByKpiHelpFileId(_kpiHelpFileId);
					results = new TList<KpiHelpFiles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
                case KpiHelpFilesSelectMethod.GetByKpiHelpCaption:
                    _kpiHelpCaption = (values["KpiHelpCaption"] != null) ? (System.String)EntityUtil.ChangeType(values["KpiHelpCaption"], typeof(System.String)) : string.Empty;
                    results = KpiHelpFilesProvider.GetByKpiHelpCaption(_kpiHelpCaption, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case KpiHelpFilesSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = KpiHelpFilesProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
            if (SelectMethod == KpiHelpFilesSelectMethod.Get || SelectMethod == KpiHelpFilesSelectMethod.GetByKpiHelpFileId)
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
                KpiHelpFiles entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
                    KpiHelpFilesProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
        internal override void DeepLoad(TList<KpiHelpFiles> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
            KpiHelpFilesProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileVaultDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultDataSource class.
	/// </summary>
	public class KpiHelpFilesDataSourceDesigner : ProviderDataSourceDesigner<KpiHelpFiles, KpiHelpFilesKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileVaultDataSourceDesigner class.
		/// </summary>
        public KpiHelpFilesDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
        public KpiHelpFilesSelectMethod SelectMethod
		{
            get { return ((KpiHelpFilesDataSource)DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
                actions.Add(new KpiHelpFilesDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileVaultDataSourceActionList

	/// <summary>
	/// Supports the FileVaultDataSourceDesigner class.
	/// </summary>
	internal class KpiHelpFilesDataSourceActionList : DesignerActionList
	{
		private KpiHelpFilesDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileVaultDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
        public KpiHelpFilesDataSourceActionList(KpiHelpFilesDataSourceDesigner designer)
            : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
        public KpiHelpFilesSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileVaultDataSourceActionList
	
	#endregion FileVaultDataSourceDesigner
	
	#region FileVaultSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileVaultDataSource.SelectMethod property.
	/// </summary>
    public enum KpiHelpFilesSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileVaultId method.
		/// </summary>
        GetByKpiHelpFileId,
		/// <summary>
		/// Represents the GetByFileVaultCategoryId method.
		/// </summary>
        GetByKpiHelpCaption,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion FileVaultSelectMethod

	#region FileVaultFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
    public class KpiHelpFilesFilter : SqlFilter<KpiHelpFilesColumn>
	{
	}
	
	#endregion FileVaultFilter

	#region FileVaultExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
    public class KpiHelpFilesExpressionBuilder : SqlExpressionBuilder<KpiHelpFilesColumn>
	{
	}
	
	#endregion FileVaultExpressionBuilder	

	#region FileVaultProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileVaultChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
    public class KpiHelpFilesProperty : ChildEntityProperty<KpiHelpFilesChildEntityTypes>
	{
	}
	
	#endregion FileVaultProperty
}

