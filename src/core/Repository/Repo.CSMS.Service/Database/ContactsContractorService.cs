﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IContactsContractorService
    {
        ContactsContractor Get(Expression<Func<ContactsContractor, bool>> where, List<Expression<Func<ContactsContractor, object>>> includes);
        List<ContactsContractor> GetMany(int? top, Expression<Func<ContactsContractor, bool>> where, Func<IQueryable<ContactsContractor>, IOrderedQueryable<ContactsContractor>> orderBy, List<Expression<Func<ContactsContractor, object>>> includes);
        List<ContactsContractor> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ContactsContractor, bool>> where, Func<IQueryable<ContactsContractor>, IOrderedQueryable<ContactsContractor>> orderBy, List<Expression<Func<ContactsContractor, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ContactsContractor, bool>> where, List<Expression<Func<ContactsContractor, object>>> includes);
        void Insert(ContactsContractor item, bool saveChanges = true);
        void Update(ContactsContractor item, bool saveChanges = true);
        void Delete(ContactsContractor item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ContactsContractorService : IContactsContractorService
    {
        private readonly IRepository<ContactsContractor> _repository;

        public ContactsContractorService(IRepository<ContactsContractor> repository)
        {
            this._repository = repository;
        }

        public ContactsContractor Get(Expression<Func<ContactsContractor, bool>> where, List<Expression<Func<ContactsContractor, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ContactsContractor> GetMany(int? top, Expression<Func<ContactsContractor, bool>> where, Func<IQueryable<ContactsContractor>, IOrderedQueryable<ContactsContractor>> orderBy, List<Expression<Func<ContactsContractor, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ContactsContractor> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ContactsContractor, bool>> where, Func<IQueryable<ContactsContractor>, IOrderedQueryable<ContactsContractor>> orderBy, List<Expression<Func<ContactsContractor, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ContactsContractor, bool>> where, List<Expression<Func<ContactsContractor, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ContactsContractor item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContactsContractor");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ContactsContractor item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContactsContractor");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ContactsContractor item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContactsContractor");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
