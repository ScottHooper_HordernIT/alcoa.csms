﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'KpiPurchaseOrderList_OpenProjects_Asc' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class KpiPurchaseOrderListOpenProjectsAsc : KpiPurchaseOrderListOpenProjectsAscBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> instance.
		///</summary>
		public KpiPurchaseOrderListOpenProjectsAsc():base(){}	
		
		#endregion
	}
}
