﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SitesEhsimsIhsListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class SitesEhsimsIhsListProviderBaseCore : EntityViewProviderBase<SitesEhsimsIhsList>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;SitesEhsimsIhsList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;SitesEhsimsIhsList&gt;"/></returns>
		protected static VList&lt;SitesEhsimsIhsList&gt; Fill(DataSet dataSet, VList<SitesEhsimsIhsList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<SitesEhsimsIhsList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;SitesEhsimsIhsList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<SitesEhsimsIhsList>"/></returns>
		protected static VList&lt;SitesEhsimsIhsList&gt; Fill(DataTable dataTable, VList<SitesEhsimsIhsList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					SitesEhsimsIhsList c = new SitesEhsimsIhsList();
					c.SiteNameIhs = (Convert.IsDBNull(row["SiteNameIhs"]))?string.Empty:(System.String)row["SiteNameIhs"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.LocCode = (Convert.IsDBNull(row["LocCode"]))?string.Empty:(System.String)row["LocCode"];
					c.LocCodeSiteNameIhs = (Convert.IsDBNull(row["LocCodeSiteNameIhs"]))?string.Empty:(System.String)row["LocCodeSiteNameIhs"];
					c.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(row["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)row["SiteNameLocCodeSiteNameIhs"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.SiteAbbrev = (Convert.IsDBNull(row["SiteAbbrev"]))?string.Empty:(System.String)row["SiteAbbrev"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;SitesEhsimsIhsList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;SitesEhsimsIhsList&gt;"/></returns>
		protected VList<SitesEhsimsIhsList> Fill(IDataReader reader, VList<SitesEhsimsIhsList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					SitesEhsimsIhsList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<SitesEhsimsIhsList>("SitesEhsimsIhsList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new SitesEhsimsIhsList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.SiteNameIhs = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.SiteNameIhs)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.SiteNameIhs)];
					//entity.SiteNameIhs = (Convert.IsDBNull(reader["SiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameIhs"];
					entity.SiteId = (System.Int32)reader[((int)SitesEhsimsIhsListColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.LocCode = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.LocCode)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.LocCode)];
					//entity.LocCode = (Convert.IsDBNull(reader["LocCode"]))?string.Empty:(System.String)reader["LocCode"];
					entity.LocCodeSiteNameIhs = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.LocCodeSiteNameIhs)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.LocCodeSiteNameIhs)];
					//entity.LocCodeSiteNameIhs = (Convert.IsDBNull(reader["LocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["LocCodeSiteNameIhs"];
					entity.SiteNameLocCodeSiteNameIhs = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)];
					//entity.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(reader["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameLocCodeSiteNameIhs"];
					entity.SiteName = (System.String)reader[((int)SitesEhsimsIhsListColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.SiteAbbrev = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.SiteAbbrev)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.SiteAbbrev)];
					//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="SitesEhsimsIhsList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="SitesEhsimsIhsList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, SitesEhsimsIhsList entity)
		{
			reader.Read();
			entity.SiteNameIhs = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.SiteNameIhs)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.SiteNameIhs)];
			//entity.SiteNameIhs = (Convert.IsDBNull(reader["SiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameIhs"];
			entity.SiteId = (System.Int32)reader[((int)SitesEhsimsIhsListColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.LocCode = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.LocCode)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.LocCode)];
			//entity.LocCode = (Convert.IsDBNull(reader["LocCode"]))?string.Empty:(System.String)reader["LocCode"];
			entity.LocCodeSiteNameIhs = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.LocCodeSiteNameIhs)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.LocCodeSiteNameIhs)];
			//entity.LocCodeSiteNameIhs = (Convert.IsDBNull(reader["LocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["LocCodeSiteNameIhs"];
			entity.SiteNameLocCodeSiteNameIhs = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.SiteNameLocCodeSiteNameIhs)];
			//entity.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(reader["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)reader["SiteNameLocCodeSiteNameIhs"];
			entity.SiteName = (System.String)reader[((int)SitesEhsimsIhsListColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.SiteAbbrev = (reader.IsDBNull(((int)SitesEhsimsIhsListColumn.SiteAbbrev)))?null:(System.String)reader[((int)SitesEhsimsIhsListColumn.SiteAbbrev)];
			//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="SitesEhsimsIhsList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="SitesEhsimsIhsList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, SitesEhsimsIhsList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SiteNameIhs = (Convert.IsDBNull(dataRow["SiteNameIhs"]))?string.Empty:(System.String)dataRow["SiteNameIhs"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.LocCode = (Convert.IsDBNull(dataRow["LocCode"]))?string.Empty:(System.String)dataRow["LocCode"];
			entity.LocCodeSiteNameIhs = (Convert.IsDBNull(dataRow["LocCodeSiteNameIhs"]))?string.Empty:(System.String)dataRow["LocCodeSiteNameIhs"];
			entity.SiteNameLocCodeSiteNameIhs = (Convert.IsDBNull(dataRow["SiteNameLocCodeSiteNameIhs"]))?string.Empty:(System.String)dataRow["SiteNameLocCodeSiteNameIhs"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.SiteAbbrev = (Convert.IsDBNull(dataRow["SiteAbbrev"]))?string.Empty:(System.String)dataRow["SiteAbbrev"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region SitesEhsimsIhsListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesEhsimsIhsListFilterBuilder : SqlFilterBuilder<SitesEhsimsIhsListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListFilterBuilder class.
		/// </summary>
		public SitesEhsimsIhsListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesEhsimsIhsListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesEhsimsIhsListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesEhsimsIhsListFilterBuilder

	#region SitesEhsimsIhsListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesEhsimsIhsListParameterBuilder : ParameterizedSqlFilterBuilder<SitesEhsimsIhsListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListParameterBuilder class.
		/// </summary>
		public SitesEhsimsIhsListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesEhsimsIhsListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesEhsimsIhsListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesEhsimsIhsListParameterBuilder
	
	#region SitesEhsimsIhsListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SitesEhsimsIhsListSortBuilder : SqlSortBuilder<SitesEhsimsIhsListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListSqlSortBuilder class.
		/// </summary>
		public SitesEhsimsIhsListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SitesEhsimsIhsListSortBuilder

} // end namespace
