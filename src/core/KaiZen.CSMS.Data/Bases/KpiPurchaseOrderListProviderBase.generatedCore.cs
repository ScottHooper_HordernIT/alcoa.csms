﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiPurchaseOrderListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class KpiPurchaseOrderListProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.KpiPurchaseOrderList, KaiZen.CSMS.Entities.KpiPurchaseOrderListKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiPurchaseOrderListKey key)
		{
			return Delete(transactionManager, key.KpiPurchaseOrderListId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_kpiPurchaseOrderListId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _kpiPurchaseOrderListId)
		{
			return Delete(null, _kpiPurchaseOrderListId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiPurchaseOrderListId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _kpiPurchaseOrderListId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.KpiPurchaseOrderList Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiPurchaseOrderListKey key, int start, int pageLength)
		{
			return GetByKpiPurchaseOrderListId(transactionManager, key.KpiPurchaseOrderListId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_KpiPurchaseOrderListId index.
		/// </summary>
		/// <param name="_kpiPurchaseOrderListId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiPurchaseOrderList GetByKpiPurchaseOrderListId(System.Int32 _kpiPurchaseOrderListId)
		{
			int count = -1;
			return GetByKpiPurchaseOrderListId(null,_kpiPurchaseOrderListId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiPurchaseOrderListId index.
		/// </summary>
		/// <param name="_kpiPurchaseOrderListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiPurchaseOrderList GetByKpiPurchaseOrderListId(System.Int32 _kpiPurchaseOrderListId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiPurchaseOrderListId(null, _kpiPurchaseOrderListId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiPurchaseOrderListId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiPurchaseOrderListId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiPurchaseOrderList GetByKpiPurchaseOrderListId(TransactionManager transactionManager, System.Int32 _kpiPurchaseOrderListId)
		{
			int count = -1;
			return GetByKpiPurchaseOrderListId(transactionManager, _kpiPurchaseOrderListId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiPurchaseOrderListId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiPurchaseOrderListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiPurchaseOrderList GetByKpiPurchaseOrderListId(TransactionManager transactionManager, System.Int32 _kpiPurchaseOrderListId, int start, int pageLength)
		{
			int count = -1;
			return GetByKpiPurchaseOrderListId(transactionManager, _kpiPurchaseOrderListId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiPurchaseOrderListId index.
		/// </summary>
		/// <param name="_kpiPurchaseOrderListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> class.</returns>
		public KaiZen.CSMS.Entities.KpiPurchaseOrderList GetByKpiPurchaseOrderListId(System.Int32 _kpiPurchaseOrderListId, int start, int pageLength, out int count)
		{
			return GetByKpiPurchaseOrderListId(null, _kpiPurchaseOrderListId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_KpiPurchaseOrderListId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_kpiPurchaseOrderListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.KpiPurchaseOrderList GetByKpiPurchaseOrderListId(TransactionManager transactionManager, System.Int32 _kpiPurchaseOrderListId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_Area index.
		/// </summary>
		/// <param name="_area"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByArea(System.String _area)
		{
			int count = -1;
			return GetByArea(null,_area, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_Area index.
		/// </summary>
		/// <param name="_area"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByArea(System.String _area, int start, int pageLength)
		{
			int count = -1;
			return GetByArea(null, _area, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_Area index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_area"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByArea(TransactionManager transactionManager, System.String _area)
		{
			int count = -1;
			return GetByArea(transactionManager, _area, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_Area index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_area"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByArea(TransactionManager transactionManager, System.String _area, int start, int pageLength)
		{
			int count = -1;
			return GetByArea(transactionManager, _area, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_Area index.
		/// </summary>
		/// <param name="_area"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByArea(System.String _area, int start, int pageLength, out int count)
		{
			return GetByArea(null, _area, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_Area index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_area"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByArea(TransactionManager transactionManager, System.String _area, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_ProjectNumber index.
		/// </summary>
		/// <param name="_projectNumber"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectNumber(System.String _projectNumber)
		{
			int count = -1;
			return GetByProjectNumber(null,_projectNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectNumber index.
		/// </summary>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectNumber(System.String _projectNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectNumber(null, _projectNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectNumber(TransactionManager transactionManager, System.String _projectNumber)
		{
			int count = -1;
			return GetByProjectNumber(transactionManager, _projectNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectNumber(TransactionManager transactionManager, System.String _projectNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectNumber(transactionManager, _projectNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectNumber index.
		/// </summary>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectNumber(System.String _projectNumber, int start, int pageLength, out int count)
		{
			return GetByProjectNumber(null, _projectNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByProjectNumber(TransactionManager transactionManager, System.String _projectNumber, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_ProjectStatus index.
		/// </summary>
		/// <param name="_projectStatus"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectStatus(System.String _projectStatus)
		{
			int count = -1;
			return GetByProjectStatus(null,_projectStatus, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectStatus index.
		/// </summary>
		/// <param name="_projectStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectStatus(System.String _projectStatus, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectStatus(null, _projectStatus, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectStatus"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectStatus(TransactionManager transactionManager, System.String _projectStatus)
		{
			int count = -1;
			return GetByProjectStatus(transactionManager, _projectStatus, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectStatus(TransactionManager transactionManager, System.String _projectStatus, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectStatus(transactionManager, _projectStatus, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectStatus index.
		/// </summary>
		/// <param name="_projectStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectStatus(System.String _projectStatus, int start, int pageLength, out int count)
		{
			return GetByProjectStatus(null, _projectStatus, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByProjectStatus(TransactionManager transactionManager, System.String _projectStatus, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_TaskNumber index.
		/// </summary>
		/// <param name="_taskNumber"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByTaskNumber(System.String _taskNumber)
		{
			int count = -1;
			return GetByTaskNumber(null,_taskNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_TaskNumber index.
		/// </summary>
		/// <param name="_taskNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByTaskNumber(System.String _taskNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByTaskNumber(null, _taskNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_TaskNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taskNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByTaskNumber(TransactionManager transactionManager, System.String _taskNumber)
		{
			int count = -1;
			return GetByTaskNumber(transactionManager, _taskNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_TaskNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taskNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByTaskNumber(TransactionManager transactionManager, System.String _taskNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByTaskNumber(transactionManager, _taskNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_TaskNumber index.
		/// </summary>
		/// <param name="_taskNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByTaskNumber(System.String _taskNumber, int start, int pageLength, out int count)
		{
			return GetByTaskNumber(null, _taskNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_TaskNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taskNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByTaskNumber(TransactionManager transactionManager, System.String _taskNumber, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_ProjectDesc index.
		/// </summary>
		/// <param name="_projectDesc"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectDesc(System.String _projectDesc)
		{
			int count = -1;
			return GetByProjectDesc(null,_projectDesc, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectDesc index.
		/// </summary>
		/// <param name="_projectDesc"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectDesc(System.String _projectDesc, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectDesc(null, _projectDesc, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectDesc"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectDesc(TransactionManager transactionManager, System.String _projectDesc)
		{
			int count = -1;
			return GetByProjectDesc(transactionManager, _projectDesc, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectDesc"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectDesc(TransactionManager transactionManager, System.String _projectDesc, int start, int pageLength)
		{
			int count = -1;
			return GetByProjectDesc(transactionManager, _projectDesc, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectDesc index.
		/// </summary>
		/// <param name="_projectDesc"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByProjectDesc(System.String _projectDesc, int start, int pageLength, out int count)
		{
			return GetByProjectDesc(null, _projectDesc, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_ProjectDesc index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_projectDesc"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByProjectDesc(TransactionManager transactionManager, System.String _projectDesc, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_PurchaseOrderNumber_PurchaseOrderLineNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="_purchaseOrderLineNumber"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumberPurchaseOrderLineNumber(System.String _purchaseOrderNumber, System.String _purchaseOrderLineNumber)
		{
			int count = -1;
			return GetByPurchaseOrderNumberPurchaseOrderLineNumber(null,_purchaseOrderNumber, _purchaseOrderLineNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber_PurchaseOrderLineNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="_purchaseOrderLineNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumberPurchaseOrderLineNumber(System.String _purchaseOrderNumber, System.String _purchaseOrderLineNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByPurchaseOrderNumberPurchaseOrderLineNumber(null, _purchaseOrderNumber, _purchaseOrderLineNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber_PurchaseOrderLineNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="_purchaseOrderLineNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumberPurchaseOrderLineNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, System.String _purchaseOrderLineNumber)
		{
			int count = -1;
			return GetByPurchaseOrderNumberPurchaseOrderLineNumber(transactionManager, _purchaseOrderNumber, _purchaseOrderLineNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber_PurchaseOrderLineNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="_purchaseOrderLineNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumberPurchaseOrderLineNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, System.String _purchaseOrderLineNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByPurchaseOrderNumberPurchaseOrderLineNumber(transactionManager, _purchaseOrderNumber, _purchaseOrderLineNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber_PurchaseOrderLineNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="_purchaseOrderLineNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumberPurchaseOrderLineNumber(System.String _purchaseOrderNumber, System.String _purchaseOrderLineNumber, int start, int pageLength, out int count)
		{
			return GetByPurchaseOrderNumberPurchaseOrderLineNumber(null, _purchaseOrderNumber, _purchaseOrderLineNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber_PurchaseOrderLineNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="_purchaseOrderLineNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByPurchaseOrderNumberPurchaseOrderLineNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, System.String _purchaseOrderLineNumber, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_KpiPurchaseOrderList_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumber(System.String _purchaseOrderNumber)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(null,_purchaseOrderNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumber(System.String _purchaseOrderNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(null, _purchaseOrderNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(transactionManager, _purchaseOrderNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(transactionManager, _purchaseOrderNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public TList<KpiPurchaseOrderList> GetByPurchaseOrderNumber(System.String _purchaseOrderNumber, int start, int pageLength, out int count)
		{
			return GetByPurchaseOrderNumber(null, _purchaseOrderNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_KpiPurchaseOrderList_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;KpiPurchaseOrderList&gt;"/> class.</returns>
		public abstract TList<KpiPurchaseOrderList> GetByPurchaseOrderNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _KpiPurchaseOrderList_GetDistinct_ProjectNumber 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumber()
		{
			return GetDistinct_ProjectNumber(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumber(int start, int pageLength)
		{
			return GetDistinct_ProjectNumber(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumber(TransactionManager transactionManager)
		{
			return GetDistinct_ProjectNumber(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_ProjectNumber(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_ProjectDesc 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectDesc' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectDesc()
		{
			return GetDistinct_ProjectDesc(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectDesc' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectDesc(int start, int pageLength)
		{
			return GetDistinct_ProjectDesc(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectDesc' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectDesc(TransactionManager transactionManager)
		{
			return GetDistinct_ProjectDesc(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectDesc' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_ProjectDesc(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber_Open()
		{
			return GetDistinct_PurchaseOrderNumber_Open(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber_Open(int start, int pageLength)
		{
			return GetDistinct_PurchaseOrderNumber_Open(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber_Open(TransactionManager transactionManager)
		{
			return GetDistinct_PurchaseOrderNumber_Open(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_PurchaseOrderNumber_Open(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_TaskNumber 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_TaskNumber' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_TaskNumber()
		{
			return GetDistinct_TaskNumber(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_TaskNumber' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_TaskNumber(int start, int pageLength)
		{
			return GetDistinct_TaskNumber(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_TaskNumber' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_TaskNumber(TransactionManager transactionManager)
		{
			return GetDistinct_TaskNumber(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_TaskNumber' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_TaskNumber(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_ProjectNumberDesc_Open 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumberDesc_Open' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumberDesc_Open()
		{
			return GetDistinct_ProjectNumberDesc_Open(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumberDesc_Open' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumberDesc_Open(int start, int pageLength)
		{
			return GetDistinct_ProjectNumberDesc_Open(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumberDesc_Open' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumberDesc_Open(TransactionManager transactionManager)
		{
			return GetDistinct_ProjectNumberDesc_Open(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumberDesc_Open' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_ProjectNumberDesc_Open(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_ProjectNumber_Open 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber_Open' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumber_Open()
		{
			return GetDistinct_ProjectNumber_Open(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber_Open' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumber_Open(int start, int pageLength)
		{
			return GetDistinct_ProjectNumber_Open(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber_Open' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectNumber_Open(TransactionManager transactionManager)
		{
			return GetDistinct_ProjectNumber_Open(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectNumber_Open' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_ProjectNumber_Open(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open_List 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open_List' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber_Open_List()
		{
			return GetDistinct_PurchaseOrderNumber_Open_List(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open_List' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber_Open_List(int start, int pageLength)
		{
			return GetDistinct_PurchaseOrderNumber_Open_List(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open_List' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber_Open_List(TransactionManager transactionManager)
		{
			return GetDistinct_PurchaseOrderNumber_Open_List(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open_List' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_PurchaseOrderNumber_Open_List(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_Reseed 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_Reseed' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reseed()
		{
			return Reseed(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_Reseed' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reseed(int start, int pageLength)
		{
			return Reseed(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_Reseed' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Reseed(TransactionManager transactionManager)
		{
			return Reseed(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_Reseed' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Reseed(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber()
		{
			return GetDistinct_PurchaseOrderNumber(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber(int start, int pageLength)
		{
			return GetDistinct_PurchaseOrderNumber(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_PurchaseOrderNumber(TransactionManager transactionManager)
		{
			return GetDistinct_PurchaseOrderNumber(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_PurchaseOrderNumber(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_ProjectStatus 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectStatus' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectStatus()
		{
			return GetDistinct_ProjectStatus(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectStatus' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectStatus(int start, int pageLength)
		{
			return GetDistinct_ProjectStatus(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectStatus' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_ProjectStatus(TransactionManager transactionManager)
		{
			return GetDistinct_ProjectStatus(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_ProjectStatus' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_ProjectStatus(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _KpiPurchaseOrderList_GetDistinct_Area 
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_Area' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_Area()
		{
			return GetDistinct_Area(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_Area' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_Area(int start, int pageLength)
		{
			return GetDistinct_Area(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_Area' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinct_Area(TransactionManager transactionManager)
		{
			return GetDistinct_Area(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_KpiPurchaseOrderList_GetDistinct_Area' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinct_Area(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;KpiPurchaseOrderList&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;KpiPurchaseOrderList&gt;"/></returns>
		public static TList<KpiPurchaseOrderList> Fill(IDataReader reader, TList<KpiPurchaseOrderList> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.KpiPurchaseOrderList c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("KpiPurchaseOrderList")
					.Append("|").Append((System.Int32)reader[((int)KpiPurchaseOrderListColumn.KpiPurchaseOrderListId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<KpiPurchaseOrderList>(
					key.ToString(), // EntityTrackingKey
					"KpiPurchaseOrderList",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.KpiPurchaseOrderList();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.KpiPurchaseOrderListId = (System.Int32)reader[((int)KpiPurchaseOrderListColumn.KpiPurchaseOrderListId - 1)];
					c.PurchaseOrderNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.PurchaseOrderNumber - 1)];
					c.PurchaseOrderLineNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.PurchaseOrderLineNumber - 1)];
					c.ProjectNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.ProjectNumber - 1)];
					c.ProjectDesc = (reader.IsDBNull(((int)KpiPurchaseOrderListColumn.ProjectDesc - 1)))?null:(System.String)reader[((int)KpiPurchaseOrderListColumn.ProjectDesc - 1)];
					c.Area = (System.String)reader[((int)KpiPurchaseOrderListColumn.Area - 1)];
					c.ProjectStatus = (System.String)reader[((int)KpiPurchaseOrderListColumn.ProjectStatus - 1)];
					c.TaskNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.TaskNumber - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.KpiPurchaseOrderList entity)
		{
			if (!reader.Read()) return;
			
			entity.KpiPurchaseOrderListId = (System.Int32)reader[((int)KpiPurchaseOrderListColumn.KpiPurchaseOrderListId - 1)];
			entity.PurchaseOrderNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.PurchaseOrderNumber - 1)];
			entity.PurchaseOrderLineNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.PurchaseOrderLineNumber - 1)];
			entity.ProjectNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.ProjectNumber - 1)];
			entity.ProjectDesc = (reader.IsDBNull(((int)KpiPurchaseOrderListColumn.ProjectDesc - 1)))?null:(System.String)reader[((int)KpiPurchaseOrderListColumn.ProjectDesc - 1)];
			entity.Area = (System.String)reader[((int)KpiPurchaseOrderListColumn.Area - 1)];
			entity.ProjectStatus = (System.String)reader[((int)KpiPurchaseOrderListColumn.ProjectStatus - 1)];
			entity.TaskNumber = (System.String)reader[((int)KpiPurchaseOrderListColumn.TaskNumber - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.KpiPurchaseOrderList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.KpiPurchaseOrderListId = (System.Int32)dataRow["KpiPurchaseOrderListId"];
			entity.PurchaseOrderNumber = (System.String)dataRow["PurchaseOrderNumber"];
			entity.PurchaseOrderLineNumber = (System.String)dataRow["PurchaseOrderLineNumber"];
			entity.ProjectNumber = (System.String)dataRow["ProjectNumber"];
			entity.ProjectDesc = Convert.IsDBNull(dataRow["ProjectDesc"]) ? null : (System.String)dataRow["ProjectDesc"];
			entity.Area = (System.String)dataRow["Area"];
			entity.ProjectStatus = (System.String)dataRow["ProjectStatus"];
			entity.TaskNumber = (System.String)dataRow["TaskNumber"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.KpiPurchaseOrderList"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiPurchaseOrderList Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiPurchaseOrderList entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.KpiPurchaseOrderList object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.KpiPurchaseOrderList instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.KpiPurchaseOrderList Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.KpiPurchaseOrderList entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region KpiPurchaseOrderListChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.KpiPurchaseOrderList</c>
	///</summary>
	public enum KpiPurchaseOrderListChildEntityTypes
	{
	}
	
	#endregion KpiPurchaseOrderListChildEntityTypes
	
	#region KpiPurchaseOrderListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;KpiPurchaseOrderListColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListFilterBuilder : SqlFilterBuilder<KpiPurchaseOrderListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListFilterBuilder class.
		/// </summary>
		public KpiPurchaseOrderListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListFilterBuilder
	
	#region KpiPurchaseOrderListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;KpiPurchaseOrderListColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListParameterBuilder : ParameterizedSqlFilterBuilder<KpiPurchaseOrderListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListParameterBuilder class.
		/// </summary>
		public KpiPurchaseOrderListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListParameterBuilder
	
	#region KpiPurchaseOrderListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;KpiPurchaseOrderListColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiPurchaseOrderListSortBuilder : SqlSortBuilder<KpiPurchaseOrderListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListSqlSortBuilder class.
		/// </summary>
		public KpiPurchaseOrderListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiPurchaseOrderListSortBuilder
	
} // end namespace
