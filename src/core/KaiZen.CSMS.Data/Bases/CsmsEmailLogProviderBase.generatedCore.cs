﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsmsEmailLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsmsEmailLogProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CsmsEmailLog, KaiZen.CSMS.Entities.CsmsEmailLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLogKey key)
		{
			return Delete(transactionManager, key.CsmsEmailLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csmsEmailLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csmsEmailLogId)
		{
			return Delete(null, _csmsEmailLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csmsEmailLogId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CsmsEmailLog Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLogKey key, int start, int pageLength)
		{
			return GetByCsmsEmailLogId(transactionManager, key.CsmsEmailLogId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CsmsEmailLog index.
		/// </summary>
		/// <param name="_csmsEmailLogId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLog GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId)
		{
			int count = -1;
			return GetByCsmsEmailLogId(null,_csmsEmailLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailLog index.
		/// </summary>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLog GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailLogId(null, _csmsEmailLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLog GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId)
		{
			int count = -1;
			return GetByCsmsEmailLogId(transactionManager, _csmsEmailLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLog GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailLogId(transactionManager, _csmsEmailLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailLog index.
		/// </summary>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsEmailLog GetByCsmsEmailLogId(System.Int32 _csmsEmailLogId, int start, int pageLength, out int count)
		{
			return GetByCsmsEmailLogId(null, _csmsEmailLogId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsEmailLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsEmailLog GetByCsmsEmailLogId(TransactionManager transactionManager, System.Int32 _csmsEmailLogId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _CsmsEmailLog_Get_List_Detailed_BySentUserId 
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_BySentUserId' stored procedure. 
		/// </summary>
		/// <param name="sentByUserId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed_BySentUserId(System.Int32? sentByUserId)
		{
			return Get_List_Detailed_BySentUserId(null, 0, int.MaxValue , sentByUserId);
		}
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_BySentUserId' stored procedure. 
		/// </summary>
		/// <param name="sentByUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed_BySentUserId(int start, int pageLength, System.Int32? sentByUserId)
		{
			return Get_List_Detailed_BySentUserId(null, start, pageLength , sentByUserId);
		}
				
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_BySentUserId' stored procedure. 
		/// </summary>
		/// <param name="sentByUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed_BySentUserId(TransactionManager transactionManager, System.Int32? sentByUserId)
		{
			return Get_List_Detailed_BySentUserId(transactionManager, 0, int.MaxValue , sentByUserId);
		}
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_BySentUserId' stored procedure. 
		/// </summary>
		/// <param name="sentByUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Get_List_Detailed_BySentUserId(TransactionManager transactionManager, int start, int pageLength , System.Int32? sentByUserId);
		
		#endregion
		
		#region _CsmsEmailLog_Get_List_Detailed_ByAdminTaskId 
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_ByAdminTaskId' stored procedure. 
		/// </summary>
		/// <param name="adminTaskId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed_ByAdminTaskId(System.Int32? adminTaskId)
		{
			return Get_List_Detailed_ByAdminTaskId(null, 0, int.MaxValue , adminTaskId);
		}
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_ByAdminTaskId' stored procedure. 
		/// </summary>
		/// <param name="adminTaskId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed_ByAdminTaskId(int start, int pageLength, System.Int32? adminTaskId)
		{
			return Get_List_Detailed_ByAdminTaskId(null, start, pageLength , adminTaskId);
		}
				
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_ByAdminTaskId' stored procedure. 
		/// </summary>
		/// <param name="adminTaskId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed_ByAdminTaskId(TransactionManager transactionManager, System.Int32? adminTaskId)
		{
			return Get_List_Detailed_ByAdminTaskId(transactionManager, 0, int.MaxValue , adminTaskId);
		}
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed_ByAdminTaskId' stored procedure. 
		/// </summary>
		/// <param name="adminTaskId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Get_List_Detailed_ByAdminTaskId(TransactionManager transactionManager, int start, int pageLength , System.Int32? adminTaskId);
		
		#endregion
		
		#region _CsmsEmailLog_Get_List_Detailed 
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed()
		{
			return Get_List_Detailed(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed(int start, int pageLength)
		{
			return Get_List_Detailed(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Detailed(TransactionManager transactionManager)
		{
			return Get_List_Detailed(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_CsmsEmailLog_Get_List_Detailed' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Get_List_Detailed(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CsmsEmailLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CsmsEmailLog&gt;"/></returns>
		public static TList<CsmsEmailLog> Fill(IDataReader reader, TList<CsmsEmailLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CsmsEmailLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CsmsEmailLog")
					.Append("|").Append((System.Int32)reader[((int)CsmsEmailLogColumn.CsmsEmailLogId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CsmsEmailLog>(
					key.ToString(), // EntityTrackingKey
					"CsmsEmailLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CsmsEmailLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsmsEmailLogId = (System.Int32)reader[((int)CsmsEmailLogColumn.CsmsEmailLogId - 1)];
					c.SentByUserId = (System.Int32)reader[((int)CsmsEmailLogColumn.SentByUserId - 1)];
					c.EmailSentDateTime = (System.DateTime)reader[((int)CsmsEmailLogColumn.EmailSentDateTime - 1)];
					c.Subject = (System.String)reader[((int)CsmsEmailLogColumn.Subject - 1)];
					c.Body = (System.Byte[])reader[((int)CsmsEmailLogColumn.Body - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CsmsEmailLog entity)
		{
			if (!reader.Read()) return;
			
			entity.CsmsEmailLogId = (System.Int32)reader[((int)CsmsEmailLogColumn.CsmsEmailLogId - 1)];
			entity.SentByUserId = (System.Int32)reader[((int)CsmsEmailLogColumn.SentByUserId - 1)];
			entity.EmailSentDateTime = (System.DateTime)reader[((int)CsmsEmailLogColumn.EmailSentDateTime - 1)];
			entity.Subject = (System.String)reader[((int)CsmsEmailLogColumn.Subject - 1)];
			entity.Body = (System.Byte[])reader[((int)CsmsEmailLogColumn.Body - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CsmsEmailLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsmsEmailLogId = (System.Int32)dataRow["CsmsEmailLogId"];
			entity.SentByUserId = (System.Int32)dataRow["SentByUserId"];
			entity.EmailSentDateTime = (System.DateTime)dataRow["EmailSentDateTime"];
			entity.Subject = (System.String)dataRow["Subject"];
			entity.Body = (System.Byte[])dataRow["Body"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsEmailLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsEmailLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCsmsEmailLogId methods when available
			
			#region CsmsEmailLogRecipientCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CsmsEmailLogRecipient>|CsmsEmailLogRecipientCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsEmailLogRecipientCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsmsEmailLogRecipientCollection = DataRepository.CsmsEmailLogRecipientProvider.GetByCsmsEmailLogId(transactionManager, entity.CsmsEmailLogId);

				if (deep && entity.CsmsEmailLogRecipientCollection.Count > 0)
				{
					deepHandles.Add("CsmsEmailLogRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CsmsEmailLogRecipient>) DataRepository.CsmsEmailLogRecipientProvider.DeepLoad,
						new object[] { transactionManager, entity.CsmsEmailLogRecipientCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdminTaskEmailLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailLog>|AdminTaskEmailLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailLogCollection = DataRepository.AdminTaskEmailLogProvider.GetByCsmsEmailLogId(transactionManager, entity.CsmsEmailLogId);

				if (deep && entity.AdminTaskEmailLogCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailLog>) DataRepository.AdminTaskEmailLogProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CsmsEmailLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CsmsEmailLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsEmailLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsEmailLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CsmsEmailLogRecipient>
				if (CanDeepSave(entity.CsmsEmailLogRecipientCollection, "List<CsmsEmailLogRecipient>|CsmsEmailLogRecipientCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CsmsEmailLogRecipient child in entity.CsmsEmailLogRecipientCollection)
					{
						if(child.CsmsEmailLogIdSource != null)
						{
							child.CsmsEmailLogId = child.CsmsEmailLogIdSource.CsmsEmailLogId;
						}
						else
						{
							child.CsmsEmailLogId = entity.CsmsEmailLogId;
						}

					}

					if (entity.CsmsEmailLogRecipientCollection.Count > 0 || entity.CsmsEmailLogRecipientCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CsmsEmailLogRecipientProvider.Save(transactionManager, entity.CsmsEmailLogRecipientCollection);
						
						deepHandles.Add("CsmsEmailLogRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CsmsEmailLogRecipient >) DataRepository.CsmsEmailLogRecipientProvider.DeepSave,
							new object[] { transactionManager, entity.CsmsEmailLogRecipientCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdminTaskEmailLog>
				if (CanDeepSave(entity.AdminTaskEmailLogCollection, "List<AdminTaskEmailLog>|AdminTaskEmailLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailLog child in entity.AdminTaskEmailLogCollection)
					{
						if(child.CsmsEmailLogIdSource != null)
						{
							child.CsmsEmailLogId = child.CsmsEmailLogIdSource.CsmsEmailLogId;
						}
						else
						{
							child.CsmsEmailLogId = entity.CsmsEmailLogId;
						}

					}

					if (entity.AdminTaskEmailLogCollection.Count > 0 || entity.AdminTaskEmailLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailLogProvider.Save(transactionManager, entity.AdminTaskEmailLogCollection);
						
						deepHandles.Add("AdminTaskEmailLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailLog >) DataRepository.AdminTaskEmailLogProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsmsEmailLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CsmsEmailLog</c>
	///</summary>
	public enum CsmsEmailLogChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CsmsEmailLog</c> as OneToMany for CsmsEmailLogRecipientCollection
		///</summary>
		[ChildEntityType(typeof(TList<CsmsEmailLogRecipient>))]
		CsmsEmailLogRecipientCollection,

		///<summary>
		/// Collection of <c>CsmsEmailLog</c> as OneToMany for AdminTaskEmailLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailLog>))]
		AdminTaskEmailLogCollection,
	}
	
	#endregion CsmsEmailLogChildEntityTypes
	
	#region CsmsEmailLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsmsEmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogFilterBuilder : SqlFilterBuilder<CsmsEmailLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogFilterBuilder class.
		/// </summary>
		public CsmsEmailLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogFilterBuilder
	
	#region CsmsEmailLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsmsEmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogParameterBuilder : ParameterizedSqlFilterBuilder<CsmsEmailLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogParameterBuilder class.
		/// </summary>
		public CsmsEmailLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsEmailLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsEmailLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsEmailLogParameterBuilder
	
	#region CsmsEmailLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsmsEmailLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsmsEmailLogSortBuilder : SqlSortBuilder<CsmsEmailLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogSqlSortBuilder class.
		/// </summary>
		public CsmsEmailLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsmsEmailLogSortBuilder
	
} // end namespace
