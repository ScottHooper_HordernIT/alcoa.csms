//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class EbiDailyReport
    {
        public int EbiDailyReportId { get; set; }
        public System.DateTime DateTime { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameCsms { get; set; }
        public string SiteName { get; set; }
        public string ApprovedOnSite { get; set; }
        public int EbiIssueId { get; set; }
        public string SqValidTill { get; set; }
        public string CompanySqStatus { get; set; }
        public string QuestionnaireId { get; set; }
        public string Validity { get; set; }
        public string Type { get; set; }
        public string ProcurementContact { get; set; }
        public string HSAssessor { get; set; }
    
        public virtual EbiIssue EbiIssue { get; set; }
    }
}
