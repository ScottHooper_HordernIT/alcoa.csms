﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data;
using DevExpress.Web.ASPxGridView;
using System.Configuration;
using System.Data.SqlClient;

namespace ALCOA.CSMS.Website.UserControls.Main.PeopleManagementTraining
{
    public partial class WorkPlanningFormSummary : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);

            if (!Page.IsPostBack)
            {




                Helper.ExportGrid.Settings(ucExportButtons2, "People Management – Training – Work Planning Form Summary", "grid");
                imgCross.Src = @"~/Images/redcross.gif";
                FillCompanyDropdown(cmbCompanies);
                //FillDateFrom();
                //lblDateTo.Text = DateTime.Now.ToShortDateString();
                int startIndex = 0;

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):

                        cmbCompanies.Enabled = true;
                        FillSitesCombo("-1", cmbSites);
                        startIndex = cmbSites.Items.IndexOfText("Australia");
                        cmbSites.SelectedIndex = startIndex;
                        break;
                    case ((int)RoleList.Contractor):
                        cmbCompanies.Value = auth.CompanyId;
                        cmbCompanies.Enabled = false;
                        FillSitesCombo(auth.CompanyId.ToString(), cmbSites);


                        break;
                    case ((int)RoleList.Administrator):
                        cmbCompanies.Enabled = true;
                        FillSitesCombo("-1", cmbSites);
                        startIndex = cmbSites.Items.IndexOfText("Australia");
                        cmbSites.SelectedIndex = startIndex;
                        break;
                    default:
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }

                GridViewBandColumn db = new GridViewBandColumn();
                db.Caption = "Training Competency Valid To";
                db.Name = "HeaderColumn";
                db.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                db.VisibleIndex = 4;


                DataView dvCompetency = (DataView)sqldaCometency.Select(DataSourceSelectArguments.Empty);
                DataTable dtCompetency = dvCompetency.ToTable();
                for (int i = 0; i < dtCompetency.Rows.Count; i++)
                {
                    GridViewDataTextColumn dc = new GridViewDataTextColumn();
                    dc.Name = dtCompetency.Rows[i][0].ToString();
                    dc.Caption = dtCompetency.Rows[i][0].ToString();
                    dc.FieldName = dtCompetency.Rows[i][0].ToString();
                    dc.VisibleIndex = i + 4;
                    dc.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                    dc.HeaderStyle.Wrap = DevExpress.Utils.DefaultBoolean.True;
                    
                    dc.Width = 80;
                    //GridViewDataItemTemplateContainer dcc = (GridViewDataItemTemplateContainer)dc.DataItemTemplate;
                    //Control dccc = (Control)dcc;

                    //dc.DataItemTemplate = new MyTemplate(dccc);

                    //Image imgCrl = new Image();
                    //imgCrl.ID = "img" + i.ToString();
                    //imgCrl.ImageUrl = "~/Images/spacer.gif";
                    //dc.DataItemTemplate.InstantiateIn(imgCrl);

                    db.Columns.Add(dc);
                }

                this.grid.Columns.Add(db);



                //GridViewBandColumn parentColumn = grid.Columns["HeaderColumn"] as GridViewBandColumn;
                //for (int i = 0; i < dtCompetency.Rows.Count - 1; i++)
                //{
                //     GridViewDataTextColumn dataTextcolm = parentColumn.Columns["One Induction"] as GridViewDataTextColumn;
                //     dataTextcolm.DataItemTemplate = new MyTemplate();
                //}

            }
            else
            {
                if (cmbSites.Value != null)
                {
                    if (Convert.ToInt32(cmbSites.Value) < 0)
                    {
                        string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";
                        SqlDataSite1.SelectCommand = strQuery;
                        grid.Columns["SiteId"].Visible = true;

                    }
                    else
                    {
                        string strQuery = "select SiteId , SiteName from Sites where SiteId=2";
                        SqlDataSite1.SelectCommand = strQuery;
                        grid.Columns["SiteId"].Visible = false;
                    }
                    grid.DataBind();
                    if (ViewState["CompanyId"] != null)
                    {
                        DataTable dt = new DataTable();
                        string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                        int lastYear = DateTime.Now.Year;
                        int FirstYear = 2011;

                        using (SqlConnection cnDel = new SqlConnection(sqldb))
                        {
                            cnDel.Open();
                            //Console.WriteLine("..Creating table: WorkFormPlanSummary" + "...");
                            SqlCommand cmdDel = new SqlCommand("WorkPlanningFormSummary_Select3", cnDel);
                            cmdDel.CommandTimeout = 7000;
                            cmdDel.CommandType = CommandType.StoredProcedure;
                            cmdDel.Parameters.AddWithValue("@DateFrom", Convert.ToDateTime(deFrom.Date.ToString("yyyy-MM-dd")));
                            cmdDel.Parameters.AddWithValue("@DateTo", Convert.ToDateTime(deTo.Date.ToString("yyyy-MM-dd")));
                            cmdDel.Parameters.AddWithValue("@CompanyId", Convert.ToInt32(ViewState["CompanyId"]));
                            cmdDel.Parameters.AddWithValue("@SiteId", Convert.ToInt32(ViewState["SiteId"]));

                            SqlDataAdapter sqlda = new SqlDataAdapter(cmdDel);
                            sqlda.Fill(dt);
                            cnDel.Close();
                        }
                        DataView dv = new DataView(dt);
                        if (Convert.ToInt32(cmbSites.Value) > 0)
                        {
                            dv.RowFilter = "SiteId =" + Convert.ToString(cmbSites.Value);
                        }
                        else if (Convert.ToInt32(cmbSites.Value) < 0)
                        {
                            int count = 0;
                            string strSites = string.Empty;
                            RegionsSitesService reSer = new RegionsSitesService();
                            TList<RegionsSites> REList = reSer.GetByRegionId(-1 * Convert.ToInt32(cmbSites.Value));
                            foreach (RegionsSites re in REList)
                            {
                                if (count > 0) { strSites = strSites + ", "; }
                                strSites = strSites + Convert.ToString(re.SiteId);
                                count++;
                            }

                            dv.RowFilter = "SiteId in (" + strSites + ")";
                        }
                        else
                        {
                            throw new Exception("Please select a valid site/region");
                        }
                        DataTable dt1 = dv.ToTable();
                        
                        grid.DataSource = dt1;
                    }

                    grid.DataBind();
                }
                

            }
        }

        protected void cmbSites_Callback(object source, CallbackEventArgsBase e)
        {
            cmbSites.SelectedIndex = 0;
            FillSitesCombo(e.Parameter, cmbSites);
        }


        protected void FillSitesCombo(string companyId, ASPxComboBox CmbSite)
        {
            int _i = 0;
            if (string.IsNullOrEmpty(companyId)) return;

            int _companyId;
            bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
            if (!companyId_isInt) return;



            if (_companyId > 0) //Specific Company
            {

                SitesService sService = new SitesService();
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_companyId);

                CmbSite.Items.Clear();

                if (cscsTlist != null)
                {
                    if (cscsTlist.Count > 0)
                    {
                        SitesFilters sitesFilters = new SitesFilters();
                        sitesFilters.Append(SitesColumn.IsVisible, "1");
                        TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                        foreach (Sites s in sitesList)
                        {
                            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                            {
                                if (cscs.CompanySiteCategoryId != null)
                                {
                                    if (s.SiteId == cscs.SiteId)
                                    {
                                        CmbSite.Items.Add(s.SiteName, cscs.SiteId);
                                    }
                                }
                            }
                        }

                        cmbSites.Items.Add("----------------", 0);

                        //Add Regions
                        RegionsService rService = new RegionsService();
                        DataSet dsRegion = rService.GetByCompanyId(_companyId);
                        if (dsRegion.Tables[0] != null)
                        {
                            if (dsRegion.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in dsRegion.Tables[0].Rows)
                                {
                                    CmbSite.Items.Add(dr[0].ToString(), (-1 * Convert.ToInt32(dr[1].ToString())));
                                }
                            }
                        }
                        CmbSite.Items.Add("Australia", -2);
                        
                        //CmbSite.Value = -2;
                    }
                }
            }
            else
            {
                if (_companyId == -1) // All Companies
                {
                    CmbSite.Items.Clear();
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        CmbSite.Items.Add(s.SiteName, s.SiteId);
                    }
                    CmbSite.Items.Add("----------------", 0);

                    RegionsFilters regionsFilters = new RegionsFilters();
                    regionsFilters.Append(RegionsColumn.IsVisible, "1");
                    TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                    foreach (Regions r in regionsList)
                    {
                        CmbSite.Items.Add(r.RegionName, (-1 * r.RegionId));
                        //CmbSite.Items.Add(r.RegionName, (r.RegionId));
                    }
                }
                else if (_companyId == -2) //Selected "----" invalid option, so lets hide all sites.
                {
                    CmbSite.Items.Clear();
                    CmbSite.DataSourceID = "";
                    CmbSite.DataBind();
                }
            }

        }


        protected void FillCompanyDropdown(ASPxComboBox CmbCompany)
        {
            CmbCompany.DataSourceID = "CompaniesDataSource3";
            CmbCompany.TextField = "CompanyName";
            CmbCompany.ValueField = "CompanyId";
            CmbCompany.DataBind();
            CmbCompany.Items.Add("-----------------------------", 0);
            CmbCompany.Items.Add("All", -1);
            CmbCompany.Value = -1;
        }




        protected void cmbCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSitesCombo(cmbCompanies.SelectedItem.Value.ToString(), cmbSites);

        }

        protected void btnGoFilter_click(object sender, EventArgs e)
        {
            //SqlDataSourcEWorkPlanningSummary.SelectParameters["DateFrom"].DefaultValue = deFrom.Date.ToString("yyyy-MM-dd");
            //SqlDataSourcEWorkPlanningSummary.SelectParameters["DateTo"].DefaultValue = deTo.Date.ToString("yyyy-MM-dd");
            //SqlDataSourcEWorkPlanningSummary.SelectParameters["CompanyId"].DefaultValue = cmbCompanies.Value.ToString();
            //SqlDataSourcEWorkPlanningSummary.SelectParameters["SiteId"].DefaultValue = cmbSites.Value.ToString();
            //grid.DataSourceID = "SqlDataSourcEWorkPlanningSummary";
            if (Convert.ToInt32(cmbSites.SelectedItem.Value) != 0 && Convert.ToInt32(cmbCompanies.SelectedItem.Value) != 0 && (Convert.ToDateTime(deFrom.Date.ToString("yyyy-MM-dd")) <= Convert.ToDateTime(deTo.Date.ToString("yyyy-MM-dd"))))
            {
                DataTable dt = new DataTable();
                string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                int lastYear = DateTime.Now.Year;
                int FirstYear = 2011;

                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    //Console.WriteLine("..Creating table: WorkFormPlanSummary" + "...");
                    SqlCommand cmdDel = new SqlCommand("WorkPlanningFormSummary_Select3", cnDel);
                    cmdDel.CommandTimeout = 7000;
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.Parameters.AddWithValue("@DateFrom", Convert.ToDateTime(deFrom.Date.ToString("yyyy-MM-dd")));
                    cmdDel.Parameters.AddWithValue("@DateTo", Convert.ToDateTime(deTo.Date.ToString("yyyy-MM-dd")));
                    cmdDel.Parameters.AddWithValue("@CompanyId", Convert.ToInt32(cmbCompanies.Value));
                    cmdDel.Parameters.AddWithValue("@SiteId", Convert.ToInt32(cmbSites.Value));

                    SqlDataAdapter sqlda = new SqlDataAdapter(cmdDel);
                    sqlda.Fill(dt);
                    cnDel.Close();
                }
                DataView dv = new DataView(dt);
                if (Convert.ToInt32(cmbSites.Value) > 0)
                {
                    dv.RowFilter = "SiteId =" + Convert.ToString(cmbSites.Value);
                }
                else
                {
                    int count = 0;
                    string strSites = string.Empty;
                    RegionsSitesService reSer = new RegionsSitesService();
                    TList<RegionsSites> REList = reSer.GetByRegionId(-1 * Convert.ToInt32(cmbSites.Value));
                    foreach (RegionsSites re in REList)
                    {
                        if (count > 0) { strSites = strSites + ", "; }
                        strSites = strSites + Convert.ToString(re.SiteId);
                        count++;
                    }

                    dv.RowFilter = "SiteId in (" + strSites + ")";
                }
                DataTable dt1 = dv.ToTable();
                ViewState["CompanyId"] = Convert.ToInt32(cmbCompanies.Value);
                ViewState["SiteId"] = Convert.ToInt32(cmbSites.Value);
                grid.DataSource = dt1;

                grid.DataBind();

                grid.Visible = true;
                ucExportButtons2.Visible = true;

                divLegend.Style.Add(HtmlTextWriterStyle.Display, "block");

                if (cmbCompanies.Value.ToString() == "-1")
                {
                    divHeader.Style.Add(HtmlTextWriterStyle.Display, "none");
                    grid.Columns["CompanyId"].Visible = true;
                }
                else
                {
                    divHeader.Style.Add(HtmlTextWriterStyle.Display, "block");
                    grid.Columns["CompanyId"].Visible = false;

                    CompaniesService _cService = new CompaniesService();
                    Companies c = _cService.GetByCompanyId(Convert.ToInt32(cmbCompanies.Value));

                    if (c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor)
                    {
                        lblCompanyType.Text = "Contractor";
                    }
                    if (c.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
                    {
                        lblCompanyType.Text = "Sub Contractor";
                    }
                    lblCompanyName.Text = c.CompanyName;
                    DisplaySafetyQualificationData(c.CompanyId);
                }
                if (cmbSites.Value != null)
                {
                    if (Convert.ToInt32(cmbSites.Value) < 0)
                    {
                        string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";
                        SqlDataSite1.SelectCommand = strQuery;
                        grid.Columns["SiteId"].Visible = true;

                    }
                    else
                    {
                        string strQuery = "select SiteId , SiteName from Sites where SiteId in (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToInt32(cmbSites.Value) + ")) order by SiteName";

                        SqlDataSite1.SelectCommand = strQuery;
                        grid.Columns["SiteId"].Visible = false;
                    }
                }
            }
            else 
            {
                if (Convert.ToInt32(cmbSites.SelectedItem.Value) == 0)
                    throw new Exception("Please select a valid site/region");
                else if (Convert.ToInt32(cmbCompanies.SelectedItem.Value) == 0)
                    throw new Exception("Please select a valid Company");
                else if(Convert.ToDateTime(deFrom.Date.ToString("yyyy-MM-dd")) > Convert.ToDateTime(deTo.Date.ToString("yyyy-MM-dd")))
                    throw new Exception("The 'From Date' must be earlier than 'To Date'");
            }

        }

        protected void deFrom_Init(object sender, EventArgs e)
        {
            DateTime dtFrom = DateTime.Now.AddDays(-7);
            deFrom.Date = new DateTime(dtFrom.Year, dtFrom.Month, dtFrom.Day);
        }

        protected void deTo_Init(object sender, EventArgs e)
        {
            DateTime dtTo = DateTime.Now;
            deTo.Date = new DateTime(dtTo.Year, dtTo.Month, dtTo.Day);
        }

        protected string GetUrlPath()
        {
            string imgChartPath = string.Empty;
            string[] arr = Request.Url.ToString().Split('/');
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (i > 0) imgChartPath = imgChartPath + "/";
                imgChartPath = imgChartPath + arr[i];
            }
            return imgChartPath;
        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {

            if (e.RowType == GridViewRowType.Data)
            {
                DataView dvCompetency = (DataView)sqldaCometency.Select(DataSourceSelectArguments.Empty);
                DataTable dtCompetency = dvCompetency.ToTable();
                for (int i = 0; i < dtCompetency.Rows.Count; i++)
                {
                    if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, dtCompetency.Rows[i][0].ToString())) == "")
                    {
                        int val = 1;
                        if (grid.Columns["CompanyId"].Visible == true) { val++; }
                        if (grid.Columns["SiteId"].Visible == true) { val++; }

                        e.Row.Cells[i + val].Text = "<img style='padding-left:16px' name='img1' src='" + GetUrlPath() + "/Images/redcross.gif' />";
                        

                        //if (grid.Columns["CompanyId"].Visible == false)
                        //{
                        //    e.Row.Cells[i + 1].Text = "<img style='padding-left:16px' name='img1' src='" + GetUrlPath() + "/Images/redcross.gif' />";
                        //}
                        //else
                        //{
                        //    e.Row.Cells[i + 2].Text = "<img style='padding-left:16px' name='img1' src='" + GetUrlPath() + "/Images/redcross.gif' />";
                        //}


                        //Image img1 = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "HyLink1") as Image;
                        //if (img1 != null)
                        //{
                        //    img1.Visible = true;
                        //    img1.ImageUrl = @"~/Images/redcross.gif";
                        //}

                    }
                }


                //GridViewBandColumn grdBandclm = grid.Columns["HeaderColumn"] as GridViewBandColumn;

                //for (int i = 0; i < grdBandclm.Columns.Count; i++)
                //{
                //    if (e.Row.Cells[i+2].Text == "")
                //    {
                //        e.Row.Cells[i+2].Text = "<img name='img1' src='/Images/redcross.gif' />";
                //    }
                //}


                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "OneInduction")) == "")
                //{
                //    Image imgOneInduction = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgOneInduction") as Image;
                //    imgOneInduction.Visible = true;
                //    imgOneInduction.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "ARPCRP")) == "")
                //{
                //    Image imgArpCrp = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgArpCrp") as Image;
                //    imgArpCrp.Visible = true;
                //    imgArpCrp.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "AreaInduction")) == "")
                //{
                //    Image imgAreaInduction = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgAreaInduction") as Image;
                //    imgAreaInduction.Visible = true;
                //    imgAreaInduction.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "CSE")) == "")
                //{
                //    Image imgCSE = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgCSE") as Image;
                //    imgCSE.Visible = true;
                //    imgCSE.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "LOTOV")) == "")
                //{
                //    Image imgLOTOV = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgLOTOV") as Image;
                //    imgLOTOV.Visible = true;
                //    imgLOTOV.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "Fall")) == "")
                //{
                //    Image imgFall = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgFall") as Image;
                //    imgFall.Visible = true;
                //    imgFall.ImageUrl = @"~/Images/redcross.gif";
                //}

                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "ME")) == "")
                //{
                //    Image imgME = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgME") as Image;
                //    imgME.Visible = true;
                //    imgME.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "MG")) == "")
                //{
                //    Image imgMG = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgMG") as Image;
                //    imgMG.Visible = true;
                //    imgMG.ImageUrl = @"~/Images/redcross.gif";
                //}
                //if (Convert.ToString(grid.GetRowValues(e.VisibleIndex, "Resp")) == "")
                //{
                //    Image imgResp = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgResp") as Image;
                //    imgResp.Visible = true;
                //    imgResp.ImageUrl = @"~/Images/redcross.gif";
                //}

            }
        }


        protected void DisplaySafetyQualificationData(int CompanyId)
        {
            #region Safety Qualification & Subcontractors
            int count = 0;

            CompaniesService _cService = new CompaniesService();
            Companies c = _cService.GetByCompanyId(Convert.ToInt32(cmbCompanies.Value));

            QuestionnaireWithLocationApprovalViewService qService = new QuestionnaireWithLocationApprovalViewService();
            QuestionnaireWithLocationApprovalViewFilters qFilters = new QuestionnaireWithLocationApprovalViewFilters();
            qFilters.Append(QuestionnaireWithLocationApprovalViewColumn.CompanyId, CompanyId.ToString());
            VList<QuestionnaireWithLocationApprovalView> qVlist = qService.GetPaged(qFilters.ToString(), "QuestionnaireId DESC", 0, 100, out count);

            if (count > 0)
            {
                //hlTL_SQ.Text = "View";

                bool requaling = false;
                if (qVlist[0].Status == (int)QuestionnaireStatusList.Incomplete)
                {
                    lblSafetyQValid.Text = "Safety Qualification - Incomplete";
                    if (count > 1) requaling = true;
                }
                if (qVlist[0].Status == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    lblSafetyQValid.Text = "Safety Qualification - Being Assessed";
                    if (count > 1) requaling = true;
                }


                if (qVlist[0].MainAssessmentValidTo != null && qVlist[0].Status != (int)QuestionnaireStatusList.AssessmentComplete)
                {
                    DateTime dt1 = (DateTime)qVlist[0].MainAssessmentValidTo;
                    lblSafetyQValid.Text = "Safety Qualification valid until " + dt1.ToString("dd/MM/yyyy");
                }

                bool done = false;
                int no = 0;
                int i = 0;
                while (i <= qVlist.Count - 1)
                {
                    if (!done)
                    {
                        if (qVlist[i] != null)
                        {
                            if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                            {
                                lblSafetyQValid.Text = "Safety Qualification - Assessment Complete";
                                if (qVlist[i].IsVerificationRequired && (c.CompanyStatus2Id == (int)CompanyStatus2List.Contractor))
                                {
                                    //Changed by Pankaj DIkshit for <DT 2473, 03-Aug-2012> condtn:- '&& qVlist[i].MainAssessmentValidTo >= DateTime.Now' added
                                    if (qVlist[i].LevelOfSupervision != "Direct" && qVlist[i].MainAssessmentValidTo >= DateTime.Now)
                                    {
                                        lblApprovedEng.Text = "Yes";
                                        //hlTL_SC.Text = "List";
                                        //hlTL_SC.NavigateUrl = String.Format("~/ListSubContractorsCompliance.aspx{0}", QueryStringModule.Encrypt("q=" + c.CompanyId));
                                        //gcTL_SC.Value = GetImageURL(TrafficLightColor2.Green);
                                    }
                                    else
                                    {
                                        lblApprovedEng.Text = "No";
                                    }
                                }

                                if (qVlist[i].MainAssessmentValidTo != null)
                                {
                                    if (qVlist[i].MainAssessmentValidTo > DateTime.Now)
                                    {
                                        DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                        if (qVlist[i].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                        {
                                            done = true;

                                            lblSafetyQValid.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy");
                                            if (requaling) lblSafetyQValid.Text += " (Currently Re-Qualifying)";
                                            // gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);

                                            lblSupReq.Text = qVlist[i].LevelOfSupervision;

                                            DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                            TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                            if (ts.Days <= 60)
                                            {
                                                // gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);
                                                lblSupReq.Text = qVlist[i].LevelOfSupervision;
                                                if (ts.Days == 1)
                                                {
                                                    lblSafetyQValid.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Day)";
                                                }
                                                else
                                                {
                                                    lblSafetyQValid.Text = "Safety Qualification valid until " + dt.ToString("dd/MM/yyyy") + " (Expires in " + ts.Days + " Days)";
                                                }
                                                no = i;
                                            }
                                        }
                                        else
                                        {
                                            //gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Yellow);

                                            if (qVlist[i + 1] != null)
                                            {
                                                if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete &&
                                                    c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                                                {
                                                    DateTime dtExpiry = Convert.ToDateTime(qVlist[i].MainAssessmentValidTo);
                                                    TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                                    if (ts.Days <= 60)
                                                    {
                                                        lblSupReq.Text = qVlist[i].LevelOfSupervision;
                                                        lblSafetyQValid.Text = "Safety Qualification Valid until " + dt.ToString("dd/MM/yyyy") + " - Expires in " + ts.Days + " Days.";
                                                        no = i;
                                                        done = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DateTime dt = (DateTime)qVlist[i].MainAssessmentValidTo;
                                        lblSafetyQValid.Text = "Safety Qualification Expired at " + dt.ToString("dd/MM/yyyy");
                                        //gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Red);
                                        if (qVlist.Count >= i + 2) //todo: check this...
                                        {
                                            if (qVlist[i + 1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                            {
                                                lblSupReq.Text = qVlist[i + 1].LevelOfSupervision;
                                            }
                                        }
                                        no = i;
                                        done = true;

                                        //Code add to display updated valid date for safty qualification
                                        //Author: Bishwajit Sahoo
                                        //Date: 23/10/2012
                                        //if (qVlist[0].ActionName == "SupplierQuestionnaireIncomplete" && qVlist[0].ActionDescription == "Supplier - Supplier Questionnaire - Resubmitting")
                                        if (qVlist[0].MainAssessmentValidTo != null && qVlist[0].Status != (int)QuestionnaireStatusList.AssessmentComplete)
                                        {
                                            DateTime dt1 = (DateTime)qVlist[0].MainAssessmentValidTo;
                                            lblSafetyQValid.Text = "Safety Qualification valid until " + dt1.ToString("dd/MM/yyyy");
                                            // gcTL_SQ.Value = GetImageURL(TrafficLightColor2.Green);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i++;
                }

            }
            else
            {
                lblSafetyQValid.Text = "";
            }


            #endregion
        }
    }
}