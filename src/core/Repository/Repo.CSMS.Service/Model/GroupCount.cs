﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.CSMS.Service.Model
{
    public class GroupCount
    {
        public object Name { get; set; }
        public int Count { get; set; }
    }
}
