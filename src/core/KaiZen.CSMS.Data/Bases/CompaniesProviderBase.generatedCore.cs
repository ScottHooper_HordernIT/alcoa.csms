﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompaniesProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Companies, KaiZen.CSMS.Entities.CompaniesKey>
	{		
		#region Get from Many To Many Relationship Functions
		#region GetByParentIdFromCompaniesRelationship
		
		/// <summary>
		///		Gets Companies objects from the datasource by ParentId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="_parentId"></param>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByParentIdFromCompaniesRelationship(System.Int32 _parentId)
		{
			int count = -1;
			return GetByParentIdFromCompaniesRelationship(null,_parentId, 0, int.MaxValue, out count);
			
		}
		
		/// <summary>
		///		Gets KaiZen.CSMS.Entities.Companies objects from the datasource by ParentId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of Companies objects.</returns>
		public TList<Companies> GetByParentIdFromCompaniesRelationship(System.Int32 _parentId, int start, int pageLength)
		{
			int count = -1;
			return GetByParentIdFromCompaniesRelationship(null, _parentId, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets Companies objects from the datasource by ParentId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByParentIdFromCompaniesRelationship(TransactionManager transactionManager, System.Int32 _parentId)
		{
			int count = -1;
			return GetByParentIdFromCompaniesRelationship(transactionManager, _parentId, 0, int.MaxValue, out count);
		}
		
		
		/// <summary>
		///		Gets Companies objects from the datasource by ParentId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByParentIdFromCompaniesRelationship(TransactionManager transactionManager, System.Int32 _parentId,int start, int pageLength)
		{
			int count = -1;
			return GetByParentIdFromCompaniesRelationship(transactionManager, _parentId, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets Companies objects from the datasource by ParentId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="_parentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByParentIdFromCompaniesRelationship(System.Int32 _parentId,int start, int pageLength, out int count)
		{
			
			return GetByParentIdFromCompaniesRelationship(null, _parentId, start, pageLength, out count);
		}


		/// <summary>
		///		Gets Companies objects from the datasource by ParentId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <param name="_parentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of Companies objects.</returns>
		public abstract TList<Companies> GetByParentIdFromCompaniesRelationship(TransactionManager transactionManager,System.Int32 _parentId, int start, int pageLength, out int count);
		
		#endregion GetByParentIdFromCompaniesRelationship
		
		#region GetByChildIdFromCompaniesRelationship
		
		/// <summary>
		///		Gets Companies objects from the datasource by ChildId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="_childId"></param>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByChildIdFromCompaniesRelationship(System.Int32 _childId)
		{
			int count = -1;
			return GetByChildIdFromCompaniesRelationship(null,_childId, 0, int.MaxValue, out count);
			
		}
		
		/// <summary>
		///		Gets KaiZen.CSMS.Entities.Companies objects from the datasource by ChildId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_childId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of Companies objects.</returns>
		public TList<Companies> GetByChildIdFromCompaniesRelationship(System.Int32 _childId, int start, int pageLength)
		{
			int count = -1;
			return GetByChildIdFromCompaniesRelationship(null, _childId, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets Companies objects from the datasource by ChildId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByChildIdFromCompaniesRelationship(TransactionManager transactionManager, System.Int32 _childId)
		{
			int count = -1;
			return GetByChildIdFromCompaniesRelationship(transactionManager, _childId, 0, int.MaxValue, out count);
		}
		
		
		/// <summary>
		///		Gets Companies objects from the datasource by ChildId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByChildIdFromCompaniesRelationship(TransactionManager transactionManager, System.Int32 _childId,int start, int pageLength)
		{
			int count = -1;
			return GetByChildIdFromCompaniesRelationship(transactionManager, _childId, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets Companies objects from the datasource by ChildId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of Companies objects.</returns>
		public TList<Companies> GetByChildIdFromCompaniesRelationship(System.Int32 _childId,int start, int pageLength, out int count)
		{
			
			return GetByChildIdFromCompaniesRelationship(null, _childId, start, pageLength, out count);
		}


		/// <summary>
		///		Gets Companies objects from the datasource by ChildId in the
		///		CompaniesRelationship table. Table Companies is related to table Companies
		///		through the (M:N) relationship defined in the CompaniesRelationship table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <param name="_childId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of Companies objects.</returns>
		public abstract TList<Companies> GetByChildIdFromCompaniesRelationship(TransactionManager transactionManager,System.Int32 _childId, int start, int pageLength, out int count);
		
		#endregion GetByChildIdFromCompaniesRelationship
		
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesKey key)
		{
			return Delete(transactionManager, key.CompanyId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companyId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companyId)
		{
			return Delete(null, _companyId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companyId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus key.
		///		FK_Companies_CompanyStatus Description: 
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatusId(System.Int32 _companyStatusId)
		{
			int count = -1;
			return GetByCompanyStatusId(_companyStatusId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus key.
		///		FK_Companies_CompanyStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		/// <remarks></remarks>
		public TList<Companies> GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId)
		{
			int count = -1;
			return GetByCompanyStatusId(transactionManager, _companyStatusId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus key.
		///		FK_Companies_CompanyStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusId(transactionManager, _companyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus key.
		///		fkCompaniesCompanyStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatusId(System.Int32 _companyStatusId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyStatusId(null, _companyStatusId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus key.
		///		fkCompaniesCompanyStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatusId(System.Int32 _companyStatusId, int start, int pageLength,out int count)
		{
			return GetByCompanyStatusId(null, _companyStatusId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus key.
		///		FK_Companies_CompanyStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public abstract TList<Companies> GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus2 key.
		///		FK_Companies_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="_companyStatus2Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatus2Id(System.Int32 _companyStatus2Id)
		{
			int count = -1;
			return GetByCompanyStatus2Id(_companyStatus2Id, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus2 key.
		///		FK_Companies_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatus2Id"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		/// <remarks></remarks>
		public TList<Companies> GetByCompanyStatus2Id(TransactionManager transactionManager, System.Int32 _companyStatus2Id)
		{
			int count = -1;
			return GetByCompanyStatus2Id(transactionManager, _companyStatus2Id, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus2 key.
		///		FK_Companies_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatus2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatus2Id(TransactionManager transactionManager, System.Int32 _companyStatus2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatus2Id(transactionManager, _companyStatus2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus2 key.
		///		fkCompaniesCompanyStatus2 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyStatus2Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatus2Id(System.Int32 _companyStatus2Id, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyStatus2Id(null, _companyStatus2Id, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus2 key.
		///		fkCompaniesCompanyStatus2 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyStatus2Id"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByCompanyStatus2Id(System.Int32 _companyStatus2Id, int start, int pageLength,out int count)
		{
			return GetByCompanyStatus2Id(null, _companyStatus2Id, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_CompanyStatus2 key.
		///		FK_Companies_CompanyStatus2 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatus2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public abstract TList<Companies> GetByCompanyStatus2Id(TransactionManager transactionManager, System.Int32 _companyStatus2Id, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_ModifiedByUserId key.
		///		FK_Companies_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(_modifiedbyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_ModifiedByUserId key.
		///		FK_Companies_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		/// <remarks></remarks>
		public TList<Companies> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_ModifiedByUserId key.
		///		FK_Companies_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_ModifiedByUserId key.
		///		fkCompaniesModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_ModifiedByUserId key.
		///		fkCompaniesModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public TList<Companies> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Companies_ModifiedByUserId key.
		///		FK_Companies_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Companies objects.</returns>
		public abstract TList<Companies> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Companies Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesKey key, int start, int pageLength)
		{
			return GetByCompanyId(transactionManager, key.CompanyId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Companies index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(null,_companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Companies index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Companies index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Companies index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Companies index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyId(System.Int32 _companyId, int start, int pageLength, out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Companies index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Companies GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyName(System.String _companyName)
		{
			int count = -1;
			return GetByCompanyName(null,_companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyName(System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyName(null, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyName(TransactionManager transactionManager, System.String _companyName)
		{
			int count = -1;
			return GetByCompanyName(transactionManager, _companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyName(TransactionManager transactionManager, System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyName(transactionManager, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyName(System.String _companyName, int start, int pageLength, out int count)
		{
			return GetByCompanyName(null, _companyName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Companies GetByCompanyName(TransactionManager transactionManager, System.String _companyName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Companies_IProcSupplierNo index.
		/// </summary>
		/// <param name="_iprocSupplierNo"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByIprocSupplierNo(System.String _iprocSupplierNo)
		{
			int count = -1;
			return GetByIprocSupplierNo(null,_iprocSupplierNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies_IProcSupplierNo index.
		/// </summary>
		/// <param name="_iprocSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByIprocSupplierNo(System.String _iprocSupplierNo, int start, int pageLength)
		{
			int count = -1;
			return GetByIprocSupplierNo(null, _iprocSupplierNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies_IProcSupplierNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iprocSupplierNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByIprocSupplierNo(TransactionManager transactionManager, System.String _iprocSupplierNo)
		{
			int count = -1;
			return GetByIprocSupplierNo(transactionManager, _iprocSupplierNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies_IProcSupplierNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iprocSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByIprocSupplierNo(TransactionManager transactionManager, System.String _iprocSupplierNo, int start, int pageLength)
		{
			int count = -1;
			return GetByIprocSupplierNo(transactionManager, _iprocSupplierNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies_IProcSupplierNo index.
		/// </summary>
		/// <param name="_iprocSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByIprocSupplierNo(System.String _iprocSupplierNo, int start, int pageLength, out int count)
		{
			return GetByIprocSupplierNo(null, _iprocSupplierNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies_IProcSupplierNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iprocSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public abstract TList<Companies> GetByIprocSupplierNo(TransactionManager transactionManager, System.String _iprocSupplierNo, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanyAbn index.
		/// </summary>
		/// <param name="_companyAbn"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyAbn(System.String _companyAbn)
		{
			int count = -1;
			return GetByCompanyAbn(null,_companyAbn, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyAbn index.
		/// </summary>
		/// <param name="_companyAbn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyAbn(System.String _companyAbn, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyAbn(null, _companyAbn, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyAbn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyAbn"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyAbn(TransactionManager transactionManager, System.String _companyAbn)
		{
			int count = -1;
			return GetByCompanyAbn(transactionManager, _companyAbn, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyAbn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyAbn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyAbn(TransactionManager transactionManager, System.String _companyAbn, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyAbn(transactionManager, _companyAbn, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyAbn index.
		/// </summary>
		/// <param name="_companyAbn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public KaiZen.CSMS.Entities.Companies GetByCompanyAbn(System.String _companyAbn, int start, int pageLength, out int count)
		{
			return GetByCompanyAbn(null, _companyAbn, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyAbn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyAbn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Companies"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Companies GetByCompanyAbn(TransactionManager transactionManager, System.String _companyAbn, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EHSConsultantId index.
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByEhsConsultantId(System.Int32? _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(null,_ehsConsultantId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultantId index.
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByEhsConsultantId(System.Int32? _ehsConsultantId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultantId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultantId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultantId index.
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByEhsConsultantId(System.Int32? _ehsConsultantId, int start, int pageLength, out int count)
		{
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultantId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public abstract TList<Companies> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Companies index.
		/// </summary>
		/// <param name="_companyNameEbi"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByCompanyNameEbi(System.String _companyNameEbi)
		{
			int count = -1;
			return GetByCompanyNameEbi(null,_companyNameEbi, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies index.
		/// </summary>
		/// <param name="_companyNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByCompanyNameEbi(System.String _companyNameEbi, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyNameEbi(null, _companyNameEbi, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNameEbi"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByCompanyNameEbi(TransactionManager transactionManager, System.String _companyNameEbi)
		{
			int count = -1;
			return GetByCompanyNameEbi(transactionManager, _companyNameEbi, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByCompanyNameEbi(TransactionManager transactionManager, System.String _companyNameEbi, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyNameEbi(transactionManager, _companyNameEbi, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies index.
		/// </summary>
		/// <param name="_companyNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public TList<Companies> GetByCompanyNameEbi(System.String _companyNameEbi, int start, int pageLength, out int count)
		{
			return GetByCompanyNameEbi(null, _companyNameEbi, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Companies index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Companies&gt;"/> class.</returns>
		public abstract TList<Companies> GetByCompanyNameEbi(TransactionManager transactionManager, System.String _companyNameEbi, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Companies_GetCompanyNameList 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameList()
		{
			return GetCompanyNameList(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameList(int start, int pageLength)
		{
			return GetCompanyNameList(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameList(TransactionManager transactionManager)
		{
			return GetCompanyNameList(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyNameList(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetActive 
		
		/// <summary>
		///	This method wrap the '_Companies_GetActive' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetActive()
		{
			return GetActive(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetActive' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetActive(int start, int pageLength)
		{
			return GetActive(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetActive' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetActive(TransactionManager transactionManager)
		{
			return GetActive(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetActive' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetActive(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_WithSq_Latest 
		
		/// <summary>
		///	This method wrap the '_Companies_WithSq_Latest' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WithSq_Latest()
		{
			return WithSq_Latest(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_WithSq_Latest' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WithSq_Latest(int start, int pageLength)
		{
			return WithSq_Latest(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_WithSq_Latest' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WithSq_Latest(TransactionManager transactionManager)
		{
			return WithSq_Latest(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_WithSq_Latest' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet WithSq_Latest(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetCompanyNameListInSqDatabase_SubContractors 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase_SubContractors' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameListInSqDatabase_SubContractors()
		{
			return GetCompanyNameListInSqDatabase_SubContractors(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase_SubContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameListInSqDatabase_SubContractors(int start, int pageLength)
		{
			return GetCompanyNameListInSqDatabase_SubContractors(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase_SubContractors' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameListInSqDatabase_SubContractors(TransactionManager transactionManager)
		{
			return GetCompanyNameListInSqDatabase_SubContractors(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase_SubContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyNameListInSqDatabase_SubContractors(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetCompanyNameList_SubContractors 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList_SubContractors' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameList_SubContractors()
		{
			return GetCompanyNameList_SubContractors(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList_SubContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameList_SubContractors(int start, int pageLength)
		{
			return GetCompanyNameList_SubContractors(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList_SubContractors' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameList_SubContractors(TransactionManager transactionManager)
		{
			return GetCompanyNameList_SubContractors(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameList_SubContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyNameList_SubContractors(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_EHSConsultants 
		
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet EHSConsultants()
		{
			return EHSConsultants(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet EHSConsultants(int start, int pageLength)
		{
			return EHSConsultants(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet EHSConsultants(TransactionManager transactionManager)
		{
			return EHSConsultants(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet EHSConsultants(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetCompanyIdByCompanyName 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyIdByCompanyName' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyIdByCompanyName(System.String companyName)
		{
			return GetCompanyIdByCompanyName(null, 0, int.MaxValue , companyName);
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyIdByCompanyName' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyIdByCompanyName(int start, int pageLength, System.String companyName)
		{
			return GetCompanyIdByCompanyName(null, start, pageLength , companyName);
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyIdByCompanyName' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyIdByCompanyName(TransactionManager transactionManager, System.String companyName)
		{
			return GetCompanyIdByCompanyName(transactionManager, 0, int.MaxValue , companyName);
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyIdByCompanyName' stored procedure. 
		/// </summary>
		/// <param name="companyName"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyIdByCompanyName(TransactionManager transactionManager, int start, int pageLength , System.String companyName);
		
		#endregion
		
		#region _Companies_EHSConsultants2 
		
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants2' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet EHSConsultants2()
		{
			return EHSConsultants2(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants2' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet EHSConsultants2(int start, int pageLength)
		{
			return EHSConsultants2(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants2' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet EHSConsultants2(TransactionManager transactionManager)
		{
			return EHSConsultants2(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_EHSConsultants2' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet EHSConsultants2(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetIndirectContractors 
		
		/// <summary>
		///	This method wrap the '_Companies_GetIndirectContractors' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetIndirectContractors()
		{
			return GetIndirectContractors(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetIndirectContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetIndirectContractors(int start, int pageLength)
		{
			return GetIndirectContractors(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetIndirectContractors' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetIndirectContractors(TransactionManager transactionManager)
		{
			return GetIndirectContractors(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetIndirectContractors' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetIndirectContractors(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetCountSites_ByRegion 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCountSites_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCountSites_ByRegion(System.Int32? companyId, System.Int32? regionId)
		{
			return GetCountSites_ByRegion(null, 0, int.MaxValue , companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCountSites_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCountSites_ByRegion(int start, int pageLength, System.Int32? companyId, System.Int32? regionId)
		{
			return GetCountSites_ByRegion(null, start, pageLength , companyId, regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCountSites_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCountSites_ByRegion(TransactionManager transactionManager, System.Int32? companyId, System.Int32? regionId)
		{
			return GetCountSites_ByRegion(transactionManager, 0, int.MaxValue , companyId, regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCountSites_ByRegion' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCountSites_ByRegion(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? regionId);
		
		#endregion
		
		#region _Companies_GetCompanyNameListInSqDatabase 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameListInSqDatabase()
		{
			return GetCompanyNameListInSqDatabase(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameListInSqDatabase(int start, int pageLength)
		{
			return GetCompanyNameListInSqDatabase(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameListInSqDatabase(TransactionManager transactionManager)
		{
			return GetCompanyNameListInSqDatabase(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameListInSqDatabase' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyNameListInSqDatabase(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetAll_CompanyName_ExcludeAlcoa 
		
		/// <summary>
		///	This method wrap the '_Companies_GetAll_CompanyName_ExcludeAlcoa' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_CompanyName_ExcludeAlcoa()
		{
			return GetAll_CompanyName_ExcludeAlcoa(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetAll_CompanyName_ExcludeAlcoa' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_CompanyName_ExcludeAlcoa(int start, int pageLength)
		{
			return GetAll_CompanyName_ExcludeAlcoa(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetAll_CompanyName_ExcludeAlcoa' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAll_CompanyName_ExcludeAlcoa(TransactionManager transactionManager)
		{
			return GetAll_CompanyName_ExcludeAlcoa(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetAll_CompanyName_ExcludeAlcoa' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAll_CompanyName_ExcludeAlcoa(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Companies_GetCompanyNameByIprocSupplierNo 
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameByIprocSupplierNo' stored procedure. 
		/// </summary>
		/// <param name="iprocSupplierNo"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameByIprocSupplierNo(System.String iprocSupplierNo)
		{
			return GetCompanyNameByIprocSupplierNo(null, 0, int.MaxValue , iprocSupplierNo);
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameByIprocSupplierNo' stored procedure. 
		/// </summary>
		/// <param name="iprocSupplierNo"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameByIprocSupplierNo(int start, int pageLength, System.String iprocSupplierNo)
		{
			return GetCompanyNameByIprocSupplierNo(null, start, pageLength , iprocSupplierNo);
		}
				
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameByIprocSupplierNo' stored procedure. 
		/// </summary>
		/// <param name="iprocSupplierNo"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyNameByIprocSupplierNo(TransactionManager transactionManager, System.String iprocSupplierNo)
		{
			return GetCompanyNameByIprocSupplierNo(transactionManager, 0, int.MaxValue , iprocSupplierNo);
		}
		
		/// <summary>
		///	This method wrap the '_Companies_GetCompanyNameByIprocSupplierNo' stored procedure. 
		/// </summary>
		/// <param name="iprocSupplierNo"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyNameByIprocSupplierNo(TransactionManager transactionManager, int start, int pageLength , System.String iprocSupplierNo);
		
		#endregion
		
		#region _Companies_WithSq 
		
		/// <summary>
		///	This method wrap the '_Companies_WithSq' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WithSq()
		{
			return WithSq(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_WithSq' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WithSq(int start, int pageLength)
		{
			return WithSq(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Companies_WithSq' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet WithSq(TransactionManager transactionManager)
		{
			return WithSq(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Companies_WithSq' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet WithSq(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Companies&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Companies&gt;"/></returns>
		public static TList<Companies> Fill(IDataReader reader, TList<Companies> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Companies c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Companies")
					.Append("|").Append((System.Int32)reader[((int)CompaniesColumn.CompanyId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Companies>(
					key.ToString(), // EntityTrackingKey
					"Companies",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Companies();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanyId = (System.Int32)reader[((int)CompaniesColumn.CompanyId - 1)];
					c.CompanyName = (System.String)reader[((int)CompaniesColumn.CompanyName - 1)];
					c.CompanyNameEbi = (reader.IsDBNull(((int)CompaniesColumn.CompanyNameEbi - 1)))?null:(System.String)reader[((int)CompaniesColumn.CompanyNameEbi - 1)];
					c.CompanyAbn = (System.String)reader[((int)CompaniesColumn.CompanyAbn - 1)];
					c.PhoneNo = (reader.IsDBNull(((int)CompaniesColumn.PhoneNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.PhoneNo - 1)];
					c.FaxNo = (reader.IsDBNull(((int)CompaniesColumn.FaxNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.FaxNo - 1)];
					c.MobNo = (reader.IsDBNull(((int)CompaniesColumn.MobNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.MobNo - 1)];
					c.AddressBusiness = (reader.IsDBNull(((int)CompaniesColumn.AddressBusiness - 1)))?null:(System.String)reader[((int)CompaniesColumn.AddressBusiness - 1)];
					c.AddressPostal = (reader.IsDBNull(((int)CompaniesColumn.AddressPostal - 1)))?null:(System.String)reader[((int)CompaniesColumn.AddressPostal - 1)];
					c.ProcurementSupplierNo = (reader.IsDBNull(((int)CompaniesColumn.ProcurementSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.ProcurementSupplierNo - 1)];
					c.EhsConsultantId = (reader.IsDBNull(((int)CompaniesColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)CompaniesColumn.EhsConsultantId - 1)];
					c.ModifiedbyUserId = (System.Int32)reader[((int)CompaniesColumn.ModifiedbyUserId - 1)];
					c.IprocSupplierNo = (reader.IsDBNull(((int)CompaniesColumn.IprocSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.IprocSupplierNo - 1)];
					c.CompanyStatusId = (System.Int32)reader[((int)CompaniesColumn.CompanyStatusId - 1)];
					c.StartDate = (reader.IsDBNull(((int)CompaniesColumn.StartDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesColumn.StartDate - 1)];
					c.EndDate = (reader.IsDBNull(((int)CompaniesColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesColumn.EndDate - 1)];
					c.CompanyStatus2Id = (System.Int32)reader[((int)CompaniesColumn.CompanyStatus2Id - 1)];
					c.ContactsLastUpdated = (reader.IsDBNull(((int)CompaniesColumn.ContactsLastUpdated - 1)))?null:(System.DateTime?)reader[((int)CompaniesColumn.ContactsLastUpdated - 1)];
					c.Deactivated = (reader.IsDBNull(((int)CompaniesColumn.Deactivated - 1)))?null:(System.Boolean?)reader[((int)CompaniesColumn.Deactivated - 1)];
					c.IhsContCompanyCode = (reader.IsDBNull(((int)CompaniesColumn.IhsContCompanyCode - 1)))?null:(System.Int32?)reader[((int)CompaniesColumn.IhsContCompanyCode - 1)];
					c.IhsContCompanyField = (reader.IsDBNull(((int)CompaniesColumn.IhsContCompanyField - 1)))?null:(System.String)reader[((int)CompaniesColumn.IhsContCompanyField - 1)];
					c.IhsLocCode = (reader.IsDBNull(((int)CompaniesColumn.IhsLocCode - 1)))?null:(System.String)reader[((int)CompaniesColumn.IhsLocCode - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Companies"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Companies"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Companies entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanyId = (System.Int32)reader[((int)CompaniesColumn.CompanyId - 1)];
			entity.CompanyName = (System.String)reader[((int)CompaniesColumn.CompanyName - 1)];
			entity.CompanyNameEbi = (reader.IsDBNull(((int)CompaniesColumn.CompanyNameEbi - 1)))?null:(System.String)reader[((int)CompaniesColumn.CompanyNameEbi - 1)];
			entity.CompanyAbn = (System.String)reader[((int)CompaniesColumn.CompanyAbn - 1)];
			entity.PhoneNo = (reader.IsDBNull(((int)CompaniesColumn.PhoneNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.PhoneNo - 1)];
			entity.FaxNo = (reader.IsDBNull(((int)CompaniesColumn.FaxNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.FaxNo - 1)];
			entity.MobNo = (reader.IsDBNull(((int)CompaniesColumn.MobNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.MobNo - 1)];
			entity.AddressBusiness = (reader.IsDBNull(((int)CompaniesColumn.AddressBusiness - 1)))?null:(System.String)reader[((int)CompaniesColumn.AddressBusiness - 1)];
			entity.AddressPostal = (reader.IsDBNull(((int)CompaniesColumn.AddressPostal - 1)))?null:(System.String)reader[((int)CompaniesColumn.AddressPostal - 1)];
			entity.ProcurementSupplierNo = (reader.IsDBNull(((int)CompaniesColumn.ProcurementSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.ProcurementSupplierNo - 1)];
			entity.EhsConsultantId = (reader.IsDBNull(((int)CompaniesColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)CompaniesColumn.EhsConsultantId - 1)];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)CompaniesColumn.ModifiedbyUserId - 1)];
			entity.IprocSupplierNo = (reader.IsDBNull(((int)CompaniesColumn.IprocSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesColumn.IprocSupplierNo - 1)];
			entity.CompanyStatusId = (System.Int32)reader[((int)CompaniesColumn.CompanyStatusId - 1)];
			entity.StartDate = (reader.IsDBNull(((int)CompaniesColumn.StartDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesColumn.StartDate - 1)];
			entity.EndDate = (reader.IsDBNull(((int)CompaniesColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesColumn.EndDate - 1)];
			entity.CompanyStatus2Id = (System.Int32)reader[((int)CompaniesColumn.CompanyStatus2Id - 1)];
			entity.ContactsLastUpdated = (reader.IsDBNull(((int)CompaniesColumn.ContactsLastUpdated - 1)))?null:(System.DateTime?)reader[((int)CompaniesColumn.ContactsLastUpdated - 1)];
			entity.Deactivated = (reader.IsDBNull(((int)CompaniesColumn.Deactivated - 1)))?null:(System.Boolean?)reader[((int)CompaniesColumn.Deactivated - 1)];
			entity.IhsContCompanyCode = (reader.IsDBNull(((int)CompaniesColumn.IhsContCompanyCode - 1)))?null:(System.Int32?)reader[((int)CompaniesColumn.IhsContCompanyCode - 1)];
			entity.IhsContCompanyField = (reader.IsDBNull(((int)CompaniesColumn.IhsContCompanyField - 1)))?null:(System.String)reader[((int)CompaniesColumn.IhsContCompanyField - 1)];
			entity.IhsLocCode = (reader.IsDBNull(((int)CompaniesColumn.IhsLocCode - 1)))?null:(System.String)reader[((int)CompaniesColumn.IhsLocCode - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Companies"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Companies"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Companies entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (System.String)dataRow["CompanyName"];
			entity.CompanyNameEbi = Convert.IsDBNull(dataRow["CompanyNameEbi"]) ? null : (System.String)dataRow["CompanyNameEbi"];
			entity.CompanyAbn = (System.String)dataRow["CompanyAbn"];
			entity.PhoneNo = Convert.IsDBNull(dataRow["PhoneNo"]) ? null : (System.String)dataRow["PhoneNo"];
			entity.FaxNo = Convert.IsDBNull(dataRow["FaxNo"]) ? null : (System.String)dataRow["FaxNo"];
			entity.MobNo = Convert.IsDBNull(dataRow["MobNo"]) ? null : (System.String)dataRow["MobNo"];
			entity.AddressBusiness = Convert.IsDBNull(dataRow["AddressBusiness"]) ? null : (System.String)dataRow["AddressBusiness"];
			entity.AddressPostal = Convert.IsDBNull(dataRow["AddressPostal"]) ? null : (System.String)dataRow["AddressPostal"];
			entity.ProcurementSupplierNo = Convert.IsDBNull(dataRow["ProcurementSupplierNo"]) ? null : (System.String)dataRow["ProcurementSupplierNo"];
			entity.EhsConsultantId = Convert.IsDBNull(dataRow["EHSConsultantId"]) ? null : (System.Int32?)dataRow["EHSConsultantId"];
			entity.ModifiedbyUserId = (System.Int32)dataRow["ModifiedbyUserId"];
			entity.IprocSupplierNo = Convert.IsDBNull(dataRow["IprocSupplierNo"]) ? null : (System.String)dataRow["IprocSupplierNo"];
			entity.CompanyStatusId = (System.Int32)dataRow["CompanyStatusId"];
			entity.StartDate = Convert.IsDBNull(dataRow["StartDate"]) ? null : (System.DateTime?)dataRow["StartDate"];
			entity.EndDate = Convert.IsDBNull(dataRow["EndDate"]) ? null : (System.DateTime?)dataRow["EndDate"];
			entity.CompanyStatus2Id = (System.Int32)dataRow["CompanyStatus2Id"];
			entity.ContactsLastUpdated = Convert.IsDBNull(dataRow["ContactsLastUpdated"]) ? null : (System.DateTime?)dataRow["ContactsLastUpdated"];
			entity.Deactivated = Convert.IsDBNull(dataRow["Deactivated"]) ? null : (System.Boolean?)dataRow["Deactivated"];
			entity.IhsContCompanyCode = Convert.IsDBNull(dataRow["IhsContCompanyCode"]) ? null : (System.Int32?)dataRow["IhsContCompanyCode"];
			entity.IhsContCompanyField = Convert.IsDBNull(dataRow["IhsContCompanyField"]) ? null : (System.String)dataRow["IhsContCompanyField"];
			entity.IhsLocCode = Convert.IsDBNull(dataRow["IhsLocCode"]) ? null : (System.String)dataRow["IhsLocCode"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Companies"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Companies Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Companies entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyStatusIdSource	
			if (CanDeepLoad(entity, "CompanyStatus|CompanyStatusIdSource", deepLoadType, innerList) 
				&& entity.CompanyStatusIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyStatusId;
				CompanyStatus tmpEntity = EntityManager.LocateEntity<CompanyStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanyStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyStatusIdSource = tmpEntity;
				else
					entity.CompanyStatusIdSource = DataRepository.CompanyStatusProvider.GetByCompanyStatusId(transactionManager, entity.CompanyStatusId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyStatusIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanyStatusProvider.DeepLoad(transactionManager, entity.CompanyStatusIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyStatusIdSource

			#region CompanyStatus2IdSource	
			if (CanDeepLoad(entity, "CompanyStatus2|CompanyStatus2IdSource", deepLoadType, innerList) 
				&& entity.CompanyStatus2IdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyStatus2Id;
				CompanyStatus2 tmpEntity = EntityManager.LocateEntity<CompanyStatus2>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanyStatus2), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyStatus2IdSource = tmpEntity;
				else
					entity.CompanyStatus2IdSource = DataRepository.CompanyStatus2Provider.GetByCompanyStatusId(transactionManager, entity.CompanyStatus2Id);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatus2IdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyStatus2IdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanyStatus2Provider.DeepLoad(transactionManager, entity.CompanyStatus2IdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyStatus2IdSource

			#region ModifiedbyUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedbyUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedbyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedbyUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedbyUserIdSource = tmpEntity;
				else
					entity.ModifiedbyUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedbyUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedbyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedbyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedbyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedbyUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCompanyId methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanyStatusChangeApprovalCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusChangeApprovalCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanyStatusChangeApprovalCollection = DataRepository.CompanyStatusChangeApprovalProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CompanyStatusChangeApprovalCollection.Count > 0)
				{
					deepHandles.Add("CompanyStatusChangeApprovalCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanyStatusChangeApproval>) DataRepository.CompanyStatusChangeApprovalProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ContactsContractorsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContactsContractors>|ContactsContractorsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactsContractorsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContactsContractorsCollection = DataRepository.ContactsContractorsProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.ContactsContractorsCollection.Count > 0)
				{
					deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContactsContractors>) DataRepository.ContactsContractorsProvider.DeepLoad,
						new object[] { transactionManager, entity.ContactsContractorsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region KpiCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Kpi>|KpiCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'KpiCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.KpiCollection = DataRepository.KpiProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.KpiCollection.Count > 0)
				{
					deepHandles.Add("KpiCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Kpi>) DataRepository.KpiProvider.DeepLoad,
						new object[] { transactionManager, entity.KpiCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryExceptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryException>|CompanySiteCategoryExceptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryExceptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryExceptionCollection = DataRepository.CompanySiteCategoryExceptionProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CompanySiteCategoryExceptionCollection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryExceptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryException>) DataRepository.CompanySiteCategoryExceptionProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryExceptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FileDbCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileDb>|FileDbCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileDbCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileDbCollection = DataRepository.FileDbProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.FileDbCollection.Count > 0)
				{
					deepHandles.Add("FileDbCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileDb>) DataRepository.FileDbProvider.DeepLoad,
						new object[] { transactionManager, entity.FileDbCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompaniesRelationshipCollectionGetByChildId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesRelationship>|CompaniesRelationshipCollectionGetByChildId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesRelationshipCollectionGetByChildId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesRelationshipCollectionGetByChildId = DataRepository.CompaniesRelationshipProvider.GetByChildId(transactionManager, entity.CompanyId);

				if (deep && entity.CompaniesRelationshipCollectionGetByChildId.Count > 0)
				{
					deepHandles.Add("CompaniesRelationshipCollectionGetByChildId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesRelationship>) DataRepository.CompaniesRelationshipProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesRelationshipCollectionGetByChildId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompaniesEhsimsMapCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesEhsimsMap>|CompaniesEhsimsMapCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesEhsimsMapCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesEhsimsMapCollection = DataRepository.CompaniesEhsimsMapProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CompaniesEhsimsMapCollection.Count > 0)
				{
					deepHandles.Add("CompaniesEhsimsMapCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesEhsimsMap>) DataRepository.CompaniesEhsimsMapProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesEhsimsMapCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryException2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryException2>|CompanySiteCategoryException2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryException2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryException2Collection = DataRepository.CompanySiteCategoryException2Provider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CompanySiteCategoryException2Collection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryException2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryException2>) DataRepository.CompanySiteCategoryException2Provider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryException2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region UsersCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Users>|UsersCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UsersCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.UsersCollection = DataRepository.UsersProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.UsersCollection.Count > 0)
				{
					deepHandles.Add("UsersCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Users>) DataRepository.UsersProvider.DeepLoad,
						new object[] { transactionManager, entity.UsersCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SafetyPlansSeResponsesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SafetyPlansSeResponses>|SafetyPlansSeResponsesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SafetyPlansSeResponsesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SafetyPlansSeResponsesCollection = DataRepository.SafetyPlansSeResponsesProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.SafetyPlansSeResponsesCollection.Count > 0)
				{
					deepHandles.Add("SafetyPlansSeResponsesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SafetyPlansSeResponses>) DataRepository.SafetyPlansSeResponsesProvider.DeepLoad,
						new object[] { transactionManager, entity.SafetyPlansSeResponsesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CsaCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Csa>|CsaCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsaCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsaCollection = DataRepository.CsaProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CsaCollection.Count > 0)
				{
					deepHandles.Add("CsaCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Csa>) DataRepository.CsaProvider.DeepLoad,
						new object[] { transactionManager, entity.CsaCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireContractorRsCollectionGetByContractorCompanyId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireContractorRs>|QuestionnaireContractorRsCollectionGetByContractorCompanyId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireContractorRsCollectionGetByContractorCompanyId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId = DataRepository.QuestionnaireContractorRsProvider.GetByContractorCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId.Count > 0)
				{
					deepHandles.Add("QuestionnaireContractorRsCollectionGetByContractorCompanyId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireContractorRs>) DataRepository.QuestionnaireContractorRsProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FileDbMedicalTrainingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FileDbMedicalTraining>|FileDbMedicalTrainingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileDbMedicalTrainingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FileDbMedicalTrainingCollection = DataRepository.FileDbMedicalTrainingProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.FileDbMedicalTrainingCollection.Count > 0)
				{
					deepHandles.Add("FileDbMedicalTrainingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FileDbMedicalTraining>) DataRepository.FileDbMedicalTrainingProvider.DeepLoad,
						new object[] { transactionManager, entity.FileDbMedicalTrainingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ResidentialCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Residential>|ResidentialCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ResidentialCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ResidentialCollection = DataRepository.ResidentialProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.ResidentialCollection.Count > 0)
				{
					deepHandles.Add("ResidentialCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Residential>) DataRepository.ResidentialProvider.DeepLoad,
						new object[] { transactionManager, entity.ResidentialCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SqExemptionCollectionGetByCompanyId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SqExemption>|SqExemptionCollectionGetByCompanyId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SqExemptionCollectionGetByCompanyId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SqExemptionCollectionGetByCompanyId = DataRepository.SqExemptionProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.SqExemptionCollectionGetByCompanyId.Count > 0)
				{
					deepHandles.Add("SqExemptionCollectionGetByCompanyId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SqExemption>) DataRepository.SqExemptionProvider.DeepLoad,
						new object[] { transactionManager, entity.SqExemptionCollectionGetByCompanyId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ParentIdCompaniesCollection_From_CompaniesRelationship
			// RelationshipType.ManyToMany
			if (CanDeepLoad(entity, "List<Companies>|ParentIdCompaniesCollection_From_CompaniesRelationship", deepLoadType, innerList))
			{
				entity.ParentIdCompaniesCollection_From_CompaniesRelationship = DataRepository.CompaniesProvider.GetByChildIdFromCompaniesRelationship(transactionManager, entity.CompanyId);			 
		
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentIdCompaniesCollection_From_CompaniesRelationship' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentIdCompaniesCollection_From_CompaniesRelationship != null)
				{
					deepHandles.Add("ParentIdCompaniesCollection_From_CompaniesRelationship",
						new KeyValuePair<Delegate, object>((DeepLoadHandle< Companies >) DataRepository.CompaniesProvider.DeepLoad,
						new object[] { transactionManager, entity.ParentIdCompaniesCollection_From_CompaniesRelationship, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion
			
			
			
			#region QuestionnaireContractorRsCollectionGetBySubContractorCompanyId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireContractorRs>|QuestionnaireContractorRsCollectionGetBySubContractorCompanyId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireContractorRsCollectionGetBySubContractorCompanyId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId = DataRepository.QuestionnaireContractorRsProvider.GetBySubContractorCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId.Count > 0)
				{
					deepHandles.Add("QuestionnaireContractorRsCollectionGetBySubContractorCompanyId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireContractorRs>) DataRepository.QuestionnaireContractorRsProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SqExemptionCollectionGetByRequestedByCompanyId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SqExemption>|SqExemptionCollectionGetByRequestedByCompanyId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SqExemptionCollectionGetByRequestedByCompanyId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SqExemptionCollectionGetByRequestedByCompanyId = DataRepository.SqExemptionProvider.GetByRequestedByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.SqExemptionCollectionGetByRequestedByCompanyId.Count > 0)
				{
					deepHandles.Add("SqExemptionCollectionGetByRequestedByCompanyId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SqExemption>) DataRepository.SqExemptionProvider.DeepLoad,
						new object[] { transactionManager, entity.SqExemptionCollectionGetByRequestedByCompanyId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompaniesRelationshipCollectionGetByParentId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesRelationship>|CompaniesRelationshipCollectionGetByParentId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesRelationshipCollectionGetByParentId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesRelationshipCollectionGetByParentId = DataRepository.CompaniesRelationshipProvider.GetByParentId(transactionManager, entity.CompanyId);

				if (deep && entity.CompaniesRelationshipCollectionGetByParentId.Count > 0)
				{
					deepHandles.Add("CompaniesRelationshipCollectionGetByParentId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesRelationship>) DataRepository.CompaniesRelationshipProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesRelationshipCollectionGetByParentId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryStandardCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollection = DataRepository.CompanySiteCategoryStandardProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CompanySiteCategoryStandardCollection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdHocRadarItems2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdHocRadarItems2>|AdHocRadarItems2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarItems2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdHocRadarItems2Collection = DataRepository.AdHocRadarItems2Provider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.AdHocRadarItems2Collection.Count > 0)
				{
					deepHandles.Add("AdHocRadarItems2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdHocRadarItems2>) DataRepository.AdHocRadarItems2Provider.DeepLoad,
						new object[] { transactionManager, entity.AdHocRadarItems2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ChildIdCompaniesCollection_From_CompaniesRelationship
			// RelationshipType.ManyToMany
			if (CanDeepLoad(entity, "List<Companies>|ChildIdCompaniesCollection_From_CompaniesRelationship", deepLoadType, innerList))
			{
				entity.ChildIdCompaniesCollection_From_CompaniesRelationship = DataRepository.CompaniesProvider.GetByParentIdFromCompaniesRelationship(transactionManager, entity.CompanyId);			 
		
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ChildIdCompaniesCollection_From_CompaniesRelationship' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ChildIdCompaniesCollection_From_CompaniesRelationship != null)
				{
					deepHandles.Add("ChildIdCompaniesCollection_From_CompaniesRelationship",
						new KeyValuePair<Delegate, object>((DeepLoadHandle< Companies >) DataRepository.CompaniesProvider.DeepLoad,
						new object[] { transactionManager, entity.ChildIdCompaniesCollection_From_CompaniesRelationship, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Companies object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Companies instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Companies Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Companies entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyStatusIdSource
			if (CanDeepSave(entity, "CompanyStatus|CompanyStatusIdSource", deepSaveType, innerList) 
				&& entity.CompanyStatusIdSource != null)
			{
				DataRepository.CompanyStatusProvider.Save(transactionManager, entity.CompanyStatusIdSource);
				entity.CompanyStatusId = entity.CompanyStatusIdSource.CompanyStatusId;
			}
			#endregion 
			
			#region CompanyStatus2IdSource
			if (CanDeepSave(entity, "CompanyStatus2|CompanyStatus2IdSource", deepSaveType, innerList) 
				&& entity.CompanyStatus2IdSource != null)
			{
				DataRepository.CompanyStatus2Provider.Save(transactionManager, entity.CompanyStatus2IdSource);
				entity.CompanyStatus2Id = entity.CompanyStatus2IdSource.CompanyStatusId;
			}
			#endregion 
			
			#region ModifiedbyUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedbyUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedbyUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedbyUserIdSource);
				entity.ModifiedbyUserId = entity.ModifiedbyUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region ParentIdCompaniesCollection_From_CompaniesRelationship>
			if (CanDeepSave(entity.ParentIdCompaniesCollection_From_CompaniesRelationship, "List<Companies>|ParentIdCompaniesCollection_From_CompaniesRelationship", deepSaveType, innerList))
			{
				if (entity.ParentIdCompaniesCollection_From_CompaniesRelationship.Count > 0 || entity.ParentIdCompaniesCollection_From_CompaniesRelationship.DeletedItems.Count > 0)
				{
					DataRepository.CompaniesProvider.Save(transactionManager, entity.ParentIdCompaniesCollection_From_CompaniesRelationship); 
					deepHandles.Add("ParentIdCompaniesCollection_From_CompaniesRelationship",
						new KeyValuePair<Delegate, object>((DeepSaveHandle<Companies>) DataRepository.CompaniesProvider.DeepSave,
						new object[] { transactionManager, entity.ParentIdCompaniesCollection_From_CompaniesRelationship, deepSaveType, childTypes, innerList }
					));
				}
			}
			#endregion 

			#region ChildIdCompaniesCollection_From_CompaniesRelationship>
			if (CanDeepSave(entity.ChildIdCompaniesCollection_From_CompaniesRelationship, "List<Companies>|ChildIdCompaniesCollection_From_CompaniesRelationship", deepSaveType, innerList))
			{
				if (entity.ChildIdCompaniesCollection_From_CompaniesRelationship.Count > 0 || entity.ChildIdCompaniesCollection_From_CompaniesRelationship.DeletedItems.Count > 0)
				{
					DataRepository.CompaniesProvider.Save(transactionManager, entity.ChildIdCompaniesCollection_From_CompaniesRelationship); 
					deepHandles.Add("ChildIdCompaniesCollection_From_CompaniesRelationship",
						new KeyValuePair<Delegate, object>((DeepSaveHandle<Companies>) DataRepository.CompaniesProvider.DeepSave,
						new object[] { transactionManager, entity.ChildIdCompaniesCollection_From_CompaniesRelationship, deepSaveType, childTypes, innerList }
					));
				}
			}
			#endregion 
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanyStatusChangeApproval>
				if (CanDeepSave(entity.CompanyStatusChangeApprovalCollection, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanyStatusChangeApproval child in entity.CompanyStatusChangeApprovalCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CompanyStatusChangeApprovalCollection.Count > 0 || entity.CompanyStatusChangeApprovalCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanyStatusChangeApprovalProvider.Save(transactionManager, entity.CompanyStatusChangeApprovalCollection);
						
						deepHandles.Add("CompanyStatusChangeApprovalCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanyStatusChangeApproval >) DataRepository.CompanyStatusChangeApprovalProvider.DeepSave,
							new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContactsContractors>
				if (CanDeepSave(entity.ContactsContractorsCollection, "List<ContactsContractors>|ContactsContractorsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContactsContractors child in entity.ContactsContractorsCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.ContactsContractorsCollection.Count > 0 || entity.ContactsContractorsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContactsContractorsProvider.Save(transactionManager, entity.ContactsContractorsCollection);
						
						deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContactsContractors >) DataRepository.ContactsContractorsProvider.DeepSave,
							new object[] { transactionManager, entity.ContactsContractorsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Kpi>
				if (CanDeepSave(entity.KpiCollection, "List<Kpi>|KpiCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Kpi child in entity.KpiCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.KpiCollection.Count > 0 || entity.KpiCollection.DeletedItems.Count > 0)
					{
						//DataRepository.KpiProvider.Save(transactionManager, entity.KpiCollection);
						
						deepHandles.Add("KpiCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Kpi >) DataRepository.KpiProvider.DeepSave,
							new object[] { transactionManager, entity.KpiCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryException>
				if (CanDeepSave(entity.CompanySiteCategoryExceptionCollection, "List<CompanySiteCategoryException>|CompanySiteCategoryExceptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryException child in entity.CompanySiteCategoryExceptionCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CompanySiteCategoryExceptionCollection.Count > 0 || entity.CompanySiteCategoryExceptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryExceptionProvider.Save(transactionManager, entity.CompanySiteCategoryExceptionCollection);
						
						deepHandles.Add("CompanySiteCategoryExceptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryException >) DataRepository.CompanySiteCategoryExceptionProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryExceptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileDb>
				if (CanDeepSave(entity.FileDbCollection, "List<FileDb>|FileDbCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileDb child in entity.FileDbCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.FileDbCollection.Count > 0 || entity.FileDbCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileDbProvider.Save(transactionManager, entity.FileDbCollection);
						
						deepHandles.Add("FileDbCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileDb >) DataRepository.FileDbProvider.DeepSave,
							new object[] { transactionManager, entity.FileDbCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompaniesRelationship>
				if (CanDeepSave(entity.CompaniesRelationshipCollectionGetByChildId, "List<CompaniesRelationship>|CompaniesRelationshipCollectionGetByChildId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesRelationship child in entity.CompaniesRelationshipCollectionGetByChildId)
					{
						if(child.ChildIdSource != null)
						{
								child.ChildId = child.ChildIdSource.CompanyId;
						}

						if(child.ParentIdSource != null)
						{
								child.ParentId = child.ParentIdSource.CompanyId;
						}

					}

					if (entity.CompaniesRelationshipCollectionGetByChildId.Count > 0 || entity.CompaniesRelationshipCollectionGetByChildId.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesRelationshipProvider.Save(transactionManager, entity.CompaniesRelationshipCollectionGetByChildId);
						
						deepHandles.Add("CompaniesRelationshipCollectionGetByChildId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesRelationship >) DataRepository.CompaniesRelationshipProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesRelationshipCollectionGetByChildId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompaniesEhsimsMap>
				if (CanDeepSave(entity.CompaniesEhsimsMapCollection, "List<CompaniesEhsimsMap>|CompaniesEhsimsMapCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesEhsimsMap child in entity.CompaniesEhsimsMapCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CompaniesEhsimsMapCollection.Count > 0 || entity.CompaniesEhsimsMapCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesEhsimsMapProvider.Save(transactionManager, entity.CompaniesEhsimsMapCollection);
						
						deepHandles.Add("CompaniesEhsimsMapCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesEhsimsMap >) DataRepository.CompaniesEhsimsMapProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesEhsimsMapCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryException2>
				if (CanDeepSave(entity.CompanySiteCategoryException2Collection, "List<CompanySiteCategoryException2>|CompanySiteCategoryException2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryException2 child in entity.CompanySiteCategoryException2Collection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CompanySiteCategoryException2Collection.Count > 0 || entity.CompanySiteCategoryException2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryException2Provider.Save(transactionManager, entity.CompanySiteCategoryException2Collection);
						
						deepHandles.Add("CompanySiteCategoryException2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryException2 >) DataRepository.CompanySiteCategoryException2Provider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryException2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Users>
				if (CanDeepSave(entity.UsersCollection, "List<Users>|UsersCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Users child in entity.UsersCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.UsersCollection.Count > 0 || entity.UsersCollection.DeletedItems.Count > 0)
					{
						//DataRepository.UsersProvider.Save(transactionManager, entity.UsersCollection);
						
						deepHandles.Add("UsersCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Users >) DataRepository.UsersProvider.DeepSave,
							new object[] { transactionManager, entity.UsersCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SafetyPlansSeResponses>
				if (CanDeepSave(entity.SafetyPlansSeResponsesCollection, "List<SafetyPlansSeResponses>|SafetyPlansSeResponsesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SafetyPlansSeResponses child in entity.SafetyPlansSeResponsesCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.SafetyPlansSeResponsesCollection.Count > 0 || entity.SafetyPlansSeResponsesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SafetyPlansSeResponsesProvider.Save(transactionManager, entity.SafetyPlansSeResponsesCollection);
						
						deepHandles.Add("SafetyPlansSeResponsesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SafetyPlansSeResponses >) DataRepository.SafetyPlansSeResponsesProvider.DeepSave,
							new object[] { transactionManager, entity.SafetyPlansSeResponsesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Csa>
				if (CanDeepSave(entity.CsaCollection, "List<Csa>|CsaCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Csa child in entity.CsaCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CsaCollection.Count > 0 || entity.CsaCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CsaProvider.Save(transactionManager, entity.CsaCollection);
						
						deepHandles.Add("CsaCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Csa >) DataRepository.CsaProvider.DeepSave,
							new object[] { transactionManager, entity.CsaCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireContractorRs>
				if (CanDeepSave(entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId, "List<QuestionnaireContractorRs>|QuestionnaireContractorRsCollectionGetByContractorCompanyId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireContractorRs child in entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId)
					{
						if(child.ContractorCompanyIdSource != null)
						{
							child.ContractorCompanyId = child.ContractorCompanyIdSource.CompanyId;
						}
						else
						{
							child.ContractorCompanyId = entity.CompanyId;
						}

					}

					if (entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId.Count > 0 || entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireContractorRsProvider.Save(transactionManager, entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId);
						
						deepHandles.Add("QuestionnaireContractorRsCollectionGetByContractorCompanyId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireContractorRs >) DataRepository.QuestionnaireContractorRsProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireContractorRsCollectionGetByContractorCompanyId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FileDbMedicalTraining>
				if (CanDeepSave(entity.FileDbMedicalTrainingCollection, "List<FileDbMedicalTraining>|FileDbMedicalTrainingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FileDbMedicalTraining child in entity.FileDbMedicalTrainingCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.FileDbMedicalTrainingCollection.Count > 0 || entity.FileDbMedicalTrainingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FileDbMedicalTrainingProvider.Save(transactionManager, entity.FileDbMedicalTrainingCollection);
						
						deepHandles.Add("FileDbMedicalTrainingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FileDbMedicalTraining >) DataRepository.FileDbMedicalTrainingProvider.DeepSave,
							new object[] { transactionManager, entity.FileDbMedicalTrainingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Residential>
				if (CanDeepSave(entity.ResidentialCollection, "List<Residential>|ResidentialCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Residential child in entity.ResidentialCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.ResidentialCollection.Count > 0 || entity.ResidentialCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ResidentialProvider.Save(transactionManager, entity.ResidentialCollection);
						
						deepHandles.Add("ResidentialCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Residential >) DataRepository.ResidentialProvider.DeepSave,
							new object[] { transactionManager, entity.ResidentialCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SqExemption>
				if (CanDeepSave(entity.SqExemptionCollectionGetByCompanyId, "List<SqExemption>|SqExemptionCollectionGetByCompanyId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SqExemption child in entity.SqExemptionCollectionGetByCompanyId)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.SqExemptionCollectionGetByCompanyId.Count > 0 || entity.SqExemptionCollectionGetByCompanyId.DeletedItems.Count > 0)
					{
						//DataRepository.SqExemptionProvider.Save(transactionManager, entity.SqExemptionCollectionGetByCompanyId);
						
						deepHandles.Add("SqExemptionCollectionGetByCompanyId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SqExemption >) DataRepository.SqExemptionProvider.DeepSave,
							new object[] { transactionManager, entity.SqExemptionCollectionGetByCompanyId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireContractorRs>
				if (CanDeepSave(entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId, "List<QuestionnaireContractorRs>|QuestionnaireContractorRsCollectionGetBySubContractorCompanyId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireContractorRs child in entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId)
					{
						if(child.SubContractorCompanyIdSource != null)
						{
							child.SubContractorCompanyId = child.SubContractorCompanyIdSource.CompanyId;
						}
						else
						{
							child.SubContractorCompanyId = entity.CompanyId;
						}

					}

					if (entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId.Count > 0 || entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireContractorRsProvider.Save(transactionManager, entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId);
						
						deepHandles.Add("QuestionnaireContractorRsCollectionGetBySubContractorCompanyId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireContractorRs >) DataRepository.QuestionnaireContractorRsProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireContractorRsCollectionGetBySubContractorCompanyId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SqExemption>
				if (CanDeepSave(entity.SqExemptionCollectionGetByRequestedByCompanyId, "List<SqExemption>|SqExemptionCollectionGetByRequestedByCompanyId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SqExemption child in entity.SqExemptionCollectionGetByRequestedByCompanyId)
					{
						if(child.RequestedByCompanyIdSource != null)
						{
							child.RequestedByCompanyId = child.RequestedByCompanyIdSource.CompanyId;
						}
						else
						{
							child.RequestedByCompanyId = entity.CompanyId;
						}

					}

					if (entity.SqExemptionCollectionGetByRequestedByCompanyId.Count > 0 || entity.SqExemptionCollectionGetByRequestedByCompanyId.DeletedItems.Count > 0)
					{
						//DataRepository.SqExemptionProvider.Save(transactionManager, entity.SqExemptionCollectionGetByRequestedByCompanyId);
						
						deepHandles.Add("SqExemptionCollectionGetByRequestedByCompanyId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SqExemption >) DataRepository.SqExemptionProvider.DeepSave,
							new object[] { transactionManager, entity.SqExemptionCollectionGetByRequestedByCompanyId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompaniesRelationship>
				if (CanDeepSave(entity.CompaniesRelationshipCollectionGetByParentId, "List<CompaniesRelationship>|CompaniesRelationshipCollectionGetByParentId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesRelationship child in entity.CompaniesRelationshipCollectionGetByParentId)
					{
						if(child.ChildIdSource != null)
						{
								child.ChildId = child.ChildIdSource.CompanyId;
						}

						if(child.ParentIdSource != null)
						{
								child.ParentId = child.ParentIdSource.CompanyId;
						}

					}

					if (entity.CompaniesRelationshipCollectionGetByParentId.Count > 0 || entity.CompaniesRelationshipCollectionGetByParentId.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesRelationshipProvider.Save(transactionManager, entity.CompaniesRelationshipCollectionGetByParentId);
						
						deepHandles.Add("CompaniesRelationshipCollectionGetByParentId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesRelationship >) DataRepository.CompaniesRelationshipProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesRelationshipCollectionGetByParentId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollection, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollection.Count > 0 || entity.CompanySiteCategoryStandardCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollection);
						
						deepHandles.Add("CompanySiteCategoryStandardCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdHocRadarItems2>
				if (CanDeepSave(entity.AdHocRadarItems2Collection, "List<AdHocRadarItems2>|AdHocRadarItems2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdHocRadarItems2 child in entity.AdHocRadarItems2Collection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.AdHocRadarItems2Collection.Count > 0 || entity.AdHocRadarItems2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.AdHocRadarItems2Provider.Save(transactionManager, entity.AdHocRadarItems2Collection);
						
						deepHandles.Add("AdHocRadarItems2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdHocRadarItems2 >) DataRepository.AdHocRadarItems2Provider.DeepSave,
							new object[] { transactionManager, entity.AdHocRadarItems2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompaniesChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Companies</c>
	///</summary>
	public enum CompaniesChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CompanyStatus</c> at CompanyStatusIdSource
		///</summary>
		[ChildEntityType(typeof(CompanyStatus))]
		CompanyStatus,
			
		///<summary>
		/// Composite Property for <c>CompanyStatus2</c> at CompanyStatus2IdSource
		///</summary>
		[ChildEntityType(typeof(CompanyStatus2))]
		CompanyStatus2,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedbyUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompanyStatusChangeApprovalCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanyStatusChangeApproval>))]
		CompanyStatusChangeApprovalCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for ContactsContractorsCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContactsContractors>))]
		ContactsContractorsCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for KpiCollection
		///</summary>
		[ChildEntityType(typeof(TList<Kpi>))]
		KpiCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompanySiteCategoryExceptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryException>))]
		CompanySiteCategoryExceptionCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for FileDbCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileDb>))]
		FileDbCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompaniesRelationshipCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesRelationship>))]
		CompaniesRelationshipCollectionGetByChildId,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompaniesEhsimsMapCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesEhsimsMap>))]
		CompaniesEhsimsMapCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompanySiteCategoryException2Collection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryException2>))]
		CompanySiteCategoryException2Collection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for UsersCollection
		///</summary>
		[ChildEntityType(typeof(TList<Users>))]
		UsersCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for SafetyPlansSeResponsesCollection
		///</summary>
		[ChildEntityType(typeof(TList<SafetyPlansSeResponses>))]
		SafetyPlansSeResponsesCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CsaCollection
		///</summary>
		[ChildEntityType(typeof(TList<Csa>))]
		CsaCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for QuestionnaireContractorRsCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireContractorRs>))]
		QuestionnaireContractorRsCollectionGetByContractorCompanyId,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for FileDbMedicalTrainingCollection
		///</summary>
		[ChildEntityType(typeof(TList<FileDbMedicalTraining>))]
		FileDbMedicalTrainingCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for ResidentialCollection
		///</summary>
		[ChildEntityType(typeof(TList<Residential>))]
		ResidentialCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for SqExemptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<SqExemption>))]
		SqExemptionCollectionGetByCompanyId,

		///<summary>
		/// Collection of <c>Companies</c> as ManyToMany for CompaniesCollection_From_CompaniesRelationship
		///</summary>
		[ChildEntityType(typeof(TList<Companies>))]
		ParentIdCompaniesCollection_From_CompaniesRelationship,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for QuestionnaireContractorRsCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireContractorRs>))]
		QuestionnaireContractorRsCollectionGetBySubContractorCompanyId,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for SqExemptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<SqExemption>))]
		SqExemptionCollectionGetByRequestedByCompanyId,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompaniesRelationshipCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesRelationship>))]
		CompaniesRelationshipCollectionGetByParentId,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollection,

		///<summary>
		/// Collection of <c>Companies</c> as OneToMany for AdHocRadarItems2Collection
		///</summary>
		[ChildEntityType(typeof(TList<AdHocRadarItems2>))]
		AdHocRadarItems2Collection,

		///<summary>
		/// Collection of <c>Companies</c> as ManyToMany for CompaniesCollection_From_CompaniesRelationship
		///</summary>
		[ChildEntityType(typeof(TList<Companies>))]
		ChildIdCompaniesCollection_From_CompaniesRelationship,
	}
	
	#endregion CompaniesChildEntityTypes
	
	#region CompaniesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompaniesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesFilterBuilder : SqlFilterBuilder<CompaniesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesFilterBuilder class.
		/// </summary>
		public CompaniesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesFilterBuilder
	
	#region CompaniesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompaniesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesParameterBuilder class.
		/// </summary>
		public CompaniesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesParameterBuilder
	
	#region CompaniesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompaniesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Companies"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesSortBuilder : SqlSortBuilder<CompaniesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesSqlSortBuilder class.
		/// </summary>
		public CompaniesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesSortBuilder
	
} // end namespace
