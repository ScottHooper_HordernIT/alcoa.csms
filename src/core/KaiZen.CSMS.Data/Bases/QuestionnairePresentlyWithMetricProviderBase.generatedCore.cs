﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnairePresentlyWithMetricProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnairePresentlyWithMetricProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetricKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetricKey key)
		{
			return Delete(transactionManager, key.QuestionnairePresentlyWithMetricId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnairePresentlyWithMetricId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnairePresentlyWithMetricId)
		{
			return Delete(null, _questionnairePresentlyWithMetricId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithMetricId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithMetricId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_Sites key.
		///		FK_QuestionnairePresentlyWithMetric_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetBySiteId(System.Int32? _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_Sites key.
		///		FK_QuestionnairePresentlyWithMetric_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnairePresentlyWithMetric> GetBySiteId(TransactionManager transactionManager, System.Int32? _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_Sites key.
		///		FK_QuestionnairePresentlyWithMetric_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetBySiteId(TransactionManager transactionManager, System.Int32? _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_Sites key.
		///		fkQuestionnairePresentlyWithMetricSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetBySiteId(System.Int32? _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_Sites key.
		///		fkQuestionnairePresentlyWithMetricSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetBySiteId(System.Int32? _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_Sites key.
		///		FK_QuestionnairePresentlyWithMetric_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public abstract TList<QuestionnairePresentlyWithMetric> GetBySiteId(TransactionManager transactionManager, System.Int32? _siteId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction Description: 
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithActionId(System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(_questionnairePresentlyWithActionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(transactionManager, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(transactionManager, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction key.
		///		fkQuestionnairePresentlyWithMetricQuestionnairePresentlyWithAction Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithActionId(System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnairePresentlyWithActionId(null, _questionnairePresentlyWithActionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction key.
		///		fkQuestionnairePresentlyWithMetricQuestionnairePresentlyWithAction Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithActionId(System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength,out int count)
		{
			return GetByQuestionnairePresentlyWithActionId(null, _questionnairePresentlyWithActionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithAction Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public abstract TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers Description: 
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(_questionnairePresentlyWithUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(transactionManager, _questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(transactionManager, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers key.
		///		fkQuestionnairePresentlyWithMetricQuestionnairePresentlyWithUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnairePresentlyWithUserId(null, _questionnairePresentlyWithUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers key.
		///		fkQuestionnairePresentlyWithMetricQuestionnairePresentlyWithUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength,out int count)
		{
			return GetByQuestionnairePresentlyWithUserId(null, _questionnairePresentlyWithUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers key.
		///		FK_QuestionnairePresentlyWithMetric_QuestionnairePresentlyWithUsers Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric objects.</returns>
		public abstract TList<QuestionnairePresentlyWithMetric> GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetricKey key, int start, int pageLength)
		{
			return GetByQuestionnairePresentlyWithMetricId(transactionManager, key.QuestionnairePresentlyWithMetricId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithMetricId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByQuestionnairePresentlyWithMetricId(System.Int32 _questionnairePresentlyWithMetricId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithMetricId(null,_questionnairePresentlyWithMetricId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByQuestionnairePresentlyWithMetricId(System.Int32 _questionnairePresentlyWithMetricId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithMetricId(null, _questionnairePresentlyWithMetricId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithMetricId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByQuestionnairePresentlyWithMetricId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithMetricId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithMetricId(transactionManager, _questionnairePresentlyWithMetricId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByQuestionnairePresentlyWithMetricId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithMetricId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithMetricId(transactionManager, _questionnairePresentlyWithMetricId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByQuestionnairePresentlyWithMetricId(System.Int32 _questionnairePresentlyWithMetricId, int start, int pageLength, out int count)
		{
			return GetByQuestionnairePresentlyWithMetricId(null, _questionnairePresentlyWithMetricId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithMetricId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByQuestionnairePresentlyWithMetricId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithMetricId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(null,_year, _weekNo, _siteId, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(null, _year, _weekNo, _siteId, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(transactionManager, _year, _weekNo, _siteId, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(transactionManager, _year, _weekNo, _siteId, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count)
		{
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(null, _year, _weekNo, _siteId, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric GetByYearWeekNoSiteIdQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithMetric_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(null,_year, _weekNo, _siteId, _questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(null, _year, _weekNo, _siteId, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(transactionManager, _year, _weekNo, _siteId, _questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(transactionManager, _year, _weekNo, _siteId, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_1 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count)
		{
			return GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(null, _year, _weekNo, _siteId, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public abstract TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteIdQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithMetric_2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId)
		{
			int count = -1;
			return GetByYearWeekNoSiteId(null,_year, _weekNo, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoSiteId(null, _year, _weekNo, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId)
		{
			int count = -1;
			return GetByYearWeekNoSiteId(transactionManager, _year, _weekNo, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoSiteId(transactionManager, _year, _weekNo, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_2 index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteId(System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, int start, int pageLength, out int count)
		{
			return GetByYearWeekNoSiteId(null, _year, _weekNo, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric_2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public abstract TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoSiteId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32? _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoQuestionnairePresentlyWithActionId(System.Int32 _year, System.Int32 _weekNo, System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByYearWeekNoQuestionnairePresentlyWithActionId(null,_year, _weekNo, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoQuestionnairePresentlyWithActionId(System.Int32 _year, System.Int32 _weekNo, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoQuestionnairePresentlyWithActionId(null, _year, _weekNo, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32 _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByYearWeekNoQuestionnairePresentlyWithActionId(transactionManager, _year, _weekNo, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByYearWeekNoQuestionnairePresentlyWithActionId(transactionManager, _year, _weekNo, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoQuestionnairePresentlyWithActionId(System.Int32 _year, System.Int32 _weekNo, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count)
		{
			return GetByYearWeekNoQuestionnairePresentlyWithActionId(null, _year, _weekNo, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithMetric index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_year"></param>
		/// <param name="_weekNo"></param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/> class.</returns>
		public abstract TList<QuestionnairePresentlyWithMetric> GetByYearWeekNoQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32 _year, System.Int32 _weekNo, System.Int32 _questionnairePresentlyWithActionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _QuestionnairePresentlyWithMetric_GetYear 
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetYear(System.Int32? year)
		{
			return GetYear(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetYear(int start, int pageLength, System.Int32? year)
		{
			return GetYear(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetYear(TransactionManager transactionManager, System.Int32? year)
		{
			return GetYear(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetYear(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#region _QuestionnairePresentlyWithMetric_GetYear2 
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetYear2(System.Int32? year)
		{
			return GetYear2(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetYear2(int start, int pageLength, System.Int32? year)
		{
			return GetYear2(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetYear2(TransactionManager transactionManager, System.Int32? year)
		{
			return GetYear2(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithMetric_GetYear2' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetYear2(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnairePresentlyWithMetric&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnairePresentlyWithMetric&gt;"/></returns>
		public static TList<QuestionnairePresentlyWithMetric> Fill(IDataReader reader, TList<QuestionnairePresentlyWithMetric> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnairePresentlyWithMetric")
					.Append("|").Append((System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithMetricId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnairePresentlyWithMetric>(
					key.ToString(), // EntityTrackingKey
					"QuestionnairePresentlyWithMetric",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnairePresentlyWithMetricId = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithMetricId - 1)];
					c.SiteId = (reader.IsDBNull(((int)QuestionnairePresentlyWithMetricColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)QuestionnairePresentlyWithMetricColumn.SiteId - 1)];
					c.QuestionnairePresentlyWithActionId = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithActionId - 1)];
					c.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithUserId - 1)];
					c.Date = (System.DateTime)reader[((int)QuestionnairePresentlyWithMetricColumn.Date - 1)];
					c.Year = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.Year - 1)];
					c.WeekNo = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.WeekNo - 1)];
					c.Metric = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.Metric - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnairePresentlyWithMetricId = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithMetricId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)QuestionnairePresentlyWithMetricColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)QuestionnairePresentlyWithMetricColumn.SiteId - 1)];
			entity.QuestionnairePresentlyWithActionId = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithActionId - 1)];
			entity.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.QuestionnairePresentlyWithUserId - 1)];
			entity.Date = (System.DateTime)reader[((int)QuestionnairePresentlyWithMetricColumn.Date - 1)];
			entity.Year = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.Year - 1)];
			entity.WeekNo = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.WeekNo - 1)];
			entity.Metric = (System.Int32)reader[((int)QuestionnairePresentlyWithMetricColumn.Metric - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnairePresentlyWithMetricId = (System.Int32)dataRow["QuestionnairePresentlyWithMetricId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.QuestionnairePresentlyWithActionId = (System.Int32)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (System.Int32)dataRow["QuestionnairePresentlyWithUserId"];
			entity.Date = (System.DateTime)dataRow["Date"];
			entity.Year = (System.Int32)dataRow["Year"];
			entity.WeekNo = (System.Int32)dataRow["WeekNo"];
			entity.Metric = (System.Int32)dataRow["Metric"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SiteId ?? (int)0);
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, (entity.SiteId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource

			#region QuestionnairePresentlyWithActionIdSource	
			if (CanDeepLoad(entity, "QuestionnairePresentlyWithAction|QuestionnairePresentlyWithActionIdSource", deepLoadType, innerList) 
				&& entity.QuestionnairePresentlyWithActionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnairePresentlyWithActionId;
				QuestionnairePresentlyWithAction tmpEntity = EntityManager.LocateEntity<QuestionnairePresentlyWithAction>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnairePresentlyWithAction), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnairePresentlyWithActionIdSource = tmpEntity;
				else
					entity.QuestionnairePresentlyWithActionIdSource = DataRepository.QuestionnairePresentlyWithActionProvider.GetByQuestionnairePresentlyWithActionId(transactionManager, entity.QuestionnairePresentlyWithActionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnairePresentlyWithActionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnairePresentlyWithActionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnairePresentlyWithActionProvider.DeepLoad(transactionManager, entity.QuestionnairePresentlyWithActionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnairePresentlyWithActionIdSource

			#region QuestionnairePresentlyWithUserIdSource	
			if (CanDeepLoad(entity, "QuestionnairePresentlyWithUsers|QuestionnairePresentlyWithUserIdSource", deepLoadType, innerList) 
				&& entity.QuestionnairePresentlyWithUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnairePresentlyWithUserId;
				QuestionnairePresentlyWithUsers tmpEntity = EntityManager.LocateEntity<QuestionnairePresentlyWithUsers>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnairePresentlyWithUsers), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnairePresentlyWithUserIdSource = tmpEntity;
				else
					entity.QuestionnairePresentlyWithUserIdSource = DataRepository.QuestionnairePresentlyWithUsersProvider.GetByQuestionnairePresentlyWithUserId(transactionManager, entity.QuestionnairePresentlyWithUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnairePresentlyWithUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnairePresentlyWithUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnairePresentlyWithUsersProvider.DeepLoad(transactionManager, entity.QuestionnairePresentlyWithUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnairePresentlyWithUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			
			#region QuestionnairePresentlyWithActionIdSource
			if (CanDeepSave(entity, "QuestionnairePresentlyWithAction|QuestionnairePresentlyWithActionIdSource", deepSaveType, innerList) 
				&& entity.QuestionnairePresentlyWithActionIdSource != null)
			{
				DataRepository.QuestionnairePresentlyWithActionProvider.Save(transactionManager, entity.QuestionnairePresentlyWithActionIdSource);
				entity.QuestionnairePresentlyWithActionId = entity.QuestionnairePresentlyWithActionIdSource.QuestionnairePresentlyWithActionId;
			}
			#endregion 
			
			#region QuestionnairePresentlyWithUserIdSource
			if (CanDeepSave(entity, "QuestionnairePresentlyWithUsers|QuestionnairePresentlyWithUserIdSource", deepSaveType, innerList) 
				&& entity.QuestionnairePresentlyWithUserIdSource != null)
			{
				DataRepository.QuestionnairePresentlyWithUsersProvider.Save(transactionManager, entity.QuestionnairePresentlyWithUserIdSource);
				entity.QuestionnairePresentlyWithUserId = entity.QuestionnairePresentlyWithUserIdSource.QuestionnairePresentlyWithUserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnairePresentlyWithMetricChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnairePresentlyWithMetric</c>
	///</summary>
	public enum QuestionnairePresentlyWithMetricChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
			
		///<summary>
		/// Composite Property for <c>QuestionnairePresentlyWithAction</c> at QuestionnairePresentlyWithActionIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnairePresentlyWithAction))]
		QuestionnairePresentlyWithAction,
			
		///<summary>
		/// Composite Property for <c>QuestionnairePresentlyWithUsers</c> at QuestionnairePresentlyWithUserIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnairePresentlyWithUsers))]
		QuestionnairePresentlyWithUsers,
		}
	
	#endregion QuestionnairePresentlyWithMetricChildEntityTypes
	
	#region QuestionnairePresentlyWithMetricFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnairePresentlyWithMetricColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricFilterBuilder : SqlFilterBuilder<QuestionnairePresentlyWithMetricColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricFilterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithMetricFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithMetricFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithMetricFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithMetricFilterBuilder
	
	#region QuestionnairePresentlyWithMetricParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnairePresentlyWithMetricColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithMetricParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnairePresentlyWithMetricColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricParameterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithMetricParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithMetricParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithMetricParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithMetricParameterBuilder
	
	#region QuestionnairePresentlyWithMetricSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnairePresentlyWithMetricColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithMetric"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnairePresentlyWithMetricSortBuilder : SqlSortBuilder<QuestionnairePresentlyWithMetricColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithMetricSqlSortBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithMetricSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnairePresentlyWithMetricSortBuilder
	
} // end namespace
