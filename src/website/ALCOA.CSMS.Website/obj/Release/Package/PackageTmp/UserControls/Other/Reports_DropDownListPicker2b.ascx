﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Tiny_Reports_DropDownListPicker2b" Codebehind="Reports_DropDownListPicker2b.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td class="pageName" colspan="7" style="width: 900px; height: 24px; text-align: left">
            <div align="center">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                    <tr>
                        <td style="width: 34px; height: 24px; text-align: right">
                            <strong>Site:</strong>
                        </td>
                        <td style="width: 250px; height: 24px">
                            <dxe:ASPxComboBox ID="ddlSites" runat="server" ClientInstanceName="cmbSites" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" EnableSynchronization="False"
                                ValueType="System.Int32" Width="250px" DropDownHeight="300px"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                    ValidationGroup="site">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                        <td style="width: 174px; height: 24px; text-align: right">
                            <strong>Residential Category:</strong>
                        </td>
                        <td style="width: 130px; height: 24px">
                            <dxe:ASPxComboBox ID="ddlCategory" runat="server" ClientInstanceName="cmbCategory"
                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                IncrementalFilteringMode="StartsWith" EnableSynchronization="False" ValueType="System.Int32"
                                Width="130px" DropDownHeight="300px"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                    ValidationGroup="site">
                                    <RequiredField IsRequired="True" />
                                </ValidationSettings>
                            </dxe:ASPxComboBox>
                        </td>
                        <td style="width: 44px; height: 24px; text-align: right">
                            <strong>Year:</strong>
                        </td>
                        <td style="width: 65px; height: 24px">
                            <dxe:ASPxComboBox ID="ddlYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"
                                ValueType="System.Int32" Width="60px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                            </dxe:ASPxComboBox>
                        </td>
                        <td style="width: 360px; height: 24px; text-align: left; padding-left:5px;">
                                <dxe:ASPxButton ID="btnSearchGo" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" OnClick="btnSearchGo_Click" Text="Go / Refresh" ValidationGroup="site"
                                    Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                </dxe:ASPxButton>
                            </td>
                    </tr>
                </table>

            </div>
            <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>
            <data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
                Sort="Ordinal ASC">
            </data:CompanySiteCategoryDataSource>
            <data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True"
                Sort="SiteName ASC">
            </data:SitesDataSource>
            <asp:SqlDataSource ID="sqldsKpi_GetAllYearsSubmitted" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                SelectCommand="_Kpi_GetAllYearsSubmitted" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>
        </td>
    </tr>
</table>
