using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

public partial class UserControls_Admin_UsersBulkChange : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadCbs();
        }
    }
    protected void LoadCbs()
    {
        cba1From.Text = "";
        cba2From.Text = "";
        cba3From.Text = "";
        cba4From.Text = "";
        cba1To.Text = "";
        cba2To.Text = "";
        cba3To.Text = "";
        cba4To.Text = "";

        EhsConsultantService eService = new EhsConsultantService();
        DataSet ds1From = eService.ListAssignedToCompanies();
        cba1From.DataSource = ds1From.Tables[0];
        cba1From.ValueField = "EhsConsultantId";
        cba1From.TextField = "UserFullName";
        cba1From.DataBind();
        UsersEhsConsultantsService ueService = new UsersEhsConsultantsService();
        DataSet ds1To = ueService.ListOrdered();
        cba1To.DataSource = ds1To.Tables[0];
        cba1To.ValueField = "EhsConsultantId";
        cba1To.TextField = "UserFullName";
        cba1To.DataBind();

        DataSet ds2From = eService.ListAssignedToSmps();
        cba2From.DataSource = ds2From.Tables[0];
        cba2From.ValueField = "EhsConsultantId";
        cba2From.TextField = "UserFullName";
        cba2From.DataBind();
        cba2To.DataSource = ds1To.Tables[0];
        cba2To.ValueField = "EhsConsultantId";
        cba2To.TextField = "UserFullName";
        cba2To.DataBind();

        QuestionnaireService qService = new QuestionnaireService();
        DataSet ds3From = qService.ListAssignedProcurementContacts();
        cba3From.DataSource = ds3From.Tables[0];
        cba3From.ValueField = "UserId";
        cba3From.TextField = "UserFullName";
        cba3From.DataBind();
        UsersProcurementListService uplService = new UsersProcurementListService();
        DataSet ds3To = uplService.ListOrdered();
        cba3To.DataSource = ds3To.Tables[0];
        cba3To.ValueField = "UserId";
        cba3To.TextField = "UserFullName";
        cba3To.DataBind();

        DataSet ds4From = qService.ListAssignedProcurementFunctionalManagers();
        cba4From.DataSource = ds4From.Tables[0];
        cba4From.ValueField = "UserId";
        cba4From.TextField = "UserFullName";
        cba4From.DataBind();
        cba4To.DataSource = ds3To.Tables[0];
        cba4To.ValueField = "UserId";
        cba4To.TextField = "UserFullName";
        cba4To.DataBind();

    }
    protected void btna1_Click(object sender, EventArgs e)
    {
        //Change Companies EhsConsultantId
        aResults.Text = "Please Select an Option.";
        CompaniesService cService = new CompaniesService();
        TList<Companies> cList = cService.GetByEhsConsultantId(Convert.ToInt32(cba1From.SelectedItem.Value));
        string failure = "";
        int fail = 0;
        int skipped = 0;
        if (cList.Count > 0)
        {
            foreach (Companies c in cList)
            {
                bool cont = true;
                if (cbSafetyAssessorChangeInProgress.Checked)
                {
                    cont = false;
                    if (c.CompanyStatusId == (int)CompanyStatusList.Being_Assessed || c.CompanyStatusId == (int)CompanyStatusList.Requal_Incomplete)
                    {
                        cont = true;
                    }
                    else
                    {
                        skipped++;
                    }
                }

                if (cont)
                {
                    try
                    {
                        c.EhsConsultantId = (int)cba1To.SelectedItem.Value;
                        cService.Save(c);
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                        fail++;
                        failure += c.CompanyName + ", ";
                    }
                }
            }


            if (String.IsNullOrEmpty(failure))
            {
                if (cbSafetyAssessorChangeInProgress.Checked)
                {
                    int changed = cList.Count - skipped;
                    aResults.Text = changed.ToString() + " (" + skipped.ToString() + " skipped) Companies of EHSConsultant '" + cba1From.SelectedItem.Text + "' bulk changed to '" + cba1To.SelectedItem.Text + "'.";
                }
                else
                {
                    aResults.Text = cList.Count.ToString() + " Companies of EHSConsultant '" + cba1From.SelectedItem.Text + "' bulk changed to '" + cba1To.SelectedItem.Text + "'.";
                }
            }
            else
            {
                int success = cList.Count - fail;
                if (cbSafetyAssessorChangeInProgress.Checked)
                {
                    success = success - skipped;
                    aResults.Text = success.ToString() + " (" + skipped.ToString() + " skipped) Companies of EHSConsultant '" + cba1From.SelectedItem.Text + "' bulk changed to '" + cba1To.SelectedItem.Text + "' except for " + fail.ToString() + ": " + failure + ". Contact your System Admin to manually perform this change.";
                }
                else
                {
                    aResults.Text = success.ToString() + " Companies of EHSConsultant '" + cba1From.SelectedItem.Text + "' bulk changed to '" + cba1To.SelectedItem.Text + "' except for " + fail.ToString() + ": " + failure + ". Contact your System Admin to manually perform this change.";
                }
            }
            LoadCbs();
        }
        else
        {
            aResults.Text = "No Companies have an EHSConsultant of '" + cba1From.SelectedItem.Text + "' to bulk change.";
        }
    }
    protected void btna2_Click(object sender, EventArgs e)
    {
        bool failure = false;
        //Change Safety Management Plan - EHSConsultantId
        aResults.Text = "Please Select an Option.";
        //Custom stored procedure to do this? Loading time probably slow doing it this way???
        int? to = null;
        int? from = null;
        try
        {
            if(!String.IsNullOrEmpty(cba2To.SelectedItem.Value.ToString())) to = Convert.ToInt32(cba2To.SelectedItem.Value.ToString());
            if(!String.IsNullOrEmpty(cba2From.SelectedItem.Value.ToString())) from = Convert.ToInt32(cba2From.SelectedItem.Value.ToString());
            FileDbService fdbService = new FileDbService();
            fdbService.BulkUpdateEhsConsultantId(from, to);
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            failure = true;
        }

        if (!failure)
        {
            aResults.Text = "All Safety Management Plans with a H&S Contact of '" + cba2From.SelectedItem.Text + "' bulk changed to '" + cba2To.SelectedItem.Text + "'.";
        }
        else
        {
            aResults.Text = "Errors Occured. Contact your System Admin to manually perform this change.";
        }
        LoadCbs();
    }
    protected void btna3_Click(object sender, EventArgs e)
    {
        changeQuestionnaireInitialResponse("2", "Procurement Contact");
    }
    private void changeQuestionnaireInitialResponse(string QuestionId, string Type)
    {
        bool failure = false;
        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        TList<QuestionnaireInitialResponse> qirList = qirService.GetByQuestionId(QuestionId);
        int count = 0;
        try
        {
            foreach (QuestionnaireInitialResponse qir in qirList)
            {
                if (QuestionId == "2")
                {
                    if (qir.AnswerText == cba3From.SelectedItem.Value.ToString())
                    {
                        qir.AnswerText = cba3To.SelectedItem.Value.ToString();
                        qirService.Save(qir);
                        count++;
                    }
                }
                if (QuestionId == "2_1")
                {
                    if (qir.AnswerText == cba4From.SelectedItem.Value.ToString())
                    {
                        qir.AnswerText = cba4To.SelectedItem.Value.ToString();
                        qirService.Save(qir);
                        count++;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            failure = true;
        }

        if (!failure)
        {
            if (QuestionId == "2")
            {
                aResults.Text = count.ToString() + " Company Questionnaires with a " + Type + " of '" + cba3From.SelectedItem.Text + "' bulk changed to '" + cba3To.SelectedItem.Text + "'.";
            }
            if (QuestionId == "2_1")
            {
                aResults.Text = count.ToString() + " Company Questionnaires with a " + Type + " of '" + cba4From.SelectedItem.Text + "' bulk changed to '" + cba4To.SelectedItem.Text + "'.";
            }
        }
        else
        {
            aResults.Text = "Errors Occured. Contact your System Admin to manually perform this change.";
        }
        LoadCbs();
    }

    protected void btna4_Click(object sender, EventArgs e)
    {
        changeQuestionnaireInitialResponse("2_1", "Procurement Functional Supervisor");
    }
}
