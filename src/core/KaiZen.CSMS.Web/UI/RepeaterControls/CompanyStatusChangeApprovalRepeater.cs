﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>CompanyStatusChangeApprovalRepeater</c>
    /// </summary>
	public class CompanyStatusChangeApprovalRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanyStatusChangeApprovalRepeaterDesigner"/> class.
        /// </summary>
		public CompanyStatusChangeApprovalRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is CompanyStatusChangeApprovalRepeater))
			{ 
				throw new ArgumentException("Component is not a CompanyStatusChangeApprovalRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			CompanyStatusChangeApprovalRepeater z = (CompanyStatusChangeApprovalRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="CompanyStatusChangeApprovalRepeater"/> Type.
    /// </summary>
	[Designer(typeof(CompanyStatusChangeApprovalRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:CompanyStatusChangeApprovalRepeater runat=\"server\"></{0}:CompanyStatusChangeApprovalRepeater>")]
	public class CompanyStatusChangeApprovalRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanyStatusChangeApprovalRepeater"/> class.
        /// </summary>
		public CompanyStatusChangeApprovalRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanyStatusChangeApprovalItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanyStatusChangeApprovalItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(CompanyStatusChangeApprovalItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanyStatusChangeApprovalItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CompanyStatusChangeApprovalItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.CompanyStatusChangeApproval entity = o as KaiZen.CSMS.Entities.CompanyStatusChangeApproval;
						CompanyStatusChangeApprovalItem container = new CompanyStatusChangeApprovalItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class CompanyStatusChangeApprovalItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.CompanyStatusChangeApproval _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanyStatusChangeApprovalItem"/> class.
        /// </summary>
		public CompanyStatusChangeApprovalItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CompanyStatusChangeApprovalItem"/> class.
        /// </summary>
		public CompanyStatusChangeApprovalItem(KaiZen.CSMS.Entities.CompanyStatusChangeApproval entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyStatusChangeApprovalId
        /// </summary>
        /// <value>The CompanyStatusChangeApprovalId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyStatusChangeApprovalId
		{
			get { return _entity.CompanyStatusChangeApprovalId; }
		}
        /// <summary>
        /// Gets the RequestedDate
        /// </summary>
        /// <value>The RequestedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime RequestedDate
		{
			get { return _entity.RequestedDate; }
		}
        /// <summary>
        /// Gets the RequestedByUserId
        /// </summary>
        /// <value>The RequestedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 RequestedByUserId
		{
			get { return _entity.RequestedByUserId; }
		}
        /// <summary>
        /// Gets the Comments
        /// </summary>
        /// <value>The Comments.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Comments
		{
			get { return _entity.Comments; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the CurrentCompanyStatusId
        /// </summary>
        /// <value>The CurrentCompanyStatusId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CurrentCompanyStatusId
		{
			get { return _entity.CurrentCompanyStatusId; }
		}
        /// <summary>
        /// Gets the RequestedCompanyStatusId
        /// </summary>
        /// <value>The RequestedCompanyStatusId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 RequestedCompanyStatusId
		{
			get { return _entity.RequestedCompanyStatusId; }
		}
        /// <summary>
        /// Gets the AssessedByUserId
        /// </summary>
        /// <value>The AssessedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AssessedByUserId
		{
			get { return _entity.AssessedByUserId; }
		}
        /// <summary>
        /// Gets the AssessedDate
        /// </summary>
        /// <value>The AssessedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? AssessedDate
		{
			get { return _entity.AssessedDate; }
		}
        /// <summary>
        /// Gets the AssessorComments
        /// </summary>
        /// <value>The AssessorComments.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AssessorComments
		{
			get { return _entity.AssessorComments; }
		}
        /// <summary>
        /// Gets the Approved
        /// </summary>
        /// <value>The Approved.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Approved
		{
			get { return _entity.Approved; }
		}
        /// <summary>
        /// Gets the QuestionnaireId
        /// </summary>
        /// <value>The QuestionnaireId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QuestionnaireId
		{
			get { return _entity.QuestionnaireId; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.CompanyStatusChangeApproval"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.CompanyStatusChangeApproval Entity
        {
            get { return _entity; }
        }
	}
}
