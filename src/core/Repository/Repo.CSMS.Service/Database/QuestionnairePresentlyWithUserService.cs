﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnairePresentlyWithUserService
    {
        QuestionnairePresentlyWithUser Get(Expression<Func<QuestionnairePresentlyWithUser, bool>> where, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes);
        List<QuestionnairePresentlyWithUser> GetMany(int? top, Expression<Func<QuestionnairePresentlyWithUser, bool>> where, Func<IQueryable<QuestionnairePresentlyWithUser>, IOrderedQueryable<QuestionnairePresentlyWithUser>> orderBy, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes);
        List<QuestionnairePresentlyWithUser> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnairePresentlyWithUser, bool>> where, Func<IQueryable<QuestionnairePresentlyWithUser>, IOrderedQueryable<QuestionnairePresentlyWithUser>> orderBy, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnairePresentlyWithUser, bool>> where, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes);
        void Insert(QuestionnairePresentlyWithUser item, bool saveChanges = true);
        void Update(QuestionnairePresentlyWithUser item, bool saveChanges = true);
        void Delete(QuestionnairePresentlyWithUser item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnairePresentlyWithUserService : IQuestionnairePresentlyWithUserService
    {
        private readonly IRepository<QuestionnairePresentlyWithUser> _repository;

        public QuestionnairePresentlyWithUserService(IRepository<QuestionnairePresentlyWithUser> repository)
        {
            this._repository = repository;
        }

        public QuestionnairePresentlyWithUser Get(Expression<Func<QuestionnairePresentlyWithUser, bool>> where, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnairePresentlyWithUser> GetMany(int? top, Expression<Func<QuestionnairePresentlyWithUser, bool>> where, Func<IQueryable<QuestionnairePresentlyWithUser>, IOrderedQueryable<QuestionnairePresentlyWithUser>> orderBy, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnairePresentlyWithUser> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnairePresentlyWithUser, bool>> where, Func<IQueryable<QuestionnairePresentlyWithUser>, IOrderedQueryable<QuestionnairePresentlyWithUser>> orderBy, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnairePresentlyWithUser, bool>> where, List<Expression<Func<QuestionnairePresentlyWithUser, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnairePresentlyWithUser item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithUser");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnairePresentlyWithUser item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithUser");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnairePresentlyWithUser item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithUser");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
