﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EnumApprovalProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EnumApprovalProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EnumApproval, KaiZen.CSMS.Entities.EnumApprovalKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumApprovalKey key)
		{
			return Delete(transactionManager, key.EnumApprovalId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_enumApprovalId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _enumApprovalId)
		{
			return Delete(null, _enumApprovalId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumApprovalId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _enumApprovalId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EnumApproval Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumApprovalKey key, int start, int pageLength)
		{
			return GetByEnumApprovalId(transactionManager, key.EnumApprovalId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanySiteCategoryStandardApproval index.
		/// </summary>
		/// <param name="_enumApprovalId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByEnumApprovalId(System.Int32 _enumApprovalId)
		{
			int count = -1;
			return GetByEnumApprovalId(null,_enumApprovalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardApproval index.
		/// </summary>
		/// <param name="_enumApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByEnumApprovalId(System.Int32 _enumApprovalId, int start, int pageLength)
		{
			int count = -1;
			return GetByEnumApprovalId(null, _enumApprovalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardApproval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumApprovalId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByEnumApprovalId(TransactionManager transactionManager, System.Int32 _enumApprovalId)
		{
			int count = -1;
			return GetByEnumApprovalId(transactionManager, _enumApprovalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardApproval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByEnumApprovalId(TransactionManager transactionManager, System.Int32 _enumApprovalId, int start, int pageLength)
		{
			int count = -1;
			return GetByEnumApprovalId(transactionManager, _enumApprovalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardApproval index.
		/// </summary>
		/// <param name="_enumApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByEnumApprovalId(System.Int32 _enumApprovalId, int start, int pageLength, out int count)
		{
			return GetByEnumApprovalId(null, _enumApprovalId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryStandardApproval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enumApprovalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EnumApproval GetByEnumApprovalId(TransactionManager transactionManager, System.Int32 _enumApprovalId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_EnumApproval_Approval index.
		/// </summary>
		/// <param name="_approval"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByApproval(System.Boolean? _approval)
		{
			int count = -1;
			return GetByApproval(null,_approval, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumApproval_Approval index.
		/// </summary>
		/// <param name="_approval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByApproval(System.Boolean? _approval, int start, int pageLength)
		{
			int count = -1;
			return GetByApproval(null, _approval, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumApproval_Approval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approval"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByApproval(TransactionManager transactionManager, System.Boolean? _approval)
		{
			int count = -1;
			return GetByApproval(transactionManager, _approval, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumApproval_Approval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByApproval(TransactionManager transactionManager, System.Boolean? _approval, int start, int pageLength)
		{
			int count = -1;
			return GetByApproval(transactionManager, _approval, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumApproval_Approval index.
		/// </summary>
		/// <param name="_approval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public KaiZen.CSMS.Entities.EnumApproval GetByApproval(System.Boolean? _approval, int start, int pageLength, out int count)
		{
			return GetByApproval(null, _approval, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EnumApproval_Approval index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approval"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EnumApproval GetByApproval(TransactionManager transactionManager, System.Boolean? _approval, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EnumApproval&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EnumApproval&gt;"/></returns>
		public static TList<EnumApproval> Fill(IDataReader reader, TList<EnumApproval> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EnumApproval c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EnumApproval")
					.Append("|").Append((System.Int32)reader[((int)EnumApprovalColumn.EnumApprovalId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EnumApproval>(
					key.ToString(), // EntityTrackingKey
					"EnumApproval",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EnumApproval();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EnumApprovalId = (System.Int32)reader[((int)EnumApprovalColumn.EnumApprovalId - 1)];
					c.Approval = (reader.IsDBNull(((int)EnumApprovalColumn.Approval - 1)))?null:(System.Boolean?)reader[((int)EnumApprovalColumn.Approval - 1)];
					c.ApprovalText = (System.String)reader[((int)EnumApprovalColumn.ApprovalText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EnumApproval"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EnumApproval entity)
		{
			if (!reader.Read()) return;
			
			entity.EnumApprovalId = (System.Int32)reader[((int)EnumApprovalColumn.EnumApprovalId - 1)];
			entity.Approval = (reader.IsDBNull(((int)EnumApprovalColumn.Approval - 1)))?null:(System.Boolean?)reader[((int)EnumApprovalColumn.Approval - 1)];
			entity.ApprovalText = (System.String)reader[((int)EnumApprovalColumn.ApprovalText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EnumApproval"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EnumApproval"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EnumApproval entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EnumApprovalId = (System.Int32)dataRow["EnumApprovalId"];
			entity.Approval = Convert.IsDBNull(dataRow["Approval"]) ? null : (System.Boolean?)dataRow["Approval"];
			entity.ApprovalText = (System.String)dataRow["ApprovalText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EnumApproval"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EnumApproval Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumApproval entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EnumApproval object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EnumApproval instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EnumApproval Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EnumApproval entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EnumApprovalChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EnumApproval</c>
	///</summary>
	public enum EnumApprovalChildEntityTypes
	{
	}
	
	#endregion EnumApprovalChildEntityTypes
	
	#region EnumApprovalFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EnumApprovalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumApprovalFilterBuilder : SqlFilterBuilder<EnumApprovalColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumApprovalFilterBuilder class.
		/// </summary>
		public EnumApprovalFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumApprovalFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumApprovalFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumApprovalFilterBuilder
	
	#region EnumApprovalParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EnumApprovalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumApproval"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumApprovalParameterBuilder : ParameterizedSqlFilterBuilder<EnumApprovalColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumApprovalParameterBuilder class.
		/// </summary>
		public EnumApprovalParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumApprovalParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumApprovalParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumApprovalParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumApprovalParameterBuilder
	
	#region EnumApprovalSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EnumApprovalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumApproval"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EnumApprovalSortBuilder : SqlSortBuilder<EnumApprovalColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumApprovalSqlSortBuilder class.
		/// </summary>
		public EnumApprovalSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EnumApprovalSortBuilder
	
} // end namespace
