﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyRelationshipService
    {
        CompaniesRelationship Get(Expression<Func<CompaniesRelationship, bool>> where, List<Expression<Func<CompaniesRelationship, object>>> includes);
        List<CompaniesRelationship> GetMany(int? top, Expression<Func<CompaniesRelationship, bool>> where, Func<IQueryable<CompaniesRelationship>, IOrderedQueryable<CompaniesRelationship>> orderBy, List<Expression<Func<CompaniesRelationship, object>>> includes);
        List<CompaniesRelationship> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesRelationship, bool>> where, Func<IQueryable<CompaniesRelationship>, IOrderedQueryable<CompaniesRelationship>> orderBy, List<Expression<Func<CompaniesRelationship, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompaniesRelationship, bool>> where, List<Expression<Func<CompaniesRelationship, object>>> includes);
        void Insert(CompaniesRelationship item, bool saveChanges = true);
        void Update(CompaniesRelationship item, bool saveChanges = true);
        void Delete(CompaniesRelationship item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyRelationshipService : ICompanyRelationshipService
    {
        private readonly IRepository<CompaniesRelationship> _repository;

        public CompanyRelationshipService(IRepository<CompaniesRelationship> repository)
        {
            this._repository = repository;
        }

        public CompaniesRelationship Get(Expression<Func<CompaniesRelationship, bool>> where, List<Expression<Func<CompaniesRelationship, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompaniesRelationship> GetMany(int? top, Expression<Func<CompaniesRelationship, bool>> where, Func<IQueryable<CompaniesRelationship>, IOrderedQueryable<CompaniesRelationship>> orderBy, List<Expression<Func<CompaniesRelationship, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompaniesRelationship> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesRelationship, bool>> where, Func<IQueryable<CompaniesRelationship>, IOrderedQueryable<CompaniesRelationship>> orderBy, List<Expression<Func<CompaniesRelationship, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompaniesRelationship, bool>> where, List<Expression<Func<CompaniesRelationship, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompaniesRelationship item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesRelationship");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompaniesRelationship item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesRelationship");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompaniesRelationship item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesRelationship");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
