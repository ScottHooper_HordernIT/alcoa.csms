﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SqExemptionFriendlyCurrentProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class SqExemptionFriendlyCurrentProviderBaseCore : EntityViewProviderBase<SqExemptionFriendlyCurrent>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;SqExemptionFriendlyCurrent&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;SqExemptionFriendlyCurrent&gt;"/></returns>
		protected static VList&lt;SqExemptionFriendlyCurrent&gt; Fill(DataSet dataSet, VList<SqExemptionFriendlyCurrent> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<SqExemptionFriendlyCurrent>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;SqExemptionFriendlyCurrent&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<SqExemptionFriendlyCurrent>"/></returns>
		protected static VList&lt;SqExemptionFriendlyCurrent&gt; Fill(DataTable dataTable, VList<SqExemptionFriendlyCurrent> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					SqExemptionFriendlyCurrent c = new SqExemptionFriendlyCurrent();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.RequestingCompanyName = (Convert.IsDBNull(row["RequestingCompanyName"]))?string.Empty:(System.String)row["RequestingCompanyName"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.DateApplied = (Convert.IsDBNull(row["DateApplied"]))?DateTime.MinValue:(System.DateTime)row["DateApplied"];
					c.ValidFrom = (Convert.IsDBNull(row["ValidFrom"]))?DateTime.MinValue:(System.DateTime)row["ValidFrom"];
					c.ValidTo = (Convert.IsDBNull(row["ValidTo"]))?DateTime.MinValue:(System.DateTime)row["ValidTo"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.SqExemptionId = (Convert.IsDBNull(row["SqExemptionId"]))?(int)0:(System.Int32)row["SqExemptionId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;SqExemptionFriendlyCurrent&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;SqExemptionFriendlyCurrent&gt;"/></returns>
		protected VList<SqExemptionFriendlyCurrent> Fill(IDataReader reader, VList<SqExemptionFriendlyCurrent> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					SqExemptionFriendlyCurrent entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<SqExemptionFriendlyCurrent>("SqExemptionFriendlyCurrent",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new SqExemptionFriendlyCurrent();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)SqExemptionFriendlyCurrentColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.RequestingCompanyName = (System.String)reader[((int)SqExemptionFriendlyCurrentColumn.RequestingCompanyName)];
					//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
					entity.SiteName = (System.String)reader[((int)SqExemptionFriendlyCurrentColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.DateApplied = (System.DateTime)reader[((int)SqExemptionFriendlyCurrentColumn.DateApplied)];
					//entity.DateApplied = (Convert.IsDBNull(reader["DateApplied"]))?DateTime.MinValue:(System.DateTime)reader["DateApplied"];
					entity.ValidFrom = (System.DateTime)reader[((int)SqExemptionFriendlyCurrentColumn.ValidFrom)];
					//entity.ValidFrom = (Convert.IsDBNull(reader["ValidFrom"]))?DateTime.MinValue:(System.DateTime)reader["ValidFrom"];
					entity.ValidTo = (System.DateTime)reader[((int)SqExemptionFriendlyCurrentColumn.ValidTo)];
					//entity.ValidTo = (Convert.IsDBNull(reader["ValidTo"]))?DateTime.MinValue:(System.DateTime)reader["ValidTo"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)SqExemptionFriendlyCurrentColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)SqExemptionFriendlyCurrentColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.SqExemptionId = (System.Int32)reader[((int)SqExemptionFriendlyCurrentColumn.SqExemptionId)];
					//entity.SqExemptionId = (Convert.IsDBNull(reader["SqExemptionId"]))?(int)0:(System.Int32)reader["SqExemptionId"];
					entity.CompanyId = (System.Int32)reader[((int)SqExemptionFriendlyCurrentColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="SqExemptionFriendlyCurrent"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="SqExemptionFriendlyCurrent"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, SqExemptionFriendlyCurrent entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)SqExemptionFriendlyCurrentColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.RequestingCompanyName = (System.String)reader[((int)SqExemptionFriendlyCurrentColumn.RequestingCompanyName)];
			//entity.RequestingCompanyName = (Convert.IsDBNull(reader["RequestingCompanyName"]))?string.Empty:(System.String)reader["RequestingCompanyName"];
			entity.SiteName = (System.String)reader[((int)SqExemptionFriendlyCurrentColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.DateApplied = (System.DateTime)reader[((int)SqExemptionFriendlyCurrentColumn.DateApplied)];
			//entity.DateApplied = (Convert.IsDBNull(reader["DateApplied"]))?DateTime.MinValue:(System.DateTime)reader["DateApplied"];
			entity.ValidFrom = (System.DateTime)reader[((int)SqExemptionFriendlyCurrentColumn.ValidFrom)];
			//entity.ValidFrom = (Convert.IsDBNull(reader["ValidFrom"]))?DateTime.MinValue:(System.DateTime)reader["ValidFrom"];
			entity.ValidTo = (System.DateTime)reader[((int)SqExemptionFriendlyCurrentColumn.ValidTo)];
			//entity.ValidTo = (Convert.IsDBNull(reader["ValidTo"]))?DateTime.MinValue:(System.DateTime)reader["ValidTo"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)SqExemptionFriendlyCurrentColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)SqExemptionFriendlyCurrentColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.SqExemptionId = (System.Int32)reader[((int)SqExemptionFriendlyCurrentColumn.SqExemptionId)];
			//entity.SqExemptionId = (Convert.IsDBNull(reader["SqExemptionId"]))?(int)0:(System.Int32)reader["SqExemptionId"];
			entity.CompanyId = (System.Int32)reader[((int)SqExemptionFriendlyCurrentColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="SqExemptionFriendlyCurrent"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="SqExemptionFriendlyCurrent"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, SqExemptionFriendlyCurrent entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.RequestingCompanyName = (Convert.IsDBNull(dataRow["RequestingCompanyName"]))?string.Empty:(System.String)dataRow["RequestingCompanyName"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.DateApplied = (Convert.IsDBNull(dataRow["DateApplied"]))?DateTime.MinValue:(System.DateTime)dataRow["DateApplied"];
			entity.ValidFrom = (Convert.IsDBNull(dataRow["ValidFrom"]))?DateTime.MinValue:(System.DateTime)dataRow["ValidFrom"];
			entity.ValidTo = (Convert.IsDBNull(dataRow["ValidTo"]))?DateTime.MinValue:(System.DateTime)dataRow["ValidTo"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.SqExemptionId = (Convert.IsDBNull(dataRow["SqExemptionId"]))?(int)0:(System.Int32)dataRow["SqExemptionId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region SqExemptionFriendlyCurrentFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyCurrentFilterBuilder : SqlFilterBuilder<SqExemptionFriendlyCurrentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentFilterBuilder class.
		/// </summary>
		public SqExemptionFriendlyCurrentFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyCurrentFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyCurrentFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyCurrentFilterBuilder

	#region SqExemptionFriendlyCurrentParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyCurrentParameterBuilder : ParameterizedSqlFilterBuilder<SqExemptionFriendlyCurrentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentParameterBuilder class.
		/// </summary>
		public SqExemptionFriendlyCurrentParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SqExemptionFriendlyCurrentParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SqExemptionFriendlyCurrentParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SqExemptionFriendlyCurrentParameterBuilder
	
	#region SqExemptionFriendlyCurrentSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SqExemptionFriendlyCurrentSortBuilder : SqlSortBuilder<SqExemptionFriendlyCurrentColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentSqlSortBuilder class.
		/// </summary>
		public SqExemptionFriendlyCurrentSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SqExemptionFriendlyCurrentSortBuilder

} // end namespace
