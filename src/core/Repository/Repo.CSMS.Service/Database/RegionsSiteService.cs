﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IRegionsSiteService
    {
        RegionsSite Get(Expression<Func<RegionsSite, bool>> where, List<Expression<Func<RegionsSite, object>>> includes);
        List<RegionsSite> GetMany(int? top, Expression<Func<RegionsSite, bool>> where, Func<IQueryable<RegionsSite>, IOrderedQueryable<RegionsSite>> orderBy, List<Expression<Func<RegionsSite, object>>> includes);
        List<RegionsSite> GetManyPaged(int pageNumber, int pageSize, Expression<Func<RegionsSite, bool>> where, Func<IQueryable<RegionsSite>, IOrderedQueryable<RegionsSite>> orderBy, List<Expression<Func<RegionsSite, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<RegionsSite, bool>> where, List<Expression<Func<RegionsSite, object>>> includes);
        void Insert(RegionsSite item, bool saveChanges = true);
        void Update(RegionsSite item, bool saveChanges = true);
        void Delete(RegionsSite item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class RegionsSiteService : IRegionsSiteService
    {
        private readonly IRepository<RegionsSite> _repository;

        public RegionsSiteService(IRepository<RegionsSite> repository)
        {
            this._repository = repository;
        }

        public RegionsSite Get(Expression<Func<RegionsSite, bool>> where, List<Expression<Func<RegionsSite, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<RegionsSite> GetMany(int? top, Expression<Func<RegionsSite, bool>> where, Func<IQueryable<RegionsSite>, IOrderedQueryable<RegionsSite>> orderBy, List<Expression<Func<RegionsSite, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<RegionsSite> GetManyPaged(int pageNumber, int pageSize, Expression<Func<RegionsSite, bool>> where, Func<IQueryable<RegionsSite>, IOrderedQueryable<RegionsSite>> orderBy, List<Expression<Func<RegionsSite, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<RegionsSite, bool>> where, List<Expression<Func<RegionsSite, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(RegionsSite item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("RegionsSite");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(RegionsSite item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("RegionsSite");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(RegionsSite item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("RegionsSite");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
