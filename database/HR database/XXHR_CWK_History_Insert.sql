USE [HR]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[XXHR_CWK_History_Insert]      
(      
 @AssignmentId numeric(20,0),    
 @PERSON_ID numeric(10,0),       
 @CWK_NBR varchar(50),      
 @FULL_NAME varchar(max),      
 @LAST_NAME varchar(max),      
 @FIRST_NAME varchar(max),      
 @MIDDLE_NAMES varchar(max),      
 @AOA_PREF_NAME varchar(max),      
 @Gender varchar(50),      
 @LOGINID varchar(max),      
 @PER_TYPE_ID varchar(50),      
 @SERVICE_DATE datetime,      
 @ORIGINAL_DATE_OF_HIRE datetime,      
 @TERMINATION_DATE datetime,      
 @Pos_Descr varchar(max),      
 @Supervisor_Id varchar(50),      
 @EFFECTIVE_START_DATE datetime,      
 @EFFECTIVE_END_DATE datetime,      
 @ALCOA_PRIV varchar(max),      
 @LABOUR_CLASS varchar(max),      
 @NON_PREFF_SUPP varchar(max),      
 @SPONSOR_ID varchar(max),      
 @SUPPLY_SUP varchar(max),      
 @LOCATION_CODE varchar(max),      
 @ORGANIZATION_ID numeric(15,0),      
 @ORG_CODE varchar(max),      
 @DEPT_ORG_CODE varchar(max),      
 @OP_CENTRE varchar(max),      
 @NAME varchar(max),      
 @LBC varchar(max),      
 @DEPT varchar(max),      
 @ACCOUNT varchar(max),      
 @CWK_VENDOR_ID numeric(30,0),      
 @CWK_VENDOR_NO varchar(max),      
 @CWK_VENDOR_NAME varchar(max),      
 @CWK_NON_PREFERRED_SUPP_NAME varchar(max),      
 @CWK_SPONSOR_ID varchar(max),      
 @CWK_SUPP_SUPERVISOR_ID varchar(max),      
 @CWK_PRIVILEGE_FLAG varchar(max),      
 @LAST_UPDATE_DATE_PER datetime,      
 @LAST_UPDATE_DATE_ASS datetime      
 )      
AS      
      
    
 INSERT INTO HR..XXHR_CWK_HISTORY values      
 (      
 @AssignmentId,    
 @PERSON_ID,       
 @CWK_NBR,      
 @FULL_NAME,      
 @LAST_NAME,      
 @FIRST_NAME,      
 @MIDDLE_NAMES,      
 @AOA_PREF_NAME,      
 @Gender,      
 @LOGINID,      
 @PER_TYPE_ID,      
 @SERVICE_DATE ,      
 @ORIGINAL_DATE_OF_HIRE ,      
 @TERMINATION_DATE ,      
 @Pos_Descr,      
 @Supervisor_Id,      
 @EFFECTIVE_START_DATE ,      
 @EFFECTIVE_END_DATE ,      
 @ALCOA_PRIV ,      
 @LABOUR_CLASS ,      
 @NON_PREFF_SUPP ,      
 @SPONSOR_ID ,      
 @SUPPLY_SUP,      
 @LOCATION_CODE ,      
 @ORGANIZATION_ID,      
 @ORG_CODE ,      
 @DEPT_ORG_CODE ,      
 @OP_CENTRE ,      
 @NAME ,      
 @LBC ,      
 @DEPT ,      
 @ACCOUNT ,      
 @CWK_VENDOR_ID ,      
 @CWK_VENDOR_NO ,      
 @CWK_VENDOR_NAME ,      
 @CWK_NON_PREFERRED_SUPP_NAME ,      
 @CWK_SPONSOR_ID ,      
 @CWK_SUPP_SUPERVISOR_ID ,      
 @CWK_PRIVILEGE_FLAG ,      
 @LAST_UPDATE_DATE_PER ,      
 @LAST_UPDATE_DATE_ASS ,      
 GETdATE()      
    
)  
    
--------------------------------------------------------------------------------

GO

