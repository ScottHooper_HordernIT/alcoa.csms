﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnairePresentlyWithActionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnairePresentlyWithActionDataSourceDesigner))]
	public class QuestionnairePresentlyWithActionDataSource : ProviderDataSource<QuestionnairePresentlyWithAction, QuestionnairePresentlyWithActionKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionDataSource class.
		/// </summary>
		public QuestionnairePresentlyWithActionDataSource() : base(new QuestionnairePresentlyWithActionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnairePresentlyWithActionDataSourceView used by the QuestionnairePresentlyWithActionDataSource.
		/// </summary>
		protected QuestionnairePresentlyWithActionDataSourceView QuestionnairePresentlyWithActionView
		{
			get { return ( View as QuestionnairePresentlyWithActionDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnairePresentlyWithActionDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnairePresentlyWithActionSelectMethod SelectMethod
		{
			get
			{
				QuestionnairePresentlyWithActionSelectMethod selectMethod = QuestionnairePresentlyWithActionSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnairePresentlyWithActionSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnairePresentlyWithActionDataSourceView class that is to be
		/// used by the QuestionnairePresentlyWithActionDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnairePresentlyWithActionDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnairePresentlyWithAction, QuestionnairePresentlyWithActionKey> GetNewDataSourceView()
		{
			return new QuestionnairePresentlyWithActionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnairePresentlyWithActionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnairePresentlyWithActionDataSourceView : ProviderDataSourceView<QuestionnairePresentlyWithAction, QuestionnairePresentlyWithActionKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnairePresentlyWithActionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnairePresentlyWithActionDataSourceView(QuestionnairePresentlyWithActionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnairePresentlyWithActionDataSource QuestionnairePresentlyWithActionOwner
		{
			get { return Owner as QuestionnairePresentlyWithActionDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnairePresentlyWithActionSelectMethod SelectMethod
		{
			get { return QuestionnairePresentlyWithActionOwner.SelectMethod; }
			set { QuestionnairePresentlyWithActionOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnairePresentlyWithActionService QuestionnairePresentlyWithActionProvider
		{
			get { return Provider as QuestionnairePresentlyWithActionService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnairePresentlyWithAction> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnairePresentlyWithAction> results = null;
			QuestionnairePresentlyWithAction item;
			count = 0;
			
			System.Int32 _questionnairePresentlyWithActionId;
			System.Int32 _questionnairePresentlyWithUserId;
			System.String _actionName;
			System.Int32? _processNo_nullable;

			switch ( SelectMethod )
			{
				case QuestionnairePresentlyWithActionSelectMethod.Get:
					QuestionnairePresentlyWithActionKey entityKey  = new QuestionnairePresentlyWithActionKey();
					entityKey.Load(values);
					item = QuestionnairePresentlyWithActionProvider.Get(entityKey);
					results = new TList<QuestionnairePresentlyWithAction>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnairePresentlyWithActionSelectMethod.GetAll:
                    results = QuestionnairePresentlyWithActionProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnairePresentlyWithActionSelectMethod.GetPaged:
					results = QuestionnairePresentlyWithActionProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnairePresentlyWithActionSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnairePresentlyWithActionProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnairePresentlyWithActionProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnairePresentlyWithActionSelectMethod.GetByQuestionnairePresentlyWithActionId:
					_questionnairePresentlyWithActionId = ( values["QuestionnairePresentlyWithActionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithActionId"], typeof(System.Int32)) : (int)0;
					item = QuestionnairePresentlyWithActionProvider.GetByQuestionnairePresentlyWithActionId(_questionnairePresentlyWithActionId);
					results = new TList<QuestionnairePresentlyWithAction>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnairePresentlyWithActionSelectMethod.GetByQuestionnairePresentlyWithUserId:
					_questionnairePresentlyWithUserId = ( values["QuestionnairePresentlyWithUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnairePresentlyWithUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnairePresentlyWithActionProvider.GetByQuestionnairePresentlyWithUserId(_questionnairePresentlyWithUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnairePresentlyWithActionSelectMethod.GetByActionName:
					_actionName = ( values["ActionName"] != null ) ? (System.String) EntityUtil.ChangeType(values["ActionName"], typeof(System.String)) : string.Empty;
					item = QuestionnairePresentlyWithActionProvider.GetByActionName(_actionName);
					results = new TList<QuestionnairePresentlyWithAction>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnairePresentlyWithActionSelectMethod.GetByProcessNo:
					_processNo_nullable = (System.Int32?) EntityUtil.ChangeType(values["ProcessNo"], typeof(System.Int32?));
					item = QuestionnairePresentlyWithActionProvider.GetByProcessNo(_processNo_nullable);
					results = new TList<QuestionnairePresentlyWithAction>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnairePresentlyWithActionSelectMethod.Get || SelectMethod == QuestionnairePresentlyWithActionSelectMethod.GetByQuestionnairePresentlyWithActionId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnairePresentlyWithAction entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnairePresentlyWithActionProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnairePresentlyWithAction> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnairePresentlyWithActionProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnairePresentlyWithActionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnairePresentlyWithActionDataSource class.
	/// </summary>
	public class QuestionnairePresentlyWithActionDataSourceDesigner : ProviderDataSourceDesigner<QuestionnairePresentlyWithAction, QuestionnairePresentlyWithActionKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionDataSourceDesigner class.
		/// </summary>
		public QuestionnairePresentlyWithActionDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnairePresentlyWithActionSelectMethod SelectMethod
		{
			get { return ((QuestionnairePresentlyWithActionDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnairePresentlyWithActionDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnairePresentlyWithActionDataSourceActionList

	/// <summary>
	/// Supports the QuestionnairePresentlyWithActionDataSourceDesigner class.
	/// </summary>
	internal class QuestionnairePresentlyWithActionDataSourceActionList : DesignerActionList
	{
		private QuestionnairePresentlyWithActionDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithActionDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnairePresentlyWithActionDataSourceActionList(QuestionnairePresentlyWithActionDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnairePresentlyWithActionSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnairePresentlyWithActionDataSourceActionList
	
	#endregion QuestionnairePresentlyWithActionDataSourceDesigner
	
	#region QuestionnairePresentlyWithActionSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnairePresentlyWithActionDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnairePresentlyWithActionSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithActionId method.
		/// </summary>
		GetByQuestionnairePresentlyWithActionId,
		/// <summary>
		/// Represents the GetByQuestionnairePresentlyWithUserId method.
		/// </summary>
		GetByQuestionnairePresentlyWithUserId,
		/// <summary>
		/// Represents the GetByActionName method.
		/// </summary>
		GetByActionName,
		/// <summary>
		/// Represents the GetByProcessNo method.
		/// </summary>
		GetByProcessNo
	}
	
	#endregion QuestionnairePresentlyWithActionSelectMethod

	#region QuestionnairePresentlyWithActionFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionFilter : SqlFilter<QuestionnairePresentlyWithActionColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithActionFilter

	#region QuestionnairePresentlyWithActionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionExpressionBuilder : SqlExpressionBuilder<QuestionnairePresentlyWithActionColumn>
	{
	}
	
	#endregion QuestionnairePresentlyWithActionExpressionBuilder	

	#region QuestionnairePresentlyWithActionProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnairePresentlyWithActionChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithActionProperty : ChildEntityProperty<QuestionnairePresentlyWithActionChildEntityTypes>
	{
	}
	
	#endregion QuestionnairePresentlyWithActionProperty
}

