﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EmailLogTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EmailLogTypeDataSourceDesigner))]
	public class EmailLogTypeDataSource : ProviderDataSource<EmailLogType, EmailLogTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeDataSource class.
		/// </summary>
		public EmailLogTypeDataSource() : base(new EmailLogTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EmailLogTypeDataSourceView used by the EmailLogTypeDataSource.
		/// </summary>
		protected EmailLogTypeDataSourceView EmailLogTypeView
		{
			get { return ( View as EmailLogTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EmailLogTypeDataSource control invokes to retrieve data.
		/// </summary>
		public EmailLogTypeSelectMethod SelectMethod
		{
			get
			{
				EmailLogTypeSelectMethod selectMethod = EmailLogTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EmailLogTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EmailLogTypeDataSourceView class that is to be
		/// used by the EmailLogTypeDataSource.
		/// </summary>
		/// <returns>An instance of the EmailLogTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<EmailLogType, EmailLogTypeKey> GetNewDataSourceView()
		{
			return new EmailLogTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EmailLogTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EmailLogTypeDataSourceView : ProviderDataSourceView<EmailLogType, EmailLogTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EmailLogTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EmailLogTypeDataSourceView(EmailLogTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EmailLogTypeDataSource EmailLogTypeOwner
		{
			get { return Owner as EmailLogTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EmailLogTypeSelectMethod SelectMethod
		{
			get { return EmailLogTypeOwner.SelectMethod; }
			set { EmailLogTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EmailLogTypeService EmailLogTypeProvider
		{
			get { return Provider as EmailLogTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EmailLogType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EmailLogType> results = null;
			EmailLogType item;
			count = 0;
			
			System.Int32 _emailLogTypeId;
			System.String _emailLogTypeName;

			switch ( SelectMethod )
			{
				case EmailLogTypeSelectMethod.Get:
					EmailLogTypeKey entityKey  = new EmailLogTypeKey();
					entityKey.Load(values);
					item = EmailLogTypeProvider.Get(entityKey);
					results = new TList<EmailLogType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EmailLogTypeSelectMethod.GetAll:
                    results = EmailLogTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EmailLogTypeSelectMethod.GetPaged:
					results = EmailLogTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EmailLogTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = EmailLogTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EmailLogTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EmailLogTypeSelectMethod.GetByEmailLogTypeId:
					_emailLogTypeId = ( values["EmailLogTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EmailLogTypeId"], typeof(System.Int32)) : (int)0;
					item = EmailLogTypeProvider.GetByEmailLogTypeId(_emailLogTypeId);
					results = new TList<EmailLogType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EmailLogTypeSelectMethod.GetByEmailLogTypeName:
					_emailLogTypeName = ( values["EmailLogTypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["EmailLogTypeName"], typeof(System.String)) : string.Empty;
					item = EmailLogTypeProvider.GetByEmailLogTypeName(_emailLogTypeName);
					results = new TList<EmailLogType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EmailLogTypeSelectMethod.Get || SelectMethod == EmailLogTypeSelectMethod.GetByEmailLogTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EmailLogType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EmailLogTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EmailLogType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EmailLogTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EmailLogTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EmailLogTypeDataSource class.
	/// </summary>
	public class EmailLogTypeDataSourceDesigner : ProviderDataSourceDesigner<EmailLogType, EmailLogTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the EmailLogTypeDataSourceDesigner class.
		/// </summary>
		public EmailLogTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EmailLogTypeSelectMethod SelectMethod
		{
			get { return ((EmailLogTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EmailLogTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EmailLogTypeDataSourceActionList

	/// <summary>
	/// Supports the EmailLogTypeDataSourceDesigner class.
	/// </summary>
	internal class EmailLogTypeDataSourceActionList : DesignerActionList
	{
		private EmailLogTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EmailLogTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EmailLogTypeDataSourceActionList(EmailLogTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EmailLogTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EmailLogTypeDataSourceActionList
	
	#endregion EmailLogTypeDataSourceDesigner
	
	#region EmailLogTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EmailLogTypeDataSource.SelectMethod property.
	/// </summary>
	public enum EmailLogTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEmailLogTypeId method.
		/// </summary>
		GetByEmailLogTypeId,
		/// <summary>
		/// Represents the GetByEmailLogTypeName method.
		/// </summary>
		GetByEmailLogTypeName
	}
	
	#endregion EmailLogTypeSelectMethod

	#region EmailLogTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeFilter : SqlFilter<EmailLogTypeColumn>
	{
	}
	
	#endregion EmailLogTypeFilter

	#region EmailLogTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeExpressionBuilder : SqlExpressionBuilder<EmailLogTypeColumn>
	{
	}
	
	#endregion EmailLogTypeExpressionBuilder	

	#region EmailLogTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EmailLogTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EmailLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EmailLogTypeProperty : ChildEntityProperty<EmailLogTypeChildEntityTypes>
	{
	}
	
	#endregion EmailLogTypeProperty
}

