﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using System.Globalization;
using DevExpress.XtraCharts;
using DevExpress.Data;
using System.Drawing;

public partial class PopUps_SafetyPQ_QuestionnaireReport : System.Web.UI.Page
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private static bool isEHSConsultant(int UserId)
    {

        try
        {
            int count;
            EhsConsultantFilters queryEhsConsultant = new EhsConsultantFilters();
            queryEhsConsultant.Append(EhsConsultantColumn.UserId, UserId.ToString());
            TList<EhsConsultant> EhsConsultantList = DataRepository.EhsConsultantProvider.GetPaged(queryEhsConsultant.ToString(), null, 0, 100, out count);
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            return false;
        }
    }
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            //lol new changes. 25-03-09
            btnSave.Visible = false;
            //end

            if (auth.RoleId != (int)RoleList.Administrator)
            {
                if (!isEHSConsultant(auth.UserId))
                {
                    btnSave.Visible = false;
                    dtAssessmentDate.Enabled = false;
                    dtValid.Enabled = false;
                }
            }

            if (auth.RoleId == (int)RoleList.Disabled)
            {
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
            }

            string q = "";
            try
            {
                q = Request.QueryString["id"].ToString();
                int _q = Convert.ToInt32(q);
                SessionHandler.spVar_SafetyPlans_QuestionnaireId = q;

                lblLegalName.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", ""));
                cbOperatingName.Value = Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", "")));
                lblABN.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", ""));
                lblStreetAddress.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A"));
                lblCity.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B"));
                lblState.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C"));
                lblPostCode.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D"));
                lblCountry.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E"));
                lblContactName.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", ""));
                lblContactTitle.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", ""));
                lblContactEmail.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", ""));
                lblContactPhone.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", ""));
                lblContactFax.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(_q, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", ""));

                QuestionnaireService qs = new QuestionnaireService();
                Questionnaire questionnaire = qs.GetByQuestionnaireId(_q);


                if (questionnaire.MainAssessmentDate.HasValue)
                {
                    dtAssessmentDate.Date = (DateTime)questionnaire.MainAssessmentDate;
                }
                if (questionnaire.MainAssessmentValidTo.HasValue)
                {
                    dtValid.Date = (DateTime)questionnaire.MainAssessmentValidTo;
                }

                lblRiskRating.Text = questionnaire.MainAssessmentRiskRating;
                setScoreColor(lblRiskRating);

                UsersService usersService = new UsersService();
                CompaniesService companiesService = new CompaniesService();
                if (questionnaire.ApprovedByUserId != null)
                {
                    int i = Convert.ToInt32(questionnaire.ApprovedByUserId);

                    Users userEntity = usersService.GetByUserId(i);

                    Companies companiesEntity = companiesService.GetByCompanyId(userEntity.CompanyId);
                    lblAssessedBy.Text = userEntity.LastName + ", " + userEntity.FirstName + " (" + companiesEntity.CompanyName + ") at " + questionnaire.ApprovedDate.ToString();
                }
                else
                {
                    lblAssessedBy.Text = "-";
                    //check if this has been assessed by h&s assesors yet. if so, set name to lblassessedby.

                    if (questionnaire.AssessedByUserId != null)
                    {
                        Users userAssessor = usersService.GetByUserId(Convert.ToInt32(questionnaire.AssessedByUserId));
                        Companies companiesAssessor = companiesService.GetByCompanyId(userAssessor.CompanyId);
                        lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userAssessor.LastName, userAssessor.FirstName, companiesAssessor.CompanyName, questionnaire.AssessedDate);
                    }
                    else
                    {
                        //old ones. no assessedbyuserid/assesseddate columns.
                        int QuestionnaireMainAssessmentCount = 0;
                        int QuestionnaireVerificationAssessmentCount = 0;
                        DateTime? QuestionnaireMainAssessmentDateTime = null;
                        DateTime? QuestionnaireVerificationAssessmentDateTime = null;

                        QuestionnaireMainAssessmentFilters queryQuestionnaireMainAssessment = new QuestionnaireMainAssessmentFilters();
                        queryQuestionnaireMainAssessment.Append(QuestionnaireMainAssessmentColumn.QuestionnaireId, questionnaire.QuestionnaireId.ToString());

                        TList<QuestionnaireMainAssessment> QuestionnaireMainAssessmentList = DataRepository.QuestionnaireMainAssessmentProvider.GetPaged(queryQuestionnaireMainAssessment.ToString(), "ModifiedDate DESC, ResponseId DESC", 0, 99999, out QuestionnaireMainAssessmentCount);

                        if (QuestionnaireMainAssessmentCount > 0)
                        {
                            Users userAssessor = usersService.GetByUserId(QuestionnaireMainAssessmentList[0].ModifiedByUserId);
                            Companies companiesAssessor = companiesService.GetByCompanyId(userAssessor.CompanyId);
                            lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userAssessor.LastName, userAssessor.FirstName, companiesAssessor.CompanyName, QuestionnaireMainAssessmentList[0].ModifiedDate);
                            QuestionnaireMainAssessmentDateTime = QuestionnaireMainAssessmentList[0].ModifiedDate;
                        }

                        QuestionnaireVerificationAssessmentFilters queryQuestionnaireVerificationAssessment = new QuestionnaireVerificationAssessmentFilters();
                        queryQuestionnaireVerificationAssessment.Append(QuestionnaireVerificationAssessmentColumn.QuestionnaireId, questionnaire.QuestionnaireId.ToString());

                        TList<QuestionnaireVerificationAssessment> QuestionnaireVerificationAssessmentList = DataRepository.QuestionnaireVerificationAssessmentProvider.GetPaged(queryQuestionnaireVerificationAssessment.ToString(), "ModifiedDate DESC, ResponseId DESC", 0, 99999, out QuestionnaireVerificationAssessmentCount);

                        if (QuestionnaireVerificationAssessmentCount > 0)
                        {
                            QuestionnaireVerificationAssessmentDateTime = QuestionnaireVerificationAssessmentList[0].ModifiedDate;

                            if (QuestionnaireMainAssessmentDateTime == null || QuestionnaireVerificationAssessmentDateTime > QuestionnaireMainAssessmentDateTime)
                            {
                                Users userAssessor = usersService.GetByUserId(QuestionnaireVerificationAssessmentList[0].ModifiedByUserId);
                                Companies companiesAssessor = companiesService.GetByCompanyId(userAssessor.CompanyId);
                                lblAssessedBy.Text = String.Format("{0}, {1} ({2}) at {3}", userAssessor.LastName, userAssessor.FirstName, companiesAssessor.CompanyName, QuestionnaireVerificationAssessmentList[0].ModifiedDate);
                            }
                        }
                    }
                }


                if (questionnaire.MainScoreSystems.HasValue)
                {
                    lblSystemsScore.Text = questionnaire.MainScoreSystems.ToString();
                    //Decimal d = (Convert.ToDecimal(questionnaire.MainScoreSystems) / Convert.ToDecimal(lblSystemsScoreMax.Text)) * 100;
                    //questionnaire.MainScorePsystems = Convert.ToInt32(d);
                    Decimal d = Convert.ToDecimal(questionnaire.MainScorePsystems) * Convert.ToDecimal(0.3);
                    lblSystemsScoreP.Text = Decimal.Round(d, 2).ToString();
                    setScoreColor2(lblSystemsScoreP);

                }
                if (questionnaire.MainScoreStaffing.HasValue)
                {
                    lblStaffingScore.Text = questionnaire.MainScoreStaffing.ToString();
                    //Decimal d = (Convert.ToDecimal(questionnaire.MainScoreStaffing) / Convert.ToDecimal(lblStaffingScoreMax.Text)) * 100;
                    //questionnaire.MainScorePstaffing = Convert.ToInt32(d);
                    Decimal d = Convert.ToDecimal(questionnaire.MainScorePstaffing) * Convert.ToDecimal(0.25);
                    lblStaffingScoreP.Text = Decimal.Round(d, 2).ToString();
                    setScoreColor2(lblStaffingScoreP);
                }
                if (questionnaire.MainScoreExpectations.HasValue)
                {
                    lblExpectationsScore.Text = questionnaire.MainScoreExpectations.ToString();
                    //Decimal d = (Convert.ToDecimal(questionnaire.MainScoreExpectations) / Convert.ToDecimal(lblExpectationsScoreMax.Text)) * 100;
                    //questionnaire.MainScorePexpectations = Convert.ToInt32(d);
                    Decimal d = Convert.ToDecimal(questionnaire.MainScorePexpectations) * Convert.ToDecimal(0.25);
                    lblExpectationsScoreP.Text = Decimal.Round(d, 2).ToString();
                    setScoreColor2(lblExpectationsScoreP);
                }
                if (questionnaire.MainScoreResults.HasValue)
                {
                    lblResultsScore.Text = questionnaire.MainScoreResults.ToString();
                    //Decimal d = (Convert.ToDecimal(questionnaire.MainScoreResults) / Convert.ToDecimal(lblResultsScoreMax.Text)) * 100;
                    //questionnaire.MainScorePresults = Convert.ToInt32(d);
                    Decimal d = Convert.ToDecimal(questionnaire.MainScorePresults) * Convert.ToDecimal(0.2);
                    lblResultsScoreP.Text = Decimal.Round(d, 2).ToString();
                    setScoreColor(lblResultsScoreP);
                }
                if (questionnaire.MainScoreOverall.HasValue)
                {
                    lblOverallScore.Text = questionnaire.MainScoreOverall.ToString();
                    //Decimal d = (Convert.ToDecimal(questionnaire.MainScoreOverall) / Convert.ToDecimal(lblOverallScoreMax.Text)) * 100;
                    //questionnaire.MainScorePoverall = Convert.ToInt32(d);
                    Decimal d = Convert.ToDecimal(lblSystemsScoreP.Text) + Convert.ToDecimal(lblStaffingScoreP.Text) +
                                Convert.ToDecimal(lblExpectationsScoreP.Text) + Convert.ToDecimal(lblResultsScoreP.Text);
                    lblOverallScoreP.Text = Decimal.Round(d, 2).ToString();
                    setScoreColor2(lblOverallScoreP);
                    //if (questionnaire.MainScorePoverall > 0) lblRiskRating.Text = "High Risk";
                    //if (questionnaire.MainScorePoverall > 39) lblRiskRating.Text = "Medium Risk";
                    //if (questionnaire.MainScorePoverall > 74) lblRiskRating.Text = "Low Risk";
                }

                lblRiskRating.Text = questionnaire.MainAssessmentRiskRating;
                //qs.Save(questionnaire);
                SqlDataSource1.DataBind();

                WebChartControl1.Series[0].ArgumentScaleType = ScaleType.Qualitative;
                WebChartControl1.Series[0].ArgumentDataMember = "Section";
                WebChartControl1.Series[0].ValueScaleType = ScaleType.Numerical;
                WebChartControl1.Series[0].ValueDataMembers.AddRange(new string[] { "Score" });
                WebChartControl1.DataBind();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", ex.Message));
            }
        }
    }

    protected void setScoreColor(Label lbl)
    {
        switch (lbl.Text)
        {
            case "Low Risk":
                ASPxImage1.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                //lbl.ForeColor = Color.Green;
                break;
            case "Medium Risk":
                ASPxImage1.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                //lbl.ForeColor = Color.Yellow;
                break;
            case "High Risk":
                ASPxImage1.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                //lbl.ForeColor = Color.Red;
                break;
            case "Forced High":
                ASPxImage1.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                //lbl.ForeColor = Color.Red;
                break;
            default:
                lbl.ForeColor = Color.Black;
                break;
        }
    }
    protected static void setScoreColor2(Label lbl)
    {
        try
        {
            int i = Convert.ToInt32(lbl.Text);
            if (i > 0 && i < 40) lbl.ForeColor = Color.Red;
            if (i > 39 && i < 75) lbl.ForeColor = Color.Yellow;
            if (i > 74 && i < 101) lbl.ForeColor = Color.Green;
        }
        catch (Exception)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            //ToDo: Handle Exception
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        Page.Theme = "Default";
    }



    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            QuestionnaireService qs = new QuestionnaireService();

            string q = Request.QueryString["id"].ToString();
            int _q = Convert.ToInt32(q);

            Questionnaire questionnaire = qs.GetByQuestionnaireId(_q);

            if(!String.IsNullOrEmpty(dtAssessmentDate.Text))
            {
                questionnaire.MainAssessmentDate = DateTime.ParseExact(dtAssessmentDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            if(!String.IsNullOrEmpty(dtValid.Text))
            {
                questionnaire.MainAssessmentValidTo = DateTime.ParseExact(dtValid.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            questionnaire.MainAssessmentByUserId = auth.UserId;

            qs.Save(questionnaire);

            lblSave.Text = "Saved Successfully";
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            lblSave.Text = String.Format("Save Failed: {0}", ex.Message);
        }

    }
}
