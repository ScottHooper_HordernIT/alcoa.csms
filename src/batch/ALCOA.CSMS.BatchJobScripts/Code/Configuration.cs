using System;
using System.Data;
using System.Configuration;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

/// <summary>
/// Summary description for Configuration
/// </summary>

namespace ALCOA.CSMS.BatchJobScripts
{
    public class Configuration
    {
        public Configuration()
        {
        }

        public string GetValue(ConfigList Key)
        {
            ConfigService configService = new ConfigService();
            TList<Config> configList = configService.GetAll(); //All KPI
            TList<Config> configList2 = configList.FindAll( //Find KPI We Need
                delegate(Config config)
                {
                    return
                        config.Key == Key.ToString();
                }
            );

            return configList2[0].Value.ToString();
        }
    }
}
