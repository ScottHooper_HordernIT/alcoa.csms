﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireWithLocationApprovalViewLatestRepeater</c>
    /// </summary>
	public class QuestionnaireWithLocationApprovalViewLatestRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireWithLocationApprovalViewLatestRepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireWithLocationApprovalViewLatestRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireWithLocationApprovalViewLatestRepeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireWithLocationApprovalViewLatestRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireWithLocationApprovalViewLatestRepeater z = (QuestionnaireWithLocationApprovalViewLatestRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireWithLocationApprovalViewLatestRepeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireWithLocationApprovalViewLatestRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireWithLocationApprovalViewLatestRepeater runat=\"server\"></{0}:QuestionnaireWithLocationApprovalViewLatestRepeater>")]
	public class QuestionnaireWithLocationApprovalViewLatestRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireWithLocationApprovalViewLatestRepeater"/> class.
        /// </summary>
		public QuestionnaireWithLocationApprovalViewLatestRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireWithLocationApprovalViewLatestItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireWithLocationApprovalViewLatestItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireWithLocationApprovalViewLatestItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireWithLocationApprovalViewLatestItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireWithLocationApprovalViewLatestItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireWithLocationApprovalViewLatest entity = o as KaiZen.CSMS.Entities.QuestionnaireWithLocationApprovalViewLatest;
						QuestionnaireWithLocationApprovalViewLatestItem container = new QuestionnaireWithLocationApprovalViewLatestItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireWithLocationApprovalViewLatestItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireWithLocationApprovalViewLatest _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireWithLocationApprovalViewLatestItem"/> class.
        /// </summary>
		public QuestionnaireWithLocationApprovalViewLatestItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireWithLocationApprovalViewLatestItem"/> class.
        /// </summary>
		public QuestionnaireWithLocationApprovalViewLatestItem(KaiZen.CSMS.Entities.QuestionnaireWithLocationApprovalViewLatest entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the QuestionnaireId
        /// </summary>
        /// <value>The QuestionnaireId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QuestionnaireId
		{
			get { return _entity.QuestionnaireId; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the CreatedByUserId
        /// </summary>
        /// <value>The CreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CreatedByUserId
		{
			get { return _entity.CreatedByUserId; }
		}
        /// <summary>
        /// Gets the CreatedByUser
        /// </summary>
        /// <value>The CreatedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CreatedByUser
		{
			get { return _entity.CreatedByUser; }
		}
        /// <summary>
        /// Gets the CreatedDate
        /// </summary>
        /// <value>The CreatedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime CreatedDate
		{
			get { return _entity.CreatedDate; }
		}
        /// <summary>
        /// Gets the ModifiedByUserId
        /// </summary>
        /// <value>The ModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedByUserId
		{
			get { return _entity.ModifiedByUserId; }
		}
        /// <summary>
        /// Gets the ModifiedByUser
        /// </summary>
        /// <value>The ModifiedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ModifiedByUser
		{
			get { return _entity.ModifiedByUser; }
		}
        /// <summary>
        /// Gets the ModifiedDate
        /// </summary>
        /// <value>The ModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime ModifiedDate
		{
			get { return _entity.ModifiedDate; }
		}
        /// <summary>
        /// Gets the ApprovedDate
        /// </summary>
        /// <value>The ApprovedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? ApprovedDate
		{
			get { return _entity.ApprovedDate; }
		}
        /// <summary>
        /// Gets the DaysSinceLastActivity
        /// </summary>
        /// <value>The DaysSinceLastActivity.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? DaysSinceLastActivity
		{
			get { return _entity.DaysSinceLastActivity; }
		}
        /// <summary>
        /// Gets the QmaLastModified
        /// </summary>
        /// <value>The QmaLastModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QmaLastModified
		{
			get { return _entity.QmaLastModified; }
		}
        /// <summary>
        /// Gets the QvaLastModified
        /// </summary>
        /// <value>The QvaLastModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QvaLastModified
		{
			get { return _entity.QvaLastModified; }
		}
        /// <summary>
        /// Gets the AssessedDate
        /// </summary>
        /// <value>The AssessedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? AssessedDate
		{
			get { return _entity.AssessedDate; }
		}
        /// <summary>
        /// Gets the AssessedByUserId
        /// </summary>
        /// <value>The AssessedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? AssessedByUserId
		{
			get { return _entity.AssessedByUserId; }
		}
        /// <summary>
        /// Gets the Status
        /// </summary>
        /// <value>The Status.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Status
		{
			get { return _entity.Status; }
		}
        /// <summary>
        /// Gets the InitialCreatedByUserId
        /// </summary>
        /// <value>The InitialCreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? InitialCreatedByUserId
		{
			get { return _entity.InitialCreatedByUserId; }
		}
        /// <summary>
        /// Gets the InitialCreatedDate
        /// </summary>
        /// <value>The InitialCreatedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? InitialCreatedDate
		{
			get { return _entity.InitialCreatedDate; }
		}
        /// <summary>
        /// Gets the InitialSubmittedByUserId
        /// </summary>
        /// <value>The InitialSubmittedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? InitialSubmittedByUserId
		{
			get { return _entity.InitialSubmittedByUserId; }
		}
        /// <summary>
        /// Gets the InitialSubmittedDate
        /// </summary>
        /// <value>The InitialSubmittedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? InitialSubmittedDate
		{
			get { return _entity.InitialSubmittedDate; }
		}
        /// <summary>
        /// Gets the MainCreatedByUserId
        /// </summary>
        /// <value>The MainCreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainCreatedByUserId
		{
			get { return _entity.MainCreatedByUserId; }
		}
        /// <summary>
        /// Gets the MainCreatedDate
        /// </summary>
        /// <value>The MainCreatedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainCreatedDate
		{
			get { return _entity.MainCreatedDate; }
		}
        /// <summary>
        /// Gets the VerificationCreatedByUserId
        /// </summary>
        /// <value>The VerificationCreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? VerificationCreatedByUserId
		{
			get { return _entity.VerificationCreatedByUserId; }
		}
        /// <summary>
        /// Gets the VerificationCreatedDate
        /// </summary>
        /// <value>The VerificationCreatedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? VerificationCreatedDate
		{
			get { return _entity.VerificationCreatedDate; }
		}
        /// <summary>
        /// Gets the Recommended
        /// </summary>
        /// <value>The Recommended.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Recommended
		{
			get { return _entity.Recommended; }
		}
        /// <summary>
        /// Gets the IsMainRequired
        /// </summary>
        /// <value>The IsMainRequired.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsMainRequired
		{
			get { return _entity.IsMainRequired; }
		}
        /// <summary>
        /// Gets the IsVerificationRequired
        /// </summary>
        /// <value>The IsVerificationRequired.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsVerificationRequired
		{
			get { return _entity.IsVerificationRequired; }
		}
        /// <summary>
        /// Gets the InitialCategoryHigh
        /// </summary>
        /// <value>The InitialCategoryHigh.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? InitialCategoryHigh
		{
			get { return _entity.InitialCategoryHigh; }
		}
        /// <summary>
        /// Gets the InitialModifiedByUserId
        /// </summary>
        /// <value>The InitialModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? InitialModifiedByUserId
		{
			get { return _entity.InitialModifiedByUserId; }
		}
        /// <summary>
        /// Gets the InitialModifiedDate
        /// </summary>
        /// <value>The InitialModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? InitialModifiedDate
		{
			get { return _entity.InitialModifiedDate; }
		}
        /// <summary>
        /// Gets the InitialStatus
        /// </summary>
        /// <value>The InitialStatus.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 InitialStatus
		{
			get { return _entity.InitialStatus; }
		}
        /// <summary>
        /// Gets the InitialRiskAssessment
        /// </summary>
        /// <value>The InitialRiskAssessment.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String InitialRiskAssessment
		{
			get { return _entity.InitialRiskAssessment; }
		}
        /// <summary>
        /// Gets the MainModifiedByUserId
        /// </summary>
        /// <value>The MainModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainModifiedByUserId
		{
			get { return _entity.MainModifiedByUserId; }
		}
        /// <summary>
        /// Gets the MainModifiedDate
        /// </summary>
        /// <value>The MainModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainModifiedDate
		{
			get { return _entity.MainModifiedDate; }
		}
        /// <summary>
        /// Gets the MainScoreExpectations
        /// </summary>
        /// <value>The MainScoreExpectations.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScoreExpectations
		{
			get { return _entity.MainScoreExpectations; }
		}
        /// <summary>
        /// Gets the MainScoreOverall
        /// </summary>
        /// <value>The MainScoreOverall.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScoreOverall
		{
			get { return _entity.MainScoreOverall; }
		}
        /// <summary>
        /// Gets the MainScoreResults
        /// </summary>
        /// <value>The MainScoreResults.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScoreResults
		{
			get { return _entity.MainScoreResults; }
		}
        /// <summary>
        /// Gets the MainScoreStaffing
        /// </summary>
        /// <value>The MainScoreStaffing.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScoreStaffing
		{
			get { return _entity.MainScoreStaffing; }
		}
        /// <summary>
        /// Gets the MainScoreSystems
        /// </summary>
        /// <value>The MainScoreSystems.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScoreSystems
		{
			get { return _entity.MainScoreSystems; }
		}
        /// <summary>
        /// Gets the MainStatus
        /// </summary>
        /// <value>The MainStatus.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 MainStatus
		{
			get { return _entity.MainStatus; }
		}
        /// <summary>
        /// Gets the MainAssessmentByUserId
        /// </summary>
        /// <value>The MainAssessmentByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainAssessmentByUserId
		{
			get { return _entity.MainAssessmentByUserId; }
		}
        /// <summary>
        /// Gets the MainAssessmentComments
        /// </summary>
        /// <value>The MainAssessmentComments.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MainAssessmentComments
		{
			get { return _entity.MainAssessmentComments; }
		}
        /// <summary>
        /// Gets the MainAssessmentDate
        /// </summary>
        /// <value>The MainAssessmentDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainAssessmentDate
		{
			get { return _entity.MainAssessmentDate; }
		}
        /// <summary>
        /// Gets the MainAssessmentRiskRating
        /// </summary>
        /// <value>The MainAssessmentRiskRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MainAssessmentRiskRating
		{
			get { return _entity.MainAssessmentRiskRating; }
		}
        /// <summary>
        /// Gets the MainAssessmentStatus
        /// </summary>
        /// <value>The MainAssessmentStatus.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MainAssessmentStatus
		{
			get { return _entity.MainAssessmentStatus; }
		}
        /// <summary>
        /// Gets the MainAssessmentValidTo
        /// </summary>
        /// <value>The MainAssessmentValidTo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? MainAssessmentValidTo
		{
			get { return _entity.MainAssessmentValidTo; }
		}
        /// <summary>
        /// Gets the MainScorePexpectations
        /// </summary>
        /// <value>The MainScorePexpectations.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePexpectations
		{
			get { return _entity.MainScorePexpectations; }
		}
        /// <summary>
        /// Gets the MainScorePoverall
        /// </summary>
        /// <value>The MainScorePoverall.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePoverall
		{
			get { return _entity.MainScorePoverall; }
		}
        /// <summary>
        /// Gets the MainScorePresults
        /// </summary>
        /// <value>The MainScorePresults.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePresults
		{
			get { return _entity.MainScorePresults; }
		}
        /// <summary>
        /// Gets the MainScorePstaffing
        /// </summary>
        /// <value>The MainScorePstaffing.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePstaffing
		{
			get { return _entity.MainScorePstaffing; }
		}
        /// <summary>
        /// Gets the MainScorePsystems
        /// </summary>
        /// <value>The MainScorePsystems.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MainScorePsystems
		{
			get { return _entity.MainScorePsystems; }
		}
        /// <summary>
        /// Gets the VerificationModifiedByUserId
        /// </summary>
        /// <value>The VerificationModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? VerificationModifiedByUserId
		{
			get { return _entity.VerificationModifiedByUserId; }
		}
        /// <summary>
        /// Gets the VerificationModifiedDate
        /// </summary>
        /// <value>The VerificationModifiedDate.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? VerificationModifiedDate
		{
			get { return _entity.VerificationModifiedDate; }
		}
        /// <summary>
        /// Gets the VerificationRiskRating
        /// </summary>
        /// <value>The VerificationRiskRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String VerificationRiskRating
		{
			get { return _entity.VerificationRiskRating; }
		}
        /// <summary>
        /// Gets the VerificationStatus
        /// </summary>
        /// <value>The VerificationStatus.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 VerificationStatus
		{
			get { return _entity.VerificationStatus; }
		}
        /// <summary>
        /// Gets the SupplierContactFirstName
        /// </summary>
        /// <value>The SupplierContactFirstName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContactFirstName
		{
			get { return _entity.SupplierContactFirstName; }
		}
        /// <summary>
        /// Gets the SupplierContactLastName
        /// </summary>
        /// <value>The SupplierContactLastName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContactLastName
		{
			get { return _entity.SupplierContactLastName; }
		}
        /// <summary>
        /// Gets the SupplierContactEmail
        /// </summary>
        /// <value>The SupplierContactEmail.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SupplierContactEmail
		{
			get { return _entity.SupplierContactEmail; }
		}
        /// <summary>
        /// Gets the ProcurementContactUserId
        /// </summary>
        /// <value>The ProcurementContactUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ProcurementContactUserId
		{
			get { return _entity.ProcurementContactUserId; }
		}
        /// <summary>
        /// Gets the ContractManagerUserId
        /// </summary>
        /// <value>The ContractManagerUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ContractManagerUserId
		{
			get { return _entity.ContractManagerUserId; }
		}
        /// <summary>
        /// Gets the Kwi
        /// </summary>
        /// <value>The Kwi.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Kwi
		{
			get { return _entity.Kwi; }
		}
        /// <summary>
        /// Gets the Pin
        /// </summary>
        /// <value>The Pin.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pin
		{
			get { return _entity.Pin; }
		}
        /// <summary>
        /// Gets the Wgp
        /// </summary>
        /// <value>The Wgp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Wgp
		{
			get { return _entity.Wgp; }
		}
        /// <summary>
        /// Gets the Hun
        /// </summary>
        /// <value>The Hun.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Hun
		{
			get { return _entity.Hun; }
		}
        /// <summary>
        /// Gets the Wdl
        /// </summary>
        /// <value>The Wdl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Wdl
		{
			get { return _entity.Wdl; }
		}
        /// <summary>
        /// Gets the Bun
        /// </summary>
        /// <value>The Bun.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Bun
		{
			get { return _entity.Bun; }
		}
        /// <summary>
        /// Gets the Fml
        /// </summary>
        /// <value>The Fml.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Fml
		{
			get { return _entity.Fml; }
		}
        /// <summary>
        /// Gets the Bgn
        /// </summary>
        /// <value>The Bgn.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Bgn
		{
			get { return _entity.Bgn; }
		}
        /// <summary>
        /// Gets the Ang
        /// </summary>
        /// <value>The Ang.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ang
		{
			get { return _entity.Ang; }
		}
        /// <summary>
        /// Gets the Ptl
        /// </summary>
        /// <value>The Ptl.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Ptl
		{
			get { return _entity.Ptl; }
		}
        /// <summary>
        /// Gets the Pth
        /// </summary>
        /// <value>The Pth.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pth
		{
			get { return _entity.Pth; }
		}
        /// <summary>
        /// Gets the Pel
        /// </summary>
        /// <value>The Pel.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Pel
		{
			get { return _entity.Pel; }
		}
        /// <summary>
        /// Gets the Arp
        /// </summary>
        /// <value>The Arp.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Arp
		{
			get { return _entity.Arp; }
		}
        /// <summary>
        /// Gets the Yen
        /// </summary>
        /// <value>The Yen.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Yen
		{
			get { return _entity.Yen; }
		}
        /// <summary>
        /// Gets the ProcurementModified
        /// </summary>
        /// <value>The ProcurementModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean ProcurementModified
		{
			get { return _entity.ProcurementModified; }
		}
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the SafetyAssessor
        /// </summary>
        /// <value>The SafetyAssessor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SafetyAssessor
		{
			get { return _entity.SafetyAssessor; }
		}
        /// <summary>
        /// Gets the SafetyAssessorUserId
        /// </summary>
        /// <value>The SafetyAssessorUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? SafetyAssessorUserId
		{
			get { return _entity.SafetyAssessorUserId; }
		}
        /// <summary>
        /// Gets the SubContractor
        /// </summary>
        /// <value>The SubContractor.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? SubContractor
		{
			get { return _entity.SubContractor; }
		}
        /// <summary>
        /// Gets the CompanyStatusId
        /// </summary>
        /// <value>The CompanyStatusId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyStatusId
		{
			get { return _entity.CompanyStatusId; }
		}
        /// <summary>
        /// Gets the CompanyStatusDesc
        /// </summary>
        /// <value>The CompanyStatusDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyStatusDesc
		{
			get { return _entity.CompanyStatusDesc; }
		}
        /// <summary>
        /// Gets the Deactivated
        /// </summary>
        /// <value>The Deactivated.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean? Deactivated
		{
			get { return _entity.Deactivated; }
		}
        /// <summary>
        /// Gets the ProcurementContactUser
        /// </summary>
        /// <value>The ProcurementContactUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ProcurementContactUser
		{
			get { return _entity.ProcurementContactUser; }
		}
        /// <summary>
        /// Gets the ContractManagerUser
        /// </summary>
        /// <value>The ContractManagerUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ContractManagerUser
		{
			get { return _entity.ContractManagerUser; }
		}
        /// <summary>
        /// Gets the NoSqExpiryEmailsSent
        /// </summary>
        /// <value>The NoSqExpiryEmailsSent.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String NoSqExpiryEmailsSent
		{
			get { return _entity.NoSqExpiryEmailsSent; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithActionId
        /// </summary>
        /// <value>The QuestionnairePresentlyWithActionId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithActionId
		{
			get { return _entity.QuestionnairePresentlyWithActionId; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithUserId
        /// </summary>
        /// <value>The QuestionnairePresentlyWithUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithUserId
		{
			get { return _entity.QuestionnairePresentlyWithUserId; }
		}
        /// <summary>
        /// Gets the ProcessNo
        /// </summary>
        /// <value>The ProcessNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ProcessNo
		{
			get { return _entity.ProcessNo; }
		}
        /// <summary>
        /// Gets the UserName
        /// </summary>
        /// <value>The UserName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserName
		{
			get { return _entity.UserName; }
		}
        /// <summary>
        /// Gets the ActionName
        /// </summary>
        /// <value>The ActionName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionName
		{
			get { return _entity.ActionName; }
		}
        /// <summary>
        /// Gets the UserDescription
        /// </summary>
        /// <value>The UserDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserDescription
		{
			get { return _entity.UserDescription; }
		}
        /// <summary>
        /// Gets the ActionDescription
        /// </summary>
        /// <value>The ActionDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionDescription
		{
			get { return _entity.ActionDescription; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSince
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSince.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QuestionnairePresentlyWithSince
		{
			get { return _entity.QuestionnairePresentlyWithSince; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSinceDaysCount
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSinceDaysCount.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? QuestionnairePresentlyWithSinceDaysCount
		{
			get { return _entity.QuestionnairePresentlyWithSinceDaysCount; }
		}
        /// <summary>
        /// Gets the LevelOfSupervision
        /// </summary>
        /// <value>The LevelOfSupervision.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String LevelOfSupervision
		{
			get { return _entity.LevelOfSupervision; }
		}
        /// <summary>
        /// Gets the RecommendedComments
        /// </summary>
        /// <value>The RecommendedComments.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String RecommendedComments
		{
			get { return _entity.RecommendedComments; }
		}
        /// <summary>
        /// Gets the IsReQualification
        /// </summary>
        /// <value>The IsReQualification.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Boolean IsReQualification
		{
			get { return _entity.IsReQualification; }
		}
        /// <summary>
        /// Gets the ApprovedByUserId
        /// </summary>
        /// <value>The ApprovedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? ApprovedByUserId
		{
			get { return _entity.ApprovedByUserId; }
		}
        /// <summary>
        /// Gets the LastReminderEmailSentOn
        /// </summary>
        /// <value>The LastReminderEmailSentOn.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? LastReminderEmailSentOn
		{
			get { return _entity.LastReminderEmailSentOn; }
		}
        /// <summary>
        /// Gets the SupplierContactVerifiedOn
        /// </summary>
        /// <value>The SupplierContactVerifiedOn.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? SupplierContactVerifiedOn
		{
			get { return _entity.SupplierContactVerifiedOn; }
		}
        /// <summary>
        /// Gets the NoReminderEmailsSent
        /// </summary>
        /// <value>The NoReminderEmailsSent.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? NoReminderEmailsSent
		{
			get { return _entity.NoReminderEmailsSent; }
		}
        /// <summary>
        /// Gets the EhsConsultantId
        /// </summary>
        /// <value>The EhsConsultantId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? EhsConsultantId
		{
			get { return _entity.EhsConsultantId; }
		}

	}
}
