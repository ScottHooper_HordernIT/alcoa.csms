﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Questionnaire, KaiZen.CSMS.Entities.QuestionnaireKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireKey key)
		{
			return Delete(transactionManager, key.QuestionnaireId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireId)
		{
			return Delete(null, _questionnaireId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_AssessmentByUserId key.
		///		FK_Questionnaire_AssessmentByUserId Description: 
		/// </summary>
		/// <param name="_mainAssessmentByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainAssessmentByUserId(System.Int32? _mainAssessmentByUserId)
		{
			int count = -1;
			return GetByMainAssessmentByUserId(_mainAssessmentByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_AssessmentByUserId key.
		///		FK_Questionnaire_AssessmentByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainAssessmentByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByMainAssessmentByUserId(TransactionManager transactionManager, System.Int32? _mainAssessmentByUserId)
		{
			int count = -1;
			return GetByMainAssessmentByUserId(transactionManager, _mainAssessmentByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_AssessmentByUserId key.
		///		FK_Questionnaire_AssessmentByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainAssessmentByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainAssessmentByUserId(TransactionManager transactionManager, System.Int32? _mainAssessmentByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByMainAssessmentByUserId(transactionManager, _mainAssessmentByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_AssessmentByUserId key.
		///		fkQuestionnaireAssessmentByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainAssessmentByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainAssessmentByUserId(System.Int32? _mainAssessmentByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMainAssessmentByUserId(null, _mainAssessmentByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_AssessmentByUserId key.
		///		fkQuestionnaireAssessmentByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainAssessmentByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainAssessmentByUserId(System.Int32? _mainAssessmentByUserId, int start, int pageLength,out int count)
		{
			return GetByMainAssessmentByUserId(null, _mainAssessmentByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_AssessmentByUserId key.
		///		FK_Questionnaire_AssessmentByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainAssessmentByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByMainAssessmentByUserId(TransactionManager transactionManager, System.Int32? _mainAssessmentByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_InitialModifiedByUserId key.
		///		FK_Questionnaire_InitialModifiedByUserId Description: 
		/// </summary>
		/// <param name="_initialModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialModifiedByUserId(System.Int32? _initialModifiedByUserId)
		{
			int count = -1;
			return GetByInitialModifiedByUserId(_initialModifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_InitialModifiedByUserId key.
		///		FK_Questionnaire_InitialModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_initialModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByInitialModifiedByUserId(TransactionManager transactionManager, System.Int32? _initialModifiedByUserId)
		{
			int count = -1;
			return GetByInitialModifiedByUserId(transactionManager, _initialModifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_InitialModifiedByUserId key.
		///		FK_Questionnaire_InitialModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_initialModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialModifiedByUserId(TransactionManager transactionManager, System.Int32? _initialModifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByInitialModifiedByUserId(transactionManager, _initialModifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_InitialModifiedByUserId key.
		///		fkQuestionnaireInitialModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_initialModifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialModifiedByUserId(System.Int32? _initialModifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByInitialModifiedByUserId(null, _initialModifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_InitialModifiedByUserId key.
		///		fkQuestionnaireInitialModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_initialModifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialModifiedByUserId(System.Int32? _initialModifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByInitialModifiedByUserId(null, _initialModifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_InitialModifiedByUserId key.
		///		FK_Questionnaire_InitialModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_initialModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByInitialModifiedByUserId(TransactionManager transactionManager, System.Int32? _initialModifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_MainModifiedByUserId key.
		///		FK_Questionnaire_MainModifiedByUserId Description: 
		/// </summary>
		/// <param name="_mainModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainModifiedByUserId(System.Int32? _mainModifiedByUserId)
		{
			int count = -1;
			return GetByMainModifiedByUserId(_mainModifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_MainModifiedByUserId key.
		///		FK_Questionnaire_MainModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByMainModifiedByUserId(TransactionManager transactionManager, System.Int32? _mainModifiedByUserId)
		{
			int count = -1;
			return GetByMainModifiedByUserId(transactionManager, _mainModifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_MainModifiedByUserId key.
		///		FK_Questionnaire_MainModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainModifiedByUserId(TransactionManager transactionManager, System.Int32? _mainModifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByMainModifiedByUserId(transactionManager, _mainModifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_MainModifiedByUserId key.
		///		fkQuestionnaireMainModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainModifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainModifiedByUserId(System.Int32? _mainModifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByMainModifiedByUserId(null, _mainModifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_MainModifiedByUserId key.
		///		fkQuestionnaireMainModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainModifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainModifiedByUserId(System.Int32? _mainModifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByMainModifiedByUserId(null, _mainModifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_MainModifiedByUserId key.
		///		FK_Questionnaire_MainModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByMainModifiedByUserId(TransactionManager transactionManager, System.Int32? _mainModifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatus key.
		///		FK_Questionnaire_QuestionnaireStatus Description: 
		/// </summary>
		/// <param name="_status"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByStatus(System.Int32 _status)
		{
			int count = -1;
			return GetByStatus(_status, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatus key.
		///		FK_Questionnaire_QuestionnaireStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_status"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByStatus(TransactionManager transactionManager, System.Int32 _status)
		{
			int count = -1;
			return GetByStatus(transactionManager, _status, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatus key.
		///		FK_Questionnaire_QuestionnaireStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_status"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByStatus(TransactionManager transactionManager, System.Int32 _status, int start, int pageLength)
		{
			int count = -1;
			return GetByStatus(transactionManager, _status, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatus key.
		///		fkQuestionnaireQuestionnaireStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_status"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByStatus(System.Int32 _status, int start, int pageLength)
		{
			int count =  -1;
			return GetByStatus(null, _status, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatus key.
		///		fkQuestionnaireQuestionnaireStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_status"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByStatus(System.Int32 _status, int start, int pageLength,out int count)
		{
			return GetByStatus(null, _status, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatus key.
		///		FK_Questionnaire_QuestionnaireStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_status"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByStatus(TransactionManager transactionManager, System.Int32 _status, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusInitial key.
		///		FK_Questionnaire_QuestionnaireStatusInitial Description: 
		/// </summary>
		/// <param name="_initialStatus"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialStatus(System.Int32 _initialStatus)
		{
			int count = -1;
			return GetByInitialStatus(_initialStatus, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusInitial key.
		///		FK_Questionnaire_QuestionnaireStatusInitial Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_initialStatus"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByInitialStatus(TransactionManager transactionManager, System.Int32 _initialStatus)
		{
			int count = -1;
			return GetByInitialStatus(transactionManager, _initialStatus, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusInitial key.
		///		FK_Questionnaire_QuestionnaireStatusInitial Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_initialStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialStatus(TransactionManager transactionManager, System.Int32 _initialStatus, int start, int pageLength)
		{
			int count = -1;
			return GetByInitialStatus(transactionManager, _initialStatus, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusInitial key.
		///		fkQuestionnaireQuestionnaireStatusInitial Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_initialStatus"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialStatus(System.Int32 _initialStatus, int start, int pageLength)
		{
			int count =  -1;
			return GetByInitialStatus(null, _initialStatus, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusInitial key.
		///		fkQuestionnaireQuestionnaireStatusInitial Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_initialStatus"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByInitialStatus(System.Int32 _initialStatus, int start, int pageLength,out int count)
		{
			return GetByInitialStatus(null, _initialStatus, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusInitial key.
		///		FK_Questionnaire_QuestionnaireStatusInitial Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_initialStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByInitialStatus(TransactionManager transactionManager, System.Int32 _initialStatus, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusMain key.
		///		FK_Questionnaire_QuestionnaireStatusMain Description: 
		/// </summary>
		/// <param name="_mainStatus"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainStatus(System.Int32 _mainStatus)
		{
			int count = -1;
			return GetByMainStatus(_mainStatus, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusMain key.
		///		FK_Questionnaire_QuestionnaireStatusMain Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainStatus"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByMainStatus(TransactionManager transactionManager, System.Int32 _mainStatus)
		{
			int count = -1;
			return GetByMainStatus(transactionManager, _mainStatus, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusMain key.
		///		FK_Questionnaire_QuestionnaireStatusMain Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainStatus(TransactionManager transactionManager, System.Int32 _mainStatus, int start, int pageLength)
		{
			int count = -1;
			return GetByMainStatus(transactionManager, _mainStatus, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusMain key.
		///		fkQuestionnaireQuestionnaireStatusMain Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainStatus"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainStatus(System.Int32 _mainStatus, int start, int pageLength)
		{
			int count =  -1;
			return GetByMainStatus(null, _mainStatus, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusMain key.
		///		fkQuestionnaireQuestionnaireStatusMain Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_mainStatus"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByMainStatus(System.Int32 _mainStatus, int start, int pageLength,out int count)
		{
			return GetByMainStatus(null, _mainStatus, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusMain key.
		///		FK_Questionnaire_QuestionnaireStatusMain Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_mainStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByMainStatus(TransactionManager transactionManager, System.Int32 _mainStatus, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusVerification key.
		///		FK_Questionnaire_QuestionnaireStatusVerification Description: 
		/// </summary>
		/// <param name="_verificationStatus"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationStatus(System.Int32 _verificationStatus)
		{
			int count = -1;
			return GetByVerificationStatus(_verificationStatus, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusVerification key.
		///		FK_Questionnaire_QuestionnaireStatusVerification Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_verificationStatus"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByVerificationStatus(TransactionManager transactionManager, System.Int32 _verificationStatus)
		{
			int count = -1;
			return GetByVerificationStatus(transactionManager, _verificationStatus, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusVerification key.
		///		FK_Questionnaire_QuestionnaireStatusVerification Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_verificationStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationStatus(TransactionManager transactionManager, System.Int32 _verificationStatus, int start, int pageLength)
		{
			int count = -1;
			return GetByVerificationStatus(transactionManager, _verificationStatus, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusVerification key.
		///		fkQuestionnaireQuestionnaireStatusVerification Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_verificationStatus"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationStatus(System.Int32 _verificationStatus, int start, int pageLength)
		{
			int count =  -1;
			return GetByVerificationStatus(null, _verificationStatus, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusVerification key.
		///		fkQuestionnaireQuestionnaireStatusVerification Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_verificationStatus"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationStatus(System.Int32 _verificationStatus, int start, int pageLength,out int count)
		{
			return GetByVerificationStatus(null, _verificationStatus, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_QuestionnaireStatusVerification key.
		///		FK_Questionnaire_QuestionnaireStatusVerification Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_verificationStatus"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByVerificationStatus(TransactionManager transactionManager, System.Int32 _verificationStatus, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users key.
		///		FK_Questionnaire_Users Description: 
		/// </summary>
		/// <param name="_approvedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByApprovedByUserId(System.Int32? _approvedByUserId)
		{
			int count = -1;
			return GetByApprovedByUserId(_approvedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users key.
		///		FK_Questionnaire_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId)
		{
			int count = -1;
			return GetByApprovedByUserId(transactionManager, _approvedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users key.
		///		FK_Questionnaire_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByApprovedByUserId(transactionManager, _approvedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users key.
		///		fkQuestionnaireUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_approvedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByApprovedByUserId(System.Int32? _approvedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByApprovedByUserId(null, _approvedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users key.
		///		fkQuestionnaireUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByApprovedByUserId(System.Int32? _approvedByUserId, int start, int pageLength,out int count)
		{
			return GetByApprovedByUserId(null, _approvedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users key.
		///		FK_Questionnaire_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_approvedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByApprovedByUserId(TransactionManager transactionManager, System.Int32? _approvedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Created key.
		///		FK_Questionnaire_Users_Created Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Created key.
		///		FK_Questionnaire_Users_Created Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Created key.
		///		FK_Questionnaire_Users_Created Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Created key.
		///		fkQuestionnaireUsersCreated Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Created key.
		///		fkQuestionnaireUsersCreated Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Created key.
		///		FK_Questionnaire_Users_Created Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Modified key.
		///		FK_Questionnaire_Users_Modified Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Modified key.
		///		FK_Questionnaire_Users_Modified Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Modified key.
		///		FK_Questionnaire_Users_Modified Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Modified key.
		///		fkQuestionnaireUsersModified Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Modified key.
		///		fkQuestionnaireUsersModified Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_Users_Modified key.
		///		FK_Questionnaire_Users_Modified Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_VerificationModifiedByUserId key.
		///		FK_Questionnaire_VerificationModifiedByUserId Description: 
		/// </summary>
		/// <param name="_verificationModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationModifiedByUserId(System.Int32? _verificationModifiedByUserId)
		{
			int count = -1;
			return GetByVerificationModifiedByUserId(_verificationModifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_VerificationModifiedByUserId key.
		///		FK_Questionnaire_VerificationModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_verificationModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		/// <remarks></remarks>
		public TList<Questionnaire> GetByVerificationModifiedByUserId(TransactionManager transactionManager, System.Int32? _verificationModifiedByUserId)
		{
			int count = -1;
			return GetByVerificationModifiedByUserId(transactionManager, _verificationModifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_VerificationModifiedByUserId key.
		///		FK_Questionnaire_VerificationModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_verificationModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationModifiedByUserId(TransactionManager transactionManager, System.Int32? _verificationModifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByVerificationModifiedByUserId(transactionManager, _verificationModifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_VerificationModifiedByUserId key.
		///		fkQuestionnaireVerificationModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_verificationModifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationModifiedByUserId(System.Int32? _verificationModifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByVerificationModifiedByUserId(null, _verificationModifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_VerificationModifiedByUserId key.
		///		fkQuestionnaireVerificationModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_verificationModifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public TList<Questionnaire> GetByVerificationModifiedByUserId(System.Int32? _verificationModifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByVerificationModifiedByUserId(null, _verificationModifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Questionnaire_VerificationModifiedByUserId key.
		///		FK_Questionnaire_VerificationModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_verificationModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Questionnaire objects.</returns>
		public abstract TList<Questionnaire> GetByVerificationModifiedByUserId(TransactionManager transactionManager, System.Int32? _verificationModifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Questionnaire Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireKey key, int start, int pageLength)
		{
			return GetByQuestionnaireId(transactionManager, key.QuestionnaireId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Questionnaire index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> class.</returns>
		public KaiZen.CSMS.Entities.Questionnaire GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Questionnaire index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> class.</returns>
		public KaiZen.CSMS.Entities.Questionnaire GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Questionnaire index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> class.</returns>
		public KaiZen.CSMS.Entities.Questionnaire GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Questionnaire index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> class.</returns>
		public KaiZen.CSMS.Entities.Questionnaire GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Questionnaire index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> class.</returns>
		public KaiZen.CSMS.Entities.Questionnaire GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Questionnaire index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Questionnaire GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);

        #region GetByCompanyId

        public TList<Questionnaire> GetByCompanyId(System.Int32 _companyId)
        {
            int count = -1;
            return GetByCompanyId(null, _companyId, 0, int.MaxValue, out count);
        }


        public TList<Questionnaire> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
        {
            int count = -1;
            return GetByCompanyId(null, _companyId, start, pageLength, out count);
        }


        public TList<Questionnaire> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
        {
            int count = -1;
            return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
        }

        public TList<Questionnaire> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
        {
            int count = -1;
            return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
        }


        public abstract TList<Questionnaire> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
        
        #endregion








		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Questionnaire index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Questionnaire&gt;"/> class.</returns>
		public TList<Questionnaire> GetByQuestionnairePresentlyWithActionId(System.Int32? _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(null,_questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Questionnaire index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Questionnaire&gt;"/> class.</returns>
		public TList<Questionnaire> GetByQuestionnairePresentlyWithActionId(System.Int32? _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(null, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Questionnaire index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Questionnaire&gt;"/> class.</returns>
		public TList<Questionnaire> GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32? _questionnairePresentlyWithActionId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(transactionManager, _questionnairePresentlyWithActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Questionnaire index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Questionnaire&gt;"/> class.</returns>
		public TList<Questionnaire> GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32? _questionnairePresentlyWithActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithActionId(transactionManager, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Questionnaire index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Questionnaire&gt;"/> class.</returns>
		public TList<Questionnaire> GetByQuestionnairePresentlyWithActionId(System.Int32? _questionnairePresentlyWithActionId, int start, int pageLength, out int count)
		{
			return GetByQuestionnairePresentlyWithActionId(null, _questionnairePresentlyWithActionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Questionnaire index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Questionnaire&gt;"/> class.</returns>
		public abstract TList<Questionnaire> GetByQuestionnairePresentlyWithActionId(TransactionManager transactionManager, System.Int32? _questionnairePresentlyWithActionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Questionnaire_ListIfValidAndAllowedSubContractors_ByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListIfValidAndAllowedSubContractors_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListIfValidAndAllowedSubContractors_ByCompanyId(System.Int32? companyId)
		{
			return ListIfValidAndAllowedSubContractors_ByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListIfValidAndAllowedSubContractors_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListIfValidAndAllowedSubContractors_ByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return ListIfValidAndAllowedSubContractors_ByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_ListIfValidAndAllowedSubContractors_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListIfValidAndAllowedSubContractors_ByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return ListIfValidAndAllowedSubContractors_ByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListIfValidAndAllowedSubContractors_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListIfValidAndAllowedSubContractors_ByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Questionnaire_ListAssignedProcurementContacts 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementContacts' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedProcurementContacts()
		{
			return ListAssignedProcurementContacts(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementContacts' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedProcurementContacts(int start, int pageLength)
		{
			return ListAssignedProcurementContacts(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementContacts' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedProcurementContacts(TransactionManager transactionManager)
		{
			return ListAssignedProcurementContacts(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementContacts' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedProcurementContacts(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Questionnaire_GetProcurementWorkLoadByUserId_Questionnaires 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoadByUserId_Questionnaires' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementWorkLoadByUserId_Questionnaires(System.Int32? userId, System.Int32? functionalProcurementManagerUserId)
		{
			return GetProcurementWorkLoadByUserId_Questionnaires(null, 0, int.MaxValue , userId, functionalProcurementManagerUserId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoadByUserId_Questionnaires' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementWorkLoadByUserId_Questionnaires(int start, int pageLength, System.Int32? userId, System.Int32? functionalProcurementManagerUserId)
		{
			return GetProcurementWorkLoadByUserId_Questionnaires(null, start, pageLength , userId, functionalProcurementManagerUserId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoadByUserId_Questionnaires' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementWorkLoadByUserId_Questionnaires(TransactionManager transactionManager, System.Int32? userId, System.Int32? functionalProcurementManagerUserId)
		{
			return GetProcurementWorkLoadByUserId_Questionnaires(transactionManager, 0, int.MaxValue , userId, functionalProcurementManagerUserId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoadByUserId_Questionnaires' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetProcurementWorkLoadByUserId_Questionnaires(TransactionManager transactionManager, int start, int pageLength , System.Int32? userId, System.Int32? functionalProcurementManagerUserId);
		
		#endregion
		
		#region _Questionnaire_GetAssignedFunctionalProcurementManagers 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetAssignedFunctionalProcurementManagers' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAssignedFunctionalProcurementManagers()
		{
			return GetAssignedFunctionalProcurementManagers(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetAssignedFunctionalProcurementManagers' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAssignedFunctionalProcurementManagers(int start, int pageLength)
		{
			return GetAssignedFunctionalProcurementManagers(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetAssignedFunctionalProcurementManagers' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetAssignedFunctionalProcurementManagers(TransactionManager transactionManager)
		{
			return GetAssignedFunctionalProcurementManagers(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetAssignedFunctionalProcurementManagers' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetAssignedFunctionalProcurementManagers(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Questionnaire_GetLastModifiedQuestionnaireId 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLastModifiedQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLastModifiedQuestionnaireId(System.Int32? companyId)
		{
			return GetLastModifiedQuestionnaireId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLastModifiedQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLastModifiedQuestionnaireId(int start, int pageLength, System.Int32? companyId)
		{
			return GetLastModifiedQuestionnaireId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLastModifiedQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLastModifiedQuestionnaireId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetLastModifiedQuestionnaireId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLastModifiedQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetLastModifiedQuestionnaireId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Questionnaire_GetProcurementWorkLoad 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementWorkLoad(System.Int32? functionalProcurementManagerUserId)
		{
			return GetProcurementWorkLoad(null, 0, int.MaxValue , functionalProcurementManagerUserId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementWorkLoad(int start, int pageLength, System.Int32? functionalProcurementManagerUserId)
		{
			return GetProcurementWorkLoad(null, start, pageLength , functionalProcurementManagerUserId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementWorkLoad(TransactionManager transactionManager, System.Int32? functionalProcurementManagerUserId)
		{
			return GetProcurementWorkLoad(transactionManager, 0, int.MaxValue , functionalProcurementManagerUserId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetProcurementWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="functionalProcurementManagerUserId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetProcurementWorkLoad(TransactionManager transactionManager, int start, int pageLength , System.Int32? functionalProcurementManagerUserId);
		
		#endregion
		
		#region _Questionnaire_ListAssignedProcurementFunctionalManagers 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementFunctionalManagers' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedProcurementFunctionalManagers()
		{
			return ListAssignedProcurementFunctionalManagers(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementFunctionalManagers' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedProcurementFunctionalManagers(int start, int pageLength)
		{
			return ListAssignedProcurementFunctionalManagers(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementFunctionalManagers' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedProcurementFunctionalManagers(TransactionManager transactionManager)
		{
			return ListAssignedProcurementFunctionalManagers(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_ListAssignedProcurementFunctionalManagers' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedProcurementFunctionalManagers(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Questionnaire_DeleteByQuestionnaireId_VerificationOnly 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId_VerificationOnly' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void DeleteByQuestionnaireId_VerificationOnly(System.Int32? questionnaireId)
		{
			 DeleteByQuestionnaireId_VerificationOnly(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId_VerificationOnly' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void DeleteByQuestionnaireId_VerificationOnly(int start, int pageLength, System.Int32? questionnaireId)
		{
			 DeleteByQuestionnaireId_VerificationOnly(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId_VerificationOnly' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void DeleteByQuestionnaireId_VerificationOnly(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			 DeleteByQuestionnaireId_VerificationOnly(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId_VerificationOnly' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public abstract void DeleteByQuestionnaireId_VerificationOnly(TransactionManager transactionManager, int start, int pageLength , System.Int32? questionnaireId);
		
		#endregion
		
		#region _Questionnaire_GetDistinctYears 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears()
		{
			return GetDistinctYears(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears(int start, int pageLength)
		{
			return GetDistinctYears(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears(TransactionManager transactionManager)
		{
			return GetDistinctYears(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinctYears(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Questionnaire_GetEhsConsultantsWorkLoad 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetEhsConsultantsWorkLoad(System.String currentYear)
		{
			return GetEhsConsultantsWorkLoad(null, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetEhsConsultantsWorkLoad(int start, int pageLength, System.String currentYear)
		{
			return GetEhsConsultantsWorkLoad(null, start, pageLength , currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetEhsConsultantsWorkLoad(TransactionManager transactionManager, System.String currentYear)
		{
			return GetEhsConsultantsWorkLoad(transactionManager, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetEhsConsultantsWorkLoad(TransactionManager transactionManager, int start, int pageLength , System.String currentYear);
		
		#endregion
		
		#region _Questionnaire_GetLatestAssessmentComplete_ByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLatestAssessmentComplete_ByCompanyId(System.Int32? companyId)
		{
			return GetLatestAssessmentComplete_ByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLatestAssessmentComplete_ByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetLatestAssessmentComplete_ByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetLatestAssessmentComplete_ByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetLatestAssessmentComplete_ByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetLatestAssessmentComplete_ByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion

        //Start:Enhancement_009
        #region _Questionnaire_GetLastQuestionnaire_ByCompanyId

        /// <summary>
        ///	This method wrap the '_Questionnaire_GetLast_ByCompanyId' stored procedure. 
        /// </summary>
        /// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet GetLastQuestionnaire_ByCompanyId(System.Int32? companyId)
        {
            return GetLastQuestionnaire_ByCompanyId(null, 0, int.MaxValue, companyId);
        }

        /// <summary>
        ///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
        /// </summary>
        /// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet GetLastQuestionnaire_ByCompanyId(int start, int pageLength, System.Int32? companyId)
        {
            return GetLastQuestionnaire_ByCompanyId(null, start, pageLength, companyId);
        }

        /// <summary>
        ///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
        /// </summary>
        /// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet GetLastQuestionnaire_ByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
        {
            return GetLastQuestionnaire_ByCompanyId(transactionManager, 0, int.MaxValue, companyId);
        }



        /// <summary>
        ///	This method wrap the '_Questionnaire_GetLatestAssessmentComplete_ByCompanyId' stored procedure. 
        /// </summary>
        /// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <remark>This method is generate from a stored procedure.</remark>
        /// <returns>A <see cref="DataSet"/> instance.</returns>
        public abstract DataSet GetLastQuestionnaire_ByCompanyId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId);


        #endregion
        //End:Enhancement_009

		#region _Questionnaire_CountEhsConsultantsUnallocated 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CountEhsConsultantsUnallocated(System.String currentYear)
		{
			return CountEhsConsultantsUnallocated(null, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CountEhsConsultantsUnallocated(int start, int pageLength, System.String currentYear)
		{
			return CountEhsConsultantsUnallocated(null, start, pageLength , currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CountEhsConsultantsUnallocated(TransactionManager transactionManager, System.String currentYear)
		{
			return CountEhsConsultantsUnallocated(transactionManager, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CountEhsConsultantsUnallocated(TransactionManager transactionManager, int start, int pageLength , System.String currentYear);
		
		#endregion
		
		#region _Questionnaire_SendBackToProcurementStage 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_SendBackToProcurementStage' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void SendBackToProcurementStage(System.Int32? questionnaireId)
		{
			 SendBackToProcurementStage(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_SendBackToProcurementStage' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void SendBackToProcurementStage(int start, int pageLength, System.Int32? questionnaireId)
		{
			 SendBackToProcurementStage(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_SendBackToProcurementStage' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void SendBackToProcurementStage(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			 SendBackToProcurementStage(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_SendBackToProcurementStage' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public abstract void SendBackToProcurementStage(TransactionManager transactionManager, int start, int pageLength , System.Int32? questionnaireId);
		
		#endregion
		
		#region _Questionnaire_DeleteByQuestionnaireId 
		
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void DeleteByQuestionnaireId(System.Int32? questionnaireId)
		{
			 DeleteByQuestionnaireId(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void DeleteByQuestionnaireId(int start, int pageLength, System.Int32? questionnaireId)
		{
			 DeleteByQuestionnaireId(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void DeleteByQuestionnaireId(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			 DeleteByQuestionnaireId(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_Questionnaire_DeleteByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public abstract void DeleteByQuestionnaireId(TransactionManager transactionManager, int start, int pageLength , System.Int32? questionnaireId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Questionnaire&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Questionnaire&gt;"/></returns>
		public static TList<Questionnaire> Fill(IDataReader reader, TList<Questionnaire> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Questionnaire c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Questionnaire")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireColumn.QuestionnaireId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Questionnaire>(
					key.ToString(), // EntityTrackingKey
					"Questionnaire",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Questionnaire();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireColumn.QuestionnaireId - 1)];
					c.CompanyId = (System.Int32)reader[((int)QuestionnaireColumn.CompanyId - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireColumn.CreatedByUserId - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)QuestionnaireColumn.CreatedDate - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireColumn.ModifiedDate - 1)];
					c.Status = (System.Int32)reader[((int)QuestionnaireColumn.Status - 1)];
					c.IsReQualification = (System.Boolean)reader[((int)QuestionnaireColumn.IsReQualification - 1)];
					c.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireColumn.IsMainRequired - 1)];
					c.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireColumn.IsVerificationRequired - 1)];
					c.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireColumn.InitialCategoryHigh - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireColumn.InitialCategoryHigh - 1)];
					c.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.InitialCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.InitialCreatedByUserId - 1)];
					c.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireColumn.InitialCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.InitialCreatedDate - 1)];
					c.InitialSubmittedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.InitialSubmittedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.InitialSubmittedByUserId - 1)];
					c.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireColumn.InitialSubmittedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.InitialSubmittedDate - 1)];
					c.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.InitialModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.InitialModifiedByUserId - 1)];
					c.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireColumn.InitialModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.InitialModifiedDate - 1)];
					c.InitialStatus = (System.Int32)reader[((int)QuestionnaireColumn.InitialStatus - 1)];
					c.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireColumn.InitialRiskAssessment - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.InitialRiskAssessment - 1)];
					c.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.MainCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainCreatedByUserId - 1)];
					c.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireColumn.MainCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainCreatedDate - 1)];
					c.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.MainModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainModifiedByUserId - 1)];
					c.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireColumn.MainModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainModifiedDate - 1)];
					c.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreExpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreExpectations - 1)];
					c.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreOverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreOverall - 1)];
					c.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreResults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreResults - 1)];
					c.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreStaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreStaffing - 1)];
					c.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreSystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreSystems - 1)];
					c.MainStatus = (System.Int32)reader[((int)QuestionnaireColumn.MainStatus - 1)];
					c.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainAssessmentByUserId - 1)];
					c.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentComments - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.MainAssessmentComments - 1)];
					c.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainAssessmentDate - 1)];
					c.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.MainAssessmentRiskRating - 1)];
					c.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentStatus - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.MainAssessmentStatus - 1)];
					c.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentValidTo - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainAssessmentValidTo - 1)];
					c.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePexpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePexpectations - 1)];
					c.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePoverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePoverall - 1)];
					c.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePresults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePresults - 1)];
					c.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePstaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePstaffing - 1)];
					c.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePsystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePsystems - 1)];
					c.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.VerificationCreatedByUserId - 1)];
					c.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.VerificationCreatedDate - 1)];
					c.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.VerificationModifiedByUserId - 1)];
					c.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.VerificationModifiedDate - 1)];
					c.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.VerificationRiskRating - 1)];
					c.VerificationStatus = (System.Int32)reader[((int)QuestionnaireColumn.VerificationStatus - 1)];
					c.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.ApprovedByUserId - 1)];
					c.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.ApprovedDate - 1)];
					c.Recommended = (reader.IsDBNull(((int)QuestionnaireColumn.Recommended - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireColumn.Recommended - 1)];
					c.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireColumn.RecommendedComments - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.RecommendedComments - 1)];
					c.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireColumn.LevelOfSupervision - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.LevelOfSupervision - 1)];
					c.ProcurementModified = (System.Boolean)reader[((int)QuestionnaireColumn.ProcurementModified - 1)];
					c.SubContractor = (reader.IsDBNull(((int)QuestionnaireColumn.SubContractor - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireColumn.SubContractor - 1)];
					c.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.AssessedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.AssessedByUserId - 1)];
					c.AssessedDate = (reader.IsDBNull(((int)QuestionnaireColumn.AssessedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.AssessedDate - 1)];
					c.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireColumn.LastReminderEmailSentOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.LastReminderEmailSentOn - 1)];
					c.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireColumn.NoReminderEmailsSent - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.NoReminderEmailsSent - 1)];
					c.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireColumn.SupplierContactVerifiedOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.SupplierContactVerifiedOn - 1)];
					c.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireColumn.QuestionnairePresentlyWithActionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.QuestionnairePresentlyWithActionId - 1)];
					c.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireColumn.QuestionnairePresentlyWithSince - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.QuestionnairePresentlyWithSince - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Questionnaire"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Questionnaire entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireColumn.QuestionnaireId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireColumn.CompanyId - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireColumn.CreatedByUserId - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireColumn.CreatedDate - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireColumn.ModifiedDate - 1)];
			entity.Status = (System.Int32)reader[((int)QuestionnaireColumn.Status - 1)];
			entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireColumn.IsReQualification - 1)];
			entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireColumn.IsMainRequired - 1)];
			entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireColumn.IsVerificationRequired - 1)];
			entity.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireColumn.InitialCategoryHigh - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireColumn.InitialCategoryHigh - 1)];
			entity.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.InitialCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.InitialCreatedByUserId - 1)];
			entity.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireColumn.InitialCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.InitialCreatedDate - 1)];
			entity.InitialSubmittedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.InitialSubmittedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.InitialSubmittedByUserId - 1)];
			entity.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireColumn.InitialSubmittedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.InitialSubmittedDate - 1)];
			entity.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.InitialModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.InitialModifiedByUserId - 1)];
			entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireColumn.InitialModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.InitialModifiedDate - 1)];
			entity.InitialStatus = (System.Int32)reader[((int)QuestionnaireColumn.InitialStatus - 1)];
			entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireColumn.InitialRiskAssessment - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.InitialRiskAssessment - 1)];
			entity.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.MainCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainCreatedByUserId - 1)];
			entity.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireColumn.MainCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainCreatedDate - 1)];
			entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.MainModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainModifiedByUserId - 1)];
			entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireColumn.MainModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainModifiedDate - 1)];
			entity.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreExpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreExpectations - 1)];
			entity.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreOverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreOverall - 1)];
			entity.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreResults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreResults - 1)];
			entity.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreStaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreStaffing - 1)];
			entity.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireColumn.MainScoreSystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScoreSystems - 1)];
			entity.MainStatus = (System.Int32)reader[((int)QuestionnaireColumn.MainStatus - 1)];
			entity.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainAssessmentByUserId - 1)];
			entity.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentComments - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.MainAssessmentComments - 1)];
			entity.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainAssessmentDate - 1)];
			entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.MainAssessmentRiskRating - 1)];
			entity.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentStatus - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.MainAssessmentStatus - 1)];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireColumn.MainAssessmentValidTo - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.MainAssessmentValidTo - 1)];
			entity.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePexpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePexpectations - 1)];
			entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePoverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePoverall - 1)];
			entity.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePresults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePresults - 1)];
			entity.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePstaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePstaffing - 1)];
			entity.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireColumn.MainScorePsystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.MainScorePsystems - 1)];
			entity.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.VerificationCreatedByUserId - 1)];
			entity.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.VerificationCreatedDate - 1)];
			entity.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.VerificationModifiedByUserId - 1)];
			entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.VerificationModifiedDate - 1)];
			entity.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireColumn.VerificationRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.VerificationRiskRating - 1)];
			entity.VerificationStatus = (System.Int32)reader[((int)QuestionnaireColumn.VerificationStatus - 1)];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.ApprovedByUserId - 1)];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.ApprovedDate - 1)];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireColumn.Recommended - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireColumn.Recommended - 1)];
			entity.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireColumn.RecommendedComments - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.RecommendedComments - 1)];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireColumn.LevelOfSupervision - 1)))?null:(System.String)reader[((int)QuestionnaireColumn.LevelOfSupervision - 1)];
			entity.ProcurementModified = (System.Boolean)reader[((int)QuestionnaireColumn.ProcurementModified - 1)];
			entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireColumn.SubContractor - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireColumn.SubContractor - 1)];
			entity.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireColumn.AssessedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.AssessedByUserId - 1)];
			entity.AssessedDate = (reader.IsDBNull(((int)QuestionnaireColumn.AssessedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.AssessedDate - 1)];
			entity.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireColumn.LastReminderEmailSentOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.LastReminderEmailSentOn - 1)];
			entity.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireColumn.NoReminderEmailsSent - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.NoReminderEmailsSent - 1)];
			entity.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireColumn.SupplierContactVerifiedOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.SupplierContactVerifiedOn - 1)];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireColumn.QuestionnairePresentlyWithActionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireColumn.QuestionnairePresentlyWithActionId - 1)];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireColumn.QuestionnairePresentlyWithSince - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireColumn.QuestionnairePresentlyWithSince - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Questionnaire"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Questionnaire"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Questionnaire entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.Status = (System.Int32)dataRow["Status"];
			entity.IsReQualification = (System.Boolean)dataRow["IsReQualification"];
			entity.IsMainRequired = (System.Boolean)dataRow["IsMainRequired"];
			entity.IsVerificationRequired = (System.Boolean)dataRow["IsVerificationRequired"];
			entity.InitialCategoryHigh = Convert.IsDBNull(dataRow["InitialCategoryHigh"]) ? null : (System.Boolean?)dataRow["InitialCategoryHigh"];
			entity.InitialCreatedByUserId = Convert.IsDBNull(dataRow["InitialCreatedByUserId"]) ? null : (System.Int32?)dataRow["InitialCreatedByUserId"];
			entity.InitialCreatedDate = Convert.IsDBNull(dataRow["InitialCreatedDate"]) ? null : (System.DateTime?)dataRow["InitialCreatedDate"];
			entity.InitialSubmittedByUserId = Convert.IsDBNull(dataRow["InitialSubmittedByUserId"]) ? null : (System.Int32?)dataRow["InitialSubmittedByUserId"];
			entity.InitialSubmittedDate = Convert.IsDBNull(dataRow["InitialSubmittedDate"]) ? null : (System.DateTime?)dataRow["InitialSubmittedDate"];
			entity.InitialModifiedByUserId = Convert.IsDBNull(dataRow["InitialModifiedByUserId"]) ? null : (System.Int32?)dataRow["InitialModifiedByUserId"];
			entity.InitialModifiedDate = Convert.IsDBNull(dataRow["InitialModifiedDate"]) ? null : (System.DateTime?)dataRow["InitialModifiedDate"];
			entity.InitialStatus = (System.Int32)dataRow["InitialStatus"];
			entity.InitialRiskAssessment = Convert.IsDBNull(dataRow["InitialRiskAssessment"]) ? null : (System.String)dataRow["InitialRiskAssessment"];
			entity.MainCreatedByUserId = Convert.IsDBNull(dataRow["MainCreatedByUserId"]) ? null : (System.Int32?)dataRow["MainCreatedByUserId"];
			entity.MainCreatedDate = Convert.IsDBNull(dataRow["MainCreatedDate"]) ? null : (System.DateTime?)dataRow["MainCreatedDate"];
			entity.MainModifiedByUserId = Convert.IsDBNull(dataRow["MainModifiedByUserId"]) ? null : (System.Int32?)dataRow["MainModifiedByUserId"];
			entity.MainModifiedDate = Convert.IsDBNull(dataRow["MainModifiedDate"]) ? null : (System.DateTime?)dataRow["MainModifiedDate"];
			entity.MainScoreExpectations = Convert.IsDBNull(dataRow["MainScoreExpectations"]) ? null : (System.Int32?)dataRow["MainScoreExpectations"];
			entity.MainScoreOverall = Convert.IsDBNull(dataRow["MainScoreOverall"]) ? null : (System.Int32?)dataRow["MainScoreOverall"];
			entity.MainScoreResults = Convert.IsDBNull(dataRow["MainScoreResults"]) ? null : (System.Int32?)dataRow["MainScoreResults"];
			entity.MainScoreStaffing = Convert.IsDBNull(dataRow["MainScoreStaffing"]) ? null : (System.Int32?)dataRow["MainScoreStaffing"];
			entity.MainScoreSystems = Convert.IsDBNull(dataRow["MainScoreSystems"]) ? null : (System.Int32?)dataRow["MainScoreSystems"];
			entity.MainStatus = (System.Int32)dataRow["MainStatus"];
			entity.MainAssessmentByUserId = Convert.IsDBNull(dataRow["MainAssessmentByUserId"]) ? null : (System.Int32?)dataRow["MainAssessmentByUserId"];
			entity.MainAssessmentComments = Convert.IsDBNull(dataRow["MainAssessmentComments"]) ? null : (System.String)dataRow["MainAssessmentComments"];
			entity.MainAssessmentDate = Convert.IsDBNull(dataRow["MainAssessmentDate"]) ? null : (System.DateTime?)dataRow["MainAssessmentDate"];
			entity.MainAssessmentRiskRating = Convert.IsDBNull(dataRow["MainAssessmentRiskRating"]) ? null : (System.String)dataRow["MainAssessmentRiskRating"];
			entity.MainAssessmentStatus = Convert.IsDBNull(dataRow["MainAssessmentStatus"]) ? null : (System.String)dataRow["MainAssessmentStatus"];
			entity.MainAssessmentValidTo = Convert.IsDBNull(dataRow["MainAssessmentValidTo"]) ? null : (System.DateTime?)dataRow["MainAssessmentValidTo"];
			entity.MainScorePexpectations = Convert.IsDBNull(dataRow["MainScorePExpectations"]) ? null : (System.Int32?)dataRow["MainScorePExpectations"];
			entity.MainScorePoverall = Convert.IsDBNull(dataRow["MainScorePOverall"]) ? null : (System.Int32?)dataRow["MainScorePOverall"];
			entity.MainScorePresults = Convert.IsDBNull(dataRow["MainScorePResults"]) ? null : (System.Int32?)dataRow["MainScorePResults"];
			entity.MainScorePstaffing = Convert.IsDBNull(dataRow["MainScorePStaffing"]) ? null : (System.Int32?)dataRow["MainScorePStaffing"];
			entity.MainScorePsystems = Convert.IsDBNull(dataRow["MainScorePSystems"]) ? null : (System.Int32?)dataRow["MainScorePSystems"];
			entity.VerificationCreatedByUserId = Convert.IsDBNull(dataRow["VerificationCreatedByUserId"]) ? null : (System.Int32?)dataRow["VerificationCreatedByUserId"];
			entity.VerificationCreatedDate = Convert.IsDBNull(dataRow["VerificationCreatedDate"]) ? null : (System.DateTime?)dataRow["VerificationCreatedDate"];
			entity.VerificationModifiedByUserId = Convert.IsDBNull(dataRow["VerificationModifiedByUserId"]) ? null : (System.Int32?)dataRow["VerificationModifiedByUserId"];
			entity.VerificationModifiedDate = Convert.IsDBNull(dataRow["VerificationModifiedDate"]) ? null : (System.DateTime?)dataRow["VerificationModifiedDate"];
			entity.VerificationRiskRating = Convert.IsDBNull(dataRow["VerificationRiskRating"]) ? null : (System.String)dataRow["VerificationRiskRating"];
			entity.VerificationStatus = (System.Int32)dataRow["VerificationStatus"];
			entity.ApprovedByUserId = Convert.IsDBNull(dataRow["ApprovedByUserId"]) ? null : (System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = Convert.IsDBNull(dataRow["ApprovedDate"]) ? null : (System.DateTime?)dataRow["ApprovedDate"];
			entity.Recommended = Convert.IsDBNull(dataRow["Recommended"]) ? null : (System.Boolean?)dataRow["Recommended"];
			entity.RecommendedComments = Convert.IsDBNull(dataRow["RecommendedComments"]) ? null : (System.String)dataRow["RecommendedComments"];
			entity.LevelOfSupervision = Convert.IsDBNull(dataRow["LevelOfSupervision"]) ? null : (System.String)dataRow["LevelOfSupervision"];
			entity.ProcurementModified = (System.Boolean)dataRow["ProcurementModified"];
			entity.SubContractor = Convert.IsDBNull(dataRow["SubContractor"]) ? null : (System.Boolean?)dataRow["SubContractor"];
			entity.AssessedByUserId = Convert.IsDBNull(dataRow["AssessedByUserId"]) ? null : (System.Int32?)dataRow["AssessedByUserId"];
			entity.AssessedDate = Convert.IsDBNull(dataRow["AssessedDate"]) ? null : (System.DateTime?)dataRow["AssessedDate"];
			entity.LastReminderEmailSentOn = Convert.IsDBNull(dataRow["LastReminderEmailSentOn"]) ? null : (System.DateTime?)dataRow["LastReminderEmailSentOn"];
			entity.NoReminderEmailsSent = Convert.IsDBNull(dataRow["NoReminderEmailsSent"]) ? null : (System.Int32?)dataRow["NoReminderEmailsSent"];
			entity.SupplierContactVerifiedOn = Convert.IsDBNull(dataRow["SupplierContactVerifiedOn"]) ? null : (System.DateTime?)dataRow["SupplierContactVerifiedOn"];
			entity.QuestionnairePresentlyWithActionId = Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]) ? null : (System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithSince = Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]) ? null : (System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Questionnaire"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Questionnaire Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Questionnaire entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region MainAssessmentByUserIdSource	
			if (CanDeepLoad(entity, "Users|MainAssessmentByUserIdSource", deepLoadType, innerList) 
				&& entity.MainAssessmentByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MainAssessmentByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MainAssessmentByUserIdSource = tmpEntity;
				else
					entity.MainAssessmentByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.MainAssessmentByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MainAssessmentByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MainAssessmentByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.MainAssessmentByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MainAssessmentByUserIdSource

			#region InitialModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|InitialModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.InitialModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.InitialModifiedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.InitialModifiedByUserIdSource = tmpEntity;
				else
					entity.InitialModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.InitialModifiedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'InitialModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.InitialModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.InitialModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion InitialModifiedByUserIdSource

			#region MainModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|MainModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.MainModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MainModifiedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MainModifiedByUserIdSource = tmpEntity;
				else
					entity.MainModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.MainModifiedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MainModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MainModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.MainModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MainModifiedByUserIdSource

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region StatusSource	
			if (CanDeepLoad(entity, "QuestionnaireStatus|StatusSource", deepLoadType, innerList) 
				&& entity.StatusSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.Status;
				QuestionnaireStatus tmpEntity = EntityManager.LocateEntity<QuestionnaireStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.StatusSource = tmpEntity;
				else
					entity.StatusSource = DataRepository.QuestionnaireStatusProvider.GetByQuestionnaireStatusId(transactionManager, entity.Status);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StatusSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.StatusSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireStatusProvider.DeepLoad(transactionManager, entity.StatusSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion StatusSource

			#region InitialStatusSource	
			if (CanDeepLoad(entity, "QuestionnaireStatus|InitialStatusSource", deepLoadType, innerList) 
				&& entity.InitialStatusSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.InitialStatus;
				QuestionnaireStatus tmpEntity = EntityManager.LocateEntity<QuestionnaireStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.InitialStatusSource = tmpEntity;
				else
					entity.InitialStatusSource = DataRepository.QuestionnaireStatusProvider.GetByQuestionnaireStatusId(transactionManager, entity.InitialStatus);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'InitialStatusSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.InitialStatusSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireStatusProvider.DeepLoad(transactionManager, entity.InitialStatusSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion InitialStatusSource

			#region MainStatusSource	
			if (CanDeepLoad(entity, "QuestionnaireStatus|MainStatusSource", deepLoadType, innerList) 
				&& entity.MainStatusSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MainStatus;
				QuestionnaireStatus tmpEntity = EntityManager.LocateEntity<QuestionnaireStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MainStatusSource = tmpEntity;
				else
					entity.MainStatusSource = DataRepository.QuestionnaireStatusProvider.GetByQuestionnaireStatusId(transactionManager, entity.MainStatus);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MainStatusSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MainStatusSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireStatusProvider.DeepLoad(transactionManager, entity.MainStatusSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MainStatusSource

			#region VerificationStatusSource	
			if (CanDeepLoad(entity, "QuestionnaireStatus|VerificationStatusSource", deepLoadType, innerList) 
				&& entity.VerificationStatusSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.VerificationStatus;
				QuestionnaireStatus tmpEntity = EntityManager.LocateEntity<QuestionnaireStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.VerificationStatusSource = tmpEntity;
				else
					entity.VerificationStatusSource = DataRepository.QuestionnaireStatusProvider.GetByQuestionnaireStatusId(transactionManager, entity.VerificationStatus);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'VerificationStatusSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.VerificationStatusSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireStatusProvider.DeepLoad(transactionManager, entity.VerificationStatusSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion VerificationStatusSource

			#region ApprovedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ApprovedByUserIdSource", deepLoadType, innerList) 
				&& entity.ApprovedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ApprovedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ApprovedByUserIdSource = tmpEntity;
				else
					entity.ApprovedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ApprovedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ApprovedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ApprovedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ApprovedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ApprovedByUserIdSource

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region VerificationModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|VerificationModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.VerificationModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.VerificationModifiedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.VerificationModifiedByUserIdSource = tmpEntity;
				else
					entity.VerificationModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.VerificationModifiedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'VerificationModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.VerificationModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.VerificationModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion VerificationModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireId methods when available
			
			#region QuestionnaireActionLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireActionLog>|QuestionnaireActionLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireActionLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireActionLogCollection = DataRepository.QuestionnaireActionLogProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireActionLogCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireActionLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireActionLog>) DataRepository.QuestionnaireActionLogProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireActionLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireVerificationAssessmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireVerificationAssessment>|QuestionnaireVerificationAssessmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationAssessmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireVerificationAssessmentCollection = DataRepository.QuestionnaireVerificationAssessmentProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireVerificationAssessmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireVerificationAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireVerificationAssessment>) DataRepository.QuestionnaireVerificationAssessmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationAssessmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialContactEmailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactEmailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactEmailCollection = DataRepository.QuestionnaireInitialContactEmailProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireInitialContactEmailCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContactEmail>) DataRepository.QuestionnaireInitialContactEmailProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireVerificationAttachmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireVerificationAttachment>|QuestionnaireVerificationAttachmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationAttachmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireVerificationAttachmentCollection = DataRepository.QuestionnaireVerificationAttachmentProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireVerificationAttachmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireVerificationAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireVerificationAttachment>) DataRepository.QuestionnaireVerificationAttachmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationAttachmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireMainAssessmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainAssessment>|QuestionnaireMainAssessmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainAssessmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainAssessmentCollection = DataRepository.QuestionnaireMainAssessmentProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireMainAssessmentCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainAssessment>) DataRepository.QuestionnaireMainAssessmentProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainAssessmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireMainResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireMainResponse>|QuestionnaireMainResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireMainResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireMainResponseCollection = DataRepository.QuestionnaireMainResponseProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireMainResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireMainResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireMainResponse>) DataRepository.QuestionnaireMainResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireMainResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialLocationCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialLocationCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialLocationCollection = DataRepository.QuestionnaireInitialLocationProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireInitialLocationCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialLocationCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialLocation>) DataRepository.QuestionnaireInitialLocationProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialLocationCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region Questionnaire
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "Questionnaire|Questionnaire", deepLoadType, innerList))
			{
				entity.Questionnaire = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Questionnaire' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.Questionnaire != null)
				{
					deepHandles.Add("Questionnaire",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepLoad,
						new object[] { transactionManager, entity.Questionnaire, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region QuestionnaireVerificationResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireVerificationResponse>|QuestionnaireVerificationResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireVerificationResponseCollection = DataRepository.QuestionnaireVerificationResponseProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireVerificationResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireVerificationResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireVerificationResponse>) DataRepository.QuestionnaireVerificationResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialResponseCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialResponse>|QuestionnaireInitialResponseCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialResponseCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialResponseCollection = DataRepository.QuestionnaireInitialResponseProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireInitialResponseCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialResponseCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialResponse>) DataRepository.QuestionnaireInitialResponseProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialResponseCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireCommentsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireComments>|QuestionnaireCommentsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireCommentsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireCommentsCollection = DataRepository.QuestionnaireCommentsProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireCommentsCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireCommentsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireComments>) DataRepository.QuestionnaireCommentsProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireCommentsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireServicesSelectedCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireServicesSelected>|QuestionnaireServicesSelectedCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireServicesSelectedCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireServicesSelectedCollection = DataRepository.QuestionnaireServicesSelectedProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.QuestionnaireServicesSelectedCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireServicesSelectedCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireServicesSelected>) DataRepository.QuestionnaireServicesSelectedProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireServicesSelectedCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanyStatusChangeApprovalCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusChangeApprovalCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanyStatusChangeApprovalCollection = DataRepository.CompanyStatusChangeApprovalProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);

				if (deep && entity.CompanyStatusChangeApprovalCollection.Count > 0)
				{
					deepHandles.Add("CompanyStatusChangeApprovalCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanyStatusChangeApproval>) DataRepository.CompanyStatusChangeApprovalProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireVerificationSection
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "QuestionnaireVerificationSection|QuestionnaireVerificationSection", deepLoadType, innerList))
			{
				entity.QuestionnaireVerificationSection = DataRepository.QuestionnaireVerificationSectionProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireVerificationSection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.QuestionnaireVerificationSection != null)
				{
					deepHandles.Add("QuestionnaireVerificationSection",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< QuestionnaireVerificationSection >) DataRepository.QuestionnaireVerificationSectionProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireVerificationSection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region QuestionnaireInitialContact
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "QuestionnaireInitialContact|QuestionnaireInitialContact", deepLoadType, innerList))
			{
				entity.QuestionnaireInitialContact = DataRepository.QuestionnaireInitialContactProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContact' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.QuestionnaireInitialContact != null)
				{
					deepHandles.Add("QuestionnaireInitialContact",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< QuestionnaireInitialContact >) DataRepository.QuestionnaireInitialContactProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContact, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Questionnaire object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Questionnaire instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Questionnaire Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Questionnaire entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region MainAssessmentByUserIdSource
			if (CanDeepSave(entity, "Users|MainAssessmentByUserIdSource", deepSaveType, innerList) 
				&& entity.MainAssessmentByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.MainAssessmentByUserIdSource);
				entity.MainAssessmentByUserId = entity.MainAssessmentByUserIdSource.UserId;
			}
			#endregion 
			
			#region InitialModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|InitialModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.InitialModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.InitialModifiedByUserIdSource);
				entity.InitialModifiedByUserId = entity.InitialModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region MainModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|MainModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.MainModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.MainModifiedByUserIdSource);
				entity.MainModifiedByUserId = entity.MainModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region StatusSource
			if (CanDeepSave(entity, "QuestionnaireStatus|StatusSource", deepSaveType, innerList) 
				&& entity.StatusSource != null)
			{
				DataRepository.QuestionnaireStatusProvider.Save(transactionManager, entity.StatusSource);
				entity.Status = entity.StatusSource.QuestionnaireStatusId;
			}
			#endregion 
			
			#region InitialStatusSource
			if (CanDeepSave(entity, "QuestionnaireStatus|InitialStatusSource", deepSaveType, innerList) 
				&& entity.InitialStatusSource != null)
			{
				DataRepository.QuestionnaireStatusProvider.Save(transactionManager, entity.InitialStatusSource);
				entity.InitialStatus = entity.InitialStatusSource.QuestionnaireStatusId;
			}
			#endregion 
			
			#region MainStatusSource
			if (CanDeepSave(entity, "QuestionnaireStatus|MainStatusSource", deepSaveType, innerList) 
				&& entity.MainStatusSource != null)
			{
				DataRepository.QuestionnaireStatusProvider.Save(transactionManager, entity.MainStatusSource);
				entity.MainStatus = entity.MainStatusSource.QuestionnaireStatusId;
			}
			#endregion 
			
			#region VerificationStatusSource
			if (CanDeepSave(entity, "QuestionnaireStatus|VerificationStatusSource", deepSaveType, innerList) 
				&& entity.VerificationStatusSource != null)
			{
				DataRepository.QuestionnaireStatusProvider.Save(transactionManager, entity.VerificationStatusSource);
				entity.VerificationStatus = entity.VerificationStatusSource.QuestionnaireStatusId;
			}
			#endregion 
			
			#region ApprovedByUserIdSource
			if (CanDeepSave(entity, "Users|ApprovedByUserIdSource", deepSaveType, innerList) 
				&& entity.ApprovedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ApprovedByUserIdSource);
				entity.ApprovedByUserId = entity.ApprovedByUserIdSource.UserId;
			}
			#endregion 
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region VerificationModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|VerificationModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.VerificationModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.VerificationModifiedByUserIdSource);
				entity.VerificationModifiedByUserId = entity.VerificationModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region Questionnaire
			if (CanDeepSave(entity.Questionnaire, "Questionnaire|Questionnaire", deepSaveType, innerList))
			{

				if (entity.Questionnaire != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.Questionnaire.QuestionnaireId = entity.QuestionnaireId;
					//DataRepository.QuestionnaireProvider.Save(transactionManager, entity.Questionnaire);
					deepHandles.Add("Questionnaire",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< Questionnaire >) DataRepository.QuestionnaireProvider.DeepSave,
						new object[] { transactionManager, entity.Questionnaire, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 

			#region QuestionnaireVerificationSection
			if (CanDeepSave(entity.QuestionnaireVerificationSection, "QuestionnaireVerificationSection|QuestionnaireVerificationSection", deepSaveType, innerList))
			{

				if (entity.QuestionnaireVerificationSection != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.QuestionnaireVerificationSection.QuestionnaireId = entity.QuestionnaireId;
					//DataRepository.QuestionnaireVerificationSectionProvider.Save(transactionManager, entity.QuestionnaireVerificationSection);
					deepHandles.Add("QuestionnaireVerificationSection",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< QuestionnaireVerificationSection >) DataRepository.QuestionnaireVerificationSectionProvider.DeepSave,
						new object[] { transactionManager, entity.QuestionnaireVerificationSection, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 

			#region QuestionnaireInitialContact
			if (CanDeepSave(entity.QuestionnaireInitialContact, "QuestionnaireInitialContact|QuestionnaireInitialContact", deepSaveType, innerList))
			{

				if (entity.QuestionnaireInitialContact != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.QuestionnaireInitialContact.QuestionnaireId = entity.QuestionnaireId;
					//DataRepository.QuestionnaireInitialContactProvider.Save(transactionManager, entity.QuestionnaireInitialContact);
					deepHandles.Add("QuestionnaireInitialContact",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< QuestionnaireInitialContact >) DataRepository.QuestionnaireInitialContactProvider.DeepSave,
						new object[] { transactionManager, entity.QuestionnaireInitialContact, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<QuestionnaireActionLog>
				if (CanDeepSave(entity.QuestionnaireActionLogCollection, "List<QuestionnaireActionLog>|QuestionnaireActionLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireActionLog child in entity.QuestionnaireActionLogCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireActionLogCollection.Count > 0 || entity.QuestionnaireActionLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireActionLogProvider.Save(transactionManager, entity.QuestionnaireActionLogCollection);
						
						deepHandles.Add("QuestionnaireActionLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireActionLog >) DataRepository.QuestionnaireActionLogProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireActionLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireVerificationAssessment>
				if (CanDeepSave(entity.QuestionnaireVerificationAssessmentCollection, "List<QuestionnaireVerificationAssessment>|QuestionnaireVerificationAssessmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireVerificationAssessment child in entity.QuestionnaireVerificationAssessmentCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireVerificationAssessmentCollection.Count > 0 || entity.QuestionnaireVerificationAssessmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireVerificationAssessmentProvider.Save(transactionManager, entity.QuestionnaireVerificationAssessmentCollection);
						
						deepHandles.Add("QuestionnaireVerificationAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireVerificationAssessment >) DataRepository.QuestionnaireVerificationAssessmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireVerificationAssessmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialContactEmail>
				if (CanDeepSave(entity.QuestionnaireInitialContactEmailCollection, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContactEmail child in entity.QuestionnaireInitialContactEmailCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireInitialContactEmailCollection.Count > 0 || entity.QuestionnaireInitialContactEmailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactEmailProvider.Save(transactionManager, entity.QuestionnaireInitialContactEmailCollection);
						
						deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContactEmail >) DataRepository.QuestionnaireInitialContactEmailProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireVerificationAttachment>
				if (CanDeepSave(entity.QuestionnaireVerificationAttachmentCollection, "List<QuestionnaireVerificationAttachment>|QuestionnaireVerificationAttachmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireVerificationAttachment child in entity.QuestionnaireVerificationAttachmentCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireVerificationAttachmentCollection.Count > 0 || entity.QuestionnaireVerificationAttachmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireVerificationAttachmentProvider.Save(transactionManager, entity.QuestionnaireVerificationAttachmentCollection);
						
						deepHandles.Add("QuestionnaireVerificationAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireVerificationAttachment >) DataRepository.QuestionnaireVerificationAttachmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireVerificationAttachmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireMainAssessment>
				if (CanDeepSave(entity.QuestionnaireMainAssessmentCollection, "List<QuestionnaireMainAssessment>|QuestionnaireMainAssessmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainAssessment child in entity.QuestionnaireMainAssessmentCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireMainAssessmentCollection.Count > 0 || entity.QuestionnaireMainAssessmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainAssessmentProvider.Save(transactionManager, entity.QuestionnaireMainAssessmentCollection);
						
						deepHandles.Add("QuestionnaireMainAssessmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainAssessment >) DataRepository.QuestionnaireMainAssessmentProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainAssessmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireMainResponse>
				if (CanDeepSave(entity.QuestionnaireMainResponseCollection, "List<QuestionnaireMainResponse>|QuestionnaireMainResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireMainResponse child in entity.QuestionnaireMainResponseCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireMainResponseCollection.Count > 0 || entity.QuestionnaireMainResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireMainResponseProvider.Save(transactionManager, entity.QuestionnaireMainResponseCollection);
						
						deepHandles.Add("QuestionnaireMainResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireMainResponse >) DataRepository.QuestionnaireMainResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireMainResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialLocation>
				if (CanDeepSave(entity.QuestionnaireInitialLocationCollection, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialLocation child in entity.QuestionnaireInitialLocationCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireInitialLocationCollection.Count > 0 || entity.QuestionnaireInitialLocationCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialLocationProvider.Save(transactionManager, entity.QuestionnaireInitialLocationCollection);
						
						deepHandles.Add("QuestionnaireInitialLocationCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialLocation >) DataRepository.QuestionnaireInitialLocationProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialLocationCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireVerificationResponse>
				if (CanDeepSave(entity.QuestionnaireVerificationResponseCollection, "List<QuestionnaireVerificationResponse>|QuestionnaireVerificationResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireVerificationResponse child in entity.QuestionnaireVerificationResponseCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireVerificationResponseCollection.Count > 0 || entity.QuestionnaireVerificationResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireVerificationResponseProvider.Save(transactionManager, entity.QuestionnaireVerificationResponseCollection);
						
						deepHandles.Add("QuestionnaireVerificationResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireVerificationResponse >) DataRepository.QuestionnaireVerificationResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireVerificationResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialResponse>
				if (CanDeepSave(entity.QuestionnaireInitialResponseCollection, "List<QuestionnaireInitialResponse>|QuestionnaireInitialResponseCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialResponse child in entity.QuestionnaireInitialResponseCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireInitialResponseCollection.Count > 0 || entity.QuestionnaireInitialResponseCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialResponseProvider.Save(transactionManager, entity.QuestionnaireInitialResponseCollection);
						
						deepHandles.Add("QuestionnaireInitialResponseCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialResponse >) DataRepository.QuestionnaireInitialResponseProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialResponseCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireComments>
				if (CanDeepSave(entity.QuestionnaireCommentsCollection, "List<QuestionnaireComments>|QuestionnaireCommentsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireComments child in entity.QuestionnaireCommentsCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireCommentsCollection.Count > 0 || entity.QuestionnaireCommentsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireCommentsProvider.Save(transactionManager, entity.QuestionnaireCommentsCollection);
						
						deepHandles.Add("QuestionnaireCommentsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireComments >) DataRepository.QuestionnaireCommentsProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireCommentsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireServicesSelected>
				if (CanDeepSave(entity.QuestionnaireServicesSelectedCollection, "List<QuestionnaireServicesSelected>|QuestionnaireServicesSelectedCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireServicesSelected child in entity.QuestionnaireServicesSelectedCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.QuestionnaireServicesSelectedCollection.Count > 0 || entity.QuestionnaireServicesSelectedCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireServicesSelectedProvider.Save(transactionManager, entity.QuestionnaireServicesSelectedCollection);
						
						deepHandles.Add("QuestionnaireServicesSelectedCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireServicesSelected >) DataRepository.QuestionnaireServicesSelectedProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireServicesSelectedCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanyStatusChangeApproval>
				if (CanDeepSave(entity.CompanyStatusChangeApprovalCollection, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanyStatusChangeApproval child in entity.CompanyStatusChangeApprovalCollection)
					{
						if(child.QuestionnaireIdSource != null)
						{
							child.QuestionnaireId = child.QuestionnaireIdSource.QuestionnaireId;
						}
						else
						{
							child.QuestionnaireId = entity.QuestionnaireId;
						}

					}

					if (entity.CompanyStatusChangeApprovalCollection.Count > 0 || entity.CompanyStatusChangeApprovalCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanyStatusChangeApprovalProvider.Save(transactionManager, entity.CompanyStatusChangeApprovalCollection);
						
						deepHandles.Add("CompanyStatusChangeApprovalCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanyStatusChangeApproval >) DataRepository.CompanyStatusChangeApprovalProvider.DeepSave,
							new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Questionnaire</c>
	///</summary>
	public enum QuestionnaireChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at MainAssessmentByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireStatus</c> at StatusSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireStatus))]
		QuestionnaireStatus,
	
		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireActionLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireActionLog>))]
		QuestionnaireActionLogCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireVerificationAssessmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireVerificationAssessment>))]
		QuestionnaireVerificationAssessmentCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireInitialContactEmailCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContactEmail>))]
		QuestionnaireInitialContactEmailCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireVerificationAttachmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireVerificationAttachment>))]
		QuestionnaireVerificationAttachmentCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireMainAssessmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainAssessment>))]
		QuestionnaireMainAssessmentCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireMainResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireMainResponse>))]
		QuestionnaireMainResponseCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireInitialLocationCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialLocation>))]
		QuestionnaireInitialLocationCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireVerificationResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireVerificationResponse>))]
		QuestionnaireVerificationResponseCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireInitialResponseCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialResponse>))]
		QuestionnaireInitialResponseCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireCommentsCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireComments>))]
		QuestionnaireCommentsCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for QuestionnaireServicesSelectedCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireServicesSelected>))]
		QuestionnaireServicesSelectedCollection,

		///<summary>
		/// Collection of <c>Questionnaire</c> as OneToMany for CompanyStatusChangeApprovalCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanyStatusChangeApproval>))]
		CompanyStatusChangeApprovalCollection,
		///<summary>
		/// Entity <c>QuestionnaireVerificationSection</c> as OneToOne for QuestionnaireVerificationSection
		///</summary>
		[ChildEntityType(typeof(QuestionnaireVerificationSection))]
		QuestionnaireVerificationSection,
		///<summary>
		/// Entity <c>QuestionnaireInitialContact</c> as OneToOne for QuestionnaireInitialContact
		///</summary>
		[ChildEntityType(typeof(QuestionnaireInitialContact))]
		QuestionnaireInitialContact,
	}
	
	#endregion QuestionnaireChildEntityTypes
	
	#region QuestionnaireFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireFilterBuilder : SqlFilterBuilder<QuestionnaireColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireFilterBuilder class.
		/// </summary>
		public QuestionnaireFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireFilterBuilder
	
	#region QuestionnaireParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireParameterBuilder class.
		/// </summary>
		public QuestionnaireParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireParameterBuilder
	
	#region QuestionnaireSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Questionnaire"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireSortBuilder : SqlSortBuilder<QuestionnaireColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireSqlSortBuilder class.
		/// </summary>
		public QuestionnaireSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireSortBuilder
	






} // end namespace
