﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="KpiPurchaseOrderListOpenProjectsDistinctAscProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class KpiPurchaseOrderListOpenProjectsDistinctAscProviderBaseCore : EntityViewProviderBase<KpiPurchaseOrderListOpenProjectsDistinctAsc>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt;"/></returns>
		protected static VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt; Fill(DataSet dataSet, VList<KpiPurchaseOrderListOpenProjectsDistinctAsc> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<KpiPurchaseOrderListOpenProjectsDistinctAsc>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<KpiPurchaseOrderListOpenProjectsDistinctAsc>"/></returns>
		protected static VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt; Fill(DataTable dataTable, VList<KpiPurchaseOrderListOpenProjectsDistinctAsc> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					KpiPurchaseOrderListOpenProjectsDistinctAsc c = new KpiPurchaseOrderListOpenProjectsDistinctAsc();
					c.ProjectNumber = (Convert.IsDBNull(row["ProjectNumber"]))?string.Empty:(System.String)row["ProjectNumber"];
					c.ProjectDesc = (Convert.IsDBNull(row["ProjectDesc"]))?string.Empty:(System.String)row["ProjectDesc"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;KpiPurchaseOrderListOpenProjectsDistinctAsc&gt;"/></returns>
		protected VList<KpiPurchaseOrderListOpenProjectsDistinctAsc> Fill(IDataReader reader, VList<KpiPurchaseOrderListOpenProjectsDistinctAsc> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					KpiPurchaseOrderListOpenProjectsDistinctAsc entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<KpiPurchaseOrderListOpenProjectsDistinctAsc>("KpiPurchaseOrderListOpenProjectsDistinctAsc",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new KpiPurchaseOrderListOpenProjectsDistinctAsc();
					}
					
					entity.SuppressEntityEvents = true;

					entity.ProjectNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsDistinctAscColumn.ProjectNumber)];
					//entity.ProjectNumber = (Convert.IsDBNull(reader["ProjectNumber"]))?string.Empty:(System.String)reader["ProjectNumber"];
					entity.ProjectDesc = (reader.IsDBNull(((int)KpiPurchaseOrderListOpenProjectsDistinctAscColumn.ProjectDesc)))?null:(System.String)reader[((int)KpiPurchaseOrderListOpenProjectsDistinctAscColumn.ProjectDesc)];
					//entity.ProjectDesc = (Convert.IsDBNull(reader["ProjectDesc"]))?string.Empty:(System.String)reader["ProjectDesc"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, KpiPurchaseOrderListOpenProjectsDistinctAsc entity)
		{
			reader.Read();
			entity.ProjectNumber = (System.String)reader[((int)KpiPurchaseOrderListOpenProjectsDistinctAscColumn.ProjectNumber)];
			//entity.ProjectNumber = (Convert.IsDBNull(reader["ProjectNumber"]))?string.Empty:(System.String)reader["ProjectNumber"];
			entity.ProjectDesc = (reader.IsDBNull(((int)KpiPurchaseOrderListOpenProjectsDistinctAscColumn.ProjectDesc)))?null:(System.String)reader[((int)KpiPurchaseOrderListOpenProjectsDistinctAscColumn.ProjectDesc)];
			//entity.ProjectDesc = (Convert.IsDBNull(reader["ProjectDesc"]))?string.Empty:(System.String)reader["ProjectDesc"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, KpiPurchaseOrderListOpenProjectsDistinctAsc entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProjectNumber = (Convert.IsDBNull(dataRow["ProjectNumber"]))?string.Empty:(System.String)dataRow["ProjectNumber"];
			entity.ProjectDesc = (Convert.IsDBNull(dataRow["ProjectDesc"]))?string.Empty:(System.String)dataRow["ProjectDesc"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder : SqlFilterBuilder<KpiPurchaseOrderListOpenProjectsDistinctAscColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscFilterBuilder

	#region KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder : ParameterizedSqlFilterBuilder<KpiPurchaseOrderListOpenProjectsDistinctAscColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscParameterBuilder
	
	#region KpiPurchaseOrderListOpenProjectsDistinctAscSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class KpiPurchaseOrderListOpenProjectsDistinctAscSortBuilder : SqlSortBuilder<KpiPurchaseOrderListOpenProjectsDistinctAscColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscSqlSortBuilder class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAscSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion KpiPurchaseOrderListOpenProjectsDistinctAscSortBuilder

} // end namespace
