﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit04Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit04RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit04RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit04RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit04Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit04Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit04Repeater z = (TwentyOnePointAudit04Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit04Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit04RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit04Repeater runat=\"server\"></{0}:TwentyOnePointAudit04Repeater>")]
	public class TwentyOnePointAudit04Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit04Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit04Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit04Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit04Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit04Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit04Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit04Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit04 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit04;
						TwentyOnePointAudit04Item container = new TwentyOnePointAudit04Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit04Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit04 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit04Item"/> class.
        /// </summary>
		public TwentyOnePointAudit04Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit04Item"/> class.
        /// </summary>
		public TwentyOnePointAudit04Item(KaiZen.CSMS.Entities.TwentyOnePointAudit04 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved4a
        /// </summary>
        /// <value>The Achieved4a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4a
		{
			get { return _entity.Achieved4a; }
		}
        /// <summary>
        /// Gets the Achieved4b
        /// </summary>
        /// <value>The Achieved4b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4b
		{
			get { return _entity.Achieved4b; }
		}
        /// <summary>
        /// Gets the Achieved4c
        /// </summary>
        /// <value>The Achieved4c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4c
		{
			get { return _entity.Achieved4c; }
		}
        /// <summary>
        /// Gets the Achieved4d
        /// </summary>
        /// <value>The Achieved4d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4d
		{
			get { return _entity.Achieved4d; }
		}
        /// <summary>
        /// Gets the Achieved4e
        /// </summary>
        /// <value>The Achieved4e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4e
		{
			get { return _entity.Achieved4e; }
		}
        /// <summary>
        /// Gets the Achieved4f
        /// </summary>
        /// <value>The Achieved4f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4f
		{
			get { return _entity.Achieved4f; }
		}
        /// <summary>
        /// Gets the Achieved4g
        /// </summary>
        /// <value>The Achieved4g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4g
		{
			get { return _entity.Achieved4g; }
		}
        /// <summary>
        /// Gets the Achieved4h
        /// </summary>
        /// <value>The Achieved4h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4h
		{
			get { return _entity.Achieved4h; }
		}
        /// <summary>
        /// Gets the Achieved4i
        /// </summary>
        /// <value>The Achieved4i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4i
		{
			get { return _entity.Achieved4i; }
		}
        /// <summary>
        /// Gets the Achieved4j
        /// </summary>
        /// <value>The Achieved4j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4j
		{
			get { return _entity.Achieved4j; }
		}
        /// <summary>
        /// Gets the Achieved4k
        /// </summary>
        /// <value>The Achieved4k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4k
		{
			get { return _entity.Achieved4k; }
		}
        /// <summary>
        /// Gets the Achieved4l
        /// </summary>
        /// <value>The Achieved4l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved4l
		{
			get { return _entity.Achieved4l; }
		}
        /// <summary>
        /// Gets the Observation4a
        /// </summary>
        /// <value>The Observation4a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4a
		{
			get { return _entity.Observation4a; }
		}
        /// <summary>
        /// Gets the Observation4b
        /// </summary>
        /// <value>The Observation4b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4b
		{
			get { return _entity.Observation4b; }
		}
        /// <summary>
        /// Gets the Observation4c
        /// </summary>
        /// <value>The Observation4c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4c
		{
			get { return _entity.Observation4c; }
		}
        /// <summary>
        /// Gets the Observation4d
        /// </summary>
        /// <value>The Observation4d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4d
		{
			get { return _entity.Observation4d; }
		}
        /// <summary>
        /// Gets the Observation4e
        /// </summary>
        /// <value>The Observation4e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4e
		{
			get { return _entity.Observation4e; }
		}
        /// <summary>
        /// Gets the Observation4f
        /// </summary>
        /// <value>The Observation4f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4f
		{
			get { return _entity.Observation4f; }
		}
        /// <summary>
        /// Gets the Observation4g
        /// </summary>
        /// <value>The Observation4g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4g
		{
			get { return _entity.Observation4g; }
		}
        /// <summary>
        /// Gets the Observation4h
        /// </summary>
        /// <value>The Observation4h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4h
		{
			get { return _entity.Observation4h; }
		}
        /// <summary>
        /// Gets the Observation4i
        /// </summary>
        /// <value>The Observation4i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4i
		{
			get { return _entity.Observation4i; }
		}
        /// <summary>
        /// Gets the Observation4j
        /// </summary>
        /// <value>The Observation4j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4j
		{
			get { return _entity.Observation4j; }
		}
        /// <summary>
        /// Gets the Observation4k
        /// </summary>
        /// <value>The Observation4k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4k
		{
			get { return _entity.Observation4k; }
		}
        /// <summary>
        /// Gets the Observation4l
        /// </summary>
        /// <value>The Observation4l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation4l
		{
			get { return _entity.Observation4l; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit04"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit04 Entity
        {
            get { return _entity; }
        }
	}
}
