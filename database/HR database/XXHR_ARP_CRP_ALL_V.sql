USE [HR]
GO

/****** Object:  View [dbo].[XXHR_ARP_CRP_ALL_V]    Script Date: 8/07/2015 4:33:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[XXHR_ARP_CRP_ALL_V] AS
select 
   trn.PERSON_ID,
   trn.FULL_NAME,
   trn.EMPL_ID,
   trn.COURSE_NAME,
   trn.TRAINING_TITLE,
   trn.COURSE_END_DATE,
   trn.EXPIRATION_DATE,
   trn.PER_TYPE_ID,
   trn.RECORD_TYPE,
   peo.op_centre
from XXHR_add_TRNG trn,
XXHR_PEOPLE_DETAILS peo
where
trn.person_id = peo.person_id
and trn.training_title like 'Alcoa Responsible Person (WAO)%'
union all
select 
   trn.PERSON_ID,
   trn.FULL_NAME,
   trn.EMPL_ID,
   trn.COURSE_NAME,
   trn.TRAINING_TITLE,
   trn.COURSE_END_DATE,
   trn.EXPIRATION_DATE,
   trn.PER_TYPE_ID,
   trn.RECORD_TYPE,
   peo.op_centre
from XXHR_enr_TRNG trn,
XXHR_PEOPLE_DETAILS peo
where
trn.person_id = peo.person_id
and trn.training_title like 'Alcoa Responsible Person (WAO)%'




GO

