﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryStandardNamesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompanySiteCategoryStandardNamesProviderBaseCore : EntityViewProviderBase<CompanySiteCategoryStandardNames>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;CompanySiteCategoryStandardNames&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;CompanySiteCategoryStandardNames&gt;"/></returns>
		protected static VList&lt;CompanySiteCategoryStandardNames&gt; Fill(DataSet dataSet, VList<CompanySiteCategoryStandardNames> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<CompanySiteCategoryStandardNames>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;CompanySiteCategoryStandardNames&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<CompanySiteCategoryStandardNames>"/></returns>
		protected static VList&lt;CompanySiteCategoryStandardNames&gt; Fill(DataTable dataTable, VList<CompanySiteCategoryStandardNames> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					CompanySiteCategoryStandardNames c = new CompanySiteCategoryStandardNames();
					c.CompanySiteCategoryStandardId = (Convert.IsDBNull(row["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)row["CompanySiteCategoryStandardId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.SiteId = (Convert.IsDBNull(row["SiteId"]))?(int)0:(System.Int32)row["SiteId"];
					c.CompanySiteCategoryId = (Convert.IsDBNull(row["CompanySiteCategoryId"]))?(int)0:(System.Int32?)row["CompanySiteCategoryId"];
					c.LocationSponsorUserId = (Convert.IsDBNull(row["LocationSponsorUserId"]))?(int)0:(System.Int32?)row["LocationSponsorUserId"];
					c.Approved = (Convert.IsDBNull(row["Approved"]))?false:(System.Boolean?)row["Approved"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.SiteName = (Convert.IsDBNull(row["SiteName"]))?string.Empty:(System.String)row["SiteName"];
					c.SiteAbbrev = (Convert.IsDBNull(row["SiteAbbrev"]))?string.Empty:(System.String)row["SiteAbbrev"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;CompanySiteCategoryStandardNames&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;CompanySiteCategoryStandardNames&gt;"/></returns>
		protected VList<CompanySiteCategoryStandardNames> Fill(IDataReader reader, VList<CompanySiteCategoryStandardNames> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					CompanySiteCategoryStandardNames entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<CompanySiteCategoryStandardNames>("CompanySiteCategoryStandardNames",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new CompanySiteCategoryStandardNames();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanySiteCategoryStandardId)];
					//entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(reader["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)reader["CompanySiteCategoryStandardId"];
					entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.SiteId)];
					//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
					entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanySiteCategoryId)];
					//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
					entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.LocationSponsorUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardNamesColumn.LocationSponsorUserId)];
					//entity.LocationSponsorUserId = (Convert.IsDBNull(reader["LocationSponsorUserId"]))?(int)0:(System.Int32?)reader["LocationSponsorUserId"];
					entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.Approved)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardNamesColumn.Approved)];
					//entity.Approved = (Convert.IsDBNull(reader["Approved"]))?false:(System.Boolean?)reader["Approved"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardNamesColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardNamesColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedDate = (System.DateTime)reader[((int)CompanySiteCategoryStandardNamesColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.SiteName = (System.String)reader[((int)CompanySiteCategoryStandardNamesColumn.SiteName)];
					//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
					entity.SiteAbbrev = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompanySiteCategoryStandardNamesColumn.SiteAbbrev)];
					//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
					entity.CompanyName = (System.String)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="CompanySiteCategoryStandardNames"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="CompanySiteCategoryStandardNames"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, CompanySiteCategoryStandardNames entity)
		{
			reader.Read();
			entity.CompanySiteCategoryStandardId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanySiteCategoryStandardId)];
			//entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(reader["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)reader["CompanySiteCategoryStandardId"];
			entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.SiteId)];
			//entity.SiteId = (Convert.IsDBNull(reader["SiteId"]))?(int)0:(System.Int32)reader["SiteId"];
			entity.CompanySiteCategoryId = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.CompanySiteCategoryId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanySiteCategoryId)];
			//entity.CompanySiteCategoryId = (Convert.IsDBNull(reader["CompanySiteCategoryId"]))?(int)0:(System.Int32?)reader["CompanySiteCategoryId"];
			entity.LocationSponsorUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.LocationSponsorUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardNamesColumn.LocationSponsorUserId)];
			//entity.LocationSponsorUserId = (Convert.IsDBNull(reader["LocationSponsorUserId"]))?(int)0:(System.Int32?)reader["LocationSponsorUserId"];
			entity.Approved = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.Approved)))?null:(System.Boolean?)reader[((int)CompanySiteCategoryStandardNamesColumn.Approved)];
			//entity.Approved = (Convert.IsDBNull(reader["Approved"]))?false:(System.Boolean?)reader["Approved"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)CompanySiteCategoryStandardNamesColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedDate = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)CompanySiteCategoryStandardNamesColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CompanySiteCategoryStandardNamesColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)reader[((int)CompanySiteCategoryStandardNamesColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.SiteName = (System.String)reader[((int)CompanySiteCategoryStandardNamesColumn.SiteName)];
			//entity.SiteName = (Convert.IsDBNull(reader["SiteName"]))?string.Empty:(System.String)reader["SiteName"];
			entity.SiteAbbrev = (reader.IsDBNull(((int)CompanySiteCategoryStandardNamesColumn.SiteAbbrev)))?null:(System.String)reader[((int)CompanySiteCategoryStandardNamesColumn.SiteAbbrev)];
			//entity.SiteAbbrev = (Convert.IsDBNull(reader["SiteAbbrev"]))?string.Empty:(System.String)reader["SiteAbbrev"];
			entity.CompanyName = (System.String)reader[((int)CompanySiteCategoryStandardNamesColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="CompanySiteCategoryStandardNames"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="CompanySiteCategoryStandardNames"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, CompanySiteCategoryStandardNames entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanySiteCategoryStandardId = (Convert.IsDBNull(dataRow["CompanySiteCategoryStandardId"]))?(int)0:(System.Int32)dataRow["CompanySiteCategoryStandardId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.SiteId = (Convert.IsDBNull(dataRow["SiteId"]))?(int)0:(System.Int32)dataRow["SiteId"];
			entity.CompanySiteCategoryId = (Convert.IsDBNull(dataRow["CompanySiteCategoryId"]))?(int)0:(System.Int32?)dataRow["CompanySiteCategoryId"];
			entity.LocationSponsorUserId = (Convert.IsDBNull(dataRow["LocationSponsorUserId"]))?(int)0:(System.Int32?)dataRow["LocationSponsorUserId"];
			entity.Approved = (Convert.IsDBNull(dataRow["Approved"]))?false:(System.Boolean?)dataRow["Approved"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.SiteName = (Convert.IsDBNull(dataRow["SiteName"]))?string.Empty:(System.String)dataRow["SiteName"];
			entity.SiteAbbrev = (Convert.IsDBNull(dataRow["SiteAbbrev"]))?string.Empty:(System.String)dataRow["SiteAbbrev"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region CompanySiteCategoryStandardNamesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardNamesFilterBuilder : SqlFilterBuilder<CompanySiteCategoryStandardNamesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesFilterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardNamesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardNamesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardNamesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardNamesFilterBuilder

	#region CompanySiteCategoryStandardNamesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardNamesParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryStandardNamesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardNamesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryStandardNamesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryStandardNamesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryStandardNamesParameterBuilder
	
	#region CompanySiteCategoryStandardNamesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandardNames"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategoryStandardNamesSortBuilder : SqlSortBuilder<CompanySiteCategoryStandardNamesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardNamesSqlSortBuilder class.
		/// </summary>
		public CompanySiteCategoryStandardNamesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategoryStandardNamesSortBuilder

} // end namespace
