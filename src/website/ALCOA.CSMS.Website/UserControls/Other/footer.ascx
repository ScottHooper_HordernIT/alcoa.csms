﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_footer" Codebehind="footer.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register src="userLine.ascx" tagname="userLine" tagprefix="uc1" %>
<%@ Register src="help.ascx" tagname="help" tagprefix="uc2" %>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
    <tr>
        <td><img height="1" src="Images/spacer.gif" width="1" /></td>
        <td style="background-image:url(App_Themes/AlcoaMenu/Web/bckgrn_3dot.gif)" colspan="3"><img height="9" src="Images/spacer.gif" width="1" /></td>
        <td style="width: 11px"><img height="1" src="Images/spacer.gif" width="1" /></td>
    </tr>
    <tr>
        <td><img height="28" src="Images/spacer.gif" width="1" /></td>
        <td style="background-image: url(App_Themes/AlcoaMenu/Web/bckgrn_blue.gif); background-repeat: repeat-x; background-color: #003399"></td>
        <td style="background-image: url(App_Themes/AlcoaMenu/Web/bckgrn_blue.gif); background-repeat: repeat-x;background-color: #003399" width="100%">
            <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                <tbody>
                    <tr>
                        <td nowrap="nowrap" valign="middle"><span class="whitefooter">&nbsp; Copyright © 2007-2011 Alcoa Inc.</span></td>
                        <td class="whitefooter" valign="middle" width="100%"></td>
                        <td id="cell/global/en/careers/info_page/overview.asp"
                            class="whitemenubar" name="cell/global/en/careers/info_page/overview.asp"
                            nowrap="nowrap" style="padding-right: 8px; padding-left: 8px; padding-bottom: 7px;
                            border-left: transparent 0px solid; cursor: default; color: #ffffff; padding-top: 6px"
                            valign="center">
                            <img height="1" name="/global/en/careers/info_page/overview.asp" src="Images/spacer.gif"
                                width="1" /><br />
                            &nbsp;<a id="anchor/global/en/careers/info_page/overview.asp" class="whitemenubar"
                                href="" style="color: #ffffff"></a><uc1:userLine ID="userLine1" 
                                runat="server" />
                        </td>
                        <td id="cellcountry_menu" class="whitemenubar" name="cellcountry_menu" nowrap="nowrap"
                            onclick="colorCell('country_menu', '#000000', '#CCDDFF', '#003399');"
                            onfocus="blur('this')" onmouseout="colorCell('country_menu', '#FFFFFF', '', '#CCDDFF');"
                            onmouseover="colorCell('country_menu', '#000000', '#CCDDFF', '#003399');"
                            style="padding-right: 8px; padding-left: 8px; padding-bottom: 7px; border-left: #ccddff 1px solid;
                            cursor: default; color: #ffffff; padding-top: 6px" valign="center">
                            <img height="1" name="country_menu" src="Images/spacer.gif" width="1" /><br />
                            <a id="anchorcountry_menu" class="whitemenubar" href="javascript:void(0);"
                                style="color: #ffffff">help</a></td>

                        <td id="celllogin_menu" class="whitemenubar" name="celllogin_menu" nowrap="nowrap"
                            onclick="colorCell('login_menu', '#000000', '#CCDDFF', '#003399');"
                            onfocus="blur('this')" onmouseout="colorCell('login_menu', '#FFFFFF', '', '#CCDDFF');"
                            onmouseover="colorCell('login_menu', '#000000', '#CCDDFF', '#003399');"
                            style="padding-right: 8px; padding-left: 8px; padding-bottom: 7px; border-left: #ccddff 1px solid;
                            cursor: default; color: #ffffff; padding-top: 6px" valign="center">
                            <img height="1" name="login_menu" src="Images/spacer.gif" width="1" /><br />
                            <a id="anchorlogin_menu" class="whitemenubar" href="Admin.aspx" style="color: #ffffff">
                                admin login</a></td>
                         <td id="celllogout_menu" class="whitemenubar" name="celllogout_menu" nowrap="nowrap"
                            onclick="colorCell('logout_menu', '#000000', '#CCDDFF', '#003399');"
                            onfocus="blur('this')" onmouseout="colorCell('logout_menu', '#FFFFFF', '', '#CCDDFF');"
                            onmouseover="colorCell('logout_menu', '#000000', '#CCDDFF', '#003399');"
                            style="padding-right: 8px; padding-left: 8px; padding-bottom: 7px; border-left: #ccddff 1px solid;
                            cursor: default; color: #ffffff; padding-top: 6px" valign="center">
                            <img height="1" name="logout_menu" src="Images/spacer.gif" width="1" /><br />
                             <a id="anchorlogout_menu" class="whitemenubar" href="logout.aspx" style="color: #ffffff">
                                logout</a></td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td style="background-image: url(App_Themes/AlcoaMenu/Web/bckgrn_blue.gif); background-repeat: repeat-x;
            background-color: #003399">
        </td>
        <td style="width: 12px">
            <img height="1" src="Images/spacer.gif" width="1" /></td>
    </tr>
    <tr>
        <td>
            <img height="1" src="Images/spacer.gif" width="1" /></td>
        <td align="center" class="footer" colspan="3" style="text-align: center">
            <img height="5" src="Images/spacer.gif" width="1" /><br />
            <span class="nobr">
                <a class="footer" href="SiteMap.aspx" target="_self">Sitemap</a></span>
            |
            <span class="nobr">
                <a class="footer" href="http://www.alcoa.com/australia/en/general/privacy.asp?lc=3" target="_blank">Privacy</a></span>
            |
            <span class="nobr">
                <a class="footer" href="http://www.alcoa.com/australia/en/general/legal.asp?lc=3" target="_blank">Legal
                    Notices</a></span>
        </td>
        <td style="width: 11px"><img height="1" src="Images/spacer.gif" width="1" /></td>
    </tr>
    <tr><td colspan="5"><img height="6" src="Images/spacer.gif" width="1" /></td></tr>
</table>
<dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" 
    HeaderText="Help - Contractor Services Management System v1.99 - Build Date: 30th January 2013" 
    Height="95px" Width="392px" CssPostfix="Office2003Blue" EnableHotTrack="False" 
    Modal="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
    PopupElementID="anchorcountry_menu" PopupHorizontalAlign="WindowCenter" 
    PopupVerticalAlign="WindowCenter" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" AppearAfter="0" 
    EnableAnimation="False" EnableViewState="False">
    <contentcollection>
        <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True" runat="server">
            <uc2:help ID="help" runat="server" />
        </dx:popupcontrolcontentcontrol>
</contentcollection>
    <HeaderStyle>
<Paddings PaddingRight="6px"></Paddings>
</HeaderStyle>
</dx:ASPxPopupControl>

    