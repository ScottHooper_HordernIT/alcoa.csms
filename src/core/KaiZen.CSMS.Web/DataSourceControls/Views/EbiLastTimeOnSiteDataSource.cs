﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiLastTimeOnSiteProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EbiLastTimeOnSiteDataSourceDesigner))]
	public class EbiLastTimeOnSiteDataSource : ReadOnlyDataSource<EbiLastTimeOnSite>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDataSource class.
		/// </summary>
		public EbiLastTimeOnSiteDataSource() : base(new EbiLastTimeOnSiteService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiLastTimeOnSiteDataSourceView used by the EbiLastTimeOnSiteDataSource.
		/// </summary>
		protected EbiLastTimeOnSiteDataSourceView EbiLastTimeOnSiteView
		{
			get { return ( View as EbiLastTimeOnSiteDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiLastTimeOnSiteDataSourceView class that is to be
		/// used by the EbiLastTimeOnSiteDataSource.
		/// </summary>
		/// <returns>An instance of the EbiLastTimeOnSiteDataSourceView class.</returns>
		protected override BaseDataSourceView<EbiLastTimeOnSite, Object> GetNewDataSourceView()
		{
			return new EbiLastTimeOnSiteDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiLastTimeOnSiteDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiLastTimeOnSiteDataSourceView : ReadOnlyDataSourceView<EbiLastTimeOnSite>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiLastTimeOnSiteDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiLastTimeOnSiteDataSourceView(EbiLastTimeOnSiteDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiLastTimeOnSiteDataSource EbiLastTimeOnSiteOwner
		{
			get { return Owner as EbiLastTimeOnSiteDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiLastTimeOnSiteService EbiLastTimeOnSiteProvider
		{
			get { return Provider as EbiLastTimeOnSiteService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EbiLastTimeOnSiteDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiLastTimeOnSiteDataSource class.
	/// </summary>
	public class EbiLastTimeOnSiteDataSourceDesigner : ReadOnlyDataSourceDesigner<EbiLastTimeOnSite>
	{
	}

	#endregion EbiLastTimeOnSiteDataSourceDesigner

	#region EbiLastTimeOnSiteFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteFilter : SqlFilter<EbiLastTimeOnSiteColumn>
	{
	}

	#endregion EbiLastTimeOnSiteFilter

	#region EbiLastTimeOnSiteExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteExpressionBuilder : SqlExpressionBuilder<EbiLastTimeOnSiteColumn>
	{
	}
	
	#endregion EbiLastTimeOnSiteExpressionBuilder		
}

