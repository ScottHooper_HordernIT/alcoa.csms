using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Text;

public partial class UserControls_Admin_ConfigSa812 : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();

            TemplateService tService = new TemplateService();
            Template t = tService.GetByTemplateId((int)TemplateTypeList.S812Audit);
            if (t != null)
            {
                aheTemplate.Html = Encoding.ASCII.GetString(t.TemplateContent);
            }
            else
            {
                aheTemplate.Html = "";
            }
        }
    }

    protected void grid_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "EhsConsultantId" || e.Column.FieldName == "SupervisorUserId")
        {

            (e.Editor as ASPxComboBox).DataBound += new EventHandler(EhsConsultantId_EditComboBox_DataBound);
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "EhsConsultantId" || e.Column.FieldName == "SupervisorUserId")
        {
            (e.Editor as ASPxComboBox).DataBound += new EventHandler(EhsConsultantId_FilterComboBox_DataBound);
        }
    }


    private void EhsConsultantId_EditComboBox_DataBound(object sender, EventArgs e)
    {
        ListEditItem noneItem = new ListEditItem("(None)", null);
        (sender as ASPxComboBox).Items.Insert(0, noneItem);
    }
    private void EhsConsultantId_FilterComboBox_DataBound(object sender, EventArgs e)
    {
        ListEditItem noneItem = new ListEditItem("(None)", null);
        (sender as ASPxComboBox).Items.Insert(0, noneItem);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            ASCIIEncoding encoding = new ASCIIEncoding();

            TemplateService tService = new TemplateService();
            Template t = tService.GetByTemplateId((int)TemplateTypeList.S812Audit);
            if (t == null) t = new Template();
            t.TemplateContent = (byte[])encoding.GetBytes(aheTemplate.Html);
            tService.Save(t);

            lblSave.Text = "Saved Successfully";
        }
        catch (Exception ex)
        {
            lblSave.Text = "Error Saving: " + ex.Message;
            Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
        }
    }
}
