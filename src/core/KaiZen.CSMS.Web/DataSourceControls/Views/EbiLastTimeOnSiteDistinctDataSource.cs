﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiLastTimeOnSiteDistinctProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EbiLastTimeOnSiteDistinctDataSourceDesigner))]
	public class EbiLastTimeOnSiteDistinctDataSource : ReadOnlyDataSource<EbiLastTimeOnSiteDistinct>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctDataSource class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctDataSource() : base(new EbiLastTimeOnSiteDistinctService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiLastTimeOnSiteDistinctDataSourceView used by the EbiLastTimeOnSiteDistinctDataSource.
		/// </summary>
		protected EbiLastTimeOnSiteDistinctDataSourceView EbiLastTimeOnSiteDistinctView
		{
			get { return ( View as EbiLastTimeOnSiteDistinctDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiLastTimeOnSiteDistinctDataSourceView class that is to be
		/// used by the EbiLastTimeOnSiteDistinctDataSource.
		/// </summary>
		/// <returns>An instance of the EbiLastTimeOnSiteDistinctDataSourceView class.</returns>
		protected override BaseDataSourceView<EbiLastTimeOnSiteDistinct, Object> GetNewDataSourceView()
		{
			return new EbiLastTimeOnSiteDistinctDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiLastTimeOnSiteDistinctDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiLastTimeOnSiteDistinctDataSourceView : ReadOnlyDataSourceView<EbiLastTimeOnSiteDistinct>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiLastTimeOnSiteDistinctDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiLastTimeOnSiteDistinctDataSourceView(EbiLastTimeOnSiteDistinctDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiLastTimeOnSiteDistinctDataSource EbiLastTimeOnSiteDistinctOwner
		{
			get { return Owner as EbiLastTimeOnSiteDistinctDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiLastTimeOnSiteDistinctService EbiLastTimeOnSiteDistinctProvider
		{
			get { return Provider as EbiLastTimeOnSiteDistinctService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EbiLastTimeOnSiteDistinctDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiLastTimeOnSiteDistinctDataSource class.
	/// </summary>
	public class EbiLastTimeOnSiteDistinctDataSourceDesigner : ReadOnlyDataSourceDesigner<EbiLastTimeOnSiteDistinct>
	{
	}

	#endregion EbiLastTimeOnSiteDistinctDataSourceDesigner

	#region EbiLastTimeOnSiteDistinctFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctFilter : SqlFilter<EbiLastTimeOnSiteDistinctColumn>
	{
	}

	#endregion EbiLastTimeOnSiteDistinctFilter

	#region EbiLastTimeOnSiteDistinctExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctExpressionBuilder : SqlExpressionBuilder<EbiLastTimeOnSiteDistinctColumn>
	{
	}
	
	#endregion EbiLastTimeOnSiteDistinctExpressionBuilder		
}

