﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnairePresentlyWithUsersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnairePresentlyWithUsersProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsersKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsersKey key)
		{
			return Delete(transactionManager, key.QuestionnairePresentlyWithUserId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnairePresentlyWithUserId)
		{
			return Delete(null, _questionnairePresentlyWithUserId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsersKey key, int start, int pageLength)
		{
			return GetByQuestionnairePresentlyWithUserId(transactionManager, key.QuestionnairePresentlyWithUserId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(null,_questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(null, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(transactionManager, _questionnairePresentlyWithUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnairePresentlyWithUserId(transactionManager, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByQuestionnairePresentlyWithUserId(System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count)
		{
			return GetByQuestionnairePresentlyWithUserId(null, _questionnairePresentlyWithUserId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnairePresentlyWithUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByQuestionnairePresentlyWithUserId(TransactionManager transactionManager, System.Int32 _questionnairePresentlyWithUserId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="_userName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByUserName(System.String _userName)
		{
			int count = -1;
			return GetByUserName(null,_userName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="_userName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByUserName(System.String _userName, int start, int pageLength)
		{
			int count = -1;
			return GetByUserName(null, _userName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByUserName(TransactionManager transactionManager, System.String _userName)
		{
			int count = -1;
			return GetByUserName(transactionManager, _userName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByUserName(TransactionManager transactionManager, System.String _userName, int start, int pageLength)
		{
			int count = -1;
			return GetByUserName(transactionManager, _userName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="_userName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByUserName(System.String _userName, int start, int pageLength, out int count)
		{
			return GetByUserName(null, _userName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnairePresentlyWithUsers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers GetByUserName(TransactionManager transactionManager, System.String _userName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnairePresentlyWithUsers&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnairePresentlyWithUsers&gt;"/></returns>
		public static TList<QuestionnairePresentlyWithUsers> Fill(IDataReader reader, TList<QuestionnairePresentlyWithUsers> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnairePresentlyWithUsers")
					.Append("|").Append((System.Int32)reader[((int)QuestionnairePresentlyWithUsersColumn.QuestionnairePresentlyWithUserId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnairePresentlyWithUsers>(
					key.ToString(), // EntityTrackingKey
					"QuestionnairePresentlyWithUsers",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithUsersColumn.QuestionnairePresentlyWithUserId - 1)];
					c.UserName = (System.String)reader[((int)QuestionnairePresentlyWithUsersColumn.UserName - 1)];
					c.UserDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersColumn.UserDescription - 1)))?null:(System.String)reader[((int)QuestionnairePresentlyWithUsersColumn.UserDescription - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithUsersColumn.QuestionnairePresentlyWithUserId - 1)];
			entity.UserName = (System.String)reader[((int)QuestionnairePresentlyWithUsersColumn.UserName - 1)];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersColumn.UserDescription - 1)))?null:(System.String)reader[((int)QuestionnairePresentlyWithUsersColumn.UserDescription - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnairePresentlyWithUserId = (System.Int32)dataRow["QuestionnairePresentlyWithUserId"];
			entity.UserName = (System.String)dataRow["UserName"];
			entity.UserDescription = Convert.IsDBNull(dataRow["UserDescription"]) ? null : (System.String)dataRow["UserDescription"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnairePresentlyWithUserId methods when available
			
			#region QuestionnairePresentlyWithMetricCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnairePresentlyWithMetric>|QuestionnairePresentlyWithMetricCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnairePresentlyWithMetricCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnairePresentlyWithMetricCollection = DataRepository.QuestionnairePresentlyWithMetricProvider.GetByQuestionnairePresentlyWithUserId(transactionManager, entity.QuestionnairePresentlyWithUserId);

				if (deep && entity.QuestionnairePresentlyWithMetricCollection.Count > 0)
				{
					deepHandles.Add("QuestionnairePresentlyWithMetricCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnairePresentlyWithMetric>) DataRepository.QuestionnairePresentlyWithMetricProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnairePresentlyWithMetricCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnairePresentlyWithMetric>
				if (CanDeepSave(entity.QuestionnairePresentlyWithMetricCollection, "List<QuestionnairePresentlyWithMetric>|QuestionnairePresentlyWithMetricCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnairePresentlyWithMetric child in entity.QuestionnairePresentlyWithMetricCollection)
					{
						if(child.QuestionnairePresentlyWithUserIdSource != null)
						{
							child.QuestionnairePresentlyWithUserId = child.QuestionnairePresentlyWithUserIdSource.QuestionnairePresentlyWithUserId;
						}
						else
						{
							child.QuestionnairePresentlyWithUserId = entity.QuestionnairePresentlyWithUserId;
						}

					}

					if (entity.QuestionnairePresentlyWithMetricCollection.Count > 0 || entity.QuestionnairePresentlyWithMetricCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnairePresentlyWithMetricProvider.Save(transactionManager, entity.QuestionnairePresentlyWithMetricCollection);
						
						deepHandles.Add("QuestionnairePresentlyWithMetricCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnairePresentlyWithMetric >) DataRepository.QuestionnairePresentlyWithMetricProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnairePresentlyWithMetricCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnairePresentlyWithUsersChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnairePresentlyWithUsers</c>
	///</summary>
	public enum QuestionnairePresentlyWithUsersChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnairePresentlyWithUsers</c> as OneToMany for QuestionnairePresentlyWithMetricCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnairePresentlyWithMetric>))]
		QuestionnairePresentlyWithMetricCollection,
	}
	
	#endregion QuestionnairePresentlyWithUsersChildEntityTypes
	
	#region QuestionnairePresentlyWithUsersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnairePresentlyWithUsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersFilterBuilder : SqlFilterBuilder<QuestionnairePresentlyWithUsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersFilterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithUsersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersFilterBuilder
	
	#region QuestionnairePresentlyWithUsersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnairePresentlyWithUsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnairePresentlyWithUsersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersParameterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithUsersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersParameterBuilder
	
	#region QuestionnairePresentlyWithUsersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnairePresentlyWithUsersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsers"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnairePresentlyWithUsersSortBuilder : SqlSortBuilder<QuestionnairePresentlyWithUsersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersSqlSortBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithUsersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnairePresentlyWithUsersSortBuilder
	
} // end namespace
