﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanySiteCategoryStandardProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanySiteCategoryStandardDataSourceDesigner))]
	public class CompanySiteCategoryStandardDataSource : ProviderDataSource<CompanySiteCategoryStandard, CompanySiteCategoryStandardKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDataSource class.
		/// </summary>
		public CompanySiteCategoryStandardDataSource() : base(new CompanySiteCategoryStandardService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanySiteCategoryStandardDataSourceView used by the CompanySiteCategoryStandardDataSource.
		/// </summary>
		protected CompanySiteCategoryStandardDataSourceView CompanySiteCategoryStandardView
		{
			get { return ( View as CompanySiteCategoryStandardDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanySiteCategoryStandardDataSource control invokes to retrieve data.
		/// </summary>
		public CompanySiteCategoryStandardSelectMethod SelectMethod
		{
			get
			{
				CompanySiteCategoryStandardSelectMethod selectMethod = CompanySiteCategoryStandardSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanySiteCategoryStandardSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanySiteCategoryStandardDataSourceView class that is to be
		/// used by the CompanySiteCategoryStandardDataSource.
		/// </summary>
		/// <returns>An instance of the CompanySiteCategoryStandardDataSourceView class.</returns>
		protected override BaseDataSourceView<CompanySiteCategoryStandard, CompanySiteCategoryStandardKey> GetNewDataSourceView()
		{
			return new CompanySiteCategoryStandardDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanySiteCategoryStandardDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanySiteCategoryStandardDataSourceView : ProviderDataSourceView<CompanySiteCategoryStandard, CompanySiteCategoryStandardKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanySiteCategoryStandardDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanySiteCategoryStandardDataSourceView(CompanySiteCategoryStandardDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanySiteCategoryStandardDataSource CompanySiteCategoryStandardOwner
		{
			get { return Owner as CompanySiteCategoryStandardDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanySiteCategoryStandardSelectMethod SelectMethod
		{
			get { return CompanySiteCategoryStandardOwner.SelectMethod; }
			set { CompanySiteCategoryStandardOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanySiteCategoryStandardService CompanySiteCategoryStandardProvider
		{
			get { return Provider as CompanySiteCategoryStandardService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanySiteCategoryStandard> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanySiteCategoryStandard> results = null;
			CompanySiteCategoryStandard item;
			count = 0;
			
			System.Int32 _companySiteCategoryStandardId;
			System.Int32 _companyId;
			System.Int32 _siteId;
			System.Int32? _companySiteCategoryId_nullable;
			System.Int32 _modifiedByUserId;
			System.Int32? _approvedByUserId_nullable;
			System.Int32? _locationSponsorUserId_nullable;
			System.Int32? _arpUserId_nullable;

			switch ( SelectMethod )
			{
				case CompanySiteCategoryStandardSelectMethod.Get:
					CompanySiteCategoryStandardKey entityKey  = new CompanySiteCategoryStandardKey();
					entityKey.Load(values);
					item = CompanySiteCategoryStandardProvider.Get(entityKey);
					results = new TList<CompanySiteCategoryStandard>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryStandardSelectMethod.GetAll:
                    results = CompanySiteCategoryStandardProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanySiteCategoryStandardSelectMethod.GetPaged:
					results = CompanySiteCategoryStandardProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanySiteCategoryStandardProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanySiteCategoryStandardProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanySiteCategoryStandardSelectMethod.GetByCompanySiteCategoryStandardId:
					_companySiteCategoryStandardId = ( values["CompanySiteCategoryStandardId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanySiteCategoryStandardId"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryStandardProvider.GetByCompanySiteCategoryStandardId(_companySiteCategoryStandardId);
					results = new TList<CompanySiteCategoryStandard>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompanySiteCategoryStandardSelectMethod.GetByCompanyIdSiteId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = CompanySiteCategoryStandardProvider.GetByCompanyIdSiteId(_companyId, _siteId);
					results = new TList<CompanySiteCategoryStandard>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanySiteCategoryStandardSelectMethod.GetBySiteIdCompanySiteCategoryId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_companySiteCategoryId_nullable = (System.Int32?) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32?));
					results = CompanySiteCategoryStandardProvider.GetBySiteIdCompanySiteCategoryId(_siteId, _companySiteCategoryId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case CompanySiteCategoryStandardSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryStandardProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.GetByApprovedByUserId:
					_approvedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ApprovedByUserId"], typeof(System.Int32?));
					results = CompanySiteCategoryStandardProvider.GetByApprovedByUserId(_approvedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.GetByLocationSponsorUserId:
					_locationSponsorUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["LocationSponsorUserId"], typeof(System.Int32?));
					results = CompanySiteCategoryStandardProvider.GetByLocationSponsorUserId(_locationSponsorUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.GetByArpUserId:
					_arpUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ArpUserId"], typeof(System.Int32?));
					results = CompanySiteCategoryStandardProvider.GetByArpUserId(_arpUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryStandardProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.GetByCompanySiteCategoryId:
					_companySiteCategoryId_nullable = (System.Int32?) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32?));
					results = CompanySiteCategoryStandardProvider.GetByCompanySiteCategoryId(_companySiteCategoryId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case CompanySiteCategoryStandardSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = CompanySiteCategoryStandardProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanySiteCategoryStandardSelectMethod.Get || SelectMethod == CompanySiteCategoryStandardSelectMethod.GetByCompanySiteCategoryStandardId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanySiteCategoryStandard entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanySiteCategoryStandardProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanySiteCategoryStandard> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanySiteCategoryStandardProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanySiteCategoryStandardDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanySiteCategoryStandardDataSource class.
	/// </summary>
	public class CompanySiteCategoryStandardDataSourceDesigner : ProviderDataSourceDesigner<CompanySiteCategoryStandard, CompanySiteCategoryStandardKey>
	{
		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDataSourceDesigner class.
		/// </summary>
		public CompanySiteCategoryStandardDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryStandardSelectMethod SelectMethod
		{
			get { return ((CompanySiteCategoryStandardDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanySiteCategoryStandardDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanySiteCategoryStandardDataSourceActionList

	/// <summary>
	/// Supports the CompanySiteCategoryStandardDataSourceDesigner class.
	/// </summary>
	internal class CompanySiteCategoryStandardDataSourceActionList : DesignerActionList
	{
		private CompanySiteCategoryStandardDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryStandardDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanySiteCategoryStandardDataSourceActionList(CompanySiteCategoryStandardDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanySiteCategoryStandardSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanySiteCategoryStandardDataSourceActionList
	
	#endregion CompanySiteCategoryStandardDataSourceDesigner
	
	#region CompanySiteCategoryStandardSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanySiteCategoryStandardDataSource.SelectMethod property.
	/// </summary>
	public enum CompanySiteCategoryStandardSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryStandardId method.
		/// </summary>
		GetByCompanySiteCategoryStandardId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteId method.
		/// </summary>
		GetByCompanyIdSiteId,
		/// <summary>
		/// Represents the GetBySiteIdCompanySiteCategoryId method.
		/// </summary>
		GetBySiteIdCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByApprovedByUserId method.
		/// </summary>
		GetByApprovedByUserId,
		/// <summary>
		/// Represents the GetByLocationSponsorUserId method.
		/// </summary>
		GetByLocationSponsorUserId,
		/// <summary>
		/// Represents the GetByArpUserId method.
		/// </summary>
		GetByArpUserId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion CompanySiteCategoryStandardSelectMethod

	#region CompanySiteCategoryStandardFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardFilter : SqlFilter<CompanySiteCategoryStandardColumn>
	{
	}
	
	#endregion CompanySiteCategoryStandardFilter

	#region CompanySiteCategoryStandardExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardExpressionBuilder : SqlExpressionBuilder<CompanySiteCategoryStandardColumn>
	{
	}
	
	#endregion CompanySiteCategoryStandardExpressionBuilder	

	#region CompanySiteCategoryStandardProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanySiteCategoryStandardChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryStandard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryStandardProperty : ChildEntityProperty<CompanySiteCategoryStandardChildEntityTypes>
	{
	}
	
	#endregion CompanySiteCategoryStandardProperty
}

