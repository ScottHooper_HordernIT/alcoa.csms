﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'AdHoc_Radar_Items' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AdHocRadarItems : AdHocRadarItemsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AdHocRadarItems"/> instance.
		///</summary>
		public AdHocRadarItems():base(){}	
		
		#endregion
	}
}
