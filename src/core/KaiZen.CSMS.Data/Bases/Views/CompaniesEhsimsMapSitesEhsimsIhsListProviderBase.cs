﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesEhsimsMapSitesEhsimsIhsListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class CompaniesEhsimsMapSitesEhsimsIhsListProviderBase : CompaniesEhsimsMapSitesEhsimsIhsListProviderBaseCore
	{
	} // end class
} // end namespace
