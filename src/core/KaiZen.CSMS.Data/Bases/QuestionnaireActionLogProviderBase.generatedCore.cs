﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireActionLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireActionLogProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireActionLog, KaiZen.CSMS.Entities.QuestionnaireActionLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireActionLogKey key)
		{
			return Delete(transactionManager, key.QuestionnaireActionLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireActionLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireActionLogId)
		{
			return Delete(null, _questionnaireActionLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireActionLogId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireActionLog_Role key.
		///		FK_QuestionnaireActionLog_Role Description: 
		/// </summary>
		/// <param name="_createdByRoleId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByRoleId(System.Int32 _createdByRoleId)
		{
			int count = -1;
			return GetByCreatedByRoleId(_createdByRoleId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireActionLog_Role key.
		///		FK_QuestionnaireActionLog_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByRoleId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireActionLog> GetByCreatedByRoleId(TransactionManager transactionManager, System.Int32 _createdByRoleId)
		{
			int count = -1;
			return GetByCreatedByRoleId(transactionManager, _createdByRoleId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireActionLog_Role key.
		///		FK_QuestionnaireActionLog_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByRoleId(TransactionManager transactionManager, System.Int32 _createdByRoleId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByRoleId(transactionManager, _createdByRoleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireActionLog_Role key.
		///		fkQuestionnaireActionLogRole Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByRoleId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByRoleId(System.Int32 _createdByRoleId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByRoleId(null, _createdByRoleId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireActionLog_Role key.
		///		fkQuestionnaireActionLogRole Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByRoleId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByRoleId(System.Int32 _createdByRoleId, int start, int pageLength,out int count)
		{
			return GetByCreatedByRoleId(null, _createdByRoleId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireActionLog_Role key.
		///		FK_QuestionnaireActionLog_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public abstract TList<QuestionnaireActionLog> GetByCreatedByRoleId(TransactionManager transactionManager, System.Int32 _createdByRoleId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_QuestionnaireAction key.
		///		FK_QuestionnaireHistoryLog_QuestionnaireAction Description: 
		/// </summary>
		/// <param name="_questionnaireActionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireActionId(System.Int32 _questionnaireActionId)
		{
			int count = -1;
			return GetByQuestionnaireActionId(_questionnaireActionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_QuestionnaireAction key.
		///		FK_QuestionnaireHistoryLog_QuestionnaireAction Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireActionLog> GetByQuestionnaireActionId(TransactionManager transactionManager, System.Int32 _questionnaireActionId)
		{
			int count = -1;
			return GetByQuestionnaireActionId(transactionManager, _questionnaireActionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_QuestionnaireAction key.
		///		FK_QuestionnaireHistoryLog_QuestionnaireAction Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireActionId(TransactionManager transactionManager, System.Int32 _questionnaireActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireActionId(transactionManager, _questionnaireActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_QuestionnaireAction key.
		///		fkQuestionnaireHistoryLogQuestionnaireAction Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireActionId(System.Int32 _questionnaireActionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireActionId(null, _questionnaireActionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_QuestionnaireAction key.
		///		fkQuestionnaireHistoryLogQuestionnaireAction Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireActionId(System.Int32 _questionnaireActionId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireActionId(null, _questionnaireActionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_QuestionnaireAction key.
		///		FK_QuestionnaireHistoryLog_QuestionnaireAction Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public abstract TList<QuestionnaireActionLog> GetByQuestionnaireActionId(TransactionManager transactionManager, System.Int32 _questionnaireActionId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_Users key.
		///		FK_QuestionnaireHistoryLog_Users Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_Users key.
		///		FK_QuestionnaireHistoryLog_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireActionLog> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_Users key.
		///		FK_QuestionnaireHistoryLog_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_Users key.
		///		fkQuestionnaireHistoryLogUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_Users key.
		///		fkQuestionnaireHistoryLogUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public TList<QuestionnaireActionLog> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireHistoryLog_Users key.
		///		FK_QuestionnaireHistoryLog_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireActionLog objects.</returns>
		public abstract TList<QuestionnaireActionLog> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireActionLog Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireActionLogKey key, int start, int pageLength)
		{
			return GetByQuestionnaireActionLogId(transactionManager, key.QuestionnaireActionLogId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireActionLog index.
		/// </summary>
		/// <param name="_questionnaireActionLogId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireActionLog GetByQuestionnaireActionLogId(System.Int32 _questionnaireActionLogId)
		{
			int count = -1;
			return GetByQuestionnaireActionLogId(null,_questionnaireActionLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireActionLog index.
		/// </summary>
		/// <param name="_questionnaireActionLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireActionLog GetByQuestionnaireActionLogId(System.Int32 _questionnaireActionLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireActionLogId(null, _questionnaireActionLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireActionLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionLogId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireActionLog GetByQuestionnaireActionLogId(TransactionManager transactionManager, System.Int32 _questionnaireActionLogId)
		{
			int count = -1;
			return GetByQuestionnaireActionLogId(transactionManager, _questionnaireActionLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireActionLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireActionLog GetByQuestionnaireActionLogId(TransactionManager transactionManager, System.Int32 _questionnaireActionLogId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireActionLogId(transactionManager, _questionnaireActionLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireActionLog index.
		/// </summary>
		/// <param name="_questionnaireActionLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireActionLog GetByQuestionnaireActionLogId(System.Int32 _questionnaireActionLogId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireActionLogId(null, _questionnaireActionLogId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireActionLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireActionLog GetByQuestionnaireActionLogId(TransactionManager transactionManager, System.Int32 _questionnaireActionLogId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireHistoryLog index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireActionLog&gt;"/> class.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireHistoryLog index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireActionLog&gt;"/> class.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireHistoryLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireActionLog&gt;"/> class.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireHistoryLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireActionLog&gt;"/> class.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireHistoryLog index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireActionLog&gt;"/> class.</returns>
		public TList<QuestionnaireActionLog> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireHistoryLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireActionLog&gt;"/> class.</returns>
		public abstract TList<QuestionnaireActionLog> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireActionLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireActionLog&gt;"/></returns>
		public static TList<QuestionnaireActionLog> Fill(IDataReader reader, TList<QuestionnaireActionLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireActionLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireActionLog")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireActionLogId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireActionLog>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireActionLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireActionLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireActionLogId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireActionLogId - 1)];
					c.QuestionnaireActionId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireActionId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireId - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)QuestionnaireActionLogColumn.CreatedDate - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.CreatedByUserId - 1)];
					c.CreatedByRoleId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.CreatedByRoleId - 1)];
					c.CreatedByCompanyId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.CreatedByCompanyId - 1)];
					c.CreatedByPrivledgedRole = (reader.IsDBNull(((int)QuestionnaireActionLogColumn.CreatedByPrivledgedRole - 1)))?null:(System.String)reader[((int)QuestionnaireActionLogColumn.CreatedByPrivledgedRole - 1)];
					c.HistoryLog = (System.String)reader[((int)QuestionnaireActionLogColumn.HistoryLog - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireActionLog entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireActionLogId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireActionLogId - 1)];
			entity.QuestionnaireActionId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireActionId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.QuestionnaireId - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireActionLogColumn.CreatedDate - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.CreatedByUserId - 1)];
			entity.CreatedByRoleId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.CreatedByRoleId - 1)];
			entity.CreatedByCompanyId = (System.Int32)reader[((int)QuestionnaireActionLogColumn.CreatedByCompanyId - 1)];
			entity.CreatedByPrivledgedRole = (reader.IsDBNull(((int)QuestionnaireActionLogColumn.CreatedByPrivledgedRole - 1)))?null:(System.String)reader[((int)QuestionnaireActionLogColumn.CreatedByPrivledgedRole - 1)];
			entity.HistoryLog = (System.String)reader[((int)QuestionnaireActionLogColumn.HistoryLog - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireActionLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireActionLogId = (System.Int32)dataRow["QuestionnaireActionLogId"];
			entity.QuestionnaireActionId = (System.Int32)dataRow["QuestionnaireActionId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedByRoleId = (System.Int32)dataRow["CreatedByRoleId"];
			entity.CreatedByCompanyId = (System.Int32)dataRow["CreatedByCompanyId"];
			entity.CreatedByPrivledgedRole = Convert.IsDBNull(dataRow["CreatedByPrivledgedRole"]) ? null : (System.String)dataRow["CreatedByPrivledgedRole"];
			entity.HistoryLog = (System.String)dataRow["HistoryLog"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireActionLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireActionLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireActionLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CreatedByRoleIdSource	
			if (CanDeepLoad(entity, "Role|CreatedByRoleIdSource", deepLoadType, innerList) 
				&& entity.CreatedByRoleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByRoleId;
				Role tmpEntity = EntityManager.LocateEntity<Role>(EntityLocator.ConstructKeyFromPkItems(typeof(Role), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByRoleIdSource = tmpEntity;
				else
					entity.CreatedByRoleIdSource = DataRepository.RoleProvider.GetByRoleId(transactionManager, entity.CreatedByRoleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByRoleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByRoleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RoleProvider.DeepLoad(transactionManager, entity.CreatedByRoleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByRoleIdSource

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region QuestionnaireActionIdSource	
			if (CanDeepLoad(entity, "QuestionnaireAction|QuestionnaireActionIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireActionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireActionId;
				QuestionnaireAction tmpEntity = EntityManager.LocateEntity<QuestionnaireAction>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireAction), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireActionIdSource = tmpEntity;
				else
					entity.QuestionnaireActionIdSource = DataRepository.QuestionnaireActionProvider.GetByQuestionnaireActionId(transactionManager, entity.QuestionnaireActionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireActionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireActionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireActionProvider.DeepLoad(transactionManager, entity.QuestionnaireActionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireActionIdSource

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireActionLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireActionLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireActionLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireActionLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CreatedByRoleIdSource
			if (CanDeepSave(entity, "Role|CreatedByRoleIdSource", deepSaveType, innerList) 
				&& entity.CreatedByRoleIdSource != null)
			{
				DataRepository.RoleProvider.Save(transactionManager, entity.CreatedByRoleIdSource);
				entity.CreatedByRoleId = entity.CreatedByRoleIdSource.RoleId;
			}
			#endregion 
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region QuestionnaireActionIdSource
			if (CanDeepSave(entity, "QuestionnaireAction|QuestionnaireActionIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireActionIdSource != null)
			{
				DataRepository.QuestionnaireActionProvider.Save(transactionManager, entity.QuestionnaireActionIdSource);
				entity.QuestionnaireActionId = entity.QuestionnaireActionIdSource.QuestionnaireActionId;
			}
			#endregion 
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireActionLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireActionLog</c>
	///</summary>
	public enum QuestionnaireActionLogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Role</c> at CreatedByRoleIdSource
		///</summary>
		[ChildEntityType(typeof(Role))]
		Role,
			
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireAction</c> at QuestionnaireActionIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireAction))]
		QuestionnaireAction,
			
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion QuestionnaireActionLogChildEntityTypes
	
	#region QuestionnaireActionLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireActionLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFilterBuilder : SqlFilterBuilder<QuestionnaireActionLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFilterBuilder class.
		/// </summary>
		public QuestionnaireActionLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogFilterBuilder
	
	#region QuestionnaireActionLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireActionLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireActionLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogParameterBuilder class.
		/// </summary>
		public QuestionnaireActionLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionLogParameterBuilder
	
	#region QuestionnaireActionLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireActionLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireActionLogSortBuilder : SqlSortBuilder<QuestionnaireActionLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogSqlSortBuilder class.
		/// </summary>
		public QuestionnaireActionLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireActionLogSortBuilder
	
} // end namespace
