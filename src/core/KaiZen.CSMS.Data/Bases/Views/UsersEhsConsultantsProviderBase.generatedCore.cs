﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersEhsConsultantsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersEhsConsultantsProviderBaseCore : EntityViewProviderBase<UsersEhsConsultants>
	{
		#region Custom Methods
		
		
		#region _UsersEhsConsultants_GetAll_Active
		
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_GetAll_Active' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;UsersEhsConsultants&gt;"/> instance.</returns>
		public VList<UsersEhsConsultants> GetAll_Active()
		{
			return GetAll_Active(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_GetAll_Active' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;UsersEhsConsultants&gt;"/> instance.</returns>
		public VList<UsersEhsConsultants> GetAll_Active(int start, int pageLength)
		{
			return GetAll_Active(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_GetAll_Active' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="VList&lt;UsersEhsConsultants&gt;"/> instance.</returns>
		public VList<UsersEhsConsultants> GetAll_Active(TransactionManager transactionManager)
		{
			return GetAll_Active(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_GetAll_Active' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="VList&lt;UsersEhsConsultants&gt;"/> instance.</returns>
		public abstract VList<UsersEhsConsultants> GetAll_Active(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _UsersEhsConsultants_ListOrdered
		
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_ListOrdered' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListOrdered()
		{
			return ListOrdered(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_ListOrdered' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListOrdered(int start, int pageLength)
		{
			return ListOrdered(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_ListOrdered' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListOrdered(TransactionManager transactionManager)
		{
			return ListOrdered(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersEhsConsultants_ListOrdered' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListOrdered(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersEhsConsultants&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersEhsConsultants&gt;"/></returns>
		protected static VList&lt;UsersEhsConsultants&gt; Fill(DataSet dataSet, VList<UsersEhsConsultants> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersEhsConsultants>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersEhsConsultants&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersEhsConsultants>"/></returns>
		protected static VList&lt;UsersEhsConsultants&gt; Fill(DataTable dataTable, VList<UsersEhsConsultants> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersEhsConsultants c = new UsersEhsConsultants();
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32)row["EHSConsultantId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.Enabled = (Convert.IsDBNull(row["Enabled"]))?false:(System.Boolean)row["Enabled"];
					c.SafeNameDefault = (Convert.IsDBNull(row["Default"]))?false:(System.Boolean?)row["Default"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersEhsConsultants&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersEhsConsultants&gt;"/></returns>
		protected VList<UsersEhsConsultants> Fill(IDataReader reader, VList<UsersEhsConsultants> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersEhsConsultants entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersEhsConsultants>("UsersEhsConsultants",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersEhsConsultants();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsConsultantId = (System.Int32)reader[((int)UsersEhsConsultantsColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32)reader["EHSConsultantId"];
					entity.UserId = (System.Int32)reader[((int)UsersEhsConsultantsColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserFullName = (System.String)reader[((int)UsersEhsConsultantsColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.UserFullNameLogon = (System.String)reader[((int)UsersEhsConsultantsColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.Enabled = (System.Boolean)reader[((int)UsersEhsConsultantsColumn.Enabled)];
					//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
					entity.SafeNameDefault = (reader.IsDBNull(((int)UsersEhsConsultantsColumn.SafeNameDefault)))?null:(System.Boolean?)reader[((int)UsersEhsConsultantsColumn.SafeNameDefault)];
					//entity.SafeNameDefault = (Convert.IsDBNull(reader["Default"]))?false:(System.Boolean?)reader["Default"];
					entity.Email = (System.String)reader[((int)UsersEhsConsultantsColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersEhsConsultants"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersEhsConsultants"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersEhsConsultants entity)
		{
			reader.Read();
			entity.EhsConsultantId = (System.Int32)reader[((int)UsersEhsConsultantsColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32)reader["EHSConsultantId"];
			entity.UserId = (System.Int32)reader[((int)UsersEhsConsultantsColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserFullName = (System.String)reader[((int)UsersEhsConsultantsColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			entity.UserFullNameLogon = (System.String)reader[((int)UsersEhsConsultantsColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.Enabled = (System.Boolean)reader[((int)UsersEhsConsultantsColumn.Enabled)];
			//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
			entity.SafeNameDefault = (reader.IsDBNull(((int)UsersEhsConsultantsColumn.SafeNameDefault)))?null:(System.Boolean?)reader[((int)UsersEhsConsultantsColumn.SafeNameDefault)];
			//entity.SafeNameDefault = (Convert.IsDBNull(reader["Default"]))?false:(System.Boolean?)reader["Default"];
			entity.Email = (System.String)reader[((int)UsersEhsConsultantsColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersEhsConsultants"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersEhsConsultants"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersEhsConsultants entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32)dataRow["EHSConsultantId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.Enabled = (Convert.IsDBNull(dataRow["Enabled"]))?false:(System.Boolean)dataRow["Enabled"];
			entity.SafeNameDefault = (Convert.IsDBNull(dataRow["Default"]))?false:(System.Boolean?)dataRow["Default"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersEhsConsultantsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEhsConsultantsFilterBuilder : SqlFilterBuilder<UsersEhsConsultantsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsFilterBuilder class.
		/// </summary>
		public UsersEhsConsultantsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEhsConsultantsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEhsConsultantsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEhsConsultantsFilterBuilder

	#region UsersEhsConsultantsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEhsConsultantsParameterBuilder : ParameterizedSqlFilterBuilder<UsersEhsConsultantsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsParameterBuilder class.
		/// </summary>
		public UsersEhsConsultantsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersEhsConsultantsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersEhsConsultantsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersEhsConsultantsParameterBuilder
	
	#region UsersEhsConsultantsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEhsConsultants"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersEhsConsultantsSortBuilder : SqlSortBuilder<UsersEhsConsultantsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsSqlSortBuilder class.
		/// </summary>
		public UsersEhsConsultantsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersEhsConsultantsSortBuilder

} // end namespace
