﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>CsaCompanySiteCategoryRepeater</c>
    /// </summary>
	public class CsaCompanySiteCategoryRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CsaCompanySiteCategoryRepeaterDesigner"/> class.
        /// </summary>
		public CsaCompanySiteCategoryRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is CsaCompanySiteCategoryRepeater))
			{ 
				throw new ArgumentException("Component is not a CsaCompanySiteCategoryRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			CsaCompanySiteCategoryRepeater z = (CsaCompanySiteCategoryRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="CsaCompanySiteCategoryRepeater"/> Type.
    /// </summary>
	[Designer(typeof(CsaCompanySiteCategoryRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:CsaCompanySiteCategoryRepeater runat=\"server\"></{0}:CsaCompanySiteCategoryRepeater>")]
	public class CsaCompanySiteCategoryRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CsaCompanySiteCategoryRepeater"/> class.
        /// </summary>
		public CsaCompanySiteCategoryRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsaCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsaCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(CsaCompanySiteCategoryItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsaCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsaCompanySiteCategoryItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.CsaCompanySiteCategory entity = o as KaiZen.CSMS.Entities.CsaCompanySiteCategory;
						CsaCompanySiteCategoryItem container = new CsaCompanySiteCategoryItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class CsaCompanySiteCategoryItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.CsaCompanySiteCategory _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CsaCompanySiteCategoryItem"/> class.
        /// </summary>
		public CsaCompanySiteCategoryItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CsaCompanySiteCategoryItem"/> class.
        /// </summary>
		public CsaCompanySiteCategoryItem(KaiZen.CSMS.Entities.CsaCompanySiteCategory entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CsaId
        /// </summary>
        /// <value>The CsaId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CsaId
		{
			get { return _entity.CsaId; }
		}
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the SiteId
        /// </summary>
        /// <value>The SiteId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 SiteId
		{
			get { return _entity.SiteId; }
		}
        /// <summary>
        /// Gets the CreatedByUserId
        /// </summary>
        /// <value>The CreatedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CreatedByUserId
		{
			get { return _entity.CreatedByUserId; }
		}
        /// <summary>
        /// Gets the ModifiedByUserId
        /// </summary>
        /// <value>The ModifiedByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 ModifiedByUserId
		{
			get { return _entity.ModifiedByUserId; }
		}
        /// <summary>
        /// Gets the DateAdded
        /// </summary>
        /// <value>The DateAdded.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateAdded
		{
			get { return _entity.DateAdded; }
		}
        /// <summary>
        /// Gets the DateModified
        /// </summary>
        /// <value>The DateModified.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateModified
		{
			get { return _entity.DateModified; }
		}
        /// <summary>
        /// Gets the QtrId
        /// </summary>
        /// <value>The QtrId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QtrId
		{
			get { return _entity.QtrId; }
		}
        /// <summary>
        /// Gets the Year
        /// </summary>
        /// <value>The Year.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int64 Year
		{
			get { return _entity.Year; }
		}
        /// <summary>
        /// Gets the TotalPossibleScore
        /// </summary>
        /// <value>The TotalPossibleScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalPossibleScore
		{
			get { return _entity.TotalPossibleScore; }
		}
        /// <summary>
        /// Gets the TotalActualScore
        /// </summary>
        /// <value>The TotalActualScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalActualScore
		{
			get { return _entity.TotalActualScore; }
		}
        /// <summary>
        /// Gets the TotalRating
        /// </summary>
        /// <value>The TotalRating.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalRating
		{
			get { return _entity.TotalRating; }
		}
        /// <summary>
        /// Gets the CompanySiteCategoryId
        /// </summary>
        /// <value>The CompanySiteCategoryId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? CompanySiteCategoryId
		{
			get { return _entity.CompanySiteCategoryId; }
		}

	}
}
