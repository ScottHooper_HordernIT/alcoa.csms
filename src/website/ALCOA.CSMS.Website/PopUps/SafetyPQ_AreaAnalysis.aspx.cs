﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;

using System.Web.UI.HtmlControls;
using System.Data;

public partial class PopUps_SafetyPQ_AreaAnalysis : System.Web.UI.Page
{
    bool forcedHigh = false;
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            int q = Convert.ToInt32(Request.QueryString["q"].ToString());
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire _q = qs.GetByQuestionnaireId(q);
            if (_q != null)
            {
                if (_q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    Draw(q);
                }
                else
                {
                    ASPxLabel1.Text = "Score Analysis can not be displayed. Supplier Questionnaire is incomplete.";
                    grid.Visible = false;
                }
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Safety Qualification - Score Analysis"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true, true);

        }
        else
        {
            grid.DataSource = (DataTable)Session["dtGrid"];
            grid.DataBind();
        }
    }

    public void Draw(int q)
    {

        DataTable dtGrid = new DataTable();
        dtGrid.Columns.Add("Area", typeof(string));
        dtGrid.Columns.Add("QuestionNo", typeof(string));
        dtGrid.Columns.Add("Question", typeof(string));
        dtGrid.Columns.Add("Answer", typeof(string));
        dtGrid.Columns.Add("FileUploaded", typeof(string));
        dtGrid.Columns.Add("Comments", typeof(string));
        dtGrid.Columns.Add("Score", typeof(int));
        dtGrid.Columns.Add("ForcedHigh", typeof(string)); //hidden

        dtGrid.Rows.Add(NewRow(dtGrid, q, "13", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "14", "A", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "14", "B", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "14", "C", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "14", "D", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "21", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "23", "Expectations"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "24", "Expectations"));

        dtGrid.Rows.Add(NewRow(dtGrid, q, "26", "A", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "28", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "25", "A", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "25", "B", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "25", "C", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "27", "A", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "27", "B", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "27", "C", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "27", "D", "Results"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "27", "E", "Results"));

        dtGrid.Rows.Add(NewRow(dtGrid, q, "16", "A", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "16", "B", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "16", "C", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "18", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "19", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "20", "A", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "20", "B", "Staffing"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "22", "Staffing"));

        dtGrid.Rows.Add(NewRow(dtGrid, q, "12", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "15", "A", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "15", "B", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "15", "C", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "17", "A", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "17", "B", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "29i", "A", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "29i", "B", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "29i", "C", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "29i", "D", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "29ii", "A", "Systems"));
        dtGrid.Rows.Add(NewRow(dtGrid, q, "30", "Systems"));

        try
        {
            Session["dtGrid"] = dtGrid;
            grid.DataSource = (DataTable)Session["dtGrid"];
            grid.SortBy((GridViewDataColumn)grid.Columns["Area"], 0);
            grid.SortBy((GridViewDataColumn)grid.Columns["QuestionNo"], 1);
            //grid.SortBy((GridViewDataColumn)grid.Columns["Score"], 2);
            grid.DataBind();
        }
        catch { }
    }

    public DataRow NewRow(DataTable dt, int q, string qNo, string cat)
    {
        DataRow dr = dt.NewRow();
        dr["QuestionNo"] = qNo;
        dr["Area"] = cat;

        try
        {
            string selected = "";

            dr["Question"] = GetQuestionText(qNo);
            selected = GetVal(q, qNo);

            switch (qNo)
            {
                case "22":
                    dr["Question"] = String.Format("{0} {1} {2} {3}", GetQuestionText(qNo), GetAnswerText(qNo, "A"), GetAnswerText(qNo, "B"), GetAnswerText(qNo, "C"));
                    dr["Answer"] = String.Format("A) {0} B) {1} C) {2}", GetVal(q, "22", "A", true), GetVal(q, "22", "B", true), GetVal(q, "22", "C", true));
                    break;
                case "28":
                    dr["Answer"] = "...";
                    selected = "...";
                    break;
                case "27":
                    dr["Answer"] = "...";
                    break;
                default:
                    dr["Answer"] = GetAnswerText(qNo, selected);
                    break;
            }

            dr["Score"] = GetScore(q, qNo, selected);
            dr["FileUploaded"] = HasAttachment(q, qNo, selected);
            dr["Comments"] = HasComments(q, qNo);
            if (forcedHigh == true) { dr["ForcedHigh"] = "Yes"; };
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //ToDo: Handle Exception
        }

        return dr;
    }

    public DataRow NewRow(DataTable dt, int q, string qNo, string subquestion, string cat)
    {
        DataRow dr = dt.NewRow();
        dr["QuestionNo"] = qNo + subquestion;
        dr["Area"] = cat;

        if (qNo == "27") subquestion = subquestion + "1";

        try
        {
            string selected = subquestion;
            string _selected = "No";

            if (selected == GetVal(q, qNo, subquestion))
            {
                _selected = "Yes";
            }
            dr["Question"] = String.Format("{0}\n{1}", GetQuestionText(qNo), GetAnswerText(qNo, selected));

            dr["Answer"] = _selected;
            dr["Score"] = GetScore(q, qNo, selected, _selected);
            dr["FileUploaded"] = HasAttachment(q, qNo + subquestion, selected);
            dr["Comments"] = HasComments(q, qNo);
            string test = HasComments(q, qNo, subquestion);
            if (!String.IsNullOrEmpty(test)) dr["Comments"] = test;
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //ToDo: Handle Exception
        }

        return dr;
    }


    public string GetVal(int q, string qNo)
    {
        string selected = "";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        TList<QuestionnaireMainAnswer> qmaList = qmaService.GetByQuestionNo(qNo);
        foreach (QuestionnaireMainAnswer qma in qmaList)
        {
            if (qma.AnswerNo == "A" || qma.AnswerNo == "B" || qma.AnswerNo == "C" || qma.AnswerNo == "D" || qma.AnswerNo == "E")
            {
                QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
                QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
                if (qmr != null)
                {
                    if (qmr.AnswerText == "Yes")
                    {
                        selected = qma.AnswerNo;
                    }
                }
            }
        }
        return selected;
    }

    public string GetVal(int q, string qNo, string subquestion)
    {
        string selected = "";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        QuestionnaireMainAnswer qma = qmaService.GetByQuestionNoAnswerNo(qNo, subquestion);

        QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
        QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
        if (qmr != null)
        {
            if (qmr.AnswerText == "Yes")
            {
                selected = qma.AnswerNo;
            }
        }
        return selected;
    }

    public string GetVal(int q, string qNo, string subquestion, bool number)
    {
        string selected = "";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        QuestionnaireMainAnswer qma = qmaService.GetByQuestionNoAnswerNo(qNo, subquestion);

        QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
        QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
        if (qmr != null)
        {
            selected = qmr.AnswerText;
        }
        return selected;
    }

    public string GetVal(int q, string qNo, bool number)
    {
        string selected = "";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        TList<QuestionnaireMainAnswer> qmaList = qmaService.GetByQuestionNo(qNo);
        foreach (QuestionnaireMainAnswer qma in qmaList)
        {
            if (qma.AnswerNo == "A" || qma.AnswerNo == "B" || qma.AnswerNo == "C" || qma.AnswerNo == "D" || qma.AnswerNo == "E")
            {
                QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
                QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
                if (qmr != null)
                {
                    selected = qmr.AnswerText;
                }
            }
        }
        return selected;
    }
    public int GetScore(int q, string qNo, string selected)
    {
        int score = 0;
        bool hasExample = false;
        forcedHigh = false;

        if (!String.IsNullOrEmpty(selected))
        {
            switch (qNo)
            {
                case "12":
                    switch (selected)
                    {
                        case "A":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score += 2;
                            }
                            else
                            {
                                score++;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "13":
                    switch (selected)
                    {
                        case "A":
                            score += 2;
                            break;
                        case "B":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                //14 to 17 = subquestions = other section
                case "18":
                    switch (selected)
                    {
                        case "C":
                            score++;
                            break;
                        case "D":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "19":
                    switch (selected)
                    {
                        case "A":
                            score += 2;
                            break;
                        case "B":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                //20A/B = other section
                case "21":
                    switch (selected)
                    {
                        case "A":
                            score += 2;
                            break;
                        case "B":
                            score++;
                            break;
                        case "D":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "22":
                    if (GetVal(q, qNo, "A") == "A")
                    {
                        score += 2;
                    }
                    else
                    {
                        if (GetVal(q, qNo, "B") == "B" && GetVal(q, qNo, "C") == "C")
                        {
                            score++;
                        }
                    }
                    break;
                case "23":
                    switch (selected)
                    {
                        case "A":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score += 2;
                            }
                            break;
                        case "B":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score++;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "24":
                    switch (selected)
                    {
                        case "A":
                            score += 2;
                            break;
                        case "B":
                            score++;
                            break;
                        case "D":
                            if (!String.IsNullOrEmpty(HasComments(q, qNo)))
                            {
                                score++;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "28":
                    int? count04 = 0;
                    if (GetDec(GetVal(q, qNo, "A6", true)) < GetDec(GetVal(q, qNo, "A3", true))) count04++;
                    if (GetDec(GetVal(q, qNo, "B6", true)) < GetDec(GetVal(q, qNo, "B3", true))) count04++;
                    if (GetDec(GetVal(q, qNo, "C6", true)) < GetDec(GetVal(q, qNo, "C3", true))) count04++;
                    if (count04 == 2) score++;
                    if (count04 == 3) score += 2;
                    break;
                case "30":
                    switch (selected)
                    {
                        case "A":
                            score += 2;
                            break;
                        case "B":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
            return score;
        }
        else
        {
            return 0;
        }
    }
    public int GetScore(int q, string qNo, string selected, string _selected)
    {
        int score = 0;
        bool hasExample = false;
        forcedHigh = false;

        if (!String.IsNullOrEmpty(selected))
        {
            switch (qNo + selected)
            {
                case "14A":
                    switch (_selected)
                    {
                        case "Yes":
                            score += 2;
                            break;
                        case "No":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "14B":
                    switch (_selected)
                    {
                        case "Yes":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "14C":
                    switch (_selected)
                    {
                        case "Yes":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "14D":
                    switch (_selected)
                    {
                        case "Yes":
                            if (!String.IsNullOrEmpty(HasComments(q, qNo)))
                            {
                                score += 2;
                            }
                            else
                            {
                                score++;
                            }
                            break;
                        case "No":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "15A":
                    switch (_selected)
                    {
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "15B":
                    switch (_selected)
                    {
                        case "Yes":
                            score++;
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "15C":
                    switch (_selected)
                    {
                        case "Yes":
                            if (!String.IsNullOrEmpty(HasComments(q, qNo, selected)))
                            {
                                score += 2;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "16A":
                    switch (_selected)
                    {
                        case "Yes":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "16B":
                    switch (_selected)
                    {
                        case "Yes":
                            if (!String.IsNullOrEmpty(HasComments(q, qNo, selected)))
                            {
                                score += 2;
                            }
                            else
                            {
                                score++;
                            }
                            break;
                        default:
                            score++;
                            break;
                    }
                    break;
                case "16C":
                    switch (_selected)
                    {
                        case "Yes":
                            if (!String.IsNullOrEmpty(HasComments(q, qNo, selected)))
                            {
                                score += 2;
                            }
                            else
                            {
                                score++;
                            }
                            break;
                        default:
                            score++;
                            break;
                    }
                    break;
                case "17A":
                    switch (_selected)
                    {
                        case "Yes":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "17B":
                    switch (_selected)
                    {
                        case "Yes":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score += 2;
                            }
                            else
                            {
                                score++;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "20A":
                    switch (_selected)
                    {
                        case "Yes":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "20B":
                    switch (_selected)
                    {
                        case "Yes":
                            if (!String.IsNullOrEmpty(HasComments(q, qNo, selected)))
                            {
                                score += 2;
                            }
                            else
                            {
                                score++;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case "25A":
                    switch (_selected)
                    {
                        case "Yes":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score++;
                            }
                            else
                            {
                                forcedHigh = true;
                            }
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "25B":
                    switch (_selected)
                    {
                        case "Yes":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score++;
                            }
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "25C":
                    switch (_selected)
                    {
                        case "Yes":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;

                            if (hasExample)
                            {
                                score++;
                            }
                            else
                            {
                                forcedHigh = true;
                            }
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "26A":
                    switch (_selected)
                    {
                        case "Yes":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;
                            if (hasExample) score += 2;
                            break;
                        case "No":
                            score++;
                            break;
                        default:
                            break;
                    }
                    break;
                case "27A1":
                    int? total = GetInt(GetVal(q, qNo, "A1", true)) + GetInt(GetVal(q, qNo, "A2", true)) + GetInt(GetVal(q, qNo, "A3", true));
                    if (total == 0 && total != null)
                    {
                        score += 2;
                    }
                    else
                    {
                        int? count0 = 0;
                        if (GetInt(GetVal(q, qNo, "A1", true)) == 0) count0++;
                        if (GetInt(GetVal(q, qNo, "A2", true)) == 0) count0++;
                        if (GetInt(GetVal(q, qNo, "A3", true)) == 0) count0++;

                        if (count0 > 1)
                        {
                            score++;
                        }
                    }
                    break;
                case "27B1":
                    int? _total = GetInt(GetVal(q, qNo, "B1", true)) + GetInt(GetVal(q, qNo, "B2", true)) + GetInt(GetVal(q, qNo, "B3", true));
                    if (_total == 0 && _total != null)
                    {
                        score += 2;
                    }
                    else
                    {
                        int? count0 = 0;
                        if (GetInt(GetVal(q, qNo, "B1", true)) == 0) count0++;
                        if (GetInt(GetVal(q, qNo, "B2", true)) == 0) count0++;
                        if (GetInt(GetVal(q, qNo, "B3", true)) == 0) count0++;

                        if (count0 > 1)
                        {
                            score++;
                        }
                    }
                    break;
                case "27C1":
                    int? _27C = 0;
                    _27C = GetInt(GetVal(q, qNo, "C3", true));
                    int? _27E = 0;
                    _27E = GetInt(GetVal(q, qNo, "E3", true));
                    if (_27E != 0 & _27C != 0)
                    {
                        Double _27C_27E = (Convert.ToDouble(_27C) / Convert.ToDouble(_27E));
                        Double _1 = 0.000025d;
                        Double _2 = 0.0005;

                        if (_27C_27E < _1)
                        {
                            score += 2;
                        }
                        else
                        {
                            if (_27C_27E > 1 && _27C_27E < _2)
                            {
                                score++;
                            }
                        }

                    }
                    else
                    {
                        if (_27C == 0)
                        {
                            score += 2;
                        }
                    }
                    break;
                case "27D1":
                    Double _27A = 0;
                    _27A = Convert.ToDouble(GetInt(GetVal(q, qNo, "A3", true)));
                    Double _27B = 0;
                    _27B = Convert.ToDouble(GetInt(GetVal(q, qNo, "B3", true)));
                    Double _27C_2 = 0;
                    _27C_2 = Convert.ToDouble(GetInt(GetVal(q, qNo, "C3", true)));
                    Double _27D = 0;
                    _27D = Convert.ToDouble(GetInt(GetVal(q, qNo, "D3", true)));

                    if (_27D < (5 * (_27A + _27B + _27C_2))) score++;
                    if (_27D >= (5 * (_27A + _27B + _27C_2))) score += 2;
                    break;
                case "27E1":
                    int? count03 = 0;
                    if (GetInt(GetVal(q, qNo, "E1", true)) != null) count03++;
                    if (GetInt(GetVal(q, qNo, "E2", true)) != null) count03++;
                    if (GetInt(GetVal(q, qNo, "E3", true)) != null) count03++;
                    if (count03 == 3) score += 2;
                    break;
                //TODO: 27A.b.c.d.e go here.

                case "29iA":
                    switch (_selected)
                    {
                        case "Yes":
                            score++;
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "29iB":
                    switch (_selected)
                    {
                        case "Yes":
                            forcedHigh = true;
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "29iC":
                    switch (_selected)
                    {
                        case "Yes":
                            score++;
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "29iD":
                    switch (_selected)
                    {
                        case "Yes":
                            forcedHigh = true;
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                case "29iiA":
                    switch (_selected)
                    {
                        case "Yes":
                            if (HasAttachment(q, qNo, selected) == "Yes") hasExample = true;
                            if (hasExample && !String.IsNullOrEmpty(HasComments(q, qNo))) score++;
                            break;
                        case "No":
                            score += 2;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            return score;
        }
        else
        {
            return 0;
        }
    }

    public string GetQuestionText(string qNo)
    {
        QuestionnaireMainQuestionService qmqService = new QuestionnaireMainQuestionService();
        QuestionnaireMainQuestion qmq = qmqService.GetByQuestionNo(qNo);
        return qmq.QuestionText;
    }
    public string GetAnswerText(string qNo, string selected)
    {
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        QuestionnaireMainAnswer qma = qmaService.GetByQuestionNoAnswerNo(qNo, selected);
        if (qNo.Contains("27")) selected = selected.Replace("1", "");
        return selected + ") " + qma.AnswerText;
    }

    public string HasComments(int q, string qNo)
    {
        string hasComments = "";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        QuestionnaireMainAnswer qma = qmaService.GetByQuestionNoAnswerNo(qNo, "Comments");
        if (qma != null)
        {
            QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
            QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
            if (qmr != null)
            {
                hasComments = qmr.AnswerText;
            }
        }
        return hasComments;
    }
    public string HasComments(int q, string qNo, string subquestion)
    {
        string hasComments = "";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        QuestionnaireMainAnswer qma = qmaService.GetByQuestionNoAnswerNo(qNo, String.Format("{0}Comments", subquestion));
        if (qma != null)
        {
            QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
            QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
            if (qmr != null)
            {
                hasComments = qmr.AnswerText;
            }
        }
        return hasComments;
    }
    public string HasAttachment(int q, string qNo, string selected)
    {
        string hasAttachment = "n/a";

        switch (qNo)
        {
            case "12":
                if (!String.IsNullOrEmpty(selected)) hasAttachment = HasAttachment2(q, qNo, String.Format("{0}Example", selected));
                break;
            case "17":
                if (!String.IsNullOrEmpty(selected))
                {
                    hasAttachment = HasAttachment2(q, qNo, String.Format("{0}Example", selected));
                }
                else
                {
                    hasAttachment = HasAttachment2(q, qNo, "Example");
                }
                break;
            case "23":
                hasAttachment = HasAttachment2(q, qNo, "Example");
                break;
            case "25":
                if (!String.IsNullOrEmpty(selected)) hasAttachment = HasAttachment2(q, qNo, String.Format("{0}Example", selected));
                break;
            case "26":
                if (!String.IsNullOrEmpty(selected)) hasAttachment = HasAttachment2(q, qNo, String.Format("{0}Example", selected));
                break;
            case "29ii":
                if (!String.IsNullOrEmpty(selected)) hasAttachment = HasAttachment2(q, qNo, String.Format("{0}Example", selected));
                break;

            default:
                break;
        }
        return hasAttachment;
    }

    public string HasAttachment2(int q, string qNo, string AnswerNo)
    {
        string hasAttachment = "n/a";
        QuestionnaireMainAnswerService qmaService = new QuestionnaireMainAnswerService();
        QuestionnaireMainAnswer qma = qmaService.GetByQuestionNoAnswerNo(qNo, AnswerNo);

        if (qma != null)
        {
            QuestionnaireMainAttachmentService qmatService = new QuestionnaireMainAttachmentService();
            QuestionnaireMainAttachment qmat = qmatService.GetByQuestionnaireIdAnswerId(q, qma.AnswerId);
            if (qmat != null)
            {
                hasAttachment = "Yes";
            }
            else
            {
                hasAttachment = "No";
            }
        }
        return hasAttachment;
    }

    protected void grid_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.GetValue("Score") != null)
        {
            if (e.GetValue("ForcedHigh") != null)
            {
                if (!String.IsNullOrEmpty(e.GetValue("ForcedHigh").ToString()))
                {
                    if (e.GetValue("ForcedHigh").ToString() == "Yes") e.Row.Font.Bold = true;
                }
            }
            if (!String.IsNullOrEmpty(e.GetValue("Score").ToString()))
            {
                int score = 0;
                score = Convert.ToInt32(e.GetValue("Score").ToString());
                if (score < 1)
                {
                    e.Row.ForeColor = System.Drawing.Color.Red;

                }
            }
        }
    }

    protected int? GetInt(object o)
    {
        try
        {
            return Convert.ToInt32(o.ToString());
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            return null;
        }
    }
    protected Decimal? GetDec(object o)
    {
        try
        {
            return Convert.ToDecimal(o.ToString());
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            return null;
        }
    }
}
