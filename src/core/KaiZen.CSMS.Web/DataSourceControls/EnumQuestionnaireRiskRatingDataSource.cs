﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EnumQuestionnaireRiskRatingProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EnumQuestionnaireRiskRatingDataSourceDesigner))]
	public class EnumQuestionnaireRiskRatingDataSource : ProviderDataSource<EnumQuestionnaireRiskRating, EnumQuestionnaireRiskRatingKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingDataSource class.
		/// </summary>
		public EnumQuestionnaireRiskRatingDataSource() : base(new EnumQuestionnaireRiskRatingService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EnumQuestionnaireRiskRatingDataSourceView used by the EnumQuestionnaireRiskRatingDataSource.
		/// </summary>
		protected EnumQuestionnaireRiskRatingDataSourceView EnumQuestionnaireRiskRatingView
		{
			get { return ( View as EnumQuestionnaireRiskRatingDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EnumQuestionnaireRiskRatingDataSource control invokes to retrieve data.
		/// </summary>
		public EnumQuestionnaireRiskRatingSelectMethod SelectMethod
		{
			get
			{
				EnumQuestionnaireRiskRatingSelectMethod selectMethod = EnumQuestionnaireRiskRatingSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EnumQuestionnaireRiskRatingSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EnumQuestionnaireRiskRatingDataSourceView class that is to be
		/// used by the EnumQuestionnaireRiskRatingDataSource.
		/// </summary>
		/// <returns>An instance of the EnumQuestionnaireRiskRatingDataSourceView class.</returns>
		protected override BaseDataSourceView<EnumQuestionnaireRiskRating, EnumQuestionnaireRiskRatingKey> GetNewDataSourceView()
		{
			return new EnumQuestionnaireRiskRatingDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EnumQuestionnaireRiskRatingDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EnumQuestionnaireRiskRatingDataSourceView : ProviderDataSourceView<EnumQuestionnaireRiskRating, EnumQuestionnaireRiskRatingKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EnumQuestionnaireRiskRatingDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EnumQuestionnaireRiskRatingDataSourceView(EnumQuestionnaireRiskRatingDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EnumQuestionnaireRiskRatingDataSource EnumQuestionnaireRiskRatingOwner
		{
			get { return Owner as EnumQuestionnaireRiskRatingDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EnumQuestionnaireRiskRatingSelectMethod SelectMethod
		{
			get { return EnumQuestionnaireRiskRatingOwner.SelectMethod; }
			set { EnumQuestionnaireRiskRatingOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EnumQuestionnaireRiskRatingService EnumQuestionnaireRiskRatingProvider
		{
			get { return Provider as EnumQuestionnaireRiskRatingService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EnumQuestionnaireRiskRating> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EnumQuestionnaireRiskRating> results = null;
			EnumQuestionnaireRiskRating item;
			count = 0;
			
			System.Int32 _enumQuestionnaireRiskRatingId;
			System.Int32 _ordinal;

			switch ( SelectMethod )
			{
				case EnumQuestionnaireRiskRatingSelectMethod.Get:
					EnumQuestionnaireRiskRatingKey entityKey  = new EnumQuestionnaireRiskRatingKey();
					entityKey.Load(values);
					item = EnumQuestionnaireRiskRatingProvider.Get(entityKey);
					results = new TList<EnumQuestionnaireRiskRating>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EnumQuestionnaireRiskRatingSelectMethod.GetAll:
                    results = EnumQuestionnaireRiskRatingProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EnumQuestionnaireRiskRatingSelectMethod.GetPaged:
					results = EnumQuestionnaireRiskRatingProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EnumQuestionnaireRiskRatingSelectMethod.Find:
					if ( FilterParameters != null )
						results = EnumQuestionnaireRiskRatingProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EnumQuestionnaireRiskRatingProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EnumQuestionnaireRiskRatingSelectMethod.GetByEnumQuestionnaireRiskRatingId:
					_enumQuestionnaireRiskRatingId = ( values["EnumQuestionnaireRiskRatingId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EnumQuestionnaireRiskRatingId"], typeof(System.Int32)) : (int)0;
					item = EnumQuestionnaireRiskRatingProvider.GetByEnumQuestionnaireRiskRatingId(_enumQuestionnaireRiskRatingId);
					results = new TList<EnumQuestionnaireRiskRating>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EnumQuestionnaireRiskRatingSelectMethod.GetByOrdinal:
					_ordinal = ( values["Ordinal"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Ordinal"], typeof(System.Int32)) : (int)0;
					item = EnumQuestionnaireRiskRatingProvider.GetByOrdinal(_ordinal);
					results = new TList<EnumQuestionnaireRiskRating>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EnumQuestionnaireRiskRatingSelectMethod.Get || SelectMethod == EnumQuestionnaireRiskRatingSelectMethod.GetByEnumQuestionnaireRiskRatingId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EnumQuestionnaireRiskRating entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EnumQuestionnaireRiskRatingProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EnumQuestionnaireRiskRating> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EnumQuestionnaireRiskRatingProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EnumQuestionnaireRiskRatingDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EnumQuestionnaireRiskRatingDataSource class.
	/// </summary>
	public class EnumQuestionnaireRiskRatingDataSourceDesigner : ProviderDataSourceDesigner<EnumQuestionnaireRiskRating, EnumQuestionnaireRiskRatingKey>
	{
		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingDataSourceDesigner class.
		/// </summary>
		public EnumQuestionnaireRiskRatingDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EnumQuestionnaireRiskRatingSelectMethod SelectMethod
		{
			get { return ((EnumQuestionnaireRiskRatingDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EnumQuestionnaireRiskRatingDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EnumQuestionnaireRiskRatingDataSourceActionList

	/// <summary>
	/// Supports the EnumQuestionnaireRiskRatingDataSourceDesigner class.
	/// </summary>
	internal class EnumQuestionnaireRiskRatingDataSourceActionList : DesignerActionList
	{
		private EnumQuestionnaireRiskRatingDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireRiskRatingDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EnumQuestionnaireRiskRatingDataSourceActionList(EnumQuestionnaireRiskRatingDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EnumQuestionnaireRiskRatingSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EnumQuestionnaireRiskRatingDataSourceActionList
	
	#endregion EnumQuestionnaireRiskRatingDataSourceDesigner
	
	#region EnumQuestionnaireRiskRatingSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EnumQuestionnaireRiskRatingDataSource.SelectMethod property.
	/// </summary>
	public enum EnumQuestionnaireRiskRatingSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEnumQuestionnaireRiskRatingId method.
		/// </summary>
		GetByEnumQuestionnaireRiskRatingId,
		/// <summary>
		/// Represents the GetByOrdinal method.
		/// </summary>
		GetByOrdinal
	}
	
	#endregion EnumQuestionnaireRiskRatingSelectMethod

	#region EnumQuestionnaireRiskRatingFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingFilter : SqlFilter<EnumQuestionnaireRiskRatingColumn>
	{
	}
	
	#endregion EnumQuestionnaireRiskRatingFilter

	#region EnumQuestionnaireRiskRatingExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingExpressionBuilder : SqlExpressionBuilder<EnumQuestionnaireRiskRatingColumn>
	{
	}
	
	#endregion EnumQuestionnaireRiskRatingExpressionBuilder	

	#region EnumQuestionnaireRiskRatingProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EnumQuestionnaireRiskRatingChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireRiskRating"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireRiskRatingProperty : ChildEntityProperty<EnumQuestionnaireRiskRatingChildEntityTypes>
	{
	}
	
	#endregion EnumQuestionnaireRiskRatingProperty
}

