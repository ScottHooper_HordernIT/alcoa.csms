using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using System.Data.Sql;
using System.Data.SqlClient;


public partial class Adminv2_UserControls_Users : System.Web.UI.UserControl
{
    Auth auth = new Auth();



    protected void Page_Load(object sender, EventArgs e) 
    {
       // grid.Visible = false;
        UsersService us = new UsersService();
        bool chkDisablUser = chkDisabledUser.Checked;

        if (chkDisablUser == true)
        {
            firstgrid.Attributes.Add("style", "display:block");

            secondgrid.Attributes.Add("style", "display:none");
        }
        else {
        
            secondgrid.Attributes.Add("style", "display:block");

            firstgrid.Attributes.Add("style", "display:none");
        }


        ASPxGridView1.DataSourceID = "UsersDataSource1";
        ASPxGridView1.DataBind();

        grid.DataSourceID = "UsersDataSource2";
        grid.DataBind();

        Helper._Auth.PageLoad(auth);
        
        moduleInit(Page.IsPostBack); 
    }

    private void moduleInit(bool postBack)
    {
        Control ucProcurement = LoadControl("~/UserControls/Admin/UsersProcurementEscalation.ascx");
        phProcurement.Controls.Add(ucProcurement);

        Control ucHsAssessor = LoadControl("~/UserControls/Admin/UsersHsAssessorEscalation.ascx");
        phHsAssessor.Controls.Add(ucHsAssessor);

        Control ucSafetyQualificationReadAccess = LoadControl("~/UserControls/Admin/UsersSafetyQualificationReadAccess.ascx");
        phSafetyQualificationReadAccess.Controls.Add(ucSafetyQualificationReadAccess);

        Control ucFinancialReportingAccess = LoadControl("~/UserControls/Admin/UsersFinancialReportingAccess.ascx");
        phFinancialReportingAccess.Controls.Add(ucFinancialReportingAccess);

        Control ucEbi = LoadControl("~/UserControls/Admin/UsersEbi.ascx");
        phEbiDataAccess.Controls.Add(ucEbi);

        Control ucTrainingPackageTrainer = LoadControl("~/UserControls/Admin/UsersTrainingPackageTrainers.ascx");
        phTrainingPackageTrainer.Controls.Add(ucTrainingPackageTrainer);

        Control ucViewEngineeringProjectHours = LoadControl("~/UserControls/Admin/UsersViewEngineeringProjectHours.ascx");
        phViewEngineeringProjectHours.Controls.Add(ucViewEngineeringProjectHours);
        

        if (!Page.IsPostBack)
        {
            chkDisabledUser.Checked = true;
            cbDefaultEhsConsultant_WAO.DataSourceID = "UsersEhsConsultantsDataSource";
            cbDefaultEhsConsultant_WAO.DataBind();
            cbDefaultEhsConsultant_WAO.Items.Add("(No One)", "");

            cbDefaultEhsConsultant_VICOPS.DataSourceID = "UsersEhsConsultantsDataSource";
            cbDefaultEhsConsultant_VICOPS.DataBind();
            cbDefaultEhsConsultant_VICOPS.Items.Add("(No One)", "");

            ConfigService cService = new ConfigService();
            Config c = cService.GetByKey("DefaultEhsConsultant_WAO");
            if (!String.IsNullOrEmpty(c.Value))
            {
                cbDefaultEhsConsultant_WAO.Value = c.Value;
            }
            else
            {
                cbDefaultEhsConsultant_WAO.Value = "";
                cbDefaultEhsConsultant_WAO.Text = "(No One)";
            }

            Config c2 = cService.GetByKey("DefaultEhsConsultant_VICOPS");
            if (!String.IsNullOrEmpty(c2.Value))
            {
                cbDefaultEhsConsultant_VICOPS.Value = c2.Value;
            }
            else
            {
                cbDefaultEhsConsultant_VICOPS.Value = "";
                cbDefaultEhsConsultant_VICOPS.Text = "(No One)";
            }
        }

        if (!postBack)
        { //first time load
            if (auth.RoleId == (int)RoleList.Administrator)
            {
                //

            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }
    }

   
    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;

    }
    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        if(Convert.ToInt32(e.NewValues["CompanyId"]) != 0)
        {
            QuestionnaireService qService0 = new QuestionnaireService();
            DataSet qDataSet = qService0.GetLastModifiedQuestionnaireId(Convert.ToInt32(e.NewValues["CompanyId"]));
            if (qDataSet.Tables[0] == null) throw new Exception("Company does not appear to have Safety Questionnaire. Cannot Add User.");
            if (qDataSet.Tables[0].Rows.Count != 1) throw new Exception("Company does not appear to have Safety Questionnaire. Cannot Add User.");

            string LatestQuestionnaireId = qDataSet.Tables[0].Rows[0].ItemArray[0].ToString();

            if (!String.IsNullOrEmpty(LatestQuestionnaireId))
            {
                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire q = qService.GetByQuestionnaireId(Convert.ToInt32(LatestQuestionnaireId));
                if (q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.CsmsCreateCompanyLogins)
                {
                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;
                    q.QuestionnairePresentlyWithSince = DateTime.Now;
                    qService.Save(q);
                }
            }

            
        }
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        if (e.NewValues["ModifiedByUserId"] == null) { e.NewValues["ModifiedByUserId"] = auth.UserId; }
        e.NewValues["UserLogon"] = "AUSTRALIA_ASIA\\";
    }

    //added by subhro

    protected void grid_RowUpdating1(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;

    }
    protected void grid_RowInserting1(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        if (Convert.ToInt32(e.NewValues["CompanyId"]) != 0)
        {
            QuestionnaireService qService0 = new QuestionnaireService();
            DataSet qDataSet = qService0.GetLastModifiedQuestionnaireId(Convert.ToInt32(e.NewValues["CompanyId"]));
            if (qDataSet.Tables[0] == null) throw new Exception("Company does not appear to have Safety Questionnaire. Cannot Add User.");
            if (qDataSet.Tables[0].Rows.Count != 1) throw new Exception("Company does not appear to have Safety Questionnaire. Cannot Add User.");

            string LatestQuestionnaireId = qDataSet.Tables[0].Rows[0].ItemArray[0].ToString();

            if (!String.IsNullOrEmpty(LatestQuestionnaireId))
            {
                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire q = qService.GetByQuestionnaireId(Convert.ToInt32(LatestQuestionnaireId));
                if (q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.CsmsCreateCompanyLogins)
                {
                    q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;
                    q.QuestionnairePresentlyWithSince = DateTime.Now;
                    qService.Save(q);
                }
            }


        }
    }
    protected void grid_InitNewRow1(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        if (e.NewValues["ModifiedByUserId"] == null) { e.NewValues["ModifiedByUserId"] = auth.UserId; }
        e.NewValues["UserLogon"] = "AUSTRALIA_ASIA\\";
    }

    //ended by subhro


    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue.ToString() != "All")
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
            grid.SettingsPager.PageSize = Convert.ToInt32(DropDownList1.SelectedValue.ToString());
        }
        else
        {
            grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
        }
        grid.DataBind();
    }

    protected void ASPxButton3_Click(object sender, EventArgs e)
    {
        System.Collections.Generic.List<object> keyValues = grid.GetSelectedFieldValues("Email");
        if (keyValues.Count != 0)
        {
            System.Threading.Thread.Sleep(500);
            string mailTo = "";
            Hashtable ht = new Hashtable();

            foreach (string key in keyValues)
            {
                string add;
                if ((key == null) || (key == "")) { add = ""; }
                else { add = key.ToString(); }

                if (!ht.ContainsKey(add)) //Anti-Duplication
                {
                    ht[add] = 0;
                    if (!String.IsNullOrEmpty(mailTo))
                    {
                        mailTo += ";" + add;
                    }
                    else
                    {
                        mailTo = add;
                    }
                    //lblEmailList.Text += "<br />" + add + ",";
                }
            }
            ASPxMemo1.Text = "mailto:" + mailTo;
            ASPxMemo1.Visible = true;
            ASPxHyperLink1.NavigateUrl = "javascript:popUp2('mailto:" + mailTo + "');";
            Label1.Visible = true;
            //Label1.Visible = true;
            //Response.Redirect("javascript:popUp2('mailto:" + mailTo + "')");
            //Response.Redirect("mailto:" + mailTo);
        }
    }
    //protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    //{
    //    //xls
    //    Response.Clear();
    //    ASPxGridViewExporter1.WriteXlsToResponse();
    //    Response.Close();
    //}
    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    //rtf
    //    Response.Clear();
    //    ASPxGridViewExporter1.WriteRtfToResponse();
    //    Response.Close();
    //}
    //protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    //{
    //    //pdf
    //    Response.Clear();
    //    ASPxGridViewExporter1.WritePdfToResponse();
    //    Response.Close();
    //}
    protected void btnSaveDefaultEhsConsultants_Click(object sender, EventArgs e)
    {
        ConfigService cService = new ConfigService();
        Config c = cService.GetByKey("DefaultEhsConsultant_WAO");
        string email = "";
        if (cbDefaultEhsConsultant_WAO.Value != null) email = cbDefaultEhsConsultant_WAO.Value.ToString();
        c.Value = email;
        cService.Save(c);

        Config c2 = cService.GetByKey("DefaultEhsConsultant_VICOPS");
        string email2 = "";
        if (cbDefaultEhsConsultant_VICOPS.Value != null) email2 = cbDefaultEhsConsultant_VICOPS.Value.ToString();
        c2.Value = email2;
        cService.Save(c2);
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        //int errorCount = 0;
        string UserLogon = string.Empty;
        try
        {
            //if (String.IsNullOrEmpty(Convert.ToString(ASPxComboBox1.SelectedIndex == null))) { errorCount++; };
            UserLogon = KaiZen.CSMS.Encryption.Crypt.EncryptTripleDES(ASPxComboBox1.SelectedItem.Text, "AlcoaSecretKey");
        }

        catch (Exception ex)
        {
            //ToDo: Handle Exception
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //errorCount = 1;
            PopupWindow pcWindow = new PopupWindow("You haven't selected a user.");
            pcWindow.FooterText = "";
            pcWindow.ShowOnPageLoad = true;
            pcWindow.Modal = true;
            ASPxPopupControl1.Windows.Add(pcWindow);
            return;
        }

        //if (errorCount > 0)
        //{
        //    return;
        //}
        System.Threading.Thread.Sleep(500);
        UserLogon = KaiZen.CSMS.Encryption.Crypt.EncryptTripleDES(ASPxComboBox1.SelectedItem.Text, "AlcoaSecretKey");
        string _url = "awaproxy/";
        if (Request.Url.AbsoluteUri.Contains("awaproxy/")) _url = "";

        SessionHandler.LoggedIn = "No";
        Response.Buffer = true;
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
        Response.Expires = -1500;
        Response.CacheControl = "no-cache";
        Response.Cache.SetNoStore();
        Session.Clear();
        Session.Abandon();

        string redirUrl = _url + "login.aspx?" + auth._AlcoaDirectHTTPParameter + "=" + UserLogon;
        //ASPxLabel2.Text = redirUrl;
        Response.Redirect(redirUrl);

    }

}