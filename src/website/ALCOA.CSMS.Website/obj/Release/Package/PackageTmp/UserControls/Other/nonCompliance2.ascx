﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Other_nonCompliance2" Codebehind="nonCompliance2.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>    
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.State" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" TagPrefix="dx" %>
<%@ Register Src="userProfile2.ascx" TagName="userProfile2" TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.State" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.1" namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" tagprefix="dx" %>
<asp:Panel ID="PanelPreQual" runat="server" Width="680px" Visible="false">
    <div align="left" style="border-right: #003399 1px solid; padding-right: 3px; border-top: #003399 1px solid;
        padding-left: 3px; padding-bottom: 3px; margin-left: 0px; border-left: #003399 1px solid;
        padding-top: 3px; border-bottom: #003399 1px solid; background-color: #e9efff;
        height: 320px;">
        <div align="center">
            <strong><span style="font-size: 14px; color: #003399; font-family: Arial">Safety Qualification</span></strong>
        </div>
        <br />
        <div style="padding-right: 5px; padding-left: 5px; height: 13px; text-align: left;
            position: relative; float: left;">
            Welcome to the Alcoa Contractor Management Portal and our Contractor Safety Qualification
            Process.<br />
            <br />
            This process requires you to complete a Risk questionnaire online and upon completion
            the system will automatically categorise your company in accordance with Alcoa corporate
            requirements.<br />
            <br />
            Please take a few minutes to read the Safety Qualification Process Overview and
            Quick Reference Guide on how to use the system by clicking on the links below.<br />
            &nbsp;<br />
            &nbsp;<br />
            <div align="center" style="margin-left: auto; margin-right: auto;">
                <dx:ASPxButton ID="ASPxButton2" runat="server" PostBackUrl="javascript:popUp('PopUps/SafetyPQ_ProcessOverview.aspx');"
                    Text="Process Overview" Width="280px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                </dx:ASPxButton>
                <br />
                <br />
                <dx:ASPxButton ID="ASPxButton4" runat="server" PostBackUrl="~/SafetyPQ_Questionnaire.aspx"
                    Text="Online Questionnaire" Width="280px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                </dx:ASPxButton>
                <br />
                <br />
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="PanelSubContractor" runat="server" Width="680px" Visible="false">
    <div align="left" style="border-right: #003399 1px solid; padding-right: 3px; border-top: #003399 1px solid;
        padding-left: 3px; padding-bottom: 3px; margin-left: 0px; border-left: #003399 1px solid;
        padding-top: 3px; border-bottom: #003399 1px solid; background-color: #e9efff;
        height: 320px;">
        <div align="center">
            <strong><span style="font-size: 14px; color: #003399; font-family: Arial">Sub Contractors</span></strong>
        </div>
        <br />
        <div style="padding-right: 5px; padding-left: 5px; height: 13px; text-align: left;
            position: relative; float: left;">
            Welcome to the Alcoa Contractor Management Portal and our Subcontractor Safety Qualification
            Process.<br />
            <br />
            This process requires you to complete a Risk questionnaire online and upon completion
            the system will automatically categorise your company in accordance with Alcoa corporate
            requirements.<br />
            <br />
            Please take a few minutes to read the Safety Pre-Qualification Process Overview
            and Quick Reference Guide on how to use the system by clicking on the links below.<br />
            &nbsp;<br />
            &nbsp;<br />
            <div align="center" style="margin-left: auto; margin-right: auto;">
                <dx:ASPxButton ID="ASPxButton5" runat="server" PostBackUrl="javascript:popUp('PopUps/SafetyPQ_ProcessOverview.aspx');"
                    Text="Process Overview" Width="280px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                </dx:ASPxButton>
                <br />
                <br />
                <dx:ASPxButton ID="ASPxButton7" runat="server" PostBackUrl="~/SafetyPQ_Questionnaire.aspx"
                    Text="Online Questionnaire" Width="280px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                </dx:ASPxButton>
                <br />
                <br />
            </div>
        </div>
        <br />
        <br />
    </div>
</asp:Panel>
<dx:ASPxPanel ID="PanelAll" runat="server" Width="680px">
    <PanelCollection>
        <dx:PanelContent>
            <table align="left" style="border-right: #003399 1px solid; padding-right: 3px; border-top: #003399 1px solid;
                padding-left: 3px; padding-bottom: 3px; margin-left: 0px; border-left: #003399 1px solid;
                padding-top: 3px; border-bottom: #003399 1px solid; background-color: #e9efff;
                height: 50px;">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <strong><span style="font-size: 14px; color: #003399; font-family: Arial">&nbsp;Health Check</span></strong>
                                </td>
                                <td>
                                    <dx:ASPxComboBox ID="ddlCompanies" runat="server" ValueType="System.Int32" Width="318px"
                                        AutoPostBack="True" IncrementalFilteringMode="StartsWith" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged1"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ButtonStyle Width="13px">
                                        </ButtonStyle>
                                    </dx:ASPxComboBox>
                                </td>
                                <td>
                                    <dx:ASPxComboBox ID="ddlYear" runat="server" ValueType="System.Int32" Width="57px"
                                        AutoPostBack="True" IncrementalFilteringMode="StartsWith" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged1"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" EnableViewState="true">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ButtonStyle Width="13px">
                                        </ButtonStyle>
                                    </dx:ASPxComboBox>
                                    <asp:SqlDataSource ID="sqldsNoSubmitted" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                                        SelectCommand="_Kpi_HealthCheck_NoSubmitted" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spVar_CompanyId"
                                                Type="Int32" />
                                            <asp:ControlParameter ControlID="ddlYear" DefaultValue="2007" Name="CURRENTYEAR"
                                                PropertyName="Value" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </td>
                                <td style="width: 175px">
                                    <dx:ASPxButton ID="btnEnterKpi" runat="server" Visible="false" Text="Enter Monthly KPI's data"
                                        PostBackUrl="~/KPI.aspx" Width="160px">
                                    </dx:ASPxButton>
                                </td>
                                <td style="padding-left: 5px">
                                    <div id="divNews" runat="server">
                                    <a href="news.aspx">
                                        <dx:ASPxImage ID="imgNews" ClientInstanceName="imgNews" runat="server" ImageUrl="~/Images/newsBlue.png"
                                            IsPng="True">
                                        </dx:ASPxImage></a></div>
                                </td>
                            </tr>
                        </table>
                        <div style="position: relative">
                            <div style="position: relative">
                                <dx:ASPxPanel ID="pnlHealthCheck" runat="server" Width="100%" Height="100%" Visible="false">
                                    <PanelCollection>
                                        <dx:PanelContent ID="PanelContent1" runat="server">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: right">
                                                        <strong>Company Type:</strong>
                                                    </td>
                                                    <td>
                                                        &nbsp;<dx:ASPxLabel ID="lblCompanyType" runat="server" Text="-">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td style="text-align: right; padding-left: 10px;">
                                                        <strong>Supervision Requirement:</strong>
                                                    </td>
                                                    <td>
                                                        &nbsp;<dx:ASPxLabel ID="lblSupervisionRequirement" runat="server" Text="-" ForeColor="Red">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td style="text-align: right; padding-left: 20px;">
                                                        <dx:ASPxHyperLink ID="hlDefinitions" runat="server" Text="Supervision Definition"
                                                            NavigateUrl="javascript:popUp('PopUps/Library_SupervisionRequirements.aspx');"
                                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                        </dx:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                <tr>
                                                    <td style="width: 430px; text-align: left">
                                                        <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                                                            style="width: 100%; border-collapse: collapse; empty-cells: show;">
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_SQ" runat="server" Text="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_SQ" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_SQ" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <asp:Panel ID="trNote" Visible=false runat=server>
                                                            <tr>
                                                            <td style="width:48px;height:13px;align="right"></td>
                                                            <td style="width:30px"></td>
                                                            <td style="text-align:left">
                                                            <%--Added by Vikas for Change DT2443--%>
                                                                <font style="color:#1F497D;font-size:smaller">  Technical Professional(office duties only)/Low Level 
                                                                Delivery<br><b>NOTE</b>:If scope changes,must complete Pre-Qualification 
                                                                </font> <%--End DT2443--%>
                                                            </td>
                                                            </tr>
                                                            </asp:Panel>
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_SC" runat="server" Text="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_SC" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_SC" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr id="Plan" runat="server">
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_SMP" runat="server" Text="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_SMP" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_SMP" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_MRD" runat="server" Text="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_MRD" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_MRD" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_CC" runat="server" Text="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_CC" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>

<LayoutPadding All="0" Left="0" Top="0" Right="0" Bottom="0"></LayoutPadding>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_CC" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlCSR" runat="server" Text="View" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblCSR2" runat="server" Text="Compliance Summary Report">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5px">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 247px; text-align: left">
                                                        <uc1:userProfile2 ID="userProfile21" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="text-align: right; padding-top: 2px;">
                                            </div>
                                            <br />
                                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="False" OnHtmlRowCreated="grid_HtmlRowCreated"
                                                EnableCallBacks="false" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared">

<SettingsText EmptyDataRow="No Sites currently Allocated for this Company."></SettingsText>

                                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                    </LoadingPanelOnStatusBar>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                    </LoadingPanel>
                                                </Images>
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Residential Category" FieldName="CompanySiteCategoryDesc"
                                                        VisibleIndex="10" Width="140px">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Site Access Approval" FieldName="SiteAccessApproved"
                                                        VisibleIndex="12">
                                                        <CellStyle HorizontalAlign="Center" />
<CellStyle HorizontalAlign="Center"></CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewBandColumn Caption="Safety Compliance Performance" VisibleIndex="0">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Site" FieldName="SiteName" VisibleIndex="0" Width="70px">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewBandColumn Caption="Month Safety Compliance Score" VisibleIndex="1">
                                                                <Columns>
                                                                    <dx:GridViewDataTextColumn Caption="Month" FieldName="SafetyComplianceScore_LastMonth"
                                                                        Name="PrevMonth" VisibleIndex="0" Width="70px">
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                    <dx:GridViewDataTextColumn Caption="YTD" FieldName="SafetyComplianceScore_Ytd" VisibleIndex="1"
                                                                        Width="70px">
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dx:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dx:GridViewBandColumn>
                                                            <dx:GridViewDataTextColumn FieldName="MandatedTraining_Ytd"
                                                                VisibleIndex="2" Width="60px">
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="CSA (Prev. Period)" FieldName="Csa_LastQtr" VisibleIndex="3">
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="KPI Compliance Radar" VisibleIndex="4" Width="50px">
                                                                <DataItemTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td style="text-align: center; width: 100%">
                                                                                <asp:HyperLink ID="hlRadar" runat="server" NavigateUrl="" Text="Radar"></asp:HyperLink>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </DataItemTemplate>
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewBandColumn Caption="Safety Statistics" VisibleIndex="6">
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="LWDR" VisibleIndex="0">
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="TRIR" VisibleIndex="1">
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn FieldName="AIFR" VisibleIndex="2">
                                                                <CellStyle HorizontalAlign="Center">
                                                                </CellStyle>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:GridViewBandColumn>
                                                    <dx:GridViewDataTextColumn Caption="Comments" FieldName="Comments" ShowInCustomizationForm="True" Visible="False" VisibleIndex="13">
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />

<SettingsBehavior AllowDragDrop="False" AllowSort="False" AllowGroup="False"></SettingsBehavior>

                                                <SettingsPager Mode="ShowAllRecords">
                                                </SettingsPager>
                                                <ImagesFilterControl>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                    </LoadingPanel>
                                                </ImagesFilterControl>
                                                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                    <Header ImageSpacing="5px" SortingImageSpacing="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                        Wrap="True">
                                                    </Header>
                                                    <LoadingPanel ImageSpacing="10px">
                                                    </LoadingPanel>
                                                </Styles>
                                                <SettingsText EmptyDataRow="No Sites currently Allocated for this Company." />
                                                <StylesEditors>
                                                    <ProgressBar Height="25px">
                                                    </ProgressBar>
                                                </StylesEditors>
                                            </dx:ASPxGridView>
                                            <br />
                                            <table>
                                            <tr>
                                            <td>
                                                <dx:ASPxHyperLink ID="hlRadar" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" runat="server" Text="Additional Statistics By Region and State"
                                                    NavigateUrl="javascript:void(0);">
                                                </dx:ASPxHyperLink>
                                            </td>
                                            <td> | </td>
                                            <td>
                                                <dx:ASPxHyperLink ID="hlArpCrp" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" runat="server" Text="View ARP/CRP listing"
                                                    >
                                                </dx:ASPxHyperLink>
                                                <%--<dx:ASPxHyperLink ID="hlArpCrp" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                    CssPostfix="Office2003Blue" runat="server" Text="View ARP/CRP "
                                                    NavigateUrl="javascript:popUp('PopUps/ArpCrpHealthPopup.aspx?Help=Default');">
                                                </dx:ASPxHyperLink>--%>
                                            </td>
                                            </tr>
                                            </table>
                                            <%--<dx:ASPxHyperLink ID="hlRadar" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" runat="server" Text="Additional Statistics By Region and State"
                                                NavigateUrl="javascript:void(0);">
                                            </dx:ASPxHyperLink>--%>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxPanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </dx:PanelContent>
    </PanelCollection>
</dx:ASPxPanel>

<%--Added by Vikas for Change DT2443--%>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList_SubContractors" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameListInSqDatabase_SubContractors" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsActiveCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_Companies_GetActiveCompanyNameList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

