﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminAuditScoreService
    {
        AdminAuditScore Get(Expression<Func<AdminAuditScore, bool>> where, List<Expression<Func<AdminAuditScore, object>>> includes);
        List<AdminAuditScore> GetMany(int? top, Expression<Func<AdminAuditScore, bool>> where, Func<IQueryable<AdminAuditScore>, IOrderedQueryable<AdminAuditScore>> orderBy, List<Expression<Func<AdminAuditScore, object>>> includes);
        List<AdminAuditScore> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminAuditScore, bool>> where, Func<IQueryable<AdminAuditScore>, IOrderedQueryable<AdminAuditScore>> orderBy, List<Expression<Func<AdminAuditScore, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminAuditScore, bool>> where, List<Expression<Func<AdminAuditScore, object>>> includes);
        void Insert(AdminAuditScore item, bool saveChanges = true);
        void Update(AdminAuditScore item, bool saveChanges = true);
        void Delete(AdminAuditScore item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminAuditScoreService : IAdminAuditScoreService
    {
        private readonly IRepository<AdminAuditScore> _repository;

        public AdminAuditScoreService(IRepository<AdminAuditScore> repository)
        {
            this._repository = repository;
        }

        public AdminAuditScore Get(Expression<Func<AdminAuditScore, bool>> where, List<Expression<Func<AdminAuditScore, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminAuditScore> GetMany(int? top, Expression<Func<AdminAuditScore, bool>> where, Func<IQueryable<AdminAuditScore>, IOrderedQueryable<AdminAuditScore>> orderBy, List<Expression<Func<AdminAuditScore, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminAuditScore> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminAuditScore, bool>> where, Func<IQueryable<AdminAuditScore>, IOrderedQueryable<AdminAuditScore>> orderBy, List<Expression<Func<AdminAuditScore, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminAuditScore, bool>> where, List<Expression<Func<AdminAuditScore, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminAuditScore item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminAuditScore");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminAuditScore item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminAuditScore");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminAuditScore item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminAuditScore");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
