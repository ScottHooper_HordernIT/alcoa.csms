﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailTemplateRecipientProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskEmailTemplateRecipientProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipientKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipientKey key)
		{
			return Delete(transactionManager, key.AdminTaskEmailTemplateRecipientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateRecipientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskEmailTemplateRecipientId)
		{
			return Delete(null, _adminTaskEmailTemplateRecipientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateRecipientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateRecipientId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(_adminTaskEmailTemplateId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(transactionManager, _adminTaskEmailTemplateId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(transactionManager, _adminTaskEmailTemplateId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate key.
		///		fkAdminTaskEmailTemplateRecipientAdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskEmailTemplateId(null, _adminTaskEmailTemplateId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate key.
		///		fkAdminTaskEmailTemplateRecipientAdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskEmailTemplateId(null, _adminTaskEmailTemplateId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailTemplate Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public abstract TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient Description: 
		/// </summary>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailRecipientId(System.Int32 _adminTaskEmailRecipientId)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(_adminTaskEmailRecipientId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(transactionManager, _adminTaskEmailRecipientId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(transactionManager, _adminTaskEmailRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient key.
		///		fkAdminTaskEmailTemplateRecipientAdminTaskEmailRecipient Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailRecipientId(System.Int32 _adminTaskEmailRecipientId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAdminTaskEmailRecipientId(null, _adminTaskEmailRecipientId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient key.
		///		fkAdminTaskEmailTemplateRecipientAdminTaskEmailRecipient Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailRecipientId(System.Int32 _adminTaskEmailRecipientId, int start, int pageLength,out int count)
		{
			return GetByAdminTaskEmailRecipientId(null, _adminTaskEmailRecipientId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient key.
		///		FK_AdminTaskEmailTemplateRecipient_AdminTaskEmailRecipient Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public abstract TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId key.
		///		FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId Description: 
		/// </summary>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByCsmsEmailRecipientTypeId(System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientTypeId(_csmsEmailRecipientTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId key.
		///		FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		/// <remarks></remarks>
		public TList<AdminTaskEmailTemplateRecipient> GetByCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByCsmsEmailRecipientTypeId(transactionManager, _csmsEmailRecipientTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId key.
		///		FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsEmailRecipientTypeId(transactionManager, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId key.
		///		fkAdminTaskEmailTemplateRecipientCsmsEmailRecipientTypeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByCsmsEmailRecipientTypeId(System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsEmailRecipientTypeId(null, _csmsEmailRecipientTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId key.
		///		fkAdminTaskEmailTemplateRecipientCsmsEmailRecipientTypeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByCsmsEmailRecipientTypeId(System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength,out int count)
		{
			return GetByCsmsEmailRecipientTypeId(null, _csmsEmailRecipientTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId key.
		///		FK_AdminTaskEmailTemplateRecipient_CsmsEmailRecipientTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient objects.</returns>
		public abstract TList<AdminTaskEmailTemplateRecipient> GetByCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipientKey key, int start, int pageLength)
		{
			return GetByAdminTaskEmailTemplateRecipientId(transactionManager, key.AdminTaskEmailTemplateRecipientId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskEmailTemplateRecipientId index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateRecipientId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateRecipientId(System.Int32 _adminTaskEmailTemplateRecipientId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateRecipientId(null,_adminTaskEmailTemplateRecipientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateRecipientId index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateRecipientId(System.Int32 _adminTaskEmailTemplateRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateRecipientId(null, _adminTaskEmailTemplateRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateRecipientId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateRecipientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateRecipientId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateRecipientId(transactionManager, _adminTaskEmailTemplateRecipientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateRecipientId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateRecipientId(transactionManager, _adminTaskEmailTemplateRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateRecipientId index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateRecipientId(System.Int32 _adminTaskEmailTemplateRecipientId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailTemplateRecipientId(null, _adminTaskEmailTemplateRecipientId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailTemplateRecipientId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateRecipientId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdminTaskEmailTemplateRecipient index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _adminTaskEmailRecipientId, System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(null,_adminTaskEmailTemplateId, _adminTaskEmailRecipientId, _csmsEmailRecipientTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateRecipient index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _adminTaskEmailRecipientId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(null, _adminTaskEmailTemplateId, _adminTaskEmailRecipientId, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _adminTaskEmailRecipientId, System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(transactionManager, _adminTaskEmailTemplateId, _adminTaskEmailRecipientId, _csmsEmailRecipientTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _adminTaskEmailRecipientId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(transactionManager, _adminTaskEmailTemplateId, _adminTaskEmailRecipientId, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateRecipient index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _adminTaskEmailRecipientId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(null, _adminTaskEmailTemplateId, _adminTaskEmailRecipientId, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient GetByAdminTaskEmailTemplateIdAdminTaskEmailRecipientIdCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _adminTaskEmailRecipientId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdminTaskEmailTemplateId_ CsmsEmailRecipientTypeId index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/> class.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(null,_adminTaskEmailTemplateId, _csmsEmailRecipientTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateId_ CsmsEmailRecipientTypeId index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/> class.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(null, _adminTaskEmailTemplateId, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateId_ CsmsEmailRecipientTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/> class.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsEmailRecipientTypeId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(transactionManager, _adminTaskEmailTemplateId, _csmsEmailRecipientTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateId_ CsmsEmailRecipientTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/> class.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(transactionManager, _adminTaskEmailTemplateId, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateId_ CsmsEmailRecipientTypeId index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/> class.</returns>
		public TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(null, _adminTaskEmailTemplateId, _csmsEmailRecipientTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdminTaskEmailTemplateId_ CsmsEmailRecipientTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="_csmsEmailRecipientTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/> class.</returns>
		public abstract TList<AdminTaskEmailTemplateRecipient> GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, System.Int32 _csmsEmailRecipientTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskEmailTemplateRecipient&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskEmailTemplateRecipient&gt;"/></returns>
		public static TList<AdminTaskEmailTemplateRecipient> Fill(IDataReader reader, TList<AdminTaskEmailTemplateRecipient> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskEmailTemplateRecipient")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailTemplateRecipientId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskEmailTemplateRecipient>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskEmailTemplateRecipient",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskEmailTemplateRecipientId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailTemplateRecipientId - 1)];
					c.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailTemplateId - 1)];
					c.AdminTaskEmailRecipientId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailRecipientId - 1)];
					c.CsmsEmailRecipientTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.CsmsEmailRecipientTypeId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskEmailTemplateRecipientId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailTemplateRecipientId - 1)];
			entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailTemplateId - 1)];
			entity.AdminTaskEmailRecipientId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.AdminTaskEmailRecipientId - 1)];
			entity.CsmsEmailRecipientTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateRecipientColumn.CsmsEmailRecipientTypeId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailTemplateRecipientId = (System.Int32)dataRow["AdminTaskEmailTemplateRecipientId"];
			entity.AdminTaskEmailTemplateId = (System.Int32)dataRow["AdminTaskEmailTemplateId"];
			entity.AdminTaskEmailRecipientId = (System.Int32)dataRow["AdminTaskEmailRecipientId"];
			entity.CsmsEmailRecipientTypeId = (System.Int32)dataRow["CsmsEmailRecipientTypeId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdminTaskEmailTemplateIdSource	
			if (CanDeepLoad(entity, "AdminTaskEmailTemplate|AdminTaskEmailTemplateIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskEmailTemplateIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskEmailTemplateId;
				AdminTaskEmailTemplate tmpEntity = EntityManager.LocateEntity<AdminTaskEmailTemplate>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskEmailTemplate), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskEmailTemplateIdSource = tmpEntity;
				else
					entity.AdminTaskEmailTemplateIdSource = DataRepository.AdminTaskEmailTemplateProvider.GetByAdminTaskEmailTemplateId(transactionManager, entity.AdminTaskEmailTemplateId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskEmailTemplateIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskEmailTemplateProvider.DeepLoad(transactionManager, entity.AdminTaskEmailTemplateIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskEmailTemplateIdSource

			#region AdminTaskEmailRecipientIdSource	
			if (CanDeepLoad(entity, "AdminTaskEmailRecipient|AdminTaskEmailRecipientIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskEmailRecipientIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskEmailRecipientId;
				AdminTaskEmailRecipient tmpEntity = EntityManager.LocateEntity<AdminTaskEmailRecipient>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskEmailRecipient), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskEmailRecipientIdSource = tmpEntity;
				else
					entity.AdminTaskEmailRecipientIdSource = DataRepository.AdminTaskEmailRecipientProvider.GetByAdminTaskEmailRecipientId(transactionManager, entity.AdminTaskEmailRecipientId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailRecipientIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskEmailRecipientIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskEmailRecipientProvider.DeepLoad(transactionManager, entity.AdminTaskEmailRecipientIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskEmailRecipientIdSource

			#region CsmsEmailRecipientTypeIdSource	
			if (CanDeepLoad(entity, "CsmsEmailRecipientType|CsmsEmailRecipientTypeIdSource", deepLoadType, innerList) 
				&& entity.CsmsEmailRecipientTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsEmailRecipientTypeId;
				CsmsEmailRecipientType tmpEntity = EntityManager.LocateEntity<CsmsEmailRecipientType>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsEmailRecipientType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsEmailRecipientTypeIdSource = tmpEntity;
				else
					entity.CsmsEmailRecipientTypeIdSource = DataRepository.CsmsEmailRecipientTypeProvider.GetByCsmsEmailRecipientEmailTypeId(transactionManager, entity.CsmsEmailRecipientTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsEmailRecipientTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsEmailRecipientTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsEmailRecipientTypeProvider.DeepLoad(transactionManager, entity.CsmsEmailRecipientTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsEmailRecipientTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdminTaskEmailTemplateIdSource
			if (CanDeepSave(entity, "AdminTaskEmailTemplate|AdminTaskEmailTemplateIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskEmailTemplateIdSource != null)
			{
				DataRepository.AdminTaskEmailTemplateProvider.Save(transactionManager, entity.AdminTaskEmailTemplateIdSource);
				entity.AdminTaskEmailTemplateId = entity.AdminTaskEmailTemplateIdSource.AdminTaskEmailTemplateId;
			}
			#endregion 
			
			#region AdminTaskEmailRecipientIdSource
			if (CanDeepSave(entity, "AdminTaskEmailRecipient|AdminTaskEmailRecipientIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskEmailRecipientIdSource != null)
			{
				DataRepository.AdminTaskEmailRecipientProvider.Save(transactionManager, entity.AdminTaskEmailRecipientIdSource);
				entity.AdminTaskEmailRecipientId = entity.AdminTaskEmailRecipientIdSource.AdminTaskEmailRecipientId;
			}
			#endregion 
			
			#region CsmsEmailRecipientTypeIdSource
			if (CanDeepSave(entity, "CsmsEmailRecipientType|CsmsEmailRecipientTypeIdSource", deepSaveType, innerList) 
				&& entity.CsmsEmailRecipientTypeIdSource != null)
			{
				DataRepository.CsmsEmailRecipientTypeProvider.Save(transactionManager, entity.CsmsEmailRecipientTypeIdSource);
				entity.CsmsEmailRecipientTypeId = entity.CsmsEmailRecipientTypeIdSource.CsmsEmailRecipientEmailTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskEmailTemplateRecipientChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskEmailTemplateRecipient</c>
	///</summary>
	public enum AdminTaskEmailTemplateRecipientChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdminTaskEmailTemplate</c> at AdminTaskEmailTemplateIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskEmailTemplate))]
		AdminTaskEmailTemplate,
			
		///<summary>
		/// Composite Property for <c>AdminTaskEmailRecipient</c> at AdminTaskEmailRecipientIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskEmailRecipient))]
		AdminTaskEmailRecipient,
			
		///<summary>
		/// Composite Property for <c>CsmsEmailRecipientType</c> at CsmsEmailRecipientTypeIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsEmailRecipientType))]
		CsmsEmailRecipientType,
		}
	
	#endregion AdminTaskEmailTemplateRecipientChildEntityTypes
	
	#region AdminTaskEmailTemplateRecipientFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskEmailTemplateRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientFilterBuilder : SqlFilterBuilder<AdminTaskEmailTemplateRecipientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientFilterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateRecipientFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateRecipientFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateRecipientFilterBuilder
	
	#region AdminTaskEmailTemplateRecipientParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskEmailTemplateRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateRecipientParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailTemplateRecipientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientParameterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateRecipientParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateRecipientParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateRecipientParameterBuilder
	
	#region AdminTaskEmailTemplateRecipientSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskEmailTemplateRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateRecipient"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailTemplateRecipientSortBuilder : SqlSortBuilder<AdminTaskEmailTemplateRecipientColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateRecipientSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateRecipientSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailTemplateRecipientSortBuilder
	
} // end namespace
