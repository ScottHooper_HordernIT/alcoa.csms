using System;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

using KaiZen.CSMS.Entities;


public partial class Library_APSS_Search : System.Web.UI.UserControl
{
    public int cnt;
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            //ASPxTextBox1.Focus();
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    break;
                default:
                    // do something
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }

            //http://weblogs.asp.net/jeff/archive/2005/07/26/420618.aspx enter button keypress
            ASPxtbSearch.Attributes.Add("onKeyPress", String.Format("javascript:if (event.keyCode == 13) __doPostBack('{0}','')", ASPxbtnSearch.UniqueID));
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtApssSearch"];
            grid.DataBind();
        }
    }

    public string getpagelink(string srcfile)
    {
        string temp = String.Format("{0}.) {1}", cnt, srcfile);
        return temp;
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        string fileName = (string)grid.GetRowValues(e.VisibleIndex, "FILENAME");
        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFileName") as ASPxHyperLink;
        if (hl != null)
        {
            hl.Text = fileName;
            hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}');", QueryStringModule.Encrypt("Type=APSS&File=" + fileName));
        }
    }

    protected void ASPxbtnSearch_Click(object sender, EventArgs e)
    {
        OleDbConnection odbSearch = new OleDbConnection();
        using (OleDbCommand cmdSearch = new OleDbCommand())
        {
            //Microsoft Indexing Service (catalog name = WAOCSM APSS; directory = \\aua.alcoa.com\dfs\BGN\InfoShare\CONTROL\CDS\WAOCSM\CSMCOMBINED)
            odbSearch.ConnectionString = string.Format("Provider=\"MSIDXS\";Data Source=\"{0}\";", ConfigurationManager.AppSettings["APSS_Search_DataSource"]);
            cmdSearch.Connection = odbSearch;
            try
            {
                lbl.Text = "";

                string sqlCmd, sqlCmdFilename;
                string search = ASPxtbSearch.Text;
                if (search.Contains(" "))
                {
                    if (search.Contains(" and ") || search.Contains(" AND ") || search.Contains(" And "))
                    {
                        search = search.Replace(" and ", " ");
                        search = search.Replace(" AND ", " ");
                        search = search.Replace(" And ", " ");
                    }

                    sqlCmd = String.Format("select doctitle, filename, vpath, rank, characterization from scope() where (CONTAINS(doctitle, '{0}') or CONTAINS(Contents, '{0}')) and filename NOT LIKE '%{0}%' and filename <> 'search.aspx' order by rank desc ", (search.Replace(" ", " & ")));
                    sqlCmdFilename = String.Format("select doctitle, filename, vpath, rank, characterization from scope() where filename LIKE '%{0}%' and filename <> 'search.aspx' order by rank desc ", (search.Replace(" ", " & ")));
                }
                else
                {
                    sqlCmd = String.Format("select doctitle, filename, vpath, rank, characterization from scope() where (FREETEXT(doctitle, '{0}') or FREETEXT(Contents, '{0}')) and filename NOT LIKE '%{0}%' and filename <> 'search.aspx' order by rank desc ", search);
                    sqlCmdFilename = String.Format("select doctitle, filename, vpath, rank, characterization from scope() where filename LIKE '%{0}%' and filename <> 'search.aspx' order by rank desc ", search);
                }

                DataTable dt = new DataTable();

                //Filename
                cmdSearch.CommandText = sqlCmdFilename;
                odbSearch.Open();
                try
                {
                    OleDbDataReader rdrSearch = cmdSearch.ExecuteReader();
                    OleDbDataAdapter da = new OleDbDataAdapter(sqlCmdFilename, odbSearch);
                    using (DataSet ds = new DataSet())
                    {
                        da.Fill(ds, "SearchResults");
                        dt = ds.Tables[0];
                    }

                    //Priorities filename row rankings
                    var col = dt.Columns["rank"];
                    foreach (DataRow row in dt.Rows)
                        row[col] = (int)row[col] * 1000;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    lbl.Text = ex.Message;
                }
                odbSearch.Close();

                //Content
                cmdSearch.CommandText = sqlCmd;
                odbSearch.Open();
                try
                {
                    OleDbDataReader rdrSearch = cmdSearch.ExecuteReader();
                    //dg1.DataSource=rdrSearch; //dg1.DataBind(); cnt=1; 
                    OleDbDataAdapter da = new OleDbDataAdapter(sqlCmd, odbSearch);
                    using (DataSet ds = new DataSet())
                    {
                        da.Fill(ds, "SearchResults");
                        dt.Merge(ds.Tables[0]);
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    lbl.Text = ex.Message;
                }
                odbSearch.Close();

                //Bind grid
                Session["dtApssSearch"] = dt;
                grid.DataSource = (DataTable)Session["dtApssSearch"];
                grid.DataBind();
                grid.Visible = true;
                
                cnt = dt.Rows.Count;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                lbl.Text = ex.Message;
                odbSearch.Close();
            }
        }
    }
}
