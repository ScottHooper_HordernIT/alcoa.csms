﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsaAnswersProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsaAnswersProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CsaAnswers, KaiZen.CSMS.Entities.CsaAnswersKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsaAnswersKey key)
		{
			return Delete(transactionManager, key.CsaAnswerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csaAnswerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csaAnswerId)
		{
			return Delete(null, _csaAnswerId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaAnswerId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csaAnswerId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Answers_CSA_Answers key.
		///		FK_CSA_Answers_CSA_Answers Description: 
		/// </summary>
		/// <param name="_csaId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsaAnswers objects.</returns>
		public TList<CsaAnswers> GetByCsaId(System.Int32 _csaId)
		{
			int count = -1;
			return GetByCsaId(_csaId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Answers_CSA_Answers key.
		///		FK_CSA_Answers_CSA_Answers Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsaAnswers objects.</returns>
		/// <remarks></remarks>
		public TList<CsaAnswers> GetByCsaId(TransactionManager transactionManager, System.Int32 _csaId)
		{
			int count = -1;
			return GetByCsaId(transactionManager, _csaId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Answers_CSA_Answers key.
		///		FK_CSA_Answers_CSA_Answers Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsaAnswers objects.</returns>
		public TList<CsaAnswers> GetByCsaId(TransactionManager transactionManager, System.Int32 _csaId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaId(transactionManager, _csaId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Answers_CSA_Answers key.
		///		fkCsaAnswersCsaAnswers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csaId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsaAnswers objects.</returns>
		public TList<CsaAnswers> GetByCsaId(System.Int32 _csaId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsaId(null, _csaId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Answers_CSA_Answers key.
		///		fkCsaAnswersCsaAnswers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csaId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsaAnswers objects.</returns>
		public TList<CsaAnswers> GetByCsaId(System.Int32 _csaId, int start, int pageLength,out int count)
		{
			return GetByCsaId(null, _csaId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CSA_Answers_CSA_Answers key.
		///		FK_CSA_Answers_CSA_Answers Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsaAnswers objects.</returns>
		public abstract TList<CsaAnswers> GetByCsaId(TransactionManager transactionManager, System.Int32 _csaId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CsaAnswers Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsaAnswersKey key, int start, int pageLength)
		{
			return GetByCsaAnswerId(transactionManager, key.CsaAnswerId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CSA_Answers index.
		/// </summary>
		/// <param name="_csaAnswerId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaAnswerId(System.Int32 _csaAnswerId)
		{
			int count = -1;
			return GetByCsaAnswerId(null,_csaAnswerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA_Answers index.
		/// </summary>
		/// <param name="_csaAnswerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaAnswerId(System.Int32 _csaAnswerId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaAnswerId(null, _csaAnswerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaAnswerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaAnswerId(TransactionManager transactionManager, System.Int32 _csaAnswerId)
		{
			int count = -1;
			return GetByCsaAnswerId(transactionManager, _csaAnswerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaAnswerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaAnswerId(TransactionManager transactionManager, System.Int32 _csaAnswerId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaAnswerId(transactionManager, _csaAnswerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA_Answers index.
		/// </summary>
		/// <param name="_csaAnswerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaAnswerId(System.Int32 _csaAnswerId, int start, int pageLength, out int count)
		{
			return GetByCsaAnswerId(null, _csaAnswerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CSA_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaAnswerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsaAnswers GetByCsaAnswerId(TransactionManager transactionManager, System.Int32 _csaAnswerId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CSA_Answers index.
		/// </summary>
		/// <param name="_csaId"></param>
		/// <param name="_questionNo"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaIdQuestionNo(System.Int32 _csaId, System.String _questionNo)
		{
			int count = -1;
			return GetByCsaIdQuestionNo(null,_csaId, _questionNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA_Answers index.
		/// </summary>
		/// <param name="_csaId"></param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaIdQuestionNo(System.Int32 _csaId, System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaIdQuestionNo(null, _csaId, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="_questionNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaIdQuestionNo(TransactionManager transactionManager, System.Int32 _csaId, System.String _questionNo)
		{
			int count = -1;
			return GetByCsaIdQuestionNo(transactionManager, _csaId, _questionNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaIdQuestionNo(TransactionManager transactionManager, System.Int32 _csaId, System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByCsaIdQuestionNo(transactionManager, _csaId, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA_Answers index.
		/// </summary>
		/// <param name="_csaId"></param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public KaiZen.CSMS.Entities.CsaAnswers GetByCsaIdQuestionNo(System.Int32 _csaId, System.String _questionNo, int start, int pageLength, out int count)
		{
			return GetByCsaIdQuestionNo(null, _csaId, _questionNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CSA_Answers index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csaId"></param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsaAnswers GetByCsaIdQuestionNo(TransactionManager transactionManager, System.Int32 _csaId, System.String _questionNo, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CsaAnswers&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CsaAnswers&gt;"/></returns>
		public static TList<CsaAnswers> Fill(IDataReader reader, TList<CsaAnswers> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CsaAnswers c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CsaAnswers")
					.Append("|").Append((System.Int32)reader[((int)CsaAnswersColumn.CsaAnswerId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CsaAnswers>(
					key.ToString(), // EntityTrackingKey
					"CsaAnswers",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CsaAnswers();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsaAnswerId = (System.Int32)reader[((int)CsaAnswersColumn.CsaAnswerId - 1)];
					c.CsaId = (System.Int32)reader[((int)CsaAnswersColumn.CsaId - 1)];
					c.QuestionNo = (System.String)reader[((int)CsaAnswersColumn.QuestionNo - 1)];
					c.AnswerValue = (reader.IsDBNull(((int)CsaAnswersColumn.AnswerValue - 1)))?null:(System.Int32?)reader[((int)CsaAnswersColumn.AnswerValue - 1)];
					c.Comment = (reader.IsDBNull(((int)CsaAnswersColumn.Comment - 1)))?null:(System.Byte[])reader[((int)CsaAnswersColumn.Comment - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CsaAnswers entity)
		{
			if (!reader.Read()) return;
			
			entity.CsaAnswerId = (System.Int32)reader[((int)CsaAnswersColumn.CsaAnswerId - 1)];
			entity.CsaId = (System.Int32)reader[((int)CsaAnswersColumn.CsaId - 1)];
			entity.QuestionNo = (System.String)reader[((int)CsaAnswersColumn.QuestionNo - 1)];
			entity.AnswerValue = (reader.IsDBNull(((int)CsaAnswersColumn.AnswerValue - 1)))?null:(System.Int32?)reader[((int)CsaAnswersColumn.AnswerValue - 1)];
			entity.Comment = (reader.IsDBNull(((int)CsaAnswersColumn.Comment - 1)))?null:(System.Byte[])reader[((int)CsaAnswersColumn.Comment - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CsaAnswers entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsaAnswerId = (System.Int32)dataRow["CSAAnswerId"];
			entity.CsaId = (System.Int32)dataRow["CSAId"];
			entity.QuestionNo = (System.String)dataRow["QuestionNo"];
			entity.AnswerValue = Convert.IsDBNull(dataRow["AnswerValue"]) ? null : (System.Int32?)dataRow["AnswerValue"];
			entity.Comment = Convert.IsDBNull(dataRow["Comment"]) ? null : (System.Byte[])dataRow["Comment"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsaAnswers"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsaAnswers Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsaAnswers entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CsaIdSource	
			if (CanDeepLoad(entity, "Csa|CsaIdSource", deepLoadType, innerList) 
				&& entity.CsaIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsaId;
				Csa tmpEntity = EntityManager.LocateEntity<Csa>(EntityLocator.ConstructKeyFromPkItems(typeof(Csa), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsaIdSource = tmpEntity;
				else
					entity.CsaIdSource = DataRepository.CsaProvider.GetByCsaId(transactionManager, entity.CsaId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsaIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsaIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsaProvider.DeepLoad(transactionManager, entity.CsaIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsaIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CsaAnswers object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CsaAnswers instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsaAnswers Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsaAnswers entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CsaIdSource
			if (CanDeepSave(entity, "Csa|CsaIdSource", deepSaveType, innerList) 
				&& entity.CsaIdSource != null)
			{
				DataRepository.CsaProvider.Save(transactionManager, entity.CsaIdSource);
				entity.CsaId = entity.CsaIdSource.CsaId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsaAnswersChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CsaAnswers</c>
	///</summary>
	public enum CsaAnswersChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Csa</c> at CsaIdSource
		///</summary>
		[ChildEntityType(typeof(Csa))]
		Csa,
		}
	
	#endregion CsaAnswersChildEntityTypes
	
	#region CsaAnswersFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsaAnswersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersFilterBuilder : SqlFilterBuilder<CsaAnswersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersFilterBuilder class.
		/// </summary>
		public CsaAnswersFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaAnswersFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaAnswersFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaAnswersFilterBuilder
	
	#region CsaAnswersParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsaAnswersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersParameterBuilder : ParameterizedSqlFilterBuilder<CsaAnswersColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersParameterBuilder class.
		/// </summary>
		public CsaAnswersParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsaAnswersParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsaAnswersParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsaAnswersParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsaAnswersParameterBuilder
	
	#region CsaAnswersSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsaAnswersColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsaAnswersSortBuilder : SqlSortBuilder<CsaAnswersColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersSqlSortBuilder class.
		/// </summary>
		public CsaAnswersSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsaAnswersSortBuilder
	
} // end namespace
