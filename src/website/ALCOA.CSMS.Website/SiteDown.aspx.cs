using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class SiteDown : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Configuration configuration = new Configuration();
        ASPxHyperLinkContact.NavigateUrl = "mailto:" + configuration.GetValue(ConfigList.ContactEmail);
        Label1.Text = configuration.GetValue(ConfigList.SiteOfflineReason);
        Label2.Text = configuration.GetValue(ConfigList.SiteOfflineETA);
        //string error = Request.QueryString["error"];
        //if (!String.IsNullOrEmpty(error))
        //{
        //    Label1.Text = error;
        //    //Log Error?!
        //    if (error == "Not Logged In.")
        //    {
        //        Label1.Text = "You are not logged in.<br />" +
        //                    "If you have logged in and been inactive for over 20 minutes you will have to re-login for security reasons";
        //    }
        //}
        //else
        //{
        //    Label1.Text = "No Error Specified.";
        //}
    }
}
