﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace ALCOA.CSMS.Website.UserControls.Admin {
    
    
    public partial class AdminTasks {
        
        /// <summary>
        /// ASPxPageControl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxTabControl.ASPxPageControl ASPxPageControl1;
        
        /// <summary>
        /// grid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxGridView.ASPxGridView grid;
        
        /// <summary>
        /// hlAddNewTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxHyperLink hlAddNewTask;
        
        /// <summary>
        /// popupAddNewTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.ASPxPopupControl popupAddNewTask;
        
        /// <summary>
        /// popupContentAddNewTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPopupControl.PopupControlContentControl popupContentAddNewTask;
        
        /// <summary>
        /// rpAddNewTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxRoundPanel.ASPxRoundPanel rpAddNewTask;
        
        /// <summary>
        /// PanelContent4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxPanel.PanelContent PanelContent4;
        
        /// <summary>
        /// lblAddNewTaskError control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxLabel lblAddNewTaskError;
        
        /// <summary>
        /// cbAddNewTaskType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cbAddNewTaskType;
        
        /// <summary>
        /// cbAddNewTaskAccess control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cbAddNewTaskAccess;
        
        /// <summary>
        /// tbAddNewTaskLogin control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskLogin;
        
        /// <summary>
        /// tbAddNewTaskEmail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskEmail;
        
        /// <summary>
        /// cbAddNewTaskCompany control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxComboBox cbAddNewTaskCompany;
        
        /// <summary>
        /// tbAddNewTaskFirstName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskFirstName;
        
        /// <summary>
        /// tbAddNewTaskLastName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskLastName;
        
        /// <summary>
        /// tbAddNewTaskJobTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskJobTitle;
        
        /// <summary>
        /// tbAddNewTaskMobileNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskMobileNumber;
        
        /// <summary>
        /// tbAddNewTaskTelephoneNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskTelephoneNumber;
        
        /// <summary>
        /// tbAddNewTaskFaxNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxTextBox tbAddNewTaskFaxNumber;
        
        /// <summary>
        /// mAddNewTaskMemoComments control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxMemo mAddNewTaskMemoComments;
        
        /// <summary>
        /// btnAddNewTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DevExpress.Web.ASPxEditors.ASPxButton btnAddNewTask;
        
        /// <summary>
        /// AdminTaskDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.AdminTaskDataSource AdminTaskDataSource1;
        
        /// <summary>
        /// AdminTaskSourceDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.AdminTaskSourceDataSource AdminTaskSourceDataSource1;
        
        /// <summary>
        /// AdminTaskTypeDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.AdminTaskTypeDataSource AdminTaskTypeDataSource1;
        
        /// <summary>
        /// AdminTaskTypeDataSource2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.AdminTaskTypeDataSource AdminTaskTypeDataSource2;
        
        /// <summary>
        /// CompaniesDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.CompaniesDataSource CompaniesDataSource1;
        
        /// <summary>
        /// UsersFullNameDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.UsersFullNameDataSource UsersFullNameDataSource1;
        
        /// <summary>
        /// AdminTaskStatusDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.AdminTaskStatusDataSource AdminTaskStatusDataSource1;
        
        /// <summary>
        /// CsmsAccessDataSource1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::KaiZen.CSMS.Web.Data.CsmsAccessDataSource CsmsAccessDataSource1;
        
        /// <summary>
        /// CompanyDS control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource CompanyDS;
        
        /// <summary>
        /// UsersOpenedByDS control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource UsersOpenedByDS;
        
        /// <summary>
        /// UsersClosedByDS control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource UsersClosedByDS;
        
        /// <summary>
        /// AdminTaskMetrics1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::ALCOA.CSMS.Website.UserControls.Admin.AdminTaskMetrics AdminTaskMetrics1;
        
        /// <summary>
        /// sqlDaAdminTask control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.SqlDataSource sqlDaAdminTask;
    }
}
