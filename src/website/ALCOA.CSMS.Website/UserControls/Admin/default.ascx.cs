using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class Adminv2_UserControls_Default : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        try
        {
            if (auth.RoleId == (int)RoleList.Administrator)
            {
                ASPxPageControl1.Visible = true;
                
            }
            else
            {
                ASPxPageControl1.Visible = false;
                Label1.Text = "Access Denied. You do not appear to have permissions to access this page. Please Contact your Administration for more information.";
            }
        }
        catch (Exception ex)
        {
            ASPxPageControl1.Visible = false;
            Label1.Text = "Access Denied. You do not appear to have permissions to access this page. Please Contact your Administration for more information.";
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }
    }
}