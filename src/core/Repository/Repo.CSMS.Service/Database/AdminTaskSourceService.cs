﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskSourceService
    {
        AdminTaskSource Get(Expression<Func<AdminTaskSource, bool>> where, List<Expression<Func<AdminTaskSource, object>>> includes);
        List<AdminTaskSource> GetMany(int? top, Expression<Func<AdminTaskSource, bool>> where, Func<IQueryable<AdminTaskSource>, IOrderedQueryable<AdminTaskSource>> orderBy, List<Expression<Func<AdminTaskSource, object>>> includes);
        List<AdminTaskSource> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskSource, bool>> where, Func<IQueryable<AdminTaskSource>, IOrderedQueryable<AdminTaskSource>> orderBy, List<Expression<Func<AdminTaskSource, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskSource, bool>> where, List<Expression<Func<AdminTaskSource, object>>> includes);
        void Insert(AdminTaskSource item, bool saveChanges = true);
        void Update(AdminTaskSource item, bool saveChanges = true);
        void Delete(AdminTaskSource item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskSourceService : IAdminTaskSourceService
    {
        private readonly IRepository<AdminTaskSource> _repository;

        public AdminTaskSourceService(IRepository<AdminTaskSource> repository)
        {
            this._repository = repository;
        }

        public AdminTaskSource Get(Expression<Func<AdminTaskSource, bool>> where, List<Expression<Func<AdminTaskSource, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskSource> GetMany(int? top, Expression<Func<AdminTaskSource, bool>> where, Func<IQueryable<AdminTaskSource>, IOrderedQueryable<AdminTaskSource>> orderBy, List<Expression<Func<AdminTaskSource, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskSource> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskSource, bool>> where, Func<IQueryable<AdminTaskSource>, IOrderedQueryable<AdminTaskSource>> orderBy, List<Expression<Func<AdminTaskSource, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskSource, bool>> where, List<Expression<Func<AdminTaskSource, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskSource item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskSource");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskSource item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskSource");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskSource item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskSource");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
