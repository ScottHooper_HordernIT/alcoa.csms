﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEHSConsultantService
    {
        EHSConsultant Get(Expression<Func<EHSConsultant, bool>> where, List<Expression<Func<EHSConsultant, object>>> includes);
        List<EHSConsultant> GetMany(int? top, Expression<Func<EHSConsultant, bool>> where, Func<IQueryable<EHSConsultant>, IOrderedQueryable<EHSConsultant>> orderBy, List<Expression<Func<EHSConsultant, object>>> includes);
        List<EHSConsultant> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EHSConsultant, bool>> where, Func<IQueryable<EHSConsultant>, IOrderedQueryable<EHSConsultant>> orderBy, List<Expression<Func<EHSConsultant, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EHSConsultant, bool>> where, List<Expression<Func<EHSConsultant, object>>> includes);
        void Insert(EHSConsultant item, bool saveChanges = true);
        void Update(EHSConsultant item, bool saveChanges = true);
        void Delete(EHSConsultant item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EHSConsultantService : IEHSConsultantService
    {
        private readonly IRepository<EHSConsultant> _repository;

        public EHSConsultantService(IRepository<EHSConsultant> repository)
        {
            this._repository = repository;
        }

        public EHSConsultant Get(Expression<Func<EHSConsultant, bool>> where, List<Expression<Func<EHSConsultant, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EHSConsultant> GetMany(int? top, Expression<Func<EHSConsultant, bool>> where, Func<IQueryable<EHSConsultant>, IOrderedQueryable<EHSConsultant>> orderBy, List<Expression<Func<EHSConsultant, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EHSConsultant> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EHSConsultant, bool>> where, Func<IQueryable<EHSConsultant>, IOrderedQueryable<EHSConsultant>> orderBy, List<Expression<Func<EHSConsultant, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EHSConsultant, bool>> where, List<Expression<Func<EHSConsultant, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EHSConsultant item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EHSConsultant");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EHSConsultant item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EHSConsultant");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EHSConsultant item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EHSConsultant");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
