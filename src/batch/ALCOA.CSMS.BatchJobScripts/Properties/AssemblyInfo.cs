﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ALCOA.CSMS.BatchJobScripts")]
[assembly: AssemblyDescription("Kai-Zen CSMS (Customised for Alcoa)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Yujia Li < www.kai-zen.com.au >")]
[assembly: AssemblyProduct("Kai-Zen CSMS (Customised for Alcoa)")]
[assembly: AssemblyCopyright("Copyright © Yujia Li < www.kai-zen.com.au >")]
[assembly: AssemblyTrademark("Yujia Li < www.kai-zen.com.au >")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d53c4da5-3709-4fab-9f5a-1cf51226c531")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
