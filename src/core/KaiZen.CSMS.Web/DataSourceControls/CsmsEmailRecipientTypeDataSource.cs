﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsEmailRecipientTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsmsEmailRecipientTypeDataSourceDesigner))]
	public class CsmsEmailRecipientTypeDataSource : ProviderDataSource<CsmsEmailRecipientType, CsmsEmailRecipientTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeDataSource class.
		/// </summary>
		public CsmsEmailRecipientTypeDataSource() : base(new CsmsEmailRecipientTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsEmailRecipientTypeDataSourceView used by the CsmsEmailRecipientTypeDataSource.
		/// </summary>
		protected CsmsEmailRecipientTypeDataSourceView CsmsEmailRecipientTypeView
		{
			get { return ( View as CsmsEmailRecipientTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsmsEmailRecipientTypeDataSource control invokes to retrieve data.
		/// </summary>
		public CsmsEmailRecipientTypeSelectMethod SelectMethod
		{
			get
			{
				CsmsEmailRecipientTypeSelectMethod selectMethod = CsmsEmailRecipientTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsmsEmailRecipientTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsEmailRecipientTypeDataSourceView class that is to be
		/// used by the CsmsEmailRecipientTypeDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsEmailRecipientTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsEmailRecipientType, CsmsEmailRecipientTypeKey> GetNewDataSourceView()
		{
			return new CsmsEmailRecipientTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsEmailRecipientTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsEmailRecipientTypeDataSourceView : ProviderDataSourceView<CsmsEmailRecipientType, CsmsEmailRecipientTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsEmailRecipientTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsEmailRecipientTypeDataSourceView(CsmsEmailRecipientTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsEmailRecipientTypeDataSource CsmsEmailRecipientTypeOwner
		{
			get { return Owner as CsmsEmailRecipientTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsmsEmailRecipientTypeSelectMethod SelectMethod
		{
			get { return CsmsEmailRecipientTypeOwner.SelectMethod; }
			set { CsmsEmailRecipientTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsEmailRecipientTypeService CsmsEmailRecipientTypeProvider
		{
			get { return Provider as CsmsEmailRecipientTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsmsEmailRecipientType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsmsEmailRecipientType> results = null;
			CsmsEmailRecipientType item;
			count = 0;
			
			System.Int32 _csmsEmailRecipientEmailTypeId;
			System.String _typeName;

			switch ( SelectMethod )
			{
				case CsmsEmailRecipientTypeSelectMethod.Get:
					CsmsEmailRecipientTypeKey entityKey  = new CsmsEmailRecipientTypeKey();
					entityKey.Load(values);
					item = CsmsEmailRecipientTypeProvider.Get(entityKey);
					results = new TList<CsmsEmailRecipientType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsmsEmailRecipientTypeSelectMethod.GetAll:
                    results = CsmsEmailRecipientTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsmsEmailRecipientTypeSelectMethod.GetPaged:
					results = CsmsEmailRecipientTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsmsEmailRecipientTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsmsEmailRecipientTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsmsEmailRecipientTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsmsEmailRecipientTypeSelectMethod.GetByCsmsEmailRecipientEmailTypeId:
					_csmsEmailRecipientEmailTypeId = ( values["CsmsEmailRecipientEmailTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailRecipientEmailTypeId"], typeof(System.Int32)) : (int)0;
					item = CsmsEmailRecipientTypeProvider.GetByCsmsEmailRecipientEmailTypeId(_csmsEmailRecipientEmailTypeId);
					results = new TList<CsmsEmailRecipientType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CsmsEmailRecipientTypeSelectMethod.GetByTypeName:
					_typeName = ( values["TypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["TypeName"], typeof(System.String)) : string.Empty;
					item = CsmsEmailRecipientTypeProvider.GetByTypeName(_typeName);
					results = new TList<CsmsEmailRecipientType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsmsEmailRecipientTypeSelectMethod.Get || SelectMethod == CsmsEmailRecipientTypeSelectMethod.GetByCsmsEmailRecipientEmailTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsmsEmailRecipientType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsmsEmailRecipientTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsmsEmailRecipientType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsmsEmailRecipientTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsmsEmailRecipientTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsEmailRecipientTypeDataSource class.
	/// </summary>
	public class CsmsEmailRecipientTypeDataSourceDesigner : ProviderDataSourceDesigner<CsmsEmailRecipientType, CsmsEmailRecipientTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeDataSourceDesigner class.
		/// </summary>
		public CsmsEmailRecipientTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsEmailRecipientTypeSelectMethod SelectMethod
		{
			get { return ((CsmsEmailRecipientTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsmsEmailRecipientTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsmsEmailRecipientTypeDataSourceActionList

	/// <summary>
	/// Supports the CsmsEmailRecipientTypeDataSourceDesigner class.
	/// </summary>
	internal class CsmsEmailRecipientTypeDataSourceActionList : DesignerActionList
	{
		private CsmsEmailRecipientTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsmsEmailRecipientTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsmsEmailRecipientTypeDataSourceActionList(CsmsEmailRecipientTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsEmailRecipientTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsmsEmailRecipientTypeDataSourceActionList
	
	#endregion CsmsEmailRecipientTypeDataSourceDesigner
	
	#region CsmsEmailRecipientTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsmsEmailRecipientTypeDataSource.SelectMethod property.
	/// </summary>
	public enum CsmsEmailRecipientTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsmsEmailRecipientEmailTypeId method.
		/// </summary>
		GetByCsmsEmailRecipientEmailTypeId,
		/// <summary>
		/// Represents the GetByTypeName method.
		/// </summary>
		GetByTypeName
	}
	
	#endregion CsmsEmailRecipientTypeSelectMethod

	#region CsmsEmailRecipientTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeFilter : SqlFilter<CsmsEmailRecipientTypeColumn>
	{
	}
	
	#endregion CsmsEmailRecipientTypeFilter

	#region CsmsEmailRecipientTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeExpressionBuilder : SqlExpressionBuilder<CsmsEmailRecipientTypeColumn>
	{
	}
	
	#endregion CsmsEmailRecipientTypeExpressionBuilder	

	#region CsmsEmailRecipientTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsmsEmailRecipientTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailRecipientType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailRecipientTypeProperty : ChildEntityProperty<CsmsEmailRecipientTypeChildEntityTypes>
	{
	}
	
	#endregion CsmsEmailRecipientTypeProperty
}

