﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ElmahErrorProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ElmahErrorProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ElmahError, KaiZen.CSMS.Entities.ElmahErrorKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ElmahErrorKey key)
		{
			return Delete(transactionManager, key.ErrorId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_errorId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Guid _errorId)
		{
			return Delete(null, _errorId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_errorId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Guid _errorId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ElmahError Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ElmahErrorKey key, int start, int pageLength)
		{
			return GetByErrorId(transactionManager, key.ErrorId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ELMAH_Error index.
		/// </summary>
		/// <param name="_errorId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ElmahError"/> class.</returns>
		public KaiZen.CSMS.Entities.ElmahError GetByErrorId(System.Guid _errorId)
		{
			int count = -1;
			return GetByErrorId(null,_errorId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ELMAH_Error index.
		/// </summary>
		/// <param name="_errorId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ElmahError"/> class.</returns>
		public KaiZen.CSMS.Entities.ElmahError GetByErrorId(System.Guid _errorId, int start, int pageLength)
		{
			int count = -1;
			return GetByErrorId(null, _errorId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ELMAH_Error index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_errorId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ElmahError"/> class.</returns>
		public KaiZen.CSMS.Entities.ElmahError GetByErrorId(TransactionManager transactionManager, System.Guid _errorId)
		{
			int count = -1;
			return GetByErrorId(transactionManager, _errorId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ELMAH_Error index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_errorId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ElmahError"/> class.</returns>
		public KaiZen.CSMS.Entities.ElmahError GetByErrorId(TransactionManager transactionManager, System.Guid _errorId, int start, int pageLength)
		{
			int count = -1;
			return GetByErrorId(transactionManager, _errorId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ELMAH_Error index.
		/// </summary>
		/// <param name="_errorId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ElmahError"/> class.</returns>
		public KaiZen.CSMS.Entities.ElmahError GetByErrorId(System.Guid _errorId, int start, int pageLength, out int count)
		{
			return GetByErrorId(null, _errorId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ELMAH_Error index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_errorId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ElmahError"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ElmahError GetByErrorId(TransactionManager transactionManager, System.Guid _errorId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ELMAH_Error_App_Time_Seq index.
		/// </summary>
		/// <param name="_application"></param>
		/// <param name="_timeUtc"></param>
		/// <param name="_sequence"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ElmahError&gt;"/> class.</returns>
		public TList<ElmahError> GetByApplicationTimeUtcSequence(System.String _application, System.DateTime _timeUtc, System.Int32 _sequence)
		{
			int count = -1;
			return GetByApplicationTimeUtcSequence(null,_application, _timeUtc, _sequence, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ELMAH_Error_App_Time_Seq index.
		/// </summary>
		/// <param name="_application"></param>
		/// <param name="_timeUtc"></param>
		/// <param name="_sequence"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ElmahError&gt;"/> class.</returns>
		public TList<ElmahError> GetByApplicationTimeUtcSequence(System.String _application, System.DateTime _timeUtc, System.Int32 _sequence, int start, int pageLength)
		{
			int count = -1;
			return GetByApplicationTimeUtcSequence(null, _application, _timeUtc, _sequence, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ELMAH_Error_App_Time_Seq index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_application"></param>
		/// <param name="_timeUtc"></param>
		/// <param name="_sequence"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ElmahError&gt;"/> class.</returns>
		public TList<ElmahError> GetByApplicationTimeUtcSequence(TransactionManager transactionManager, System.String _application, System.DateTime _timeUtc, System.Int32 _sequence)
		{
			int count = -1;
			return GetByApplicationTimeUtcSequence(transactionManager, _application, _timeUtc, _sequence, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ELMAH_Error_App_Time_Seq index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_application"></param>
		/// <param name="_timeUtc"></param>
		/// <param name="_sequence"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ElmahError&gt;"/> class.</returns>
		public TList<ElmahError> GetByApplicationTimeUtcSequence(TransactionManager transactionManager, System.String _application, System.DateTime _timeUtc, System.Int32 _sequence, int start, int pageLength)
		{
			int count = -1;
			return GetByApplicationTimeUtcSequence(transactionManager, _application, _timeUtc, _sequence, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ELMAH_Error_App_Time_Seq index.
		/// </summary>
		/// <param name="_application"></param>
		/// <param name="_timeUtc"></param>
		/// <param name="_sequence"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ElmahError&gt;"/> class.</returns>
		public TList<ElmahError> GetByApplicationTimeUtcSequence(System.String _application, System.DateTime _timeUtc, System.Int32 _sequence, int start, int pageLength, out int count)
		{
			return GetByApplicationTimeUtcSequence(null, _application, _timeUtc, _sequence, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ELMAH_Error_App_Time_Seq index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_application"></param>
		/// <param name="_timeUtc"></param>
		/// <param name="_sequence"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ElmahError&gt;"/> class.</returns>
		public abstract TList<ElmahError> GetByApplicationTimeUtcSequence(TransactionManager transactionManager, System.String _application, System.DateTime _timeUtc, System.Int32 _sequence, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ElmahError&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ElmahError&gt;"/></returns>
		public static TList<ElmahError> Fill(IDataReader reader, TList<ElmahError> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ElmahError c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ElmahError")
					.Append("|").Append((System.Guid)reader[((int)ElmahErrorColumn.ErrorId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ElmahError>(
					key.ToString(), // EntityTrackingKey
					"ElmahError",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ElmahError();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ErrorId = (System.Guid)reader[((int)ElmahErrorColumn.ErrorId - 1)];
					c.OriginalErrorId = c.ErrorId;
					c.Application = (System.String)reader[((int)ElmahErrorColumn.Application - 1)];
					c.Host = (System.String)reader[((int)ElmahErrorColumn.Host - 1)];
					c.Type = (System.String)reader[((int)ElmahErrorColumn.Type - 1)];
					c.Source = (System.String)reader[((int)ElmahErrorColumn.Source - 1)];
					c.Message = (System.String)reader[((int)ElmahErrorColumn.Message - 1)];
					c.User = (System.String)reader[((int)ElmahErrorColumn.User - 1)];
					c.StatusCode = (System.Int32)reader[((int)ElmahErrorColumn.StatusCode - 1)];
					c.TimeUtc = (System.DateTime)reader[((int)ElmahErrorColumn.TimeUtc - 1)];
					c.Sequence = (System.Int32)reader[((int)ElmahErrorColumn.Sequence - 1)];
					c.AllXml = (System.String)reader[((int)ElmahErrorColumn.AllXml - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ElmahError"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ElmahError"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ElmahError entity)
		{
			if (!reader.Read()) return;
			
			entity.ErrorId = (System.Guid)reader[((int)ElmahErrorColumn.ErrorId - 1)];
			entity.OriginalErrorId = (System.Guid)reader["ErrorId"];
			entity.Application = (System.String)reader[((int)ElmahErrorColumn.Application - 1)];
			entity.Host = (System.String)reader[((int)ElmahErrorColumn.Host - 1)];
			entity.Type = (System.String)reader[((int)ElmahErrorColumn.Type - 1)];
			entity.Source = (System.String)reader[((int)ElmahErrorColumn.Source - 1)];
			entity.Message = (System.String)reader[((int)ElmahErrorColumn.Message - 1)];
			entity.User = (System.String)reader[((int)ElmahErrorColumn.User - 1)];
			entity.StatusCode = (System.Int32)reader[((int)ElmahErrorColumn.StatusCode - 1)];
			entity.TimeUtc = (System.DateTime)reader[((int)ElmahErrorColumn.TimeUtc - 1)];
			entity.Sequence = (System.Int32)reader[((int)ElmahErrorColumn.Sequence - 1)];
			entity.AllXml = (System.String)reader[((int)ElmahErrorColumn.AllXml - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ElmahError"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ElmahError"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ElmahError entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ErrorId = (System.Guid)dataRow["ErrorId"];
			entity.OriginalErrorId = (System.Guid)dataRow["ErrorId"];
			entity.Application = (System.String)dataRow["Application"];
			entity.Host = (System.String)dataRow["Host"];
			entity.Type = (System.String)dataRow["Type"];
			entity.Source = (System.String)dataRow["Source"];
			entity.Message = (System.String)dataRow["Message"];
			entity.User = (System.String)dataRow["User"];
			entity.StatusCode = (System.Int32)dataRow["StatusCode"];
			entity.TimeUtc = (System.DateTime)dataRow["TimeUtc"];
			entity.Sequence = (System.Int32)dataRow["Sequence"];
			entity.AllXml = (System.String)dataRow["AllXml"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ElmahError"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ElmahError Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ElmahError entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ElmahError object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ElmahError instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ElmahError Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ElmahError entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ElmahErrorChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ElmahError</c>
	///</summary>
	public enum ElmahErrorChildEntityTypes
	{
	}
	
	#endregion ElmahErrorChildEntityTypes
	
	#region ElmahErrorFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ElmahErrorColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ElmahError"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ElmahErrorFilterBuilder : SqlFilterBuilder<ElmahErrorColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ElmahErrorFilterBuilder class.
		/// </summary>
		public ElmahErrorFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ElmahErrorFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ElmahErrorFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ElmahErrorFilterBuilder
	
	#region ElmahErrorParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ElmahErrorColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ElmahError"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ElmahErrorParameterBuilder : ParameterizedSqlFilterBuilder<ElmahErrorColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ElmahErrorParameterBuilder class.
		/// </summary>
		public ElmahErrorParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ElmahErrorParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ElmahErrorParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ElmahErrorParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ElmahErrorParameterBuilder
	
	#region ElmahErrorSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ElmahErrorColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ElmahError"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ElmahErrorSortBuilder : SqlSortBuilder<ElmahErrorColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ElmahErrorSqlSortBuilder class.
		/// </summary>
		public ElmahErrorSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ElmahErrorSortBuilder
	
} // end namespace
