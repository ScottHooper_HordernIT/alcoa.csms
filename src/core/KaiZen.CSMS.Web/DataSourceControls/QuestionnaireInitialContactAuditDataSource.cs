﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialContactAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialContactAuditDataSourceDesigner))]
	public class QuestionnaireInitialContactAuditDataSource : ProviderDataSource<QuestionnaireInitialContactAudit, QuestionnaireInitialContactAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditDataSource class.
		/// </summary>
		public QuestionnaireInitialContactAuditDataSource() : base(new QuestionnaireInitialContactAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialContactAuditDataSourceView used by the QuestionnaireInitialContactAuditDataSource.
		/// </summary>
		protected QuestionnaireInitialContactAuditDataSourceView QuestionnaireInitialContactAuditView
		{
			get { return ( View as QuestionnaireInitialContactAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialContactAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialContactAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialContactAuditSelectMethod selectMethod = QuestionnaireInitialContactAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialContactAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialContactAuditDataSourceView class that is to be
		/// used by the QuestionnaireInitialContactAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialContactAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialContactAudit, QuestionnaireInitialContactAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialContactAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialContactAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialContactAuditDataSourceView : ProviderDataSourceView<QuestionnaireInitialContactAudit, QuestionnaireInitialContactAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialContactAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialContactAuditDataSourceView(QuestionnaireInitialContactAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialContactAuditDataSource QuestionnaireInitialContactAuditOwner
		{
			get { return Owner as QuestionnaireInitialContactAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialContactAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialContactAuditOwner.SelectMethod; }
			set { QuestionnaireInitialContactAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialContactAuditService QuestionnaireInitialContactAuditProvider
		{
			get { return Provider as QuestionnaireInitialContactAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialContactAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialContactAudit> results = null;
			QuestionnaireInitialContactAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialContactAuditSelectMethod.Get:
					QuestionnaireInitialContactAuditKey entityKey  = new QuestionnaireInitialContactAuditKey();
					entityKey.Load(values);
					item = QuestionnaireInitialContactAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialContactAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialContactAuditSelectMethod.GetAll:
                    results = QuestionnaireInitialContactAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialContactAuditSelectMethod.GetPaged:
					results = QuestionnaireInitialContactAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialContactAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialContactAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialContactAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialContactAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialContactAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireInitialContactAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialContactAuditSelectMethod.Get || SelectMethod == QuestionnaireInitialContactAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialContactAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialContactAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialContactAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialContactAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialContactAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialContactAuditDataSource class.
	/// </summary>
	public class QuestionnaireInitialContactAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialContactAudit, QuestionnaireInitialContactAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialContactAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialContactAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialContactAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialContactAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialContactAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialContactAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialContactAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialContactAuditDataSourceActionList(QuestionnaireInitialContactAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialContactAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialContactAuditDataSourceActionList
	
	#endregion QuestionnaireInitialContactAuditDataSourceDesigner
	
	#region QuestionnaireInitialContactAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialContactAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialContactAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion QuestionnaireInitialContactAuditSelectMethod

	#region QuestionnaireInitialContactAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditFilter : SqlFilter<QuestionnaireInitialContactAuditColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactAuditFilter

	#region QuestionnaireInitialContactAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialContactAuditColumn>
	{
	}
	
	#endregion QuestionnaireInitialContactAuditExpressionBuilder	

	#region QuestionnaireInitialContactAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialContactAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactAuditProperty : ChildEntityProperty<QuestionnaireInitialContactAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialContactAuditProperty
}

