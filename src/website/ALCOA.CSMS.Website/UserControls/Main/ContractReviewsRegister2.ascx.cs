﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxGridView;
using System.Collections;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxFileManager;

namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContractReviewsRegister2 : System.Web.UI.UserControl
    {

        Auth auth = new Auth();

        public void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            
            if (!IsPostBack)
            {              
                Session["ShowAll"] = false;
                FillYearCombo();

                Session["authRoleId"] = auth.RoleId;
                

                ContractReviewsRegisterService compSer = new ContractReviewsRegisterService();
                TList<KaiZen.CSMS.Entities.ContractReviewsRegister> comp = compSer.GetByYear(Convert.ToInt32(cmbYear.Value));

                
                    if (comp.Count == 0)
                    {
                        InsertAllRows();
                    }
                    

                Session["CompanyIdVal"] = null;
                Session["SiteIdVal"] = null;
                Session["filterCount"] = null;
                Session["JanVal"] = null;
                Session["FebVal"] = null;
                Session["MarVal"] = null;
                Session["AprVal"] = null;

                Session["MayVal"] = null;
                Session["JunVal"] = null;
                Session["JulVal"] = null;
                Session["AugVal"] = null;

                Session["SepVal"] = null;
                Session["OctVal"] = null;
                Session["NovVal"] = null;
                Session["DecVal"] = null;

                Session["YrCountVal"] = null;
                Session["LocSPUVal"] = null;
                Session["LocSPAVal"] = null;

               // Session["WhereCondition"] = null;
            }
            Session["YearFilter"] = cmbYear.Value.ToString();

        }

        protected void FillYearCombo()
        {
            int yearToStart = 2012;
            int currentYear = DateTime.Now.Year;

            while (currentYear >= yearToStart)
            {
                cmbYear.Items.Add(currentYear.ToString(), currentYear);
                currentYear--;
            }
            cmbYear.SelectedIndex = 0;
        }


        protected void InsertAllRows()
        {
            CompanySiteCategoryStandardService stdSer = new CompanySiteCategoryStandardService();
            TList<CompanySiteCategoryStandard> stdList = stdSer.GetAll();
            int CompanySiteCategoryStandardId = 0;
            int CompanyId = 0;
            int SiteId = 0;

            string locationSponsorUserId = string.Empty;
            string locationSpaUserId = string.Empty;

            foreach (CompanySiteCategoryStandard std in stdList)
            {
                CompanySiteCategoryStandardId = 0;
                CompanyId = 0;
                SiteId = 0;

                if (std.CompanySiteCategoryStandardId != null)
                {
                    CompanySiteCategoryStandardId = Convert.ToInt32(std.CompanySiteCategoryStandardId);
                }

                if (std.CompanyId != null)
                {
                    CompanyId = Convert.ToInt32(std.CompanyId);
                }

                if (std.SiteId != null)
                {
                    SiteId = Convert.ToInt32(std.SiteId);
                }
                locationSponsorUserId = std.LocationSponsorUserId.ToString();
                locationSpaUserId = std.LocationSpaUserId.ToString();


                InsertRows(CompanyId, SiteId, locationSponsorUserId, locationSpaUserId);
            }

            DataView dVDistinct = (DataView)SqlDataSourceDistinctCompanyID.Select(DataSourceSelectArguments.Empty);
            DataTable dtDistinct = dVDistinct.ToTable();

            for (int i = 0; i < dtDistinct.Rows.Count - 1; i++)
            {
                CompanySiteCategoryStandardService compSiteSer = new CompanySiteCategoryStandardService();
                TList<CompanySiteCategoryStandard> compSiteList = compSiteSer.GetByCompanyId(Convert.ToInt32(dtDistinct.Rows[i]["CompanyId"]));

                bool wstnAustralia = false;
                bool victoria = false;
                bool rolledProducts = false;

                foreach (CompanySiteCategoryStandard compSite in compSiteList)
                {

                    RegionsSitesService rsSer = new RegionsSitesService();
                    TList<RegionsSites> rsList = rsSer.GetBySiteId(Convert.ToInt32(compSite.SiteId));
                    foreach (RegionsSites rs in rsList)
                    {
                        //For Western Australian Region
                        if (rs.RegionId == 1 && !wstnAustralia)
                        {
                            wstnAustralia = true;

                            // DataRow drNewSites = dtData.NewRow();
                            InsertRegions(rs.RegionId, compSite.CompanyId, compSite.LocationSponsorUserId.ToString(), compSite.LocationSpaUserId.ToString());
                            // dtData.Rows.Add(drNewSites);
                            // compSiteStsId++;

                        }
                        //Victoria
                        else if (rs.RegionId == 4 && !victoria)
                        {
                            victoria = true;
                            InsertRegions(rs.RegionId, compSite.CompanyId, compSite.LocationSponsorUserId.ToString(), compSite.LocationSpaUserId.ToString());
                            //DataRow drNewSites = dtData.NewRow();
                            //drNewSites = FillDataRow(drNewSites, compSite.CompanyId, rs.RegionId, compSiteStsId);
                            //dtData.Rows.Add(drNewSites);
                            //compSiteStsId++;
                        }
                        //Rolled Product
                        else if (rs.RegionId == 7 && !rolledProducts)
                        {
                            rolledProducts = true;
                            InsertRegions(rs.RegionId, compSite.CompanyId, compSite.LocationSponsorUserId.ToString(), compSite.LocationSpaUserId.ToString());

                            //DataRow drNewSites = dtData.NewRow();
                            //drNewSites = FillDataRow(drNewSites, compSite.CompanyId, rs.RegionId, compSiteStsId);
                            //dtData.Rows.Add(drNewSites);
                            //compSiteStsId++;
                        }
                    }
                }
            }

        }


        protected void InsertRows(int CompanyId, int SiteId, string LocationSponsorUserId, string LocationSpaUserId)
        {
            try
            {
                TransactionManager transactionManager = null;

                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                KaiZen.CSMS.Entities.ContractReviewsRegister contNew = new KaiZen.CSMS.Entities.ContractReviewsRegister();

                contNew.CompanyId = CompanyId;
                contNew.SiteId = SiteId;
                contNew.Year = Convert.ToInt32(cmbYear.SelectedItem.ToString());

                contNew.January = false;
                contNew.February = false;
                contNew.March = false;
                contNew.April = false;
                contNew.May = false;

                contNew.June = false;
                contNew.July = false;
                contNew.August = false;
                contNew.September = false;
                contNew.October = false;
                contNew.November = false;
                contNew.December = false;

                contNew.YearCount = 0;

                int Num;
                bool isNum = int.TryParse(LocationSponsorUserId, out Num);
                if (isNum)
                {
                    contNew.LocationSponsorUserId = Convert.ToInt32(LocationSponsorUserId);
                }
                else
                {
                    contNew.LocationSponsorUserId = null;
                }

                isNum = int.TryParse(LocationSpaUserId, out Num);
                if (isNum)
                {
                    contNew.LocationSpaUserId = Convert.ToInt32(LocationSpaUserId);
                }



                if (contNew.EntityState != EntityState.Unchanged)
                {
                    contNew.ModifiedBy = auth.UserId;
                    contNew.ModifiedDate = DateTime.Now;
                    //conser.Save(contNew);
                    DataRepository.ContractReviewsRegisterProvider.Insert(transactionManager, contNew);
                    transactionManager.Commit();
                }
            }
            catch (Exception ex)
            {
                ;
            }
        }


        protected void InsertRegions(int regionId, int companyId, string locationSponsorUserId, string locationSpaUserId)
        {
            try
            {
                int _year = Convert.ToInt32(cmbYear.SelectedItem.ToString());
                int _siteId = 0;
                int yearCount = 0;
                //if (regionId == 7)
                //{
                //    _siteId = 99;
                //}
                //else
                //{

                RegionsService regSer = new RegionsService();
                Regions reg = regSer.GetByRegionId(regionId);

                SitesService SiteSer = new SitesService();
                Sites site = SiteSer.GetBySiteName(reg.RegionName);

                _siteId = site.SiteId;

                //}

                InsertRows(companyId, _siteId, locationSponsorUserId, locationSpaUserId);
            }
            catch (Exception)
            {

                ;
            }
        }

        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["_year"] = Convert.ToInt32(cmbYear.Value);
        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {

            if (e.RowType == GridViewRowType.Data)
            {
                if (grid.GetRowValues(e.VisibleIndex, "CompanyId") == null)
                {
                    e.Row.Visible = false;
                }


                int yearCount = 0;
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "January"))) == 1)
                {
                    Image imgJan = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgJan") as Image;
                    imgJan.Visible = true;
                    imgJan.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "February"))) == 1)
                {
                    Image imgFeb = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgFeb") as Image;
                    imgFeb.Visible = true;
                    imgFeb.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "March"))) == 1)
                {
                    Image imgMarch = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgMar") as Image;
                    imgMarch.Visible = true;
                    imgMarch.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "April"))) == 1)
                {
                    Image imgApril = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgApr") as Image;
                    imgApril.Visible = true;
                    imgApril.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }

                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "May"))) == 1)
                {
                    Image imgMay = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgMay") as Image;
                    imgMay.Visible = true;
                    imgMay.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "June"))) == 1)
                {
                    Image imgJune = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgJun") as Image;
                    imgJune.Visible = true;
                    imgJune.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "July"))) == 1)
                {
                    Image imgJuly = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgJul") as Image;
                    imgJuly.Visible = true;
                    imgJuly.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "August"))) == 1)
                {
                    Image imgAugust = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgAug") as Image;
                    imgAugust.Visible = true;
                    imgAugust.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }


                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "September"))) == 1)
                {
                    Image imgSeptember = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgSep") as Image;
                    imgSeptember.Visible = true;
                    imgSeptember.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "October"))) == 1)
                {
                    Image imgOctober = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgOct") as Image;
                    imgOctober.Visible = true;
                    imgOctober.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "November"))) == 1)
                {
                    Image imgNov = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgNov") as Image;
                    imgNov.Visible = true;
                    imgNov.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                if (Convert.ToByte(Convert.ToBoolean(grid.GetRowValues(e.VisibleIndex, "December"))) == 1)
                {
                    Image imgDec = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgDec") as Image;
                    imgDec.Visible = true;
                    imgDec.ImageUrl = "~/Images/greentick.gif";
                    yearCount++;
                }
                //if (Convert.ToInt32(Session["authRoleId"]) != (int)RoleList.Administrator)
                //{
                //    grid.Columns["Action"].Visible = false;
                //}

                
            }
            
        }

        protected bool CheckCondition(string val)
        {
            bool condition = false;
            if (val == "1") condition = true;
            else if (val == "0") condition = false;
            return condition;
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                bool _january = CheckCondition(e.NewValues["January"].ToString());
                bool _february = CheckCondition(e.NewValues["February"].ToString());
                bool _march = CheckCondition(e.NewValues["March"].ToString());

                bool _april = CheckCondition(e.NewValues["April"].ToString());
                bool _may = CheckCondition(e.NewValues["May"].ToString());
                bool _june = CheckCondition(e.NewValues["June"].ToString());

                bool _july = CheckCondition(e.NewValues["July"].ToString());
                bool _august = CheckCondition(e.NewValues["August"].ToString());
                bool _september = CheckCondition(e.NewValues["September"].ToString());

                bool _october = CheckCondition(e.NewValues["October"].ToString());
                bool _november = CheckCondition(e.NewValues["November"].ToString());
                bool _december = CheckCondition(e.NewValues["December"].ToString());

                int _siteId = (Int32)e.OldValues["SiteId"];
                int _companyId = (Int32)e.OldValues["CompanyId"];


                //int _locationSponsorUserId = Convert.ToInt32(e.OldValues["LocationSponsorUserId"]);
                //int _locationSpaUserId = Convert.ToInt32(e.OldValues["LocationSpaUserId"]);

                int _contractid = Convert.ToInt32(e.Keys["ContractId"]);

                ContractReviewsRegisterService crrService = new ContractReviewsRegisterService();

                KaiZen.CSMS.Entities.ContractReviewsRegister crr = crrService.GetByContractId(_contractid);
                int? _locationSponsorUserId = crr.LocationSponsorUserId;
                int? _locationSpaUserId = crr.LocationSpaUserId;

                int _year = Convert.ToInt32(cmbYear.Value);

                ods.UpdateParameters.Add("CompanyId", TypeCode.Int32, _companyId.ToString());
                ods.UpdateParameters.Add("SiteId", TypeCode.Int32, _siteId.ToString());
                ods.UpdateParameters.Add("January", TypeCode.Boolean, _january.ToString());
                ods.UpdateParameters.Add("February", TypeCode.Boolean, _february.ToString());
                ods.UpdateParameters.Add("March", TypeCode.Boolean, _march.ToString());
                ods.UpdateParameters.Add("April", TypeCode.Boolean, _april.ToString());
                ods.UpdateParameters.Add("May", TypeCode.Boolean, _may.ToString());
                ods.UpdateParameters.Add("June", TypeCode.Boolean, _june.ToString());
                ods.UpdateParameters.Add("July", TypeCode.Boolean, _july.ToString());
                ods.UpdateParameters.Add("August", TypeCode.Boolean, _august.ToString());
                ods.UpdateParameters.Add("September", TypeCode.Boolean, _september.ToString());
                ods.UpdateParameters.Add("October", TypeCode.Boolean, _october.ToString());
                ods.UpdateParameters.Add("November", TypeCode.Boolean, _november.ToString());
                ods.UpdateParameters.Add("December", TypeCode.Boolean, _december.ToString());
                ods.UpdateParameters.Add("ContractId", TypeCode.Int32, _contractid.ToString());
                ods.UpdateParameters.Add("UserId", TypeCode.Int32, _contractid.ToString());
                ods.UpdateParameters.Add("Year", TypeCode.Int32, _year.ToString());
                //ods.UpdateParameters.Add("LocationSponsorUserId", TypeCode.Int32, _locationSponsorUserId.ToString());
                //ods.UpdateParameters.Add("LocationSpaUserId", TypeCode.Int32, _locationSpaUserId.ToString());
                
                ods.Update();

                //Auth auth = new Auth();


                //transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                //ContractReviewsRegisterService conser = new ContractReviewsRegisterService();
                ////KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(_companyId, _siteId, _year);

                //KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByContractId(_contractid);


                //KaiZen.CSMS.Entities.ContractReviewsRegister contUpdate = new KaiZen.CSMS.Entities.ContractReviewsRegister();

                //contUpdate.ContractId = cont.ContractId;
                //contUpdate.Year = _year;

                //contUpdate.CompanyId = _companyId;
                //contUpdate.SiteId = _siteId;

                //contUpdate.January = _january;
                //contUpdate.February = _february;
                //contUpdate.March = _march;
                //contUpdate.April = _april;
                //contUpdate.May = _may;

                //contUpdate.June = _june;
                //contUpdate.July = _july;
                //contUpdate.August = _august;
                //contUpdate.September = _september;
                //contUpdate.October = _october;
                //contUpdate.November = _november;
                //contUpdate.December = _december;

                //if (contUpdate.EntityState != EntityState.Unchanged)
                //{
                //    contUpdate.ModifiedBy = auth.UserId;
                //    contUpdate.ModifiedDate = DateTime.Now;
                //    //conser.Save(contNew);
                //    DataRepository.ContractReviewsRegisterProvider.Update(transactionManager, contUpdate);
                //    transactionManager.Commit();
                //}



            }
            catch (Exception ex)
            {

            }

        }
        

        protected void grid_ProcessColumnAutoFilter(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAutoFilterEventArgs e)
        {
           
            int filterCount = 0;
            string whereCondition = string.Empty;

            if (e.Column.FieldName == "CompanyId")
            {
                //if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                //{
                    e.Criteria = new OperandProperty("CompanyId") == e.Value.ToString();
                    Session["CompanyIdVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                //}
            }

            if (e.Column.FieldName == "SiteId")
            {
                //if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                //{
                    e.Criteria = new OperandProperty("SiteId") == e.Value.ToString();
                    Session["SiteIdVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                //}
            }
            if (e.Column.FieldName == "January")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("February") == e.Value.ToString();
                    Session["JanVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "February")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("February") == e.Value.ToString();
                    Session["FebVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "March")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("March") == e.Value.ToString();
                    Session["MarVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "April")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("April") == e.Value.ToString();
                    Session["AprVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "May")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("May") == e.Value.ToString();
                    Session["MayVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "June")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("June") == e.Value.ToString();
                    Session["JunVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "July")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    //  e.Criteria = new OperandProperty("July") == e.Value.ToString();
                    Session["JulVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "August")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    //  e.Criteria = new OperandProperty("August") == e.Value.ToString();
                    Session["AugVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "September")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    // e.Criteria = new OperandProperty("September") == e.Value.ToString();
                    Session["SepVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "October")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    //  e.Criteria = new OperandProperty("October") == e.Value.ToString();
                    Session["OctVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "November")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    //  e.Criteria = new OperandProperty("November") == e.Value.ToString();
                    Session["NovVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "December")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    //  e.Criteria = new OperandProperty("December") == e.Value.ToString();
                    Session["DecVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "YearCount")
            {
                //if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                //{
                    //e.Criteria = new OperandProperty("YearCount") == e.Value.ToString();
                    Session["YrCountVal"] = e.Criteria.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
               // }
            }
            if (e.Column.FieldName == "LocationSponsorUserId")
            {
                //if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                //{
                    e.Criteria = new OperandProperty("LocationSponsorUserId") == e.Value.ToString();
                    Session["LocSPUVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                //}
            }
            if (e.Column.FieldName == "LocationSpaUserId")
            {
                //if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                //{
                    e.Criteria = new OperandProperty("LocationSpaUserId") == e.Value.ToString();
                    Session["LocSPAVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                   // grid.PageIndex = 0;
                //}
            }
            grid.DataBind();

        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            if (e.CallbackName == "APPLYCOLUMNFILTER")
            {
                grid.PageIndex = 0;
            }

            if (e.CallbackName == "APPLYFILTER")
            {
                Session["CompanyIdVal"] = null;
                Session["SiteIdVal"] = null;
                Session["filterCount"] = null;
                Session["JanVal"] = null;
                Session["FebVal"] = null;
                Session["MarVal"] = null;
                Session["AprVal"] = null;

                Session["MayVal"] = null;
                Session["JunVal"] = null;
                Session["JulVal"] = null;
                Session["AugVal"] = null;

                Session["SepVal"] = null;
                Session["OctVal"] = null;
                Session["NovVal"] = null;
                Session["DecVal"] = null;

                Session["YrCountVal"] = null;
                Session["LocSPUVal"] = null;
                Session["LocSPAVal"] = null;

                //Session["WhereCondition"] = null;

                grid.PageIndex = 0;

                grid.DataBind();
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbYear.SelectedItem.ToString() != "")
                {                   
                    ContractReviewsRegisterService compSer = new ContractReviewsRegisterService();
                    TList<KaiZen.CSMS.Entities.ContractReviewsRegister> comp = compSer.GetByYear(Convert.ToInt32(cmbYear.Value));

                        if (comp.Count == 0)
                        {
                            InsertAllRows();
                        }
                    Session["YearFilter"] = cmbYear.Value.ToString();
                    Session["ShowAll"] = false;
                    
                    //ods.SelectParameters["_year"].DefaultValue = cmbYear.Value.ToString();
                    grid.DataBind();
                }
            }
            catch (Exception)
            {


            }
        }

        protected void grid_PageIndexChanged(object sender, EventArgs e)
        {
            ViewState["CurrPageIndex"] = grid.PageIndex;
        }


        protected void grid_FilterControlCustomValueDisplayText(object sender, FilterControlCustomValueDisplayTextEventArgs e)
        {
            
           //string str= e.DisplayText;
           //if (e.PropertyInfo.PropertyName.ToString() == "CompanyId")
           //{
           //    if (e.Value != null)
           //    {
           //        Session["CompanyIdVal"] = e.Value.ToString();
           //        Session["CompanyIdFilter"] = "CompanyId";
           //    }
           //}
           //if (e.PropertyInfo.PropertyName.ToString() == "SiteId")
           //{
           //    if (e.Value != null)
           //    {
           //        Session["SiteIdVal"] = e.Value.ToString();
           //        Session["SiteIdFilter"] = "SiteId";
           //    }
           //}
           //if (Session["SiteIdFilter"] == null)
           //{ 
                
           //}
        }

        protected void grid_CustomFilterExpressionDisplayText(object sender, CustomFilterExpressionDisplayTextEventArgs e)
        {
          // string strDisplay= e.DisplayText;
          // string strFilter = e.FilterExpression;
          // string criteria = e.Criteria.ToString();
          // Session["WhereCondition"] = strFilter;
          // grid.PageIndex = 0;           
          // ods.DataBind();
          // grid.DataSourceID = "ods";
          // grid.DataBind();
          //// grid.DataBind();
           
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session["ShowAll"] != null)
            {
                if (Convert.ToBoolean(Session["ShowAll"]) == false)
                {
                    btnShowAll.Text = "Show Pager";
                    Session["ShowAll"] = true;
                    grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
                    //grid.DataSourceForceStandardPaging = false;
                    //SqlDSContractRiviewsRegister.SelectParameters["Year"].DefaultValue = cmbYear.SelectedItem.ToString();
                    //grid.DataSourceID = "SqlDSContractRiviewsRegister";                   
                    grid.DataBind();
                }
                else
                {
                   // Response.Redirect("ContractReviewsRegister.aspx", true);
                    btnShowAll.Text = "Show All";
                    Session["ShowAll"] = false;
                    grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                 
                   // grid.DataSourceForceStandardPaging = true;
                    //grid.DataSourceID = "ods";
                    grid.DataBind();
                }
            }

        }
        
    }


}