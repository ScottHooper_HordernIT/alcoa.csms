﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialResponseAuditService
    {
        QuestionnaireInitialResponseAudit Get(Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes);
        List<QuestionnaireInitialResponseAudit> GetMany(int? top, Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, Func<IQueryable<QuestionnaireInitialResponseAudit>, IOrderedQueryable<QuestionnaireInitialResponseAudit>> orderBy, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes);
        List<QuestionnaireInitialResponseAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, Func<IQueryable<QuestionnaireInitialResponseAudit>, IOrderedQueryable<QuestionnaireInitialResponseAudit>> orderBy, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes);
        void Insert(QuestionnaireInitialResponseAudit item, bool saveChanges = true);
        void Update(QuestionnaireInitialResponseAudit item, bool saveChanges = true);
        void Delete(QuestionnaireInitialResponseAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialResponseAuditService : IQuestionnaireInitialResponseAuditService
    {
        private readonly IRepository<QuestionnaireInitialResponseAudit> _repository;

        public QuestionnaireInitialResponseAuditService(IRepository<QuestionnaireInitialResponseAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialResponseAudit Get(Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialResponseAudit> GetMany(int? top, Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, Func<IQueryable<QuestionnaireInitialResponseAudit>, IOrderedQueryable<QuestionnaireInitialResponseAudit>> orderBy, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialResponseAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, Func<IQueryable<QuestionnaireInitialResponseAudit>, IOrderedQueryable<QuestionnaireInitialResponseAudit>> orderBy, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialResponseAudit, bool>> where, List<Expression<Func<QuestionnaireInitialResponseAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialResponseAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialResponseAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialResponseAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialResponseAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialResponseAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialResponseAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
