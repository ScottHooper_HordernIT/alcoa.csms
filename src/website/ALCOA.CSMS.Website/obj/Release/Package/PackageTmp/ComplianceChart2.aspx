﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="ComplianceChart2" Codebehind="ComplianceChart2.aspx.cs" %>

<%@ Register src="UserControls/Main/ComplianceChart2.ascx" tagname="ComplianceChart2" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
    <uc1:ComplianceChart2 ID="ComplianceChart" runat="server" />
</asp:Content>

