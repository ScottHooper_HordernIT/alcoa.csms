﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EhsimsExceptionsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EhsimsExceptionsDataSourceDesigner))]
	public class EhsimsExceptionsDataSource : ProviderDataSource<EhsimsExceptions, EhsimsExceptionsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsDataSource class.
		/// </summary>
		public EhsimsExceptionsDataSource() : base(new EhsimsExceptionsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EhsimsExceptionsDataSourceView used by the EhsimsExceptionsDataSource.
		/// </summary>
		protected EhsimsExceptionsDataSourceView EhsimsExceptionsView
		{
			get { return ( View as EhsimsExceptionsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EhsimsExceptionsDataSource control invokes to retrieve data.
		/// </summary>
		public EhsimsExceptionsSelectMethod SelectMethod
		{
			get
			{
				EhsimsExceptionsSelectMethod selectMethod = EhsimsExceptionsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EhsimsExceptionsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EhsimsExceptionsDataSourceView class that is to be
		/// used by the EhsimsExceptionsDataSource.
		/// </summary>
		/// <returns>An instance of the EhsimsExceptionsDataSourceView class.</returns>
		protected override BaseDataSourceView<EhsimsExceptions, EhsimsExceptionsKey> GetNewDataSourceView()
		{
			return new EhsimsExceptionsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EhsimsExceptionsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EhsimsExceptionsDataSourceView : ProviderDataSourceView<EhsimsExceptions, EhsimsExceptionsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EhsimsExceptionsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EhsimsExceptionsDataSourceView(EhsimsExceptionsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EhsimsExceptionsDataSource EhsimsExceptionsOwner
		{
			get { return Owner as EhsimsExceptionsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EhsimsExceptionsSelectMethod SelectMethod
		{
			get { return EhsimsExceptionsOwner.SelectMethod; }
			set { EhsimsExceptionsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EhsimsExceptionsService EhsimsExceptionsProvider
		{
			get { return Provider as EhsimsExceptionsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EhsimsExceptions> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EhsimsExceptions> results = null;
			EhsimsExceptions item;
			count = 0;
			
			System.Int32 _ehsimsExceptionId;

			switch ( SelectMethod )
			{
				case EhsimsExceptionsSelectMethod.Get:
					EhsimsExceptionsKey entityKey  = new EhsimsExceptionsKey();
					entityKey.Load(values);
					item = EhsimsExceptionsProvider.Get(entityKey);
					results = new TList<EhsimsExceptions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EhsimsExceptionsSelectMethod.GetAll:
                    results = EhsimsExceptionsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EhsimsExceptionsSelectMethod.GetPaged:
					results = EhsimsExceptionsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EhsimsExceptionsSelectMethod.Find:
					if ( FilterParameters != null )
						results = EhsimsExceptionsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EhsimsExceptionsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EhsimsExceptionsSelectMethod.GetByEhsimsExceptionId:
					_ehsimsExceptionId = ( values["EhsimsExceptionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EhsimsExceptionId"], typeof(System.Int32)) : (int)0;
					item = EhsimsExceptionsProvider.GetByEhsimsExceptionId(_ehsimsExceptionId);
					results = new TList<EhsimsExceptions>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EhsimsExceptionsSelectMethod.Get || SelectMethod == EhsimsExceptionsSelectMethod.GetByEhsimsExceptionId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EhsimsExceptions entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EhsimsExceptionsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EhsimsExceptions> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EhsimsExceptionsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EhsimsExceptionsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EhsimsExceptionsDataSource class.
	/// </summary>
	public class EhsimsExceptionsDataSourceDesigner : ProviderDataSourceDesigner<EhsimsExceptions, EhsimsExceptionsKey>
	{
		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsDataSourceDesigner class.
		/// </summary>
		public EhsimsExceptionsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EhsimsExceptionsSelectMethod SelectMethod
		{
			get { return ((EhsimsExceptionsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EhsimsExceptionsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EhsimsExceptionsDataSourceActionList

	/// <summary>
	/// Supports the EhsimsExceptionsDataSourceDesigner class.
	/// </summary>
	internal class EhsimsExceptionsDataSourceActionList : DesignerActionList
	{
		private EhsimsExceptionsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EhsimsExceptionsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EhsimsExceptionsDataSourceActionList(EhsimsExceptionsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EhsimsExceptionsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EhsimsExceptionsDataSourceActionList
	
	#endregion EhsimsExceptionsDataSourceDesigner
	
	#region EhsimsExceptionsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EhsimsExceptionsDataSource.SelectMethod property.
	/// </summary>
	public enum EhsimsExceptionsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEhsimsExceptionId method.
		/// </summary>
		GetByEhsimsExceptionId
	}
	
	#endregion EhsimsExceptionsSelectMethod

	#region EhsimsExceptionsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsFilter : SqlFilter<EhsimsExceptionsColumn>
	{
	}
	
	#endregion EhsimsExceptionsFilter

	#region EhsimsExceptionsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsExpressionBuilder : SqlExpressionBuilder<EhsimsExceptionsColumn>
	{
	}
	
	#endregion EhsimsExceptionsExpressionBuilder	

	#region EhsimsExceptionsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EhsimsExceptionsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EhsimsExceptions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsimsExceptionsProperty : ChildEntityProperty<EhsimsExceptionsChildEntityTypes>
	{
	}
	
	#endregion EhsimsExceptionsProperty
}

