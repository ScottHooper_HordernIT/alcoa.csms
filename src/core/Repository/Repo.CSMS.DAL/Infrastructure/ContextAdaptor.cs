﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.CSMS.DAL.Infrastructure
{
    public class ContextAdaptor : IObjectSetFactory, IObjectContext
    {
        private readonly ObjectContext _context;

        public ContextAdaptor(IDbContext context)
        {
            _context = context.GetObjectContext();
        }

        public ObjectContext Context
        {
            get { return _context; }
        }

        public void SaveChanges()
        {
            int i = _context.SaveChanges();
        }

        public ObjectContextOptions ContextOptions
        {
            get { return _context.ContextOptions; }
        }

        public IObjectSet<T> CreateObjectSet<T>() where T : class
        {
            return _context.CreateObjectSet<T>();
        }

        public void ChangeObjectState(object entity, EntityState state)
        {
            _context.ObjectStateManager.ChangeObjectState(entity, state);
        }

        public bool IsObjectAttached(System.Data.Entity.Core.EntityKey key)
        {
            ObjectStateEntry entry = null;
            return this.Context.ObjectStateManager.TryGetObjectStateEntry(key, out entry);
        }

        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing && _context != null)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
