﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using System.Data;
using model = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;
using System.Linq;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class KpiVariance : System.Web.UI.UserControl
    {
        DataSet ds = null;
        repo.IRegionService regionService = ALCOA.CSMS.Website.Global.GetInstance<repo.IRegionService>();
        repo.IRegionsSiteService regionsSiteService = ALCOA.CSMS.Website.Global.GetInstance<repo.IRegionsSiteService>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                cbMonth.Items.Clear();
                MonthsService mService = new MonthsService();
                TList<Months> mTlist = mService.GetAll();
                foreach (Months m in mTlist)
                {
                    cbMonth.Items.Add(m.MonthName, m.MonthId);
                    if (m.MonthId == DateTime.Now.Month) cbMonth.Text = m.MonthName;
                }

                cbMonth.Value = DateTime.Now.Month;

                cbYear.DataSourceID = "sqldsKpi_GetAllYearsSubmitted";
                cbYear.TextField = "Year";
                cbYear.ValueField = "Year";
                cbYear.DataBind();
                cbYear.Value = DateTime.Now.Year;

                cbCategory.DataSourceID = "CompanySiteCategoryDataSource";
                cbCategory.TextField = "CategoryDesc";
                cbCategory.ValueField = "CompanySiteCategoryId";
                cbCategory.DataBind();
                cbCategory.Items.Add("-----------------------------", 0);
                cbCategory.Items.Add("All", -1);
                cbCategory.Value = -1;

                //Modified by Ashley Goldstraw DT 260
                //DataTable dt = new DataTable();
                //dt.Columns.Add("RegionId", typeof(int));
                //dt.Columns.Add("RegionName", typeof(string));
                List<model.Region> regions = regionService.GetMany(null, null, null, null);

                cbRegion.TextField = "RegionName";
                cbRegion.ValueField = "RegionId";
                cbRegion.DataSource = regions;
                cbRegion.DataBind();
                cbRegion.Items.Add("-------------------------", 0);
                cbRegion.Items.Add("All", -1);
                cbRegion.Value = -1;
                Session["dtVariance"] = GetVariance(DateTime.Now.Month, DateTime.Now.Year, "1");
            }
            else
            {
                ds = (DataSet)Session["dtVariance"];
            }
            DataBind();
        }

        protected void DataBind()
        {
            grid.DataSource = ds.Tables[0];
            grid.DataBind();
        }
        private bool SiteIsInRegion(int siteId,List<model.RegionsSite> regionSiteList) //Method added by Ashley Goldstraw DT 260
        {
            if (regionSiteList.Where(i => i.SiteId == siteId).Count() > 0)
                return true;
            else
                return false;
        }
        protected DataSet GetVariance(int currentMonth, int currentYear, string companySiteCategoryId)
        {
            int VarianceId = 1;

            ds = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("VarianceId", typeof(int));
            dt.Columns.Add("CompanyName", typeof(string));
            dt.Columns.Add("SiteName", typeof(string));
            dt.Columns.Add("Hours_CurrentMonth", typeof(int));
            dt.Columns.Add("Hours_PreviousMonth", typeof(int));
            dt.Columns.Add("Variance_CurrentVsPrevious", typeof(int));
            dt.Columns.Add("Difference_CurrentVsPrevious", typeof(int));
            //dt.Columns.Add("AvgVariance_Last12Months", typeof(string));
            dt.PrimaryKey = new DataColumn[] { dt.Columns["CompanySiteCategoryStandardId"] };
            ds.Tables.Add(dt);
            int regionId = (int)cbRegion.Value;
            //Added by ashley Goldstraw DT 260
            List<model.RegionsSite> regionSites = new List<model.RegionsSite>();
            if (regionId > 0)
            {
                regionSites = regionsSiteService.GetMany(null, i => i.RegionId == regionId, null, null);
            }

            KpiService kService = new KpiService();
            using (DataSet dsVariance = kService.GetAll_Last12MonthsHoursEntered(currentMonth, currentYear, companySiteCategoryId))
            {
                if(dsVariance.Tables[0] != null)
                {
                    if (dsVariance.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsVariance.Tables[0].Rows)
                        {
                            //If statement added by Ashley Goldstraw to enable selecting by Region DT260
                            if (regionId <= 0 || SiteIsInRegion((int)dr["SiteId"],regionSites))
                            {
                            DataRow newdr = dt.NewRow();
                            newdr["VarianceId"] = VarianceId;
                            VarianceId++;

                            newdr["CompanyName"] = dr["CompanyName"];
                            newdr["SiteName"] = dr["SiteName"];

                            int HoursCurrent = 0;
                            int HoursPrevious_1 = 0;

                            Int32.TryParse(dr["HoursCurrent"].ToString(), out HoursCurrent);
                            Int32.TryParse(dr["HoursPrevious_1"].ToString(), out HoursPrevious_1);

                            if (!String.IsNullOrEmpty(dr["HoursCurrent"].ToString()))
                                newdr["Hours_CurrentMonth"] = HoursCurrent.ToString();
                            else
                                newdr["Hours_CurrentMonth"] = 0;

                            if (!String.IsNullOrEmpty(dr["HoursPrevious_1"].ToString()))
                                newdr["Hours_PreviousMonth"] = HoursPrevious_1.ToString();
                            else
                                newdr["Hours_PreviousMonth"] = 0;


                            Decimal Difference = Convert.ToDecimal(HoursCurrent) - Convert.ToDecimal(HoursPrevious_1);
                            if (Difference < 0) Difference = -1 * Difference;
                            newdr["Difference_CurrentVsPrevious"] = Convert.ToInt32(Difference);

                            if (HoursPrevious_1 > 0)
                                newdr["Variance_CurrentVsPrevious"] = Convert.ToInt32(((Difference) / Convert.ToDecimal(HoursPrevious_1)) * 100);
                            else
                                if (Difference > 0)
                                    newdr["Variance_CurrentVsPrevious"] = 100;
                                else
                                    newdr["Variance_CurrentVsPrevious"] = 0;

                            dt.Rows.Add(newdr);
                            }
                        }
                    }
                }
            }

            return ds;
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            string Category = "";

            switch((int)cbCategory.Value)
            {
                case (int)CompanySiteCategoryList.E:
                    Category = ((int)CompanySiteCategoryList.E).ToString();
                break;
                case (int)CompanySiteCategoryList.NE1:
                    Category = ((int)CompanySiteCategoryList.NE1).ToString();
                break;
                case (int)CompanySiteCategoryList.NE2:
                    Category = ((int)CompanySiteCategoryList.NE2).ToString();
                break;
                case (int)CompanySiteCategoryList.NE3:
                    Category = ((int)CompanySiteCategoryList.NE3).ToString();
                break;
                case 0:
                    throw new Exception("Select Category.");
                default:
                    Category = "%";
                break;
            }

            Session["dtVariance"] = GetVariance((int)cbMonth.Value, (int)cbYear.Value, Category);
            DataBind();
        }
    }
}