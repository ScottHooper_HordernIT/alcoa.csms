﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileDbAdminProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FileDbAdminProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.FileDbAdmin, KaiZen.CSMS.Entities.FileDbAdminKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAdminKey key)
		{
			return Delete(transactionManager, key.FileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_fileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _fileId)
		{
			return Delete(null, _fileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _fileId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.FileDbAdmin Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAdminKey key, int start, int pageLength)
		{
			return GetByFileId(transactionManager, key.FileId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileDbAdmin index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAdmin GetByFileId(System.Int32 _fileId)
		{
			int count = -1;
			return GetByFileId(null,_fileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDbAdmin index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAdmin GetByFileId(System.Int32 _fileId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileId(null, _fileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDbAdmin index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAdmin GetByFileId(TransactionManager transactionManager, System.Int32 _fileId)
		{
			int count = -1;
			return GetByFileId(transactionManager, _fileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDbAdmin index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAdmin GetByFileId(TransactionManager transactionManager, System.Int32 _fileId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileId(transactionManager, _fileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDbAdmin index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAdmin GetByFileId(System.Int32 _fileId, int start, int pageLength, out int count)
		{
			return GetByFileId(null, _fileId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDbAdmin index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileDbAdmin GetByFileId(TransactionManager transactionManager, System.Int32 _fileId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileDbAdmin&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileDbAdmin&gt;"/></returns>
		public static TList<FileDbAdmin> Fill(IDataReader reader, TList<FileDbAdmin> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.FileDbAdmin c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FileDbAdmin")
					.Append("|").Append((System.Int32)reader[((int)FileDbAdminColumn.FileId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FileDbAdmin>(
					key.ToString(), // EntityTrackingKey
					"FileDbAdmin",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.FileDbAdmin();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FileId = (System.Int32)reader[((int)FileDbAdminColumn.FileId - 1)];
					c.OriginalFileId = c.FileId;
					c.FileName = (System.String)reader[((int)FileDbAdminColumn.FileName - 1)];
					c.Description = (reader.IsDBNull(((int)FileDbAdminColumn.Description - 1)))?null:(System.String)reader[((int)FileDbAdminColumn.Description - 1)];
					c.Content = (System.Byte[])reader[((int)FileDbAdminColumn.Content - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)FileDbAdminColumn.ModifiedByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.FileDbAdmin entity)
		{
			if (!reader.Read()) return;
			
			entity.FileId = (System.Int32)reader[((int)FileDbAdminColumn.FileId - 1)];
			entity.OriginalFileId = (System.Int32)reader["FileId"];
			entity.FileName = (System.String)reader[((int)FileDbAdminColumn.FileName - 1)];
			entity.Description = (reader.IsDBNull(((int)FileDbAdminColumn.Description - 1)))?null:(System.String)reader[((int)FileDbAdminColumn.Description - 1)];
			entity.Content = (System.Byte[])reader[((int)FileDbAdminColumn.Content - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileDbAdminColumn.ModifiedByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.FileDbAdmin entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileId = (System.Int32)dataRow["FileId"];
			entity.OriginalFileId = (System.Int32)dataRow["FileId"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.Content = (System.Byte[])dataRow["Content"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbAdmin"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDbAdmin Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAdmin entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileDbAdmin object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileDbAdmin instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDbAdmin Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAdmin entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileDbAdminChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileDbAdmin</c>
	///</summary>
	public enum FileDbAdminChildEntityTypes
	{
	}
	
	#endregion FileDbAdminChildEntityTypes
	
	#region FileDbAdminFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileDbAdminColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminFilterBuilder : SqlFilterBuilder<FileDbAdminColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminFilterBuilder class.
		/// </summary>
		public FileDbAdminFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAdminFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAdminFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAdminFilterBuilder
	
	#region FileDbAdminParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileDbAdminColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAdminParameterBuilder : ParameterizedSqlFilterBuilder<FileDbAdminColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminParameterBuilder class.
		/// </summary>
		public FileDbAdminParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAdminParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAdminParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAdminParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAdminParameterBuilder
	
	#region FileDbAdminSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileDbAdminColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAdmin"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileDbAdminSortBuilder : SqlSortBuilder<FileDbAdminColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAdminSqlSortBuilder class.
		/// </summary>
		public FileDbAdminSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileDbAdminSortBuilder
	
} // end namespace
