﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.YearlyTargetsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(YearlyTargetsDataSourceDesigner))]
	public class YearlyTargetsDataSource : ProviderDataSource<YearlyTargets, YearlyTargetsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsDataSource class.
		/// </summary>
		public YearlyTargetsDataSource() : base(new YearlyTargetsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the YearlyTargetsDataSourceView used by the YearlyTargetsDataSource.
		/// </summary>
		protected YearlyTargetsDataSourceView YearlyTargetsView
		{
			get { return ( View as YearlyTargetsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the YearlyTargetsDataSource control invokes to retrieve data.
		/// </summary>
		public YearlyTargetsSelectMethod SelectMethod
		{
			get
			{
				YearlyTargetsSelectMethod selectMethod = YearlyTargetsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (YearlyTargetsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the YearlyTargetsDataSourceView class that is to be
		/// used by the YearlyTargetsDataSource.
		/// </summary>
		/// <returns>An instance of the YearlyTargetsDataSourceView class.</returns>
		protected override BaseDataSourceView<YearlyTargets, YearlyTargetsKey> GetNewDataSourceView()
		{
			return new YearlyTargetsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the YearlyTargetsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class YearlyTargetsDataSourceView : ProviderDataSourceView<YearlyTargets, YearlyTargetsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the YearlyTargetsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public YearlyTargetsDataSourceView(YearlyTargetsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal YearlyTargetsDataSource YearlyTargetsOwner
		{
			get { return Owner as YearlyTargetsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal YearlyTargetsSelectMethod SelectMethod
		{
			get { return YearlyTargetsOwner.SelectMethod; }
			set { YearlyTargetsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal YearlyTargetsService YearlyTargetsProvider
		{
			get { return Provider as YearlyTargetsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<YearlyTargets> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<YearlyTargets> results = null;
			YearlyTargets item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case YearlyTargetsSelectMethod.Get:
					YearlyTargetsKey entityKey  = new YearlyTargetsKey();
					entityKey.Load(values);
					item = YearlyTargetsProvider.Get(entityKey);
					results = new TList<YearlyTargets>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case YearlyTargetsSelectMethod.GetAll:
                    results = YearlyTargetsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case YearlyTargetsSelectMethod.GetPaged:
					results = YearlyTargetsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case YearlyTargetsSelectMethod.Find:
					if ( FilterParameters != null )
						results = YearlyTargetsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = YearlyTargetsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case YearlyTargetsSelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = YearlyTargetsProvider.GetById(_id);
					results = new TList<YearlyTargets>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == YearlyTargetsSelectMethod.Get || SelectMethod == YearlyTargetsSelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				YearlyTargets entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					YearlyTargetsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<YearlyTargets> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			YearlyTargetsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region YearlyTargetsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the YearlyTargetsDataSource class.
	/// </summary>
	public class YearlyTargetsDataSourceDesigner : ProviderDataSourceDesigner<YearlyTargets, YearlyTargetsKey>
	{
		/// <summary>
		/// Initializes a new instance of the YearlyTargetsDataSourceDesigner class.
		/// </summary>
		public YearlyTargetsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public YearlyTargetsSelectMethod SelectMethod
		{
			get { return ((YearlyTargetsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new YearlyTargetsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region YearlyTargetsDataSourceActionList

	/// <summary>
	/// Supports the YearlyTargetsDataSourceDesigner class.
	/// </summary>
	internal class YearlyTargetsDataSourceActionList : DesignerActionList
	{
		private YearlyTargetsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the YearlyTargetsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public YearlyTargetsDataSourceActionList(YearlyTargetsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public YearlyTargetsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion YearlyTargetsDataSourceActionList
	
	#endregion YearlyTargetsDataSourceDesigner
	
	#region YearlyTargetsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the YearlyTargetsDataSource.SelectMethod property.
	/// </summary>
	public enum YearlyTargetsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion YearlyTargetsSelectMethod

	#region YearlyTargetsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsFilter : SqlFilter<YearlyTargetsColumn>
	{
	}
	
	#endregion YearlyTargetsFilter

	#region YearlyTargetsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsExpressionBuilder : SqlExpressionBuilder<YearlyTargetsColumn>
	{
	}
	
	#endregion YearlyTargetsExpressionBuilder	

	#region YearlyTargetsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;YearlyTargetsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="YearlyTargets"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class YearlyTargetsProperty : ChildEntityProperty<YearlyTargetsChildEntityTypes>
	{
	}
	
	#endregion YearlyTargetsProperty
}

