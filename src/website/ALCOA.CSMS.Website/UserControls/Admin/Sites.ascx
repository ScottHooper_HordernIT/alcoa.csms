﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_Sites" Codebehind="Sites.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<dxwgv:aspxgridview id="ASPxGridView1" runat="server" autogeneratecolumns="False"
    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
    datasourceid="dsSitesFilter" keyfieldname="SiteId" 
    OnRowUpdating="grid_RowUpdating" OnRowInserting="grid_RowInserting">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>

<FilterRowButton Height="13px" Width="13px"></FilterRowButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
</Images>

<SettingsEditing Mode="Inline"></SettingsEditing>
<Columns>
<dxwgv:GridViewCommandColumn VisibleIndex="0">
<EditButton Visible="True"></EditButton>
    <NewButton Visible="True">
    </NewButton>
</dxwgv:GridViewCommandColumn>
<dxwgv:GridViewDataTextColumn FieldName="SiteId" Visible="False" VisibleIndex="0">
<EditFormSettings Visible="False"></EditFormSettings>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="SiteName" VisibleIndex="1">
    <PropertiesTextEdit MaxLength="50">
    </PropertiesTextEdit>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="SiteAddress" Caption="Address" VisibleIndex="2">
    <PropertiesTextEdit MaxLength="200">
    </PropertiesTextEdit>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="SiteMainContactNo" Caption="Contact #" VisibleIndex="3">
    <PropertiesTextEdit MaxLength="50">
    </PropertiesTextEdit>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="ModifiedbyUserId" Visible="False" VisibleIndex="3"></dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="SiteAbbrev" Caption="Site Abbrev." VisibleIndex="4">
    <PropertiesTextEdit MaxLength="5">
    </PropertiesTextEdit>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="LocCode" Caption="Location Code" VisibleIndex="4">
    <PropertiesTextEdit MaxLength="3">
    </PropertiesTextEdit>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="IprocCode" Caption="iProc Code" VisibleIndex="5"></dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataCheckColumn Caption="Visible" FieldName="IsVisible" VisibleIndex="6">
    </dxwgv:GridViewDataCheckColumn>
    <dxwgv:GridViewDataTextColumn Caption="Ordinal" FieldName="Ordinal" VisibleIndex="7" Visible="false">
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataComboBoxColumn Caption="EHS Consultant" FieldName="EhsConsultantId"
                    VisibleIndex="999">
                    <PropertiesComboBox DataSourceID="UsersEhsConsultantsDataSource1" DropDownHeight="150px"
                        IncrementalFilteringMode="StartsWith" TextField="UserFullNameLogon" ValueField="EhsConsultantId"
                        ValueType="System.Int32">
                    </PropertiesComboBox>
                    <Settings SortMode="DisplayText" />
                </dxwgv:GridViewDataComboBoxColumn>
</Columns>
<Settings ShowHeaderFilterButton="true" />
    <SettingsPager NumericButtonCount="999" PageSize="999">
        <AllButton Visible="True"></AllButton>
    </SettingsPager>
    <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
</dxwgv:aspxgridview>

<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="Editable = True" Sort="SiteName ASC"></data:SitesDataSource>

<br />
<br />
<strong>Regions</strong><br />
<asp:PlaceHolder ID="phRegions" runat="server"></asp:PlaceHolder>
<br />
<br />
<strong>Regions-Sites</strong><br />
<asp:PlaceHolder ID="phRegionsSites" runat="server"></asp:PlaceHolder>

<data:UsersEhsConsultantsDataSource runat="server" ID="UsersEhsConsultantsDataSource1" Filter="Enabled = True" SelectMethod="GetPaged" Sort="UserFullNameLogon ASC">
</data:UsersEhsConsultantsDataSource>