﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskEmailLogService
    {
        AdminTaskEmailLog Get(Expression<Func<AdminTaskEmailLog, bool>> where, List<Expression<Func<AdminTaskEmailLog, object>>> includes);
        List<AdminTaskEmailLog> GetMany(int? top, Expression<Func<AdminTaskEmailLog, bool>> where, Func<IQueryable<AdminTaskEmailLog>, IOrderedQueryable<AdminTaskEmailLog>> orderBy, List<Expression<Func<AdminTaskEmailLog, object>>> includes);
        List<AdminTaskEmailLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailLog, bool>> where, Func<IQueryable<AdminTaskEmailLog>, IOrderedQueryable<AdminTaskEmailLog>> orderBy, List<Expression<Func<AdminTaskEmailLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskEmailLog, bool>> where, List<Expression<Func<AdminTaskEmailLog, object>>> includes);
        void Insert(AdminTaskEmailLog item, bool saveChanges = true);
        void Update(AdminTaskEmailLog item, bool saveChanges = true);
        void Delete(AdminTaskEmailLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskEmailLogService : IAdminTaskEmailLogService
    {
        private readonly IRepository<AdminTaskEmailLog> _repository;

        public AdminTaskEmailLogService(IRepository<AdminTaskEmailLog> repository)
        {
            this._repository = repository;
        }

        public AdminTaskEmailLog Get(Expression<Func<AdminTaskEmailLog, bool>> where, List<Expression<Func<AdminTaskEmailLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskEmailLog> GetMany(int? top, Expression<Func<AdminTaskEmailLog, bool>> where, Func<IQueryable<AdminTaskEmailLog>, IOrderedQueryable<AdminTaskEmailLog>> orderBy, List<Expression<Func<AdminTaskEmailLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskEmailLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailLog, bool>> where, Func<IQueryable<AdminTaskEmailLog>, IOrderedQueryable<AdminTaskEmailLog>> orderBy, List<Expression<Func<AdminTaskEmailLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskEmailLog, bool>> where, List<Expression<Func<AdminTaskEmailLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskEmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskEmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskEmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
