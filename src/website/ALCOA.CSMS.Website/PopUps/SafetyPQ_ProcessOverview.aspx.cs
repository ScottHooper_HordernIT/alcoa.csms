﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class PopUps_SafetyPQ_ProcessOverview : System.Web.UI.Page
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e)
    {
        Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
    }
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.CompanyStatus2Id == (int)CompanyStatus2List.SubContractor)
            {
                pnlContractor.Visible = false;
                pnlSubContractor.Visible = true;
            }
        }
    }
}
