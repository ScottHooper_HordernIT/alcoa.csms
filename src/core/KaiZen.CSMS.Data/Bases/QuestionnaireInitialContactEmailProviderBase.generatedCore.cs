﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireInitialContactEmailProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireInitialContactEmailProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailKey key)
		{
			return Delete(transactionManager, key.QuestionnaireInitialContactEmailId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireInitialContactEmailId)
		{
			return Delete(null, _questionnaireInitialContactEmailId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_Questionnaire key.
		///		FK_QuestionnaireInitialContactEmail_Questionnaire Description: 
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(_questionnaireId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_Questionnaire key.
		///		FK_QuestionnaireInitialContactEmail_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContactEmail> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_Questionnaire key.
		///		FK_QuestionnaireInitialContactEmail_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_Questionnaire key.
		///		fkQuestionnaireInitialContactEmailQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_Questionnaire key.
		///		fkQuestionnaireInitialContactEmailQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_Questionnaire key.
		///		FK_QuestionnaireInitialContactEmail_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public abstract TList<QuestionnaireInitialContactEmail> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType key.
		///		FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType Description: 
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeId(System.Int32 _emailTypeId)
		{
			int count = -1;
			return GetByEmailTypeId(_emailTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType key.
		///		FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeId(TransactionManager transactionManager, System.Int32 _emailTypeId)
		{
			int count = -1;
			return GetByEmailTypeId(transactionManager, _emailTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType key.
		///		FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeId(TransactionManager transactionManager, System.Int32 _emailTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTypeId(transactionManager, _emailTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType key.
		///		fkQuestionnaireInitialContactEmailQuestionnaireInitialContactEmailType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_emailTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeId(System.Int32 _emailTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByEmailTypeId(null, _emailTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType key.
		///		fkQuestionnaireInitialContactEmailQuestionnaireInitialContactEmailType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeId(System.Int32 _emailTypeId, int start, int pageLength,out int count)
		{
			return GetByEmailTypeId(null, _emailTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType key.
		///		FK_QuestionnaireInitialContactEmail_QuestionnaireInitialContactEmailType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public abstract TList<QuestionnaireInitialContactEmail> GetByEmailTypeId(TransactionManager transactionManager, System.Int32 _emailTypeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact key.
		///		FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact Description: 
		/// </summary>
		/// <param name="_contactId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByContactId(System.Int32 _contactId)
		{
			int count = -1;
			return GetByContactId(_contactId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact key.
		///		FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContactEmail> GetByContactId(TransactionManager transactionManager, System.Int32 _contactId)
		{
			int count = -1;
			return GetByContactId(transactionManager, _contactId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact key.
		///		FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByContactId(TransactionManager transactionManager, System.Int32 _contactId, int start, int pageLength)
		{
			int count = -1;
			return GetByContactId(transactionManager, _contactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact key.
		///		fkQuestionnaireInitialContactVerificationQuestionnaireInitialContact Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contactId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByContactId(System.Int32 _contactId, int start, int pageLength)
		{
			int count =  -1;
			return GetByContactId(null, _contactId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact key.
		///		fkQuestionnaireInitialContactVerificationQuestionnaireInitialContact Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contactId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByContactId(System.Int32 _contactId, int start, int pageLength,out int count)
		{
			return GetByContactId(null, _contactId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact key.
		///		FK_QuestionnaireInitialContactVerification_QuestionnaireInitialContact Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public abstract TList<QuestionnaireInitialContactEmail> GetByContactId(TransactionManager transactionManager, System.Int32 _contactId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_Users key.
		///		FK_QuestionnaireInitialContactVerification_Users Description: 
		/// </summary>
		/// <param name="_sentByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetBySentByUserId(System.Int32 _sentByUserId)
		{
			int count = -1;
			return GetBySentByUserId(_sentByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_Users key.
		///		FK_QuestionnaireInitialContactVerification_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sentByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContactEmail> GetBySentByUserId(TransactionManager transactionManager, System.Int32 _sentByUserId)
		{
			int count = -1;
			return GetBySentByUserId(transactionManager, _sentByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_Users key.
		///		FK_QuestionnaireInitialContactVerification_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sentByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetBySentByUserId(TransactionManager transactionManager, System.Int32 _sentByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetBySentByUserId(transactionManager, _sentByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_Users key.
		///		fkQuestionnaireInitialContactVerificationUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sentByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetBySentByUserId(System.Int32 _sentByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySentByUserId(null, _sentByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_Users key.
		///		fkQuestionnaireInitialContactVerificationUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sentByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public TList<QuestionnaireInitialContactEmail> GetBySentByUserId(System.Int32 _sentByUserId, int start, int pageLength,out int count)
		{
			return GetBySentByUserId(null, _sentByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContactVerification_Users key.
		///		FK_QuestionnaireInitialContactVerification_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sentByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail objects.</returns>
		public abstract TList<QuestionnaireInitialContactEmail> GetBySentByUserId(TransactionManager transactionManager, System.Int32 _sentByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailKey key, int start, int pageLength)
		{
			return GetByQuestionnaireInitialContactEmailId(transactionManager, key.QuestionnaireInitialContactEmailId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireInitialContactVerification index.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail GetByQuestionnaireInitialContactEmailId(System.Int32 _questionnaireInitialContactEmailId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailId(null,_questionnaireInitialContactEmailId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactVerification index.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail GetByQuestionnaireInitialContactEmailId(System.Int32 _questionnaireInitialContactEmailId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailId(null, _questionnaireInitialContactEmailId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactVerification index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail GetByQuestionnaireInitialContactEmailId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailId(transactionManager, _questionnaireInitialContactEmailId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactVerification index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail GetByQuestionnaireInitialContactEmailId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailId(transactionManager, _questionnaireInitialContactEmailId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactVerification index.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail GetByQuestionnaireInitialContactEmailId(System.Int32 _questionnaireInitialContactEmailId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireInitialContactEmailId(null, _questionnaireInitialContactEmailId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactVerification index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail GetByQuestionnaireInitialContactEmailId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialContactEmail index.
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactId(System.Int32 _emailTypeId, System.Int32 _contactId)
		{
			int count = -1;
			return GetByEmailTypeIdContactId(null,_emailTypeId, _contactId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail index.
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactId(System.Int32 _emailTypeId, System.Int32 _contactId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTypeIdContactId(null, _emailTypeId, _contactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactId(TransactionManager transactionManager, System.Int32 _emailTypeId, System.Int32 _contactId)
		{
			int count = -1;
			return GetByEmailTypeIdContactId(transactionManager, _emailTypeId, _contactId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactId(TransactionManager transactionManager, System.Int32 _emailTypeId, System.Int32 _contactId, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTypeIdContactId(transactionManager, _emailTypeId, _contactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail index.
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactId(System.Int32 _emailTypeId, System.Int32 _contactId, int start, int pageLength, out int count)
		{
			return GetByEmailTypeIdContactId(null, _emailTypeId, _contactId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public abstract TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactId(TransactionManager transactionManager, System.Int32 _emailTypeId, System.Int32 _contactId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialContactEmail_1 index.
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="_contactEmail"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactIdContactEmail(System.Int32 _emailTypeId, System.Int32 _contactId, System.String _contactEmail)
		{
			int count = -1;
			return GetByEmailTypeIdContactIdContactEmail(null,_emailTypeId, _contactId, _contactEmail, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail_1 index.
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactIdContactEmail(System.Int32 _emailTypeId, System.Int32 _contactId, System.String _contactEmail, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTypeIdContactIdContactEmail(null, _emailTypeId, _contactId, _contactEmail, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="_contactEmail"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactIdContactEmail(TransactionManager transactionManager, System.Int32 _emailTypeId, System.Int32 _contactId, System.String _contactEmail)
		{
			int count = -1;
			return GetByEmailTypeIdContactIdContactEmail(transactionManager, _emailTypeId, _contactId, _contactEmail, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactIdContactEmail(TransactionManager transactionManager, System.Int32 _emailTypeId, System.Int32 _contactId, System.String _contactEmail, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailTypeIdContactIdContactEmail(transactionManager, _emailTypeId, _contactId, _contactEmail, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail_1 index.
		/// </summary>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactIdContactEmail(System.Int32 _emailTypeId, System.Int32 _contactId, System.String _contactEmail, int start, int pageLength, out int count)
		{
			return GetByEmailTypeIdContactIdContactEmail(null, _emailTypeId, _contactId, _contactEmail, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmail_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailTypeId"></param>
		/// <param name="_contactId"></param>
		/// <param name="_contactEmail"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/> class.</returns>
		public abstract TList<QuestionnaireInitialContactEmail> GetByEmailTypeIdContactIdContactEmail(TransactionManager transactionManager, System.Int32 _emailTypeId, System.Int32 _contactId, System.String _contactEmail, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireInitialContactEmail&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireInitialContactEmail&gt;"/></returns>
		public static TList<QuestionnaireInitialContactEmail> Fill(IDataReader reader, TList<QuestionnaireInitialContactEmail> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireInitialContactEmail")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.QuestionnaireInitialContactEmailId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireInitialContactEmail>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireInitialContactEmail",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireInitialContactEmailId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.QuestionnaireInitialContactEmailId - 1)];
					c.EmailTypeId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.EmailTypeId - 1)];
					c.EmailSentDateTime = (System.DateTime)reader[((int)QuestionnaireInitialContactEmailColumn.EmailSentDateTime - 1)];
					c.ContactId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.ContactId - 1)];
					c.ContactEmail = (System.String)reader[((int)QuestionnaireInitialContactEmailColumn.ContactEmail - 1)];
					c.Subject = (System.String)reader[((int)QuestionnaireInitialContactEmailColumn.Subject - 1)];
					c.Message = (System.String)reader[((int)QuestionnaireInitialContactEmailColumn.Message - 1)];
					c.SentByUserId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.SentByUserId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.QuestionnaireId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireInitialContactEmailId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.QuestionnaireInitialContactEmailId - 1)];
			entity.EmailTypeId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.EmailTypeId - 1)];
			entity.EmailSentDateTime = (System.DateTime)reader[((int)QuestionnaireInitialContactEmailColumn.EmailSentDateTime - 1)];
			entity.ContactId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.ContactId - 1)];
			entity.ContactEmail = (System.String)reader[((int)QuestionnaireInitialContactEmailColumn.ContactEmail - 1)];
			entity.Subject = (System.String)reader[((int)QuestionnaireInitialContactEmailColumn.Subject - 1)];
			entity.Message = (System.String)reader[((int)QuestionnaireInitialContactEmailColumn.Message - 1)];
			entity.SentByUserId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.SentByUserId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailColumn.QuestionnaireId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireInitialContactEmailId = (System.Int32)dataRow["QuestionnaireInitialContactEmailId"];
			entity.EmailTypeId = (System.Int32)dataRow["EmailTypeId"];
			entity.EmailSentDateTime = (System.DateTime)dataRow["EmailSentDateTime"];
			entity.ContactId = (System.Int32)dataRow["ContactId"];
			entity.ContactEmail = (System.String)dataRow["ContactEmail"];
			entity.Subject = (System.String)dataRow["Subject"];
			entity.Message = (System.String)dataRow["Message"];
			entity.SentByUserId = (System.Int32)dataRow["SentByUserId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region EmailTypeIdSource	
			if (CanDeepLoad(entity, "QuestionnaireInitialContactEmailType|EmailTypeIdSource", deepLoadType, innerList) 
				&& entity.EmailTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.EmailTypeId;
				QuestionnaireInitialContactEmailType tmpEntity = EntityManager.LocateEntity<QuestionnaireInitialContactEmailType>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireInitialContactEmailType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.EmailTypeIdSource = tmpEntity;
				else
					entity.EmailTypeIdSource = DataRepository.QuestionnaireInitialContactEmailTypeProvider.GetByQuestionnaireInitialContactEmailTypeId(transactionManager, entity.EmailTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EmailTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.EmailTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireInitialContactEmailTypeProvider.DeepLoad(transactionManager, entity.EmailTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion EmailTypeIdSource

			#region ContactIdSource	
			if (CanDeepLoad(entity, "QuestionnaireInitialContact|ContactIdSource", deepLoadType, innerList) 
				&& entity.ContactIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ContactId;
				QuestionnaireInitialContact tmpEntity = EntityManager.LocateEntity<QuestionnaireInitialContact>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireInitialContact), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ContactIdSource = tmpEntity;
				else
					entity.ContactIdSource = DataRepository.QuestionnaireInitialContactProvider.GetByQuestionnaireInitialContactId(transactionManager, entity.ContactId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ContactIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireInitialContactProvider.DeepLoad(transactionManager, entity.ContactIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ContactIdSource

			#region SentByUserIdSource	
			if (CanDeepLoad(entity, "Users|SentByUserIdSource", deepLoadType, innerList) 
				&& entity.SentByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SentByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SentByUserIdSource = tmpEntity;
				else
					entity.SentByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.SentByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SentByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SentByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.SentByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SentByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region EmailTypeIdSource
			if (CanDeepSave(entity, "QuestionnaireInitialContactEmailType|EmailTypeIdSource", deepSaveType, innerList) 
				&& entity.EmailTypeIdSource != null)
			{
				DataRepository.QuestionnaireInitialContactEmailTypeProvider.Save(transactionManager, entity.EmailTypeIdSource);
				entity.EmailTypeId = entity.EmailTypeIdSource.QuestionnaireInitialContactEmailTypeId;
			}
			#endregion 
			
			#region ContactIdSource
			if (CanDeepSave(entity, "QuestionnaireInitialContact|ContactIdSource", deepSaveType, innerList) 
				&& entity.ContactIdSource != null)
			{
				DataRepository.QuestionnaireInitialContactProvider.Save(transactionManager, entity.ContactIdSource);
				entity.ContactId = entity.ContactIdSource.QuestionnaireInitialContactId;
			}
			#endregion 
			
			#region SentByUserIdSource
			if (CanDeepSave(entity, "Users|SentByUserIdSource", deepSaveType, innerList) 
				&& entity.SentByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.SentByUserIdSource);
				entity.SentByUserId = entity.SentByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireInitialContactEmailChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireInitialContactEmail</c>
	///</summary>
	public enum QuestionnaireInitialContactEmailChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireInitialContactEmailType</c> at EmailTypeIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireInitialContactEmailType))]
		QuestionnaireInitialContactEmailType,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireInitialContact</c> at ContactIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireInitialContact))]
		QuestionnaireInitialContact,
			
		///<summary>
		/// Composite Property for <c>Users</c> at SentByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion QuestionnaireInitialContactEmailChildEntityTypes
	
	#region QuestionnaireInitialContactEmailFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireInitialContactEmailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailFilterBuilder : SqlFilterBuilder<QuestionnaireInitialContactEmailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailFilterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactEmailFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailFilterBuilder
	
	#region QuestionnaireInitialContactEmailParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireInitialContactEmailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireInitialContactEmailColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailParameterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactEmailParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailParameterBuilder
	
	#region QuestionnaireInitialContactEmailSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireInitialContactEmailColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmail"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireInitialContactEmailSortBuilder : SqlSortBuilder<QuestionnaireInitialContactEmailColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailSqlSortBuilder class.
		/// </summary>
		public QuestionnaireInitialContactEmailSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireInitialContactEmailSortBuilder
	
} // end namespace
