﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Data.SqlClient;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;


public class People
{

    private static String _connectionString;
    private static Boolean _initialized;

    public static void Initialize()
    {
        // Initialize data source. Use "Northwind" connection string from configuration.

        if (ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"] == null ||
            ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString.Trim() == "")
        {
            throw new Exception("A connection string named 'AdventureWorksConnectionString' with a valid connection string " +
                                "must exist in the <connectionStrings> configuration section for the application.");
        }

        _connectionString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

        _initialized = true;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static DataTable GetPeople(Int32 startRecord, Int32 maxRecords, String sortColumns)
    {
        VerifySortColumns(sortColumns);

        if (!_initialized) { Initialize(); }

        string field = string.Empty;
        string value = string.Empty;
        string whereCondition = string.Empty;
       // maxRecords = 10;
        int filterCount = 0;

        if (HttpContext.Current.Session["YearFilter"] != null)
        {
            whereCondition = whereCondition +  " where A.Year = " + Convert.ToString(HttpContext.Current.Session["YearFilter"]);
            filterCount++;
        }
        string whereFieldCondition = string.Empty;

        if (HttpContext.Current.Session["WhereCondition"] != null)
        {
             whereFieldCondition = Convert.ToString(HttpContext.Current.Session["WhereCondition"]);

            whereFieldCondition = whereFieldCondition.Replace("[CompanyId]", "B.CompanyId");
            whereFieldCondition = whereFieldCondition.Replace("[SiteId]", "C.SiteId");
            if (whereFieldCondition.Contains("C.SiteId = 20") || whereFieldCondition.Contains("C.SiteId = 22") ||
                whereFieldCondition.Contains("C.SiteId = 27"))
            {
                //whereFieldCondition = whereFieldCondition.Replace("SiteId", "C.SiteId");

                SitesService siSer = new SitesService();
                Sites si = siSer.GetBySiteId(Convert.ToInt32(value));
                RegionsService reSer = new RegionsService();
                Regions re = reSer.GetByRegionName(si.SiteName);
                string siteCondition = "C.SiteId in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(" + re.RegionId.ToString() + ")" +
                " union select SITEID from SITES where SiteID=" + value + ") ";
                whereFieldCondition = whereFieldCondition.Replace("C.SiteId = 20", siteCondition);
                whereFieldCondition = whereFieldCondition.Replace("C.SiteId = 22", siteCondition);
                whereFieldCondition = whereFieldCondition.Replace("C.SiteId = 27", siteCondition);
               
            }
            whereFieldCondition = whereFieldCondition.Replace("[January]", "A.January");
            whereFieldCondition = whereFieldCondition.Replace("[February]", "A.February");
            whereFieldCondition = whereFieldCondition.Replace("[March]", "A.March");
            whereFieldCondition = whereFieldCondition.Replace("[April]", "A.April");
            whereFieldCondition = whereFieldCondition.Replace("[May]", "A.May");
            whereFieldCondition = whereFieldCondition.Replace("[June]", "A.June");
            whereFieldCondition = whereFieldCondition.Replace("[July]", "A.July");
            whereFieldCondition = whereFieldCondition.Replace("[August]", "A.August");
            whereFieldCondition = whereFieldCondition.Replace("[September]", "A.September");
            whereFieldCondition = whereFieldCondition.Replace("[October]", "A.October");
            whereFieldCondition = whereFieldCondition.Replace("[November]", "A.November");
            whereFieldCondition = whereFieldCondition.Replace("[December]", "A.December");

            whereFieldCondition = whereFieldCondition.Replace("[YearCount]", "A.YearCount");
            whereFieldCondition = whereFieldCondition.Replace("[LocationSponsorUserId]", "A.LocationSponsorUserId");
            whereFieldCondition = whereFieldCondition.Replace("[LocationSponsorUserId]", "A.LocationSponsorUserId");

            whereCondition = whereCondition + " and " + whereFieldCondition;
        }

        //whereCondition = whereCondition + " and " + whereFieldCondition;








        if (HttpContext.Current.Session["CompanyIdVal"] != null)
        {
            field = "B.CompanyId";
            value = Convert.ToString(HttpContext.Current.Session["CompanyIdVal"]);
            //whereCondition =" where "+ field + " = " + value;
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["SiteIdVal"] != null)
        {
            field = "C.SiteId";
            value = Convert.ToString(HttpContext.Current.Session["SiteIdVal"]);
            if(Convert.ToInt32(value)>0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                if (Convert.ToInt32(value) == 20 || Convert.ToInt32(value) == 22 
                     || Convert.ToInt32(value) == 27)
                {
                    SitesService siSer = new SitesService();
                    Sites si = siSer.GetBySiteId(Convert.ToInt32(value));
                    RegionsService reSer = new RegionsService();
                    Regions re = reSer.GetByRegionName(si.SiteName);
                    whereCondition = whereCondition + field + " in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(" + re.RegionId.ToString() + ")"+
                    " union select SITEID from SITES where SiteID=" + value + ") ";
                    filterCount++;
                }
                else
                {
                    whereCondition = whereCondition + field + " = " + value;
                }
            }
            else if(Convert.ToInt32(value)<0)
            {
                if (filterCount > 0) { whereCondition=whereCondition+" and ";}
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*"+value+")"+
                " union select SITEID from SITES where SiteID=" + value + ") ";
                filterCount++;                
            }
        }
        if (HttpContext.Current.Session["JanVal"] != null)
        {
            field = "A.January";
            value = Convert.ToString(HttpContext.Current.Session["JanVal"]);
            if (value != "")
            {
                if (value == "UnChecked") { value = "0"; }
                else if (value == "Checked") { value = "1"; }
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            { 
               HttpContext.Current.Session["JanVal"]=null; 
            }
        }
        if (HttpContext.Current.Session["FebVal"] != null)
        {
            field = "A.February";
            value = Convert.ToString(HttpContext.Current.Session["FebVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["FebVal"] = null;
            }
        }
        if (HttpContext.Current.Session["MarVal"] != null)
        {
            field = "A.March";
            value = Convert.ToString(HttpContext.Current.Session["MarVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["MarVal"] = null;
            }
        }
        if (HttpContext.Current.Session["AprVal"] != null)
        {
            field = "A.April";
            value = Convert.ToString(HttpContext.Current.Session["AprVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["AprVal"] = null;
            }
        }
        if (HttpContext.Current.Session["MayVal"] != null)
        {
            field = "A.May";
            value = Convert.ToString(HttpContext.Current.Session["MayVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["MayVal"] = null;
            }
        }
        if (HttpContext.Current.Session["JunVal"] != null)
        {
            field = "A.June";
            value = Convert.ToString(HttpContext.Current.Session["JunVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["JunVal"] = null;
            }
        }
        if (HttpContext.Current.Session["JulVal"] != null)
        {
            field = "A.July";
            value = Convert.ToString(HttpContext.Current.Session["JulVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["JulVal"] = null;
            }
        }
        if (HttpContext.Current.Session["AugVal"] != null)
        {
            field = "A.August";
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["AugVal"] = null;
            }
        }
        if (HttpContext.Current.Session["SepVal"] != null)
        {
            field = "A.September";
            value = Convert.ToString(HttpContext.Current.Session["SepVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["SepVal"] = null;
            }
        }
        if (HttpContext.Current.Session["OctVal"] != null)
        {
            field = "A.October";
            value = Convert.ToString(HttpContext.Current.Session["OctVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["OctVal"] = null;
            }
        }
        if (HttpContext.Current.Session["NovVal"] != null)
        {
            field = "A.November";
            value = Convert.ToString(HttpContext.Current.Session["NovVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["NovVal"] = null;
            }
        }
        if (HttpContext.Current.Session["DecVal"] != null)
        {
            field = "A.December";
            value = Convert.ToString(HttpContext.Current.Session["DecVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["DecVal"] = null;
            }
        }
        if (HttpContext.Current.Session["YrCountVal"] != null)
        {
            //field = "A.YearCount";
            value = Convert.ToString(HttpContext.Current.Session["YrCountVal"]);
            value = value.Replace("[YearCount]", "A.YearCount");
            if (value.ToLower().Contains("between"))
            {
                value = value.Replace(",", " and ");
                value = value.Replace("(", " ");
                value = value.Replace(")", "");
            }
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            //whereCondition = whereCondition + field + " = " + value;
            whereCondition = whereCondition + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["LocSPUVal"] != null)
        {
            field = "A.LocationSponsorUserId";
            value = Convert.ToString(HttpContext.Current.Session["LocSPUVal"]);
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["LocSPAVal"] != null)
        {
            field = "A.LocationSpaUserId";
            value = Convert.ToString(HttpContext.Current.Session["LocSPAVal"]);
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }


        if (HttpContext.Current.Session["CompanyIdVal"] == null && HttpContext.Current.Session["SiteIdVal"] == null)
        {
            HttpContext.Current.Session["filterCount"] = null;
        }
        

        String sqlColumns = "[ContractId], [CompanyId], [SiteId], [Year], [January], [February], [March],[April], [May], [June],"+
                                    "[July], [August], [September], [October], [November], [December], "+
                                    "[YearCount],[LocationSponsorUserId], [LocationSpaUserId], [ModifiedDate],[ModifiedBy]";
        String sqlTable = "[ContractReviewsRegister]";

        string sqlSortColumn = "B.CompanyName ASC";

        if (!String.IsNullOrEmpty(sortColumns))
            sqlSortColumn = sortColumns;

        if (sqlSortColumn.Contains("CompanyId"))
        {
            sqlSortColumn = sortColumns.Replace("CompanyId", "B.CompanyName");
        }
        else if (sqlSortColumn.Contains("SiteId"))
        {
            sqlSortColumn = sortColumns.Replace("SiteId", "C.SiteName");
        }
        else if (sqlSortColumn.Contains("LocationSponsorUserId"))
        {
            sqlSortColumn = sortColumns.Replace("LocationSponsorUserId", "D.LastName");
        }
        else if (sqlSortColumn.Contains("LocationSpaUserId"))
        {
            sqlSortColumn = sortColumns.Replace("LocationSpaUserId", "E.LastName");
        }
        else
        {
            sqlSortColumn = "A." + sortColumns;
        }

        

        String sqlQuery = "SELECT ROW_NUMBER() OVER (ORDER BY " + sqlSortColumn + ") AS rownumber," +
                             "A.ContractId, A.CompanyId, A.SiteId, B.CompanyName,C.SiteName," +
                             "A.Year, A.January," +
                             "A.February, A.March," +
                             "A.April, A.May, A.June,A.July, A.August, A.September," +
                             "A.October, A.November," +
                           "A.December, A.YearCount,A.LocationSponsorUserId, A.LocationSpaUserId," +
                             "A.ModifiedDate,A.ModifiedBy    FROM ContractReviewsRegister A " +
                                 "left outer join Companies B ON A.CompanyId=B.CompanyId " +
                             " left outer join Sites C on A.SiteId=C.SiteId  " +
                             " left outer join Users D on A.LocationSponsorUserId=D.UserId "+
							" left outer join Users E on A.LocationSpaUserId=E.UserId " + whereCondition;

        if (!String.IsNullOrEmpty(sortColumns))
            sqlSortColumn = sortColumns;
        String sqlCommand = string.Empty;
        if (HttpContext.Current.Session["ShowAll"] != null)
        {
            if (Convert.ToBoolean(HttpContext.Current.Session["ShowAll"]) == true)
            {
                sqlCommand = String.Format("SELECT * FROM({0}) AS foo ", sqlQuery);

            }
            else
            {
                sqlCommand = String.Format(
               "SELECT * FROM({0}) AS foo " +
               "WHERE rownumber >= {1} AND rownumber <= {2}",
               sqlQuery,
               startRecord + 1, startRecord + maxRecords
                );
            }
            
        }
        else
        {
            sqlCommand = String.Format(
               "SELECT * FROM({0}) AS foo " +
               "WHERE rownumber >= {1} AND rownumber <= {2}",
               sqlQuery,
               startRecord + 1, startRecord + maxRecords
            );
        }

        SqlConnection conn = new SqlConnection(_connectionString);
        SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);

        DataSet ds = new DataSet();

        try
        {
            conn.Open();
            da.Fill(ds, "People");
        }
        catch (SqlException e)
        {
            // Handle exception.
        }
        finally
        {
            conn.Close();
        }

        if (ds.Tables["People"] != null)
            return ds.Tables["People"];

        return null;
    }


    // columns are specified in the sort expression to avoid a SQL Injection attack.

    private static void VerifySortColumns(string sortColumns)
    {
        sortColumns = sortColumns.ToLowerInvariant().Replace(" asc", String.Empty).Replace(" desc", String.Empty);
        String[] columnNames = sortColumns.Split(',');

        foreach (String columnName in columnNames)
        {

            switch (columnName.Trim().ToLowerInvariant())
            {
                case "contractid":
                case "companyname":
                case "companyid":
                case "siteid":
                case "year":
                case "january":
                case "february":
                case "march":
                case "april":
                case "may":
                case "june":
                case "july":
                case "august":
                case "september":
                case "october":
                case "november":
                case "december":
                case "yearcount":
                case "locationsponsoruserid":
                case "locationspauserid":
                case "":
                    break;
                default:
                    throw new ArgumentException("SortColumns contains an invalid column name.");
            }
        }
    }

    public static Int32 GetPeopleCount()
    {
        if (!_initialized) { Initialize(); }

        string field = string.Empty;
        string value = string.Empty;
        string whereCondition = string.Empty;        
        int filterCount = 0;


        if (HttpContext.Current.Session["YearFilter"] != null)
        {
            whereCondition = whereCondition + " where Year = " + Convert.ToString(HttpContext.Current.Session["YearFilter"]);
            filterCount++;
        }
        string whereFieldCondition = string.Empty;

        if (HttpContext.Current.Session["WhereCondition"] != null)
        {
            whereFieldCondition = Convert.ToString(HttpContext.Current.Session["WhereCondition"]);
           
            
            if (whereFieldCondition.Contains("SiteId = 20") || whereFieldCondition.Contains("SiteId = 22") ||
                whereFieldCondition.Contains("SiteId = 27"))
            {
                //whereFieldCondition = whereFieldCondition.Replace("SiteId", "C.SiteId");

                SitesService siSer = new SitesService();
                Sites si = siSer.GetBySiteId(Convert.ToInt32(value));
                RegionsService reSer = new RegionsService();
                Regions re = reSer.GetByRegionName(si.SiteName);
                string siteCondition = " SiteId in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(" + re.RegionId.ToString() + ")" +
                " union select SITEID from SITES where SiteID=" + value + ") ";
                whereFieldCondition = whereFieldCondition.Replace("C.SiteId = 20", siteCondition);
                whereFieldCondition = whereFieldCondition.Replace("C.SiteId = 22", siteCondition);
                whereFieldCondition = whereFieldCondition.Replace("C.SiteId = 27", siteCondition);

            }
            whereCondition = whereCondition + " and " + whereFieldCondition;
        }
        






        if (HttpContext.Current.Session["CompanyIdVal"] != null)
        {
            field = "CompanyId";
            value = Convert.ToString(HttpContext.Current.Session["CompanyIdVal"]);
            whereCondition = " where " + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["SiteIdVal"] != null)
        {
            field = "SiteId";
            value = Convert.ToString(HttpContext.Current.Session["SiteIdVal"]);
            if (Convert.ToInt32(value) > 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                if (Convert.ToInt32(value) == 20 || Convert.ToInt32(value) == 22 
                     || Convert.ToInt32(value) == 27)
                {
                    SitesService siSer = new SitesService();
                    Sites si = siSer.GetBySiteId(Convert.ToInt32(value));
                    RegionsService reSer = new RegionsService();
                    Regions re = reSer.GetByRegionName(si.SiteName);
                    whereCondition = whereCondition + field + " in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(" + re.RegionId.ToString() + ")" +
                    " union select SITEID from SITES where SiteID=" + value + ") ";
                    filterCount++;
                }
                else
                {
                    whereCondition = whereCondition + field + " = " + value;
                }
            }
            else if (Convert.ToInt32(value) < 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + value + ")" +
                " union select SITEID from SITES where SiteID=" + value + ") ";
                filterCount++;
                // (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*@SiteId)) 
            }
        }
        if (HttpContext.Current.Session["JanVal"] != null)
        {
            field = "January";
            value = Convert.ToString(HttpContext.Current.Session["JanVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["JanVal"] = null;
            }
        }
        if (HttpContext.Current.Session["FebVal"] != null)
        {
            field = "February";
            value = Convert.ToString(HttpContext.Current.Session["FebVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["FebVal"] = null;
            }
        }
        if (HttpContext.Current.Session["MarVal"] != null)
        {
            field = "March";
            value = Convert.ToString(HttpContext.Current.Session["MarVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["MarVal"] = null;
            }
        }
        if (HttpContext.Current.Session["AprVal"] != null)
        {
            field = "April";
            value = Convert.ToString(HttpContext.Current.Session["AprVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["AprVal"] = null;
            }
        }
        if (HttpContext.Current.Session["MayVal"] != null)
        {
            field = "May";
            value = Convert.ToString(HttpContext.Current.Session["MayVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["MayVal"] = null;
            }
        }
        if (HttpContext.Current.Session["JunVal"] != null)
        {
            field = "June";
            value = Convert.ToString(HttpContext.Current.Session["JunVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["JunVal"] = null;
            }
        }
        if (HttpContext.Current.Session["JulVal"] != null)
        {
            field = "July";
            value = Convert.ToString(HttpContext.Current.Session["JulVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["JulVal"] = null;
            }
        }
        if (HttpContext.Current.Session["AugVal"] != null)
        {
            field = "August";
            value = Convert.ToString(HttpContext.Current.Session["AugVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["AugVal"] = null;
            }
        }
        if (HttpContext.Current.Session["SepVal"] != null)
        {
            field = "September";
            value = Convert.ToString(HttpContext.Current.Session["SepVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["SepVal"] = null;
            }
        }
        if (HttpContext.Current.Session["OctVal"] != null)
        {
            field = "October";
            value = Convert.ToString(HttpContext.Current.Session["OctVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["OctVal"] = null;
            }
        }
        if (HttpContext.Current.Session["NovVal"] != null)
        {
            field = "November";
            value = Convert.ToString(HttpContext.Current.Session["NovVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["NovVal"] = null;
            }
        }
        if (HttpContext.Current.Session["DecVal"] != null)
        {
            field = "December";
            value = Convert.ToString(HttpContext.Current.Session["DecVal"]);
            if (value != "")
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["DecVal"] = null;
            }
        }
        if (HttpContext.Current.Session["YrCountVal"] != null)
        {
            //field = "YearCount";
            value = Convert.ToString(HttpContext.Current.Session["YrCountVal"]);
            value = value.Replace("[YearCount]", "YearCount");
            if (value.ToLower().Contains("between"))
            {
                value = value.Replace(",", " and ");
                value = value.Replace("(", " ");
                value = value.Replace(")", "");
            }
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            //whereCondition = whereCondition + field + " = " + value;
            whereCondition = whereCondition + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["LocSPUVal"] != null)
        {
            field = "LocationSponsorUserId";
            value = Convert.ToString(HttpContext.Current.Session["LocSPUVal"]);
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["LocSPAVal"] != null)
        {
            field = "LocationSpaUserId";
            value = Convert.ToString(HttpContext.Current.Session["LocSPAVal"]);
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }


        string sqlCommand = "SELECT COUNT ([ContractId]) FROM [ContractReviewsRegister]" + whereCondition;

        SqlConnection conn = new SqlConnection(_connectionString);
        SqlCommand command = new SqlCommand(sqlCommand, conn);
        SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);

        Int32 result = 0;

        try
        {
            conn.Open();
            result = (Int32)command.ExecuteScalar();
        }
        catch (SqlException e)
        {
            // Handle exception.
        }
        finally
        {
            conn.Close();
        }

        return result;
    }

    public static void UpdatePeople(int CompanyId, int SiteId, 
                          bool January, bool February, bool March, bool April, bool May,
                          bool June, bool July, bool August, bool September, bool October, bool November,
                            bool December, int ContractId, int UserId, int Year)
    {
        try
        {
            int yearCount = 0;
            if (January) { yearCount++; }
            if (February) { yearCount++; }
            if (March) { yearCount++; }
            if (April) { yearCount++; }
            if (May) { yearCount++; }
            if (June) { yearCount++; }
            if (July) { yearCount++; }
            if (August) { yearCount++; }
            if (September) { yearCount++; }
            if (October) { yearCount++; }
            if (November) { yearCount++; }
            if (December) { yearCount++; }            
            
            TransactionManager transactionManager = null;
            transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

            ContractReviewsRegisterService conser = new ContractReviewsRegisterService();
            //KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(_companyId, _siteId, _year);

            KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByContractId(ContractId);


            KaiZen.CSMS.Entities.ContractReviewsRegister contUpdate = new KaiZen.CSMS.Entities.ContractReviewsRegister();

            contUpdate.ContractId = cont.ContractId;
            contUpdate.Year = Year;

            contUpdate.CompanyId = CompanyId;
            contUpdate.SiteId = SiteId;

            contUpdate.January = January;
            contUpdate.February = February;
            contUpdate.March = March;
            contUpdate.April = April;
            contUpdate.May = May;

            contUpdate.June = June;
            contUpdate.July = July;
            contUpdate.August = August;
            contUpdate.September = September;
            contUpdate.October = October;
            contUpdate.November = November;
            contUpdate.December = December;
            contUpdate.YearCount = yearCount;
            //contUpdate.LocationSponsorUserId = LocationSponsorUserId;
            //contUpdate.LocationSpaUserId = LocationSpaUserId;

            if (contUpdate.EntityState != EntityState.Unchanged)
            {
                contUpdate.ModifiedBy = UserId;
                contUpdate.ModifiedDate = DateTime.Now;
                //conser.Save(contNew);
                DataRepository.ContractReviewsRegisterProvider.Update(transactionManager, contUpdate);
                transactionManager.Commit();
            }
        }
        catch (Exception ex)
        {
            
            
        }
    }

   

    //public void UpdatePeople(int contractid,int _companyId, int _siteId, int _year,
    //                        bool _january, bool _february, bool _march, bool _april, bool _may,
    //                        bool _june, bool _august, bool _september, bool _october, bool _november, bool _december)
    //{

    //    ContractReviewsRegisterService conser = new ContractReviewsRegisterService();
    //    KaiZen.CSMS.Entities.ContractReviewsRegister cont = conser.GetByCompanyIdSiteIdYear(_companyId, _siteId, _year);
               

    //                KaiZen.CSMS.Entities.ContractReviewsRegister contUpdate = new KaiZen.CSMS.Entities.ContractReviewsRegister();

    //                contUpdate.ContractId = cont.ContractId;
    //                contUpdate.Year = _year;

    //                contUpdate.January = _january;
    //                contUpdate.February = _february;
    //                contUpdate.March = _march;
    //                contUpdate.April = _april;
    //                contUpdate.May = _may;

    //                contUpdate.June = _june;
    //                contUpdate.July = _july;
    //                contUpdate.August = _august;
    //                contUpdate.September = _september;
    //                contUpdate.October = _october;
    //                contUpdate.November = _november;
    //                contUpdate.December = _december;

    //                if (contUpdate.EntityState != EntityState.Unchanged)
    //                {
    //                    contUpdate.ModifiedBy = auth.UserId;
    //                    contUpdate.ModifiedDate = DateTime.Now;
    //                    //conser.Save(contNew);
    //                    DataRepository.ContractReviewsRegisterProvider.Update(transactionManager, contUpdate);
    //                    transactionManager.Commit();
    //                }

    //}
 

}