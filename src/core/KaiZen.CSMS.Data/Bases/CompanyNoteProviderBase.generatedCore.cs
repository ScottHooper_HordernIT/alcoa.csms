﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using System.Reflection;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanyNoteProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanyNoteProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanyNote, KaiZen.CSMS.Entities.CompanyNoteKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyNoteKey key)
		{
			return Delete(transactionManager, key.CompanyNoteId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companyNoteId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companyNoteId)
		{
			return Delete(null, _companyNoteId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNoteId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companyNoteId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions

        #region FK_CompanyNote_Role
        /// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Role key.
		///		FK_CompanyNote_Role Description: 
		/// </summary>
		/// <param name="_createdByRoleId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByRoleId(System.Int32 _createdByRoleId)
		{
			int count = -1;
			return GetByCreatedByRoleId(_createdByRoleId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Role key.
		///		FK_CompanyNote_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByRoleId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyNote> GetByCreatedByRoleId(TransactionManager transactionManager, System.Int32 _createdByRoleId)
		{
			int count = -1;
			return GetByCreatedByRoleId(transactionManager, _createdByRoleId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Role key.
		///		FK_CompanyNote_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByRoleId(TransactionManager transactionManager, System.Int32 _createdByRoleId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByRoleId(transactionManager, _createdByRoleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Role key.
		///		FK_CompanyNote_Role Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByRoleId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByRoleId(System.Int32 _createdByRoleId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByRoleId(null, _createdByRoleId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Role key.
		///		FK_CompanyNote_Role Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByRoleId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByRoleId(System.Int32 _createdByRoleId, int start, int pageLength,out int count)
		{
			return GetByCreatedByRoleId(null, _createdByRoleId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Role key.
		///		FK_CompanyNote_Role Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public abstract TList<CompanyNote> GetByCreatedByRoleId(TransactionManager transactionManager, System.Int32 _createdByRoleId, int start, int pageLength, out int count);
        #endregion

        #region FK_CompanyNote_Users
        /// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Users key.
		///		FK_CompanyNote_Users Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Users key.
		///		FK_CompanyNote_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyNote> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Users key.
		///		FK_CompanyNote_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Users key.
		///		FK_CompanyNote_Users Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Users key.
		///		FK_CompanyNote_Users Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Users key.
		///		FK_CompanyNote_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public abstract TList<CompanyNote> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
        #endregion

        #region FK_CompanyNote_Companies
        /// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Companies key.
		///		FK_CompanyNote_Companies Description: 
		/// </summary>
		/// <param name="_createdByCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByCompanyId(System.Int32 _createdByCompanyId)
		{
			int count = -1;
			return GetByCreatedByCompanyId(_createdByCompanyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Companies key.
		///		FK_CompanyNote_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByCompanyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		/// <remarks></remarks>
		public TList<CompanyNote> GetByCreatedByCompanyId(TransactionManager transactionManager, System.Int32 _createdByCompanyId)
		{
			int count = -1;
			return GetByCreatedByCompanyId(transactionManager, _createdByCompanyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Companies key.
		///		FK_CompanyNote_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByCompanyId(TransactionManager transactionManager, System.Int32 _createdByCompanyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByCompanyId(transactionManager, _createdByCompanyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Companies key.
		///		FK_CompanyNote_Companies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByCompanyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByCompanyId(System.Int32 _createdByCompanyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByCompanyId(null, _createdByCompanyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Companies key.
		///		FK_CompanyNote_Companies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByCompanyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public TList<CompanyNote> GetByCreatedByCompanyId(System.Int32 _createdByCompanyId, int start, int pageLength,out int count)
		{
			return GetByCreatedByCompanyId(null, _createdByCompanyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanyNote_Companies key.
		///		FK_CompanyNote_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByCompanyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
		public abstract TList<CompanyNote> GetByCreatedByCompanyId(TransactionManager transactionManager, System.Int32 _createdByCompanyId, int start, int pageLength, out int count);
        #endregion
		#endregion

        #region Get By Column data

        #region Visibility
        /// <summary>
        /// 	Gets rows from the datasource based on the column: Visibility
        /// </summary>
        /// <param name="_visibility"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByVisibility(System.String _visibility)
        {
            int count = -1;
            return GetByVisibility(_visibility, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the column: Visibility
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_visibility"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        /// <remarks></remarks>
        public TList<CompanyNote> GetByVisibility(TransactionManager transactionManager, System.String _visibility)
        {
            int count = -1;
            return GetByVisibility(transactionManager, _visibility, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the column: Visibility
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_visibility"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByVisibility(TransactionManager transactionManager, System.String _visibility, int start, int pageLength)
        {
            int count = -1;
            return GetByVisibility(transactionManager, _visibility, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the column: Visibility
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_visibility"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByVisibility(System.String _visibility, int start, int pageLength)
        {
            int count = -1;
            return GetByVisibility(null, _visibility, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the column: Visibility
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_visibility"></param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByVisibility(System.String _visibility, int start, int pageLength, out int count)
        {
            return GetByVisibility(null, _visibility, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the column: Visibility
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_visibility"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public abstract TList<CompanyNote> GetByVisibility(TransactionManager transactionManager, System.String _visibility, int start, int pageLength, out int count);
        #endregion

        #region CreatedByCompanyId + Visibility
        /// <summary>
        /// 	Gets rows from the datasource based on the columns: CreatedByCompanyId & Visibility
        /// </summary>
        /// <param name="_createdByCompanyId"></param>
        /// <param name="_visibility"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByCreatedByCompanyIdVisibility(System.Int32 _createdByCompanyId, System.String _visibility)
        {
            int count = -1;
            return GetByCreatedByCompanyIdVisibility(_createdByCompanyId, _visibility, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the columns: CreatedByCompanyId & Visibility
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_createdByCompanyId"></param>
        /// <param name="_visibility"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        /// <remarks></remarks>
        public TList<CompanyNote> GetByCreatedByCompanyIdVisibility(TransactionManager transactionManager, System.Int32 _createdByCompanyId, System.String _visibility)
        {
            int count = -1;
            return GetByCreatedByCompanyIdVisibility(transactionManager, _createdByCompanyId, _visibility, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the columns: CreatedByCompanyId & Visibility
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_createdByCompanyId"></param>
        /// <param name="_visibility"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByCreatedByCompanyIdVisibility(TransactionManager transactionManager, System.Int32 _createdByCompanyId, System.String _visibility, int start, int pageLength)
        {
            int count = -1;
            return GetByCreatedByCompanyIdVisibility(transactionManager, _createdByCompanyId, _visibility, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the columns: CreatedByCompanyId & Visibility
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_createdByCompanyId"></param>
        /// <param name="_visibility"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByCreatedByCompanyIdVisibility(System.Int32 _createdByCompanyId, System.String _visibility, int start, int pageLength)
        {
            int count = -1;
            return GetByCreatedByCompanyIdVisibility(null, _createdByCompanyId, _visibility, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the columns: CreatedByCompanyId & Visibility
        /// </summary>
        /// <param name="_createdByCompanyId"></param>
        /// <param name="_visibility"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public TList<CompanyNote> GetByCreatedByCompanyIdVisibility(System.Int32 _createdByCompanyId, System.String _visibility, int start, int pageLength, out int count)
        {
            return GetByCreatedByCompanyIdVisibility(null, _createdByCompanyId, _visibility, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the columns: CreatedByCompanyId & Visibility
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_createdByCompanyId"></param>
        /// <param name="_visibility"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanyNote objects.</returns>
        public abstract TList<CompanyNote> GetByCreatedByCompanyIdVisibility(TransactionManager transactionManager, System.Int32 _createdByCompanyId, System.String _visibility, int start, int pageLength, out int count);
        #endregion
        #endregion

        #region Get By Index Functions

        #region PK_CompanyNote
        /// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanyNote Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyNoteKey key, int start, int pageLength)
		{
			return GetByCompanyNoteId(transactionManager, key.CompanyNoteId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanyNote index.
		/// </summary>
		/// <param name="_companyNoteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyNote GetByCompanyNoteId(System.Int32 _companyNoteId)
		{
			int count = -1;
			return GetByCompanyNoteId(null,_companyNoteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyNote index.
		/// </summary>
		/// <param name="_companyNoteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyNote GetByCompanyNoteId(System.Int32 _companyNoteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyNoteId(null, _companyNoteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyNote index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNoteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyNote GetByCompanyNoteId(TransactionManager transactionManager, System.Int32 _companyNoteId)
		{
			int count = -1;
			return GetByCompanyNoteId(transactionManager, _companyNoteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyNote index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNoteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyNote GetByCompanyNoteId(TransactionManager transactionManager, System.Int32 _companyNoteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyNoteId(transactionManager, _companyNoteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyNote index.
		/// </summary>
		/// <param name="_companyNoteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyNote GetByCompanyNoteId(System.Int32 _companyNoteId, int start, int pageLength, out int count)
		{
			return GetByCompanyNoteId(null, _companyNoteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyNote index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNoteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanyNote GetByCompanyNoteId(TransactionManager transactionManager, System.Int32 _companyNoteId, int start, int pageLength, out int count);
        
        #endregion
        #endregion "Get By Index Functions"

        #region Custom Methods

        #endregion

        #region Helper Functions

        /// <summary>
		/// Fill a TList&lt;CompanyNote&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanyNote&gt;"/></returns>
		public static TList<CompanyNote> Fill(IDataReader reader, TList<CompanyNote> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanyNote c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanyNote")
					.Append("|").Append((System.Int32)reader[((int)CompanyNoteColumn.CompanyNoteId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanyNote>(
					key.ToString(), // EntityTrackingKey
					"CompanyNote",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanyNote();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;

                    foreach (CompanyNoteColumn col in (CompanyNoteColumn[])Enum.GetValues(typeof(CompanyNoteColumn)))
                    {
                        if(Utility.HasColumn(reader, col.ToString())){
                            PropertyInfo prop = c.GetType().GetProperty(col.ToString(), BindingFlags.Public | BindingFlags.Instance);
                            if (null != prop && prop.CanWrite && null != reader[col.ToString()])
                            {
                                var val = Convert.ChangeType(reader[col.ToString()], prop.PropertyType);
                                prop.SetValue(c, val, null);
                            }
                        }
                    }
                    //c.CompanyNoteId = (System.Int32)reader[((int)CompanyNoteColumn.CompanyNoteId - 1)];
                    //c.CreatedDate = (System.DateTime)reader[((int)CompanyNoteColumn.CreatedDate - 1)];
                    //c.CreatedByUserId = (System.Int32)reader[((int)CompanyNoteColumn.CreatedByUserId - 1)];
                    //c.CreatedByRoleId = (System.Int32)reader[((int)CompanyNoteColumn.CreatedByRoleId - 1)];
                    //c.CreatedByCompanyId = (System.Int32)reader[((int)CompanyNoteColumn.CreatedByCompanyId - 1)];
                    //c.CreatedByPrivledgedRole = (reader.IsDBNull(((int)CompanyNoteColumn.CreatedByPrivledgedRole - 1)))?null:(System.String)reader[((int)CompanyNoteColumn.CreatedByPrivledgedRole - 1)];
                    //c.ActionNote = (System.String)reader[((int)CompanyNoteColumn.ActionNote - 1)];
                    //c.Visibility = (System.String)reader[((int)CompanyNoteColumn.Visibility - 1)];
                    //c.FullName = (System.String)reader[((int)CompanyNoteColumn.FullName - 1)];
                    //c.CompanyName = (System.String)reader[((int)CompanyNoteColumn.CompanyName - 1)];
                    //c.Role = (System.String)reader[((int)CompanyNoteColumn.Role - 1)];
                    c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyNote"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanyNote entity)
		{
			if (!reader.Read()) return;

            foreach (CompanyNoteColumn col in (CompanyNoteColumn[])Enum.GetValues(typeof(CompanyNoteColumn)))
            {
                if (Utility.HasColumn(reader, col.ToString()))
                {
                    PropertyInfo prop = entity.GetType().GetProperty(col.ToString(), BindingFlags.Public | BindingFlags.Instance);
                    if (null != prop && prop.CanWrite && null != reader[col.ToString()])
                    {
                        var val = Convert.ChangeType(reader[col.ToString()], prop.PropertyType);
                        prop.SetValue(entity, val, null);
                    }
                }
            }
			
            //entity.CompanyNoteId = (System.Int32)reader[((int)CompanyNoteColumn.CompanyNoteId - 1)];
            //entity.CreatedDate = (System.DateTime)reader[((int)CompanyNoteColumn.CreatedDate - 1)];
            //entity.CreatedByUserId = (System.Int32)reader[((int)CompanyNoteColumn.CreatedByUserId - 1)];
            //entity.CreatedByRoleId = (System.Int32)reader[((int)CompanyNoteColumn.CreatedByRoleId - 1)];
            //entity.CreatedByCompanyId = (System.Int32)reader[((int)CompanyNoteColumn.CreatedByCompanyId - 1)];
            //entity.CreatedByPrivledgedRole = (reader.IsDBNull(((int)CompanyNoteColumn.CreatedByPrivledgedRole - 1)))?null:(System.String)reader[((int)CompanyNoteColumn.CreatedByPrivledgedRole - 1)];
            //entity.ActionNote = (System.String)reader[((int)CompanyNoteColumn.ActionNote - 1)];
            //entity.Visibility = (System.String)reader[((int)CompanyNoteColumn.Visibility - 1)];
            //entity.FullName = (System.String)reader[((int)CompanyNoteColumn.FullName - 1)];
            //entity.CompanyName = (System.String)reader[((int)CompanyNoteColumn.CompanyName - 1)];
            //entity.Role = (System.String)reader[((int)CompanyNoteColumn.Role - 1)];
            entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyNote"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyNote"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanyNote entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyNoteId = (System.Int32)dataRow["CompanyNoteId"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedByRoleId = (System.Int32)dataRow["CreatedByRoleId"];
			entity.CreatedByCompanyId = (System.Int32)dataRow["CreatedByCompanyId"];
			entity.CreatedByPrivledgedRole = Convert.IsDBNull(dataRow["CreatedByPrivledgedRole"]) ? null : (System.String)dataRow["CreatedByPrivledgedRole"];
            entity.ActionNote = (System.String)dataRow["ActionNote"];
            entity.Visibility = (System.String)dataRow["Visibility"];
            entity.FullName = (System.String)dataRow["FullName"];
            entity.CompanyName = (System.String)dataRow["CompanyName"];
            entity.Role = (System.String)dataRow["Role"];
            entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyNote"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyNote Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyNote entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CreatedByRoleIdSource	
			if (CanDeepLoad(entity, "Role|CreatedByRoleIdSource", deepLoadType, innerList) 
				&& entity.CreatedByRoleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByRoleId;
				Role tmpEntity = EntityManager.LocateEntity<Role>(EntityLocator.ConstructKeyFromPkItems(typeof(Role), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByRoleIdSource = tmpEntity;
				else
					entity.CreatedByRoleIdSource = DataRepository.RoleProvider.GetByRoleId(transactionManager, entity.CreatedByRoleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByRoleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByRoleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RoleProvider.DeepLoad(transactionManager, entity.CreatedByRoleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByRoleIdSource

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

            #region CreatedByCompanyIdSource
            if (CanDeepLoad(entity, "Companies|CreatedByCompanyIdSource", deepLoadType, innerList)
                && entity.CreatedByCompanyIdSource == null)
            {
                object[] pkItems = new object[1];
                pkItems[0] = entity.CreatedByCompanyId;
                Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
                if (tmpEntity != null)
                    entity.CreatedByCompanyIdSource = tmpEntity;
                else
                    entity.CreatedByCompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CreatedByCompanyId);

#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByCompanyIdSource' loaded. key " + entity.EntityTrackingKey);
#endif

                if (deep && entity.CreatedByCompanyIdSource != null)
                {
                    innerList.SkipChildren = true;
                    DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CreatedByCompanyIdSource, deep, deepLoadType, childTypes, innerList);
                    innerList.SkipChildren = false;
                }

            }
            #endregion CreatedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanyNote object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanyNote instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyNote Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyNote entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CreatedByRoleIdSource
			if (CanDeepSave(entity, "Role|CreatedByRoleIdSource", deepSaveType, innerList) 
				&& entity.CreatedByRoleIdSource != null)
			{
				DataRepository.RoleProvider.Save(transactionManager, entity.CreatedByRoleIdSource);
				entity.CreatedByRoleId = entity.CreatedByRoleIdSource.RoleId;
			}
			#endregion 
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
            #region CreatedByCompanyIdSource
            if (CanDeepSave(entity, "Companies|CreatedByCompanyIdSource", deepSaveType, innerList)
                && entity.CreatedByCompanyIdSource != null)
            {
                DataRepository.CompaniesProvider.Save(transactionManager, entity.CreatedByCompanyIdSource);
                entity.CreatedByCompanyId = entity.CreatedByCompanyIdSource.CompanyId;
            }
            #endregion
            #endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanyNoteChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanyNote</c>
	///</summary>
	public enum CompanyNoteChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Role</c> at CreatedByRoleIdSource
		///</summary>
		[ChildEntityType(typeof(Role))]
		Role,

        ///<summary>
        /// Composite Property for <c>Users</c> at CreatedByUserIdSource
        ///</summary>
        [ChildEntityType(typeof(Users))]
        Users,

        ///<summary>
        /// Composite Property for <c>Companies</c> at CreatedByCompanyIdSource
        ///</summary>
        [ChildEntityType(typeof(Companies))]
        Companies
    }
	
	#endregion CompanyNoteChildEntityTypes
	
	#region CompanyNoteFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanyNoteColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyNote"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyNoteFilterBuilder : SqlFilterBuilder<CompanyNoteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyNoteFilterBuilder class.
		/// </summary>
		public CompanyNoteFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyNoteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyNoteFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyNoteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyNoteFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyNoteFilterBuilder
	
	#region CompanyNoteParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanyNoteColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyNote"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyNoteParameterBuilder : ParameterizedSqlFilterBuilder<CompanyNoteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyNoteParameterBuilder class.
		/// </summary>
		public CompanyNoteParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyNoteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyNoteParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyNoteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyNoteParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyNoteParameterBuilder
	
	#region CompanyNoteSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanyNoteColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyNote"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanyNoteSortBuilder : SqlSortBuilder<CompanyNoteColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyNoteSqlSortBuilder class.
		/// </summary>
		public CompanyNoteSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanyNoteSortBuilder
	
} // end namespace
