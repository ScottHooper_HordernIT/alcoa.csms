﻿using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Entities;
using OfficeOpenXml;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using dal = Repo.CSMS.DAL.EntityModels;
using repo = Repo.CSMS.Service.Database;


namespace ALCOA.CSMS.Website.UserControls.Main
{
    public partial class ContractorDataManagementDetailTrainingTab : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        bool isAdmin;
        repo.Hr.IXxhrCwkHistoryService cwkHistorySvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrCwkHistoryService>();
        repo.Hr.IXxhrAddTrngService addTrainingSvc = ALCOA.CSMS.Website.Global.GetInstance<repo.Hr.IXxhrAddTrngService>();
        XXHR_CWK_HISTORY cwkHistory;
        string CWK_NBR;

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth);
            moduleInit(Page.IsPostBack);

            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.imgBtnExcelAll);
        }

        private void moduleInit(bool postBack)
        {
            isAdmin = auth.RoleId == (int)RoleList.Administrator;
            CWK_NBR = this.Request.QueryString["CWK_NBR"];
            DateTime EFFECTIVE_START_DATE = System.Convert.ToDateTime(this.Request.QueryString["EFFECTIVE_START_DATE"]);

            if (!postBack)
            {
                switch (auth.RoleId)
                {
                    case (int)RoleList.CWKMaintainer:
                    case (int)RoleList.Administrator:
                        //Can access
                        break;

                    case (int)RoleList.CWKEnquiry:
                        //Can access limited
                        this.btnAdd.Visible = false;
                        break;

                    default:
                        Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised user")));
                        break;
                }
            }

            cwkHistory = this.cwkHistorySvc.Get(c => c.CWK_NBR == CWK_NBR && c.EFFECTIVE_START_DATE == EFFECTIVE_START_DATE, null);

            TrainingDataSource.SelectParameters.Clear();
            TrainingDataSource.SelectParameters.Add("EMPL_ID", CWK_NBR);
            TrainingDataSource.DataBind();

            populateUserDetails();
            grid_PopulateDurationUnitsCombo();

            if (!string.IsNullOrEmpty(CWK_NBR))
            {
                if (!postBack)
                {
                    //First load
                    
                }
                else
                {
                    //Postbacks
                }
            }
        }

        protected void populateUserDetails()
        {
            lblFullName.Text = cwkHistory.FULL_NAME;
            lblEmplId.Text = cwkHistory.CWK_NBR;
        }

        protected void grid_PopulateDurationUnitsCombo()
        {
            GridViewDataComboBoxColumn col =  grid.Columns["DURATION_UNITS"] as GridViewDataComboBoxColumn;
            ComboBoxProperties cmb = col.PropertiesComboBox;
            
            cmb.Items.Add(new ListEditItem("Minute", "MIN"));            
            cmb.Items.Add(new ListEditItem("Hour", "H"));
            cmb.Items.Add(new ListEditItem("Day", "D"));            
            cmb.Items.Add(new ListEditItem("Month", "M"));
            cmb.Items.Add(new ListEditItem("Quarter", "Q"));
            cmb.Items.Add(new ListEditItem("Year", "Y"));
        }

        protected void grid_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "EXPIRATION_DATE" && e.Value is string)
            {
                e.DisplayText = String.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(e.Value.ToString()));
            } 
        }

        protected void grid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
        {
            try
            {
                

                string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;
                using (SqlConnection cnAdd = new SqlConnection(sqldb))
                {
                    cnAdd.Open();

                    SqlCommand cmdAdd = new SqlCommand("HR.XXHR_ADD_TRNG_Insert", cnAdd);
                    cmdAdd.CommandTimeout = 200;
                    cmdAdd.CommandType = CommandType.StoredProcedure;
                    cmdAdd.Parameters.AddWithValue("@FULL_NAME", cwkHistory.FULL_NAME);
                    cmdAdd.Parameters.AddWithValue("@EMPL_ID", CWK_NBR);
                    cmdAdd.Parameters.AddWithValue("@COURSE_NAME", Convert.ToString(e.NewValues["COURSE_NAME"]));
                    cmdAdd.Parameters.AddWithValue("@TRAINING_TITLE", Convert.ToString(e.NewValues["TRAINING_TITLE"]));
                    cmdAdd.Parameters.AddWithValue("@COURSE_END_DATE", Convert.ToDateTime(e.NewValues["COURSE_END_DATE"]));
                    cmdAdd.Parameters.AddWithValue("@EXPIRATION_DATE", Convert.ToString(getSQLDateFormat(e.NewValues["EXPIRATION_DATE"])));
                    cmdAdd.Parameters.AddWithValue("@DURATION", Convert.ToString(e.NewValues["DURATION"]));
                    cmdAdd.Parameters.AddWithValue("@DURATION_UNITS", Convert.ToString(e.NewValues["DURATION_UNITS"]));
                    cmdAdd.Parameters.AddWithValue("@PER_TYPE_ID", cwkHistory.PER_TYPE_ID);
                    cmdAdd.Parameters.AddWithValue("@RECORD_TYPE", Convert.ToString(e.NewValues["RECORD_TYPE"]));
                    cmdAdd.Parameters.AddWithValue("@LAST_UPDATE_DATE", DateTime.Now);

                    cmdAdd.ExecuteNonQuery();
                    cnAdd.Close();
                }

                // DB update done, close grid editing
                e.Cancel = true;
                grid.CancelEdit();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        // Used for [EXPIRATION_DATE], which is stored as a string field for some reason
        protected string getSQLDateFormat(object date)
        {
            DateTime d = DateTime.ParseExact(date.ToString().Replace(" 00:00:00", ""), "MM/dd/yyyy", new CultureInfo("en-US"));
            return string.Format("{0}-{1}-{2}", d.Year, d.Month, d.Day);
        }
    }
}
