﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'KpiProjectList' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class KpiProjectList : KpiProjectListBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="KpiProjectList"/> instance.
		///</summary>
		public KpiProjectList():base(){}	
		
		#endregion
	}
}
