<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_SqQuestionnaire_QuestionnaireList_Completed" Codebehind="QuestionnaireList_Completed.ascx.cs" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
        <table style="width: 874px" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td style="width: 874px; padding-top: 2px; text-align: center" class="pageName" align="right">
                        <div align="right">
                            <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow2()">
                                Customize</a>
                        </div>
                    </td>
                </tr>
                <tr style="padding-top: 2px">
                    <td style="height: 13px; text-align: left" colspan="2">
                        <dxwgv:ASPxGridView ID="grid" runat="server" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            Width="874px" OnHtmlRowCreated="grid_RowCreated" KeyFieldName="QuestionnaireId"
                            ClientInstanceName="grid2" AutoGenerateColumns="False">
                            <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                            <SettingsBehavior ColumnResizeMode="Control"></SettingsBehavior>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                <Header SortingImageSpacing="5px" ImageSpacing="5px">
                                </Header>
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <SettingsPager>
                                <AllButton Visible="True">
                                </AllButton>
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <SettingsText EmptyDataRow="You do not have any completed Questionnaires."></SettingsText>
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="QuestionnaireId" ReadOnly="True" FixedStyle="Left"
                                    Width="57px" Caption=" " VisibleIndex="0">
                                    <Settings AllowHeaderFilter="False"></Settings>
                                    <EditFormSettings Visible="False"></EditFormSettings>
                                    <DataItemTemplate>
                                        <asp:HyperLink ID="hlAssess" runat="server" Text="Assess/Review" NavigateUrl=""></asp:HyperLink>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="CompanyName" SortIndex="0" SortOrder="Ascending"
                                    FixedStyle="Left" Width="205px" Caption="Company" VisibleIndex="1">
                                    <Settings SortMode="DisplayText"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Company Status" FieldName="CompanyStatusDesc" Visible="false" Width="185px">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Type" Width="120px" Caption="Type" 
                                    VisibleIndex="6">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="CreatedByUserId" Name="UserBox" Width="150px"
                                    Caption="Created By" VisibleIndex="7">
                                    <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersFullNameDataSource1"
                                        TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
                                    </PropertiesComboBox>
                                    <Settings SortMode="DisplayText"></Settings>
                                    <EditFormSettings Visible="True"></EditFormSettings>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="ModifiedByUserId" Name="UserBox" Width="150px"
                                    Caption="Last Modified By" VisibleIndex="6">
                                    <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersFullNameDataSource1"
                                        TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
                                    </PropertiesComboBox>
                                    <Settings SortMode="DisplayText"></Settings>
                                    <EditFormSettings Visible="True"></EditFormSettings>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" SortIndex="1" SortOrder="Descending"
                                    Width="130px" Caption="Last Modified At" VisibleIndex="5">
                                    <PropertiesDateEdit DisplayFormatString="dd/MM/yy HH:mm">
                                    </PropertiesDateEdit>
                                    <Settings AutoFilterCondition="Contains"></Settings>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="UserDescription" Caption="Presently With" Visible="false" Width="110px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                 <dxwgv:GridViewDataTextColumn FieldName="ActionDescription" Caption="Presently With Details" Visible="false" Width="110px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                 <dxwgv:GridViewDataTextColumn FieldName="QuestionnairePresentlyWithSinceDaysCount" Caption="Days With" VisibleIndex="3" 
                                                                    Width="60px">
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="InitialRiskAssessment" Width="115px" Caption="Procurement Risk Rating"
                                    VisibleIndex="8">
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="MainAssessmentRiskRating" ShowInCustomizationForm="False"
                                    Width="100px" Caption="AssessmentRiskRating" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="MainAssessmentRiskRating" Width="100px"
                                    Caption="33.055.1 Risk Rating" VisibleIndex="8">
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:HyperLink ID="RiskLevel" runat="server" Text="" NavigateUrl=""></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Name="FinalRiskRating" Width="115px" Caption="Final Risk Rating"
                                    VisibleIndex="8">
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="FinalRiskLevel" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="Status" UnboundType="String" Name="Status"
                                    Width="130px" VisibleIndex="10">
                                    <PropertiesComboBox DataSourceID="dsQuestionnaireStatus" TextField="QuestionnaireStatusDesc"
                                        ValueField="QuestionnaireStatusId" ValueType="System.String" DropDownHeight="150px">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="Recommended" Width="105px" 
                                    VisibleIndex="9">
                                    <PropertiesComboBox ValueType="System.Int32">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="1">
                                            </dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="0">
                                            </dxe:ListEditItem>
                                        </Items>
                                        <ItemStyle Font-Bold="True"></ItemStyle>
                                    </PropertiesComboBox>
                                    <Settings SortMode="DisplayText"></Settings>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="SafetyAssessor" UnboundType="String"
                                    Name="EHS Consultant" Width="115px" Caption="Safety Assessor" 
                                    VisibleIndex="11">
                                    <PropertiesComboBox ValueType="System.String">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="ProcurementContactUser" VisibleIndex="2"
                                    Width="160px" Caption="Procurement Contact"  
                                    Settings-SortMode="DisplayText">
                                    <Settings AllowAutoFilter="True" />
                                    <PropertiesComboBox DataSourceID="sqldsProcurementContact" TextField="FullName"
                                        ValueField="FullName" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <Settings AllowAutoFilter="True" />
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="ContractManagerUser" VisibleIndex="4"
                                    Width="160px" Caption="Procurement Functional Supervisor"  
                                    Settings-SortMode="DisplayText">
                                    <PropertiesComboBox DataSourceID="sqldsProcurementFunctionalManager" TextField="FullName"
                                        ValueField="FullName" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <Settings AllowAutoFilter="True" />
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="KWI" Width="40px" Caption="KWI" ToolTip="Approved to work at Kwinana?"
                                    VisibleIndex="12" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblKWI" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="PIN" Width="40px" Caption="PIN" ToolTip="Approved to work at Pinjarra?"
                                    VisibleIndex="13" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPIN" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="WGP" Width="40px" Caption="WGP" ToolTip="Approved to work at Wagerup?"
                                    VisibleIndex="14" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblWGP" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="HUN" Width="40px" Caption="HUN" ToolTip="Approved to work at Huntly (Mine)?"
                                    VisibleIndex="15" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblHUN" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="WDL" Width="40px" Caption="WDL" ToolTip="Approved to work at Willowdale (Mine)?"
                                    VisibleIndex="16" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblWDL" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="BUN" Width="40px" Caption="BUN" ToolTip="Approved to work at Bunbury Port?"
                                    VisibleIndex="17" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBUN" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="FML" Width="40px" Caption="FML" ToolTip="Approved to work at Farmlands?"
                                    VisibleIndex="18" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblFML" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="BGN" Width="40px" Caption="BGN" ToolTip="Approved to work at Booragoon Office?"
                                    VisibleIndex="19" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBGN" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ANG" Width="40px" Caption="ANG" ToolTip="Approved to work at Anglesea?"
                                    VisibleIndex="20" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblANG" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="PTL" Width="40px" Caption="PTL" ToolTip="Approved to work at Portland?"
                                    VisibleIndex="21" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPTL" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="PTH" Width="40px" Caption="PTH" ToolTip="Approved to work at Point Henry?"
                                    VisibleIndex="22" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPTH" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="PEL" Width="50px" Caption="PEEL" ToolTip="Approved to work at Peel Office?"
                                    VisibleIndex="23" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblPEEL" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="ARP" Width="50px" Caption="ARP" ToolTip="Approved to work at ARP Point Henry?"
                                    VisibleIndex="24" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblARP" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="YEN" Width="50px" Caption="YEN" ToolTip="Approved to work at Yennora?"
                                    VisibleIndex="25" Visible="false" ShowInCustomizationForm="true">
                                    <Settings AllowHeaderFilter="False" AllowSort="False"></Settings>
                                    <DataItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblYEN" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </DataItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataComboBoxColumn FieldName="InitialStatus" ShowInCustomizationForm="False"
                                    Width="100px" Caption="Initial Status" Visible="False" VisibleIndex="18">
                                    <PropertiesComboBox DataSourceID="UsersFullNameDataSource1" TextField="UserFullName"
                                        ValueField="UserId" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                    <PropertiesComboBox DataSourceID="dsQuestionnaireStatus" TextField="QuestionnaireStatusDesc"
                                        ValueField="QuestionnaireStatusId" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                </dxwgv:GridViewDataComboBoxColumn>
                                <dxwgv:GridViewDataCheckColumn FieldName="ProcurementModified" Visible="False">
                                </dxwgv:GridViewDataCheckColumn>
                            </Columns>
                            <Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowHorizontalScrollBar="True"
                                ShowVerticalScrollBar="True" VerticalScrollableHeight="225"></Settings>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>
                    </td>
                </tr>
                <tr>
                    <td style="height: 13px; text-align: left" colspan="2">
                        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 50%; height: 38px">
                                        <dxe:ASPxCheckBox ID="cbShowAll" runat="server" AutoPostBack="true" OnCheckedChanged="cbShowAll_CheckedChanged"
                                            Text="Show All" Checked="False">
                                        </dxe:ASPxCheckBox>
                                    </td>
                                    <td style="width: 50%; height: 38px; text-align: right">
                                        <div align="right">
                                        <uc1:ExportButtons ID="ucExportButtons" runat="server"></uc1:ExportButtons>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
<data:CompaniesEhsConsultantsDataSource ID="CompaniesEhsConsultantsDataSource1" runat="server">
</data:CompaniesEhsConsultantsDataSource>
<data:QuestionnaireStatusDataSource ID="dsQuestionnaireStatus" runat="server">
</data:QuestionnaireStatusDataSource>
<data:SitesDataSource ID="dsSites" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:SitesDataSource>
<data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server" Sort="UserFullName ASC">
</data:UsersFullNameDataSource>

<asp:SqlDataSource ID="sqldsProcurementContact" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT DISTINCT U.UserId As [UserId], U.UserFullName As [FullName] FROM QuestionnaireWithLocationApprovalView As Q INNER JOIN UsersFullName As U ON Q.ProcurementContactUserId = U.UserId ORDER BY U.UserFullName ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsProcurementFunctionalManager" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT DISTINCT U.UserId As [UserId], U.UserFullName As [FullName] FROM QuestionnaireWithLocationApprovalView As Q INNER JOIN UsersFullName As U ON Q.ContractManagerUserId = U.UserId ORDER BY U.UserFullName ASC">
</asp:SqlDataSource>