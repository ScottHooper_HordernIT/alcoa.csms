﻿using System;
using System.Collections.Generic;
using System.Text;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;
using ALCOA.CSMS.Batch.Common.Log;

namespace ALCOA.CSMS.BatchJobScripts
{
    class HealthCheck
    {
        public static void Update(bool success, string activity)
        {
            Update(success, activity, null, null); //CT 20/11/15 - added Exception parameter
        }
        public static void Update(bool success, string activity, string notes, Exception ex)
        {

            SystemLogService slService = new SystemLogService();
            SystemLog sl = new SystemLog();

            sl.DateTime = DateTime.Now;

            DateTime dtDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            sl.Date = dtDate;

            sl.Activity = activity;
            sl.Success = success;
            sl.Notes = notes;
            slService.Save(sl);

            if (success)
            {
                Environment.ExitCode = 0;
            }
            else
            if (!success)
            {
                //20/11/15 Cindi Thornton - add code to use different log4net depending on what is passed in
                StringBuilder healthCheckMessage = new StringBuilder();
                healthCheckMessage.Append(string.Format("HealthCheck FAIL. {0}User ID: {1}, Machine: {2}{0}DateTime: {3}{0}Activity: {4}",
                    Environment.NewLine, Environment.UserName, Environment.MachineName, sl.DateTime, sl.Activity));

                if (sl.Notes != null) {
                    healthCheckMessage.Append(string.Format(" {0}Notes:{0}{1}", Environment.NewLine, sl.Notes));
                }

                if (ex == null)
                {
                    Log4NetService.LogError(healthCheckMessage.ToString());
                }
                else
                {
                    Log4NetService.LogError(healthCheckMessage.ToString(), ex);
                }
                //Log4NetService.LogError(string.Format("HealthCheck FAIL. {0}User ID: {1}, Machine: {2}{0}DateTime: {3}{0}Activity: {4} {0}Notes: {5}", 
                //    Environment.NewLine, Environment.UserName, Environment.MachineName, sl.DateTime, sl.Activity,sl.Notes));

                Environment.ExitCode = 1; // set to fail exit code
                Log4NetService.LogInfo("Exit exit code: " + Environment.ExitCode);
                Environment.Exit(Environment.ExitCode);
            }

            Log4NetService.LogInfo("Exit exit code: " + Environment.ExitCode);
            //End change 20/11/15 Cindi Thornton
        }

        public static void Update(bool success, string activity, string notes)
        {
            Update(success, activity, notes, null);  //CT 20/11/15 - added Exception parameter
        }
    }
}
