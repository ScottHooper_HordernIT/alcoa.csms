﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileDbUsersCompaniesSitesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class FileDbUsersCompaniesSitesProviderBaseCore : EntityViewProviderBase<FileDbUsersCompaniesSites>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;FileDbUsersCompaniesSites&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;FileDbUsersCompaniesSites&gt;"/></returns>
		protected static VList&lt;FileDbUsersCompaniesSites&gt; Fill(DataSet dataSet, VList<FileDbUsersCompaniesSites> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<FileDbUsersCompaniesSites>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;FileDbUsersCompaniesSites&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<FileDbUsersCompaniesSites>"/></returns>
		protected static VList&lt;FileDbUsersCompaniesSites&gt; Fill(DataTable dataTable, VList<FileDbUsersCompaniesSites> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					FileDbUsersCompaniesSites c = new FileDbUsersCompaniesSites();
					c.FileId = (Convert.IsDBNull(row["FileId"]))?(int)0:(System.Int32)row["FileId"];
					c.FileName = (Convert.IsDBNull(row["FileName"]))?string.Empty:(System.String)row["FileName"];
					c.Description = (Convert.IsDBNull(row["Description"]))?string.Empty:(System.String)row["Description"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.UserLogon = (Convert.IsDBNull(row["UserLogon"]))?string.Empty:(System.String)row["UserLogon"];
					c.LastName = (Convert.IsDBNull(row["LastName"]))?string.Empty:(System.String)row["LastName"];
					c.FirstName = (Convert.IsDBNull(row["FirstName"]))?string.Empty:(System.String)row["FirstName"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;FileDbUsersCompaniesSites&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;FileDbUsersCompaniesSites&gt;"/></returns>
		protected VList<FileDbUsersCompaniesSites> Fill(IDataReader reader, VList<FileDbUsersCompaniesSites> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					FileDbUsersCompaniesSites entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<FileDbUsersCompaniesSites>("FileDbUsersCompaniesSites",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new FileDbUsersCompaniesSites();
					}
					
					entity.SuppressEntityEvents = true;

					entity.FileId = (System.Int32)reader[((int)FileDbUsersCompaniesSitesColumn.FileId)];
					//entity.FileId = (Convert.IsDBNull(reader["FileId"]))?(int)0:(System.Int32)reader["FileId"];
					entity.FileName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.FileName)];
					//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
					entity.Description = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.Description)];
					//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
					entity.ModifiedDate = (System.DateTime)reader[((int)FileDbUsersCompaniesSitesColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)FileDbUsersCompaniesSitesColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.UserLogon = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.UserLogon)];
					//entity.UserLogon = (Convert.IsDBNull(reader["UserLogon"]))?string.Empty:(System.String)reader["UserLogon"];
					entity.LastName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.LastName)];
					//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
					entity.FirstName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.FirstName)];
					//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
					entity.CompanyId = (System.Int32)reader[((int)FileDbUsersCompaniesSitesColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CompanyName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="FileDbUsersCompaniesSites"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="FileDbUsersCompaniesSites"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, FileDbUsersCompaniesSites entity)
		{
			reader.Read();
			entity.FileId = (System.Int32)reader[((int)FileDbUsersCompaniesSitesColumn.FileId)];
			//entity.FileId = (Convert.IsDBNull(reader["FileId"]))?(int)0:(System.Int32)reader["FileId"];
			entity.FileName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.FileName)];
			//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
			entity.Description = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.Description)];
			//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
			entity.ModifiedDate = (System.DateTime)reader[((int)FileDbUsersCompaniesSitesColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileDbUsersCompaniesSitesColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.UserLogon = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.UserLogon)];
			//entity.UserLogon = (Convert.IsDBNull(reader["UserLogon"]))?string.Empty:(System.String)reader["UserLogon"];
			entity.LastName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.LastName)];
			//entity.LastName = (Convert.IsDBNull(reader["LastName"]))?string.Empty:(System.String)reader["LastName"];
			entity.FirstName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.FirstName)];
			//entity.FirstName = (Convert.IsDBNull(reader["FirstName"]))?string.Empty:(System.String)reader["FirstName"];
			entity.CompanyId = (System.Int32)reader[((int)FileDbUsersCompaniesSitesColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CompanyName = (System.String)reader[((int)FileDbUsersCompaniesSitesColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="FileDbUsersCompaniesSites"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="FileDbUsersCompaniesSites"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, FileDbUsersCompaniesSites entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileId = (Convert.IsDBNull(dataRow["FileId"]))?(int)0:(System.Int32)dataRow["FileId"];
			entity.FileName = (Convert.IsDBNull(dataRow["FileName"]))?string.Empty:(System.String)dataRow["FileName"];
			entity.Description = (Convert.IsDBNull(dataRow["Description"]))?string.Empty:(System.String)dataRow["Description"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.UserLogon = (Convert.IsDBNull(dataRow["UserLogon"]))?string.Empty:(System.String)dataRow["UserLogon"];
			entity.LastName = (Convert.IsDBNull(dataRow["LastName"]))?string.Empty:(System.String)dataRow["LastName"];
			entity.FirstName = (Convert.IsDBNull(dataRow["FirstName"]))?string.Empty:(System.String)dataRow["FirstName"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region FileDbUsersCompaniesSitesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbUsersCompaniesSitesFilterBuilder : SqlFilterBuilder<FileDbUsersCompaniesSitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesFilterBuilder class.
		/// </summary>
		public FileDbUsersCompaniesSitesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbUsersCompaniesSitesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbUsersCompaniesSitesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbUsersCompaniesSitesFilterBuilder

	#region FileDbUsersCompaniesSitesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbUsersCompaniesSitesParameterBuilder : ParameterizedSqlFilterBuilder<FileDbUsersCompaniesSitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesParameterBuilder class.
		/// </summary>
		public FileDbUsersCompaniesSitesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbUsersCompaniesSitesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbUsersCompaniesSitesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbUsersCompaniesSitesParameterBuilder
	
	#region FileDbUsersCompaniesSitesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbUsersCompaniesSites"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileDbUsersCompaniesSitesSortBuilder : SqlSortBuilder<FileDbUsersCompaniesSitesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbUsersCompaniesSitesSqlSortBuilder class.
		/// </summary>
		public FileDbUsersCompaniesSitesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileDbUsersCompaniesSitesSortBuilder

} // end namespace
