﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireWithLocationApprovalViewProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireWithLocationApprovalViewProviderBaseCore : EntityViewProviderBase<QuestionnaireWithLocationApprovalView>
	{
		#region Custom Methods
		
		
		#region _QuestionnaireWithLocationApprovalView_GetProcurementContactHsAssessor_ByCompanyId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementContactHsAssessor_ByCompanyId(System.Int32? companyId)
		{
			return GetProcurementContactHsAssessor_ByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementContactHsAssessor_ByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return GetProcurementContactHsAssessor_ByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetProcurementContactHsAssessor_ByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetProcurementContactHsAssessor_ByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_GetProcurementContactHsAssessor_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetProcurementContactHsAssessor_ByCompanyId(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalView_Companies
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_Companies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies()
		{
			return Companies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_Companies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies(int start, int pageLength)
		{
			return Companies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_Companies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies(TransactionManager transactionManager)
		{
			return Companies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_Companies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Companies(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalView_NotAssessmentComplete_ByUserId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NotAssessmentComplete_ByUserId(System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return NotAssessmentComplete_ByUserId(null, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NotAssessmentComplete_ByUserId(int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return NotAssessmentComplete_ByUserId(null, start, pageLength , statusId, userId, proc);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet NotAssessmentComplete_ByUserId(TransactionManager transactionManager, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return NotAssessmentComplete_ByUserId(transactionManager, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_NotAssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet NotAssessmentComplete_ByUserId(TransactionManager transactionManager, int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc);
		
		#endregion

		
		#region _QuestionnaireWithLocationApprovalView_AssessmentComplete_ByUserId
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet AssessmentComplete_ByUserId(System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return AssessmentComplete_ByUserId(null, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet AssessmentComplete_ByUserId(int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return AssessmentComplete_ByUserId(null, start, pageLength , statusId, userId, proc);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet AssessmentComplete_ByUserId(TransactionManager transactionManager, System.Int32? statusId, System.Int32? userId, System.Boolean? proc)
		{
			return AssessmentComplete_ByUserId(transactionManager, 0, int.MaxValue , statusId, userId, proc);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireWithLocationApprovalView_AssessmentComplete_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="statusId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="proc"> A <c>System.Boolean?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet AssessmentComplete_ByUserId(TransactionManager transactionManager, int start, int pageLength, System.Int32? statusId, System.Int32? userId, System.Boolean? proc);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireWithLocationApprovalView&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireWithLocationApprovalView&gt;"/></returns>
		protected static VList&lt;QuestionnaireWithLocationApprovalView&gt; Fill(DataSet dataSet, VList<QuestionnaireWithLocationApprovalView> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireWithLocationApprovalView>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireWithLocationApprovalView&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireWithLocationApprovalView>"/></returns>
		protected static VList&lt;QuestionnaireWithLocationApprovalView&gt; Fill(DataTable dataTable, VList<QuestionnaireWithLocationApprovalView> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireWithLocationApprovalView c = new QuestionnaireWithLocationApprovalView();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.CreatedByUserId = (Convert.IsDBNull(row["CreatedByUserId"]))?(int)0:(System.Int32)row["CreatedByUserId"];
					c.CreatedByUser = (Convert.IsDBNull(row["CreatedByUser"]))?string.Empty:(System.String)row["CreatedByUser"];
					c.CreatedDate = (Convert.IsDBNull(row["CreatedDate"]))?DateTime.MinValue:(System.DateTime)row["CreatedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.ModifiedByUser = (Convert.IsDBNull(row["ModifiedByUser"]))?string.Empty:(System.String)row["ModifiedByUser"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.ApprovedDate = (Convert.IsDBNull(row["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)row["ApprovedDate"];
					c.DaysSinceLastActivity = (Convert.IsDBNull(row["DaysSinceLastActivity"]))?(int)0:(System.Int32?)row["DaysSinceLastActivity"];
					c.QmaLastModified = (Convert.IsDBNull(row["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)row["QMALastModified"];
					c.QvaLastModified = (Convert.IsDBNull(row["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)row["QVALastModified"];
					c.AssessedDate = (Convert.IsDBNull(row["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)row["AssessedDate"];
					c.AssessedByUserId = (Convert.IsDBNull(row["AssessedByUserId"]))?(int)0:(System.Int32?)row["AssessedByUserId"];
					c.Status = (Convert.IsDBNull(row["Status"]))?(int)0:(System.Int32)row["Status"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.InitialCreatedByUserId = (Convert.IsDBNull(row["InitialCreatedByUserId"]))?(int)0:(System.Int32?)row["InitialCreatedByUserId"];
					c.InitialCreatedDate = (Convert.IsDBNull(row["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialCreatedDate"];
					c.InitialSubmittedByUserId = (Convert.IsDBNull(row["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)row["InitialSubmittedByUserId"];
					c.InitialSubmittedDate = (Convert.IsDBNull(row["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialSubmittedDate"];
					c.MainCreatedByUserId = (Convert.IsDBNull(row["MainCreatedByUserId"]))?(int)0:(System.Int32?)row["MainCreatedByUserId"];
					c.MainCreatedDate = (Convert.IsDBNull(row["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["MainCreatedDate"];
					c.VerificationCreatedByUserId = (Convert.IsDBNull(row["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)row["VerificationCreatedByUserId"];
					c.VerificationCreatedDate = (Convert.IsDBNull(row["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)row["VerificationCreatedDate"];
					c.IsMainRequired = (Convert.IsDBNull(row["IsMainRequired"]))?false:(System.Boolean)row["IsMainRequired"];
					c.IsVerificationRequired = (Convert.IsDBNull(row["IsVerificationRequired"]))?false:(System.Boolean)row["IsVerificationRequired"];
					c.InitialCategoryHigh = (Convert.IsDBNull(row["InitialCategoryHigh"]))?false:(System.Boolean?)row["InitialCategoryHigh"];
					c.InitialModifiedByUserId = (Convert.IsDBNull(row["InitialModifiedByUserId"]))?(int)0:(System.Int32?)row["InitialModifiedByUserId"];
					c.InitialModifiedDate = (Convert.IsDBNull(row["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["InitialModifiedDate"];
					c.InitialStatus = (Convert.IsDBNull(row["InitialStatus"]))?(int)0:(System.Int32)row["InitialStatus"];
					c.InitialRiskAssessment = (Convert.IsDBNull(row["InitialRiskAssessment"]))?string.Empty:(System.String)row["InitialRiskAssessment"];
					c.MainModifiedByUserId = (Convert.IsDBNull(row["MainModifiedByUserId"]))?(int)0:(System.Int32?)row["MainModifiedByUserId"];
					c.MainModifiedDate = (Convert.IsDBNull(row["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["MainModifiedDate"];
					c.MainScoreExpectations = (Convert.IsDBNull(row["MainScoreExpectations"]))?(int)0:(System.Int32?)row["MainScoreExpectations"];
					c.MainScoreOverall = (Convert.IsDBNull(row["MainScoreOverall"]))?(int)0:(System.Int32?)row["MainScoreOverall"];
					c.MainScoreResults = (Convert.IsDBNull(row["MainScoreResults"]))?(int)0:(System.Int32?)row["MainScoreResults"];
					c.MainScoreStaffing = (Convert.IsDBNull(row["MainScoreStaffing"]))?(int)0:(System.Int32?)row["MainScoreStaffing"];
					c.MainScoreSystems = (Convert.IsDBNull(row["MainScoreSystems"]))?(int)0:(System.Int32?)row["MainScoreSystems"];
					c.MainStatus = (Convert.IsDBNull(row["MainStatus"]))?(int)0:(System.Int32)row["MainStatus"];
					c.MainAssessmentByUserId = (Convert.IsDBNull(row["MainAssessmentByUserId"]))?(int)0:(System.Int32?)row["MainAssessmentByUserId"];
					c.MainAssessmentComments = (Convert.IsDBNull(row["MainAssessmentComments"]))?string.Empty:(System.String)row["MainAssessmentComments"];
					c.MainAssessmentDate = (Convert.IsDBNull(row["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentDate"];
					c.MainAssessmentRiskRating = (Convert.IsDBNull(row["MainAssessmentRiskRating"]))?string.Empty:(System.String)row["MainAssessmentRiskRating"];
					c.MainAssessmentStatus = (Convert.IsDBNull(row["MainAssessmentStatus"]))?string.Empty:(System.String)row["MainAssessmentStatus"];
					c.MainAssessmentValidTo = (Convert.IsDBNull(row["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)row["MainAssessmentValidTo"];
					c.MainScorePexpectations = (Convert.IsDBNull(row["MainScorePExpectations"]))?(int)0:(System.Int32?)row["MainScorePExpectations"];
					c.MainScorePoverall = (Convert.IsDBNull(row["MainScorePOverall"]))?(int)0:(System.Int32?)row["MainScorePOverall"];
					c.MainScorePresults = (Convert.IsDBNull(row["MainScorePResults"]))?(int)0:(System.Int32?)row["MainScorePResults"];
					c.MainScorePstaffing = (Convert.IsDBNull(row["MainScorePStaffing"]))?(int)0:(System.Int32?)row["MainScorePStaffing"];
					c.MainScorePsystems = (Convert.IsDBNull(row["MainScorePSystems"]))?(int)0:(System.Int32?)row["MainScorePSystems"];
					c.VerificationModifiedByUserId = (Convert.IsDBNull(row["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)row["VerificationModifiedByUserId"];
					c.VerificationModifiedDate = (Convert.IsDBNull(row["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)row["VerificationModifiedDate"];
					c.VerificationRiskRating = (Convert.IsDBNull(row["VerificationRiskRating"]))?string.Empty:(System.String)row["VerificationRiskRating"];
					c.VerificationStatus = (Convert.IsDBNull(row["VerificationStatus"]))?(int)0:(System.Int32)row["VerificationStatus"];
					c.SupplierContactFirstName = (Convert.IsDBNull(row["SupplierContactFirstName"]))?string.Empty:(System.String)row["SupplierContactFirstName"];
					c.SupplierContactLastName = (Convert.IsDBNull(row["SupplierContactLastName"]))?string.Empty:(System.String)row["SupplierContactLastName"];
					c.SupplierContactEmail = (Convert.IsDBNull(row["SupplierContactEmail"]))?string.Empty:(System.String)row["SupplierContactEmail"];
					c.SupplierContactTitle = (Convert.IsDBNull(row["SupplierContactTitle"]))?string.Empty:(System.String)row["SupplierContactTitle"];
					c.SupplierContactPhone = (Convert.IsDBNull(row["SupplierContactPhone"]))?string.Empty:(System.String)row["SupplierContactPhone"];
					c.ProcurementContactUserId = (Convert.IsDBNull(row["ProcurementContactUserId"]))?(int)0:(System.Int32?)row["ProcurementContactUserId"];
					c.ContractManagerUserId = (Convert.IsDBNull(row["ContractManagerUserId"]))?(int)0:(System.Int32?)row["ContractManagerUserId"];
					c.Kwi = (Convert.IsDBNull(row["KWI"]))?string.Empty:(System.String)row["KWI"];
					c.Pin = (Convert.IsDBNull(row["PIN"]))?string.Empty:(System.String)row["PIN"];
					c.Wgp = (Convert.IsDBNull(row["WGP"]))?string.Empty:(System.String)row["WGP"];
					c.Hun = (Convert.IsDBNull(row["HUN"]))?string.Empty:(System.String)row["HUN"];
					c.Wdl = (Convert.IsDBNull(row["WDL"]))?string.Empty:(System.String)row["WDL"];
					c.Bun = (Convert.IsDBNull(row["BUN"]))?string.Empty:(System.String)row["BUN"];
					c.Fml = (Convert.IsDBNull(row["FML"]))?string.Empty:(System.String)row["FML"];
					c.Bgn = (Convert.IsDBNull(row["BGN"]))?string.Empty:(System.String)row["BGN"];
					c.Ang = (Convert.IsDBNull(row["ANG"]))?string.Empty:(System.String)row["ANG"];
					c.Ptl = (Convert.IsDBNull(row["PTL"]))?string.Empty:(System.String)row["PTL"];
					c.Pth = (Convert.IsDBNull(row["PTH"]))?string.Empty:(System.String)row["PTH"];
					c.Pel = (Convert.IsDBNull(row["PEL"]))?string.Empty:(System.String)row["PEL"];
					c.Arp = (Convert.IsDBNull(row["ARP"]))?string.Empty:(System.String)row["ARP"];
					c.Yen = (Convert.IsDBNull(row["YEN"]))?string.Empty:(System.String)row["YEN"];
					c.ProcurementModified = (Convert.IsDBNull(row["ProcurementModified"]))?false:(System.Boolean)row["ProcurementModified"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.SafetyAssessorUserId = (Convert.IsDBNull(row["SafetyAssessorUserId"]))?(int)0:(System.Int32?)row["SafetyAssessorUserId"];
					c.SubContractor = (Convert.IsDBNull(row["SubContractor"]))?false:(System.Boolean?)row["SubContractor"];
					c.CompanyStatusId = (Convert.IsDBNull(row["CompanyStatusId"]))?(int)0:(System.Int32)row["CompanyStatusId"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.Deactivated = (Convert.IsDBNull(row["Deactivated"]))?false:(System.Boolean?)row["Deactivated"];
					c.ProcurementContactUser = (Convert.IsDBNull(row["ProcurementContactUser"]))?string.Empty:(System.String)row["ProcurementContactUser"];
					c.ContractManagerUser = (Convert.IsDBNull(row["ContractManagerUser"]))?string.Empty:(System.String)row["ContractManagerUser"];
					c.NoSqExpiryEmailsSent = (Convert.IsDBNull(row["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)row["NoSqExpiryEmailsSent"];
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(row["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithUserId"];
					c.ProcessNo = (Convert.IsDBNull(row["ProcessNo"]))?(int)0:(System.Int32?)row["ProcessNo"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.ActionName = (Convert.IsDBNull(row["ActionName"]))?string.Empty:(System.String)row["ActionName"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(row["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithSinceDaysCount"];
					c.Expr1 = (Convert.IsDBNull(row["Expr1"]))?(int)0:(System.Int32?)row["Expr1"];
					c.NoReminderEmailsSent = (Convert.IsDBNull(row["NoReminderEmailsSent"]))?(int)0:(System.Int32?)row["NoReminderEmailsSent"];
					c.LastReminderEmailSentOn = (Convert.IsDBNull(row["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)row["LastReminderEmailSentOn"];
					c.SupplierContactVerifiedOn = (Convert.IsDBNull(row["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)row["SupplierContactVerifiedOn"];
					c.LevelOfSupervision = (Convert.IsDBNull(row["LevelOfSupervision"]))?string.Empty:(System.String)row["LevelOfSupervision"];
					c.RecommendedComments = (Convert.IsDBNull(row["RecommendedComments"]))?string.Empty:(System.String)row["RecommendedComments"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.IsReQualification = (Convert.IsDBNull(row["IsReQualification"]))?false:(System.Boolean)row["IsReQualification"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireWithLocationApprovalView&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireWithLocationApprovalView&gt;"/></returns>
		protected VList<QuestionnaireWithLocationApprovalView> Fill(IDataReader reader, VList<QuestionnaireWithLocationApprovalView> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireWithLocationApprovalView entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireWithLocationApprovalView>("QuestionnaireWithLocationApprovalView",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireWithLocationApprovalView();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CreatedByUserId)];
					//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
					entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CreatedByUser)];
					//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
					entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CreatedDate)];
					//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedByUser)];
					//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
					entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedDate)];
					//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
					entity.DaysSinceLastActivity = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.DaysSinceLastActivity)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.DaysSinceLastActivity)];
					//entity.DaysSinceLastActivity = (Convert.IsDBNull(reader["DaysSinceLastActivity"]))?(int)0:(System.Int32?)reader["DaysSinceLastActivity"];
					entity.QmaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QmaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QmaLastModified)];
					//entity.QmaLastModified = (Convert.IsDBNull(reader["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QMALastModified"];
					entity.QvaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QvaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QvaLastModified)];
					//entity.QvaLastModified = (Convert.IsDBNull(reader["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QVALastModified"];
					entity.AssessedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.AssessedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.AssessedDate)];
					//entity.AssessedDate = (Convert.IsDBNull(reader["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)reader["AssessedDate"];
					entity.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.AssessedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.AssessedByUserId)];
					//entity.AssessedByUserId = (Convert.IsDBNull(reader["AssessedByUserId"]))?(int)0:(System.Int32?)reader["AssessedByUserId"];
					entity.Status = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Status)];
					//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedByUserId)];
					//entity.InitialCreatedByUserId = (Convert.IsDBNull(reader["InitialCreatedByUserId"]))?(int)0:(System.Int32?)reader["InitialCreatedByUserId"];
					entity.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedDate)];
					//entity.InitialCreatedDate = (Convert.IsDBNull(reader["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialCreatedDate"];
					entity.InitialSubmittedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedByUserId)];
					//entity.InitialSubmittedByUserId = (Convert.IsDBNull(reader["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)reader["InitialSubmittedByUserId"];
					entity.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedDate)];
					//entity.InitialSubmittedDate = (Convert.IsDBNull(reader["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialSubmittedDate"];
					entity.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedByUserId)];
					//entity.MainCreatedByUserId = (Convert.IsDBNull(reader["MainCreatedByUserId"]))?(int)0:(System.Int32?)reader["MainCreatedByUserId"];
					entity.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedDate)];
					//entity.MainCreatedDate = (Convert.IsDBNull(reader["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainCreatedDate"];
					entity.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedByUserId)];
					//entity.VerificationCreatedByUserId = (Convert.IsDBNull(reader["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)reader["VerificationCreatedByUserId"];
					entity.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedDate)];
					//entity.VerificationCreatedDate = (Convert.IsDBNull(reader["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationCreatedDate"];
					entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.IsMainRequired)];
					//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
					entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.IsVerificationRequired)];
					//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
					entity.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialCategoryHigh)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialCategoryHigh)];
					//entity.InitialCategoryHigh = (Convert.IsDBNull(reader["InitialCategoryHigh"]))?false:(System.Boolean?)reader["InitialCategoryHigh"];
					entity.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedByUserId)];
					//entity.InitialModifiedByUserId = (Convert.IsDBNull(reader["InitialModifiedByUserId"]))?(int)0:(System.Int32?)reader["InitialModifiedByUserId"];
					entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedDate)];
					//entity.InitialModifiedDate = (Convert.IsDBNull(reader["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialModifiedDate"];
					entity.InitialStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialStatus)];
					//entity.InitialStatus = (Convert.IsDBNull(reader["InitialStatus"]))?(int)0:(System.Int32)reader["InitialStatus"];
					entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialRiskAssessment)];
					//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
					entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedByUserId)];
					//entity.MainModifiedByUserId = (Convert.IsDBNull(reader["MainModifiedByUserId"]))?(int)0:(System.Int32?)reader["MainModifiedByUserId"];
					entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedDate)];
					//entity.MainModifiedDate = (Convert.IsDBNull(reader["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainModifiedDate"];
					entity.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreExpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreExpectations)];
					//entity.MainScoreExpectations = (Convert.IsDBNull(reader["MainScoreExpectations"]))?(int)0:(System.Int32?)reader["MainScoreExpectations"];
					entity.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreOverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreOverall)];
					//entity.MainScoreOverall = (Convert.IsDBNull(reader["MainScoreOverall"]))?(int)0:(System.Int32?)reader["MainScoreOverall"];
					entity.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreResults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreResults)];
					//entity.MainScoreResults = (Convert.IsDBNull(reader["MainScoreResults"]))?(int)0:(System.Int32?)reader["MainScoreResults"];
					entity.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreStaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreStaffing)];
					//entity.MainScoreStaffing = (Convert.IsDBNull(reader["MainScoreStaffing"]))?(int)0:(System.Int32?)reader["MainScoreStaffing"];
					entity.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreSystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreSystems)];
					//entity.MainScoreSystems = (Convert.IsDBNull(reader["MainScoreSystems"]))?(int)0:(System.Int32?)reader["MainScoreSystems"];
					entity.MainStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainStatus)];
					//entity.MainStatus = (Convert.IsDBNull(reader["MainStatus"]))?(int)0:(System.Int32)reader["MainStatus"];
					entity.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentByUserId)];
					//entity.MainAssessmentByUserId = (Convert.IsDBNull(reader["MainAssessmentByUserId"]))?(int)0:(System.Int32?)reader["MainAssessmentByUserId"];
					entity.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentComments)];
					//entity.MainAssessmentComments = (Convert.IsDBNull(reader["MainAssessmentComments"]))?string.Empty:(System.String)reader["MainAssessmentComments"];
					entity.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentDate)];
					//entity.MainAssessmentDate = (Convert.IsDBNull(reader["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentDate"];
					entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentRiskRating)];
					//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
					entity.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentStatus)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentStatus)];
					//entity.MainAssessmentStatus = (Convert.IsDBNull(reader["MainAssessmentStatus"]))?string.Empty:(System.String)reader["MainAssessmentStatus"];
					entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentValidTo)];
					//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
					entity.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePexpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePexpectations)];
					//entity.MainScorePexpectations = (Convert.IsDBNull(reader["MainScorePExpectations"]))?(int)0:(System.Int32?)reader["MainScorePExpectations"];
					entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePoverall)];
					//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
					entity.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePresults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePresults)];
					//entity.MainScorePresults = (Convert.IsDBNull(reader["MainScorePResults"]))?(int)0:(System.Int32?)reader["MainScorePResults"];
					entity.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePstaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePstaffing)];
					//entity.MainScorePstaffing = (Convert.IsDBNull(reader["MainScorePStaffing"]))?(int)0:(System.Int32?)reader["MainScorePStaffing"];
					entity.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePsystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePsystems)];
					//entity.MainScorePsystems = (Convert.IsDBNull(reader["MainScorePSystems"]))?(int)0:(System.Int32?)reader["MainScorePSystems"];
					entity.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedByUserId)];
					//entity.VerificationModifiedByUserId = (Convert.IsDBNull(reader["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)reader["VerificationModifiedByUserId"];
					entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedDate)];
					//entity.VerificationModifiedDate = (Convert.IsDBNull(reader["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationModifiedDate"];
					entity.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationRiskRating)];
					//entity.VerificationRiskRating = (Convert.IsDBNull(reader["VerificationRiskRating"]))?string.Empty:(System.String)reader["VerificationRiskRating"];
					entity.VerificationStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationStatus)];
					//entity.VerificationStatus = (Convert.IsDBNull(reader["VerificationStatus"]))?(int)0:(System.Int32)reader["VerificationStatus"];
					entity.SupplierContactFirstName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactFirstName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactFirstName)];
					//entity.SupplierContactFirstName = (Convert.IsDBNull(reader["SupplierContactFirstName"]))?string.Empty:(System.String)reader["SupplierContactFirstName"];
					entity.SupplierContactLastName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactLastName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactLastName)];
					//entity.SupplierContactLastName = (Convert.IsDBNull(reader["SupplierContactLastName"]))?string.Empty:(System.String)reader["SupplierContactLastName"];
					entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactEmail)];
					//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
					entity.SupplierContactTitle = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactTitle)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactTitle)];
					//entity.SupplierContactTitle = (Convert.IsDBNull(reader["SupplierContactTitle"]))?string.Empty:(System.String)reader["SupplierContactTitle"];
					entity.SupplierContactPhone = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactPhone)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactPhone)];
					//entity.SupplierContactPhone = (Convert.IsDBNull(reader["SupplierContactPhone"]))?string.Empty:(System.String)reader["SupplierContactPhone"];
					entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUserId)];
					//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
					entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUserId)];
					//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?(int)0:(System.Int32?)reader["ContractManagerUserId"];
					entity.Kwi = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Kwi)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Kwi)];
					//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
					entity.Pin = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Pin)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Pin)];
					//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
					entity.Wgp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Wgp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Wgp)];
					//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
					entity.Hun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Hun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Hun)];
					//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
					entity.Wdl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Wdl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Wdl)];
					//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
					entity.Bun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Bun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Bun)];
					//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
					entity.Fml = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Fml)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Fml)];
					//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
					entity.Bgn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Bgn)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Bgn)];
					//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
					entity.Ang = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Ang)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Ang)];
					//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
					entity.Ptl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Ptl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Ptl)];
					//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
					entity.Pth = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Pth)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Pth)];
					//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
					entity.Pel = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Pel)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Pel)];
					//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
					entity.Arp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Arp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Arp)];
					//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
					entity.Yen = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Yen)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Yen)];
					//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
					entity.ProcurementModified = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementModified)];
					//entity.ProcurementModified = (Convert.IsDBNull(reader["ProcurementModified"]))?false:(System.Boolean)reader["ProcurementModified"];
					entity.CompanyName = (System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.SafetyAssessorUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessorUserId)];
					//entity.SafetyAssessorUserId = (Convert.IsDBNull(reader["SafetyAssessorUserId"]))?(int)0:(System.Int32?)reader["SafetyAssessorUserId"];
					entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SubContractor)];
					//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
					entity.CompanyStatusId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyStatusId)];
					//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.Deactivated = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Deactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Deactivated)];
					//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
					entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUser)];
					//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
					entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUser)];
					//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
					entity.NoSqExpiryEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.NoSqExpiryEmailsSent)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.NoSqExpiryEmailsSent)];
					//entity.NoSqExpiryEmailsSent = (Convert.IsDBNull(reader["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)reader["NoSqExpiryEmailsSent"];
					entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithUserId)];
					//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
					entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcessNo)];
					//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
					entity.UserName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.ActionName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ActionName)];
					//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSinceDaysCount)];
					//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
					entity.Expr1 = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Expr1)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Expr1)];
					//entity.Expr1 = (Convert.IsDBNull(reader["Expr1"]))?(int)0:(System.Int32?)reader["Expr1"];
					entity.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.NoReminderEmailsSent)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.NoReminderEmailsSent)];
					//entity.NoReminderEmailsSent = (Convert.IsDBNull(reader["NoReminderEmailsSent"]))?(int)0:(System.Int32?)reader["NoReminderEmailsSent"];
					entity.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.LastReminderEmailSentOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.LastReminderEmailSentOn)];
					//entity.LastReminderEmailSentOn = (Convert.IsDBNull(reader["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)reader["LastReminderEmailSentOn"];
					entity.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactVerifiedOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactVerifiedOn)];
					//entity.SupplierContactVerifiedOn = (Convert.IsDBNull(reader["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)reader["SupplierContactVerifiedOn"];
					entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.LevelOfSupervision)];
					//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
					entity.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.RecommendedComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.RecommendedComments)];
					//entity.RecommendedComments = (Convert.IsDBNull(reader["RecommendedComments"]))?string.Empty:(System.String)reader["RecommendedComments"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.IsReQualification)];
					//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireWithLocationApprovalView"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireWithLocationApprovalView"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireWithLocationApprovalView entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CreatedByUserId)];
			//entity.CreatedByUserId = (Convert.IsDBNull(reader["CreatedByUserId"]))?(int)0:(System.Int32)reader["CreatedByUserId"];
			entity.CreatedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.CreatedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CreatedByUser)];
			//entity.CreatedByUser = (Convert.IsDBNull(reader["CreatedByUser"]))?string.Empty:(System.String)reader["CreatedByUser"];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CreatedDate)];
			//entity.CreatedDate = (Convert.IsDBNull(reader["CreatedDate"]))?DateTime.MinValue:(System.DateTime)reader["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.ModifiedByUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedByUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedByUser)];
			//entity.ModifiedByUser = (Convert.IsDBNull(reader["ModifiedByUser"]))?string.Empty:(System.String)reader["ModifiedByUser"];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedDate)];
			//entity.ApprovedDate = (Convert.IsDBNull(reader["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)reader["ApprovedDate"];
			entity.DaysSinceLastActivity = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.DaysSinceLastActivity)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.DaysSinceLastActivity)];
			//entity.DaysSinceLastActivity = (Convert.IsDBNull(reader["DaysSinceLastActivity"]))?(int)0:(System.Int32?)reader["DaysSinceLastActivity"];
			entity.QmaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QmaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QmaLastModified)];
			//entity.QmaLastModified = (Convert.IsDBNull(reader["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QMALastModified"];
			entity.QvaLastModified = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QvaLastModified)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QvaLastModified)];
			//entity.QvaLastModified = (Convert.IsDBNull(reader["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)reader["QVALastModified"];
			entity.AssessedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.AssessedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.AssessedDate)];
			//entity.AssessedDate = (Convert.IsDBNull(reader["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)reader["AssessedDate"];
			entity.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.AssessedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.AssessedByUserId)];
			//entity.AssessedByUserId = (Convert.IsDBNull(reader["AssessedByUserId"]))?(int)0:(System.Int32?)reader["AssessedByUserId"];
			entity.Status = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Status)];
			//entity.Status = (Convert.IsDBNull(reader["Status"]))?(int)0:(System.Int32)reader["Status"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedByUserId)];
			//entity.InitialCreatedByUserId = (Convert.IsDBNull(reader["InitialCreatedByUserId"]))?(int)0:(System.Int32?)reader["InitialCreatedByUserId"];
			entity.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialCreatedDate)];
			//entity.InitialCreatedDate = (Convert.IsDBNull(reader["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialCreatedDate"];
			entity.InitialSubmittedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedByUserId)];
			//entity.InitialSubmittedByUserId = (Convert.IsDBNull(reader["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)reader["InitialSubmittedByUserId"];
			entity.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialSubmittedDate)];
			//entity.InitialSubmittedDate = (Convert.IsDBNull(reader["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialSubmittedDate"];
			entity.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedByUserId)];
			//entity.MainCreatedByUserId = (Convert.IsDBNull(reader["MainCreatedByUserId"]))?(int)0:(System.Int32?)reader["MainCreatedByUserId"];
			entity.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainCreatedDate)];
			//entity.MainCreatedDate = (Convert.IsDBNull(reader["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainCreatedDate"];
			entity.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedByUserId)];
			//entity.VerificationCreatedByUserId = (Convert.IsDBNull(reader["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)reader["VerificationCreatedByUserId"];
			entity.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationCreatedDate)];
			//entity.VerificationCreatedDate = (Convert.IsDBNull(reader["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationCreatedDate"];
			entity.IsMainRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.IsMainRequired)];
			//entity.IsMainRequired = (Convert.IsDBNull(reader["IsMainRequired"]))?false:(System.Boolean)reader["IsMainRequired"];
			entity.IsVerificationRequired = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.IsVerificationRequired)];
			//entity.IsVerificationRequired = (Convert.IsDBNull(reader["IsVerificationRequired"]))?false:(System.Boolean)reader["IsVerificationRequired"];
			entity.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialCategoryHigh)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialCategoryHigh)];
			//entity.InitialCategoryHigh = (Convert.IsDBNull(reader["InitialCategoryHigh"]))?false:(System.Boolean?)reader["InitialCategoryHigh"];
			entity.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedByUserId)];
			//entity.InitialModifiedByUserId = (Convert.IsDBNull(reader["InitialModifiedByUserId"]))?(int)0:(System.Int32?)reader["InitialModifiedByUserId"];
			entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialModifiedDate)];
			//entity.InitialModifiedDate = (Convert.IsDBNull(reader["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["InitialModifiedDate"];
			entity.InitialStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialStatus)];
			//entity.InitialStatus = (Convert.IsDBNull(reader["InitialStatus"]))?(int)0:(System.Int32)reader["InitialStatus"];
			entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.InitialRiskAssessment)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.InitialRiskAssessment)];
			//entity.InitialRiskAssessment = (Convert.IsDBNull(reader["InitialRiskAssessment"]))?string.Empty:(System.String)reader["InitialRiskAssessment"];
			entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedByUserId)];
			//entity.MainModifiedByUserId = (Convert.IsDBNull(reader["MainModifiedByUserId"]))?(int)0:(System.Int32?)reader["MainModifiedByUserId"];
			entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainModifiedDate)];
			//entity.MainModifiedDate = (Convert.IsDBNull(reader["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainModifiedDate"];
			entity.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreExpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreExpectations)];
			//entity.MainScoreExpectations = (Convert.IsDBNull(reader["MainScoreExpectations"]))?(int)0:(System.Int32?)reader["MainScoreExpectations"];
			entity.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreOverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreOverall)];
			//entity.MainScoreOverall = (Convert.IsDBNull(reader["MainScoreOverall"]))?(int)0:(System.Int32?)reader["MainScoreOverall"];
			entity.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreResults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreResults)];
			//entity.MainScoreResults = (Convert.IsDBNull(reader["MainScoreResults"]))?(int)0:(System.Int32?)reader["MainScoreResults"];
			entity.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreStaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreStaffing)];
			//entity.MainScoreStaffing = (Convert.IsDBNull(reader["MainScoreStaffing"]))?(int)0:(System.Int32?)reader["MainScoreStaffing"];
			entity.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreSystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScoreSystems)];
			//entity.MainScoreSystems = (Convert.IsDBNull(reader["MainScoreSystems"]))?(int)0:(System.Int32?)reader["MainScoreSystems"];
			entity.MainStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainStatus)];
			//entity.MainStatus = (Convert.IsDBNull(reader["MainStatus"]))?(int)0:(System.Int32)reader["MainStatus"];
			entity.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentByUserId)];
			//entity.MainAssessmentByUserId = (Convert.IsDBNull(reader["MainAssessmentByUserId"]))?(int)0:(System.Int32?)reader["MainAssessmentByUserId"];
			entity.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentComments)];
			//entity.MainAssessmentComments = (Convert.IsDBNull(reader["MainAssessmentComments"]))?string.Empty:(System.String)reader["MainAssessmentComments"];
			entity.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentDate)];
			//entity.MainAssessmentDate = (Convert.IsDBNull(reader["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentDate"];
			entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentRiskRating)];
			//entity.MainAssessmentRiskRating = (Convert.IsDBNull(reader["MainAssessmentRiskRating"]))?string.Empty:(System.String)reader["MainAssessmentRiskRating"];
			entity.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentStatus)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentStatus)];
			//entity.MainAssessmentStatus = (Convert.IsDBNull(reader["MainAssessmentStatus"]))?string.Empty:(System.String)reader["MainAssessmentStatus"];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentValidTo)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainAssessmentValidTo)];
			//entity.MainAssessmentValidTo = (Convert.IsDBNull(reader["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)reader["MainAssessmentValidTo"];
			entity.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePexpectations)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePexpectations)];
			//entity.MainScorePexpectations = (Convert.IsDBNull(reader["MainScorePExpectations"]))?(int)0:(System.Int32?)reader["MainScorePExpectations"];
			entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePoverall)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePoverall)];
			//entity.MainScorePoverall = (Convert.IsDBNull(reader["MainScorePOverall"]))?(int)0:(System.Int32?)reader["MainScorePOverall"];
			entity.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePresults)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePresults)];
			//entity.MainScorePresults = (Convert.IsDBNull(reader["MainScorePResults"]))?(int)0:(System.Int32?)reader["MainScorePResults"];
			entity.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePstaffing)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePstaffing)];
			//entity.MainScorePstaffing = (Convert.IsDBNull(reader["MainScorePStaffing"]))?(int)0:(System.Int32?)reader["MainScorePStaffing"];
			entity.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePsystems)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.MainScorePsystems)];
			//entity.MainScorePsystems = (Convert.IsDBNull(reader["MainScorePSystems"]))?(int)0:(System.Int32?)reader["MainScorePSystems"];
			entity.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedByUserId)];
			//entity.VerificationModifiedByUserId = (Convert.IsDBNull(reader["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)reader["VerificationModifiedByUserId"];
			entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedDate)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationModifiedDate)];
			//entity.VerificationModifiedDate = (Convert.IsDBNull(reader["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)reader["VerificationModifiedDate"];
			entity.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.VerificationRiskRating)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationRiskRating)];
			//entity.VerificationRiskRating = (Convert.IsDBNull(reader["VerificationRiskRating"]))?string.Empty:(System.String)reader["VerificationRiskRating"];
			entity.VerificationStatus = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.VerificationStatus)];
			//entity.VerificationStatus = (Convert.IsDBNull(reader["VerificationStatus"]))?(int)0:(System.Int32)reader["VerificationStatus"];
			entity.SupplierContactFirstName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactFirstName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactFirstName)];
			//entity.SupplierContactFirstName = (Convert.IsDBNull(reader["SupplierContactFirstName"]))?string.Empty:(System.String)reader["SupplierContactFirstName"];
			entity.SupplierContactLastName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactLastName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactLastName)];
			//entity.SupplierContactLastName = (Convert.IsDBNull(reader["SupplierContactLastName"]))?string.Empty:(System.String)reader["SupplierContactLastName"];
			entity.SupplierContactEmail = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactEmail)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactEmail)];
			//entity.SupplierContactEmail = (Convert.IsDBNull(reader["SupplierContactEmail"]))?string.Empty:(System.String)reader["SupplierContactEmail"];
			entity.SupplierContactTitle = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactTitle)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactTitle)];
			//entity.SupplierContactTitle = (Convert.IsDBNull(reader["SupplierContactTitle"]))?string.Empty:(System.String)reader["SupplierContactTitle"];
			entity.SupplierContactPhone = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactPhone)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactPhone)];
			//entity.SupplierContactPhone = (Convert.IsDBNull(reader["SupplierContactPhone"]))?string.Empty:(System.String)reader["SupplierContactPhone"];
			entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUserId)];
			//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?(int)0:(System.Int32?)reader["ProcurementContactUserId"];
			entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUserId)];
			//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?(int)0:(System.Int32?)reader["ContractManagerUserId"];
			entity.Kwi = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Kwi)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Kwi)];
			//entity.Kwi = (Convert.IsDBNull(reader["KWI"]))?string.Empty:(System.String)reader["KWI"];
			entity.Pin = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Pin)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Pin)];
			//entity.Pin = (Convert.IsDBNull(reader["PIN"]))?string.Empty:(System.String)reader["PIN"];
			entity.Wgp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Wgp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Wgp)];
			//entity.Wgp = (Convert.IsDBNull(reader["WGP"]))?string.Empty:(System.String)reader["WGP"];
			entity.Hun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Hun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Hun)];
			//entity.Hun = (Convert.IsDBNull(reader["HUN"]))?string.Empty:(System.String)reader["HUN"];
			entity.Wdl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Wdl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Wdl)];
			//entity.Wdl = (Convert.IsDBNull(reader["WDL"]))?string.Empty:(System.String)reader["WDL"];
			entity.Bun = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Bun)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Bun)];
			//entity.Bun = (Convert.IsDBNull(reader["BUN"]))?string.Empty:(System.String)reader["BUN"];
			entity.Fml = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Fml)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Fml)];
			//entity.Fml = (Convert.IsDBNull(reader["FML"]))?string.Empty:(System.String)reader["FML"];
			entity.Bgn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Bgn)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Bgn)];
			//entity.Bgn = (Convert.IsDBNull(reader["BGN"]))?string.Empty:(System.String)reader["BGN"];
			entity.Ang = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Ang)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Ang)];
			//entity.Ang = (Convert.IsDBNull(reader["ANG"]))?string.Empty:(System.String)reader["ANG"];
			entity.Ptl = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Ptl)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Ptl)];
			//entity.Ptl = (Convert.IsDBNull(reader["PTL"]))?string.Empty:(System.String)reader["PTL"];
			entity.Pth = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Pth)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Pth)];
			//entity.Pth = (Convert.IsDBNull(reader["PTH"]))?string.Empty:(System.String)reader["PTH"];
			entity.Pel = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Pel)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Pel)];
			//entity.Pel = (Convert.IsDBNull(reader["PEL"]))?string.Empty:(System.String)reader["PEL"];
			entity.Arp = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Arp)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Arp)];
			//entity.Arp = (Convert.IsDBNull(reader["ARP"]))?string.Empty:(System.String)reader["ARP"];
			entity.Yen = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Yen)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Yen)];
			//entity.Yen = (Convert.IsDBNull(reader["YEN"]))?string.Empty:(System.String)reader["YEN"];
			entity.ProcurementModified = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementModified)];
			//entity.ProcurementModified = (Convert.IsDBNull(reader["ProcurementModified"]))?false:(System.Boolean)reader["ProcurementModified"];
			entity.CompanyName = (System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.SafetyAssessorUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessorUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SafetyAssessorUserId)];
			//entity.SafetyAssessorUserId = (Convert.IsDBNull(reader["SafetyAssessorUserId"]))?(int)0:(System.Int32?)reader["SafetyAssessorUserId"];
			entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SubContractor)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SubContractor)];
			//entity.SubContractor = (Convert.IsDBNull(reader["SubContractor"]))?false:(System.Boolean?)reader["SubContractor"];
			entity.CompanyStatusId = (System.Int32)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyStatusId)];
			//entity.CompanyStatusId = (Convert.IsDBNull(reader["CompanyStatusId"]))?(int)0:(System.Int32)reader["CompanyStatusId"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.Deactivated = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Deactivated)))?null:(System.Boolean?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Deactivated)];
			//entity.Deactivated = (Convert.IsDBNull(reader["Deactivated"]))?false:(System.Boolean?)reader["Deactivated"];
			entity.ProcurementContactUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcurementContactUser)];
			//entity.ProcurementContactUser = (Convert.IsDBNull(reader["ProcurementContactUser"]))?string.Empty:(System.String)reader["ProcurementContactUser"];
			entity.ContractManagerUser = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUser)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ContractManagerUser)];
			//entity.ContractManagerUser = (Convert.IsDBNull(reader["ContractManagerUser"]))?string.Empty:(System.String)reader["ContractManagerUser"];
			entity.NoSqExpiryEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.NoSqExpiryEmailsSent)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.NoSqExpiryEmailsSent)];
			//entity.NoSqExpiryEmailsSent = (Convert.IsDBNull(reader["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)reader["NoSqExpiryEmailsSent"];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithUserId)];
			//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ProcessNo)];
			//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
			entity.UserName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.ActionName = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ActionName)];
			//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.QuestionnairePresentlyWithSinceDaysCount)];
			//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
			entity.Expr1 = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.Expr1)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.Expr1)];
			//entity.Expr1 = (Convert.IsDBNull(reader["Expr1"]))?(int)0:(System.Int32?)reader["Expr1"];
			entity.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.NoReminderEmailsSent)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.NoReminderEmailsSent)];
			//entity.NoReminderEmailsSent = (Convert.IsDBNull(reader["NoReminderEmailsSent"]))?(int)0:(System.Int32?)reader["NoReminderEmailsSent"];
			entity.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.LastReminderEmailSentOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.LastReminderEmailSentOn)];
			//entity.LastReminderEmailSentOn = (Convert.IsDBNull(reader["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)reader["LastReminderEmailSentOn"];
			entity.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactVerifiedOn)))?null:(System.DateTime?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.SupplierContactVerifiedOn)];
			//entity.SupplierContactVerifiedOn = (Convert.IsDBNull(reader["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)reader["SupplierContactVerifiedOn"];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.LevelOfSupervision)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.LevelOfSupervision)];
			//entity.LevelOfSupervision = (Convert.IsDBNull(reader["LevelOfSupervision"]))?string.Empty:(System.String)reader["LevelOfSupervision"];
			entity.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.RecommendedComments)))?null:(System.String)reader[((int)QuestionnaireWithLocationApprovalViewColumn.RecommendedComments)];
			//entity.RecommendedComments = (Convert.IsDBNull(reader["RecommendedComments"]))?string.Empty:(System.String)reader["RecommendedComments"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireWithLocationApprovalViewColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireWithLocationApprovalViewColumn.IsReQualification)];
			//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireWithLocationApprovalView"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireWithLocationApprovalView"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireWithLocationApprovalView entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.CreatedByUserId = (Convert.IsDBNull(dataRow["CreatedByUserId"]))?(int)0:(System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedByUser = (Convert.IsDBNull(dataRow["CreatedByUser"]))?string.Empty:(System.String)dataRow["CreatedByUser"];
			entity.CreatedDate = (Convert.IsDBNull(dataRow["CreatedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedByUser = (Convert.IsDBNull(dataRow["ModifiedByUser"]))?string.Empty:(System.String)dataRow["ModifiedByUser"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.ApprovedDate = (Convert.IsDBNull(dataRow["ApprovedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["ApprovedDate"];
			entity.DaysSinceLastActivity = (Convert.IsDBNull(dataRow["DaysSinceLastActivity"]))?(int)0:(System.Int32?)dataRow["DaysSinceLastActivity"];
			entity.QmaLastModified = (Convert.IsDBNull(dataRow["QMALastModified"]))?DateTime.MinValue:(System.DateTime?)dataRow["QMALastModified"];
			entity.QvaLastModified = (Convert.IsDBNull(dataRow["QVALastModified"]))?DateTime.MinValue:(System.DateTime?)dataRow["QVALastModified"];
			entity.AssessedDate = (Convert.IsDBNull(dataRow["AssessedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["AssessedDate"];
			entity.AssessedByUserId = (Convert.IsDBNull(dataRow["AssessedByUserId"]))?(int)0:(System.Int32?)dataRow["AssessedByUserId"];
			entity.Status = (Convert.IsDBNull(dataRow["Status"]))?(int)0:(System.Int32)dataRow["Status"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.InitialCreatedByUserId = (Convert.IsDBNull(dataRow["InitialCreatedByUserId"]))?(int)0:(System.Int32?)dataRow["InitialCreatedByUserId"];
			entity.InitialCreatedDate = (Convert.IsDBNull(dataRow["InitialCreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialCreatedDate"];
			entity.InitialSubmittedByUserId = (Convert.IsDBNull(dataRow["InitialSubmittedByUserId"]))?(int)0:(System.Int32?)dataRow["InitialSubmittedByUserId"];
			entity.InitialSubmittedDate = (Convert.IsDBNull(dataRow["InitialSubmittedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialSubmittedDate"];
			entity.MainCreatedByUserId = (Convert.IsDBNull(dataRow["MainCreatedByUserId"]))?(int)0:(System.Int32?)dataRow["MainCreatedByUserId"];
			entity.MainCreatedDate = (Convert.IsDBNull(dataRow["MainCreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainCreatedDate"];
			entity.VerificationCreatedByUserId = (Convert.IsDBNull(dataRow["VerificationCreatedByUserId"]))?(int)0:(System.Int32?)dataRow["VerificationCreatedByUserId"];
			entity.VerificationCreatedDate = (Convert.IsDBNull(dataRow["VerificationCreatedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["VerificationCreatedDate"];
			entity.IsMainRequired = (Convert.IsDBNull(dataRow["IsMainRequired"]))?false:(System.Boolean)dataRow["IsMainRequired"];
			entity.IsVerificationRequired = (Convert.IsDBNull(dataRow["IsVerificationRequired"]))?false:(System.Boolean)dataRow["IsVerificationRequired"];
			entity.InitialCategoryHigh = (Convert.IsDBNull(dataRow["InitialCategoryHigh"]))?false:(System.Boolean?)dataRow["InitialCategoryHigh"];
			entity.InitialModifiedByUserId = (Convert.IsDBNull(dataRow["InitialModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["InitialModifiedByUserId"];
			entity.InitialModifiedDate = (Convert.IsDBNull(dataRow["InitialModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["InitialModifiedDate"];
			entity.InitialStatus = (Convert.IsDBNull(dataRow["InitialStatus"]))?(int)0:(System.Int32)dataRow["InitialStatus"];
			entity.InitialRiskAssessment = (Convert.IsDBNull(dataRow["InitialRiskAssessment"]))?string.Empty:(System.String)dataRow["InitialRiskAssessment"];
			entity.MainModifiedByUserId = (Convert.IsDBNull(dataRow["MainModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["MainModifiedByUserId"];
			entity.MainModifiedDate = (Convert.IsDBNull(dataRow["MainModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainModifiedDate"];
			entity.MainScoreExpectations = (Convert.IsDBNull(dataRow["MainScoreExpectations"]))?(int)0:(System.Int32?)dataRow["MainScoreExpectations"];
			entity.MainScoreOverall = (Convert.IsDBNull(dataRow["MainScoreOverall"]))?(int)0:(System.Int32?)dataRow["MainScoreOverall"];
			entity.MainScoreResults = (Convert.IsDBNull(dataRow["MainScoreResults"]))?(int)0:(System.Int32?)dataRow["MainScoreResults"];
			entity.MainScoreStaffing = (Convert.IsDBNull(dataRow["MainScoreStaffing"]))?(int)0:(System.Int32?)dataRow["MainScoreStaffing"];
			entity.MainScoreSystems = (Convert.IsDBNull(dataRow["MainScoreSystems"]))?(int)0:(System.Int32?)dataRow["MainScoreSystems"];
			entity.MainStatus = (Convert.IsDBNull(dataRow["MainStatus"]))?(int)0:(System.Int32)dataRow["MainStatus"];
			entity.MainAssessmentByUserId = (Convert.IsDBNull(dataRow["MainAssessmentByUserId"]))?(int)0:(System.Int32?)dataRow["MainAssessmentByUserId"];
			entity.MainAssessmentComments = (Convert.IsDBNull(dataRow["MainAssessmentComments"]))?string.Empty:(System.String)dataRow["MainAssessmentComments"];
			entity.MainAssessmentDate = (Convert.IsDBNull(dataRow["MainAssessmentDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentDate"];
			entity.MainAssessmentRiskRating = (Convert.IsDBNull(dataRow["MainAssessmentRiskRating"]))?string.Empty:(System.String)dataRow["MainAssessmentRiskRating"];
			entity.MainAssessmentStatus = (Convert.IsDBNull(dataRow["MainAssessmentStatus"]))?string.Empty:(System.String)dataRow["MainAssessmentStatus"];
			entity.MainAssessmentValidTo = (Convert.IsDBNull(dataRow["MainAssessmentValidTo"]))?DateTime.MinValue:(System.DateTime?)dataRow["MainAssessmentValidTo"];
			entity.MainScorePexpectations = (Convert.IsDBNull(dataRow["MainScorePExpectations"]))?(int)0:(System.Int32?)dataRow["MainScorePExpectations"];
			entity.MainScorePoverall = (Convert.IsDBNull(dataRow["MainScorePOverall"]))?(int)0:(System.Int32?)dataRow["MainScorePOverall"];
			entity.MainScorePresults = (Convert.IsDBNull(dataRow["MainScorePResults"]))?(int)0:(System.Int32?)dataRow["MainScorePResults"];
			entity.MainScorePstaffing = (Convert.IsDBNull(dataRow["MainScorePStaffing"]))?(int)0:(System.Int32?)dataRow["MainScorePStaffing"];
			entity.MainScorePsystems = (Convert.IsDBNull(dataRow["MainScorePSystems"]))?(int)0:(System.Int32?)dataRow["MainScorePSystems"];
			entity.VerificationModifiedByUserId = (Convert.IsDBNull(dataRow["VerificationModifiedByUserId"]))?(int)0:(System.Int32?)dataRow["VerificationModifiedByUserId"];
			entity.VerificationModifiedDate = (Convert.IsDBNull(dataRow["VerificationModifiedDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["VerificationModifiedDate"];
			entity.VerificationRiskRating = (Convert.IsDBNull(dataRow["VerificationRiskRating"]))?string.Empty:(System.String)dataRow["VerificationRiskRating"];
			entity.VerificationStatus = (Convert.IsDBNull(dataRow["VerificationStatus"]))?(int)0:(System.Int32)dataRow["VerificationStatus"];
			entity.SupplierContactFirstName = (Convert.IsDBNull(dataRow["SupplierContactFirstName"]))?string.Empty:(System.String)dataRow["SupplierContactFirstName"];
			entity.SupplierContactLastName = (Convert.IsDBNull(dataRow["SupplierContactLastName"]))?string.Empty:(System.String)dataRow["SupplierContactLastName"];
			entity.SupplierContactEmail = (Convert.IsDBNull(dataRow["SupplierContactEmail"]))?string.Empty:(System.String)dataRow["SupplierContactEmail"];
			entity.SupplierContactTitle = (Convert.IsDBNull(dataRow["SupplierContactTitle"]))?string.Empty:(System.String)dataRow["SupplierContactTitle"];
			entity.SupplierContactPhone = (Convert.IsDBNull(dataRow["SupplierContactPhone"]))?string.Empty:(System.String)dataRow["SupplierContactPhone"];
			entity.ProcurementContactUserId = (Convert.IsDBNull(dataRow["ProcurementContactUserId"]))?(int)0:(System.Int32?)dataRow["ProcurementContactUserId"];
			entity.ContractManagerUserId = (Convert.IsDBNull(dataRow["ContractManagerUserId"]))?(int)0:(System.Int32?)dataRow["ContractManagerUserId"];
			entity.Kwi = (Convert.IsDBNull(dataRow["KWI"]))?string.Empty:(System.String)dataRow["KWI"];
			entity.Pin = (Convert.IsDBNull(dataRow["PIN"]))?string.Empty:(System.String)dataRow["PIN"];
			entity.Wgp = (Convert.IsDBNull(dataRow["WGP"]))?string.Empty:(System.String)dataRow["WGP"];
			entity.Hun = (Convert.IsDBNull(dataRow["HUN"]))?string.Empty:(System.String)dataRow["HUN"];
			entity.Wdl = (Convert.IsDBNull(dataRow["WDL"]))?string.Empty:(System.String)dataRow["WDL"];
			entity.Bun = (Convert.IsDBNull(dataRow["BUN"]))?string.Empty:(System.String)dataRow["BUN"];
			entity.Fml = (Convert.IsDBNull(dataRow["FML"]))?string.Empty:(System.String)dataRow["FML"];
			entity.Bgn = (Convert.IsDBNull(dataRow["BGN"]))?string.Empty:(System.String)dataRow["BGN"];
			entity.Ang = (Convert.IsDBNull(dataRow["ANG"]))?string.Empty:(System.String)dataRow["ANG"];
			entity.Ptl = (Convert.IsDBNull(dataRow["PTL"]))?string.Empty:(System.String)dataRow["PTL"];
			entity.Pth = (Convert.IsDBNull(dataRow["PTH"]))?string.Empty:(System.String)dataRow["PTH"];
			entity.Pel = (Convert.IsDBNull(dataRow["PEL"]))?string.Empty:(System.String)dataRow["PEL"];
			entity.Arp = (Convert.IsDBNull(dataRow["ARP"]))?string.Empty:(System.String)dataRow["ARP"];
			entity.Yen = (Convert.IsDBNull(dataRow["YEN"]))?string.Empty:(System.String)dataRow["YEN"];
			entity.ProcurementModified = (Convert.IsDBNull(dataRow["ProcurementModified"]))?false:(System.Boolean)dataRow["ProcurementModified"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.SafetyAssessorUserId = (Convert.IsDBNull(dataRow["SafetyAssessorUserId"]))?(int)0:(System.Int32?)dataRow["SafetyAssessorUserId"];
			entity.SubContractor = (Convert.IsDBNull(dataRow["SubContractor"]))?false:(System.Boolean?)dataRow["SubContractor"];
			entity.CompanyStatusId = (Convert.IsDBNull(dataRow["CompanyStatusId"]))?(int)0:(System.Int32)dataRow["CompanyStatusId"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.Deactivated = (Convert.IsDBNull(dataRow["Deactivated"]))?false:(System.Boolean?)dataRow["Deactivated"];
			entity.ProcurementContactUser = (Convert.IsDBNull(dataRow["ProcurementContactUser"]))?string.Empty:(System.String)dataRow["ProcurementContactUser"];
			entity.ContractManagerUser = (Convert.IsDBNull(dataRow["ContractManagerUser"]))?string.Empty:(System.String)dataRow["ContractManagerUser"];
			entity.NoSqExpiryEmailsSent = (Convert.IsDBNull(dataRow["NoSqExpiryEmailsSent"]))?string.Empty:(System.String)dataRow["NoSqExpiryEmailsSent"];
			entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (Convert.IsDBNull(dataRow["ProcessNo"]))?(int)0:(System.Int32?)dataRow["ProcessNo"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.ActionName = (Convert.IsDBNull(dataRow["ActionName"]))?string.Empty:(System.String)dataRow["ActionName"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithSinceDaysCount"];
			entity.Expr1 = (Convert.IsDBNull(dataRow["Expr1"]))?(int)0:(System.Int32?)dataRow["Expr1"];
			entity.NoReminderEmailsSent = (Convert.IsDBNull(dataRow["NoReminderEmailsSent"]))?(int)0:(System.Int32?)dataRow["NoReminderEmailsSent"];
			entity.LastReminderEmailSentOn = (Convert.IsDBNull(dataRow["LastReminderEmailSentOn"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastReminderEmailSentOn"];
			entity.SupplierContactVerifiedOn = (Convert.IsDBNull(dataRow["SupplierContactVerifiedOn"]))?DateTime.MinValue:(System.DateTime?)dataRow["SupplierContactVerifiedOn"];
			entity.LevelOfSupervision = (Convert.IsDBNull(dataRow["LevelOfSupervision"]))?string.Empty:(System.String)dataRow["LevelOfSupervision"];
			entity.RecommendedComments = (Convert.IsDBNull(dataRow["RecommendedComments"]))?string.Empty:(System.String)dataRow["RecommendedComments"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.IsReQualification = (Convert.IsDBNull(dataRow["IsReQualification"]))?false:(System.Boolean)dataRow["IsReQualification"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireWithLocationApprovalViewFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewFilterBuilder : SqlFilterBuilder<QuestionnaireWithLocationApprovalViewColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewFilterBuilder class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewFilterBuilder

	#region QuestionnaireWithLocationApprovalViewParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireWithLocationApprovalViewColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewParameterBuilder class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireWithLocationApprovalViewParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireWithLocationApprovalViewParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireWithLocationApprovalViewParameterBuilder
	
	#region QuestionnaireWithLocationApprovalViewSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireWithLocationApprovalViewSortBuilder : SqlSortBuilder<QuestionnaireWithLocationApprovalViewColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewSqlSortBuilder class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireWithLocationApprovalViewSortBuilder

} // end namespace
