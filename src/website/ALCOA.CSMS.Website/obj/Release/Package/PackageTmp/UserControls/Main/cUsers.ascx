﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_cUsers" Codebehind="cUsers.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 43px">
        <span class="bodycopy"><span class="title">Who Has Access</span><br><span class=date>
            <dxe:ASPxLabel ID="ASPxCompany" runat="server" Text="">
            </dxe:ASPxLabel></span>
            <br />
            <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
                </td>
    </tr>
    <tr>
        <td colspan="3" style="height: 17px"><dxe:ASPxLabel ID="ASPxlblInfo1" runat="server" Text="The following is a list of users currently granted access to this system for your Company.">
        </dxe:ASPxLabel>
            <br />
            Please Contact the
            <dxe:ASPxHyperLink ID="ASPxHyperLinkContact" runat="server" Text="AUA Alcoa Contractor Services">
            </dxe:ASPxHyperLink>
            team if this is incorrect.
            <br />
            You can also <a href="javascript:popUp('PopUps/RequestAccess.aspx');">request access</a> for someone in your company by clicking <a href="javascript:popUp('PopUps/RequestAccess.aspx');">here</a>.
            </td>
    </tr>
        <tr>
            <td colspan="3" style="height: 74px">
                <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                    KeyFieldName="UserId" Width="900px"
                    OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                    >
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                        </LoadingPanel>
                    </Images>
                    <Settings ShowGroupPanel="True" ShowFilterRow="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="235"/>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue">
                        <LoadingPanel ImageSpacing="10px">
                        </LoadingPanel>
                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                        </Header>
                    </Styles>
                    <Columns>
                        <dxwgv:GridViewDataTextColumn FieldName="UserId" ReadOnly="True" Visible="False"
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" SortIndex="0"
                            SortOrder="Ascending" UnboundType="String" VisibleIndex="0">
                            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="CompaniesDataSource" DropDownHeight="150px" TextField="CompanyName"
                                ValueField="CompanyId" ValueType="System.Int32">
                            </PropertiesComboBox>
                            <Settings SortMode="DisplayText" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="UserLogon" 
                            VisibleIndex="1" Visible="False">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="LastName" VisibleIndex="1" SortIndex="1" SortOrder="Ascending">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="2" SortIndex="2" SortOrder="Ascending">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Role" FieldName="RoleId" UnboundType="String"
                            VisibleIndex="3" Width="85px">
                            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="RoleDataSource" DropDownHeight="150px" TextField="Role"
                                ValueField="RoleId" ValueType="System.String">
                            </PropertiesComboBox>
                            <Settings SortMode="DisplayText" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Title" VisibleIndex="4">
                            <CellStyle Wrap="True">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Email" VisibleIndex="5">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="PhoneNo" VisibleIndex="6" Width="65px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="MobNo" VisibleIndex="7" Width="90px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" Visible="False" VisibleIndex="11">
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <SettingsBehavior ColumnResizeMode="Control" />
                    <SettingsPager>
                        <AllButton Visible="True">
                        </AllButton>
                    </SettingsPager>
                    <StylesEditors>
                        <ProgressBar Height="25px">
                        </ProgressBar>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
            </td>
        </tr>
    <%--<tr>
        <td colspan="3" style="height: 35px; text-align: right">
         Number of Rows to show per page:<asp:DropDownList ID="DropDownList1" runat="server"
                AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>--%>
    <tr align="right">
        <td colspan="3" style="height: 35px; text-align: right; text-align:-moz-right; " align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
            
                <data:CompaniesDataSource ID="CompaniesDataSource" runat="server" EnableDeepLoad="False"
                    SelectMethod="GetPaged" Sort="CompanyName ASC">
                </data:CompaniesDataSource>
                
                <asp:SqlDataSource ID="sqldsUsers" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
                    SelectCommand="SELECT UserId, CompanyId As [CompanyId], UserLogon, LastName, FirstName, RoleId As [RoleId], Title, Email, PhoneNo, MobNo, ModifiedByUserId FROM Users WHERE (UserId NOT LIKE 0) and (RoleId NOT LIKE 4)">
                </asp:SqlDataSource>
                
                <data:UsersDataSource ID="UsersDataSource" runat="server" EnableDeepLoad="false"
                    SelectMethod="GetPaged" Sort="UserLogon ASC" Filter="UserId NOT LIKE 0">
                </data:UsersDataSource>
                <data:RoleDataSource ID="RoleDataSource" runat="server" EnableDeepLoad="False" EnablePaging="True"
                    SelectMethod="GetPaged" Sort="RoleName ASC" Filter="RoleId NOT LIKE 4">
                </data:RoleDataSource>
                
                