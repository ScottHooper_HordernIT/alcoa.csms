﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CurrentQuestionnaireProcurementFunctionalManagerProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CurrentQuestionnaireProcurementFunctionalManagerDataSourceDesigner))]
	public class CurrentQuestionnaireProcurementFunctionalManagerDataSource : ReadOnlyDataSource<CurrentQuestionnaireProcurementFunctionalManager>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerDataSource class.
		/// </summary>
		public CurrentQuestionnaireProcurementFunctionalManagerDataSource() : base(new CurrentQuestionnaireProcurementFunctionalManagerService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CurrentQuestionnaireProcurementFunctionalManagerDataSourceView used by the CurrentQuestionnaireProcurementFunctionalManagerDataSource.
		/// </summary>
		protected CurrentQuestionnaireProcurementFunctionalManagerDataSourceView CurrentQuestionnaireProcurementFunctionalManagerView
		{
			get { return ( View as CurrentQuestionnaireProcurementFunctionalManagerDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CurrentQuestionnaireProcurementFunctionalManagerDataSourceView class that is to be
		/// used by the CurrentQuestionnaireProcurementFunctionalManagerDataSource.
		/// </summary>
		/// <returns>An instance of the CurrentQuestionnaireProcurementFunctionalManagerDataSourceView class.</returns>
		protected override BaseDataSourceView<CurrentQuestionnaireProcurementFunctionalManager, Object> GetNewDataSourceView()
		{
			return new CurrentQuestionnaireProcurementFunctionalManagerDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CurrentQuestionnaireProcurementFunctionalManagerDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CurrentQuestionnaireProcurementFunctionalManagerDataSourceView : ReadOnlyDataSourceView<CurrentQuestionnaireProcurementFunctionalManager>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentQuestionnaireProcurementFunctionalManagerDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CurrentQuestionnaireProcurementFunctionalManagerDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CurrentQuestionnaireProcurementFunctionalManagerDataSourceView(CurrentQuestionnaireProcurementFunctionalManagerDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CurrentQuestionnaireProcurementFunctionalManagerDataSource CurrentQuestionnaireProcurementFunctionalManagerOwner
		{
			get { return Owner as CurrentQuestionnaireProcurementFunctionalManagerDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CurrentQuestionnaireProcurementFunctionalManagerService CurrentQuestionnaireProcurementFunctionalManagerProvider
		{
			get { return Provider as CurrentQuestionnaireProcurementFunctionalManagerService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CurrentQuestionnaireProcurementFunctionalManagerDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CurrentQuestionnaireProcurementFunctionalManagerDataSource class.
	/// </summary>
	public class CurrentQuestionnaireProcurementFunctionalManagerDataSourceDesigner : ReadOnlyDataSourceDesigner<CurrentQuestionnaireProcurementFunctionalManager>
	{
	}

	#endregion CurrentQuestionnaireProcurementFunctionalManagerDataSourceDesigner

	#region CurrentQuestionnaireProcurementFunctionalManagerFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementFunctionalManagerFilter : SqlFilter<CurrentQuestionnaireProcurementFunctionalManagerColumn>
	{
	}

	#endregion CurrentQuestionnaireProcurementFunctionalManagerFilter

	#region CurrentQuestionnaireProcurementFunctionalManagerExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentQuestionnaireProcurementFunctionalManagerExpressionBuilder : SqlExpressionBuilder<CurrentQuestionnaireProcurementFunctionalManagerColumn>
	{
	}
	
	#endregion CurrentQuestionnaireProcurementFunctionalManagerExpressionBuilder		
}

