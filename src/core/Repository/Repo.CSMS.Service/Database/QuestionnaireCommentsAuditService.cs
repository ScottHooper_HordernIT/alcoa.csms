﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireCommentsAuditService
    {
        QuestionnaireCommentsAudit Get(Expression<Func<QuestionnaireCommentsAudit, bool>> where, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes);
        List<QuestionnaireCommentsAudit> GetMany(int? top, Expression<Func<QuestionnaireCommentsAudit, bool>> where, Func<IQueryable<QuestionnaireCommentsAudit>, IOrderedQueryable<QuestionnaireCommentsAudit>> orderBy, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes);
        List<QuestionnaireCommentsAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireCommentsAudit, bool>> where, Func<IQueryable<QuestionnaireCommentsAudit>, IOrderedQueryable<QuestionnaireCommentsAudit>> orderBy, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireCommentsAudit, bool>> where, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes);
        void Insert(QuestionnaireCommentsAudit item, bool saveChanges = true);
        void Update(QuestionnaireCommentsAudit item, bool saveChanges = true);
        void Delete(QuestionnaireCommentsAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireCommentsAuditService : IQuestionnaireCommentsAuditService
    {
        private readonly IRepository<QuestionnaireCommentsAudit> _repository;

        public QuestionnaireCommentsAuditService(IRepository<QuestionnaireCommentsAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireCommentsAudit Get(Expression<Func<QuestionnaireCommentsAudit, bool>> where, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireCommentsAudit> GetMany(int? top, Expression<Func<QuestionnaireCommentsAudit, bool>> where, Func<IQueryable<QuestionnaireCommentsAudit>, IOrderedQueryable<QuestionnaireCommentsAudit>> orderBy, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireCommentsAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireCommentsAudit, bool>> where, Func<IQueryable<QuestionnaireCommentsAudit>, IOrderedQueryable<QuestionnaireCommentsAudit>> orderBy, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireCommentsAudit, bool>> where, List<Expression<Func<QuestionnaireCommentsAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireCommentsAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireCommentsAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireCommentsAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireCommentsAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireCommentsAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireCommentsAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
