﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_CreateYourOwnReports" Codebehind="CreateYourOwnReports.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dxm" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc3" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxObjectContainer" TagPrefix="dxoc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="pageName" style="height: 44px" width="700px">
            <span class="bodycopy"><span class="title">Create Your Own Report</span><br />
                <span class="date">Ad-hoc KPI Reporting</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
        <td class="pageName" colspan="6" style="width: 199px; height: 44px">
            <p align="right" style="text-align: right">
                <strong>Current Report:<br />
                </strong>
                <dxe:ASPxLabel ID="lblLayout" runat="server" Width="100%" Font-Bold="False" Font-Italic="False"
                    Text="Default">
                </dxe:ASPxLabel>
            </p>
        </td>
    </tr>
    <tr>
        <td style="width: 346px;">
            <asp:DropDownList ID="ddlCompaniesMain" runat="server" AutoPostBack="True" DataSourceID="sqldsCompaniesList"
                DataTextField="CompanyName" DataValueField="CompanyId" OnSelectedIndexChanged="ddlCompaniesMain_SelectedIndexChanged"
                Width="350px">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;&nbsp;&nbsp;
        </td>
        <td colspan="5" style="text-align: right">
            <dxe:ASPxButton ID="btn1" runat="server" AutoPostBack="False" Text="Report Manager"
                UseSubmitBehavior="False" Width="120px">
            </dxe:ASPxButton>
        </td>
        <td align="right" class="pageName" style="width: 100px; height: 35px;">
            <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: none">
                Customize</span></a>
        </td>
    </tr>
    <tr>
        <td colspan="8" style="height: 255px">
            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                DataSourceID="sqldsCustomReport" KeyFieldName="KpiId" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                Width="100%">
                <Columns>
                    <dxwgv:GridViewCommandColumn Name="commandCol" Caption="Action" ShowInCustomizationForm="False"
                        VisibleIndex="0" Visible="False">
                        <DeleteButton Text="Delete" Visible="True">
                            <Image AlternateText="Delete" Url="~/Images/gridDelete.gif" />
                        </DeleteButton>
                        <EditButton Text="Edit" Visible="True">
                            <Image AlternateText="Edit" Url="~/Images/gridEdit.gif" />
                        </EditButton>
                        <NewButton Text="New">
                            <Image AlternateText="New" Url="~/Images/gridNew.gif" />
                        </NewButton>
                        <UpdateButton Text="Save" Visible="True">
                            <Image AlternateText="Save" Url="../Images/gridSave.gif" />
                        </UpdateButton>
                        <CancelButton Text="Cancel" Visible="True">
                            <Image AlternateText="Cancel" Url="../Images/gridCancel.gif" />
                        </CancelButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataTextColumn Caption="KPI #" FieldName="KpiId" ReadOnly="True" Visible="False"
                        VisibleIndex="0">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn Name="CompanyBox" Caption="Company" FieldName="CompanyId"
                        ReadOnly="True" SortOrder="Ascending" SortIndex="0" VisibleIndex="0" 
                        FixedStyle="Left" Width="200px">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                            ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" 
                        Caption="Residential Category" FieldName="CompanySiteCategoryId"
                        SortIndex="1" SortOrder="Ascending" UnboundType="String" VisibleIndex="1" 
                        FixedStyle="Left" Width="150px">
                        <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px" TextField="CategoryDesc"
                            ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Name="gcbSite" Caption="Site" FieldName="SiteId"
                        SortIndex="1" SortOrder="Ascending" UnboundType="String" VisibleIndex="1" 
                        FixedStyle="Left" Width="150px">
                        <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Month Commencing" FieldName="kpiDateTime"
                        SortIndex="2" SortOrder="Descending" VisibleIndex="2" FixedStyle="Left">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                            <CalendarProperties ShowTodayButton="False" ShowWeekNumbers="False">
                            </CalendarProperties>
                        </PropertiesDateEdit>
                        <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" GroupInterval="DateMonth" />
                        <EditFormSettings Visible="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Safety Plan Submitted?" FieldName="SafetyPlanSubmitted"
                        Visible="False" VisibleIndex="999">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Date Added" FieldName="DateAdded" ReadOnly="True"
                        Visible="False" VisibleIndex="5">
                        <EditFormSettings Visible="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataComboBoxColumn Name="gcbCreatedBy" Caption="Created By" FieldName="CreatedbyUserId"
                        UnboundType="String" Visible="False" VisibleIndex="6">
                        <PropertiesComboBox DataSourceID="sqldsUserFullName" DropDownHeight="150px" TextField="FullName"
                            ValueField="UserId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Date Modified" FieldName="Datemodified" ReadOnly="True"
                        Visible="False" VisibleIndex="7" Settings-SortMode="Value">
                        <Settings SortMode="Value"></Settings>
                        <EditFormSettings Visible="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataComboBoxColumn Name="gcbModifiedBy" Caption="Modified By" FieldName="ModifiedbyUserId"
                        UnboundType="String" Visible="False" VisibleIndex="8">
                        <PropertiesComboBox DataSourceID="sqldsUserFullName" DropDownHeight="150px" TextField="FullName"
                            ValueField="UserId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="General Hours" FieldName="kpiGeneral" VisibleIndex="4"
                        Visible="False">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit DisplayFormatString="n">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Calciner Expense Hours" FieldName="kpiCalcinerExpense"
                        VisibleIndex="5" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Calciner Capital Hours" FieldName="kpiCalcinerCapital"
                        VisibleIndex="6" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Residue Hours (Other)" FieldName="kpiResidue"
                        VisibleIndex="7" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #1 - Title" FieldName="projectCapital1Title"
                        VisibleIndex="8" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #1 - Hours" FieldName="projectCapital1Hours"
                        VisibleIndex="9" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #1 - Line Number" FieldName="projectCapital1Line"
                        VisibleIndex="10" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #1 - PO Number" FieldName="projectCapital1Po"
                        VisibleIndex="11" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #2 - Title" FieldName="projectCapital2Title"
                        VisibleIndex="12" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #2 - Hours" FieldName="projectCapital2Hours"
                        VisibleIndex="13" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #2 - Line Number" FieldName="projectCapital2Line"
                        VisibleIndex="14" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #2 - PO Number" FieldName="projectCapital2Po"
                        VisibleIndex="15" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #3 - Title" FieldName="projectCapital3Title"
                        VisibleIndex="16" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #3 - Hours" FieldName="projectCapital3Hours"
                        VisibleIndex="17" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #3 - Line Number" FieldName="projectCapital3Line"
                        VisibleIndex="18" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #3 - PO Number" FieldName="projectCapital3Po"
                        VisibleIndex="19" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #4 - Title" FieldName="projectCapital4Title"
                        VisibleIndex="20" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #4 - Hours" FieldName="projectCapital4Hours"
                        VisibleIndex="21" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #4 - Line Number" FieldName="projectCapital4Line"
                        VisibleIndex="22" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #4 - PO Number" FieldName="projectCapital4Po"
                        VisibleIndex="23" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #5 - Title" FieldName="projectCapital5Title"
                        VisibleIndex="124" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #5 - Hours" FieldName="projectCapital5Hours"
                        VisibleIndex="25" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #5 - Line Number" FieldName="projectCapital5Line"
                        VisibleIndex="26" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #5 - PO Number" FieldName="projectCapital5Po"
                        VisibleIndex="27" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #6 - Title" FieldName="projectCapital6Title"
                        VisibleIndex="28" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #6 - Hours" FieldName="projectCapital6Hours"
                        VisibleIndex="29" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #6 - Line Number" FieldName="projectCapital6Line"
                        VisibleIndex="30" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #6 - PO Number" FieldName="projectCapital6Po"
                        VisibleIndex="31" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #7 - Title" FieldName="projectCapital7Title"
                        VisibleIndex="32" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #7 - Hours" FieldName="projectCapital7Hours"
                        VisibleIndex="33" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #7 - Line Number" FieldName="projectCapital7Line"
                        VisibleIndex="34" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #7 - PO Number" FieldName="projectCapital7Po"
                        VisibleIndex="35" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #8 - Title" FieldName="projectCapital8Title"
                        VisibleIndex="36" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #8 - Hours" FieldName="projectCapital8Hours"
                        VisibleIndex="37" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #8 - Line Number" FieldName="projectCapital8Line"
                        VisibleIndex="38" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #8 - PO Number" FieldName="projectCapital8Po"
                        VisibleIndex="39" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #9 - Title" FieldName="projectCapital9Title"
                        VisibleIndex="40" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #9 - Hours" FieldName="projectCapital9Hours"
                        VisibleIndex="41" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #9 - Line Number" FieldName="projectCapital9Line"
                        VisibleIndex="42" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #9 - PO Number" FieldName="projectCapital9Po"
                        VisibleIndex="43" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 10 - Title" FieldName="projectCapital10Title"
                        VisibleIndex="44" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 10 - Hours" FieldName="projectCapital10Hours"
                        VisibleIndex="45" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #10 - Line Number" FieldName="projectCapital10Line"
                        VisibleIndex="46" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #10 - PO Number" FieldName="projectCapital10Po"
                        VisibleIndex="47" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 11 - Title" FieldName="projectCapital11Title"
                        VisibleIndex="48" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 11 - Hours" FieldName="projectCapital11Hours"
                        VisibleIndex="49" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #11 - Line Number" FieldName="projectCapital11Line"
                        VisibleIndex="50" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #11 - PO Number" FieldName="projectCapital11Po"
                        VisibleIndex="51" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 12 - Title" FieldName="projectCapital12Title"
                        VisibleIndex="52" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 12 - Hours" FieldName="projectCapital12Hours"
                        VisibleIndex="53" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #12 - Line Number" FieldName="projectCapital12Line"
                        VisibleIndex="54" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #12 - PO Number" FieldName="projectCapital12Po"
                        VisibleIndex="55" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 13 - Title" FieldName="projectCapital13Title"
                        VisibleIndex="56" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 13 - Hours" FieldName="projectCapital13Hours"
                        VisibleIndex="57" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #13 - Line Number" FieldName="projectCapital13Line"
                        VisibleIndex="58" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #13 - PO Number" FieldName="projectCapital13Po"
                        VisibleIndex="59" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 14 - Title" FieldName="projectCapital14Title"
                        VisibleIndex="60" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 14 - Hours" FieldName="projectCapital14Hours"
                        VisibleIndex="61" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #14 - Line Number" FieldName="projectCapital14Line"
                        VisibleIndex="62" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #14 - PO Number" FieldName="projectCapital14Po"
                        VisibleIndex="63" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 15 - Title" FieldName="projectCapital15Title"
                        VisibleIndex="64" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital 15 - Hours" FieldName="projectCapital15Hours"
                        VisibleIndex="65" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #15 - Line Number" FieldName="projectCapital15Line"
                        VisibleIndex="66" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Project Capital #15 - PO Number" FieldName="projectCapital15Po"
                        VisibleIndex="67" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Eng Project Hrs - Refinery" FieldName="aheaRefineryWork"
                        VisibleIndex="68" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Eng Project Hrs - Residue" FieldName="aheaResidueWork"
                        VisibleIndex="69" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Eng Project Hrs - Smelting" FieldName="aheaSmeltingWork"
                        VisibleIndex="70" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Eng Project Hrs - Power" FieldName="aheaPowerGenerationWork"
                        VisibleIndex="71" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Total Man Hours" FieldName="aheaTotalManHours"
                        VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit DisplayFormatString="n">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Peak No. Employees on site" FieldName="aheaPeakNopplSiteWeek"
                        VisibleIndex="72" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Avg No. Employees on site" FieldName="aheaAvgNopplSiteMonth"
                        VisibleIndex="73" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="No. Employees excess 16/64" FieldName="aheaNoEmpExcessMonth"
                        VisibleIndex="74" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="First Aid Treatment Injury" FieldName="ipFATI"
                        Visible="False" VisibleIndex="75">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Medical Treatment Injury" FieldName="ipMTI"
                        Visible="False" VisibleIndex="76">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Restricted Duty Injury" FieldName="ipRDI"
                        Visible="False" VisibleIndex="1">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Lost Time Injury" FieldName="ipLTI" Visible="False"
                        VisibleIndex="9">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Injury Free Events" FieldName="ipIFE" Visible="False"
                        VisibleIndex="27">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="No. of Lost Work Days" FieldName="ehspNoLWD"
                        Visible="False" VisibleIndex="22">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="No. of Restricted Days" FieldName="ehspNoRD"
                        Visible="False" VisibleIndex="12">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Workplace Safety &amp; Compliance" FieldName="iWSC"
                        Visible="False" VisibleIndex="23">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Health &amp; Safety work standards" FieldName="oNoHSWC"
                        Visible="False" VisibleIndex="7">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Behavioural Safety program" FieldName="oNoBSP"
                        Visible="False" VisibleIndex="37">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
<%--                    <dxwgv:GridViewDataTextColumn Caption="Alcoa Annual Audit Score" FieldName="qQAS"
                        Visible="False" VisibleIndex="2">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>--%>
                    <dxwgv:GridViewDataTextColumn Caption="Annual Audit score of Embedded and Non Embedded 1s" FieldName="qQAS"
                        Visible="False" VisibleIndex="2"  Width="200px">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="No. of Non conformance issues" FieldName="qNoNCI"
                        Visible="False" VisibleIndex="32">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Training Compliance (%)" FieldName="Training"
                        Visible="False" VisibleIndex="31">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Fatality Prevention" FieldName="mFatality"
                        Visible="False" VisibleIndex="31">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="AIFR" FieldName="AIFR" Visible="False" VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="TRIFR" FieldName="TRIFR" Visible="False" VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="IFEFR" FieldName="IFEFR" Visible="False" VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="LWDIFR" FieldName="LWDIFR" Visible="False"
                        VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="IFE:No of Injuries Ratio" FieldName="IFERatio"
                        Visible="False" VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Comments" FieldName="mtOthers" Visible="False"
                        VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="JSA Field Audits" FieldName="JSAAudits" Visible="false"
                        VisibleIndex="99">
                        <EditFormSettings Visible="False" />
                        <PropertiesTextEdit MaxLength="100">
                        </PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Blood Alcohol Tests" FieldName="ffwBloodAlcoholTests"
                        VisibleIndex="110" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Positive BAC Results" FieldName="ffwPositiveBACResults"
                        VisibleIndex="111" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Drug Tests" FieldName="ffwDrugTests"
                        VisibleIndex="112" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Non Negative Results" FieldName="ffwNonNegativeResults"
                        VisibleIndex="113" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Positive Results Medication Declared" FieldName="ffwPositiveResultsMedDeclared"
                        VisibleIndex="114" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Positive Results Medication Not Declared" FieldName="ffwPositiveResultsMedNotDeclared"
                        VisibleIndex="115" Visible="False">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ExpandedButton Height="12px" Width="11px" />
                    <LoadingPanelOnStatusBar Url= "~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px" Width="11px" />
                    <DetailCollapsedButton Height="12px" Width="11px" />
                    <DetailExpandedButton Height="12px" Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <TitlePanel BackColor="White" HorizontalAlign="Center">
                        <Paddings Padding="0px" />
                        <BackgroundImage ImageUrl="~/Images/spacer.gif" />
                        <BorderLeft BorderStyle="None" />
                        <BorderTop BorderStyle="None" />
                        <BorderRight BorderStyle="None" />
                    </TitlePanel>
                </Styles>
                <Settings ShowGroupPanel="True" ShowGroupedColumns="True" ShowFilterRow="True" ShowHorizontalScrollBar="True"
                    ShowVerticalScrollBar="True" VerticalScrollableHeight="255" 
                    ShowFilterBar="Visible" ShowFilterRowMenu="True" />
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="Control" />
                <SettingsPager AlwaysShowPager="True" PageSize="10">
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <%--<ClientSideEvents CustomizationWindowCloseUp="function(s, e) { UpdateCustomizationWindowValue_KPI(); }" />--%>
                <SettingsCookies CookiesID="kpiListView" Version="0.1" />
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr align="right">
        <td align="right" class="pageName" colspan="8" style="height: 21px; text-align: right;
            text-align: -moz-right; width: 900px; padding-top: 2px; padding-bottom: 3px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
    </tr>
</table>
<dxe:ASPxButton ID="btnSaveLayout" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" OnClick="btnSaveLayout_Click" Width="99px" Text="Save Layout"
    Visible="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
</dxe:ASPxButton>
<dxe:ASPxButton ID="btnLoadLayout" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" OnClick="btnLoadLayout_Click" Width="99px" Text="Load Layout"
    Visible="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
</dxe:ASPxButton>

<!-- @BEGIN :: Data Sources !-->
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<%--<asp:SqlDataSource ID="sqldsCompanySiteCategoryStandardSites" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Sites_withAssignedCompanyCategoryAssigned" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>--%>

<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource>     

<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server" SelectMethod="GetPaged"
    EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>


<asp:SqlDataSource ID="sqldsCustomReport" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_Reports_CustomReport" ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    
<asp:SqlDataSource ID="sqldsCustomReport_GetByCompanyId" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_Reports_CustomReport_GetByCompanyId" ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spVar_CompanyId"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsUserFullName" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Users_FullName" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

<asp:ObjectDataSource ID="odsKPIGetByCompanyId" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.Kpi"
    DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetByCompanyId" TypeName="KaiZen.CSMS.Services.KpiService" UpdateMethod="Update">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="spVar_CompanyId"
            Type="Int32" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="kpiId" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>

<!-- @END :: Data Sources !-->

<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" HeaderText="Report Manager"
    Height="213px" Width="623px" CssPostfix="Office2003Blue" EnableHotTrack="False"
    Modal="True" PopupElementID="btn1" AllowDragging="True" AllowResize="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <SizeGripImage Height="16px" Width="16px" />
    <HeaderStyle>
        <Paddings PaddingRight="6px" />
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px" />
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dxwgv:ASPxGridView runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" KeyFieldName="ReportId" AutoGenerateColumns="False"
                DataSourceID="sqldsLayouts" Width="100%" ID="ASPxGridView1" OnHtmlRowCreated="grid2_RowCreated"
                OnRowUpdating="grid2_RowUpdating" OnRowInserting="grid2_RowInserting" OnInitNewRow="grid2_InitNewRow"
                OnRowValidating="grid2_RowValidating" OnStartRowEditing="grid2_StartRowEditing">
                <Columns>
                    <dxwgv:GridViewCommandColumn ShowInCustomizationForm="False" Name="commandCol" Width="100px"
                        Caption="Action" VisibleIndex="0">
                        <EditButton Visible="True" Text="Edit">
                            <Image AlternateText="Edit" Url="~/Images/gridEdit.gif">
                            </Image>
                        </EditButton>
                        <NewButton Visible="True" Text="New">
                            <Image AlternateText="New" Url="~/Images/gridNew.gif">
                            </Image>
                        </NewButton>
                        <DeleteButton Visible="True" Text="Delete">
                            <Image AlternateText="Delete" Url="~/Images/gridDelete.gif">
                            </Image>
                        </DeleteButton>
                        <CancelButton Visible="True" Text="Cancel">
                            <Image AlternateText="Cancel" Url="../Images/gridCancel.gif">
                            </Image>
                        </CancelButton>
                        <UpdateButton Visible="True" Text="Save">
                            <Image AlternateText="Save" Url="../Images/gridSave.gif">
                            </Image>
                        </UpdateButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ReportId" ReadOnly="True" ShowInCustomizationForm="True"
                        Visible="False" VisibleIndex="1">
                        <EditFormSettings Visible="False"></EditFormSettings>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn ShowInCustomizationForm="True" Width="30px" Caption=" "
                        ToolTip="Load Selected Custom Report" VisibleIndex="1">
                        <EditFormSettings Visible="False"></EditFormSettings>
                        <DataItemTemplate>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 30px">
                                        <dxe:ASPxHyperLink ID="hlLoad" runat="server" Text="Load">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                            </table>
                        </DataItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Area" ShowInCustomizationForm="True" Visible="False"
                        VisibleIndex="2">
                        <EditFormSettings Visible="True"></EditFormSettings>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ReportName" ShowInCustomizationForm="True"
                        VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ReportDescription" ReadOnly="True" ShowInCustomizationForm="True"
                        Name="gcbReportDescription" Caption="Company" Visible="False" VisibleIndex="4">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                            ValueType="System.String" DropDownHeight="150px">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="False"></EditFormSettings>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ReportLayout" ShowInCustomizationForm="True"
                        Visible="False" VisibleIndex="5">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" ReadOnly="True" ShowInCustomizationForm="True"
                        Visible="False" VisibleIndex="6">
                        <EditFormSettings Visible="True"></EditFormSettings>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" SortIndex="0" SortOrder="Descending"
                        ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3">
                        <Settings SortMode="Value"></Settings>
                        <EditFormSettings Visible="True"></EditFormSettings>
                    </dxwgv:GridViewDataDateColumn>
                </Columns>
                <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
                <SettingsEditing Mode="Inline"></SettingsEditing>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--//Change by Debashis--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                </Styles>
            </dxwgv:ASPxGridView>
            <asp:SqlDataSource runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                DeleteCommand="DELETE FROM [CustomReportingLayouts] WHERE [ReportId] = @ReportId"
                InsertCommand="INSERT INTO [CustomReportingLayouts] ([Area], [ReportName], [ReportDescription], [ReportLayout], [ModifiedByUserId], [ModifiedDate]) VALUES (@Area, @ReportName, @ReportDescription, @ReportLayout, @ModifiedByUserId, @ModifiedDate)"
                SelectCommand="SELECT * FROM [CustomReportingLayouts] WHERE ([ModifiedByUserId] = @ModifiedByUserId AND Area = 'CYOR')"
                UpdateCommand="UPDATE [CustomReportingLayouts] SET [Area] = @Area, [ReportName] = @ReportName, [ReportDescription] = @ReportDescription, [ReportLayout] = @ReportLayout, [ModifiedByUserId] = @ModifiedByUserId, [ModifiedDate] = @ModifiedDate WHERE [ReportId] = @ReportId"
                ID="sqldsLayouts">
                <DeleteParameters>
                    <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                    <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                </InsertParameters>
                <SelectParameters>
                    <asp:SessionParameter SessionField="UserId" DefaultValue="0" Name="ModifiedByUserId"
                        Type="Int32"></asp:SessionParameter>
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Area" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ReportName" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ReportDescription" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ReportLayout" Type="String"></asp:Parameter>
                    <asp:Parameter Name="ModifiedByUserId" Type="Int32"></asp:Parameter>
                    <asp:Parameter Name="ModifiedDate" Type="DateTime"></asp:Parameter>
                    <asp:Parameter Name="ReportId" Type="Int32"></asp:Parameter>
                </UpdateParameters>
            </asp:SqlDataSource>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
