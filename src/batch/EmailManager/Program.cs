﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using System.Web.Mvc;
using Repo.CSMS.Service.Database;
using ALCOA.CSMS.Batch.Common;
using ALCOA.CSMS.Batch.Common.Log;
using Repo.CSMS.DAL.EntityModels;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Specialized;
using Aspose.Email.Outlook;
using System.IO;
using System.Reflection;
using Repo.CSMS.Common.Helpers;
using ALCOA.CSMS.Batch.Common.HealthCheck;

namespace ALCOA.CSMS.EmailManager
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Log4NetService.LogInfo("EmailManagement Program Started");

                //Setup dependencies
                DependencyConfig.RegisterDependencies(new ContainerBuilder());

                HealthCheck.ResetJobSchedule("SendEmails"); //DT259 - allows for checking if the job is running


                // get all unhandled records from EmailLog
                var emailLogService = DependencyResolver.Current.GetService<IEmailLogService>();
                var emailLogs = emailLogService.GetMany(null, p => p.Handled == false, null, null);

                // send the emails
                foreach (EmailLog unhandledEmail in emailLogs)
                {
                    bool toSend = true;
                    if (!string.IsNullOrEmpty(unhandledEmail.ConfigKey))
                    {
                        // do we want to send the email
                        var configService = DependencyResolver.Current.GetService<IConfigService>();
                        Config cfgEbiAutomatic = configService.Get(p => p.Key == "EbiAutomaticEmail", null);

                        if (cfgEbiAutomatic.Value == "No") { toSend = false; } // we only want to send to the To recipients if this is true
                    }

                    if (SendEmail(unhandledEmail, ref toSend))
                    {
                        UpdateEmailTables(emailLogService, unhandledEmail, toSend); //update the database unsent & handled email tables
                    }
                }

                // log the details
                Log4NetService.LogInfo("EmailManagement Program Complete");
            }
            catch (Exception ex)
            {
                Log4NetService.LogError("Error Message: " + ex.Message + Environment.NewLine + Environment.NewLine + "Inner Exception: " + ex.InnerException);
            }
        }


        private static bool SendEmail(EmailLog email, ref bool toSend)
        {
            NameValueCollection appSettings = ConfigurationManager.AppSettings;
            string environment = appSettings["Environment"].ToString();
            //string fromAddress = appSettings.Get("EmailFromAddress");

            const string MAILHOST = "MailServer", FROM_EMAIL = "ContactEmail";

            // get the smtp and from contact email from the config table
            var configService = DependencyResolver.Current.GetService<IConfigService>();
            List<Config> cfg = configService.GetMany(null, p => p.Key == MAILHOST || p.Key == FROM_EMAIL, null, null);

            SmtpClient smtpClientToEmail = new SmtpClient();
            smtpClientToEmail.Host = cfg.First(p => p.Key == MAILHOST).Value;
            smtpClientToEmail.DeliveryMethod = SmtpDeliveryMethod.Network;

            // set the from address
            string fromAddress;
            if (email.EmailFrom != null)
                fromAddress = email.EmailFrom; // 1st option - from email log table
            else
            {
                fromAddress = cfg.First(p => p.Key == FROM_EMAIL).Value; //2nd option - from config table

                if (fromAddress == null)
                    fromAddress = appSettings.Get("EmailFromAddress"); // 3rd option - from app.config
            }

            // only send the email to the recipients if the config file says to send and it's prod.
            if (environment.ToUpper() != "PROD")
            {
                toSend = false;
            }

            if (toSend)
            {
                SendProdEmail(email, fromAddress, smtpClientToEmail);
            }
            else
            {
                SendEmailToSupport(email, appSettings, environment, fromAddress, smtpClientToEmail);
            }
            return true;
        }


        #region SendProdEmail
        /// <summary>
        /// Create and send the email since this is production
        /// </summary>
        /// <param name="email">Email object from the UnsentEmail table</param>
        /// <param name="fromAddress">from app.config</param>
        /// <param name="smtpClientToEmail">from config db table</param>
        private static void SendProdEmail(EmailLog email, string fromAddress, SmtpClient smtpClientToEmail)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.IsBodyHtml = email.IsHTML;

            mailMessage.Body = System.Text.Encoding.Default.GetString(email.EmailLogMessageBody);

            mailMessage.Subject = email.EmailLogMessageSubject;
            mailMessage.From = new MailAddress(fromAddress);

            if (!string.IsNullOrEmpty(email.EmailTo))
                mailMessage.To.Add(CommonHelper.ConvertToCSV(email.EmailTo));

            if (!string.IsNullOrEmpty(email.EmailCc))
                mailMessage.CC.Add(CommonHelper.ConvertToCSV(email.EmailCc));

            if (!string.IsNullOrEmpty(email.EmailBcc))
                mailMessage.Bcc.Add(CommonHelper.ConvertToCSV(email.EmailBcc));

            // add the attachments if they exist
            if (email.AttachmentPath != null && AttachmentPathExists(email))
            {

                foreach (string filename in Directory.GetFiles(email.AttachmentPath))
                {
                    // if no filter, add them all, otherwise only add it if it matches the filter
                    if (email.AttachmentFileFilter == null || filename.Contains(email.AttachmentFileFilter))
                    {
                        mailMessage.Attachments.Add(new Attachment(filename));
                    }
                }

            }

            smtpClientToEmail.Send(mailMessage);
        }

        #endregion


        #region SendEmailToSupport
        /// <summary>
        /// Package the email in a .msg file, email that file to the support emails listed in the app.config
        /// </summary>
        /// <param name="email">Email object from the UnsentEmail table</param>
        /// <param name="appSettings"></param>
        /// <param name="environment">from app.config</param>
        /// <param name="fromAddress">from app.config</param>
        /// <param name="smtpClientToEmail">from config db table</param>
        private static void SendEmailToSupport(EmailLog email, NameValueCollection appSettings, string environment, string fromAddress, SmtpClient smtpClientToEmail)
        {
            string emailFolder = appSettings.Get("EmailFileLocation");
            System.IO.Directory.CreateDirectory(emailFolder); // create folder if it doen't exist

            string fileName = "Email Id = " + email.EmailLogId + " - " + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-tt") + ".msg"; ;
            string[] files = Directory.GetFiles(emailFolder, fileName); ;

            CreateEmailMessageFile(email, appSettings, fromAddress, emailFolder, fileName);
            SendSupportEmail(appSettings, environment, smtpClientToEmail, emailFolder, fileName, ref files, email);

            smtpClientToEmail.Dispose();

            if (files.Length > 0)
                File.Delete(files[0].ToString());
        }


        private static void CreateEmailMessageFile(EmailLog email, NameValueCollection appSettings, string fromAddress, string emailFolder, string fileName)
        {
            Aspose.Email.Mail.MailMessage mailMessageToFile = new Aspose.Email.Mail.MailMessage();
            mailMessageToFile.IsBodyHtml = email.IsHTML;
            mailMessageToFile.HtmlBody = System.Text.Encoding.Default.GetString(email.EmailLogMessageBody);
            mailMessageToFile.Subject = email.EmailLogMessageSubject;
            mailMessageToFile.From = new Aspose.Email.Mail.MailAddress(fromAddress);

            if (!string.IsNullOrEmpty(email.EmailTo))
                mailMessageToFile.To = CommonHelper.ConvertToCSV(email.EmailTo);

            if (!string.IsNullOrEmpty(email.EmailCc))
                mailMessageToFile.CC.Add(CommonHelper.ConvertToCSV(email.EmailCc));

            if (!string.IsNullOrEmpty(email.EmailBcc))
                mailMessageToFile.Bcc.Add(CommonHelper.ConvertToCSV(email.EmailBcc));


            // add the attachments if they exist
            if (email.AttachmentPath != null && AttachmentPathExists(email))
            {
                foreach (string filename in Directory.GetFiles(email.AttachmentPath))
                {
                    // if no filter, add them all, otherwise only add it if it matches the filter
                    if (email.AttachmentFileFilter == null || filename.Contains(email.AttachmentFileFilter))
                    {
                        mailMessageToFile.Attachments.Add(new Aspose.Email.Mail.Attachment(filename));
                    }
                }
            }

            MapiMessage outlookMsg = MapiMessage.FromMailMessage(mailMessageToFile);

            // Path and file name where message file will be saved
            System.IO.Directory.CreateDirectory(emailFolder); // create folder if it doen't exist


            string strMsgPath = emailFolder + fileName;

            // Save the message (MSG) file
            outlookMsg.Save(strMsgPath);
        }


        private static void SendSupportEmail(NameValueCollection appSettings, string environment, SmtpClient smtpClientToEmail, string emailFolder, string fileName, ref string[] files, EmailLog email)
        {
            // this is the email that is being sent to the email error recipients (from app.config)
            MailMessage mailMessageForHelpDesk;
            mailMessageForHelpDesk = new MailMessage();

            string errorRecipients = appSettings.Get("SupportEmailRecipients");
            if (!string.IsNullOrEmpty(errorRecipients))
                mailMessageForHelpDesk.To.Add(CommonHelper.ConvertToCSV(errorRecipients));

            files = Directory.GetFiles(emailFolder, fileName);

            if (files.Length > 0)
            {
                mailMessageForHelpDesk.Attachments.Add(new Attachment(files[0].ToString()));
            }

            mailMessageForHelpDesk.From = new MailAddress(appSettings.Get("EmailFromAddress"));

            var machineName = System.Environment.MachineName;
            mailMessageForHelpDesk.Subject = "CSMS " + environment.ToUpper() + " on " + machineName + " - Id: " + email.EmailLogId + ", Subject: " + email.EmailLogMessageSubject;

            smtpClientToEmail.Send(mailMessageForHelpDesk);

            mailMessageForHelpDesk.Dispose();
        }
        #endregion


        #region UpdateEmailTables
        private static void UpdateEmailTables(IEmailLogService emailLogService, EmailLog emailLog, bool toSend)
        {
            emailLog.Handled = true;
            emailLog.HandledDateTime = DateTime.Now;
            emailLog.Sent = toSend; // has the email actually been sent or just packaged and emailed to support.

            emailLogService.Update(emailLog);

            //log the details
            string sentStatus;
            if (toSend)
                sentStatus = "sent to recipients";
            else
                sentStatus = "NOT sent to recipients but was sent to the support emails";

            Log4NetService.LogInfo(string.Format("Email Id: {0} was {1}", emailLog.EmailLogId, sentStatus));
        }
        #endregion


        private static bool AttachmentPathExists(EmailLog email)
        {
            if (!Directory.Exists(email.AttachmentPath))
            {
                Log4NetService.LogError("Attachment Path [" + email.AttachmentPath + "] does not exist for email id " + email.EmailLogId);
                return false;
            }
            return true;
        }
    }
}