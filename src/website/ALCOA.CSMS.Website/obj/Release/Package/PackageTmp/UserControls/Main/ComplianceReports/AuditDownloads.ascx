﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_AuditDownloads" Codebehind="AuditDownloads.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="900px">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
        <span class="bodycopy"><span class="title">Reports</span><br />
                <span class="date">Audit Downloads</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
            </td>
    </tr>
        <tr>
            <td style="width: 117px; height: 15px; text-align: right;">
                <strong>&nbsp;Legend:</strong></td>
            <td style="width: 784px; height: 15px;">
                MR (Must Read), MFU (Most Frequently Used), S (Search), MM (Mines Medical), TP (Training
                Packages)</td>
            <td style="width: 150px; text-align: right; height: 15px;">
                <a href="javascript:ShowHideCustomizationWindow()">
                        <span style="color: #0000ff; text-decoration: underline">Customize</span>
                </a>
            </td>
        </tr>
        <tr>
            <td colspan="3">

            <dxwgv:ASPxGridView ID="grid"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                DataSourceID="dsDocumentsDownloadLog_Report"
                Width="900px"
             >
                <Columns>
                    <dxwgv:GridViewDataDateColumn Caption="Audit Date" FieldName="AuditedOn" VisibleIndex="0" SortIndex="0" SortOrder="Descending">
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataDateColumn Caption = "Audit Time" FieldName="AuditedOn" Visible="False" VisibleIndex="1" SortIndex="1" SortOrder="Descending">
                    <PropertiesDateEdit DisplayFormatString="hh:mm tt">
                        </PropertiesDateEdit>
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileName" VisibleIndex="1" Caption="File">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentType" VisibleIndex="2" Visible="false" Caption="Referrer">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DownloadedFrom" VisibleIndex="2" Caption="Type">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="UserLogon" VisibleIndex="3" Caption="Logon">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="LastName" VisibleIndex="4">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="5">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="CompanyName" VisibleIndex="6" Caption="Company">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Role" VisibleIndex="7">
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <ExpandedButton Height="12px"
                        Width="11px" />
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px"
                        Width="11px" />
                    <DetailCollapsedButton Height="12px"
                        Width="11px" />
                    <DetailExpandedButton Height="12px"
                        Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <Cell Wrap="True">
                    </Cell>
                </Styles>
                <SettingsPager>
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <SettingsBehavior ConfirmDelete="True"/>
                <SettingsCookies Version="0.1" />
                <SettingsCustomizationWindow Enabled="True" />
                <Settings ShowGroupedColumns="True" ShowGroupPanel="True" />
                
            </dxwgv:ASPxGridView>
            </td>
        </tr>
    <tr>
        <td colspan="3" style="height: 35px; text-align: right">
            Number of Rows to show per page:<asp:DropDownList ID="ddlPager" runat="server"
                AutoPostBack="True" OnSelectedIndexChanged="ddlPager_SelectedIndexChanged">
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
</table>
<table width="900px">
<tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
</tr>
</table>
<asp:ObjectDataSource  
    ID="dsDocumentsDownloadLog_Report"
    runat="server"
    TypeName="KaiZen.CSMS.Services.DocumentsDownloadLogService"
    SelectMethod="Report">
</asp:ObjectDataSource>

<asp:ObjectDataSource
    ID="dsDocumentsDownloadLog_Report_ByCompany"
    runat="server"
    TypeName="KaiZen.CSMS.Services.DocumentsDownloadLogService"
    SelectMethod="Report_ByCompany">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="0" Name="CompanyId" SessionField="CompanyId" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
