﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdHocRadarItemsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdHocRadarItemsDataSourceDesigner))]
	public class AdHocRadarItemsDataSource : ProviderDataSource<AdHocRadarItems, AdHocRadarItemsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsDataSource class.
		/// </summary>
		public AdHocRadarItemsDataSource() : base(new AdHocRadarItemsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdHocRadarItemsDataSourceView used by the AdHocRadarItemsDataSource.
		/// </summary>
		protected AdHocRadarItemsDataSourceView AdHocRadarItemsView
		{
			get { return ( View as AdHocRadarItemsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdHocRadarItemsDataSource control invokes to retrieve data.
		/// </summary>
		public AdHocRadarItemsSelectMethod SelectMethod
		{
			get
			{
				AdHocRadarItemsSelectMethod selectMethod = AdHocRadarItemsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdHocRadarItemsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdHocRadarItemsDataSourceView class that is to be
		/// used by the AdHocRadarItemsDataSource.
		/// </summary>
		/// <returns>An instance of the AdHocRadarItemsDataSourceView class.</returns>
		protected override BaseDataSourceView<AdHocRadarItems, AdHocRadarItemsKey> GetNewDataSourceView()
		{
			return new AdHocRadarItemsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdHocRadarItemsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdHocRadarItemsDataSourceView : ProviderDataSourceView<AdHocRadarItems, AdHocRadarItemsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdHocRadarItemsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdHocRadarItemsDataSourceView(AdHocRadarItemsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdHocRadarItemsDataSource AdHocRadarItemsOwner
		{
			get { return Owner as AdHocRadarItemsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdHocRadarItemsSelectMethod SelectMethod
		{
			get { return AdHocRadarItemsOwner.SelectMethod; }
			set { AdHocRadarItemsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdHocRadarItemsService AdHocRadarItemsProvider
		{
			get { return Provider as AdHocRadarItemsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdHocRadarItems> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdHocRadarItems> results = null;
			AdHocRadarItems item;
			count = 0;
			
			System.Int32 _adHocRadarItemId;
			System.Int32 _year;
			System.Int32 _adHocRadarId;
			System.Int32 _siteId;

			switch ( SelectMethod )
			{
				case AdHocRadarItemsSelectMethod.Get:
					AdHocRadarItemsKey entityKey  = new AdHocRadarItemsKey();
					entityKey.Load(values);
					item = AdHocRadarItemsProvider.Get(entityKey);
					results = new TList<AdHocRadarItems>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdHocRadarItemsSelectMethod.GetAll:
                    results = AdHocRadarItemsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdHocRadarItemsSelectMethod.GetPaged:
					results = AdHocRadarItemsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdHocRadarItemsSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdHocRadarItemsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdHocRadarItemsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdHocRadarItemsSelectMethod.GetByAdHocRadarItemId:
					_adHocRadarItemId = ( values["AdHocRadarItemId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarItemId"], typeof(System.Int32)) : (int)0;
					item = AdHocRadarItemsProvider.GetByAdHocRadarItemId(_adHocRadarItemId);
					results = new TList<AdHocRadarItems>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdHocRadarItemsSelectMethod.GetByYearAdHocRadarIdSiteId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = AdHocRadarItemsProvider.GetByYearAdHocRadarIdSiteId(_year, _adHocRadarId, _siteId);
					results = new TList<AdHocRadarItems>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdHocRadarItemsSelectMethod.GetByYearAdHocRadarId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItemsProvider.GetByYearAdHocRadarId(_year, _adHocRadarId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case AdHocRadarItemsSelectMethod.GetByAdHocRadarId:
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItemsProvider.GetByAdHocRadarId(_adHocRadarId, this.StartIndex, this.PageSize, out count);
					break;
				case AdHocRadarItemsSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItemsProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdHocRadarItemsSelectMethod.Get || SelectMethod == AdHocRadarItemsSelectMethod.GetByAdHocRadarItemId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdHocRadarItems entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdHocRadarItemsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdHocRadarItems> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdHocRadarItemsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdHocRadarItemsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdHocRadarItemsDataSource class.
	/// </summary>
	public class AdHocRadarItemsDataSourceDesigner : ProviderDataSourceDesigner<AdHocRadarItems, AdHocRadarItemsKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsDataSourceDesigner class.
		/// </summary>
		public AdHocRadarItemsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdHocRadarItemsSelectMethod SelectMethod
		{
			get { return ((AdHocRadarItemsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdHocRadarItemsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdHocRadarItemsDataSourceActionList

	/// <summary>
	/// Supports the AdHocRadarItemsDataSourceDesigner class.
	/// </summary>
	internal class AdHocRadarItemsDataSourceActionList : DesignerActionList
	{
		private AdHocRadarItemsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItemsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdHocRadarItemsDataSourceActionList(AdHocRadarItemsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdHocRadarItemsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdHocRadarItemsDataSourceActionList
	
	#endregion AdHocRadarItemsDataSourceDesigner
	
	#region AdHocRadarItemsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdHocRadarItemsDataSource.SelectMethod property.
	/// </summary>
	public enum AdHocRadarItemsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdHocRadarItemId method.
		/// </summary>
		GetByAdHocRadarItemId,
		/// <summary>
		/// Represents the GetByYearAdHocRadarIdSiteId method.
		/// </summary>
		GetByYearAdHocRadarIdSiteId,
		/// <summary>
		/// Represents the GetByYearAdHocRadarId method.
		/// </summary>
		GetByYearAdHocRadarId,
		/// <summary>
		/// Represents the GetByAdHocRadarId method.
		/// </summary>
		GetByAdHocRadarId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion AdHocRadarItemsSelectMethod

	#region AdHocRadarItemsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsFilter : SqlFilter<AdHocRadarItemsColumn>
	{
	}
	
	#endregion AdHocRadarItemsFilter

	#region AdHocRadarItemsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsExpressionBuilder : SqlExpressionBuilder<AdHocRadarItemsColumn>
	{
	}
	
	#endregion AdHocRadarItemsExpressionBuilder	

	#region AdHocRadarItemsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdHocRadarItemsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItemsProperty : ChildEntityProperty<AdHocRadarItemsChildEntityTypes>
	{
	}
	
	#endregion AdHocRadarItemsProperty
}

