﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.IO;

using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class EditSqExemption : System.Web.UI.Page
    {
        bool EditMode = false;
        int SqExemptionId = 0;
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            try
            {
                if (!postBack)
                {
                    if (Request.QueryString["q"] == null)
                    {
                        //new
                        btnUpload.Text = "Add / Upload";

                        int CompanyId = 0;
                        Int32.TryParse(Request.QueryString["c"], out CompanyId);
                        if (CompanyId == 0) throw new Exception("Invalid Url");
                        cbCompany.Value = CompanyId;
                    }
                    else
                    {
                        Int32.TryParse(Request.QueryString["q"], out SqExemptionId);

                        if (SqExemptionId == 0) throw new Exception("Invalid Url");
                        btnUpload.Text = "Edit / Upload";
                        EditMode = true;
                    }

                    if (SqExemptionId > 0)
                    {
                        Load(SqExemptionId);
                        lblStatus.Text = "View";
                    }

                    if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
                    {
                        
                        if (SqExemptionId == 0)
                            lblStatus.Text = "New";

                         if (auth.RoleId == (int)RoleList.Administrator)
                         {
                             if (SqExemptionId > 0)
                                 lblStatus.Text = "Edit";

                             btnUpload.Enabled = true;
                             btnDelete.Enabled = true;
                             btnDelete.Visible = true;
                             btnUpload.Visible = true;
                         }
                         else
                         {
                             if (SqExemptionId > 0)
                             {
                                 btnUpload.Enabled = false;
                                 btnDelete.Enabled = false;
                                 btnUpload.Visible = false;
                                 btnDelete.Visible = false;
                                 ucNewFile.Visible = false;
                                 lblCurrentFile.Text = "The following file is currently saved: ";
                             }
                         }
                    }
                    else
                    {
                        //throw new Exception("Not Allowed");

                        //set all to read only and disable input buttons
                        cbSite.Enabled = false;
                        deDateApplied.Enabled = false;
                        deValidFrom.Enabled = false;
                        deValidTo.Enabled = false;
                        cbCompanyType.Enabled = false;
                        cbRequestingCompany.Enabled = false;
                        ucNewFile.Enabled = false;

                        btnDelete.Enabled = false;
                        btnUpload.Enabled = false;
                        btnUpload.Visible = false;
                        btnDelete.Visible = false;
                        ucNewFile.Visible = false;
                        lblCurrentFile.Text = "The following file is currently saved: ";
                    }
                }
                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                throw new Exception(ex.Message);
            }
        }

        protected void Load(int sqExemptionId)
        {
            lblCurrentFile.Visible = true;
            hlCurrentFile.Visible = true;

            SqExemptionService seService = new SqExemptionService();
            using (SqExemption se = seService.GetBySqExemptionId(sqExemptionId))
            {
                if (se == null)
                {
                    throw new Exception("Could Not Find 315 Form.");
                }
                else
                {
                    //load data
                    cbCompany.Value = se.CompanyId;
                    cbRequestingCompany.Value = se.RequestedByCompanyId;
                    deDateApplied.Value = se.DateApplied;
                    deValidFrom.Value = se.ValidFrom;
                    deValidTo.Value = se.ValidTo;
                    cbCompanyType.Value = se.CompanyStatus2Id;
                    cbSite.Value = se.SiteId;

                    UsersService uService = new UsersService();
                    Users u = uService.GetByUserId(se.ModifiedByUserId);
                    CompaniesService cService = new CompaniesService();
                    Companies c = cService.GetByCompanyId(u.CompanyId);

                    lblModified.Text = String.Format("Last Modified By {0} {1} ({2}) at {3}", u.FirstName, u.LastName, c.CompanyName, se.ModifiedDate.ToString());

                    int count = 0;
                    FileVaultListSqExemptionFilters fvlseFilters = new FileVaultListSqExemptionFilters();
                    fvlseFilters.Append(FileVaultListSqExemptionColumn.FileVaultId, se.FileVaultId.ToString());
                    VList<FileVaultListSqExemption> fvlseList = DataRepository.FileVaultListSqExemptionProvider.GetPaged(fvlseFilters.ToString(), "", 0, 1, out count);
                    if (count != 1) throw new Exception("Could Not Find File.");

                    hlCurrentFile.Text = String.Format("{0} ({1})", 
                                                        fvlseList[0].FileName,
                                                        String.Format("{0:n0}KB", KaiZen.Library.FileUtilities.Convert.BytestoKilobytes(Convert.ToInt64(fvlseList[0].ContentLength)))
                                                        );

                    hlCurrentFile.NavigateUrl = String.Format("javascript:popUp2alt('../PopUps/GetFile.aspx{0}');", QueryStringModule.Encrypt("q=" + se.FileVaultId));
                }
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                bool success = false;
                string errorMsg = "No Attachment Selected. Please click Browse and select a file for upload.";
                
                try
                {
                    if (Request.QueryString["q"] != null)
                        Int32.TryParse(Request.QueryString["q"], out SqExemptionId);

                    if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
                    {
                        if (String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) || !ucNewFile.UploadedFiles[0].IsValid)
                        {
                            throw new Exception(errorMsg);
                        }
                    }
                    else
                    {
                        throw new Exception(errorMsg);
                    }
                    

                    UploadFile(SqExemptionId);
                    
                    success = true;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
                    errorMsg = ex.Message;
                }
                finally
                {
                    if (success)
                    {
                        ClientScriptManager clientScript = Page.ClientScript;
                        clientScript.RegisterStartupScript(this.GetType(), "CloseWindowScript", "<script>javascript:window.close();</script>");

                    }
                    else
                    {
                        lblFileUploadMsg.Text = String.Format("Error Occured: {0}", errorMsg);
                    }
                }
            }
        }

        protected void UploadFile(int sqExemptionId)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                int fileVaultId = 0;

                SqExemptionService seService = new SqExemptionService();
                SqExemption se = new SqExemption() { CompanyId = Convert.ToInt32(Request.QueryString["c"].ToString()), ModifiedByUserId = auth.UserId, ModifiedDate = DateTime.Now };

                if(sqExemptionId > 0)
                {
                    se = seService.GetBySqExemptionId(sqExemptionId);
                    fileVaultId = se.FileVaultId;
                }
                
                if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
                {
                    if (!String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) && ucNewFile.UploadedFiles[0].IsValid)
                    {
                        FileVault fv = new FileVault() { FileVaultCategoryId = (int)FileVaultCategoryList.SqExemption, ModifiedByUserId = auth.UserId, ModifiedDate = DateTime.Now };
                        if (fileVaultId > 0)
                        {
                            FileVaultService fvService = new FileVaultService();
                            fv = fvService.GetByFileVaultId(fileVaultId);
                        }

                        Configuration configuration = new Configuration();
                        string tempPath = @configuration.GetValue(ConfigList.TempDir);
                        string fileName = ucNewFile.UploadedFiles[0].FileName;
                        string fullPath = tempPath + fileName;
                        ucNewFile.UploadedFiles[0].SaveAs(fullPath);

                        FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        fv.FileName = fileName;
                        fv.Content = FileUtilities.Read.ReadFully(fs, 51200);
                        fv.FileHash = FileUtilities.Calculate.HashFile(fv.Content);
                        fv.ContentLength = Convert.ToInt32(fs.Length);

                        fs.Close();
                        fs.Dispose();
                        System.Threading.Thread.Sleep(1000);
                        File.Delete(fullPath);

                        if(fileVaultId > 0)
                            DataRepository.FileVaultProvider.Save(transactionManager, fv);
                        else
                            DataRepository.FileVaultProvider.Insert(transactionManager, fv);

                        fileVaultId = fv.FileVaultId;
                    }
                }

                se.FileVaultId = fileVaultId;
                se.SiteId = (int)cbSite.Value;
                se.RequestedByCompanyId = (int)cbRequestingCompany.Value;
                se.DateApplied = (DateTime)deDateApplied.Value;
                se.ValidFrom = (DateTime)deValidFrom.Value;
                se.ValidTo = (DateTime)deValidTo.Value;
                se.CompanyStatus2Id = (int)cbCompanyType.Value;

                if(sqExemptionId > 0)
                    DataRepository.SqExemptionProvider.Update(transactionManager, se);
                else
                    DataRepository.SqExemptionProvider.Insert(transactionManager, se);

                transactionManager.Commit();

            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }

        protected void Delete(int sqExemptionId)
        {
            TransactionManager transactionManager = null;
            bool success = false;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                SqExemptionService seService = new SqExemptionService();
                SqExemption se = seService.GetBySqExemptionId(sqExemptionId);
                if (se == null) throw new Exception("315 Form Not Found.");

                FileVaultService fvService = new FileVaultService();
                FileVault fv = fvService.GetByFileVaultId(se.FileVaultId);

                
                DataRepository.SqExemptionProvider.Delete(transactionManager, se);
                DataRepository.FileVaultProvider.Delete(transactionManager, fv);

                transactionManager.Commit();
                success = true;

            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
            finally
            {
                if (success)
                {
                    ClientScriptManager clientScript = Page.ClientScript;
                    clientScript.RegisterStartupScript(this.GetType(), "CloseWindowScript", "<script>javascript:window.close();</script>");

                }
                else
                {
                    lblFileUploadMsg.Text = String.Format("Error Occured while Deleting");
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                bool success = false;
                string errorMsg = "Error Deleting 315 Form. Contact your Administrator.";
                try
                {
                    if (Request.QueryString["q"] != null)
                        Int32.TryParse(Request.QueryString["q"], out SqExemptionId);

                    Delete(SqExemptionId);
                    success = true;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
                    errorMsg = ex.Message;
                }
                finally
                {
                    if (success)
                    {
                        //Close and refresh main page.
                    }
                    else
                    {
                        lblFileUploadMsg.Text = String.Format("Error Occured: {0}", errorMsg);
                    }
                }
            }
        }
    }
}