﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiCompanySiteCategoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(KpiCompanySiteCategoryDataSourceDesigner))]
	public class KpiCompanySiteCategoryDataSource : ReadOnlyDataSource<KpiCompanySiteCategory>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryDataSource class.
		/// </summary>
		public KpiCompanySiteCategoryDataSource() : base(new KpiCompanySiteCategoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiCompanySiteCategoryDataSourceView used by the KpiCompanySiteCategoryDataSource.
		/// </summary>
		protected KpiCompanySiteCategoryDataSourceView KpiCompanySiteCategoryView
		{
			get { return ( View as KpiCompanySiteCategoryDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiCompanySiteCategoryDataSource control invokes to retrieve data.
		/// </summary>
		public new KpiCompanySiteCategorySelectMethod SelectMethod
		{
			get
			{
				KpiCompanySiteCategorySelectMethod selectMethod = KpiCompanySiteCategorySelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiCompanySiteCategorySelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiCompanySiteCategoryDataSourceView class that is to be
		/// used by the KpiCompanySiteCategoryDataSource.
		/// </summary>
		/// <returns>An instance of the KpiCompanySiteCategoryDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiCompanySiteCategory, Object> GetNewDataSourceView()
		{
			return new KpiCompanySiteCategoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiCompanySiteCategoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiCompanySiteCategoryDataSourceView : ReadOnlyDataSourceView<KpiCompanySiteCategory>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiCompanySiteCategoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiCompanySiteCategoryDataSourceView(KpiCompanySiteCategoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiCompanySiteCategoryDataSource KpiCompanySiteCategoryOwner
		{
			get { return Owner as KpiCompanySiteCategoryDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal new KpiCompanySiteCategorySelectMethod SelectMethod
		{
			get { return KpiCompanySiteCategoryOwner.SelectMethod; }
			set { KpiCompanySiteCategoryOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiCompanySiteCategoryService KpiCompanySiteCategoryProvider
		{
			get { return Provider as KpiCompanySiteCategoryService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
	    /// <param name="values"></param>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<KpiCompanySiteCategory> GetSelectData(IDictionary values, out int count)
		{	
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			
			IList<KpiCompanySiteCategory> results = null;
			// KpiCompanySiteCategory item;
			count = 0;
			
			System.Int32? sp184_CompanySiteCategoryId;
			System.Int32? sp180_CompanyId;
			System.Int32? sp181_CompanyId;
			System.Int32? sp181_SiteId;
			System.Int32? sp183_CompanyId;
			System.Int32? sp183_SiteId;
			System.Int32? sp183_Year;
			System.Int32? sp183_Month;
			System.Int32? sp171_Year;
			System.Int32? sp171_Month;
			System.Int32? sp171_CompanySiteCategoryId;
			System.Int32? sp170_Year;
			System.Int32? sp170_Month;
			System.Int32? sp182_CompanyId;
			System.Int32? sp182_SiteId;
			System.Int32? sp182_Year;

			switch ( SelectMethod )
			{
				case KpiCompanySiteCategorySelectMethod.Get:
					results = KpiCompanySiteCategoryProvider.Get(WhereClause, OrderBy, StartIndex, PageSize, out count);
                    break;
				case KpiCompanySiteCategorySelectMethod.GetPaged:
					results = KpiCompanySiteCategoryProvider.GetPaged(WhereClause, OrderBy, StartIndex, PageSize, out count);
					break;
				case KpiCompanySiteCategorySelectMethod.GetAll:
					results = KpiCompanySiteCategoryProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiCompanySiteCategorySelectMethod.Find:
					results = KpiCompanySiteCategoryProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
                    break;
				// Custom
				case KpiCompanySiteCategorySelectMethod.GetByCompanySiteCategoryId:
					sp184_CompanySiteCategoryId = (System.Int32?) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.GetByCompanySiteCategoryId(sp184_CompanySiteCategoryId, StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.GetByCompanyId:
					sp180_CompanyId = (System.Int32?) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.GetByCompanyId(sp180_CompanyId, StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.GetByCompanyIdSiteId:
					sp181_CompanyId = (System.Int32?) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32?));
					sp181_SiteId = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.GetByCompanyIdSiteId(sp181_CompanyId, sp181_SiteId, StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.GetByCompanyIdSiteIdYearMonth:
					sp183_CompanyId = (System.Int32?) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32?));
					sp183_SiteId = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					sp183_Year = (System.Int32?) EntityUtil.ChangeType(values["Year"], typeof(System.Int32?));
					sp183_Month = (System.Int32?) EntityUtil.ChangeType(values["Month"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.GetByCompanyIdSiteIdYearMonth(sp183_CompanyId, sp183_SiteId, sp183_Year, sp183_Month, StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.ByMonthYear_CompanySiteCategoryId:
					sp171_Year = (System.Int32?) EntityUtil.ChangeType(values["Year"], typeof(System.Int32?));
					sp171_Month = (System.Int32?) EntityUtil.ChangeType(values["Month"], typeof(System.Int32?));
					sp171_CompanySiteCategoryId = (System.Int32?) EntityUtil.ChangeType(values["CompanySiteCategoryId"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.ByMonthYear_CompanySiteCategoryId(sp171_Year, sp171_Month, sp171_CompanySiteCategoryId, StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.ByMonthYear:
					sp170_Year = (System.Int32?) EntityUtil.ChangeType(values["Year"], typeof(System.Int32?));
					sp170_Month = (System.Int32?) EntityUtil.ChangeType(values["Month"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.ByMonthYear(sp170_Year, sp170_Month, StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.Exceptions_NoCompanySiteCategoryId:
					results = KpiCompanySiteCategoryProvider.Exceptions_NoCompanySiteCategoryId(StartIndex, PageSize);
					break;
				case KpiCompanySiteCategorySelectMethod.GetByCompanyIdSiteIdYear:
					sp182_CompanyId = (System.Int32?) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32?));
					sp182_SiteId = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					sp182_Year = (System.Int32?) EntityUtil.ChangeType(values["Year"], typeof(System.Int32?));
					results = KpiCompanySiteCategoryProvider.GetByCompanyIdSiteIdYear(sp182_CompanyId, sp182_SiteId, sp182_Year, StartIndex, PageSize);
					break;
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;
				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}				
			}
			
			return results;
		}
		
		#endregion Methods
	}

	#region KpiCompanySiteCategorySelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the KpiCompanySiteCategoryDataSource.SelectMethod property.
	/// </summary>
	public enum KpiCompanySiteCategorySelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanySiteCategoryId method.
		/// </summary>
		GetByCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteId method.
		/// </summary>
		GetByCompanyIdSiteId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdYearMonth method.
		/// </summary>
		GetByCompanyIdSiteIdYearMonth,
		/// <summary>
		/// Represents the ByMonthYear_CompanySiteCategoryId method.
		/// </summary>
		ByMonthYear_CompanySiteCategoryId,
		/// <summary>
		/// Represents the ByMonthYear method.
		/// </summary>
		ByMonthYear,
		/// <summary>
		/// Represents the Exceptions_NoCompanySiteCategoryId method.
		/// </summary>
		Exceptions_NoCompanySiteCategoryId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteIdYear method.
		/// </summary>
		GetByCompanyIdSiteIdYear
	}
	
	#endregion KpiCompanySiteCategorySelectMethod
	
	#region KpiCompanySiteCategoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiCompanySiteCategoryDataSource class.
	/// </summary>
	public class KpiCompanySiteCategoryDataSourceDesigner : ReadOnlyDataSourceDesigner<KpiCompanySiteCategory>
	{
		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryDataSourceDesigner class.
		/// </summary>
		public KpiCompanySiteCategoryDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public new KpiCompanySiteCategorySelectMethod SelectMethod
		{
			get { return ((KpiCompanySiteCategoryDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new KpiCompanySiteCategoryDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region KpiCompanySiteCategoryDataSourceActionList

	/// <summary>
	/// Supports the KpiCompanySiteCategoryDataSourceDesigner class.
	/// </summary>
	internal class KpiCompanySiteCategoryDataSourceActionList : DesignerActionList
	{
		private KpiCompanySiteCategoryDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the KpiCompanySiteCategoryDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public KpiCompanySiteCategoryDataSourceActionList(KpiCompanySiteCategoryDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiCompanySiteCategorySelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion KpiCompanySiteCategoryDataSourceActionList

	#endregion KpiCompanySiteCategoryDataSourceDesigner

	#region KpiCompanySiteCategoryFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiCompanySiteCategoryFilter : SqlFilter<KpiCompanySiteCategoryColumn>
	{
	}

	#endregion KpiCompanySiteCategoryFilter

	#region KpiCompanySiteCategoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiCompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiCompanySiteCategoryExpressionBuilder : SqlExpressionBuilder<KpiCompanySiteCategoryColumn>
	{
	}
	
	#endregion KpiCompanySiteCategoryExpressionBuilder		
}

