﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Admin_QuestionnaireRationale" Codebehind="QuestionnaireRationale.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v14.1" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<dx:ASPxComboBox ID="cbArea" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SelectedIndex="0" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    ValueType="System.String" OnSelectedIndexChanged="cbArea_SelectedIndexChanged">
    <Items>
        <dx:ListEditItem Selected="True" Text="Supplier" Value="Supplier" />
        <dx:ListEditItem Text="Verification" Value="Verification" />
    </Items>
    <ButtonStyle Width="13px">
    </ButtonStyle>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                    cmbQuestion.PerformCallback(s.GetValue());
                                                                }" />
    <ValidationSettings ValidationGroup="edit">
        <RequiredField IsRequired="True" />
    </ValidationSettings>
</dx:ASPxComboBox>
<dx:ASPxComboBox ID="cbQuestion" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" ClientInstanceName="cmbQuestion" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    ValueType="System.String" Width="700px" IncrementalFilteringMode="Contains" >
    <ButtonStyle Width="13px">
    </ButtonStyle>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <ValidationSettings ValidationGroup="edit">
        <RequiredField IsRequired="True" />
    </ValidationSettings>
</dx:ASPxComboBox>
<dx:ASPxButton ID="btnEdit" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Text="Edit" OnClick="btnEdit_Click" ValidationGroup="edit" CausesValidation="true">
</dx:ASPxButton>
<dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" Visible="False">
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
    <TabPages>
        <dx:TabPage Name="Why" Text="Why">
            <ContentCollection>
                <dx:ContentControl runat="server">
                    <dx:ASPxHtmlEditor ID="ASPxHtmlEditor1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" Visible="False">
                        <SettingsImageUpload UploadImageFolder="~/Uploads/QuestionnaireRationale/">
                            <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png">
                            </ValidationSettings>
                        </SettingsImageUpload>
                        <SettingsSpellChecker>
                            <Dictionaries>
                                <dx:ASPxSpellCheckerOpenOfficeDictionary GrammarPath="~/Common/en_AU.aff" DictionaryPath="~/Common/en_AU.dic">
                                </dx:ASPxSpellCheckerOpenOfficeDictionary>
                            </Dictionaries>
                        </SettingsSpellChecker>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <ViewArea>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ViewArea>
                        </Styles>
                        <StylesRoundPanel>
                            <ControlStyle BackColor="#DDECFE">
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ControlStyle>
                        </StylesRoundPanel>
                        <StylesStatusBar>
                            <ActiveTab BackColor="White">
                            </ActiveTab>
                        </StylesStatusBar>
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <PartsRoundPanel>
                            <HeaderLeftEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderLeftEdge>
                            <HeaderContent>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderContent>
                            <HeaderRightEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderRightEdge>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <TopEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </TopEdge>
                        </PartsRoundPanel>
                    </dx:ASPxHtmlEditor>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
        <dx:TabPage Name="Help" Text="Help with this Question">
            <ContentCollection>
                <dx:ContentControl runat="server">
                    <dx:ASPxHtmlEditor ID="ASPxHtmlEditor2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" Visible="False">
                        <SettingsImageUpload UploadImageFolder="~/Uploads/QuestionnaireRationale/">
                            <ValidationSettings AllowedFileExtensions=".jpg, .jpeg, .gif, .png">
                            </ValidationSettings>
                        </SettingsImageUpload>
                        <SettingsSpellChecker>
                            <Dictionaries>
                                <dx:ASPxSpellCheckerOpenOfficeDictionary GrammarPath="~/Common/en_AU.aff" DictionaryPath="~/Common/en_AU.dic">
                                </dx:ASPxSpellCheckerOpenOfficeDictionary>
                            </Dictionaries>
                        </SettingsSpellChecker>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <ViewArea>
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ViewArea>
                        </Styles>
                        <StylesRoundPanel>
                            <ControlStyle BackColor="#DDECFE">
                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                            </ControlStyle>
                        </StylesRoundPanel>
                        <StylesStatusBar>
                            <ActiveTab BackColor="White">
                            </ActiveTab>
                        </StylesStatusBar>
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <PartsRoundPanel>
                            <HeaderLeftEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderLeftEdge>
                            <HeaderContent>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderContent>
                            <HeaderRightEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpHeader.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </HeaderRightEdge>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <TopEdge>
                                <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/HtmlEditor/RoundPanel/herpTopEdge.png"
                                    Repeat="RepeatX" VerticalPosition="top" />
                            </TopEdge>
                        </PartsRoundPanel>
                    </dx:ASPxHtmlEditor>
                </dx:ContentControl>
            </ContentCollection>
        </dx:TabPage>
    </TabPages>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
</dx:ASPxPageControl>
<p>
    <br />
</p>
<dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Text="Save" OnClick="btnSave_Click" Visible="False">
</dx:ASPxButton>
<dx:ASPxButton ID="btnSaveClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Text="Save & Close" OnClick="btnSaveClose_Click" Visible="False">
</dx:ASPxButton>
<dx:ASPxButton ID="btnClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Text="Close" OnClick="btnClose_Click" Visible="False">
</dx:ASPxButton>
<dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="" ForeColor="Red">
</dx:ASPxLabel>
