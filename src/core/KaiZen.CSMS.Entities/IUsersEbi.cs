﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'UsersEbi' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IUsersEbi 
	{
		/// <summary>			
		/// UsersEbiId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "UsersEbi"</remarks>
		System.Int32 UsersEbiId { get; set; }
				
		
		
		/// <summary>
		/// UserId : 
		/// </summary>
		System.Int32  UserId  { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32  ModifiedByUserId  { get; set; }
		
		/// <summary>
		/// ModifiedDate : 
		/// </summary>
		System.DateTime  ModifiedDate  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


