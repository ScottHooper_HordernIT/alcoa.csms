﻿<%@ Control Language="C#" AutoEventWireup="true"
 CodeBehind="ContractorManagementSafetyDashboard.ascx.cs" Inherits="UserControls_ContractorManagementSafetyDashboard" %>


<%-- <%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>--%>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>



<%@ Register Src="~/UserControls/Other/ExportButtons1.ascx" TagName="ExportButtons1" TagPrefix="uc2" %>
<%--<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.9.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>--%>


<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>




<style type="text/css">
table.gridtable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #666666;
	border-collapse: collapse;
}
table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #dedede;
	width:10px;
}
table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666666;
	background-color: #ffffff;
	width:10px;
}
    .style1
    {
        width: 762px;
    }
    
</style>



    <table border="0" cellpadding="2" cellspacing="0" width="900px">
 

     <tr>
          <td style="height: 43px" width="100%" class="pageName">
                        <span class="bodycopy"><span class="title">Reports</span><br />
                            <span class="date">Contractor Management Safety Dashboard</span>
                            <br />
                            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
                        </span>
                    </td>
                    
     </tr>
     <tr>  </tr>

    <tr>
        <td width="100%">
            <table border="0" width="100%">
                <tr>
                    <td style="width: 6%; text-align: right">
                        <strong>Site:</strong>
                    </td>
                    <td style="text-align: right; width: 18%">
                        <dxe:ASPxComboBox ID="cbRegionSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
                    
                       <td style="width: 8%; text-align: right">
                                                    <strong>Month/Year:</strong>
                                                 </td>
                       <td style="width: 10%; text-align: left">
                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                                                        ValueType="System.Int32" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Width="90px" ID="ddlMonth" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <Items>
                                                            <dx:ListEditItem Text="January" Value="1"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="February" Value="2"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="March" Value="3"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="April" Value="4"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="May" Value="5"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="June" Value="6"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="July" Value="7"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="August" Value="8"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="September" Value="9"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="October" Value="10"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="November" Value="11"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="December" Value="12"></dx:ListEditItem>
                                                        </Items>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>

                 
                   <%-- <td style="width: 16px"/>--%>
                    <td style="text-align: right; width: 8%" >
                        <dxe:ASPxComboBox ID="ddlYear" ClientInstanceName="ddlYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                            Width="60px" AutoPostBack="true" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                            style="margin-left: 0px" 
                            onselectedindexchanged="ddlYear_SelectedIndexChanged">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Cursor="pointer" Width="13px">
                            </ButtonStyle>
                            
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="text-align: left; width:15%" >
                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Text="Go / Refresh" whith="50px"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                            onclick="ASPxButton1_Click" Wrap="False" 
                            >
                        </dxe:ASPxButton>
                    </td>
                    <td align="right"> <asp:Label ID="ValidAsOf" runat="server" ></asp:Label></td>
                    
                </tr>
            </table>

        </td>
    </tr>    

    <tr> <td width="100%"></td></tr>

    <tr>
    
    <td width="100%">
    
     <dxwgv:ASPxGridView ID="grid_Dash"
                ClientInstanceName="grid_Dash"
                runat="server"
                AutoGenerateColumns="False"
               CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                Width="100%" 

                  onhtmlrowcreated="grid_Dash_HtmlRowCreated"
                  
                
                  Border-BorderColor="Black" Border-BorderWidth="1px" 
             >
                <Columns>
                  
                  <dxwgv:GridViewDataTextColumn Caption=" " FieldName="Row1" Name="Row1" VisibleIndex="0" 
                    >
                         <HeaderStyle HorizontalAlign="left"></HeaderStyle>

                        <CellStyle HorizontalAlign="Center" ></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                   
                   <%--<dx:GridViewBandColumn Caption = "Contractor Management Safety Dashboard">
                   <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                   <Columns>--%>
                        <dxwgv:GridViewDataTextColumn  Caption="Contractor" FieldName="Row2" Name="Row2" VisibleIndex="1" 
                        >
                         <HeaderStyle HorizontalAlign="right" Border-BorderStyle="None" Paddings-PaddingRight="2"></HeaderStyle>

                        <CellStyle HorizontalAlign="Center" ></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
        
                     <dxwgv:GridViewDataTextColumn Caption="Management" FieldName="Management" Name="Management" VisibleIndex="2"
                     >
                            <HeaderStyle HorizontalAlign="center" Paddings-PaddingLeft="0"  Border-BorderStyle="None" ></HeaderStyle>

                            
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Safety   DashBoard" FieldName="FieldName" Name="FieldName" VisibleIndex="3" width="300px">
                            <HeaderStyle HorizontalAlign="left" Paddings-PaddingLeft="0" ></HeaderStyle>

                         
                    </dxwgv:GridViewDataTextColumn>
                    <%--</Columns>
                    </dx:GridViewBandColumn>--%>

                     <dxwgv:GridViewDataTextColumn Caption="Plan" FieldName="Plan" Name="Plan" VisibleIndex="4"
                     >
                         <HeaderStyle HorizontalAlign="center"  Wrap="True"></HeaderStyle>

                        <CellStyle HorizontalAlign="center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                   
                        <dxwgv:GridViewDataTextColumn Caption="AUS GPP" FieldName="AUS GPP" Name="AUS GPP" VisibleIndex="5" ExportWidth="68" 
                      >
                         <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                        <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
        
                     <dxwgv:GridViewDataTextColumn Caption="WAO" FieldName="WAO" Name ="WAO" VisibleIndex="6" ExportWidth="68"
                     >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                                                    
                     <dxwgv:GridViewDataTextColumn Caption="Kwinana" FieldName="Kwinana" Name="Kwinana" VisibleIndex="7" ExportWidth="68" 
                       >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                     <dxwgv:GridViewDataTextColumn Caption="Pinjarra" FieldName="Pinjara" Name="Pinjara" VisibleIndex="8" ExportWidth="68"
                     >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                                                    
                     <dxwgv:GridViewDataTextColumn Caption="Wagerup" FieldName="Wagerup" Name="Wagerup" VisibleIndex="9" ExportWidth="68"
                       >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                     <dxwgv:GridViewDataTextColumn Caption="Bunbury Port" FieldName="Bunbury Port" Name="Bunbury Port" VisibleIndex="10" ExportWidth="68"
                      >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                                                 
                   <dx:GridViewBandColumn Caption = "Mining" VisibleIndex="10">
                   <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                   <Columns>
                     <dxwgv:GridViewDataTextColumn Caption="Huntly (Mine)" FieldName="Huntly (Mine)" Name="Huntly (Mine)" VisibleIndex="11" ExportWidth="68"
                       >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    

                     <dxwgv:GridViewDataTextColumn Caption="Willowdale (Mine)" FieldName="Willowdale (Mine)" Name="Willowdale (Mine)" VisibleIndex="12" ExportWidth="68"
                        >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                     <dxwgv:GridViewDataTextColumn Caption="FML" FieldName="FML" Name="FML" VisibleIndex="13" ExportWidth="68" 
                    >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    
                    </Columns>                                                             
                    </dx:GridViewBandColumn> 
                    
                     <dxwgv:GridViewDataTextColumn Caption="BGN" FieldName="BGN" Name="BGN" VisibleIndex="14" ExportWidth="68" 
                     >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                   
                            

                 <dxwgv:GridViewDataTextColumn Caption="Peel" FieldName="Peel" Name="Peel" VisibleIndex="15" ExportWidth="68"
                      >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                     <dxwgv:GridViewDataTextColumn Caption="VIC OPS" FieldName="VIC OPS" Name="VIC OPS" VisibleIndex="16" ExportWidth="68"
                    >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                                                    
                     <dxwgv:GridViewDataTextColumn Caption="Anglesea" FieldName="Anglesea" Name="Anglesea" VisibleIndex="17" ExportWidth="68"
                       >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                    


                    <dxwgv:GridViewDataTextColumn Caption="Point Henry" FieldName="Point Henry" Name="Point Henry" VisibleIndex="18" ExportWidth="68"
                       >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                     <dxwgv:GridViewDataTextColumn Caption="Portland" FieldName="Portland" Name="Portland" VisibleIndex="19" ExportWidth="68"
                     >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                                                    
                     <dxwgv:GridViewDataTextColumn Caption="ARP" FieldName="ARP"  Name="ARP" VisibleIndex="20" ExportWidth="68"
                     >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                   


                   <dxwgv:GridViewDataTextColumn Caption="ARP Point Henry" FieldName="ARP Point Henry" Name="ARP Point Henry" VisibleIndex="21" ExportWidth="68"
                      >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center" ></CellStyle>
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataTextColumn Caption="Yennora" FieldName="Yennora" Name="Yennora" VisibleIndex="22" ExportWidth="68"
                     >
                            <HeaderStyle HorizontalAlign="Center"  Wrap="True"></HeaderStyle>

                            <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                                                    
       

                                                    
       
    
                </Columns>
                
               <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <SettingsPager Mode="ShowAllRecords">
                </SettingsPager>
                <Settings ShowGroupedColumns="True" ShowHorizontalScrollBar="True" 
                    ShowVerticalScrollBar="True" VerticalScrollableHeight="500" />
              
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px" >
                    </Header>
                 
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True"/>
               
                

                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>

                 
<Border BorderColor="Black" BorderWidth="1px"></Border>

                 
            </dxwgv:ASPxGridView>
   </td>
   </tr>  
    
</table>
<div style="width:100%" align="right"><uc2:ExportButtons1 Visible="true" ID="ucExportButtons1" runat="server" /></div>





    

<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    EnableHotTrack="False" HeaderText="Warning" Height="111px" Modal="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" Width="439px">
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
            SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" ID="ASPxLabel1" Font-Size="14px" Text=""
                Height="30px">
                <Border BorderWidth="10px" BorderColor="White"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>