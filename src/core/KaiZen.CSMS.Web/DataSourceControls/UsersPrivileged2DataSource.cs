﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersPrivileged2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UsersPrivileged2DataSourceDesigner))]
	public class UsersPrivileged2DataSource : ProviderDataSource<UsersPrivileged2, UsersPrivileged2Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2DataSource class.
		/// </summary>
		public UsersPrivileged2DataSource() : base(new UsersPrivileged2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersPrivileged2DataSourceView used by the UsersPrivileged2DataSource.
		/// </summary>
		protected UsersPrivileged2DataSourceView UsersPrivileged2View
		{
			get { return ( View as UsersPrivileged2DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersPrivileged2DataSource control invokes to retrieve data.
		/// </summary>
		public UsersPrivileged2SelectMethod SelectMethod
		{
			get
			{
				UsersPrivileged2SelectMethod selectMethod = UsersPrivileged2SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersPrivileged2SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersPrivileged2DataSourceView class that is to be
		/// used by the UsersPrivileged2DataSource.
		/// </summary>
		/// <returns>An instance of the UsersPrivileged2DataSourceView class.</returns>
		protected override BaseDataSourceView<UsersPrivileged2, UsersPrivileged2Key> GetNewDataSourceView()
		{
			return new UsersPrivileged2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersPrivileged2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersPrivileged2DataSourceView : ProviderDataSourceView<UsersPrivileged2, UsersPrivileged2Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersPrivileged2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersPrivileged2DataSourceView(UsersPrivileged2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersPrivileged2DataSource UsersPrivileged2Owner
		{
			get { return Owner as UsersPrivileged2DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UsersPrivileged2SelectMethod SelectMethod
		{
			get { return UsersPrivileged2Owner.SelectMethod; }
			set { UsersPrivileged2Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersPrivileged2Service UsersPrivileged2Provider
		{
			get { return Provider as UsersPrivileged2Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersPrivileged2> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UsersPrivileged2> results = null;
			UsersPrivileged2 item;
			count = 0;
			
			System.Int32 _usersPrivilegedId;
			System.Int32 _userId;

			switch ( SelectMethod )
			{
				case UsersPrivileged2SelectMethod.Get:
					UsersPrivileged2Key entityKey  = new UsersPrivileged2Key();
					entityKey.Load(values);
					item = UsersPrivileged2Provider.Get(entityKey);
					results = new TList<UsersPrivileged2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UsersPrivileged2SelectMethod.GetAll:
                    results = UsersPrivileged2Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersPrivileged2SelectMethod.GetPaged:
					results = UsersPrivileged2Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UsersPrivileged2SelectMethod.Find:
					if ( FilterParameters != null )
						results = UsersPrivileged2Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UsersPrivileged2Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UsersPrivileged2SelectMethod.GetByUsersPrivilegedId:
					_usersPrivilegedId = ( values["UsersPrivilegedId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UsersPrivilegedId"], typeof(System.Int32)) : (int)0;
					item = UsersPrivileged2Provider.GetByUsersPrivilegedId(_usersPrivilegedId);
					results = new TList<UsersPrivileged2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case UsersPrivileged2SelectMethod.GetByUserId:
					_userId = ( values["UserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["UserId"], typeof(System.Int32)) : (int)0;
					item = UsersPrivileged2Provider.GetByUserId(_userId);
					results = new TList<UsersPrivileged2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UsersPrivileged2SelectMethod.Get || SelectMethod == UsersPrivileged2SelectMethod.GetByUsersPrivilegedId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UsersPrivileged2 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UsersPrivileged2Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UsersPrivileged2> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UsersPrivileged2Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UsersPrivileged2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersPrivileged2DataSource class.
	/// </summary>
	public class UsersPrivileged2DataSourceDesigner : ProviderDataSourceDesigner<UsersPrivileged2, UsersPrivileged2Key>
	{
		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2DataSourceDesigner class.
		/// </summary>
		public UsersPrivileged2DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersPrivileged2SelectMethod SelectMethod
		{
			get { return ((UsersPrivileged2DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersPrivileged2DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersPrivileged2DataSourceActionList

	/// <summary>
	/// Supports the UsersPrivileged2DataSourceDesigner class.
	/// </summary>
	internal class UsersPrivileged2DataSourceActionList : DesignerActionList
	{
		private UsersPrivileged2DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersPrivileged2DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersPrivileged2DataSourceActionList(UsersPrivileged2DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersPrivileged2SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersPrivileged2DataSourceActionList
	
	#endregion UsersPrivileged2DataSourceDesigner
	
	#region UsersPrivileged2SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersPrivileged2DataSource.SelectMethod property.
	/// </summary>
	public enum UsersPrivileged2SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByUsersPrivilegedId method.
		/// </summary>
		GetByUsersPrivilegedId,
		/// <summary>
		/// Represents the GetByUserId method.
		/// </summary>
		GetByUserId
	}
	
	#endregion UsersPrivileged2SelectMethod

	#region UsersPrivileged2Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged2Filter : SqlFilter<UsersPrivileged2Column>
	{
	}
	
	#endregion UsersPrivileged2Filter

	#region UsersPrivileged2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged2ExpressionBuilder : SqlExpressionBuilder<UsersPrivileged2Column>
	{
	}
	
	#endregion UsersPrivileged2ExpressionBuilder	

	#region UsersPrivileged2Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UsersPrivileged2ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UsersPrivileged2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersPrivileged2Property : ChildEntityProperty<UsersPrivileged2ChildEntityTypes>
	{
	}
	
	#endregion UsersPrivileged2Property
}

