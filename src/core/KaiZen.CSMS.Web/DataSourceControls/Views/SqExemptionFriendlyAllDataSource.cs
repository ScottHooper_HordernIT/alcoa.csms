﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SqExemptionFriendlyAllProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(SqExemptionFriendlyAllDataSourceDesigner))]
	public class SqExemptionFriendlyAllDataSource : ReadOnlyDataSource<SqExemptionFriendlyAll>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllDataSource class.
		/// </summary>
		public SqExemptionFriendlyAllDataSource() : base(new SqExemptionFriendlyAllService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SqExemptionFriendlyAllDataSourceView used by the SqExemptionFriendlyAllDataSource.
		/// </summary>
		protected SqExemptionFriendlyAllDataSourceView SqExemptionFriendlyAllView
		{
			get { return ( View as SqExemptionFriendlyAllDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SqExemptionFriendlyAllDataSourceView class that is to be
		/// used by the SqExemptionFriendlyAllDataSource.
		/// </summary>
		/// <returns>An instance of the SqExemptionFriendlyAllDataSourceView class.</returns>
		protected override BaseDataSourceView<SqExemptionFriendlyAll, Object> GetNewDataSourceView()
		{
			return new SqExemptionFriendlyAllDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SqExemptionFriendlyAllDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SqExemptionFriendlyAllDataSourceView : ReadOnlyDataSourceView<SqExemptionFriendlyAll>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyAllDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SqExemptionFriendlyAllDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SqExemptionFriendlyAllDataSourceView(SqExemptionFriendlyAllDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SqExemptionFriendlyAllDataSource SqExemptionFriendlyAllOwner
		{
			get { return Owner as SqExemptionFriendlyAllDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SqExemptionFriendlyAllService SqExemptionFriendlyAllProvider
		{
			get { return Provider as SqExemptionFriendlyAllService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region SqExemptionFriendlyAllDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SqExemptionFriendlyAllDataSource class.
	/// </summary>
	public class SqExemptionFriendlyAllDataSourceDesigner : ReadOnlyDataSourceDesigner<SqExemptionFriendlyAll>
	{
	}

	#endregion SqExemptionFriendlyAllDataSourceDesigner

	#region SqExemptionFriendlyAllFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyAllFilter : SqlFilter<SqExemptionFriendlyAllColumn>
	{
	}

	#endregion SqExemptionFriendlyAllFilter

	#region SqExemptionFriendlyAllExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyAllExpressionBuilder : SqlExpressionBuilder<SqExemptionFriendlyAllColumn>
	{
	}
	
	#endregion SqExemptionFriendlyAllExpressionBuilder		
}

