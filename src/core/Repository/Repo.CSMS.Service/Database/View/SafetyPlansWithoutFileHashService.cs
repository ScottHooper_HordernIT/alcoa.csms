﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface ISafetyPlansWithoutFileHashService
    {
        SafetyPlansWithoutFileHash Get(Expression<Func<SafetyPlansWithoutFileHash, bool>> where, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes);
        List<SafetyPlansWithoutFileHash> GetMany(int? top, Expression<Func<SafetyPlansWithoutFileHash, bool>> where, Func<IQueryable<SafetyPlansWithoutFileHash>, IOrderedQueryable<SafetyPlansWithoutFileHash>> orderBy, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes);
        List<SafetyPlansWithoutFileHash> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SafetyPlansWithoutFileHash, bool>> where, Func<IQueryable<SafetyPlansWithoutFileHash>, IOrderedQueryable<SafetyPlansWithoutFileHash>> orderBy, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<SafetyPlansWithoutFileHash, bool>> where, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes);
    }
    #endregion

    #region Interface Implementation

    public class SafetyPlansWithoutFileHashService : ISafetyPlansWithoutFileHashService
    {
        private readonly IRepository<SafetyPlansWithoutFileHash> _repository;

        public SafetyPlansWithoutFileHashService(IRepository<SafetyPlansWithoutFileHash> repository)
        {
            this._repository = repository;
        }

        public SafetyPlansWithoutFileHash Get(Expression<Func<SafetyPlansWithoutFileHash, bool>> where, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<SafetyPlansWithoutFileHash> GetMany(int? top, Expression<Func<SafetyPlansWithoutFileHash, bool>> where, Func<IQueryable<SafetyPlansWithoutFileHash>, IOrderedQueryable<SafetyPlansWithoutFileHash>> orderBy, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<SafetyPlansWithoutFileHash> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SafetyPlansWithoutFileHash, bool>> where, Func<IQueryable<SafetyPlansWithoutFileHash>, IOrderedQueryable<SafetyPlansWithoutFileHash>> orderBy, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<SafetyPlansWithoutFileHash, bool>> where, List<Expression<Func<SafetyPlansWithoutFileHash, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }
    }

    #endregion
}
