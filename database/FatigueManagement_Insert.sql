USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[FatigueManagement_Insert]    Script Date: 21/07/2015 10:19:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FatigueManagement_Insert] (
	@ClockId INT
	,@TotalTime INT
	,@EntryDateTime DATETIME
	,@FullName VARCHAR(255)
	,@Vendor_Number VARCHAR(50)
	,@RollingWeekHours INT
	,@Comments VARCHAR(max)
	,@SiteId INT
	,@result INT OUTPUT
	)
AS
SET @result = 0

IF NOT EXISTS (
		SELECT *
		FROM FatigueManagementDetails
		WHERE ClockId = @ClockId
			AND DateDiff(dd, EntryDateTime, @EntryDateTime) = 0
		)
BEGIN
	INSERT INTO [dbo].[FatigueManagementDetails] (
		[ClockId]
		,[TotalTime]
		,[EntryDateTime]
		,[FullName]
		,[Vendor_Number]
		,[RollingWeekHours]
		,[Comments]
		,[SiteId]
		)
	VALUES (
		@ClockId
		,@TotalTime
		,@EntryDateTime
		,@FullName
		,@Vendor_Number
		,@RollingWeekHours
		,@Comments
		,@SiteId
		)

	SET @result = 1
END
		---------------------------------------- 
GO


