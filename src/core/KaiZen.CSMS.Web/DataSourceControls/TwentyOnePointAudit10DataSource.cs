﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit10Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit10DataSourceDesigner))]
	public class TwentyOnePointAudit10DataSource : ProviderDataSource<TwentyOnePointAudit10, TwentyOnePointAudit10Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10DataSource class.
		/// </summary>
		public TwentyOnePointAudit10DataSource() : base(new TwentyOnePointAudit10Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit10DataSourceView used by the TwentyOnePointAudit10DataSource.
		/// </summary>
		protected TwentyOnePointAudit10DataSourceView TwentyOnePointAudit10View
		{
			get { return ( View as TwentyOnePointAudit10DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit10DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit10SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit10SelectMethod selectMethod = TwentyOnePointAudit10SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit10SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit10DataSourceView class that is to be
		/// used by the TwentyOnePointAudit10DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit10DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit10, TwentyOnePointAudit10Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit10DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit10DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit10DataSourceView : ProviderDataSourceView<TwentyOnePointAudit10, TwentyOnePointAudit10Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit10DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit10DataSourceView(TwentyOnePointAudit10DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit10DataSource TwentyOnePointAudit10Owner
		{
			get { return Owner as TwentyOnePointAudit10DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit10SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit10Owner.SelectMethod; }
			set { TwentyOnePointAudit10Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit10Service TwentyOnePointAudit10Provider
		{
			get { return Provider as TwentyOnePointAudit10Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit10> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit10> results = null;
			TwentyOnePointAudit10 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit10SelectMethod.Get:
					TwentyOnePointAudit10Key entityKey  = new TwentyOnePointAudit10Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit10Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit10>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit10SelectMethod.GetAll:
                    results = TwentyOnePointAudit10Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit10SelectMethod.GetPaged:
					results = TwentyOnePointAudit10Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit10SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit10Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit10Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit10SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit10Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit10>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit10SelectMethod.Get || SelectMethod == TwentyOnePointAudit10SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit10 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit10Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit10> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit10Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit10DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit10DataSource class.
	/// </summary>
	public class TwentyOnePointAudit10DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit10, TwentyOnePointAudit10Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit10DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit10SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit10DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit10DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit10DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit10DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit10DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit10DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit10DataSourceActionList(TwentyOnePointAudit10DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit10SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit10DataSourceActionList
	
	#endregion TwentyOnePointAudit10DataSourceDesigner
	
	#region TwentyOnePointAudit10SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit10DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit10SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit10SelectMethod

	#region TwentyOnePointAudit10Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10Filter : SqlFilter<TwentyOnePointAudit10Column>
	{
	}
	
	#endregion TwentyOnePointAudit10Filter

	#region TwentyOnePointAudit10ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit10Column>
	{
	}
	
	#endregion TwentyOnePointAudit10ExpressionBuilder	

	#region TwentyOnePointAudit10Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit10ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10Property : ChildEntityProperty<TwentyOnePointAudit10ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit10Property
}

