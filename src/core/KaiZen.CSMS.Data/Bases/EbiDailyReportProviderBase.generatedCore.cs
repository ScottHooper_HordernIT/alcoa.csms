﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiDailyReportProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EbiDailyReportProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EbiDailyReport, KaiZen.CSMS.Entities.EbiDailyReportKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiDailyReportKey key)
		{
			return Delete(transactionManager, key.EbiDailyReportId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ebiDailyReportId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ebiDailyReportId)
		{
			return Delete(null, _ebiDailyReportId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiDailyReportId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ebiDailyReportId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiDailyReport_EbiIssue key.
		///		FK_EbiDailyReport_EbiIssue Description: 
		/// </summary>
		/// <param name="_ebiIssueId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiDailyReport objects.</returns>
		public TList<EbiDailyReport> GetByEbiIssueId(System.Int32 _ebiIssueId)
		{
			int count = -1;
			return GetByEbiIssueId(_ebiIssueId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiDailyReport_EbiIssue key.
		///		FK_EbiDailyReport_EbiIssue Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiDailyReport objects.</returns>
		/// <remarks></remarks>
		public TList<EbiDailyReport> GetByEbiIssueId(TransactionManager transactionManager, System.Int32 _ebiIssueId)
		{
			int count = -1;
			return GetByEbiIssueId(transactionManager, _ebiIssueId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiDailyReport_EbiIssue key.
		///		FK_EbiDailyReport_EbiIssue Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiDailyReport objects.</returns>
		public TList<EbiDailyReport> GetByEbiIssueId(TransactionManager transactionManager, System.Int32 _ebiIssueId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiIssueId(transactionManager, _ebiIssueId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiDailyReport_EbiIssue key.
		///		fkEbiDailyReportEbiIssue Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ebiIssueId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiDailyReport objects.</returns>
		public TList<EbiDailyReport> GetByEbiIssueId(System.Int32 _ebiIssueId, int start, int pageLength)
		{
			int count =  -1;
			return GetByEbiIssueId(null, _ebiIssueId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiDailyReport_EbiIssue key.
		///		fkEbiDailyReportEbiIssue Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ebiIssueId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiDailyReport objects.</returns>
		public TList<EbiDailyReport> GetByEbiIssueId(System.Int32 _ebiIssueId, int start, int pageLength,out int count)
		{
			return GetByEbiIssueId(null, _ebiIssueId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EbiDailyReport_EbiIssue key.
		///		FK_EbiDailyReport_EbiIssue Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiIssueId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EbiDailyReport objects.</returns>
		public abstract TList<EbiDailyReport> GetByEbiIssueId(TransactionManager transactionManager, System.Int32 _ebiIssueId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EbiDailyReport Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiDailyReportKey key, int start, int pageLength)
		{
			return GetByEbiDailyReportId(transactionManager, key.EbiDailyReportId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EbiDailyReport index.
		/// </summary>
		/// <param name="_ebiDailyReportId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiDailyReport GetByEbiDailyReportId(System.Int32 _ebiDailyReportId)
		{
			int count = -1;
			return GetByEbiDailyReportId(null,_ebiDailyReportId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiDailyReport index.
		/// </summary>
		/// <param name="_ebiDailyReportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiDailyReport GetByEbiDailyReportId(System.Int32 _ebiDailyReportId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiDailyReportId(null, _ebiDailyReportId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiDailyReport index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiDailyReportId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiDailyReport GetByEbiDailyReportId(TransactionManager transactionManager, System.Int32 _ebiDailyReportId)
		{
			int count = -1;
			return GetByEbiDailyReportId(transactionManager, _ebiDailyReportId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiDailyReport index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiDailyReportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiDailyReport GetByEbiDailyReportId(TransactionManager transactionManager, System.Int32 _ebiDailyReportId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiDailyReportId(transactionManager, _ebiDailyReportId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiDailyReport index.
		/// </summary>
		/// <param name="_ebiDailyReportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> class.</returns>
		public KaiZen.CSMS.Entities.EbiDailyReport GetByEbiDailyReportId(System.Int32 _ebiDailyReportId, int start, int pageLength, out int count)
		{
			return GetByEbiDailyReportId(null, _ebiDailyReportId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EbiDailyReport index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiDailyReportId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EbiDailyReport GetByEbiDailyReportId(TransactionManager transactionManager, System.Int32 _ebiDailyReportId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EbiDailyReport_DateTime index.
		/// </summary>
		/// <param name="_dateTime"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EbiDailyReport&gt;"/> class.</returns>
		public TList<EbiDailyReport> GetByDateTime(System.DateTime _dateTime)
		{
			int count = -1;
			return GetByDateTime(null,_dateTime, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EbiDailyReport_DateTime index.
		/// </summary>
		/// <param name="_dateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EbiDailyReport&gt;"/> class.</returns>
		public TList<EbiDailyReport> GetByDateTime(System.DateTime _dateTime, int start, int pageLength)
		{
			int count = -1;
			return GetByDateTime(null, _dateTime, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EbiDailyReport_DateTime index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_dateTime"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EbiDailyReport&gt;"/> class.</returns>
		public TList<EbiDailyReport> GetByDateTime(TransactionManager transactionManager, System.DateTime _dateTime)
		{
			int count = -1;
			return GetByDateTime(transactionManager, _dateTime, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EbiDailyReport_DateTime index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_dateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EbiDailyReport&gt;"/> class.</returns>
		public TList<EbiDailyReport> GetByDateTime(TransactionManager transactionManager, System.DateTime _dateTime, int start, int pageLength)
		{
			int count = -1;
			return GetByDateTime(transactionManager, _dateTime, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EbiDailyReport_DateTime index.
		/// </summary>
		/// <param name="_dateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EbiDailyReport&gt;"/> class.</returns>
		public TList<EbiDailyReport> GetByDateTime(System.DateTime _dateTime, int start, int pageLength, out int count)
		{
			return GetByDateTime(null, _dateTime, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EbiDailyReport_DateTime index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_dateTime"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EbiDailyReport&gt;"/> class.</returns>
		public abstract TList<EbiDailyReport> GetByDateTime(TransactionManager transactionManager, System.DateTime _dateTime, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EbiDailyReport&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EbiDailyReport&gt;"/></returns>
		public static TList<EbiDailyReport> Fill(IDataReader reader, TList<EbiDailyReport> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EbiDailyReport c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EbiDailyReport")
					.Append("|").Append((System.Int32)reader[((int)EbiDailyReportColumn.EbiDailyReportId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EbiDailyReport>(
					key.ToString(), // EntityTrackingKey
					"EbiDailyReport",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EbiDailyReport();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EbiDailyReportId = (System.Int32)reader[((int)EbiDailyReportColumn.EbiDailyReportId - 1)];
					c.DateTime = (System.DateTime)reader[((int)EbiDailyReportColumn.DateTime - 1)];
					c.CompanyName = (System.String)reader[((int)EbiDailyReportColumn.CompanyName - 1)];
					c.CompanyNameCsms = (System.String)reader[((int)EbiDailyReportColumn.CompanyNameCsms - 1)];
					c.SiteName = (System.String)reader[((int)EbiDailyReportColumn.SiteName - 1)];
					c.ApprovedOnSite = (System.String)reader[((int)EbiDailyReportColumn.ApprovedOnSite - 1)];
					c.EbiIssueId = (System.Int32)reader[((int)EbiDailyReportColumn.EbiIssueId - 1)];
					c.SqValidTill = (reader.IsDBNull(((int)EbiDailyReportColumn.SqValidTill - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.SqValidTill - 1)];
					c.CompanySqStatus = (reader.IsDBNull(((int)EbiDailyReportColumn.CompanySqStatus - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.CompanySqStatus - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)EbiDailyReportColumn.QuestionnaireId - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.QuestionnaireId - 1)];
					c.Validity = (reader.IsDBNull(((int)EbiDailyReportColumn.Validity - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.Validity - 1)];
					c.Type = (reader.IsDBNull(((int)EbiDailyReportColumn.Type - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.Type - 1)];
					c.ProcurementContact = (reader.IsDBNull(((int)EbiDailyReportColumn.ProcurementContact - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.ProcurementContact - 1)];
					c.HsAssessor = (reader.IsDBNull(((int)EbiDailyReportColumn.HsAssessor - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.HsAssessor - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EbiDailyReport entity)
		{
			if (!reader.Read()) return;
			
			entity.EbiDailyReportId = (System.Int32)reader[((int)EbiDailyReportColumn.EbiDailyReportId - 1)];
			entity.DateTime = (System.DateTime)reader[((int)EbiDailyReportColumn.DateTime - 1)];
			entity.CompanyName = (System.String)reader[((int)EbiDailyReportColumn.CompanyName - 1)];
			entity.CompanyNameCsms = (System.String)reader[((int)EbiDailyReportColumn.CompanyNameCsms - 1)];
			entity.SiteName = (System.String)reader[((int)EbiDailyReportColumn.SiteName - 1)];
			entity.ApprovedOnSite = (System.String)reader[((int)EbiDailyReportColumn.ApprovedOnSite - 1)];
			entity.EbiIssueId = (System.Int32)reader[((int)EbiDailyReportColumn.EbiIssueId - 1)];
			entity.SqValidTill = (reader.IsDBNull(((int)EbiDailyReportColumn.SqValidTill - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.SqValidTill - 1)];
			entity.CompanySqStatus = (reader.IsDBNull(((int)EbiDailyReportColumn.CompanySqStatus - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.CompanySqStatus - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)EbiDailyReportColumn.QuestionnaireId - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.QuestionnaireId - 1)];
			entity.Validity = (reader.IsDBNull(((int)EbiDailyReportColumn.Validity - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.Validity - 1)];
			entity.Type = (reader.IsDBNull(((int)EbiDailyReportColumn.Type - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.Type - 1)];
			entity.ProcurementContact = (reader.IsDBNull(((int)EbiDailyReportColumn.ProcurementContact - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.ProcurementContact - 1)];
			entity.HsAssessor = (reader.IsDBNull(((int)EbiDailyReportColumn.HsAssessor - 1)))?null:(System.String)reader[((int)EbiDailyReportColumn.HsAssessor - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EbiDailyReport entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EbiDailyReportId = (System.Int32)dataRow["EbiDailyReportId"];
			entity.DateTime = (System.DateTime)dataRow["DateTime"];
			entity.CompanyName = (System.String)dataRow["CompanyName"];
			entity.CompanyNameCsms = (System.String)dataRow["CompanyNameCsms"];
			entity.SiteName = (System.String)dataRow["SiteName"];
			entity.ApprovedOnSite = (System.String)dataRow["ApprovedOnSite"];
			entity.EbiIssueId = (System.Int32)dataRow["EbiIssueId"];
			entity.SqValidTill = Convert.IsDBNull(dataRow["SqValidTill"]) ? null : (System.String)dataRow["SqValidTill"];
			entity.CompanySqStatus = Convert.IsDBNull(dataRow["CompanySqStatus"]) ? null : (System.String)dataRow["CompanySqStatus"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.String)dataRow["QuestionnaireId"];
			entity.Validity = Convert.IsDBNull(dataRow["Validity"]) ? null : (System.String)dataRow["Validity"];
			entity.Type = Convert.IsDBNull(dataRow["Type"]) ? null : (System.String)dataRow["Type"];
			entity.ProcurementContact = Convert.IsDBNull(dataRow["ProcurementContact"]) ? null : (System.String)dataRow["ProcurementContact"];
			entity.HsAssessor = Convert.IsDBNull(dataRow["HSAssessor"]) ? null : (System.String)dataRow["HSAssessor"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EbiDailyReport"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EbiDailyReport Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiDailyReport entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region EbiIssueIdSource	
			if (CanDeepLoad(entity, "EbiIssue|EbiIssueIdSource", deepLoadType, innerList) 
				&& entity.EbiIssueIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.EbiIssueId;
				EbiIssue tmpEntity = EntityManager.LocateEntity<EbiIssue>(EntityLocator.ConstructKeyFromPkItems(typeof(EbiIssue), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.EbiIssueIdSource = tmpEntity;
				else
					entity.EbiIssueIdSource = DataRepository.EbiIssueProvider.GetByEbiIssueId(transactionManager, entity.EbiIssueId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EbiIssueIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.EbiIssueIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.EbiIssueProvider.DeepLoad(transactionManager, entity.EbiIssueIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion EbiIssueIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EbiDailyReport object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EbiDailyReport instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EbiDailyReport Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiDailyReport entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region EbiIssueIdSource
			if (CanDeepSave(entity, "EbiIssue|EbiIssueIdSource", deepSaveType, innerList) 
				&& entity.EbiIssueIdSource != null)
			{
				DataRepository.EbiIssueProvider.Save(transactionManager, entity.EbiIssueIdSource);
				entity.EbiIssueId = entity.EbiIssueIdSource.EbiIssueId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EbiDailyReportChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EbiDailyReport</c>
	///</summary>
	public enum EbiDailyReportChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>EbiIssue</c> at EbiIssueIdSource
		///</summary>
		[ChildEntityType(typeof(EbiIssue))]
		EbiIssue,
		}
	
	#endregion EbiDailyReportChildEntityTypes
	
	#region EbiDailyReportFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EbiDailyReportColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportFilterBuilder : SqlFilterBuilder<EbiDailyReportColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportFilterBuilder class.
		/// </summary>
		public EbiDailyReportFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiDailyReportFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiDailyReportFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiDailyReportFilterBuilder
	
	#region EbiDailyReportParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EbiDailyReportColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiDailyReportParameterBuilder : ParameterizedSqlFilterBuilder<EbiDailyReportColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportParameterBuilder class.
		/// </summary>
		public EbiDailyReportParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiDailyReportParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiDailyReportParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiDailyReportParameterBuilder
	
	#region EbiDailyReportSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EbiDailyReportColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiDailyReport"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiDailyReportSortBuilder : SqlSortBuilder<EbiDailyReportColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiDailyReportSqlSortBuilder class.
		/// </summary>
		public EbiDailyReportSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiDailyReportSortBuilder
	
} // end namespace
