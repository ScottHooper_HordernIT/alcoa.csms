﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnairePresentlyWithUsersActionsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnairePresentlyWithUsersActionsProviderBaseCore : EntityViewProviderBase<QuestionnairePresentlyWithUsersActions>
	{
		#region Custom Methods
		
		
		#region _QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId' stored procedure. 
		/// </summary>
		/// <param name="actionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo_ByActionId(System.Int32? actionId)
		{
			return OrderByProcessNo_ByActionId(null, 0, int.MaxValue , actionId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId' stored procedure. 
		/// </summary>
		/// <param name="actionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo_ByActionId(int start, int pageLength, System.Int32? actionId)
		{
			return OrderByProcessNo_ByActionId(null, start, pageLength , actionId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId' stored procedure. 
		/// </summary>
		/// <param name="actionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo_ByActionId(TransactionManager transactionManager, System.Int32? actionId)
		{
			return OrderByProcessNo_ByActionId(transactionManager, 0, int.MaxValue , actionId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByActionId' stored procedure. 
		/// </summary>
		/// <param name="actionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet OrderByProcessNo_ByActionId(TransactionManager transactionManager, int start, int pageLength, System.Int32? actionId);
		
		#endregion

		
		#region _QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo_ByUserId(System.Int32? userId)
		{
			return OrderByProcessNo_ByUserId(null, 0, int.MaxValue , userId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo_ByUserId(int start, int pageLength, System.Int32? userId)
		{
			return OrderByProcessNo_ByUserId(null, start, pageLength , userId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo_ByUserId(TransactionManager transactionManager, System.Int32? userId)
		{
			return OrderByProcessNo_ByUserId(transactionManager, 0, int.MaxValue , userId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo_ByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet OrderByProcessNo_ByUserId(TransactionManager transactionManager, int start, int pageLength, System.Int32? userId);
		
		#endregion

		
		#region _QuestionnairePresentlyWithUsersActions_OrderByProcessNo
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo()
		{
			return OrderByProcessNo(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo(int start, int pageLength)
		{
			return OrderByProcessNo(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet OrderByProcessNo(TransactionManager transactionManager)
		{
			return OrderByProcessNo(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnairePresentlyWithUsersActions_OrderByProcessNo' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet OrderByProcessNo(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnairePresentlyWithUsersActions&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnairePresentlyWithUsersActions&gt;"/></returns>
		protected static VList&lt;QuestionnairePresentlyWithUsersActions&gt; Fill(DataSet dataSet, VList<QuestionnairePresentlyWithUsersActions> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnairePresentlyWithUsersActions>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnairePresentlyWithUsersActions&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnairePresentlyWithUsersActions>"/></returns>
		protected static VList&lt;QuestionnairePresentlyWithUsersActions&gt; Fill(DataTable dataTable, VList<QuestionnairePresentlyWithUsersActions> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnairePresentlyWithUsersActions c = new QuestionnairePresentlyWithUsersActions();
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(row["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32)row["QuestionnairePresentlyWithUserId"];
					c.ProcessNo = (Convert.IsDBNull(row["ProcessNo"]))?(int)0:(System.Int32?)row["ProcessNo"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.ActionName = (Convert.IsDBNull(row["ActionName"]))?string.Empty:(System.String)row["ActionName"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnairePresentlyWithUsersActions&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnairePresentlyWithUsersActions&gt;"/></returns>
		protected VList<QuestionnairePresentlyWithUsersActions> Fill(IDataReader reader, VList<QuestionnairePresentlyWithUsersActions> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnairePresentlyWithUsersActions entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnairePresentlyWithUsersActions>("QuestionnairePresentlyWithUsersActions",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnairePresentlyWithUsersActions();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnairePresentlyWithActionId = (System.Int32)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.QuestionnairePresentlyWithUserId)];
					//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32)reader["QuestionnairePresentlyWithUserId"];
					entity.ProcessNo = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersActionsColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.ProcessNo)];
					//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
					entity.UserName = (System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.ActionName = (System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.ActionName)];
					//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersActionsColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersActionsColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnairePresentlyWithUsersActions"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnairePresentlyWithUsersActions"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnairePresentlyWithUsersActions entity)
		{
			reader.Read();
			entity.QuestionnairePresentlyWithActionId = (System.Int32)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (System.Int32)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.QuestionnairePresentlyWithUserId)];
			//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32)reader["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersActionsColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.ProcessNo)];
			//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
			entity.UserName = (System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.ActionName = (System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.ActionName)];
			//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersActionsColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnairePresentlyWithUsersActionsColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnairePresentlyWithUsersActionsColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnairePresentlyWithUsersActions"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnairePresentlyWithUsersActions"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnairePresentlyWithUsersActions entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32)dataRow["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (Convert.IsDBNull(dataRow["ProcessNo"]))?(int)0:(System.Int32?)dataRow["ProcessNo"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.ActionName = (Convert.IsDBNull(dataRow["ActionName"]))?string.Empty:(System.String)dataRow["ActionName"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnairePresentlyWithUsersActionsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersActionsFilterBuilder : SqlFilterBuilder<QuestionnairePresentlyWithUsersActionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsFilterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithUsersActionsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersActionsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersActionsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersActionsFilterBuilder

	#region QuestionnairePresentlyWithUsersActionsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnairePresentlyWithUsersActionsParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnairePresentlyWithUsersActionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsParameterBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithUsersActionsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnairePresentlyWithUsersActionsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnairePresentlyWithUsersActionsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnairePresentlyWithUsersActionsParameterBuilder
	
	#region QuestionnairePresentlyWithUsersActionsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnairePresentlyWithUsersActions"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnairePresentlyWithUsersActionsSortBuilder : SqlSortBuilder<QuestionnairePresentlyWithUsersActionsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnairePresentlyWithUsersActionsSqlSortBuilder class.
		/// </summary>
		public QuestionnairePresentlyWithUsersActionsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnairePresentlyWithUsersActionsSortBuilder

} // end namespace
