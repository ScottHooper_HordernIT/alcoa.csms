﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskTypeService
    {
        AdminTaskType Get(Expression<Func<AdminTaskType, bool>> where, List<Expression<Func<AdminTaskType, object>>> includes);
        List<AdminTaskType> GetMany(int? top, Expression<Func<AdminTaskType, bool>> where, Func<IQueryable<AdminTaskType>, IOrderedQueryable<AdminTaskType>> orderBy, List<Expression<Func<AdminTaskType, object>>> includes);
        List<AdminTaskType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskType, bool>> where, Func<IQueryable<AdminTaskType>, IOrderedQueryable<AdminTaskType>> orderBy, List<Expression<Func<AdminTaskType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskType, bool>> where, List<Expression<Func<AdminTaskType, object>>> includes);
        void Insert(AdminTaskType item, bool saveChanges = true);
        void Update(AdminTaskType item, bool saveChanges = true);
        void Delete(AdminTaskType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskTypeService : IAdminTaskTypeService
    {
        private readonly IRepository<AdminTaskType> _repository;

        public AdminTaskTypeService(IRepository<AdminTaskType> repository)
        {
            this._repository = repository;
        }

        public AdminTaskType Get(Expression<Func<AdminTaskType, bool>> where, List<Expression<Func<AdminTaskType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskType> GetMany(int? top, Expression<Func<AdminTaskType, bool>> where, Func<IQueryable<AdminTaskType>, IOrderedQueryable<AdminTaskType>> orderBy, List<Expression<Func<AdminTaskType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskType, bool>> where, Func<IQueryable<AdminTaskType>, IOrderedQueryable<AdminTaskType>> orderBy, List<Expression<Func<AdminTaskType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskType, bool>> where, List<Expression<Func<AdminTaskType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
