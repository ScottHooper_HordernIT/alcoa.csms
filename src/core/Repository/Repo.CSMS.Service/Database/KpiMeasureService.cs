﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiMeasureService
    {
        KpiMeasure Get(Expression<Func<KpiMeasure, bool>> where, List<Expression<Func<KpiMeasure, object>>> includes);
        List<KpiMeasure> GetAll();
        List<KpiMeasure> GetMany(int? top, Expression<Func<KpiMeasure, bool>> where, Func<IQueryable<KpiMeasure>, IOrderedQueryable<KpiMeasure>> orderBy, List<Expression<Func<KpiMeasure, object>>> includes);
        List<KpiMeasure> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiMeasure, bool>> where, Func<IQueryable<KpiMeasure>, IOrderedQueryable<KpiMeasure>> orderBy, List<Expression<Func<KpiMeasure, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiMeasure, bool>> where, List<Expression<Func<KpiMeasure, object>>> includes);
        void Insert(KpiMeasure item);
        void Insert(KpiMeasure item, bool saveChanges = true);
        void Update(KpiMeasure item);
        void Update(KpiMeasure item, bool saveChanges = true);
        void Delete(int KpiMeasureId, bool saveChanges);
        void Delete(KpiMeasure item);
        void Delete(KpiMeasure item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiMeasureService : IKpiMeasureService
    {
        private readonly IRepository<KpiMeasure> _repository;

        public KpiMeasureService(IRepository<KpiMeasure> repository)
        {
            this._repository = repository;
        }

        public KpiMeasure Get(Expression<Func<KpiMeasure, bool>> where, List<Expression<Func<KpiMeasure, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiMeasure> GetAll()
        {
            return _repository.GetAll(null, o => o.OrderBy(b => b.CompanySiteCategory.CategoryName).ThenBy(b => b.Description), null).ToList();
        }

        public List<KpiMeasure> GetMany(int? top, Expression<Func<KpiMeasure, bool>> where, Func<IQueryable<KpiMeasure>, IOrderedQueryable<KpiMeasure>> orderBy, List<Expression<Func<KpiMeasure, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiMeasure> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiMeasure, bool>> where, Func<IQueryable<KpiMeasure>, IOrderedQueryable<KpiMeasure>> orderBy, List<Expression<Func<KpiMeasure, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiMeasure, bool>> where, List<Expression<Func<KpiMeasure, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(KpiMeasure item)
        {
            Insert(item, true);
        }

        public void Insert(KpiMeasure item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiMeasure");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(KpiMeasure item)
        {
            Update(item, true);
        }

        public void Update(KpiMeasure item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiMeasure");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(KpiMeasure item)
        {
            if (item == null)
                throw new ArgumentNullException("KpiMeasure");

            Delete(item.KpiMeasureId, true);
        }

        public void Delete(KpiMeasure item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiMeasure");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(int KpiMeasureId, bool saveChanges = true)
        {
            _repository.Delete(e => e.KpiMeasureId == KpiMeasureId);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
