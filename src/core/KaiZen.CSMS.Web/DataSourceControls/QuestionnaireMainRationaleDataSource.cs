﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainRationaleProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainRationaleDataSourceDesigner))]
	public class QuestionnaireMainRationaleDataSource : ProviderDataSource<QuestionnaireMainRationale, QuestionnaireMainRationaleKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleDataSource class.
		/// </summary>
		public QuestionnaireMainRationaleDataSource() : base(new QuestionnaireMainRationaleService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainRationaleDataSourceView used by the QuestionnaireMainRationaleDataSource.
		/// </summary>
		protected QuestionnaireMainRationaleDataSourceView QuestionnaireMainRationaleView
		{
			get { return ( View as QuestionnaireMainRationaleDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainRationaleDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainRationaleSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainRationaleSelectMethod selectMethod = QuestionnaireMainRationaleSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainRationaleSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainRationaleDataSourceView class that is to be
		/// used by the QuestionnaireMainRationaleDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainRationaleDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainRationale, QuestionnaireMainRationaleKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainRationaleDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainRationaleDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainRationaleDataSourceView : ProviderDataSourceView<QuestionnaireMainRationale, QuestionnaireMainRationaleKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainRationaleDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainRationaleDataSourceView(QuestionnaireMainRationaleDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainRationaleDataSource QuestionnaireMainRationaleOwner
		{
			get { return Owner as QuestionnaireMainRationaleDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainRationaleSelectMethod SelectMethod
		{
			get { return QuestionnaireMainRationaleOwner.SelectMethod; }
			set { QuestionnaireMainRationaleOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainRationaleService QuestionnaireMainRationaleProvider
		{
			get { return Provider as QuestionnaireMainRationaleService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainRationale> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainRationale> results = null;
			QuestionnaireMainRationale item;
			count = 0;
			
			System.Int32 _questionnaireMainRationaleId;
			System.String _questionNo;

			switch ( SelectMethod )
			{
				case QuestionnaireMainRationaleSelectMethod.Get:
					QuestionnaireMainRationaleKey entityKey  = new QuestionnaireMainRationaleKey();
					entityKey.Load(values);
					item = QuestionnaireMainRationaleProvider.Get(entityKey);
					results = new TList<QuestionnaireMainRationale>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainRationaleSelectMethod.GetAll:
                    results = QuestionnaireMainRationaleProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainRationaleSelectMethod.GetPaged:
					results = QuestionnaireMainRationaleProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainRationaleSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainRationaleProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainRationaleProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainRationaleSelectMethod.GetByQuestionnaireMainRationaleId:
					_questionnaireMainRationaleId = ( values["QuestionnaireMainRationaleId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireMainRationaleId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireMainRationaleProvider.GetByQuestionnaireMainRationaleId(_questionnaireMainRationaleId);
					results = new TList<QuestionnaireMainRationale>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireMainRationaleSelectMethod.GetByQuestionNo:
					_questionNo = ( values["QuestionNo"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionNo"], typeof(System.String)) : string.Empty;
					item = QuestionnaireMainRationaleProvider.GetByQuestionNo(_questionNo);
					results = new TList<QuestionnaireMainRationale>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainRationaleSelectMethod.Get || SelectMethod == QuestionnaireMainRationaleSelectMethod.GetByQuestionnaireMainRationaleId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainRationale entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainRationaleProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainRationale> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainRationaleProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainRationaleDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainRationaleDataSource class.
	/// </summary>
	public class QuestionnaireMainRationaleDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainRationale, QuestionnaireMainRationaleKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainRationaleDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainRationaleSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainRationaleDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainRationaleDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainRationaleDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainRationaleDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainRationaleDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainRationaleDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainRationaleDataSourceActionList(QuestionnaireMainRationaleDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainRationaleSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainRationaleDataSourceActionList
	
	#endregion QuestionnaireMainRationaleDataSourceDesigner
	
	#region QuestionnaireMainRationaleSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainRationaleDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainRationaleSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireMainRationaleId method.
		/// </summary>
		GetByQuestionnaireMainRationaleId,
		/// <summary>
		/// Represents the GetByQuestionNo method.
		/// </summary>
		GetByQuestionNo
	}
	
	#endregion QuestionnaireMainRationaleSelectMethod

	#region QuestionnaireMainRationaleFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleFilter : SqlFilter<QuestionnaireMainRationaleColumn>
	{
	}
	
	#endregion QuestionnaireMainRationaleFilter

	#region QuestionnaireMainRationaleExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainRationaleColumn>
	{
	}
	
	#endregion QuestionnaireMainRationaleExpressionBuilder	

	#region QuestionnaireMainRationaleProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainRationaleChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleProperty : ChildEntityProperty<QuestionnaireMainRationaleChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainRationaleProperty
}

