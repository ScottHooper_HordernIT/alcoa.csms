﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CsmsFileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CsmsFileProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CsmsFile, KaiZen.CSMS.Entities.CsmsFileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsFileKey key)
		{
			return Delete(transactionManager, key.CsmsFileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_csmsFileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _csmsFileId)
		{
			return Delete(null, _csmsFileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _csmsFileId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_CsmsFileTypeId key.
		///		FK_CsmsFile_CsmsFileTypeId Description: 
		/// </summary>
		/// <param name="_csmsFileTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCsmsFileTypeId(System.Int32 _csmsFileTypeId)
		{
			int count = -1;
			return GetByCsmsFileTypeId(_csmsFileTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_CsmsFileTypeId key.
		///		FK_CsmsFile_CsmsFileTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileTypeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		/// <remarks></remarks>
		public TList<CsmsFile> GetByCsmsFileTypeId(TransactionManager transactionManager, System.Int32 _csmsFileTypeId)
		{
			int count = -1;
			return GetByCsmsFileTypeId(transactionManager, _csmsFileTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_CsmsFileTypeId key.
		///		FK_CsmsFile_CsmsFileTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCsmsFileTypeId(TransactionManager transactionManager, System.Int32 _csmsFileTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsFileTypeId(transactionManager, _csmsFileTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_CsmsFileTypeId key.
		///		fkCsmsFileCsmsFileTypeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsFileTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCsmsFileTypeId(System.Int32 _csmsFileTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCsmsFileTypeId(null, _csmsFileTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_CsmsFileTypeId key.
		///		fkCsmsFileCsmsFileTypeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_csmsFileTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCsmsFileTypeId(System.Int32 _csmsFileTypeId, int start, int pageLength,out int count)
		{
			return GetByCsmsFileTypeId(null, _csmsFileTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_CsmsFileTypeId key.
		///		FK_CsmsFile_CsmsFileTypeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public abstract TList<CsmsFile> GetByCsmsFileTypeId(TransactionManager transactionManager, System.Int32 _csmsFileTypeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_CreatedByUserId key.
		///		FK_CsmsFile_Users_CreatedByUserId Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_CreatedByUserId key.
		///		FK_CsmsFile_Users_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		/// <remarks></remarks>
		public TList<CsmsFile> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_CreatedByUserId key.
		///		FK_CsmsFile_Users_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_CreatedByUserId key.
		///		fkCsmsFileUsersCreatedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_CreatedByUserId key.
		///		fkCsmsFileUsersCreatedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_CreatedByUserId key.
		///		FK_CsmsFile_Users_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public abstract TList<CsmsFile> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_ModifiedByUserId key.
		///		FK_CsmsFile_Users_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_ModifiedByUserId key.
		///		FK_CsmsFile_Users_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		/// <remarks></remarks>
		public TList<CsmsFile> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_ModifiedByUserId key.
		///		FK_CsmsFile_Users_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_ModifiedByUserId key.
		///		fkCsmsFileUsersModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_ModifiedByUserId key.
		///		fkCsmsFileUsersModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public TList<CsmsFile> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CsmsFile_Users_ModifiedByUserId key.
		///		FK_CsmsFile_Users_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CsmsFile objects.</returns>
		public abstract TList<CsmsFile> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CsmsFile Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsFileKey key, int start, int pageLength)
		{
			return GetByCsmsFileId(transactionManager, key.CsmsFileId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CsmsFile index.
		/// </summary>
		/// <param name="_csmsFileId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsFile GetByCsmsFileId(System.Int32 _csmsFileId)
		{
			int count = -1;
			return GetByCsmsFileId(null,_csmsFileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsFile index.
		/// </summary>
		/// <param name="_csmsFileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsFile GetByCsmsFileId(System.Int32 _csmsFileId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsFileId(null, _csmsFileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsFile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsFile GetByCsmsFileId(TransactionManager transactionManager, System.Int32 _csmsFileId)
		{
			int count = -1;
			return GetByCsmsFileId(transactionManager, _csmsFileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsFile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsFile GetByCsmsFileId(TransactionManager transactionManager, System.Int32 _csmsFileId, int start, int pageLength)
		{
			int count = -1;
			return GetByCsmsFileId(transactionManager, _csmsFileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsFile index.
		/// </summary>
		/// <param name="_csmsFileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> class.</returns>
		public KaiZen.CSMS.Entities.CsmsFile GetByCsmsFileId(System.Int32 _csmsFileId, int start, int pageLength, out int count)
		{
			return GetByCsmsFileId(null, _csmsFileId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CsmsFile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_csmsFileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CsmsFile GetByCsmsFileId(TransactionManager transactionManager, System.Int32 _csmsFileId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CsmsFile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CsmsFile&gt;"/></returns>
		public static TList<CsmsFile> Fill(IDataReader reader, TList<CsmsFile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CsmsFile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CsmsFile")
					.Append("|").Append((System.Int32)reader[((int)CsmsFileColumn.CsmsFileId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CsmsFile>(
					key.ToString(), // EntityTrackingKey
					"CsmsFile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CsmsFile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CsmsFileId = (System.Int32)reader[((int)CsmsFileColumn.CsmsFileId - 1)];
					c.CsmsFileTypeId = (System.Int32)reader[((int)CsmsFileColumn.CsmsFileTypeId - 1)];
					c.FileName = (System.String)reader[((int)CsmsFileColumn.FileName - 1)];
					c.Description = (System.String)reader[((int)CsmsFileColumn.Description - 1)];
					c.FileHash = (System.Byte[])reader[((int)CsmsFileColumn.FileHash - 1)];
					c.ContentLength = (System.Int32)reader[((int)CsmsFileColumn.ContentLength - 1)];
					c.Content = (System.Byte[])reader[((int)CsmsFileColumn.Content - 1)];
					c.IsVisible = (System.Boolean)reader[((int)CsmsFileColumn.IsVisible - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)CsmsFileColumn.CreatedByUserId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)CsmsFileColumn.ModifiedByUserId - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)CsmsFileColumn.CreatedDate - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)CsmsFileColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsFile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CsmsFile entity)
		{
			if (!reader.Read()) return;
			
			entity.CsmsFileId = (System.Int32)reader[((int)CsmsFileColumn.CsmsFileId - 1)];
			entity.CsmsFileTypeId = (System.Int32)reader[((int)CsmsFileColumn.CsmsFileTypeId - 1)];
			entity.FileName = (System.String)reader[((int)CsmsFileColumn.FileName - 1)];
			entity.Description = (System.String)reader[((int)CsmsFileColumn.Description - 1)];
			entity.FileHash = (System.Byte[])reader[((int)CsmsFileColumn.FileHash - 1)];
			entity.ContentLength = (System.Int32)reader[((int)CsmsFileColumn.ContentLength - 1)];
			entity.Content = (System.Byte[])reader[((int)CsmsFileColumn.Content - 1)];
			entity.IsVisible = (System.Boolean)reader[((int)CsmsFileColumn.IsVisible - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)CsmsFileColumn.CreatedByUserId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CsmsFileColumn.ModifiedByUserId - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)CsmsFileColumn.CreatedDate - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)CsmsFileColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CsmsFile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsFile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CsmsFile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CsmsFileId = (System.Int32)dataRow["CsmsFileId"];
			entity.CsmsFileTypeId = (System.Int32)dataRow["CsmsFileTypeId"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.Description = (System.String)dataRow["Description"];
			entity.FileHash = (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (System.Int32)dataRow["ContentLength"];
			entity.Content = (System.Byte[])dataRow["Content"];
			entity.IsVisible = (System.Boolean)dataRow["IsVisible"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CsmsFile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsFile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsFile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CsmsFileTypeIdSource	
			if (CanDeepLoad(entity, "CsmsFileType|CsmsFileTypeIdSource", deepLoadType, innerList) 
				&& entity.CsmsFileTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CsmsFileTypeId;
				CsmsFileType tmpEntity = EntityManager.LocateEntity<CsmsFileType>(EntityLocator.ConstructKeyFromPkItems(typeof(CsmsFileType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CsmsFileTypeIdSource = tmpEntity;
				else
					entity.CsmsFileTypeIdSource = DataRepository.CsmsFileTypeProvider.GetByCsmsFileTypeId(transactionManager, entity.CsmsFileTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsmsFileTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CsmsFileTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CsmsFileTypeProvider.DeepLoad(transactionManager, entity.CsmsFileTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CsmsFileTypeIdSource

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCsmsFileId methods when available
			
			#region AdminTaskEmailTemplateAttachmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailTemplateAttachment>|AdminTaskEmailTemplateAttachmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateAttachmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailTemplateAttachmentCollection = DataRepository.AdminTaskEmailTemplateAttachmentProvider.GetByCsmsFileId(transactionManager, entity.CsmsFileId);

				if (deep && entity.AdminTaskEmailTemplateAttachmentCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailTemplateAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailTemplateAttachment>) DataRepository.AdminTaskEmailTemplateAttachmentProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailTemplateAttachmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CsmsFile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CsmsFile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CsmsFile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CsmsFile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CsmsFileTypeIdSource
			if (CanDeepSave(entity, "CsmsFileType|CsmsFileTypeIdSource", deepSaveType, innerList) 
				&& entity.CsmsFileTypeIdSource != null)
			{
				DataRepository.CsmsFileTypeProvider.Save(transactionManager, entity.CsmsFileTypeIdSource);
				entity.CsmsFileTypeId = entity.CsmsFileTypeIdSource.CsmsFileTypeId;
			}
			#endregion 
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTaskEmailTemplateAttachment>
				if (CanDeepSave(entity.AdminTaskEmailTemplateAttachmentCollection, "List<AdminTaskEmailTemplateAttachment>|AdminTaskEmailTemplateAttachmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailTemplateAttachment child in entity.AdminTaskEmailTemplateAttachmentCollection)
					{
						if(child.CsmsFileIdSource != null)
						{
							child.CsmsFileId = child.CsmsFileIdSource.CsmsFileId;
						}
						else
						{
							child.CsmsFileId = entity.CsmsFileId;
						}

					}

					if (entity.AdminTaskEmailTemplateAttachmentCollection.Count > 0 || entity.AdminTaskEmailTemplateAttachmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailTemplateAttachmentProvider.Save(transactionManager, entity.AdminTaskEmailTemplateAttachmentCollection);
						
						deepHandles.Add("AdminTaskEmailTemplateAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailTemplateAttachment >) DataRepository.AdminTaskEmailTemplateAttachmentProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailTemplateAttachmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CsmsFileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CsmsFile</c>
	///</summary>
	public enum CsmsFileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CsmsFileType</c> at CsmsFileTypeIdSource
		///</summary>
		[ChildEntityType(typeof(CsmsFileType))]
		CsmsFileType,
			
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>CsmsFile</c> as OneToMany for AdminTaskEmailTemplateAttachmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailTemplateAttachment>))]
		AdminTaskEmailTemplateAttachmentCollection,
	}
	
	#endregion CsmsFileChildEntityTypes
	
	#region CsmsFileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CsmsFileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileFilterBuilder : SqlFilterBuilder<CsmsFileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileFilterBuilder class.
		/// </summary>
		public CsmsFileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsFileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsFileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsFileFilterBuilder
	
	#region CsmsFileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CsmsFileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsFileParameterBuilder : ParameterizedSqlFilterBuilder<CsmsFileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileParameterBuilder class.
		/// </summary>
		public CsmsFileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CsmsFileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CsmsFileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CsmsFileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CsmsFileParameterBuilder
	
	#region CsmsFileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CsmsFileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsFile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CsmsFileSortBuilder : SqlSortBuilder<CsmsFileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsFileSqlSortBuilder class.
		/// </summary>
		public CsmsFileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CsmsFileSortBuilder
	
} // end namespace
