﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'CompaniesEhsConsultants' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CompaniesEhsConsultants : CompaniesEhsConsultantsBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CompaniesEhsConsultants"/> instance.
		///</summary>
		public CompaniesEhsConsultants():base(){}	
		
		#endregion
	}
}
