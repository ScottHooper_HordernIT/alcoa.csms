﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SafetyPlansSeResponsesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(SafetyPlansSeResponsesDataSourceDesigner))]
	public class SafetyPlansSeResponsesDataSource : ProviderDataSource<SafetyPlansSeResponses, SafetyPlansSeResponsesKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesDataSource class.
		/// </summary>
		public SafetyPlansSeResponsesDataSource() : base(new SafetyPlansSeResponsesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SafetyPlansSeResponsesDataSourceView used by the SafetyPlansSeResponsesDataSource.
		/// </summary>
		protected SafetyPlansSeResponsesDataSourceView SafetyPlansSeResponsesView
		{
			get { return ( View as SafetyPlansSeResponsesDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the SafetyPlansSeResponsesDataSource control invokes to retrieve data.
		/// </summary>
		public SafetyPlansSeResponsesSelectMethod SelectMethod
		{
			get
			{
				SafetyPlansSeResponsesSelectMethod selectMethod = SafetyPlansSeResponsesSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (SafetyPlansSeResponsesSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SafetyPlansSeResponsesDataSourceView class that is to be
		/// used by the SafetyPlansSeResponsesDataSource.
		/// </summary>
		/// <returns>An instance of the SafetyPlansSeResponsesDataSourceView class.</returns>
		protected override BaseDataSourceView<SafetyPlansSeResponses, SafetyPlansSeResponsesKey> GetNewDataSourceView()
		{
			return new SafetyPlansSeResponsesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SafetyPlansSeResponsesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SafetyPlansSeResponsesDataSourceView : ProviderDataSourceView<SafetyPlansSeResponses, SafetyPlansSeResponsesKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SafetyPlansSeResponsesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SafetyPlansSeResponsesDataSourceView(SafetyPlansSeResponsesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SafetyPlansSeResponsesDataSource SafetyPlansSeResponsesOwner
		{
			get { return Owner as SafetyPlansSeResponsesDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal SafetyPlansSeResponsesSelectMethod SelectMethod
		{
			get { return SafetyPlansSeResponsesOwner.SelectMethod; }
			set { SafetyPlansSeResponsesOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SafetyPlansSeResponsesService SafetyPlansSeResponsesProvider
		{
			get { return Provider as SafetyPlansSeResponsesService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<SafetyPlansSeResponses> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<SafetyPlansSeResponses> results = null;
			SafetyPlansSeResponses item;
			count = 0;
			
			System.Int32 _responseId;
			System.Int32 _companyId;

			switch ( SelectMethod )
			{
				case SafetyPlansSeResponsesSelectMethod.Get:
					SafetyPlansSeResponsesKey entityKey  = new SafetyPlansSeResponsesKey();
					entityKey.Load(values);
					item = SafetyPlansSeResponsesProvider.Get(entityKey);
					results = new TList<SafetyPlansSeResponses>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case SafetyPlansSeResponsesSelectMethod.GetAll:
                    results = SafetyPlansSeResponsesProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case SafetyPlansSeResponsesSelectMethod.GetPaged:
					results = SafetyPlansSeResponsesProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case SafetyPlansSeResponsesSelectMethod.Find:
					if ( FilterParameters != null )
						results = SafetyPlansSeResponsesProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = SafetyPlansSeResponsesProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case SafetyPlansSeResponsesSelectMethod.GetByResponseId:
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					item = SafetyPlansSeResponsesProvider.GetByResponseId(_responseId);
					results = new TList<SafetyPlansSeResponses>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case SafetyPlansSeResponsesSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = SafetyPlansSeResponsesProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == SafetyPlansSeResponsesSelectMethod.Get || SelectMethod == SafetyPlansSeResponsesSelectMethod.GetByResponseId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				SafetyPlansSeResponses entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					SafetyPlansSeResponsesProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<SafetyPlansSeResponses> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			SafetyPlansSeResponsesProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region SafetyPlansSeResponsesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SafetyPlansSeResponsesDataSource class.
	/// </summary>
	public class SafetyPlansSeResponsesDataSourceDesigner : ProviderDataSourceDesigner<SafetyPlansSeResponses, SafetyPlansSeResponsesKey>
	{
		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesDataSourceDesigner class.
		/// </summary>
		public SafetyPlansSeResponsesDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SafetyPlansSeResponsesSelectMethod SelectMethod
		{
			get { return ((SafetyPlansSeResponsesDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new SafetyPlansSeResponsesDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region SafetyPlansSeResponsesDataSourceActionList

	/// <summary>
	/// Supports the SafetyPlansSeResponsesDataSourceDesigner class.
	/// </summary>
	internal class SafetyPlansSeResponsesDataSourceActionList : DesignerActionList
	{
		private SafetyPlansSeResponsesDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the SafetyPlansSeResponsesDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public SafetyPlansSeResponsesDataSourceActionList(SafetyPlansSeResponsesDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SafetyPlansSeResponsesSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion SafetyPlansSeResponsesDataSourceActionList
	
	#endregion SafetyPlansSeResponsesDataSourceDesigner
	
	#region SafetyPlansSeResponsesSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the SafetyPlansSeResponsesDataSource.SelectMethod property.
	/// </summary>
	public enum SafetyPlansSeResponsesSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId
	}
	
	#endregion SafetyPlansSeResponsesSelectMethod

	#region SafetyPlansSeResponsesFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeResponses"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeResponsesFilter : SqlFilter<SafetyPlansSeResponsesColumn>
	{
	}
	
	#endregion SafetyPlansSeResponsesFilter

	#region SafetyPlansSeResponsesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeResponses"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeResponsesExpressionBuilder : SqlExpressionBuilder<SafetyPlansSeResponsesColumn>
	{
	}
	
	#endregion SafetyPlansSeResponsesExpressionBuilder	

	#region SafetyPlansSeResponsesProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;SafetyPlansSeResponsesChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlansSeResponses"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlansSeResponsesProperty : ChildEntityProperty<SafetyPlansSeResponsesChildEntityTypes>
	{
	}
	
	#endregion SafetyPlansSeResponsesProperty
}

