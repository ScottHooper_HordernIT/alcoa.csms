﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class UserControls_Admin_UsersViewEngineeringProjectHours : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId == (int)RoleList.Administrator)
            {
                //

            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }
    }

    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["PrivilegeId"] = (int)PrivilegeList.ViewKpiEngineeringProjectHours;
        e.NewValues["CreatedByUserId"] = auth.UserId;
        e.NewValues["CreatedDate"] = DateTime.Now;
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        if (e.NewValues["Active"].ToString() != "True")
        {
            e.NewValues["Active"] = 0; //make sure not null
        }
    }
    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
    }
    protected void grid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        //e.NewValues["ApprovedByUserId"] = auth.UserId;
        //e.NewValues["ApprovedDate"] = DateTime.Now;
        //e.NewValues["ModifiedByUserId"] = auth.UserId;
        //e.NewValues["ModifiedDate"] = DateTime.Now;

        //int QuestionnaireId = Convert.ToInt32(Request.QueryString["q"].ToString());
        //QuestionnaireService qService = new QuestionnaireService();
        //Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);
        //e.NewValues["CompanyId"] = q.CompanyId;
    }


}
