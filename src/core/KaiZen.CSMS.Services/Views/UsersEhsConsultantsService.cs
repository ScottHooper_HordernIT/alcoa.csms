﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An component type implementation of the 'UsersEhsConsultants' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class UsersEhsConsultantsService : KaiZen.CSMS.Services.UsersEhsConsultantsServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the UsersEhsConsultantsService class.
		/// </summary>
		public UsersEhsConsultantsService() : base()
		{
		}
		
	}//End Class


} // end namespace
