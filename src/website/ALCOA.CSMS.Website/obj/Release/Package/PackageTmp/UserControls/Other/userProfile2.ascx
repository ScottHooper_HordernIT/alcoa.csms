﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Other_userProfile2" Codebehind="userProfile2.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<style type="text/css">
    .style1
    {
        width: 70px;
    }
</style>
<div class="dxncItemContent_Blue" style="width: 247px; height: 144px; padding-left: 5px;">
    <div align="left" style="border-right: #003399 1px solid; padding-right: 3px; border-top: #003399 1px solid;
        padding-left: 3px; padding-bottom: 3px; margin-left: 0px; border-left: #003399 1px solid;
        padding-top: 2px; border-bottom: #003399 1px solid; background-color: #e9efff;">
        <table width="240px" height="137px">
            <tr>
                <td style="text-align: left" colspan="2">
                    <strong><span style="font-size: 14px; color: #003399; font-family: Arial">Your Profile</span></strong>
                </td>
            </tr>
            <tr style="padding-top: 10px;">
                <td style="padding-left: 5px; text-align: right;" class="style1">
                    <dx:ASPxLabel ID="lblTitle_Name" runat="server" Font-Bold="true" ForeColor="#003399"
                        Text="Name:">
                    </dx:ASPxLabel>
                </td>
                <td width="161px">
                    <dx:ASPxLabel ID="lblName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue">
                    </dx:ASPxLabel>
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <dx:ASPxTextBox ID="tbFirstName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    Visible="False" Width="70px">
                                    <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="Save">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                            <td>
                                <dx:ASPxTextBox ID="tbLastName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    Visible="False" Width="70px">
                                    <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="Save">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="padding-top: 20px">
                <td style="text-align: right;" class="style1">
                    <dx:ASPxLabel ID="lblTitle_Title" runat="server" Font-Bold="true" ForeColor="#003399"
                        Text="Title: ">
                    </dx:ASPxLabel>
                </td>
                <td width="161px">
                    <dx:ASPxLabel ID="lblTitle" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue">
                    </dx:ASPxLabel>
                    <dx:ASPxTextBox ID="tbTitle" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Visible="False" Width="140px">
                        <ValidationSettings CausesValidation="True" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Save">
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 5px; text-align: right;" class="style1">
                    <dx:ASPxLabel ID="lblTitle_Company" runat="server" Font-Bold="true" ForeColor="#003399"
                        Text="Company: ">
                    </dx:ASPxLabel>
                </td>
                <td width="161px">
                    <dx:ASPxLabel ID="lblCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;" class="style1">
                    <dx:ASPxLabel ID="lblTitle_Email" runat="server" Font-Bold="true" ForeColor="#003399"
                        Text="E-Mail: ">
                    </dx:ASPxLabel>
                </td>
                <td width="161px">
                    <dx:ASPxHyperLink ID="hlEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" Text="[hlEmail]">
                    </dx:ASPxHyperLink>
                    <dx:ASPxTextBox ID="tbEmail" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Visible="False" Width="140px">
                        <ValidationSettings CausesValidation="True" ValidationGroup="Save" Display="Dynamic" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    [<asp:Button ID="btnEdit" runat="server" BackColor="Transparent" BorderColor="Transparent"
                        BorderStyle="None" Font-Names="Verdana" Font-Size="XX-Small" Font-Bold="true"
                        Font-Underline="False" ForeColor="Red" OnClick="btnEdit_Click" Text="Edit" />|<asp:Button
                            ID="btnSave" runat="server" BackColor="Transparent" BorderColor="Transparent"
                            BorderStyle="None" Font-Names="Verdana" Font-Size="XX-Small" Font-Bold="true"
                            Font-Underline="False" ForeColor="Silver" OnClick="btnSave_Click" Text="Save" Enabled="false" CausesValidation="true" ValidationGroup="Save" />|<asp:Button
                                ID="btnCancel" runat="server" BackColor="Transparent" BorderColor="Transparent"
                                BorderStyle="None" Font-Names="Verdana" Font-Size="XX-Small" Font-Bold="true"
                                Font-Underline="False" ForeColor="Silver" OnClick="btnCancel_Click" Text="Cancel" Enabled="false" />]
                </td>
            </tr>
        </table>
    </div>
</div>


