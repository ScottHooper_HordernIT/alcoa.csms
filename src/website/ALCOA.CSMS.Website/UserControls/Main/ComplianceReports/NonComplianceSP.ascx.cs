using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.Library;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;

public partial class UserControls_NonComplianceSP : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        
        if (!postBack)
        { //first time load
            SessionHandler.spVar_Year = DateTime.Now.Year.ToString();
            cmbYear.Value = DateTime.Now.Year.ToString();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //restrict to own company
                    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    grid.Settings.ShowFilterRow = false;
                    grid.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                    grid.DataBind();
                    ddlRegions.Visible = false; //These two lines added by Ashley Goldstraw DT373 5/1/2016
                    lblRegion.Visible = false;
                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
            //These lines added by Ashley Goldstraw DT373 5/1/2016
            ddlRegions.DataSourceID = "sqldsRegionsList";
            ddlRegions.TextField = "RegionName";
            ddlRegions.ValueField = "RegionId";
            ddlRegions.DataBind();
            ListEditItem l = new ListEditItem("All", 0);
            ddlRegions.Items.Insert(0, l);
            ddlRegions.SelectedIndex = 0;
            //end Lines added by Ashley Goldstraw
            // Export
            String exportFileName = @"ALCOA CSMS - Non Compliance Report - Safety Plans"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
    }

    protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (SessionHandler.spVar_Year == e.Parameters) return;
        SessionHandler.spVar_Year = e.Parameters;
        grid.DataBind();
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noSPSubmitted") as Label;
        int change = (int)grid.GetRowValues(e.VisibleIndex, "Submitted");
        string status;

        if (grid.GetRowValues(e.VisibleIndex, "StatusName") != DBNull.Value)
            status = (string)grid.GetRowValues(e.VisibleIndex, "StatusName");
        else
            status = "";
        //Start:Change by Debashis for Export issue
        if (label != null)
        {
            label.Text = "";
            System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage")
                                        as System.Web.UI.WebControls.Image;
            img.Visible = false;
            if (change != 0)
            {
                img.Visible = true;

                label.ForeColor = Color.Green;
                label.Text = String.Format("({0})", change);

                switch (status)
                {
                    case "Being Assessed":
                        img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                        break;
                    case "Not Approved":
                        img.ImageUrl = "~/Images/redcross.gif";
                        break;
                    case "Approved":
                        img.ImageUrl = "~/Images/greentick.gif";
                        break;
                    default:
                        img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                        break;
                }
            }
            else
            {
                img.Visible = true;
                img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                label.ForeColor = Color.Red;
            }
        }

        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlSE") as ASPxHyperLink;
        if (hl != null)
        {
            hl.NavigateUrl = "javascript:void(0);";
            hl.Text = status;
            int r = NumberUtilities.Convert.parseObjectToInt(grid.GetRowValues(e.VisibleIndex, "SelfAssessment_ResponseId"));
            if (r > 0)
            {
                hl.NavigateUrl = "~/SafetyPlans_SelfAssessment.aspx" + QueryStringModule.Encrypt(String.Format("R={0}", r));

            }
        }
        //End:Change by Debashis for Export issue
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "SiteId" || e.Column.FieldName == "CategoryId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', '');}";
        }
    }

    //protected void cmbYear_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (SessionHandler.spVar_Year == cmbYear.SelectedItem.Value.ToString()) return;
    //    SessionHandler.spVar_Year = cmbYear.SelectedItem.Value.ToString();
    //     grid.DataBind();
    //}
}
