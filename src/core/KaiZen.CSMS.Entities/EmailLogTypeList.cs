﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'EmailLogType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table EmailLogType</remark>
	[Serializable]
	public enum EmailLogTypeList
	{
		/// <summary> 
		/// Procurement Daily Work Task Reminder
		/// </summary>
		[EnumTextValue(@"Procurement Daily Work Task Reminder")]
		ProcurementDailyWorkReminderEmail = 1, 

		/// <summary> 
		/// H&S Assessor Daily Work Task Reminder
		/// </summary>
		[EnumTextValue(@"H&S Assessor Daily Work Task Reminder")]
		HSAssessorDailyWorkReminderEmail = 2, 

		/// <summary> 
		/// Questionnaire
		/// </summary>
		[EnumTextValue(@"Questionnaire")]
		Questionnaire = 3, 

		/// <summary> 
		/// Safety Plans
		/// </summary>
		[EnumTextValue(@"Safety Plans")]
		SafetyPlans = 4, 

		/// <summary> 
		/// General
		/// </summary>
		[EnumTextValue(@"General")]
		General = 5, 

		/// <summary> 
		/// IHS Email
		/// </summary>
		[EnumTextValue(@"IHS Email")]
		IhsEmail = 6

	}
}
