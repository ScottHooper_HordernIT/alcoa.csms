﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity.Core.Objects;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IS812EvidenceFileService
    {
        S812EvidenceFile Get(Expression<Func<S812EvidenceFile, bool>> where, List<Expression<Func<S812EvidenceFile, object>>> includes);
        List<S812EvidenceFile> GetMany(int? top, Expression<Func<S812EvidenceFile, bool>> where, Func<IQueryable<S812EvidenceFile>, IOrderedQueryable<S812EvidenceFile>> orderBy, List<Expression<Func<S812EvidenceFile, object>>> includes);
        List<S812EvidenceFile> GetManyPaged(int pageNumber, int pageSize, Expression<Func<S812EvidenceFile, bool>> where, Func<IQueryable<S812EvidenceFile>, IOrderedQueryable<S812EvidenceFile>> orderBy, List<Expression<Func<S812EvidenceFile, object>>> includes, out int pageCount, out int totalRecords);
        List<S812EvidenceFile> GetAll();
        int? GetCount(Expression<Func<S812EvidenceFile, bool>> where, List<Expression<Func<S812EvidenceFile, object>>> includes);
        void Insert(S812EvidenceFile item, bool saveChanges = true);
        void Update(S812EvidenceFile item, bool saveChanges = true);
        void Delete(S812EvidenceFile item, bool saveChanges = true);
        void Delete(S812EvidenceFile item);
        void Delete(int s812EvidenceFileId, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class S812EvidenceFileService : IS812EvidenceFileService
    {
        private readonly IRepository<S812EvidenceFile> _repository;

        public S812EvidenceFileService(IRepository<S812EvidenceFile> repository)
        {
            this._repository = repository;
        }

        public S812EvidenceFile Get(Expression<Func<S812EvidenceFile, bool>> where, List<Expression<Func<S812EvidenceFile, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<S812EvidenceFile> GetMany(int? top, Expression<Func<S812EvidenceFile, bool>> where, Func<IQueryable<S812EvidenceFile>, IOrderedQueryable<S812EvidenceFile>> orderBy, List<Expression<Func<S812EvidenceFile, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<S812EvidenceFile> GetManyPaged(int pageNumber, int pageSize, Expression<Func<S812EvidenceFile, bool>> where, Func<IQueryable<S812EvidenceFile>, IOrderedQueryable<S812EvidenceFile>> orderBy, List<Expression<Func<S812EvidenceFile, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<S812EvidenceFile, bool>> where, List<Expression<Func<S812EvidenceFile, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(S812EvidenceFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("S812EvidenceFile");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(S812EvidenceFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("S812EvidenceFile");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(S812EvidenceFile item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("S812EvidenceFile");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }


        public List<S812EvidenceFile> GetAll()
        {
            return this.GetMany(null,null,null,null).ToList();
        }


        public void Delete(S812EvidenceFile item)
        {
            if (item == null)
                throw new ArgumentNullException("S812EvidenceFile");
            Delete(item.S812EvidenceFileId, true);
        }
        public void Delete(int s812EvidenceFileId, bool saveChanges = true)
        {
            _repository.Delete(e => e.S812EvidenceFileId == s812EvidenceFileId);

            if (saveChanges)
                _repository.SaveChanges();
        }
    }

    #endregion
}
