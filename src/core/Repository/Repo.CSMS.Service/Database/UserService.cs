﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUserService
    {
        User Get(Expression<Func<User, bool>> where, List<Expression<Func<User, object>>> includes);
        List<User> GetMany(int? top, Expression<Func<User, bool>> where, Func<IQueryable<User>, IOrderedQueryable<User>> orderBy, List<Expression<Func<User, object>>> includes);
        List<User> GetAll();
        List<User> GetManyPaged(int pageNumber, int pageSize, Expression<Func<User, bool>> where, Func<IQueryable<User>, IOrderedQueryable<User>> orderBy, List<Expression<Func<User, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<User, bool>> where, List<Expression<Func<User, object>>> includes);
        void Insert(User item, bool saveChanges = true);
        void Update(User item, bool saveChanges = true);
        void Delete(User item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UserService : IUserService
    {
        private readonly IRepository<User> _repository;

        public UserService(IRepository<User> repository)
        {
            this._repository = repository;
        }

        public User Get(Expression<Func<User, bool>> where, List<Expression<Func<User, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<User> GetMany(int? top, Expression<Func<User, bool>> where, Func<IQueryable<User>, IOrderedQueryable<User>> orderBy, List<Expression<Func<User, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<User> GetManyPaged(int pageNumber, int pageSize, Expression<Func<User, bool>> where, Func<IQueryable<User>, IOrderedQueryable<User>> orderBy, List<Expression<Func<User, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<User, bool>> where, List<Expression<Func<User, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(User item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("user");

            //user.CreatedBy = (HttpContext.Current != null && HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : "csms");
            //user.Created = DateTime.Now;

            //ValidateAndSetNewPassword(user);

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(User item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("user");

            //user.ModifiedBy = (HttpContext.Current != null && HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : "csms");
            //user.Modified = DateTime.Now;

            //ValidateAndSetNewPassword(user);

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(User item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("user");

            _repository.Delete(item);
            //_userRepository.ExecuteCustomFunction("spDeactivateEntity", new ObjectParameter("TableName", typeof(User).Name), new ObjectParameter("Value", user.ID));

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }

        //private void ValidateAndSetNewPassword(User user)
        //{
        //    // make sure a password is actually being specified
        //    if (string.IsNullOrEmpty(user.NewPassword)) return;

        //    if (!EncryptionHelper.VerifyComplexPassword(user.NewPassword, 8, 0, 0, 1, 0))
        //    {
        //        throw new ApplicationException("A strong password is required. The password must be 8 characters long and contain at least one (1) number.");
        //    }

        //    // set the password that is valid
        //    user.Password = EncryptionHelper.GetMD5Hash(user.NewPassword, user.EmailAddress.ToLower());
        //}


        public List<User> GetAll()
        {
            return this.GetMany(null,null,null,null);
        }
    }

    #endregion
}
