using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserControls_Admin_LinkManager : System.Web.UI.UserControl
{
    #region Services

    public Repo.CSMS.Service.Database.IUsefulLinkService UsefulLinkService { get; set; }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            grid.FilterExpression = "[IsVisible] = True";
        }
    }

    protected void ods_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IUsefulLinkService>();
    }

    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        //e.Cancel = true;
        //grid.CancelEdit();
        //grid.DataBind();
    }
    protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        //e.Cancel = true;
        //grid.CancelEdit();
        //grid.DataBind();
    }
}
