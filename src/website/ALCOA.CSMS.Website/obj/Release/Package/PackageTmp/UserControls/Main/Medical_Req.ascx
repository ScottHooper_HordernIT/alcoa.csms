﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Medical_Req" Codebehind="Medical_Req.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px">
            <span class="bodycopy"><span class="title">WA Health > Medical<asp:Button ID="btnEdit" runat="server"
                BackColor="White" BorderColor="White" BorderStyle="None" Font-Names="Verdana"
                Font-Size="XX-Small" Font-Underline="True" ForeColor="Red" OnClick="btnEdit_Click"
                Text="(Edit)" /></span><br />
                <span class="date">Requirements</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: left">
            &nbsp;<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                OnRowInserting="grid_OnRowInserting" OnHtmlRowCreated="grid_RowCreated"
                OnInitNewRow="grid_InitNewRow" OnRowUpdating="grid_RowUpdating"
                DataSourceID="sqldsMedical" KeyFieldName="DocumentId" Width="100%">
                <Columns>
                    <dxwgv:GridViewCommandColumn Caption="Action" Visible="False" VisibleIndex="0" Width="50px">
                            <EditButton Visible="True">
                            </EditButton>
                            <NewButton Visible="True">
                            </NewButton>
                            <DeleteButton Visible="True">
                            </DeleteButton>
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="DocumentId" ReadOnly="True" Visible="False"
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn ReadOnly="True" VisibleIndex="0" Width="8px">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <Paddings Padding="5px" />
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataHyperLinkColumn FieldName="DocumentFileName" Caption="File Name" VisibleIndex="1" Width="100px" ToolTip="Click to download file.">
                         <DataItemTemplate>
                             <table cellpadding="0" cellspacing="0"><tr>
                             <td style="width:120px">
                                 <dxe:ASPxHyperLink ID="hlFileDownload" runat="server" Text="">
                                 </dxe:ASPxHyperLink>
                             </td>
                             </tr></table>
                        </DataItemTemplate>
                        <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataHyperLinkColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentName" Caption="Document Name" VisibleIndex="1" SortIndex="0">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileDescription" Visible="False" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentType" Visible="False" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsEditing Mode="Inline" />
                <SettingsBehavior ConfirmDelete="True" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
            </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqldsMedical" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT * FROM [Documents] WHERE ([DocumentType] = @DocumentType)" DeleteCommand="DELETE FROM [Documents] WHERE [DocumentId] = @DocumentId" InsertCommand="INSERT INTO [Documents] ([DocumentName], [DocumentFileName], [DocumentFileDescription], [DocumentType]) VALUES (@DocumentName, @DocumentFileName, @DocumentFileDescription, @DocumentType)" UpdateCommand="UPDATE [Documents] SET [DocumentName] = @DocumentName, [DocumentFileName] = @DocumentFileName, [DocumentFileDescription] = @DocumentFileDescription, [DocumentType] = @DocumentType WHERE [DocumentId] = @DocumentId">
    <SelectParameters>
        <asp:Parameter DefaultValue="MM" Name="DocumentType" Type="String" />
    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="DocumentId" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="DocumentName" Type="String" />
        <asp:Parameter Name="DocumentFileName" Type="String" />
        <asp:Parameter Name="DocumentFileDescription" Type="String" />
        <asp:Parameter Name="DocumentType" Type="String" />
        <asp:Parameter Name="DocumentId" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="DocumentName" Type="String" />
        <asp:Parameter Name="DocumentFileName" Type="String" />
        <asp:Parameter Name="DocumentFileDescription" Type="String" />
        <asp:Parameter Name="DocumentType" Type="String" />
    </InsertParameters>
</asp:SqlDataSource>
