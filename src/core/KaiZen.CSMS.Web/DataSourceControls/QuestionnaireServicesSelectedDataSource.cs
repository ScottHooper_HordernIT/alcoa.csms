﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireServicesSelectedProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireServicesSelectedDataSourceDesigner))]
	public class QuestionnaireServicesSelectedDataSource : ProviderDataSource<QuestionnaireServicesSelected, QuestionnaireServicesSelectedKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedDataSource class.
		/// </summary>
		public QuestionnaireServicesSelectedDataSource() : base(new QuestionnaireServicesSelectedService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireServicesSelectedDataSourceView used by the QuestionnaireServicesSelectedDataSource.
		/// </summary>
		protected QuestionnaireServicesSelectedDataSourceView QuestionnaireServicesSelectedView
		{
			get { return ( View as QuestionnaireServicesSelectedDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireServicesSelectedDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireServicesSelectedSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireServicesSelectedSelectMethod selectMethod = QuestionnaireServicesSelectedSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireServicesSelectedSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireServicesSelectedDataSourceView class that is to be
		/// used by the QuestionnaireServicesSelectedDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireServicesSelectedDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireServicesSelected, QuestionnaireServicesSelectedKey> GetNewDataSourceView()
		{
			return new QuestionnaireServicesSelectedDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireServicesSelectedDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireServicesSelectedDataSourceView : ProviderDataSourceView<QuestionnaireServicesSelected, QuestionnaireServicesSelectedKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireServicesSelectedDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireServicesSelectedDataSourceView(QuestionnaireServicesSelectedDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireServicesSelectedDataSource QuestionnaireServicesSelectedOwner
		{
			get { return Owner as QuestionnaireServicesSelectedDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireServicesSelectedSelectMethod SelectMethod
		{
			get { return QuestionnaireServicesSelectedOwner.SelectMethod; }
			set { QuestionnaireServicesSelectedOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireServicesSelectedService QuestionnaireServicesSelectedProvider
		{
			get { return Provider as QuestionnaireServicesSelectedService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireServicesSelected> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireServicesSelected> results = null;
			QuestionnaireServicesSelected item;
			count = 0;
			
			System.Int32 _questionnaireServiceId;
			System.Int32 _questionnaireId;
			System.Int32 _questionnaireTypeId;
			System.Int32 _categoryId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireServicesSelectedSelectMethod.Get:
					QuestionnaireServicesSelectedKey entityKey  = new QuestionnaireServicesSelectedKey();
					entityKey.Load(values);
					item = QuestionnaireServicesSelectedProvider.Get(entityKey);
					results = new TList<QuestionnaireServicesSelected>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireServicesSelectedSelectMethod.GetAll:
                    results = QuestionnaireServicesSelectedProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireServicesSelectedSelectMethod.GetPaged:
					results = QuestionnaireServicesSelectedProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireServicesSelectedSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireServicesSelectedProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireServicesSelectedProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireServicesSelectedSelectMethod.GetByQuestionnaireServiceId:
					_questionnaireServiceId = ( values["QuestionnaireServiceId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireServiceId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireServicesSelectedProvider.GetByQuestionnaireServiceId(_questionnaireServiceId);
					results = new TList<QuestionnaireServicesSelected>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireServicesSelectedSelectMethod.GetByQuestionnaireIdQuestionnaireTypeId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_questionnaireTypeId = ( values["QuestionnaireTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireTypeId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireServicesSelectedProvider.GetByQuestionnaireIdQuestionnaireTypeId(_questionnaireId, _questionnaireTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireServicesSelectedSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireServicesSelectedProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireServicesSelectedSelectMethod.GetByCategoryId:
					_categoryId = ( values["CategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CategoryId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireServicesSelectedProvider.GetByCategoryId(_categoryId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireServicesSelectedSelectMethod.GetByQuestionnaireTypeId:
					_questionnaireTypeId = ( values["QuestionnaireTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireTypeId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireServicesSelectedProvider.GetByQuestionnaireTypeId(_questionnaireTypeId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireServicesSelectedSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireServicesSelectedProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireServicesSelectedSelectMethod.Get || SelectMethod == QuestionnaireServicesSelectedSelectMethod.GetByQuestionnaireServiceId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireServicesSelected entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireServicesSelectedProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireServicesSelected> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireServicesSelectedProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireServicesSelectedDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireServicesSelectedDataSource class.
	/// </summary>
	public class QuestionnaireServicesSelectedDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireServicesSelected, QuestionnaireServicesSelectedKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedDataSourceDesigner class.
		/// </summary>
		public QuestionnaireServicesSelectedDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireServicesSelectedSelectMethod SelectMethod
		{
			get { return ((QuestionnaireServicesSelectedDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireServicesSelectedDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireServicesSelectedDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireServicesSelectedDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireServicesSelectedDataSourceActionList : DesignerActionList
	{
		private QuestionnaireServicesSelectedDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireServicesSelectedDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireServicesSelectedDataSourceActionList(QuestionnaireServicesSelectedDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireServicesSelectedSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireServicesSelectedDataSourceActionList
	
	#endregion QuestionnaireServicesSelectedDataSourceDesigner
	
	#region QuestionnaireServicesSelectedSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireServicesSelectedDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireServicesSelectedSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireServiceId method.
		/// </summary>
		GetByQuestionnaireServiceId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdQuestionnaireTypeId method.
		/// </summary>
		GetByQuestionnaireIdQuestionnaireTypeId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByCategoryId method.
		/// </summary>
		GetByCategoryId,
		/// <summary>
		/// Represents the GetByQuestionnaireTypeId method.
		/// </summary>
		GetByQuestionnaireTypeId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireServicesSelectedSelectMethod

	#region QuestionnaireServicesSelectedFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedFilter : SqlFilter<QuestionnaireServicesSelectedColumn>
	{
	}
	
	#endregion QuestionnaireServicesSelectedFilter

	#region QuestionnaireServicesSelectedExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedExpressionBuilder : SqlExpressionBuilder<QuestionnaireServicesSelectedColumn>
	{
	}
	
	#endregion QuestionnaireServicesSelectedExpressionBuilder	

	#region QuestionnaireServicesSelectedProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireServicesSelectedChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireServicesSelected"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireServicesSelectedProperty : ChildEntityProperty<QuestionnaireServicesSelectedChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireServicesSelectedProperty
}

