﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_SafetyPQ_QuestionnaireCompareAssess" Codebehind="SafetyPQ_QuestionnaireCompareAssess.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
    
<%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>

<style type="text/css">
        .ci {COLOR:black; BACKGROUND-COLOR:#80FF80}
        .cd {COLOR:black; BACKGROUND-COLOR:#FF8080}
    </style>
    
    <script language="JavaScript">
      var needToConfirm = false;
      
      var confirmMsg = "Are you sure want to navigate away from this page?\n\nYou have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?\n\nPress OK to continue, or Cancel to stay on the current page.";
      //window.onbeforeload = setNeedToConfirmTrue;
      
      function setNeedToConfirmTrue()
      {
        if(needToConfirm == false)
        {
            needToConfirm = true;
        }
      }
      window.onbeforeunload = confirmExit;
      function confirmExit()
      {
        if (needToConfirm)
          return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save, your changes will be lost.  Are you sure you want to exit this page?";
      }
      function confirmSave()
      {
            needtoConfirm = false;
      }
</script>
            
            <table border="0" cellpadding="2" cellspacing="0" width="890">
    <tr>
        <td class="pageName" colspan="9" style="height: 17px">
            <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                    <a href="SafetyPQ_Questionnaire.aspx">Questionnaire</a> > <asp:LinkButton ID="LinkButton1" runat="server" meta:resourcekey="LinkButton1Resource1">Assess/Review</asp:LinkButton></span>&nbsp;
                    &gt; Compare & Review against previously completed Questionnaire<br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />
        </td>
    </tr>
                <tr>
                    <td class="pageName" colspan="5" style="width: 321px; height: 17px">
                    </td>
                    <td class="pageName" colspan="1" style="height: 17px; text-align: center">
                        <dxe:ASPxButton ID="btnSave" runat="server" AutoPostBack="True" CausesValidation="False"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text="Save"
                            UseSubmitBehavior="True" Font-Bold="True" meta:resourcekey="ASPxButton3Resource1" OnClick="btnSave_Click">
                            <ClientSideEvents Click="function(s, e) {
    confirmSave();
	e.processOnServer = confirm('Are you sure you wish to save your changes?');
}"></ClientSideEvents>
                        </dxe:ASPxButton>
                    </td>
                    <td class="pageName" colspan="1" style="height: 17px; text-align: center">
                        <dxe:ASPxButton ID="btnPrint" runat="server" AutoPostBack="False" CausesValidation="False"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text="Print" UseSubmitBehavior="False" meta:resourcekey="ASPxButton2Resource1">
                            <ClientSideEvents Click="function(s, e) {
	javascript:window.print();
}" />
                        </dxe:ASPxButton>
                    </td>
                    <td class="pageName" colspan="1" style="height: 17px; text-align: center">
                        <dxe:ASPxButton ID="btnHome" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        CssPostfix="Office2003Blue" OnClick="btnHome_Click" Text="Home" 
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm(confirmMsg);
}" />
                                    </dxe:ASPxButton>
                    </td>
                    <td class="pageName" colspan="1" style="width: 320px; height: 17px">
                    </td>
                </tr>
    <tr>
        <td class="pageName" colspan="9" style="height: 17px; text-align: center">
<asp:Panel runat="server" ID="panelSubmit" Width="800px" meta:resourcekey="panelSubmitResource1">
<br />
<DIV style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: black 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 1px solid; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: #ffffcc; TEXT-ALIGN: center"><SPAN style="FONT-SIZE: 13pt; COLOR: maroon"><STRONG><SPAN style="FONT-FAMILY: Verdana">
                        <asp:Label ID="lblCompanyName" runat="server" Font-Bold="True" meta:resourcekey="lblCompanyNameResource1"></asp:Label></SPAN><BR /></STRONG><SPAN style="COLOR: maroon"><SPAN style="FONT-SIZE: 10pt; FONT-FAMILY: Verdana"><asp:Label runat="server" ForeColor="Red" ID="lblError" meta:resourcekey="lblErrorResource1"></asp:Label></SPAN></SPAN></SPAN></DIV>
<br />
</asp:Panel>
</td>
</tr>
<tr>
<td colspan="9" style="text-align: center">
            <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="3" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" Width="850px">
                <ContentStyle>
                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                </ContentStyle>
                <TabPages>
                    <dxtc:TabPage Name="Questionnaire" Text="Questionnaire">
                        <ContentCollection>
                            <dxw:contentcontrol runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                                    style="width: 100%; border-collapse: collapse; empty-cells: show;">
                                    <tr>
                                        <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                            white-space: normal; height: 13px; text-align: center; width: 258px;">
                                            Question</td>
                                        <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                            border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                            Previous Questionnaire Answers</td>
                                        <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                            border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                            Current Questionnaire Answers</td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Company:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblCompany_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblCompany_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Created By:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblCreatedBy_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblCreatedBy_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Modified By:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblModifiedBy_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblModifiedBy_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Assessed By:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblAssessedBy_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblAssessedBy_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Status:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblStatus_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblStatus_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Procurement Risk Rating:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblProcurementRiskRating_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblProcurementRiskRating_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            33.055.1 Risk Rating:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl_330551RiskRating_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl_330551RiskRating_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Recommended:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblRecommended_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblRecommended_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Recommendation Comments:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblRecommendationComments_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblRecommendationComments_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 258px; height: 13px; text-align: right">
                                            Type:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblType_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lblType_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Name="Procurement" Text="Procurement">
                        <ContentCollection>
                            <dxw:contentcontrol runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                                    style="width: 100%; border-collapse: collapse; empty-cells: show;">
                                    <tr>
                                        <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                            white-space: normal; height: 13px; text-align: center; width: 458px;">
                                            Question</td>
                                        <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                            border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                            Previous Questionnaire Answers</td>
                                        <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                            border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                            Current Questionnaire Answers</td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            1 - First Name:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1FirstName_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1FirstName_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            1 - Last Name:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1LastName_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1LastName_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            1 -&nbsp; E-Mail Address:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1EmailAddress_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1EmailAddress_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            1 - Job Title:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1JobTitle_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1JobTitle_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            1 - Telephone:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1Telephone_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl1Telephone_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            2 - EHS Consultant:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2EhsConsultant_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2EhsConsultant_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            2 - Procurement Contact:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2ProcurementContact_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2ProcurementContact_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            2 - Contract Manager:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2ContractManager_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2ContractManager_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            2 - Original Requester Name:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2OriginalRequesterName_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2OriginalRequesterName_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            2 - Original Requester Telephone:</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2OriginalRequesterTelephone_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl2OriginalRequesterTelephone_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            3 - Brief Description of Work</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl3WorkDescription_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl3WorkDescription_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            4 - Primary Service offered by Contractor</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl4PrimaryService_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl4PrimaryService_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            4 - Other</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl4Other_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl4Other_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            5 - Number of people expected to work on site</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl5NoPeople_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl5NoPeople_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            7 - When is Service Required?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl7WhenServiceRequired_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl7WhenServiceRequired_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Is Pre-Qualification Required?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_PreQual_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_PreQual_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Q1 - Will all work completed by the contractor/subcontractor be completed off-site?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q1_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q1_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Q2 - Is the work completed within a defined and restricted area or travelling
                                            along a defined and restricted route?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q2_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q2_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Q3 - Does the work use, generate or expose employees to hazardous materials
                                            or chemicals?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q3_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q3_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Q4 - Does the work use tools and equipment which could create a significant
                                            hazard to themselves or others?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q4_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q4_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Q5 - Does the work require advanced layers of protection as safeguards from
                                            potential safety and health hazards?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q5_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q5_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            8 - Q6 - Works on-site within defined/restricted routes with no exposure to significant
                                            hazards and does not require advanced layers of protection?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q6_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl8_Q6_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Initial Risk Assessment</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_InitialRiskAssessment_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_InitialRiskAssessment_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q1 - Is the contractor/supplier fully (100%) supervised or fully escorted when
                                            on site?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q1_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q1_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q2 - Does the contractor/supplier have minimal exposure to production/maintenance
                                            areas?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q2_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q2_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q3 - Does the contractor/supplier have minimal exposure to hazardous risks when
                                            working on site?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q3_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q3_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q4 - Is the contractor on site more frequently and working with partial Alcoa
                                            supervision in production/maintenance areas?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q4_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q4_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q5 - Is the contractor on site more frequently and working with partial Alcoa
                                            supervision and exposed to medium levels of risk?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q5_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q5_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q6 - Is the contractor on site frequently and working under limited Alcoa supervision
                                            in production/maintenance areas?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q6_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q6_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q7 - Is the contractor on site frequently and working under limited Alcoa supervision
                                            and exposed to high levels of risk?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q7_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q7_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q8 - Is the contractor bidding for major maintenance or capital contract and
                                            mobilising a large (&gt;100 people) workforce on site?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q8_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q8_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q9 - Is the contractor seeking "embedded" status and will set up operations
                                            on Alcoa property?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q9_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q9_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q10 - Has the contractor had a serious or disabling injury, fatality to any
                                            employee or contractor within the last 5 years?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q10_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q10_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 458px; height: 13px; text-align: left">
                                            9 - Q11 - Has the contractor received any citation notices from any government agency
                                            as a result of an injury or fatality within the last 3 years?</td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q11_Previous" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                        <td class="dxgv" style="width: 972px; height: 13px; text-align: left">
                                            <dxe:ASPxLabel ID="lbl9_Q11_Current" runat="server">
                                            </dxe:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Name="Supplier" Text="Supplier">
                        <ContentCollection>
                            <dxw:contentcontrol runat="server"><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        1 - Date:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl1Date_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl1Date_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        2 - Legal Name of Company:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl2LegalCompanyName_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl2LegalCompanyName_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        3 - Company:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl3Company_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl3Company_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        4 - ABN:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl4ABN_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl4ABN_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        5 - Address - Street:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressStreet_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressStreet_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        5 - Address - Suburb:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressSuburb_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressSuburb_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        5 - Address - State:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressState_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressState_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        5 - Address - PostCode:
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressPostCode_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressPostCode_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        5 - Address - Country</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressCountry_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl5AddressCountry_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        6 - Contact Name</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl6ContactName_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl6ContactName_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        7 - Contact Title</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl7ContactTitle_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl7ContactTitle_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        8 - Contact E-Mail</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl8ContactEmail_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl8ContactEmail_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        9 - Contact Phone</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl9ContactPhone_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl9ContactPhone_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        10 - Contact Fax</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl10ContactFax_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl10ContactFax_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb10Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="smb10ApprovalComments_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        11 - Select all services that best describe the range of services offered by your
                                        company:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl11Services_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl11Services_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Other: If your range of services does not appear in the above list, please describe
                                        your service:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl11Other_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl11Other_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb11Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb11ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        12 - As part of your company’s safety process, are safety hazard assessments conducted,
                                        and written job specific safety plans prepared to eliminate safety hazards?<br />
                                        a) Yes. Please attach examples.<br />
                                        b) No</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl12_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl12_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Explain your company's usual process to eliminate safety hazards below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl12Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl12Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        12 - Example <em>(Please Upload a file which contains examples or supports your answer
                                            given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxHyperLink ID="shl12Example_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxHyperLink ID="shl12Example_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb12Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb12ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        13 - At what frequency does your company conduct and document safety activities,
                                        such as safety inductions, safety re-fresher training, safety meetings, or safety
                                        inspections?<br />
                                        a) Daily<br />
                                        b) Weekly<br />
                                        c) Monthly<br />
                                        d) Conducts a safety induction only.<br />
                                        e) None of the above.
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl13_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl13_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl13Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl13Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb13Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb13ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        14 - Do the following participate in the investigation of all significant incidents or
                                                accidents?<br />
                                            a) Owner or Senior Management.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Middle Management.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Lead Person.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        d) Other. Please specify below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14d_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14d_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 11px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 11px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 11px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl14Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb14Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb14ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        15 - Have the following changed in your company in the last 3 years?<br />
                                        a) Ownership. If “Yes,” please provide details below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        a) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15aComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15aComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Insurance Carrier. If “Yes,” please provide details below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15bComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15bComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Other significant changes which may favourably affect safety. If “Yes,” please
                                        provide details below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl15c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15cComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl15cComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb15Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb15ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        16 - Check all answers that describe the content of your company’s safety training.<br />
                                        a) Basic safety training complies with all local regulations and industry standards.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Select crafts and trades have completed advanced training beyond regulatory requirements.
                                        If “Yes,” please describe specific safety training below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16bComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16bComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Select crew leaders have completed advanced training beyond regulatory requirements.
                                        If “Yes,” please describe specific safety training below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16cComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl16cComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb16Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb16ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        17 - Documentation of basic safety training exists in the form of:<br />
                                        a) Internal certifications and records.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl17a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl17a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) External licenses and certificates issued by governments, professional organisations,
                                        trade associations or other recognised authority. If “Yes,” please attach an example
                                        or certificate issued by a recognised authority.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl17b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl17b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Example </strong><em>(Please Upload a file which contains examples or supports
                                            your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxHyperLink ID="shl17bExample_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxHyperLink ID="shl17bExample_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb17Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb17ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 12px; text-align: left">
                                        18 - What percentage of the work force has been working in the industry for 1 year
                                        or more?<br />
                                        a) 0% – 25%<br />
                                        b) 26% – 50%<br />
                                        c) 51% – 75%<br />
                                        d) 76% – 100%
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 12px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl18_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 12px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl18_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl18Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl18Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb18Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb18ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        19 - Your company has a lead person to crew ratio of:<br />
                                        a) Less than or equal to 1 to 7.<br />
                                        b) Between 1 to 8 and 1 to 11.<br />
                                        c) Between 1 to 12 and 1 to 25.<br />
                                        d) Greater than 1 to 25.
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl19_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl19_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl19Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        <dxe:ASPxLabel ID="slbl19Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb19Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb19ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        20 - The criteria used to qualify safety trainers are established by:<br />
                                        a) Internal training and certifications. If “Yes,” please describe below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        a) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20aComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20aComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) External training and certifications from governments, professional organisations,
                                        trade associations or other recognised authority. If “Yes,” please describe below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20bComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl20bComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb20Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb20ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        21 - Safety and scheduling coordination meetings with subcontractors are held:<br />
                                        a) Daily<br />
                                        b) Weekly when work is for longer than one week.<br />
                                        c) As needed.<br />
                                        d) No subcontractors used.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl21_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl21_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl21Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl21Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval:</strong></td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="scb21Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments:</strong></td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="smb21ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        22 - Indicate all that apply to your company’s substance abuse program.<br />
                                        a) Substance abuse is monitored using random substance tests.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 11px; text-align: left">
                                        b) Substance abuse is monitored using substance tests for cause or post incident.</td>
                                    <td class="dxgv" style="width: 572px; height: 11px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 11px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Substance abuse is monitored by crew leaders trained in substance abuse recognition.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl22Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb22Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb22ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        23 - Your company audits and documents safety meetings, job safety conditions or
                                        job safety performance at the rate of:<br />
                                        a) Daily. Please attach the written results from a safety audit.<br />
                                        b) Weekly. Please attach the written results from a safety audit.<br />
                                        c) Monthly. Please attach the written results from a safety audit.<br />
                                        d) Does not audit these.
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl23_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl23_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl23Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl23Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Example </strong><em>(Please Upload a file which contains examples or supports
                                            your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl23Example_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl23Example_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb23Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb23ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        24 - How often does the owner or the senior management of your company review the
                                        safety performance of work crews?<br />
                                        a) Weekly<br />
                                        b) Monthly<br />
                                        c) Quarterly<br />
                                        d) Other:
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl24_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl24_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl24Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl24Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb24Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb24ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        25 - Indicate all of the following that have occurred within the last 5 years to any
                                                personnel employed or contracted by your company while working.<br />
                                                a) A serious or disabling injury occurred due to any event. Examples of events which
                                                could cause a serious or disabling include excavation or trench collapse, scaffold
                                                failure, confined space entry incident, mobile equipment rollover or contact with
                                                electricity. If “Yes,” please attach details of the injury, the cause and the implemented
                                                corrective actions.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl25a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl25a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        a) <strong>Example </strong><em>(Please Upload a file which
                                            contains examples or supports your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl25aExample_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl25aExample_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) A serious or disabling injury occurred due to ergonomic factors. Examples of
                                        activities would include lifting, pulling, bending, reaching, and vibration resulting
                                        in strains or sprains. If “Yes”, please attach details of the injury, the cause
                                        and the implemented corrective actions.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl25b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl25b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) <strong>Example </strong><em>(Please Upload a file which
                                            contains examples or supports your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl25bExample_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl25bExample_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) A fatality occurred to anyone, including any personnel, visitor, member of the
                                        public or any other person due to any circumstances controlled by your company.
                                        If “Yes,” please attach details of the fatality, the cause and the implemented corrective
                                        actions.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl25c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl25c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) <strong>Example </strong><em>(Please Upload a file which
                                            contains examples or supports your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl25cExample_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl25cExample_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb25Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb25ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        26 - Does a government agency or any other group, such as an insurance carrier, require
                                                a log, record or similar document of reportable work related injuries or illnesses?
                                                Examples are Worksafe, workers compensation claims reports, insurance register of
                                                accidents, etc.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl26_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl26_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl26Comments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl26Comments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Example </strong><em>(Please Upload a file which
                                            contains examples or supports your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl26Example_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl26Example_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb26Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb26ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        27 - Enter the following information for the last three years for the total company.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        a) Total number of lost workday and restricted work cases. (not number of days lost/restricted)</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Total number of medical treatment cases.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Total number of first aid cases.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        d) Total number of injury free events reported.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27d_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27d_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        e) Total hourly and salaried hours worked.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27e_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl27e_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb27Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb27ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        28 - For the last three years, please indicate your Industry Levy Category and Company
                                                Net Levy Rate as allocated by ‘WORKCOVER’, or indicate if you were a self-insurer.<br />
                                                </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        a) Industry Code</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28a_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28a_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Industry Rate (%)</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28b_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28b_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Company Rate (%)</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28c_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl28c_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb28Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb28ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        29 - i) Did your company receive any serious, repeat or criminal citations for health or
                                                safety in the last 3 years that involved:
                                               </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29i_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29i_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                         a) Work at a customer’s site, but no injuries or fatalities occurred? If “Yes,”
                                                please describe below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ia_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ia_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        a) Comments</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29iaComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29iaComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Work at a customer’s site where injuries or fatalities occurred? If “Yes,” please
                                        describe below</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ib_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ib_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        b) Comments</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ibComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ibComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Any work other than at a customer’s site, but no injuries or fatalities occurred?
                                        If “Yes,” please describe below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ic_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ic_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        c) Comments</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29icComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29icComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        d) Any work other than at a customer’s site where injuries or fatalities occurred?
                                        If “Yes,” please describe below.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29id_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29id_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        d) Comments</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29idComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29idComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb29iApproval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb29iApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        29 - ii) Has your company received any notices (Prohibition, Improvement, Citations, breaches
                                                of EHS compliance, etc) from any of the regulatory authorities, in the past three
                                                years?<br />
                                                If “Yes,” please attach copies.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ii_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29ii_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Comments:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29iiComments_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl29iiComments_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Example </strong><em>(Please Upload a file which contains examples or supports
                                            your answer given above)</em></td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl29iiExample_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                         <dxe:ASPxHyperLink ID="shl29iiExample_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb29iiApproval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb29iiApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        &nbsp;</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        30 - Has your company worked at Alcoa locations during the last 3 years?<br />
                                                a) Routinely, more than 3 times.<br />
                                                b) Periodically, 2 or 3 times.<br />
                                                c) Once or never before.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30abc_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30abc_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        Please list 3 Alcoa locations, the contact, the dates and the scope of past work.<br />
                                        #1:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        #2:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        #3:</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    <dxe:ASPxLabel ID="slbl30_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxComboBox ID="scb30Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ReadOnly="false"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxMemo ID="smb30ApprovalComments_Current" runat="server" Height="71px" Width="300px" ReadOnly="False">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table></dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                    <dxtc:TabPage Name="Verification" Text="Verification">
                        <ContentCollection>
                            <dxw:contentcontrol runat="server"><span style="font-size: 14pt; color: #0000ff;
                                font-style: normal; font-family: Arial">Section 1 - Management Commitment to Safety<br />
                            </span><span style="font-size: 11pt; color: black; font-family: Arial">Each Contractor
                                is expected to have an EH&amp;S program which demonstrates that the organisation
                                is committed to the health and safety of its employees and anyone else that may
                                be affected through its organisational activities.<br />
                            </span><br /><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 1 - 1<br />
                                        </strong>Does your company have an EH&amp;S Policy which is current, signed by the
                                        Senior Management Representative, and is reviewed regularly to ensure that it remains
                                        relevant and appropriate to the company health and safety risks?
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl1_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl1_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>EH&amp;S Policy is current and signed by the Senior Management Representative.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxHyperLink ID="vhl1_1_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxHyperLink ID="vhl1_1_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb1_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb1_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 1 - 2<br />
                                        </strong>Do Management Representatives actively participate in Safety Meetings with
                                        employees?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxLabel ID="vlbl1_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxLabel ID="vlbl1_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Current copies of Safety Meeting minutes showing attendees.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxHyperLink ID="vhl1_2_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxHyperLink ID="vhl1_2_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb1_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb1_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 1 - 3<br />
                                        </strong>Does your company have Job Descriptions for all levels of Line Management
                                        that include Health and Safety Responsibilities?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl1_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl1_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Documented evidence of Job Descriptions for all levels of Line Management within
                                        the organisation.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxHyperLink ID="vhl1_3_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxHyperLink ID="vhl1_3_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb1_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb1_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 1 - 4</strong><br />
                                        Does your company have a process for resolving health and safety issues?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl1_4_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl1_4_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Documented process outlining the issue resolution process and key personnel
                                        involved.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxHyperLink ID="vhl1_4_Previous" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    <dxe:ASPxHyperLink ID="vhl1_4_Current" runat="server" Text="">
                                        </dxe:ASPxHyperLink>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb1_4_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb1_4_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table><br /><span style="font-size: 14pt; color: #0000ff; font-family: Arial">Section
                                2 - Hazard &amp; Risk Management<br />
                            </span><span style="font-size: 11pt; color: black; font-family: Arial">The Contractor
                                shall identify any potential hazards associated with the Scope of Work, including
                                work by its sub-contractors, and decide on appropriate actions to eliminate or reduce
                                unacceptable risks to as low as reasonably practicable.<br />
                            </span><br /><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 2 - 1<br />
                                        </strong>EH&amp;S Risk Management Program that complies with relevant legislation.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_1_Previous" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_1_Current" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>EH&amp;S Risk Management Program that complies with relevant legislation.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_1_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_1_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb2_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb2_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 2 - 2<br />
                                        </strong>Is there a system for employees to report and record EH&amp;S hazards?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_2_Previous" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_2_Current" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Provide a copy of your current procedure and/or register.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_2_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_2_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb2_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue ">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb2_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 2 - 3<br />
                                        </strong>Are Hazard Risk Assessments completed for proposed activities?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_3_Previous" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_3_Current" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Provide a copy of your current procedure.
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_3_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_3_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb2_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb2_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 2 - 4</strong><br />
                                        Are Safe Work Method Statements, Standard Work Instructions and JSA's developed
                                        for all activities within the contractor's scope of work?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_4_Previous" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center"><dxe:ASPxLabel ID="vlbl2_4_Current" runat="server">
                                    </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Provide a copy of your register for Safe Work Instructions (SWI) and/or
                                        Safe Work Methods (SWM), and Job Safety Analysis's (JSA's).</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_4_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_4_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb2_4_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb2_4_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 2 - 5<br />
                                        </strong>Are Standard Work Instructions and Job Safety Analysis approved by the
                                        relevant supervisor?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl2_5_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl2_5_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Provide a copy of a completed Safe Work Instruction (SWI) and a completed Job Safety
                                        Analysis (JSA).</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_5_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl2_5_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb2_5_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb2_5_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table><br /><span style="font-size: 14pt; color: #0000ff; font-family: Arial">Section
                                3 - People Skills and Training<br />
                                <span style="font-size: 11pt; color: black; font-family: Arial">The Contractor will
                                    have established Systems and Procedures that ensure all employees are trained to
                                    fulfil their roles and responsibilities, and are competent to perform all tasks
                                    in a way that is safe and does not adversely impact on themselves, others or the
                                    environment.<br />
                                </span>
                                <br />
                            </span><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 3 - 1<br />
                                        </strong>Has an EHS training needs analysis for all employees been completed and
                                        documented?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>EH&amp;S Training Needs Analysis.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_1_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_1_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb3_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb3_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 3 - 2<br />
                                        </strong>Has an EH&amp;S Training Plan been developed and documented to address
                                        the identified needs?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>EH&amp;S Training Plan or Matrix.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_2_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_2_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb3_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb3_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 3 - 3<br />
                                        </strong>Are records of EH&amp;S Training accurate and up to date?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Provide summary of the procedures that are used for tracking, recording, reviewing
                                        and renewing EHS training.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_3_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_3_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb3_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb3_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blu dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 3 - 4</strong><br />
                                        Please provide a copy of your training needs analysis for all of your management
                                        and supervision.
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_4_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_4_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Please provide a copy of your training needs analysis for all of your management
                                        and supervision.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_4_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_4_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb3_4_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb3_4_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 3 - 5<br />
                                        </strong>Have employees been evaluated or tested to ensure that they are competent
                                        to apply the training outcomes?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_5_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl3_5_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Provide a copy of your competency testing procedure such as the Records of employee
                                        EHS competencies, as per the established Contractor EHS training systems and procedures.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_5_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl3_5_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb3_5_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb3_5_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table><br /><span style="font-size: 14pt; color: #0000ff; font-family: Arial">Section
                                4 - Health and Safety Performance Management<br />
                                <span style="font-size: 11pt; font-family: Arial"><span style="color: black">The Contractor
                                    has established targets for thier Key Performance Indicators. Measurement and reporting
                                    processes are in place.<br />
                                </span></span>
                                <br />
                            </span><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 4 - 1<br />
                                        </strong>Are EHS Audits conducted?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Copies of completed EHS audits, that show frequency of audits and improvement
                                        opportunities have been identified and actioned.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_1_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_1_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb4_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td colspan="2" class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb4_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 4 - 2<br />
                                        </strong>Are Work Contacts and Compliance Inspections performed by representatives
                                        of Line Management?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Are safety compliance inspections performed by representatives of line
                                        management?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_2_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_2_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb4_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb4_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 4 - 3<br />
                                        </strong>Are Workplace Safety &amp; Housekeeping Inspections conducted regularly?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Checklist is available, schedule that shows frequency of Inspections.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_3_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_3_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb4_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb4_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 4 - 4</strong><br />
                                        Are Job Safety Analysis (JSA) field audits conducted regularly?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_4_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_4_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Provide a copy of your Job Safety Analysis (JSA) audit schedule.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_4_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_4_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb4_4_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb4_4_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 4 - 5<br />
                                        </strong>Are safety meetings conducted regularly?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_5_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_5_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Scheudle that shows frequency of meetings and Meeting Minutes.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_5_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_5_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb4_5_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb4_5_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 4 - 6<br />
                                        </strong>Are EH&amp;S Meetings attended by both representatives of Line Management
                                        and Employees?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_6_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl4_6_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Record of Attendance shows representatives of the Management Team and workforce
                                        at the EH&amp;S Meetings.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_6_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl4_6_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb4_6_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb4_6_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table><br /><span style="font-size: 14pt; color: #0000ff; font-family: Arial">Section
                                5 - Communication<br />
                                <span style="font-size: 11pt; color: black; font-family: Arial">Regular structured EH&amp;S
                                    communication programs shall be established and implemented by the contractor organisation
                                    to provide a base for communciating potential issues, EH&amp;S hazards, EH&amp;S
                                    initiatives and programs. It is important in any organisation that EH&amp;S issues
                                    are communciated effectively across all levels of the workforce.<br />
                                </span>
                                <br />
                            </span><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 5 - 1<br />
                                        </strong>Are Prestart Meetings held prior to the commencement of work for each shift
                                        and during the course of the day if the work group is transferred to a new task
                                        or location?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 11px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Meeting Minutes should confirm that the meeting is run to a set agenda
                                        with immediate action taken on any matters raised.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_1_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_1_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb5_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb5_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 5 - 2<br />
                                        </strong>Do Management Representatives actively participate in Safety Meetings with
                                        employees?Are these meetings conducted by the Supervisor for the particular work
                                        group to discuss the planned work for the day?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Meeting Record of Attendance identifies the Supervisor and workforce attendees.
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_2_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_2_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb5_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb5_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 5 - 3<br />
                                        </strong>Are Close-out Briefings or handover meetings held at the end of the standard
                                        work shift?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Meeting Minutes of Attendance, confirming immediate action taken on any EH&amp;S
                                        matters raised.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_3_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_3_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb5_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb5_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 5 - 4</strong><br />
                                        Does the contractor require sub contractors representatives to attend safety meetings?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_4_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl5_4_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Meeting Minutes of Attendance, confirming immediate action taken on any
                                        EH&amp;S matters raised.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_4_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl5_4_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb5_4_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb5_4_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table><br /><span style="font-size: 14pt; color: #0000ff; font-family: Arial">Section
                                6 - Incident and Injury Investigation<br />
                            </span><span style="font-size: 11pt; color: black; font-family: Arial"><span style="font-size: 11pt;
                                color: black; font-family: Arial">All incidents that result in an injury shall be
                                reported and investigated.<br />
                                The Contractor shall investigate and record the details for all incidents involving
                                near misses, property or equipment damage, or the environment.<br />
                                <br />
                            </span></span><table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                <tr>
                                    <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                        Question</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Previous Questionnaire Answers</td>
                                    <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                        Current Questionnaire Answers</td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Section 6 - 1<br />
                                        </strong>Describe the process for incidents and injury reporting process.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl6_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl6_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 10px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Provide copies of your injury/incident reporting process or procedure.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl6_1_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl6_1_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb6_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb6_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 6px; text-align: left">
                                        <strong>Section 6 - 2<br />
                                        </strong>Describe the investigation process used, level of participation by management
                                        and their input into the investigations.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl6_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl6_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 10px; text-align: left">
                                        <strong>Evidence<br />
                                        </strong>Provide copies of investigations into incidents or near misses, property
                                        or equipment damage, or environment.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl6_2_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl6_2_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb6_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb6_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 10px; text-align: left">
                                        <strong>Section 6 - 3<br />
                                        </strong>Do Incident Investigations include participation by Key Management Representatives?</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl6_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl6_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        <strong>Evidence</strong><br />
                                        Attendees of recent Incident Investigations are recorded on the report documents.</td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl6_3_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl6_3_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxComboBox ID="vcb6_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr class="dxgvDataRow_Office2003_Blue">
                                    <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                        <strong>Approval Comments</strong>:</td>
                                    <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                        <dxe:ASPxMemo ID="vmb6_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                    </td>
                                </tr>
                            </table><br /><span style="font-size: 14pt; color: #0000ff; font-family: Arial">Section
                                7 - Health and Safety Document and Records Management<br />
                                    <span style="font-size: 11pt; color: black; font-family: Arial">The contractor maintains
                                        an administrative system to control EH&amp;S documentation that ensures currency
                                        and relevancy for all legal and other requirements that are directly applicable
                                        to EH&amp;S.</span>
                            </span>
                                    <br />
                                <table border="0" cellpadding="0" cellspacing="0"
                                class="dxgvControl_Office2003_Blue" style="width: 100%; border-collapse: collapse;
                                empty-cells: show;">
                                    <tr>
                                        <td class="dxgvHeader_Office2003_Blue" style="border-top-width: 0px; border-left-width: 0px;
                                        white-space: normal; height: 13px; text-align: center; width: 558px;">
                                            Question</td>
                                        <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                            Previous Questionnaire Answers</td>
                                        <td class="dxgvHeader_Office2003_Blue" colspan="1" style="border-top-width: 0px;
                                        border-left-width: 0px; white-space: normal; height: 13px; text-align: center">
                                            Current Questionnaire Answers</td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                            <strong>Section 7 - 1<br />
                                            </strong>EH&amp;S Document Control Register.</td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl7_1_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl7_1_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 10px; text-align: left">
                                            <strong>Evidence<br />
                                            </strong>What system does your company use for records access management?</td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl7_1_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl7_1_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                            <strong>Approval</strong>:</td>
                                        <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                            <dxe:ASPxComboBox ID="vcb7_1_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                            <strong>Approval Comments</strong>:</td>
                                        <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                            <dxe:ASPxMemo ID="vmb7_1_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 6px; text-align: left">
                                            <strong>Section 7 - 2<br />
                                            </strong>What system does your company use for records access management?</td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl7_2_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl7_2_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 10px; text-align: left">
                                            <strong>Evidence<br />
                                            </strong>Describe the company administrative system. EH&amp;S documents are secure
                                            and accessible by relevant persons.</td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl7_2_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl7_2_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                            <strong>Approval</strong>:</td>
                                        <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                            <dxe:ASPxComboBox ID="vcb7_2_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                            <strong>Approval Comments</strong>:</td>
                                        <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                            <dxe:ASPxMemo ID="vmb7_2_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: left">
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 10px; text-align: left">
                                            <strong>Section 7 - 3<br />
                                            </strong>Are all EH&amp;S records maintained according to the EH&amp;S Document
                                            Control Register?</td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl7_3_Previous" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                        <dxe:ASPxLabel ID="vlbl7_3_Current" runat="server">
                                        </dxe:ASPxLabel>
                                    </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: left">
                                            <strong>Evidence</strong><br />
                                            EH&amp;S documents are secured and accessible by relevant persons.</td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl7_3_Previous" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td class="dxgv" style="width: 572px; height: 13px; text-align: center">
                                            <dxe:ASPxHyperLink ID="vhl7_3_Current" runat="server" Text="">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                            <strong>Approval</strong>:</td>
                                        <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                            <dxe:ASPxComboBox ID="vcb7_3_Approval_Current" runat="server" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css"
                                            CssPostfix="PlasticBlue" IncrementalFilteringMode="StartsWith" SelectedIndex="2"
                                            ShowImageInEditBox="True" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css"
                                            ValueType="System.Int32" Width="300px">
                                            <Items>
                                                <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                <dxe:ListEditItem Selected="True" Text="(Select Approval)" />
                                            </Items>
                                            <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                            </LoadingPanelImage>
                                            <ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False">
                                                <ErrorFrameStyle ImageSpacing="4px">
                                                    <ErrorTextPaddings PaddingLeft="4px" />
                                                </ErrorFrameStyle>
                                            </ValidationSettings>
                                        </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr class="dxgvDataRow_Office2003_Blue dxgvDataRowAlt_Office2003_Blue">
                                        <td class="dxgv" style="width: 558px; height: 13px; text-align: right">
                                            <strong>Approval Comments</strong>:</td>
                                        <td class="dxgv" colspan="2" style="height: 13px; text-align: center">
                                            <dxe:ASPxMemo ID="vmb7_3_Approval_Current" runat="server" Height="71px" Width="300px">
                                        </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxw:contentcontrol>
                        </ContentCollection>
                    </dxtc:TabPage>
                    </TabPages>
            </dxtc:ASPxPageControl>
</td>
</tr>
</table>
<br />