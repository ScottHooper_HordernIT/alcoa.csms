﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyHrMapService
    {
        CompaniesHrMap Get(Expression<Func<CompaniesHrMap, bool>> where, List<Expression<Func<CompaniesHrMap, object>>> includes);
        List<CompaniesHrMap> GetMany(int? top, Expression<Func<CompaniesHrMap, bool>> where, Func<IQueryable<CompaniesHrMap>, IOrderedQueryable<CompaniesHrMap>> orderBy, List<Expression<Func<CompaniesHrMap, object>>> includes);
        List<CompaniesHrMap> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesHrMap, bool>> where, Func<IQueryable<CompaniesHrMap>, IOrderedQueryable<CompaniesHrMap>> orderBy, List<Expression<Func<CompaniesHrMap, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompaniesHrMap, bool>> where, List<Expression<Func<CompaniesHrMap, object>>> includes);
        void Insert(CompaniesHrMap item, bool saveChanges = true);
        void Update(CompaniesHrMap item, bool saveChanges = true);
        void Delete(CompaniesHrMap item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyHrMapService : ICompanyHrMapService
    {
        private readonly IRepository<CompaniesHrMap> _repository;

        public CompanyHrMapService(IRepository<CompaniesHrMap> repository)
        {
            this._repository = repository;
        }

        public CompaniesHrMap Get(Expression<Func<CompaniesHrMap, bool>> where, List<Expression<Func<CompaniesHrMap, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompaniesHrMap> GetMany(int? top, Expression<Func<CompaniesHrMap, bool>> where, Func<IQueryable<CompaniesHrMap>, IOrderedQueryable<CompaniesHrMap>> orderBy, List<Expression<Func<CompaniesHrMap, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompaniesHrMap> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompaniesHrMap, bool>> where, Func<IQueryable<CompaniesHrMap>, IOrderedQueryable<CompaniesHrMap>> orderBy, List<Expression<Func<CompaniesHrMap, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompaniesHrMap, bool>> where, List<Expression<Func<CompaniesHrMap, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompaniesHrMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesHrMap");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompaniesHrMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesHrMap");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompaniesHrMap item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompaniesHrMap");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
