﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EscalationChainSafetyProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EscalationChainSafetyProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EscalationChainSafety, KaiZen.CSMS.Entities.EscalationChainSafetyKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainSafetyKey key)
		{
			return Delete(transactionManager, key.EscalationChainSafetyId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_escalationChainSafetyId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _escalationChainSafetyId)
		{
			return Delete(null, _escalationChainSafetyId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainSafetyId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _escalationChainSafetyId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafetyRoles key.
		///		FK_EscalationChainSafety_EscalationChainSafetyRoles Description: 
		/// </summary>
		/// <param name="_level"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByLevel(System.Int32 _level)
		{
			int count = -1;
			return GetByLevel(_level, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafetyRoles key.
		///		FK_EscalationChainSafety_EscalationChainSafetyRoles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		/// <remarks></remarks>
		public TList<EscalationChainSafety> GetByLevel(TransactionManager transactionManager, System.Int32 _level)
		{
			int count = -1;
			return GetByLevel(transactionManager, _level, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafetyRoles key.
		///		FK_EscalationChainSafety_EscalationChainSafetyRoles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByLevel(TransactionManager transactionManager, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetByLevel(transactionManager, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafetyRoles key.
		///		fkEscalationChainSafetyEscalationChainSafetyRoles Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_level"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByLevel(System.Int32 _level, int start, int pageLength)
		{
			int count =  -1;
			return GetByLevel(null, _level, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafetyRoles key.
		///		fkEscalationChainSafetyEscalationChainSafetyRoles Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_level"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByLevel(System.Int32 _level, int start, int pageLength,out int count)
		{
			return GetByLevel(null, _level, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafetyRoles key.
		///		FK_EscalationChainSafety_EscalationChainSafetyRoles Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public abstract TList<EscalationChainSafety> GetByLevel(TransactionManager transactionManager, System.Int32 _level, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafety key.
		///		FK_EscalationChainSafety_EscalationChainSafety Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByUserId(System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafety key.
		///		FK_EscalationChainSafety_EscalationChainSafety Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		/// <remarks></remarks>
		public TList<EscalationChainSafety> GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafety key.
		///		FK_EscalationChainSafety_EscalationChainSafety Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafety key.
		///		fkEscalationChainSafetyEscalationChainSafety Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByUserId(System.Int32 _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafety key.
		///		fkEscalationChainSafetyEscalationChainSafety Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public TList<EscalationChainSafety> GetByUserId(System.Int32 _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EscalationChainSafety_EscalationChainSafety key.
		///		FK_EscalationChainSafety_EscalationChainSafety Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EscalationChainSafety objects.</returns>
		public abstract TList<EscalationChainSafety> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EscalationChainSafety Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainSafetyKey key, int start, int pageLength)
		{
			return GetByEscalationChainSafetyId(transactionManager, key.EscalationChainSafetyId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EscalationChainSafety index.
		/// </summary>
		/// <param name="_escalationChainSafetyId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetByEscalationChainSafetyId(System.Int32 _escalationChainSafetyId)
		{
			int count = -1;
			return GetByEscalationChainSafetyId(null,_escalationChainSafetyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainSafety index.
		/// </summary>
		/// <param name="_escalationChainSafetyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetByEscalationChainSafetyId(System.Int32 _escalationChainSafetyId, int start, int pageLength)
		{
			int count = -1;
			return GetByEscalationChainSafetyId(null, _escalationChainSafetyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainSafety index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainSafetyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetByEscalationChainSafetyId(TransactionManager transactionManager, System.Int32 _escalationChainSafetyId)
		{
			int count = -1;
			return GetByEscalationChainSafetyId(transactionManager, _escalationChainSafetyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainSafety index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainSafetyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetByEscalationChainSafetyId(TransactionManager transactionManager, System.Int32 _escalationChainSafetyId, int start, int pageLength)
		{
			int count = -1;
			return GetByEscalationChainSafetyId(transactionManager, _escalationChainSafetyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainSafety index.
		/// </summary>
		/// <param name="_escalationChainSafetyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetByEscalationChainSafetyId(System.Int32 _escalationChainSafetyId, int start, int pageLength, out int count)
		{
			return GetByEscalationChainSafetyId(null, _escalationChainSafetyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainSafety index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainSafetyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EscalationChainSafety GetByEscalationChainSafetyId(TransactionManager transactionManager, System.Int32 _escalationChainSafetyId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EscalationChainSafety_SiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainSafety&gt;"/> class.</returns>
		public TList<EscalationChainSafety> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(null,_siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainSafety&gt;"/> class.</returns>
		public TList<EscalationChainSafety> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainSafety&gt;"/> class.</returns>
		public TList<EscalationChainSafety> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainSafety&gt;"/> class.</returns>
		public TList<EscalationChainSafety> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainSafety&gt;"/> class.</returns>
		public TList<EscalationChainSafety> GetBySiteId(System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;EscalationChainSafety&gt;"/> class.</returns>
		public abstract TList<EscalationChainSafety> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EscalationChainSafety_SiteId_Level index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetBySiteIdLevel(System.Int32 _siteId, System.Int32 _level)
		{
			int count = -1;
			return GetBySiteIdLevel(null,_siteId, _level, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId_Level index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetBySiteIdLevel(System.Int32 _siteId, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdLevel(null, _siteId, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId_Level index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetBySiteIdLevel(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _level)
		{
			int count = -1;
			return GetBySiteIdLevel(transactionManager, _siteId, _level, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId_Level index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetBySiteIdLevel(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdLevel(transactionManager, _siteId, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId_Level index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainSafety GetBySiteIdLevel(System.Int32 _siteId, System.Int32 _level, int start, int pageLength, out int count)
		{
			return GetBySiteIdLevel(null, _siteId, _level, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EscalationChainSafety_SiteId_Level index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EscalationChainSafety GetBySiteIdLevel(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _level, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EscalationChainSafety&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EscalationChainSafety&gt;"/></returns>
		public static TList<EscalationChainSafety> Fill(IDataReader reader, TList<EscalationChainSafety> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EscalationChainSafety c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EscalationChainSafety")
					.Append("|").Append((System.Int32)reader[((int)EscalationChainSafetyColumn.EscalationChainSafetyId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EscalationChainSafety>(
					key.ToString(), // EntityTrackingKey
					"EscalationChainSafety",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EscalationChainSafety();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EscalationChainSafetyId = (System.Int32)reader[((int)EscalationChainSafetyColumn.EscalationChainSafetyId - 1)];
					c.Level = (System.Int32)reader[((int)EscalationChainSafetyColumn.Level - 1)];
					c.SiteId = (System.Int32)reader[((int)EscalationChainSafetyColumn.SiteId - 1)];
					c.UserId = (System.Int32)reader[((int)EscalationChainSafetyColumn.UserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EscalationChainSafety entity)
		{
			if (!reader.Read()) return;
			
			entity.EscalationChainSafetyId = (System.Int32)reader[((int)EscalationChainSafetyColumn.EscalationChainSafetyId - 1)];
			entity.Level = (System.Int32)reader[((int)EscalationChainSafetyColumn.Level - 1)];
			entity.SiteId = (System.Int32)reader[((int)EscalationChainSafetyColumn.SiteId - 1)];
			entity.UserId = (System.Int32)reader[((int)EscalationChainSafetyColumn.UserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EscalationChainSafety entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EscalationChainSafetyId = (System.Int32)dataRow["EscalationChainSafetyId"];
			entity.Level = (System.Int32)dataRow["Level"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.UserId = (System.Int32)dataRow["UserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainSafety"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EscalationChainSafety Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainSafety entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region LevelSource	
			if (CanDeepLoad(entity, "EscalationChainSafetyRoles|LevelSource", deepLoadType, innerList) 
				&& entity.LevelSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.Level;
				EscalationChainSafetyRoles tmpEntity = EntityManager.LocateEntity<EscalationChainSafetyRoles>(EntityLocator.ConstructKeyFromPkItems(typeof(EscalationChainSafetyRoles), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LevelSource = tmpEntity;
				else
					entity.LevelSource = DataRepository.EscalationChainSafetyRolesProvider.GetByLevel(transactionManager, entity.Level);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LevelSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LevelSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.EscalationChainSafetyRolesProvider.DeepLoad(transactionManager, entity.LevelSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LevelSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.UserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EscalationChainSafety object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EscalationChainSafety instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EscalationChainSafety Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainSafety entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LevelSource
			if (CanDeepSave(entity, "EscalationChainSafetyRoles|LevelSource", deepSaveType, innerList) 
				&& entity.LevelSource != null)
			{
				DataRepository.EscalationChainSafetyRolesProvider.Save(transactionManager, entity.LevelSource);
				entity.Level = entity.LevelSource.Level;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EscalationChainSafetyChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EscalationChainSafety</c>
	///</summary>
	public enum EscalationChainSafetyChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>EscalationChainSafetyRoles</c> at LevelSource
		///</summary>
		[ChildEntityType(typeof(EscalationChainSafetyRoles))]
		EscalationChainSafetyRoles,
			
		///<summary>
		/// Composite Property for <c>Users</c> at UserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion EscalationChainSafetyChildEntityTypes
	
	#region EscalationChainSafetyFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EscalationChainSafetyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyFilterBuilder : SqlFilterBuilder<EscalationChainSafetyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyFilterBuilder class.
		/// </summary>
		public EscalationChainSafetyFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainSafetyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainSafetyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainSafetyFilterBuilder
	
	#region EscalationChainSafetyParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EscalationChainSafetyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyParameterBuilder : ParameterizedSqlFilterBuilder<EscalationChainSafetyColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyParameterBuilder class.
		/// </summary>
		public EscalationChainSafetyParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainSafetyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainSafetyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainSafetyParameterBuilder
	
	#region EscalationChainSafetySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EscalationChainSafetyColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafety"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EscalationChainSafetySortBuilder : SqlSortBuilder<EscalationChainSafetyColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetySqlSortBuilder class.
		/// </summary>
		public EscalationChainSafetySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EscalationChainSafetySortBuilder
	
} // end namespace
