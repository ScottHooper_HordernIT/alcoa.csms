<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_MT" Codebehind="ComplianceReport_MT.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" ClientInstanceName="grid" Enabled="False" 
    Width="100%">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <Columns>
        <dxwgv:GridViewDataTextColumn Caption="Site Name" SortIndex="1" SortOrder="Ascending"
                                                    FieldName="SiteName" VisibleIndex="0">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CompanySiteCategoryId" SortIndex="0" SortOrder="Ascending" UnboundType="String"
            VisibleIndex="2" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Jan" ReadOnly="True" Name="Jan" VisibleIndex="3">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Feb" ReadOnly="True" Name="Feb" VisibleIndex="4">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Mar" ReadOnly="True" Name="Mar" VisibleIndex="5">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Apr" ReadOnly="True" Name="Apr" VisibleIndex="6">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="May" ReadOnly="True" Name="May" VisibleIndex="7">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Jun" ReadOnly="True" Name="Jun" VisibleIndex="8">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Jul" ReadOnly="True" Name="Jul" VisibleIndex="9">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Aug" ReadOnly="True" Name="Aug" VisibleIndex="10">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Sep" ReadOnly="True" Name="Sep" VisibleIndex="11">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Oct" ReadOnly="True" Name="Oct" VisibleIndex="12">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Nov" ReadOnly="True" Name="Nov" VisibleIndex="13">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Dec" ReadOnly="True" Name="Dec" VisibleIndex="14">
        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
        <CellStyle HorizontalAlign="Center"></CellStyle>
        <PropertiesTextEdit NullDisplayText="-"></PropertiesTextEdit>
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server" SelectMethod="GetPaged"
            EnablePaging="True" EnableSorting="True">
        </data:CompanySiteCategoryDataSource>