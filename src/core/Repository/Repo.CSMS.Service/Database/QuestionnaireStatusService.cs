﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireStatusService
    {
        QuestionnaireStatus Get(Expression<Func<QuestionnaireStatus, bool>> where, List<Expression<Func<QuestionnaireStatus, object>>> includes);
        List<QuestionnaireStatus> GetMany(int? top, Expression<Func<QuestionnaireStatus, bool>> where, Func<IQueryable<QuestionnaireStatus>, IOrderedQueryable<QuestionnaireStatus>> orderBy, List<Expression<Func<QuestionnaireStatus, object>>> includes);
        List<QuestionnaireStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireStatus, bool>> where, Func<IQueryable<QuestionnaireStatus>, IOrderedQueryable<QuestionnaireStatus>> orderBy, List<Expression<Func<QuestionnaireStatus, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireStatus, bool>> where, List<Expression<Func<QuestionnaireStatus, object>>> includes);
        void Insert(QuestionnaireStatus item, bool saveChanges = true);
        void Update(QuestionnaireStatus item, bool saveChanges = true);
        void Delete(QuestionnaireStatus item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireStatusService : IQuestionnaireStatusService
    {
        private readonly IRepository<QuestionnaireStatus> _repository;

        public QuestionnaireStatusService(IRepository<QuestionnaireStatus> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireStatus Get(Expression<Func<QuestionnaireStatus, bool>> where, List<Expression<Func<QuestionnaireStatus, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireStatus> GetMany(int? top, Expression<Func<QuestionnaireStatus, bool>> where, Func<IQueryable<QuestionnaireStatus>, IOrderedQueryable<QuestionnaireStatus>> orderBy, List<Expression<Func<QuestionnaireStatus, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireStatus, bool>> where, Func<IQueryable<QuestionnaireStatus>, IOrderedQueryable<QuestionnaireStatus>> orderBy, List<Expression<Func<QuestionnaireStatus, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireStatus, bool>> where, List<Expression<Func<QuestionnaireStatus, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireStatus");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireStatus");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireStatus");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
