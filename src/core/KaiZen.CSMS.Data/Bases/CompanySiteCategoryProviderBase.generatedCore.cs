﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanySiteCategoryProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanySiteCategory, KaiZen.CSMS.Entities.CompanySiteCategoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryKey key)
		{
			return Delete(transactionManager, key.CompanySiteCategoryId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companySiteCategoryId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companySiteCategoryId)
		{
			return Delete(null, _companySiteCategoryId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companySiteCategoryId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanySiteCategory Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryKey key, int start, int pageLength)
		{
			return GetByCompanySiteCategoryId(transactionManager, key.CompanySiteCategoryId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanySiteCategory index.
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(null,_companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategory index.
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategory index.
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength, out int count)
		{
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategory GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanySiteCategory index.
		/// </summary>
		/// <param name="_categoryName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCategoryName(System.String _categoryName)
		{
			int count = -1;
			return GetByCategoryName(null,_categoryName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategory index.
		/// </summary>
		/// <param name="_categoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCategoryName(System.String _categoryName, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryName(null, _categoryName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCategoryName(TransactionManager transactionManager, System.String _categoryName)
		{
			int count = -1;
			return GetByCategoryName(transactionManager, _categoryName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCategoryName(TransactionManager transactionManager, System.String _categoryName, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryName(transactionManager, _categoryName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategory index.
		/// </summary>
		/// <param name="_categoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategory GetByCategoryName(System.String _categoryName, int start, int pageLength, out int count)
		{
			return GetByCategoryName(null, _categoryName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategory GetByCategoryName(TransactionManager transactionManager, System.String _categoryName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompanySiteCategory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanySiteCategory&gt;"/></returns>
		public static TList<CompanySiteCategory> Fill(IDataReader reader, TList<CompanySiteCategory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanySiteCategory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanySiteCategory")
					.Append("|").Append((System.Int32)reader[((int)CompanySiteCategoryColumn.CompanySiteCategoryId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanySiteCategory>(
					key.ToString(), // EntityTrackingKey
					"CompanySiteCategory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanySiteCategory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanySiteCategoryId = (System.Int32)reader[((int)CompanySiteCategoryColumn.CompanySiteCategoryId - 1)];
					c.CategoryName = (System.String)reader[((int)CompanySiteCategoryColumn.CategoryName - 1)];
					c.CategoryDesc = (System.String)reader[((int)CompanySiteCategoryColumn.CategoryDesc - 1)];
					c.Ordinal = (System.Int32)reader[((int)CompanySiteCategoryColumn.Ordinal - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanySiteCategory entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanySiteCategoryId = (System.Int32)reader[((int)CompanySiteCategoryColumn.CompanySiteCategoryId - 1)];
			entity.CategoryName = (System.String)reader[((int)CompanySiteCategoryColumn.CategoryName - 1)];
			entity.CategoryDesc = (System.String)reader[((int)CompanySiteCategoryColumn.CategoryDesc - 1)];
			entity.Ordinal = (System.Int32)reader[((int)CompanySiteCategoryColumn.Ordinal - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanySiteCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanySiteCategoryId = (System.Int32)dataRow["CompanySiteCategoryId"];
			entity.CategoryName = (System.String)dataRow["CategoryName"];
			entity.CategoryDesc = (System.String)dataRow["CategoryDesc"];
			entity.Ordinal = (System.Int32)dataRow["Ordinal"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCompanySiteCategoryId methods when available
			
			#region CompanySiteCategoryExceptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryException>|CompanySiteCategoryExceptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryExceptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryExceptionCollection = DataRepository.CompanySiteCategoryExceptionProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);

				if (deep && entity.CompanySiteCategoryExceptionCollection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryExceptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryException>) DataRepository.CompanySiteCategoryExceptionProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryExceptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdHocRadarCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdHocRadar>|AdHocRadarCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdHocRadarCollection = DataRepository.AdHocRadarProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);

				if (deep && entity.AdHocRadarCollection.Count > 0)
				{
					deepHandles.Add("AdHocRadarCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdHocRadar>) DataRepository.AdHocRadarProvider.DeepLoad,
						new object[] { transactionManager, entity.AdHocRadarCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryStandardCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollection = DataRepository.CompanySiteCategoryStandardProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);

				if (deep && entity.CompanySiteCategoryStandardCollection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryException2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryException2>|CompanySiteCategoryException2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryException2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryException2Collection = DataRepository.CompanySiteCategoryException2Provider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);

				if (deep && entity.CompanySiteCategoryException2Collection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryException2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryException2>) DataRepository.CompanySiteCategoryException2Provider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryException2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AnnualTargetsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AnnualTargets>|AnnualTargetsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AnnualTargetsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AnnualTargetsCollection = DataRepository.AnnualTargetsProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);

				if (deep && entity.AnnualTargetsCollection.Count > 0)
				{
					deepHandles.Add("AnnualTargetsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AnnualTargets>) DataRepository.AnnualTargetsProvider.DeepLoad,
						new object[] { transactionManager, entity.AnnualTargetsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanySiteCategory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanySiteCategory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CompanySiteCategoryException>
				if (CanDeepSave(entity.CompanySiteCategoryExceptionCollection, "List<CompanySiteCategoryException>|CompanySiteCategoryExceptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryException child in entity.CompanySiteCategoryExceptionCollection)
					{
						if(child.CompanySiteCategoryIdSource != null)
						{
							child.CompanySiteCategoryId = child.CompanySiteCategoryIdSource.CompanySiteCategoryId;
						}
						else
						{
							child.CompanySiteCategoryId = entity.CompanySiteCategoryId;
						}

					}

					if (entity.CompanySiteCategoryExceptionCollection.Count > 0 || entity.CompanySiteCategoryExceptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryExceptionProvider.Save(transactionManager, entity.CompanySiteCategoryExceptionCollection);
						
						deepHandles.Add("CompanySiteCategoryExceptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryException >) DataRepository.CompanySiteCategoryExceptionProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryExceptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdHocRadar>
				if (CanDeepSave(entity.AdHocRadarCollection, "List<AdHocRadar>|AdHocRadarCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdHocRadar child in entity.AdHocRadarCollection)
					{
						if(child.CompanySiteCategoryIdSource != null)
						{
							child.CompanySiteCategoryId = child.CompanySiteCategoryIdSource.CompanySiteCategoryId;
						}
						else
						{
							child.CompanySiteCategoryId = entity.CompanySiteCategoryId;
						}

					}

					if (entity.AdHocRadarCollection.Count > 0 || entity.AdHocRadarCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdHocRadarProvider.Save(transactionManager, entity.AdHocRadarCollection);
						
						deepHandles.Add("AdHocRadarCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdHocRadar >) DataRepository.AdHocRadarProvider.DeepSave,
							new object[] { transactionManager, entity.AdHocRadarCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollection, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollection)
					{
						if(child.CompanySiteCategoryIdSource != null)
						{
							child.CompanySiteCategoryId = child.CompanySiteCategoryIdSource.CompanySiteCategoryId;
						}
						else
						{
							child.CompanySiteCategoryId = entity.CompanySiteCategoryId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollection.Count > 0 || entity.CompanySiteCategoryStandardCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollection);
						
						deepHandles.Add("CompanySiteCategoryStandardCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryException2>
				if (CanDeepSave(entity.CompanySiteCategoryException2Collection, "List<CompanySiteCategoryException2>|CompanySiteCategoryException2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryException2 child in entity.CompanySiteCategoryException2Collection)
					{
						if(child.CompanySiteCategoryIdSource != null)
						{
							child.CompanySiteCategoryId = child.CompanySiteCategoryIdSource.CompanySiteCategoryId;
						}
						else
						{
							child.CompanySiteCategoryId = entity.CompanySiteCategoryId;
						}

					}

					if (entity.CompanySiteCategoryException2Collection.Count > 0 || entity.CompanySiteCategoryException2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryException2Provider.Save(transactionManager, entity.CompanySiteCategoryException2Collection);
						
						deepHandles.Add("CompanySiteCategoryException2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryException2 >) DataRepository.CompanySiteCategoryException2Provider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryException2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AnnualTargets>
				if (CanDeepSave(entity.AnnualTargetsCollection, "List<AnnualTargets>|AnnualTargetsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AnnualTargets child in entity.AnnualTargetsCollection)
					{
						if(child.CompanySiteCategoryIdSource != null)
						{
							child.CompanySiteCategoryId = child.CompanySiteCategoryIdSource.CompanySiteCategoryId;
						}
						else
						{
							child.CompanySiteCategoryId = entity.CompanySiteCategoryId;
						}

					}

					if (entity.AnnualTargetsCollection.Count > 0 || entity.AnnualTargetsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AnnualTargetsProvider.Save(transactionManager, entity.AnnualTargetsCollection);
						
						deepHandles.Add("AnnualTargetsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AnnualTargets >) DataRepository.AnnualTargetsProvider.DeepSave,
							new object[] { transactionManager, entity.AnnualTargetsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanySiteCategoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanySiteCategory</c>
	///</summary>
	public enum CompanySiteCategoryChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CompanySiteCategory</c> as OneToMany for CompanySiteCategoryExceptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryException>))]
		CompanySiteCategoryExceptionCollection,

		///<summary>
		/// Collection of <c>CompanySiteCategory</c> as OneToMany for AdHocRadarCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdHocRadar>))]
		AdHocRadarCollection,

		///<summary>
		/// Collection of <c>CompanySiteCategory</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollection,

		///<summary>
		/// Collection of <c>CompanySiteCategory</c> as OneToMany for CompanySiteCategoryException2Collection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryException2>))]
		CompanySiteCategoryException2Collection,

		///<summary>
		/// Collection of <c>CompanySiteCategory</c> as OneToMany for AnnualTargetsCollection
		///</summary>
		[ChildEntityType(typeof(TList<AnnualTargets>))]
		AnnualTargetsCollection,
	}
	
	#endregion CompanySiteCategoryChildEntityTypes
	
	#region CompanySiteCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanySiteCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryFilterBuilder : SqlFilterBuilder<CompanySiteCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryFilterBuilder class.
		/// </summary>
		public CompanySiteCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryFilterBuilder
	
	#region CompanySiteCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanySiteCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryParameterBuilder
	
	#region CompanySiteCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanySiteCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategorySortBuilder : SqlSortBuilder<CompanySiteCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategorySqlSortBuilder class.
		/// </summary>
		public CompanySiteCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategorySortBuilder
	
} // end namespace
