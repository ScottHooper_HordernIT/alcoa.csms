﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'AdminTaskEmailTemplateAttachment' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class AdminTaskEmailTemplateAttachment : AdminTaskEmailTemplateAttachmentBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="AdminTaskEmailTemplateAttachment"/> instance.
		///</summary>
		public AdminTaskEmailTemplateAttachment():base(){}	
		
		#endregion
	}
}
