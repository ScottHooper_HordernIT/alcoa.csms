﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'SqExemption' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SqExemption : SqExemptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SqExemption"/> instance.
		///</summary>
		public SqExemption():base(){}	
		
		#endregion
	}
}
