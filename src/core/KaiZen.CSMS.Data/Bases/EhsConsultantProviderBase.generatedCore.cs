﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EhsConsultantProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EhsConsultantProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EhsConsultant, KaiZen.CSMS.Entities.EhsConsultantKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsConsultantKey key)
		{
			return Delete(transactionManager, key.EhsConsultantId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ehsConsultantId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ehsConsultantId)
		{
			return Delete(null, _ehsConsultantId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ehsConsultantId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EHSConsultant_Users_Supervisor key.
		///		FK_EHSConsultant_Users_Supervisor Description: 
		/// </summary>
		/// <param name="_supervisorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EhsConsultant objects.</returns>
		public TList<EhsConsultant> GetBySupervisorUserId(System.Int32? _supervisorUserId)
		{
			int count = -1;
			return GetBySupervisorUserId(_supervisorUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EHSConsultant_Users_Supervisor key.
		///		FK_EHSConsultant_Users_Supervisor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EhsConsultant objects.</returns>
		/// <remarks></remarks>
		public TList<EhsConsultant> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId)
		{
			int count = -1;
			return GetBySupervisorUserId(transactionManager, _supervisorUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_EHSConsultant_Users_Supervisor key.
		///		FK_EHSConsultant_Users_Supervisor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EhsConsultant objects.</returns>
		public TList<EhsConsultant> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId, int start, int pageLength)
		{
			int count = -1;
			return GetBySupervisorUserId(transactionManager, _supervisorUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EHSConsultant_Users_Supervisor key.
		///		fkEhsConsultantUsersSupervisor Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supervisorUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EhsConsultant objects.</returns>
		public TList<EhsConsultant> GetBySupervisorUserId(System.Int32? _supervisorUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySupervisorUserId(null, _supervisorUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EHSConsultant_Users_Supervisor key.
		///		fkEhsConsultantUsersSupervisor Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EhsConsultant objects.</returns>
		public TList<EhsConsultant> GetBySupervisorUserId(System.Int32? _supervisorUserId, int start, int pageLength,out int count)
		{
			return GetBySupervisorUserId(null, _supervisorUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_EHSConsultant_Users_Supervisor key.
		///		FK_EHSConsultant_Users_Supervisor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.EhsConsultant objects.</returns>
		public abstract TList<EhsConsultant> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EhsConsultant Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsConsultantKey key, int start, int pageLength)
		{
			return GetByEhsConsultantId(transactionManager, key.EhsConsultantId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EHSConsultant index.
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByEhsConsultantId(System.Int32 _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(null,_ehsConsultantId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EHSConsultant index.
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByEhsConsultantId(System.Int32 _ehsConsultantId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EHSConsultant index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByEhsConsultantId(TransactionManager transactionManager, System.Int32 _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EHSConsultant index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByEhsConsultantId(TransactionManager transactionManager, System.Int32 _ehsConsultantId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EHSConsultant index.
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByEhsConsultantId(System.Int32 _ehsConsultantId, int start, int pageLength, out int count)
		{
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EHSConsultant index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EhsConsultant GetByEhsConsultantId(TransactionManager transactionManager, System.Int32 _ehsConsultantId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_EHSConsultant index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByUserId(System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(null,_userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultant index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByUserId(System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultant index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultant index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultant index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public KaiZen.CSMS.Entities.EhsConsultant GetByUserId(System.Int32 _userId, int start, int pageLength, out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_EHSConsultant index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EhsConsultant GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _EHSConsultant_ListAssignedToSmps 
		
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToSmps' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedToSmps()
		{
			return ListAssignedToSmps(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToSmps' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedToSmps(int start, int pageLength)
		{
			return ListAssignedToSmps(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToSmps' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedToSmps(TransactionManager transactionManager)
		{
			return ListAssignedToSmps(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToSmps' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedToSmps(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _EHSConsultant_ListAssignedToCompanies 
		
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedToCompanies()
		{
			return ListAssignedToCompanies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedToCompanies(int start, int pageLength)
		{
			return ListAssignedToCompanies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToCompanies' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedToCompanies(TransactionManager transactionManager)
		{
			return ListAssignedToCompanies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_EHSConsultant_ListAssignedToCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedToCompanies(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EhsConsultant&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EhsConsultant&gt;"/></returns>
		public static TList<EhsConsultant> Fill(IDataReader reader, TList<EhsConsultant> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EhsConsultant c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EhsConsultant")
					.Append("|").Append((System.Int32)reader[((int)EhsConsultantColumn.EhsConsultantId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EhsConsultant>(
					key.ToString(), // EntityTrackingKey
					"EhsConsultant",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EhsConsultant();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EhsConsultantId = (System.Int32)reader[((int)EhsConsultantColumn.EhsConsultantId - 1)];
					c.UserId = (System.Int32)reader[((int)EhsConsultantColumn.UserId - 1)];
					c.Enabled = (System.Boolean)reader[((int)EhsConsultantColumn.Enabled - 1)];
					c.SafeNameDefault = (reader.IsDBNull(((int)EhsConsultantColumn.SafeNameDefault - 1)))?null:(System.Boolean?)reader[((int)EhsConsultantColumn.SafeNameDefault - 1)];
					c.SiteId = (reader.IsDBNull(((int)EhsConsultantColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.SiteId - 1)];
					c.RegionId = (reader.IsDBNull(((int)EhsConsultantColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.RegionId - 1)];
					c.SupervisorUserId = (reader.IsDBNull(((int)EhsConsultantColumn.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.SupervisorUserId - 1)];
					c.Level3UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level3UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level3UserId - 1)];
					c.Level4UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level4UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level4UserId - 1)];
					c.Level5UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level5UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level5UserId - 1)];
					c.Level6UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level6UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level6UserId - 1)];
					c.Level7UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level7UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level7UserId - 1)];
					c.Level8UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level8UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level8UserId - 1)];
					c.Level9UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level9UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level9UserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EhsConsultant entity)
		{
			if (!reader.Read()) return;
			
			entity.EhsConsultantId = (System.Int32)reader[((int)EhsConsultantColumn.EhsConsultantId - 1)];
			entity.UserId = (System.Int32)reader[((int)EhsConsultantColumn.UserId - 1)];
			entity.Enabled = (System.Boolean)reader[((int)EhsConsultantColumn.Enabled - 1)];
			entity.SafeNameDefault = (reader.IsDBNull(((int)EhsConsultantColumn.SafeNameDefault - 1)))?null:(System.Boolean?)reader[((int)EhsConsultantColumn.SafeNameDefault - 1)];
			entity.SiteId = (reader.IsDBNull(((int)EhsConsultantColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.SiteId - 1)];
			entity.RegionId = (reader.IsDBNull(((int)EhsConsultantColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.RegionId - 1)];
			entity.SupervisorUserId = (reader.IsDBNull(((int)EhsConsultantColumn.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.SupervisorUserId - 1)];
			entity.Level3UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level3UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level3UserId - 1)];
			entity.Level4UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level4UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level4UserId - 1)];
			entity.Level5UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level5UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level5UserId - 1)];
			entity.Level6UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level6UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level6UserId - 1)];
			entity.Level7UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level7UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level7UserId - 1)];
			entity.Level8UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level8UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level8UserId - 1)];
			entity.Level9UserId = (reader.IsDBNull(((int)EhsConsultantColumn.Level9UserId - 1)))?null:(System.Int32?)reader[((int)EhsConsultantColumn.Level9UserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EhsConsultant entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (System.Int32)dataRow["EHSConsultantId"];
			entity.UserId = (System.Int32)dataRow["UserId"];
			entity.Enabled = (System.Boolean)dataRow["Enabled"];
			entity.SafeNameDefault = Convert.IsDBNull(dataRow["Default"]) ? null : (System.Boolean?)dataRow["Default"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.RegionId = Convert.IsDBNull(dataRow["RegionId"]) ? null : (System.Int32?)dataRow["RegionId"];
			entity.SupervisorUserId = Convert.IsDBNull(dataRow["SupervisorUserId"]) ? null : (System.Int32?)dataRow["SupervisorUserId"];
			entity.Level3UserId = Convert.IsDBNull(dataRow["Level3UserId"]) ? null : (System.Int32?)dataRow["Level3UserId"];
			entity.Level4UserId = Convert.IsDBNull(dataRow["Level4UserId"]) ? null : (System.Int32?)dataRow["Level4UserId"];
			entity.Level5UserId = Convert.IsDBNull(dataRow["Level5UserId"]) ? null : (System.Int32?)dataRow["Level5UserId"];
			entity.Level6UserId = Convert.IsDBNull(dataRow["Level6UserId"]) ? null : (System.Int32?)dataRow["Level6UserId"];
			entity.Level7UserId = Convert.IsDBNull(dataRow["Level7UserId"]) ? null : (System.Int32?)dataRow["Level7UserId"];
			entity.Level8UserId = Convert.IsDBNull(dataRow["Level8UserId"]) ? null : (System.Int32?)dataRow["Level8UserId"];
			entity.Level9UserId = Convert.IsDBNull(dataRow["Level9UserId"]) ? null : (System.Int32?)dataRow["Level9UserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EhsConsultant"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EhsConsultant Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsConsultant entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SupervisorUserIdSource	
			if (CanDeepLoad(entity, "Users|SupervisorUserIdSource", deepLoadType, innerList) 
				&& entity.SupervisorUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupervisorUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupervisorUserIdSource = tmpEntity;
				else
					entity.SupervisorUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.SupervisorUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupervisorUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupervisorUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.SupervisorUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupervisorUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByEhsConsultantId methods when available
			
			#region SitesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Sites>|SitesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SitesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SitesCollection = DataRepository.SitesProvider.GetByEhsConsultantId(transactionManager, entity.EhsConsultantId);

				if (deep && entity.SitesCollection.Count > 0)
				{
					deepHandles.Add("SitesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Sites>) DataRepository.SitesProvider.DeepLoad,
						new object[] { transactionManager, entity.SitesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ConfigSa812Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ConfigSa812>|ConfigSa812Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigSa812Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ConfigSa812Collection = DataRepository.ConfigSa812Provider.GetByEhsConsultantId(transactionManager, entity.EhsConsultantId);

				if (deep && entity.ConfigSa812Collection.Count > 0)
				{
					deepHandles.Add("ConfigSa812Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ConfigSa812>) DataRepository.ConfigSa812Provider.DeepLoad,
						new object[] { transactionManager, entity.ConfigSa812Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EhsConsultant object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EhsConsultant instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EhsConsultant Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EhsConsultant entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SupervisorUserIdSource
			if (CanDeepSave(entity, "Users|SupervisorUserIdSource", deepSaveType, innerList) 
				&& entity.SupervisorUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.SupervisorUserIdSource);
				entity.SupervisorUserId = entity.SupervisorUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Sites>
				if (CanDeepSave(entity.SitesCollection, "List<Sites>|SitesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Sites child in entity.SitesCollection)
					{
						if(child.EhsConsultantIdSource != null)
						{
							child.EhsConsultantId = child.EhsConsultantIdSource.EhsConsultantId;
						}
						else
						{
							child.EhsConsultantId = entity.EhsConsultantId;
						}

					}

					if (entity.SitesCollection.Count > 0 || entity.SitesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SitesProvider.Save(transactionManager, entity.SitesCollection);
						
						deepHandles.Add("SitesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Sites >) DataRepository.SitesProvider.DeepSave,
							new object[] { transactionManager, entity.SitesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ConfigSa812>
				if (CanDeepSave(entity.ConfigSa812Collection, "List<ConfigSa812>|ConfigSa812Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ConfigSa812 child in entity.ConfigSa812Collection)
					{
						if(child.EhsConsultantIdSource != null)
						{
							child.EhsConsultantId = child.EhsConsultantIdSource.EhsConsultantId;
						}
						else
						{
							child.EhsConsultantId = entity.EhsConsultantId;
						}

					}

					if (entity.ConfigSa812Collection.Count > 0 || entity.ConfigSa812Collection.DeletedItems.Count > 0)
					{
						//DataRepository.ConfigSa812Provider.Save(transactionManager, entity.ConfigSa812Collection);
						
						deepHandles.Add("ConfigSa812Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ConfigSa812 >) DataRepository.ConfigSa812Provider.DeepSave,
							new object[] { transactionManager, entity.ConfigSa812Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EhsConsultantChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EhsConsultant</c>
	///</summary>
	public enum EhsConsultantChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at SupervisorUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>EhsConsultant</c> as OneToMany for SitesCollection
		///</summary>
		[ChildEntityType(typeof(TList<Sites>))]
		SitesCollection,

		///<summary>
		/// Collection of <c>EhsConsultant</c> as OneToMany for ConfigSa812Collection
		///</summary>
		[ChildEntityType(typeof(TList<ConfigSa812>))]
		ConfigSa812Collection,
	}
	
	#endregion EhsConsultantChildEntityTypes
	
	#region EhsConsultantFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EhsConsultantColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantFilterBuilder : SqlFilterBuilder<EhsConsultantColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantFilterBuilder class.
		/// </summary>
		public EhsConsultantFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsConsultantFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsConsultantFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsConsultantFilterBuilder
	
	#region EhsConsultantParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EhsConsultantColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EhsConsultantParameterBuilder : ParameterizedSqlFilterBuilder<EhsConsultantColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantParameterBuilder class.
		/// </summary>
		public EhsConsultantParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EhsConsultantParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EhsConsultantParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EhsConsultantParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EhsConsultantParameterBuilder
	
	#region EhsConsultantSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EhsConsultantColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EhsConsultant"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EhsConsultantSortBuilder : SqlSortBuilder<EhsConsultantColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EhsConsultantSqlSortBuilder class.
		/// </summary>
		public EhsConsultantSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EhsConsultantSortBuilder
	
} // end namespace
