﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TemplateTypeProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TemplateTypeDataSourceDesigner))]
	public class TemplateTypeDataSource : ProviderDataSource<TemplateType, TemplateTypeKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeDataSource class.
		/// </summary>
		public TemplateTypeDataSource() : base(new TemplateTypeService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TemplateTypeDataSourceView used by the TemplateTypeDataSource.
		/// </summary>
		protected TemplateTypeDataSourceView TemplateTypeView
		{
			get { return ( View as TemplateTypeDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TemplateTypeDataSource control invokes to retrieve data.
		/// </summary>
		public TemplateTypeSelectMethod SelectMethod
		{
			get
			{
				TemplateTypeSelectMethod selectMethod = TemplateTypeSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TemplateTypeSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TemplateTypeDataSourceView class that is to be
		/// used by the TemplateTypeDataSource.
		/// </summary>
		/// <returns>An instance of the TemplateTypeDataSourceView class.</returns>
		protected override BaseDataSourceView<TemplateType, TemplateTypeKey> GetNewDataSourceView()
		{
			return new TemplateTypeDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TemplateTypeDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TemplateTypeDataSourceView : ProviderDataSourceView<TemplateType, TemplateTypeKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TemplateTypeDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TemplateTypeDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TemplateTypeDataSourceView(TemplateTypeDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TemplateTypeDataSource TemplateTypeOwner
		{
			get { return Owner as TemplateTypeDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TemplateTypeSelectMethod SelectMethod
		{
			get { return TemplateTypeOwner.SelectMethod; }
			set { TemplateTypeOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TemplateTypeService TemplateTypeProvider
		{
			get { return Provider as TemplateTypeService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TemplateType> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TemplateType> results = null;
			TemplateType item;
			count = 0;
			
			System.Int32 _templateTypeId;
			System.String _templateTypeName;

			switch ( SelectMethod )
			{
				case TemplateTypeSelectMethod.Get:
					TemplateTypeKey entityKey  = new TemplateTypeKey();
					entityKey.Load(values);
					item = TemplateTypeProvider.Get(entityKey);
					results = new TList<TemplateType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TemplateTypeSelectMethod.GetAll:
                    results = TemplateTypeProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TemplateTypeSelectMethod.GetPaged:
					results = TemplateTypeProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TemplateTypeSelectMethod.Find:
					if ( FilterParameters != null )
						results = TemplateTypeProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TemplateTypeProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TemplateTypeSelectMethod.GetByTemplateTypeId:
					_templateTypeId = ( values["TemplateTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["TemplateTypeId"], typeof(System.Int32)) : (int)0;
					item = TemplateTypeProvider.GetByTemplateTypeId(_templateTypeId);
					results = new TList<TemplateType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case TemplateTypeSelectMethod.GetByTemplateTypeName:
					_templateTypeName = ( values["TemplateTypeName"] != null ) ? (System.String) EntityUtil.ChangeType(values["TemplateTypeName"], typeof(System.String)) : string.Empty;
					item = TemplateTypeProvider.GetByTemplateTypeName(_templateTypeName);
					results = new TList<TemplateType>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TemplateTypeSelectMethod.Get || SelectMethod == TemplateTypeSelectMethod.GetByTemplateTypeId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TemplateType entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TemplateTypeProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TemplateType> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TemplateTypeProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TemplateTypeDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TemplateTypeDataSource class.
	/// </summary>
	public class TemplateTypeDataSourceDesigner : ProviderDataSourceDesigner<TemplateType, TemplateTypeKey>
	{
		/// <summary>
		/// Initializes a new instance of the TemplateTypeDataSourceDesigner class.
		/// </summary>
		public TemplateTypeDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TemplateTypeSelectMethod SelectMethod
		{
			get { return ((TemplateTypeDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TemplateTypeDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TemplateTypeDataSourceActionList

	/// <summary>
	/// Supports the TemplateTypeDataSourceDesigner class.
	/// </summary>
	internal class TemplateTypeDataSourceActionList : DesignerActionList
	{
		private TemplateTypeDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TemplateTypeDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TemplateTypeDataSourceActionList(TemplateTypeDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TemplateTypeSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TemplateTypeDataSourceActionList
	
	#endregion TemplateTypeDataSourceDesigner
	
	#region TemplateTypeSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TemplateTypeDataSource.SelectMethod property.
	/// </summary>
	public enum TemplateTypeSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByTemplateTypeId method.
		/// </summary>
		GetByTemplateTypeId,
		/// <summary>
		/// Represents the GetByTemplateTypeName method.
		/// </summary>
		GetByTemplateTypeName
	}
	
	#endregion TemplateTypeSelectMethod

	#region TemplateTypeFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeFilter : SqlFilter<TemplateTypeColumn>
	{
	}
	
	#endregion TemplateTypeFilter

	#region TemplateTypeExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeExpressionBuilder : SqlExpressionBuilder<TemplateTypeColumn>
	{
	}
	
	#endregion TemplateTypeExpressionBuilder	

	#region TemplateTypeProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TemplateTypeChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TemplateType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TemplateTypeProperty : ChildEntityProperty<TemplateTypeChildEntityTypes>
	{
	}
	
	#endregion TemplateTypeProperty
}

