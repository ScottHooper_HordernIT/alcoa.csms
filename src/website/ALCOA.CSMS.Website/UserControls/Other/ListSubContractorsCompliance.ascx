﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_ListSubContractorsCompliance" Codebehind="ListSubContractorsCompliance.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.State" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGauges.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" TagPrefix="dx" %>


<table align="center" border="0" cellpadding="0" cellspacing="0" width="900">
    <tbody>
        <tr>
            <td style="width: 1px">
                <img height="1" src="Images/spacer.gif" width="0" />
            </td>
            <td style="width: 200px; margin-left: 40px; vertical-align: top">
                <br />
                <a href="default.aspx"> << return to Health Check</a> 
            </td>
            <td bgcolor="#336699" style="height: 400px" width="1">
            </td>
            <td style="width: 694px; vertical-align: top;">
                    <table>
                        <tr>
                            <td>
                                <table align="left" style="border-right: #003399 1px solid; padding-right: 3px; border-top: #003399 1px solid;
    padding-left: 3px; padding-bottom: 3px; margin-left: 0px; border-left: #003399 1px solid;
    padding-top: 3px; border-bottom: #003399 1px solid; background-color: #e9efff;
    height: 50px;">
    <tr>
        <td>
            <dx:ASPxPanel ID="PanelAll" runat="server" Width="680px">
                <PanelCollection>
                    <dx:PanelContent>
                        
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxLabel 
                                            ID="lblCompanyName" runat="server" Font-Bold="true" Font-Size="15px" Font-Names="Arial" ForeColor="#003399">
                                        </dx:ASPxLabel><strong><span style="font-size: 12px; color: #003399; font-family: Arial">
                                        &nbsp;&gt; Available Sub Contractor List</span></strong>&nbsp;
                                    </td>
                                    <td>
                                        <dx:ASPxComboBox ID="ddlCompanies" runat="server" ValueType="System.Int32" Width="318px"
                                            AutoPostBack="True" IncrementalFilteringMode="StartsWith" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged1"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                            
                            <div style="position: relative">
                            <div style="position: relative">
                                <dx:ASPxPanel ID="pnlHealthCheck" runat="server" Width="100%" Height="100%" Visible="false">
                                    <PanelCollection>
                                        <dx:PanelContent ID="PanelContent1" runat="server">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="text-align: right">
                                                        <strong>Company Type:</strong>
                                                    </td>
                                                    <td>
                                                        &nbsp;<dx:ASPxLabel ID="lblCompanyType" runat="server" Text="-">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td style="text-align: right; padding-left: 10px;">
                                                        <strong>Supervision Requirement:</strong>
                                                    </td>
                                                    <td>
                                                        &nbsp;<dx:ASPxLabel ID="lblSupervisionRequirement" runat="server" Text="-" ForeColor="Red">
                                                        </dx:ASPxLabel>
                                                    </td>
                                                    <td style="text-align: right; padding-left: 20px;">
                                                        <dx:ASPxHyperLink ID="hlDefinitions" runat="server" Text="View Definitions" NavigateUrl="javascript:popUp('PopUps/Library_SupervisionRequirements.aspx');" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue">
                                                        </dx:ASPxHyperLink>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                                                <tr>
                                                    <td style="width: 430px; text-align: left">
                                                        <table border="0" cellpadding="0" cellspacing="0" class="dxgvControl_Office2003_Blue"
                                                            style="width: 100%; border-collapse: collapse; empty-cells: show;">
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_SQ" runat="server" Text="" NavigateUrl="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_SQ" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_SQ" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48px; height: 13px; text-align: right" align="right">
                                                                    <dx:ASPxHyperLink ID="hlTL_SC" runat="server" Text="" />
                                                                </td>
                                                                <td style="width: 30px; text-align: center">
                                                                    <dx:ASPxGaugeControl ID="gcTL_SC" runat="server" BackColor="White" Height="25px"
                                                                        ImageType="Png" Value="3" Width="25px">
                                                                        <Gauges>
                                                                            <dx:StateIndicatorGauge Bounds="0, 0, 20, 20" Name="Gauge0">
                                                                                <indicators>
                                                                                <dx:StateIndicatorComponent Center="124, 124" Name="stateIndicatorComponent1" 
                                                                                    StateIndex="0">
                                                                                    <states>
                                                                                        <dx:IndicatorStateWeb Name="State1" ShapeType="ElectricLight1" />
                                                                                        <dx:IndicatorStateWeb Name="State2" ShapeType="ElectricLight2" />
                                                                                        <dx:IndicatorStateWeb Name="State3" ShapeType="ElectricLight3" />
                                                                                        <dx:IndicatorStateWeb Name="State4" ShapeType="ElectricLight4" />
                                                                                    </states>
                                                                                </dx:StateIndicatorComponent>
                                                                            </indicators>
                                                                            </dx:StateIndicatorGauge>
                                                                        </Gauges>
                                                                    </dx:ASPxGaugeControl>
                                                                </td>
                                                                <td style="text-align: left">
                                                                    <dx:ASPxLabel ID="lblTL_SC" runat="server" Text="">
                                                                    </dx:ASPxLabel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5px">
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="text-align: right; padding-top: 2px;">
                                            </div>
                                            <br />
                                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="False" OnHtmlRowCreated="grid_HtmlRowCreated"
                                                EnableCallBacks="false">
                                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                    </LoadingPanelOnStatusBar>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                                    </LoadingPanel>
                                                </Images>
                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="Site" FieldName="SiteName" VisibleIndex="0">
                                                            </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Residential Category" FieldName="CompanySiteCategoryDesc"
                                                        VisibleIndex="1" Width="140px">
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Site Access Approval" FieldName="SiteAccessApproved"
                                                        VisibleIndex="2">
                                                        <CellStyle HorizontalAlign="Center" />
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                                <SettingsPager Mode="ShowAllRecords">
                                                </SettingsPager>
                                                <ImagesFilterControl>
                                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                    </LoadingPanel>
                                                </ImagesFilterControl>
                                                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                                    <Header ImageSpacing="5px" SortingImageSpacing="5px" HorizontalAlign="Center" VerticalAlign="Middle"
                                                        Wrap="True">
                                                    </Header>
                                                    <LoadingPanel ImageSpacing="10px">
                                                    </LoadingPanel>
                                                </Styles>
                                                <SettingsText EmptyDataRow="No Sites currently Allocated for this Company." />
                                                <StylesEditors>
                                                    <ProgressBar Height="25px">
                                                    </ProgressBar>
                                                </StylesEditors>
                                            </dx:ASPxGridView>
                                        </dx:PanelContent>
                                    </PanelCollection>
                                </dx:ASPxPanel>
                            </div>
                        </div>
                    </dx:PanelContent>
                </PanelCollection>
            </dx:ASPxPanel>
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqldsCompaniesList_SubContractors" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameListInSqDatabase" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </table>
            </td>
            <td style="width: 1px">
                <img height="1" src="Images/spacer.gif" width="0" />
            </td>
        </tr>
    </tbody>
</table>


