﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" CodeBehind="EditHelpFile.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.EditHelpFile" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function closePopup() { window.close(); }

    window.onunload = function () {
        if (window.opener && !window.opener.closed) {
            window.opener.popUpClosed();
        }
    };
    </script>
<dx:ASPxLabel ID="lblFileUploadMsg" runat="server" ForeColor="Red">
    </dx:ASPxLabel>
    <br />
    <br />
<dx:ASPxUploadControl ID="ucNewFile" runat="server" ShowProgressPanel="True" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="26252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 25Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        <dx:ASPxLabel ID="lblCurrentFile" runat="server" ForeColor="Red" Text="The following file is currently saved. Uploading another file will overwrite this: " Visible="false">
        </dx:ASPxLabel>
        <dx:ASPxHyperLink ID="hlCurrentFile" runat="server" Text="-" Visible="false">
        </dx:ASPxHyperLink>
        <br />

    <br />
    <br />
    Please select all areas (at least one) that this document is applicable to:<br />
    <br />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                File Name (uses default file name if blank)</td>
            <td>
                Category</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Everyone" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox>
            </td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Everyone" runat="server" Text="Everyone" 
                    CheckState="Unchecked">
        
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Administrator" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox>
            </td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Administrator" runat="server" Text="Administrator">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Contractor_Prequal" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Contractor_Prequal" runat="server" Text="Contractor (Pre-Qual)">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Contractor_Requal" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Contractor_Requal" runat="server" Text="Contractor">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Contractor_ViewEngineeringHours" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Contractor_ViewEngineeringHours" runat="server" Text="Contractor - View Engineering Hours">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_HsAssessor" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_HsAssessor" runat="server" Text="H&S Assessor">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_HsAssessor_Lead" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_HsAssessor_Lead" runat="server" Text="H&S Assessor - Lead">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Procurement" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Procurement" runat="server" Text="Procurement">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Reader" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Reader" runat="server" Text="Reader">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Reader_Ebi" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Reader_Ebi" runat="server" Text="Reader - EBI Data Access">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Reader_SafetyQualificationReadAccess" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Reader_SafetyQualificationReadAccess" runat="server" Text="Reader - Safety Qualification Read Access">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Reader_FinancialReportingAccess" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Reader_FinancialReportingAccess" runat="server" Text="Reader - Financial Reporting Access">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxTextBox ID="tbHelpFiles_Reader_TrainingPackageTrainers" runat="server"
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Enabled="True" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="350px">
                </dx:ASPxTextBox></td>
            <td>
    <dx:ASPxCheckBox ID="cbHelpFiles_Reader_TrainingPackageTrainers" runat="server" Text="Reader - Training Package Trainers">
    </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
<dx:ASPxButton ID="btnUpload" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" onclick="btnUpload_Click" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        Text="Add / Upload">
</dx:ASPxButton>
<dx:ASPxButton ID="btnDelete" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" onclick="btnDelete_Click" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        Text="Delete" Visible="false">
</dx:ASPxButton>
    </asp:Content>
