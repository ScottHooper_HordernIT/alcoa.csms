﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanyStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanyStatusProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanyStatus, KaiZen.CSMS.Entities.CompanyStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatusKey key)
		{
			return Delete(transactionManager, key.CompanyStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companyStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companyStatusId)
		{
			return Delete(null, _companyStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companyStatusId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanyStatus Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatusKey key, int start, int pageLength)
		{
			return GetByCompanyStatusId(transactionManager, key.CompanyStatusId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanyStatus index.
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusId(System.Int32 _companyStatusId)
		{
			int count = -1;
			return GetByCompanyStatusId(null,_companyStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus index.
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusId(System.Int32 _companyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusId(null, _companyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId)
		{
			int count = -1;
			return GetByCompanyStatusId(transactionManager, _companyStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusId(transactionManager, _companyStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus index.
		/// </summary>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusId(System.Int32 _companyStatusId, int start, int pageLength, out int count)
		{
			return GetByCompanyStatusId(null, _companyStatusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanyStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusId(TransactionManager transactionManager, System.Int32 _companyStatusId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanyStatus index.
		/// </summary>
		/// <param name="_companyStatusName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusName(System.String _companyStatusName)
		{
			int count = -1;
			return GetByCompanyStatusName(null,_companyStatusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus index.
		/// </summary>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusName(System.String _companyStatusName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusName(null, _companyStatusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusName(TransactionManager transactionManager, System.String _companyStatusName)
		{
			int count = -1;
			return GetByCompanyStatusName(transactionManager, _companyStatusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusName(TransactionManager transactionManager, System.String _companyStatusName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyStatusName(transactionManager, _companyStatusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus index.
		/// </summary>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusName(System.String _companyStatusName, int start, int pageLength, out int count)
		{
			return GetByCompanyStatusName(null, _companyStatusName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanyStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyStatusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanyStatus GetByCompanyStatusName(TransactionManager transactionManager, System.String _companyStatusName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompanyStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanyStatus&gt;"/></returns>
		public static TList<CompanyStatus> Fill(IDataReader reader, TList<CompanyStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanyStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanyStatus")
					.Append("|").Append((System.Int32)reader[((int)CompanyStatusColumn.CompanyStatusId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanyStatus>(
					key.ToString(), // EntityTrackingKey
					"CompanyStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanyStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanyStatusId = (System.Int32)reader[((int)CompanyStatusColumn.CompanyStatusId - 1)];
					c.CompanyStatusName = (System.String)reader[((int)CompanyStatusColumn.CompanyStatusName - 1)];
					c.CompanyStatusDesc = (reader.IsDBNull(((int)CompanyStatusColumn.CompanyStatusDesc - 1)))?null:(System.String)reader[((int)CompanyStatusColumn.CompanyStatusDesc - 1)];
					c.Ordinal = (reader.IsDBNull(((int)CompanyStatusColumn.Ordinal - 1)))?null:(System.Int32?)reader[((int)CompanyStatusColumn.Ordinal - 1)];
					c.ChangeRequiresApproval = (System.Boolean)reader[((int)CompanyStatusColumn.ChangeRequiresApproval - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanyStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanyStatusId = (System.Int32)reader[((int)CompanyStatusColumn.CompanyStatusId - 1)];
			entity.CompanyStatusName = (System.String)reader[((int)CompanyStatusColumn.CompanyStatusName - 1)];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)CompanyStatusColumn.CompanyStatusDesc - 1)))?null:(System.String)reader[((int)CompanyStatusColumn.CompanyStatusDesc - 1)];
			entity.Ordinal = (reader.IsDBNull(((int)CompanyStatusColumn.Ordinal - 1)))?null:(System.Int32?)reader[((int)CompanyStatusColumn.Ordinal - 1)];
			entity.ChangeRequiresApproval = (System.Boolean)reader[((int)CompanyStatusColumn.ChangeRequiresApproval - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanyStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyStatusId = (System.Int32)dataRow["CompanyStatusId"];
			entity.CompanyStatusName = (System.String)dataRow["CompanyStatusName"];
			entity.CompanyStatusDesc = Convert.IsDBNull(dataRow["CompanyStatusDesc"]) ? null : (System.String)dataRow["CompanyStatusDesc"];
			entity.Ordinal = Convert.IsDBNull(dataRow["Ordinal"]) ? null : (System.Int32?)dataRow["Ordinal"];
			entity.ChangeRequiresApproval = (System.Boolean)dataRow["ChangeRequiresApproval"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanyStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCompanyStatusId methods when available
			
			#region CompaniesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Companies>|CompaniesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesCollection = DataRepository.CompaniesProvider.GetByCompanyStatusId(transactionManager, entity.CompanyStatusId);

				if (deep && entity.CompaniesCollection.Count > 0)
				{
					deepHandles.Add("CompaniesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Companies>) DataRepository.CompaniesProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId = DataRepository.CompanyStatusChangeApprovalProvider.GetByRequestedCompanyStatusId(transactionManager, entity.CompanyStatusId);

				if (deep && entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId.Count > 0)
				{
					deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanyStatusChangeApproval>) DataRepository.CompanyStatusChangeApprovalProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId = DataRepository.CompanyStatusChangeApprovalProvider.GetByCurrentCompanyStatusId(transactionManager, entity.CompanyStatusId);

				if (deep && entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId.Count > 0)
				{
					deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanyStatusChangeApproval>) DataRepository.CompanyStatusChangeApprovalProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanyStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanyStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanyStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanyStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Companies>
				if (CanDeepSave(entity.CompaniesCollection, "List<Companies>|CompaniesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Companies child in entity.CompaniesCollection)
					{
						if(child.CompanyStatusIdSource != null)
						{
							child.CompanyStatusId = child.CompanyStatusIdSource.CompanyStatusId;
						}
						else
						{
							child.CompanyStatusId = entity.CompanyStatusId;
						}

					}

					if (entity.CompaniesCollection.Count > 0 || entity.CompaniesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesProvider.Save(transactionManager, entity.CompaniesCollection);
						
						deepHandles.Add("CompaniesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Companies >) DataRepository.CompaniesProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanyStatusChangeApproval>
				if (CanDeepSave(entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanyStatusChangeApproval child in entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId)
					{
						if(child.RequestedCompanyStatusIdSource != null)
						{
							child.RequestedCompanyStatusId = child.RequestedCompanyStatusIdSource.CompanyStatusId;
						}
						else
						{
							child.RequestedCompanyStatusId = entity.CompanyStatusId;
						}

					}

					if (entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId.Count > 0 || entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanyStatusChangeApprovalProvider.Save(transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId);
						
						deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanyStatusChangeApproval >) DataRepository.CompanyStatusChangeApprovalProvider.DeepSave,
							new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanyStatusChangeApproval>
				if (CanDeepSave(entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId, "List<CompanyStatusChangeApproval>|CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanyStatusChangeApproval child in entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId)
					{
						if(child.CurrentCompanyStatusIdSource != null)
						{
							child.CurrentCompanyStatusId = child.CurrentCompanyStatusIdSource.CompanyStatusId;
						}
						else
						{
							child.CurrentCompanyStatusId = entity.CompanyStatusId;
						}

					}

					if (entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId.Count > 0 || entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId.DeletedItems.Count > 0)
					{
						//DataRepository.CompanyStatusChangeApprovalProvider.Save(transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId);
						
						deepHandles.Add("CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanyStatusChangeApproval >) DataRepository.CompanyStatusChangeApprovalProvider.DeepSave,
							new object[] { transactionManager, entity.CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanyStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanyStatus</c>
	///</summary>
	public enum CompanyStatusChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CompanyStatus</c> as OneToMany for CompaniesCollection
		///</summary>
		[ChildEntityType(typeof(TList<Companies>))]
		CompaniesCollection,

		///<summary>
		/// Collection of <c>CompanyStatus</c> as OneToMany for CompanyStatusChangeApprovalCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanyStatusChangeApproval>))]
		CompanyStatusChangeApprovalCollectionGetByRequestedCompanyStatusId,

		///<summary>
		/// Collection of <c>CompanyStatus</c> as OneToMany for CompanyStatusChangeApprovalCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanyStatusChangeApproval>))]
		CompanyStatusChangeApprovalCollectionGetByCurrentCompanyStatusId,
	}
	
	#endregion CompanyStatusChildEntityTypes
	
	#region CompanyStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanyStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusFilterBuilder : SqlFilterBuilder<CompanyStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusFilterBuilder class.
		/// </summary>
		public CompanyStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusFilterBuilder
	
	#region CompanyStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanyStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatusParameterBuilder : ParameterizedSqlFilterBuilder<CompanyStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusParameterBuilder class.
		/// </summary>
		public CompanyStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanyStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanyStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanyStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanyStatusParameterBuilder
	
	#region CompanyStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanyStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanyStatusSortBuilder : SqlSortBuilder<CompanyStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatusSqlSortBuilder class.
		/// </summary>
		public CompanyStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanyStatusSortBuilder
	
} // end namespace
