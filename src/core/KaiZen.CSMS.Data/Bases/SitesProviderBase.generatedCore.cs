﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SitesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SitesProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Sites, KaiZen.CSMS.Entities.SitesKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SitesKey key)
		{
			return Delete(transactionManager, key.SiteId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_siteId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _siteId)
		{
			return Delete(null, _siteId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _siteId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_EHSConsultantId key.
		///		FK_Sites_EHSConsultantId Description: 
		/// </summary>
		/// <param name="_ehsConsultantId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByEhsConsultantId(System.Int32? _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(_ehsConsultantId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_EHSConsultantId key.
		///		FK_Sites_EHSConsultantId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		/// <remarks></remarks>
		public TList<Sites> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_EHSConsultantId key.
		///		FK_Sites_EHSConsultantId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId, int start, int pageLength)
		{
			int count = -1;
			return GetByEhsConsultantId(transactionManager, _ehsConsultantId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_EHSConsultantId key.
		///		fkSitesEhsConsultantId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ehsConsultantId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByEhsConsultantId(System.Int32? _ehsConsultantId, int start, int pageLength)
		{
			int count =  -1;
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_EHSConsultantId key.
		///		fkSitesEhsConsultantId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByEhsConsultantId(System.Int32? _ehsConsultantId, int start, int pageLength,out int count)
		{
			return GetByEhsConsultantId(null, _ehsConsultantId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_EHSConsultantId key.
		///		FK_Sites_EHSConsultantId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ehsConsultantId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public abstract TList<Sites> GetByEhsConsultantId(TransactionManager transactionManager, System.Int32? _ehsConsultantId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_User_ModifiedBy key.
		///		FK_Sites_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(_modifiedbyUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_User_ModifiedBy key.
		///		FK_Sites_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		/// <remarks></remarks>
		public TList<Sites> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_User_ModifiedBy key.
		///		FK_Sites_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedbyUserId(transactionManager, _modifiedbyUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_User_ModifiedBy key.
		///		fkSitesUserModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_User_ModifiedBy key.
		///		fkSitesUserModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public TList<Sites> GetByModifiedbyUserId(System.Int32 _modifiedbyUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedbyUserId(null, _modifiedbyUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Sites_User_ModifiedBy key.
		///		FK_Sites_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedbyUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Sites objects.</returns>
		public abstract TList<Sites> GetByModifiedbyUserId(TransactionManager transactionManager, System.Int32 _modifiedbyUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Sites Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SitesKey key, int start, int pageLength)
		{
			return GetBySiteId(transactionManager, key.SiteId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Sites index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(null,_siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Sites index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Sites index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Sites index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Sites index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteId(System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Sites index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Sites GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Sites index.
		/// </summary>
		/// <param name="_siteNameEbi"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameEbi(System.String _siteNameEbi)
		{
			int count = -1;
			return GetBySiteNameEbi(null,_siteNameEbi, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites index.
		/// </summary>
		/// <param name="_siteNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameEbi(System.String _siteNameEbi, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteNameEbi(null, _siteNameEbi, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteNameEbi"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameEbi(TransactionManager transactionManager, System.String _siteNameEbi)
		{
			int count = -1;
			return GetBySiteNameEbi(transactionManager, _siteNameEbi, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameEbi(TransactionManager transactionManager, System.String _siteNameEbi, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteNameEbi(transactionManager, _siteNameEbi, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites index.
		/// </summary>
		/// <param name="_siteNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameEbi(System.String _siteNameEbi, int start, int pageLength, out int count)
		{
			return GetBySiteNameEbi(null, _siteNameEbi, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteNameEbi"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public abstract TList<Sites> GetBySiteNameEbi(TransactionManager transactionManager, System.String _siteNameEbi, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Sites_Editable index.
		/// </summary>
		/// <param name="_editable"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByEditable(System.Boolean _editable)
		{
			int count = -1;
			return GetByEditable(null,_editable, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_Editable index.
		/// </summary>
		/// <param name="_editable"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByEditable(System.Boolean _editable, int start, int pageLength)
		{
			int count = -1;
			return GetByEditable(null, _editable, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_Editable index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_editable"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByEditable(TransactionManager transactionManager, System.Boolean _editable)
		{
			int count = -1;
			return GetByEditable(transactionManager, _editable, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_Editable index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_editable"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByEditable(TransactionManager transactionManager, System.Boolean _editable, int start, int pageLength)
		{
			int count = -1;
			return GetByEditable(transactionManager, _editable, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_Editable index.
		/// </summary>
		/// <param name="_editable"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByEditable(System.Boolean _editable, int start, int pageLength, out int count)
		{
			return GetByEditable(null, _editable, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_Editable index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_editable"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public abstract TList<Sites> GetByEditable(TransactionManager transactionManager, System.Boolean _editable, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Sites_LocCode index.
		/// </summary>
		/// <param name="_locCode"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByLocCode(System.String _locCode)
		{
			int count = -1;
			return GetByLocCode(null,_locCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_LocCode index.
		/// </summary>
		/// <param name="_locCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByLocCode(System.String _locCode, int start, int pageLength)
		{
			int count = -1;
			return GetByLocCode(null, _locCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_LocCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_locCode"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByLocCode(TransactionManager transactionManager, System.String _locCode)
		{
			int count = -1;
			return GetByLocCode(transactionManager, _locCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_LocCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_locCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByLocCode(TransactionManager transactionManager, System.String _locCode, int start, int pageLength)
		{
			int count = -1;
			return GetByLocCode(transactionManager, _locCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_LocCode index.
		/// </summary>
		/// <param name="_locCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetByLocCode(System.String _locCode, int start, int pageLength, out int count)
		{
			return GetByLocCode(null, _locCode, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_LocCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_locCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public abstract TList<Sites> GetByLocCode(TransactionManager transactionManager, System.String _locCode, int start, int pageLength, out int count);
				
		
        //added by Sayani For Task #1
        public TList<Sites> GetByLocCode_Iproc(TransactionManager transactionManager, System.String _locCode)
        {
            int count = -1;
            return GetByLocCode_Iproc(transactionManager, _locCode, 0, int.MaxValue, out count);
        }

        public abstract TList<Sites> GetByLocCode_Iproc(TransactionManager transactionManager, System.String _locCode, int start, int pageLength, out int count);

        //End
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Sites_SiteAbbrev index.
		/// </summary>
		/// <param name="_siteAbbrev"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteAbbrev(System.String _siteAbbrev)
		{
			int count = -1;
			return GetBySiteAbbrev(null,_siteAbbrev, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteAbbrev index.
		/// </summary>
		/// <param name="_siteAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteAbbrev(System.String _siteAbbrev, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteAbbrev(null, _siteAbbrev, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteAbbrev index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteAbbrev"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteAbbrev(TransactionManager transactionManager, System.String _siteAbbrev)
		{
			int count = -1;
			return GetBySiteAbbrev(transactionManager, _siteAbbrev, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteAbbrev index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteAbbrev(TransactionManager transactionManager, System.String _siteAbbrev, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteAbbrev(transactionManager, _siteAbbrev, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteAbbrev index.
		/// </summary>
		/// <param name="_siteAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteAbbrev(System.String _siteAbbrev, int start, int pageLength, out int count)
		{
			return GetBySiteAbbrev(null, _siteAbbrev, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteAbbrev index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteAbbrev"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Sites GetBySiteAbbrev(TransactionManager transactionManager, System.String _siteAbbrev, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Sites_SiteName index.
		/// </summary>
		/// <param name="_siteName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteName(System.String _siteName)
		{
			int count = -1;
			return GetBySiteName(null,_siteName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteName index.
		/// </summary>
		/// <param name="_siteName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteName(System.String _siteName, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteName(null, _siteName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteName(TransactionManager transactionManager, System.String _siteName)
		{
			int count = -1;
			return GetBySiteName(transactionManager, _siteName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteName(TransactionManager transactionManager, System.String _siteName, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteName(transactionManager, _siteName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteName index.
		/// </summary>
		/// <param name="_siteName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public KaiZen.CSMS.Entities.Sites GetBySiteName(System.String _siteName, int start, int pageLength, out int count)
		{
			return GetBySiteName(null, _siteName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Sites"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Sites GetBySiteName(TransactionManager transactionManager, System.String _siteName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Sites_SiteNameIhs index.
		/// </summary>
		/// <param name="_siteNameIhs"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameIhs(System.String _siteNameIhs)
		{
			int count = -1;
			return GetBySiteNameIhs(null,_siteNameIhs, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteNameIhs index.
		/// </summary>
		/// <param name="_siteNameIhs"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameIhs(System.String _siteNameIhs, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteNameIhs(null, _siteNameIhs, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteNameIhs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteNameIhs"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameIhs(TransactionManager transactionManager, System.String _siteNameIhs)
		{
			int count = -1;
			return GetBySiteNameIhs(transactionManager, _siteNameIhs, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteNameIhs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteNameIhs"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameIhs(TransactionManager transactionManager, System.String _siteNameIhs, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteNameIhs(transactionManager, _siteNameIhs, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteNameIhs index.
		/// </summary>
		/// <param name="_siteNameIhs"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public TList<Sites> GetBySiteNameIhs(System.String _siteNameIhs, int start, int pageLength, out int count)
		{
			return GetBySiteNameIhs(null, _siteNameIhs, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Sites_SiteNameIhs index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteNameIhs"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Sites&gt;"/> class.</returns>
		public abstract TList<Sites> GetBySiteNameIhs(TransactionManager transactionManager, System.String _siteNameIhs, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Sites_GetSiteNameByIprocCode 
		
		/// <summary>
		///	This method wrap the '_Sites_GetSiteNameByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSiteNameByIprocCode(System.Int32? iprocCode)
		{
			return GetSiteNameByIprocCode(null, 0, int.MaxValue , iprocCode);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetSiteNameByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSiteNameByIprocCode(int start, int pageLength, System.Int32? iprocCode)
		{
			return GetSiteNameByIprocCode(null, start, pageLength , iprocCode);
		}
				
		/// <summary>
		///	This method wrap the '_Sites_GetSiteNameByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSiteNameByIprocCode(TransactionManager transactionManager, System.Int32? iprocCode)
		{
			return GetSiteNameByIprocCode(transactionManager, 0, int.MaxValue , iprocCode);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetSiteNameByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetSiteNameByIprocCode(TransactionManager transactionManager, int start, int pageLength , System.Int32? iprocCode);
		
		#endregion
		
		#region _Sites_GetSiteIdByIprocCode 
		
		/// <summary>
		///	This method wrap the '_Sites_GetSiteIdByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSiteIdByIprocCode(System.Int32? iprocCode)
		{
			return GetSiteIdByIprocCode(null, 0, int.MaxValue , iprocCode);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetSiteIdByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSiteIdByIprocCode(int start, int pageLength, System.Int32? iprocCode)
		{
			return GetSiteIdByIprocCode(null, start, pageLength , iprocCode);
		}
				
		/// <summary>
		///	This method wrap the '_Sites_GetSiteIdByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSiteIdByIprocCode(TransactionManager transactionManager, System.Int32? iprocCode)
		{
			return GetSiteIdByIprocCode(transactionManager, 0, int.MaxValue , iprocCode);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetSiteIdByIprocCode' stored procedure. 
		/// </summary>
		/// <param name="iprocCode"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetSiteIdByIprocCode(TransactionManager transactionManager, int start, int pageLength , System.Int32? iprocCode);
		
		#endregion
		
		#region _Sites_CompanySiteCategoryStandard_ByRegionId 
		
		/// <summary>
		///	This method wrap the '_Sites_CompanySiteCategoryStandard_ByRegionId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompanySiteCategoryStandard_ByRegionId(System.Int32? regionId)
		{
			return CompanySiteCategoryStandard_ByRegionId(null, 0, int.MaxValue , regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_CompanySiteCategoryStandard_ByRegionId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompanySiteCategoryStandard_ByRegionId(int start, int pageLength, System.Int32? regionId)
		{
			return CompanySiteCategoryStandard_ByRegionId(null, start, pageLength , regionId);
		}
				
		/// <summary>
		///	This method wrap the '_Sites_CompanySiteCategoryStandard_ByRegionId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompanySiteCategoryStandard_ByRegionId(TransactionManager transactionManager, System.Int32? regionId)
		{
			return CompanySiteCategoryStandard_ByRegionId(transactionManager, 0, int.MaxValue , regionId);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_CompanySiteCategoryStandard_ByRegionId' stored procedure. 
		/// </summary>
		/// <param name="regionId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompanySiteCategoryStandard_ByRegionId(TransactionManager transactionManager, int start, int pageLength , System.Int32? regionId);
		
		#endregion
		
		#region _Sites_withAssignedCompanyCategoryAssigned 
		
		/// <summary>
		///	This method wrap the '_Sites_withAssignedCompanyCategoryAssigned' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet withAssignedCompanyCategoryAssigned()
		{
			return withAssignedCompanyCategoryAssigned(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Sites_withAssignedCompanyCategoryAssigned' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet withAssignedCompanyCategoryAssigned(int start, int pageLength)
		{
			return withAssignedCompanyCategoryAssigned(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Sites_withAssignedCompanyCategoryAssigned' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet withAssignedCompanyCategoryAssigned(TransactionManager transactionManager)
		{
			return withAssignedCompanyCategoryAssigned(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Sites_withAssignedCompanyCategoryAssigned' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet withAssignedCompanyCategoryAssigned(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Sites_GetByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Sites_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(System.Int32? companyId, System.String residentialStatus)
		{
			return GetByCompanyId(null, 0, int.MaxValue , companyId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(int start, int pageLength, System.Int32? companyId, System.String residentialStatus)
		{
			return GetByCompanyId(null, start, pageLength , companyId, residentialStatus);
		}
				
		/// <summary>
		///	This method wrap the '_Sites_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId(TransactionManager transactionManager, System.Int32? companyId, System.String residentialStatus)
		{
			return GetByCompanyId(transactionManager, 0, int.MaxValue , companyId, residentialStatus);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="residentialStatus"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.String residentialStatus);
		
		#endregion
		
		#region _Sites_GetByYearAdHocRadarId 
		
		/// <summary>
		///	This method wrap the '_Sites_GetByYearAdHocRadarId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="adHocRadarId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByYearAdHocRadarId(System.Int32? year, System.Int32? adHocRadarId)
		{
			return GetByYearAdHocRadarId(null, 0, int.MaxValue , year, adHocRadarId);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetByYearAdHocRadarId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="adHocRadarId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByYearAdHocRadarId(int start, int pageLength, System.Int32? year, System.Int32? adHocRadarId)
		{
			return GetByYearAdHocRadarId(null, start, pageLength , year, adHocRadarId);
		}
				
		/// <summary>
		///	This method wrap the '_Sites_GetByYearAdHocRadarId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="adHocRadarId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByYearAdHocRadarId(TransactionManager transactionManager, System.Int32? year, System.Int32? adHocRadarId)
		{
			return GetByYearAdHocRadarId(transactionManager, 0, int.MaxValue , year, adHocRadarId);
		}
		
		/// <summary>
		///	This method wrap the '_Sites_GetByYearAdHocRadarId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="adHocRadarId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByYearAdHocRadarId(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? adHocRadarId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Sites&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Sites&gt;"/></returns>
		public static TList<Sites> Fill(IDataReader reader, TList<Sites> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Sites c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Sites")
					.Append("|").Append((System.Int32)reader[((int)SitesColumn.SiteId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Sites>(
					key.ToString(), // EntityTrackingKey
					"Sites",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Sites();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SiteId = (System.Int32)reader[((int)SitesColumn.SiteId - 1)];
					c.SiteName = (System.String)reader[((int)SitesColumn.SiteName - 1)];
					c.SiteAbbrev = (reader.IsDBNull(((int)SitesColumn.SiteAbbrev - 1)))?null:(System.String)reader[((int)SitesColumn.SiteAbbrev - 1)];
					c.SiteAddress = (System.String)reader[((int)SitesColumn.SiteAddress - 1)];
					c.SiteMainContactNo = (System.String)reader[((int)SitesColumn.SiteMainContactNo - 1)];
					c.LocCode = (reader.IsDBNull(((int)SitesColumn.LocCode - 1)))?null:(System.String)reader[((int)SitesColumn.LocCode - 1)];
					c.IprocCode = (reader.IsDBNull(((int)SitesColumn.IprocCode - 1)))?null:(System.Int32?)reader[((int)SitesColumn.IprocCode - 1)];
					c.IsVisible = (System.Boolean)reader[((int)SitesColumn.IsVisible - 1)];
					c.Ordinal = (reader.IsDBNull(((int)SitesColumn.Ordinal - 1)))?null:(System.Int32?)reader[((int)SitesColumn.Ordinal - 1)];
					c.ModifiedbyUserId = (System.Int32)reader[((int)SitesColumn.ModifiedbyUserId - 1)];
					c.Editable = (System.Boolean)reader[((int)SitesColumn.Editable - 1)];
					c.EhsConsultantId = (reader.IsDBNull(((int)SitesColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)SitesColumn.EhsConsultantId - 1)];
					c.SiteNameEbi = (reader.IsDBNull(((int)SitesColumn.SiteNameEbi - 1)))?null:(System.String)reader[((int)SitesColumn.SiteNameEbi - 1)];
					c.SiteNameIhs = (reader.IsDBNull(((int)SitesColumn.SiteNameIhs - 1)))?null:(System.String)reader[((int)SitesColumn.SiteNameIhs - 1)];
					c.EbiAllViewName = (reader.IsDBNull(((int)SitesColumn.EbiAllViewName - 1)))?null:(System.String)reader[((int)SitesColumn.EbiAllViewName - 1)];
					c.EbiServerName = (reader.IsDBNull(((int)SitesColumn.EbiServerName - 1)))?null:(System.String)reader[((int)SitesColumn.EbiServerName - 1)];
					c.EbiTodayViewName = (reader.IsDBNull(((int)SitesColumn.EbiTodayViewName - 1)))?null:(System.String)reader[((int)SitesColumn.EbiTodayViewName - 1)];
					c.SiteNameHr = (reader.IsDBNull(((int)SitesColumn.SiteNameHr - 1)))?null:(System.String)reader[((int)SitesColumn.SiteNameHr - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Sites"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Sites"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Sites entity)
		{
			if (!reader.Read()) return;
			
			entity.SiteId = (System.Int32)reader[((int)SitesColumn.SiteId - 1)];
			entity.SiteName = (System.String)reader[((int)SitesColumn.SiteName - 1)];
			entity.SiteAbbrev = (reader.IsDBNull(((int)SitesColumn.SiteAbbrev - 1)))?null:(System.String)reader[((int)SitesColumn.SiteAbbrev - 1)];
			entity.SiteAddress = (System.String)reader[((int)SitesColumn.SiteAddress - 1)];
			entity.SiteMainContactNo = (System.String)reader[((int)SitesColumn.SiteMainContactNo - 1)];
			entity.LocCode = (reader.IsDBNull(((int)SitesColumn.LocCode - 1)))?null:(System.String)reader[((int)SitesColumn.LocCode - 1)];
			entity.IprocCode = (reader.IsDBNull(((int)SitesColumn.IprocCode - 1)))?null:(System.Int32?)reader[((int)SitesColumn.IprocCode - 1)];
			entity.IsVisible = (System.Boolean)reader[((int)SitesColumn.IsVisible - 1)];
			entity.Ordinal = (reader.IsDBNull(((int)SitesColumn.Ordinal - 1)))?null:(System.Int32?)reader[((int)SitesColumn.Ordinal - 1)];
			entity.ModifiedbyUserId = (System.Int32)reader[((int)SitesColumn.ModifiedbyUserId - 1)];
			entity.Editable = (System.Boolean)reader[((int)SitesColumn.Editable - 1)];
			entity.EhsConsultantId = (reader.IsDBNull(((int)SitesColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)SitesColumn.EhsConsultantId - 1)];
			entity.SiteNameEbi = (reader.IsDBNull(((int)SitesColumn.SiteNameEbi - 1)))?null:(System.String)reader[((int)SitesColumn.SiteNameEbi - 1)];
			entity.SiteNameIhs = (reader.IsDBNull(((int)SitesColumn.SiteNameIhs - 1)))?null:(System.String)reader[((int)SitesColumn.SiteNameIhs - 1)];
			entity.EbiAllViewName = (reader.IsDBNull(((int)SitesColumn.EbiAllViewName - 1)))?null:(System.String)reader[((int)SitesColumn.EbiAllViewName - 1)];
			entity.EbiServerName = (reader.IsDBNull(((int)SitesColumn.EbiServerName - 1)))?null:(System.String)reader[((int)SitesColumn.EbiServerName - 1)];
			entity.EbiTodayViewName = (reader.IsDBNull(((int)SitesColumn.EbiTodayViewName - 1)))?null:(System.String)reader[((int)SitesColumn.EbiTodayViewName - 1)];
			entity.SiteNameHr = (reader.IsDBNull(((int)SitesColumn.SiteNameHr - 1)))?null:(System.String)reader[((int)SitesColumn.SiteNameHr - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Sites"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Sites"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Sites entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.SiteName = (System.String)dataRow["SiteName"];
			entity.SiteAbbrev = Convert.IsDBNull(dataRow["SiteAbbrev"]) ? null : (System.String)dataRow["SiteAbbrev"];
			entity.SiteAddress = (System.String)dataRow["SiteAddress"];
			entity.SiteMainContactNo = (System.String)dataRow["SiteMainContactNo"];
			entity.LocCode = Convert.IsDBNull(dataRow["LocCode"]) ? null : (System.String)dataRow["LocCode"];
			entity.IprocCode = Convert.IsDBNull(dataRow["IprocCode"]) ? null : (System.Int32?)dataRow["IprocCode"];
			entity.IsVisible = (System.Boolean)dataRow["IsVisible"];
			entity.Ordinal = Convert.IsDBNull(dataRow["Ordinal"]) ? null : (System.Int32?)dataRow["Ordinal"];
			entity.ModifiedbyUserId = (System.Int32)dataRow["ModifiedbyUserId"];
			entity.Editable = (System.Boolean)dataRow["Editable"];
			entity.EhsConsultantId = Convert.IsDBNull(dataRow["EhsConsultantId"]) ? null : (System.Int32?)dataRow["EhsConsultantId"];
			entity.SiteNameEbi = Convert.IsDBNull(dataRow["SiteNameEbi"]) ? null : (System.String)dataRow["SiteNameEbi"];
			entity.SiteNameIhs = Convert.IsDBNull(dataRow["SiteNameIhs"]) ? null : (System.String)dataRow["SiteNameIhs"];
			entity.EbiAllViewName = Convert.IsDBNull(dataRow["EbiAllViewName"]) ? null : (System.String)dataRow["EbiAllViewName"];
			entity.EbiServerName = Convert.IsDBNull(dataRow["EbiServerName"]) ? null : (System.String)dataRow["EbiServerName"];
			entity.EbiTodayViewName = Convert.IsDBNull(dataRow["EbiTodayViewName"]) ? null : (System.String)dataRow["EbiTodayViewName"];
			entity.SiteNameHr = Convert.IsDBNull(dataRow["SiteNameHr"]) ? null : (System.String)dataRow["SiteNameHr"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Sites"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Sites Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Sites entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region EhsConsultantIdSource	
			if (CanDeepLoad(entity, "EhsConsultant|EhsConsultantIdSource", deepLoadType, innerList) 
				&& entity.EhsConsultantIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.EhsConsultantId ?? (int)0);
				EhsConsultant tmpEntity = EntityManager.LocateEntity<EhsConsultant>(EntityLocator.ConstructKeyFromPkItems(typeof(EhsConsultant), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.EhsConsultantIdSource = tmpEntity;
				else
					entity.EhsConsultantIdSource = DataRepository.EhsConsultantProvider.GetByEhsConsultantId(transactionManager, (entity.EhsConsultantId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EhsConsultantIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.EhsConsultantIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.EhsConsultantProvider.DeepLoad(transactionManager, entity.EhsConsultantIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion EhsConsultantIdSource

			#region ModifiedbyUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedbyUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedbyUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedbyUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedbyUserIdSource = tmpEntity;
				else
					entity.ModifiedbyUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedbyUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedbyUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedbyUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedbyUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedbyUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySiteId methods when available
			
			#region CompanySiteCategoryStandardCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryStandardCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryStandardCollection = DataRepository.CompanySiteCategoryStandardProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.CompanySiteCategoryStandardCollection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryStandardCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryStandard>) DataRepository.CompanySiteCategoryStandardProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryStandardCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ConfigSa812
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "ConfigSa812|ConfigSa812", deepLoadType, innerList))
			{
				entity.ConfigSa812 = DataRepository.ConfigSa812Provider.GetBySiteId(transactionManager, entity.SiteId);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ConfigSa812' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.ConfigSa812 != null)
				{
					deepHandles.Add("ConfigSa812",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< ConfigSa812 >) DataRepository.ConfigSa812Provider.DeepLoad,
						new object[] { transactionManager, entity.ConfigSa812, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			#region ContactsContractorsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContactsContractors>|ContactsContractorsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactsContractorsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContactsContractorsCollection = DataRepository.ContactsContractorsProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.ContactsContractorsCollection.Count > 0)
				{
					deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContactsContractors>) DataRepository.ContactsContractorsProvider.DeepLoad,
						new object[] { transactionManager, entity.ContactsContractorsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region KpiCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Kpi>|KpiCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'KpiCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.KpiCollection = DataRepository.KpiProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.KpiCollection.Count > 0)
				{
					deepHandles.Add("KpiCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Kpi>) DataRepository.KpiProvider.DeepLoad,
						new object[] { transactionManager, entity.KpiCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryExceptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryException>|CompanySiteCategoryExceptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryExceptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryExceptionCollection = DataRepository.CompanySiteCategoryExceptionProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.CompanySiteCategoryExceptionCollection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryExceptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryException>) DataRepository.CompanySiteCategoryExceptionProvider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryExceptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EscalationChainProcurementCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EscalationChainProcurement>|EscalationChainProcurementCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EscalationChainProcurementCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EscalationChainProcurementCollection = DataRepository.EscalationChainProcurementProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.EscalationChainProcurementCollection.Count > 0)
				{
					deepHandles.Add("EscalationChainProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EscalationChainProcurement>) DataRepository.EscalationChainProcurementProvider.DeepLoad,
						new object[] { transactionManager, entity.EscalationChainProcurementCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdHocRadarItemsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdHocRadarItems>|AdHocRadarItemsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarItemsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdHocRadarItemsCollection = DataRepository.AdHocRadarItemsProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.AdHocRadarItemsCollection.Count > 0)
				{
					deepHandles.Add("AdHocRadarItemsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdHocRadarItems>) DataRepository.AdHocRadarItemsProvider.DeepLoad,
						new object[] { transactionManager, entity.AdHocRadarItemsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompaniesEhsimsMapCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesEhsimsMap>|CompaniesEhsimsMapCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesEhsimsMapCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesEhsimsMapCollection = DataRepository.CompaniesEhsimsMapProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.CompaniesEhsimsMapCollection.Count > 0)
				{
					deepHandles.Add("CompaniesEhsimsMapCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesEhsimsMap>) DataRepository.CompaniesEhsimsMapProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesEhsimsMapCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EbiMetricCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EbiMetric>|EbiMetricCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EbiMetricCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EbiMetricCollection = DataRepository.EbiMetricProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.EbiMetricCollection.Count > 0)
				{
					deepHandles.Add("EbiMetricCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EbiMetric>) DataRepository.EbiMetricProvider.DeepLoad,
						new object[] { transactionManager, entity.EbiMetricCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompaniesHrCrpDataCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesHrCrpData>|CompaniesHrCrpDataCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesHrCrpDataCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesHrCrpDataCollection = DataRepository.CompaniesHrCrpDataProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.CompaniesHrCrpDataCollection.Count > 0)
				{
					deepHandles.Add("CompaniesHrCrpDataCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesHrCrpData>) DataRepository.CompaniesHrCrpDataProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesHrCrpDataCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region RegionsSitesCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<RegionsSites>|RegionsSitesCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RegionsSitesCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RegionsSitesCollection = DataRepository.RegionsSitesProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.RegionsSitesCollection.Count > 0)
				{
					deepHandles.Add("RegionsSitesCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<RegionsSites>) DataRepository.RegionsSitesProvider.DeepLoad,
						new object[] { transactionManager, entity.RegionsSitesCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CsaCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Csa>|CsaCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CsaCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CsaCollection = DataRepository.CsaProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.CsaCollection.Count > 0)
				{
					deepHandles.Add("CsaCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Csa>) DataRepository.CsaProvider.DeepLoad,
						new object[] { transactionManager, entity.CsaCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnaireInitialLocationCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialLocationCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialLocationCollection = DataRepository.QuestionnaireInitialLocationProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.QuestionnaireInitialLocationCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialLocationCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialLocation>) DataRepository.QuestionnaireInitialLocationProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialLocationCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ResidentialCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Residential>|ResidentialCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ResidentialCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ResidentialCollection = DataRepository.ResidentialProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.ResidentialCollection.Count > 0)
				{
					deepHandles.Add("ResidentialCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Residential>) DataRepository.ResidentialProvider.DeepLoad,
						new object[] { transactionManager, entity.ResidentialCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CompanySiteCategoryException2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompanySiteCategoryException2>|CompanySiteCategoryException2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryException2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompanySiteCategoryException2Collection = DataRepository.CompanySiteCategoryException2Provider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.CompanySiteCategoryException2Collection.Count > 0)
				{
					deepHandles.Add("CompanySiteCategoryException2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompanySiteCategoryException2>) DataRepository.CompanySiteCategoryException2Provider.DeepLoad,
						new object[] { transactionManager, entity.CompanySiteCategoryException2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region QuestionnairePresentlyWithMetricCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnairePresentlyWithMetric>|QuestionnairePresentlyWithMetricCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnairePresentlyWithMetricCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnairePresentlyWithMetricCollection = DataRepository.QuestionnairePresentlyWithMetricProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.QuestionnairePresentlyWithMetricCollection.Count > 0)
				{
					deepHandles.Add("QuestionnairePresentlyWithMetricCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnairePresentlyWithMetric>) DataRepository.QuestionnairePresentlyWithMetricProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnairePresentlyWithMetricCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SqExemptionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SqExemption>|SqExemptionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SqExemptionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SqExemptionCollection = DataRepository.SqExemptionProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.SqExemptionCollection.Count > 0)
				{
					deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SqExemption>) DataRepository.SqExemptionProvider.DeepLoad,
						new object[] { transactionManager, entity.SqExemptionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ContactsAlcoaCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContactsAlcoa>|ContactsAlcoaCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactsAlcoaCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContactsAlcoaCollection = DataRepository.ContactsAlcoaProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.ContactsAlcoaCollection.Count > 0)
				{
					deepHandles.Add("ContactsAlcoaCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContactsAlcoa>) DataRepository.ContactsAlcoaProvider.DeepLoad,
						new object[] { transactionManager, entity.ContactsAlcoaCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region EscalationChainSafetyCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EscalationChainSafety>|EscalationChainSafetyCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EscalationChainSafetyCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EscalationChainSafetyCollection = DataRepository.EscalationChainSafetyProvider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.EscalationChainSafetyCollection.Count > 0)
				{
					deepHandles.Add("EscalationChainSafetyCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EscalationChainSafety>) DataRepository.EscalationChainSafetyProvider.DeepLoad,
						new object[] { transactionManager, entity.EscalationChainSafetyCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdHocRadarItems2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdHocRadarItems2>|AdHocRadarItems2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarItems2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdHocRadarItems2Collection = DataRepository.AdHocRadarItems2Provider.GetBySiteId(transactionManager, entity.SiteId);

				if (deep && entity.AdHocRadarItems2Collection.Count > 0)
				{
					deepHandles.Add("AdHocRadarItems2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdHocRadarItems2>) DataRepository.AdHocRadarItems2Provider.DeepLoad,
						new object[] { transactionManager, entity.AdHocRadarItems2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Sites object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Sites instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Sites Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Sites entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region EhsConsultantIdSource
			if (CanDeepSave(entity, "EhsConsultant|EhsConsultantIdSource", deepSaveType, innerList) 
				&& entity.EhsConsultantIdSource != null)
			{
				DataRepository.EhsConsultantProvider.Save(transactionManager, entity.EhsConsultantIdSource);
				entity.EhsConsultantId = entity.EhsConsultantIdSource.EhsConsultantId;
			}
			#endregion 
			
			#region ModifiedbyUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedbyUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedbyUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedbyUserIdSource);
				entity.ModifiedbyUserId = entity.ModifiedbyUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region ConfigSa812
			if (CanDeepSave(entity.ConfigSa812, "ConfigSa812|ConfigSa812", deepSaveType, innerList))
			{

				if (entity.ConfigSa812 != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.ConfigSa812.SiteId = entity.SiteId;
					//DataRepository.ConfigSa812Provider.Save(transactionManager, entity.ConfigSa812);
					deepHandles.Add("ConfigSa812",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< ConfigSa812 >) DataRepository.ConfigSa812Provider.DeepSave,
						new object[] { transactionManager, entity.ConfigSa812, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<CompanySiteCategoryStandard>
				if (CanDeepSave(entity.CompanySiteCategoryStandardCollection, "List<CompanySiteCategoryStandard>|CompanySiteCategoryStandardCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryStandard child in entity.CompanySiteCategoryStandardCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.CompanySiteCategoryStandardCollection.Count > 0 || entity.CompanySiteCategoryStandardCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryStandardProvider.Save(transactionManager, entity.CompanySiteCategoryStandardCollection);
						
						deepHandles.Add("CompanySiteCategoryStandardCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryStandard >) DataRepository.CompanySiteCategoryStandardProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryStandardCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContactsContractors>
				if (CanDeepSave(entity.ContactsContractorsCollection, "List<ContactsContractors>|ContactsContractorsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContactsContractors child in entity.ContactsContractorsCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.ContactsContractorsCollection.Count > 0 || entity.ContactsContractorsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContactsContractorsProvider.Save(transactionManager, entity.ContactsContractorsCollection);
						
						deepHandles.Add("ContactsContractorsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContactsContractors >) DataRepository.ContactsContractorsProvider.DeepSave,
							new object[] { transactionManager, entity.ContactsContractorsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Kpi>
				if (CanDeepSave(entity.KpiCollection, "List<Kpi>|KpiCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Kpi child in entity.KpiCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.KpiCollection.Count > 0 || entity.KpiCollection.DeletedItems.Count > 0)
					{
						//DataRepository.KpiProvider.Save(transactionManager, entity.KpiCollection);
						
						deepHandles.Add("KpiCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Kpi >) DataRepository.KpiProvider.DeepSave,
							new object[] { transactionManager, entity.KpiCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryException>
				if (CanDeepSave(entity.CompanySiteCategoryExceptionCollection, "List<CompanySiteCategoryException>|CompanySiteCategoryExceptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryException child in entity.CompanySiteCategoryExceptionCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.CompanySiteCategoryExceptionCollection.Count > 0 || entity.CompanySiteCategoryExceptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryExceptionProvider.Save(transactionManager, entity.CompanySiteCategoryExceptionCollection);
						
						deepHandles.Add("CompanySiteCategoryExceptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryException >) DataRepository.CompanySiteCategoryExceptionProvider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryExceptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EscalationChainProcurement>
				if (CanDeepSave(entity.EscalationChainProcurementCollection, "List<EscalationChainProcurement>|EscalationChainProcurementCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EscalationChainProcurement child in entity.EscalationChainProcurementCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.EscalationChainProcurementCollection.Count > 0 || entity.EscalationChainProcurementCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EscalationChainProcurementProvider.Save(transactionManager, entity.EscalationChainProcurementCollection);
						
						deepHandles.Add("EscalationChainProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EscalationChainProcurement >) DataRepository.EscalationChainProcurementProvider.DeepSave,
							new object[] { transactionManager, entity.EscalationChainProcurementCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdHocRadarItems>
				if (CanDeepSave(entity.AdHocRadarItemsCollection, "List<AdHocRadarItems>|AdHocRadarItemsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdHocRadarItems child in entity.AdHocRadarItemsCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.AdHocRadarItemsCollection.Count > 0 || entity.AdHocRadarItemsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdHocRadarItemsProvider.Save(transactionManager, entity.AdHocRadarItemsCollection);
						
						deepHandles.Add("AdHocRadarItemsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdHocRadarItems >) DataRepository.AdHocRadarItemsProvider.DeepSave,
							new object[] { transactionManager, entity.AdHocRadarItemsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompaniesEhsimsMap>
				if (CanDeepSave(entity.CompaniesEhsimsMapCollection, "List<CompaniesEhsimsMap>|CompaniesEhsimsMapCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesEhsimsMap child in entity.CompaniesEhsimsMapCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.CompaniesEhsimsMapCollection.Count > 0 || entity.CompaniesEhsimsMapCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesEhsimsMapProvider.Save(transactionManager, entity.CompaniesEhsimsMapCollection);
						
						deepHandles.Add("CompaniesEhsimsMapCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesEhsimsMap >) DataRepository.CompaniesEhsimsMapProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesEhsimsMapCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EbiMetric>
				if (CanDeepSave(entity.EbiMetricCollection, "List<EbiMetric>|EbiMetricCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EbiMetric child in entity.EbiMetricCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.EbiMetricCollection.Count > 0 || entity.EbiMetricCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EbiMetricProvider.Save(transactionManager, entity.EbiMetricCollection);
						
						deepHandles.Add("EbiMetricCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EbiMetric >) DataRepository.EbiMetricProvider.DeepSave,
							new object[] { transactionManager, entity.EbiMetricCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompaniesHrCrpData>
				if (CanDeepSave(entity.CompaniesHrCrpDataCollection, "List<CompaniesHrCrpData>|CompaniesHrCrpDataCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesHrCrpData child in entity.CompaniesHrCrpDataCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.CompaniesHrCrpDataCollection.Count > 0 || entity.CompaniesHrCrpDataCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesHrCrpDataProvider.Save(transactionManager, entity.CompaniesHrCrpDataCollection);
						
						deepHandles.Add("CompaniesHrCrpDataCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesHrCrpData >) DataRepository.CompaniesHrCrpDataProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesHrCrpDataCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<RegionsSites>
				if (CanDeepSave(entity.RegionsSitesCollection, "List<RegionsSites>|RegionsSitesCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(RegionsSites child in entity.RegionsSitesCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.RegionsSitesCollection.Count > 0 || entity.RegionsSitesCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RegionsSitesProvider.Save(transactionManager, entity.RegionsSitesCollection);
						
						deepHandles.Add("RegionsSitesCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< RegionsSites >) DataRepository.RegionsSitesProvider.DeepSave,
							new object[] { transactionManager, entity.RegionsSitesCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Csa>
				if (CanDeepSave(entity.CsaCollection, "List<Csa>|CsaCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Csa child in entity.CsaCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.CsaCollection.Count > 0 || entity.CsaCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CsaProvider.Save(transactionManager, entity.CsaCollection);
						
						deepHandles.Add("CsaCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Csa >) DataRepository.CsaProvider.DeepSave,
							new object[] { transactionManager, entity.CsaCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnaireInitialLocation>
				if (CanDeepSave(entity.QuestionnaireInitialLocationCollection, "List<QuestionnaireInitialLocation>|QuestionnaireInitialLocationCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialLocation child in entity.QuestionnaireInitialLocationCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.QuestionnaireInitialLocationCollection.Count > 0 || entity.QuestionnaireInitialLocationCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialLocationProvider.Save(transactionManager, entity.QuestionnaireInitialLocationCollection);
						
						deepHandles.Add("QuestionnaireInitialLocationCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialLocation >) DataRepository.QuestionnaireInitialLocationProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialLocationCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Residential>
				if (CanDeepSave(entity.ResidentialCollection, "List<Residential>|ResidentialCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Residential child in entity.ResidentialCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.ResidentialCollection.Count > 0 || entity.ResidentialCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ResidentialProvider.Save(transactionManager, entity.ResidentialCollection);
						
						deepHandles.Add("ResidentialCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Residential >) DataRepository.ResidentialProvider.DeepSave,
							new object[] { transactionManager, entity.ResidentialCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CompanySiteCategoryException2>
				if (CanDeepSave(entity.CompanySiteCategoryException2Collection, "List<CompanySiteCategoryException2>|CompanySiteCategoryException2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompanySiteCategoryException2 child in entity.CompanySiteCategoryException2Collection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.CompanySiteCategoryException2Collection.Count > 0 || entity.CompanySiteCategoryException2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.CompanySiteCategoryException2Provider.Save(transactionManager, entity.CompanySiteCategoryException2Collection);
						
						deepHandles.Add("CompanySiteCategoryException2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompanySiteCategoryException2 >) DataRepository.CompanySiteCategoryException2Provider.DeepSave,
							new object[] { transactionManager, entity.CompanySiteCategoryException2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<QuestionnairePresentlyWithMetric>
				if (CanDeepSave(entity.QuestionnairePresentlyWithMetricCollection, "List<QuestionnairePresentlyWithMetric>|QuestionnairePresentlyWithMetricCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnairePresentlyWithMetric child in entity.QuestionnairePresentlyWithMetricCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.QuestionnairePresentlyWithMetricCollection.Count > 0 || entity.QuestionnairePresentlyWithMetricCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnairePresentlyWithMetricProvider.Save(transactionManager, entity.QuestionnairePresentlyWithMetricCollection);
						
						deepHandles.Add("QuestionnairePresentlyWithMetricCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnairePresentlyWithMetric >) DataRepository.QuestionnairePresentlyWithMetricProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnairePresentlyWithMetricCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SqExemption>
				if (CanDeepSave(entity.SqExemptionCollection, "List<SqExemption>|SqExemptionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SqExemption child in entity.SqExemptionCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.SqExemptionCollection.Count > 0 || entity.SqExemptionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SqExemptionProvider.Save(transactionManager, entity.SqExemptionCollection);
						
						deepHandles.Add("SqExemptionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SqExemption >) DataRepository.SqExemptionProvider.DeepSave,
							new object[] { transactionManager, entity.SqExemptionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContactsAlcoa>
				if (CanDeepSave(entity.ContactsAlcoaCollection, "List<ContactsAlcoa>|ContactsAlcoaCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContactsAlcoa child in entity.ContactsAlcoaCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.ContactsAlcoaCollection.Count > 0 || entity.ContactsAlcoaCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContactsAlcoaProvider.Save(transactionManager, entity.ContactsAlcoaCollection);
						
						deepHandles.Add("ContactsAlcoaCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContactsAlcoa >) DataRepository.ContactsAlcoaProvider.DeepSave,
							new object[] { transactionManager, entity.ContactsAlcoaCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<EscalationChainSafety>
				if (CanDeepSave(entity.EscalationChainSafetyCollection, "List<EscalationChainSafety>|EscalationChainSafetyCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EscalationChainSafety child in entity.EscalationChainSafetyCollection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.EscalationChainSafetyCollection.Count > 0 || entity.EscalationChainSafetyCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EscalationChainSafetyProvider.Save(transactionManager, entity.EscalationChainSafetyCollection);
						
						deepHandles.Add("EscalationChainSafetyCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EscalationChainSafety >) DataRepository.EscalationChainSafetyProvider.DeepSave,
							new object[] { transactionManager, entity.EscalationChainSafetyCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdHocRadarItems2>
				if (CanDeepSave(entity.AdHocRadarItems2Collection, "List<AdHocRadarItems2>|AdHocRadarItems2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdHocRadarItems2 child in entity.AdHocRadarItems2Collection)
					{
						if(child.SiteIdSource != null)
						{
							child.SiteId = child.SiteIdSource.SiteId;
						}
						else
						{
							child.SiteId = entity.SiteId;
						}

					}

					if (entity.AdHocRadarItems2Collection.Count > 0 || entity.AdHocRadarItems2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.AdHocRadarItems2Provider.Save(transactionManager, entity.AdHocRadarItems2Collection);
						
						deepHandles.Add("AdHocRadarItems2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdHocRadarItems2 >) DataRepository.AdHocRadarItems2Provider.DeepSave,
							new object[] { transactionManager, entity.AdHocRadarItems2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SitesChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Sites</c>
	///</summary>
	public enum SitesChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>EhsConsultant</c> at EhsConsultantIdSource
		///</summary>
		[ChildEntityType(typeof(EhsConsultant))]
		EhsConsultant,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedbyUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for CompanySiteCategoryStandardCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryStandard>))]
		CompanySiteCategoryStandardCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
		///<summary>
		/// Entity <c>ConfigSa812</c> as OneToOne for ConfigSa812
		///</summary>
		[ChildEntityType(typeof(ConfigSa812))]
		ConfigSa812,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for ContactsContractorsCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContactsContractors>))]
		ContactsContractorsCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for KpiCollection
		///</summary>
		[ChildEntityType(typeof(TList<Kpi>))]
		KpiCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for CompanySiteCategoryExceptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryException>))]
		CompanySiteCategoryExceptionCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for EscalationChainProcurementCollection
		///</summary>
		[ChildEntityType(typeof(TList<EscalationChainProcurement>))]
		EscalationChainProcurementCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for AdHocRadarItemsCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdHocRadarItems>))]
		AdHocRadarItemsCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for CompaniesEhsimsMapCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesEhsimsMap>))]
		CompaniesEhsimsMapCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for EbiMetricCollection
		///</summary>
		[ChildEntityType(typeof(TList<EbiMetric>))]
		EbiMetricCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for CompaniesHrCrpDataCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesHrCrpData>))]
		CompaniesHrCrpDataCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for RegionsSitesCollection
		///</summary>
		[ChildEntityType(typeof(TList<RegionsSites>))]
		RegionsSitesCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for CsaCollection
		///</summary>
		[ChildEntityType(typeof(TList<Csa>))]
		CsaCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for QuestionnaireInitialLocationCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialLocation>))]
		QuestionnaireInitialLocationCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for ResidentialCollection
		///</summary>
		[ChildEntityType(typeof(TList<Residential>))]
		ResidentialCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for CompanySiteCategoryException2Collection
		///</summary>
		[ChildEntityType(typeof(TList<CompanySiteCategoryException2>))]
		CompanySiteCategoryException2Collection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for QuestionnairePresentlyWithMetricCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnairePresentlyWithMetric>))]
		QuestionnairePresentlyWithMetricCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for SqExemptionCollection
		///</summary>
		[ChildEntityType(typeof(TList<SqExemption>))]
		SqExemptionCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for ContactsAlcoaCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContactsAlcoa>))]
		ContactsAlcoaCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for EscalationChainSafetyCollection
		///</summary>
		[ChildEntityType(typeof(TList<EscalationChainSafety>))]
		EscalationChainSafetyCollection,

		///<summary>
		/// Collection of <c>Sites</c> as OneToMany for AdHocRadarItems2Collection
		///</summary>
		[ChildEntityType(typeof(TList<AdHocRadarItems2>))]
		AdHocRadarItems2Collection,
	}
	
	#endregion SitesChildEntityTypes
	
	#region SitesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SitesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesFilterBuilder : SqlFilterBuilder<SitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesFilterBuilder class.
		/// </summary>
		public SitesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesFilterBuilder
	
	#region SitesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SitesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Sites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesParameterBuilder : ParameterizedSqlFilterBuilder<SitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesParameterBuilder class.
		/// </summary>
		public SitesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SitesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SitesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SitesParameterBuilder
	
	#region SitesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SitesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Sites"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SitesSortBuilder : SqlSortBuilder<SitesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesSqlSortBuilder class.
		/// </summary>
		public SitesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SitesSortBuilder
	
} // end namespace
