﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireCommentsAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireCommentsAuditDataSourceDesigner))]
	public class QuestionnaireCommentsAuditDataSource : ProviderDataSource<QuestionnaireCommentsAudit, QuestionnaireCommentsAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditDataSource class.
		/// </summary>
		public QuestionnaireCommentsAuditDataSource() : base(new QuestionnaireCommentsAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireCommentsAuditDataSourceView used by the QuestionnaireCommentsAuditDataSource.
		/// </summary>
		protected QuestionnaireCommentsAuditDataSourceView QuestionnaireCommentsAuditView
		{
			get { return ( View as QuestionnaireCommentsAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireCommentsAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireCommentsAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireCommentsAuditSelectMethod selectMethod = QuestionnaireCommentsAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireCommentsAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireCommentsAuditDataSourceView class that is to be
		/// used by the QuestionnaireCommentsAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireCommentsAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireCommentsAudit, QuestionnaireCommentsAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireCommentsAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireCommentsAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireCommentsAuditDataSourceView : ProviderDataSourceView<QuestionnaireCommentsAudit, QuestionnaireCommentsAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireCommentsAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireCommentsAuditDataSourceView(QuestionnaireCommentsAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireCommentsAuditDataSource QuestionnaireCommentsAuditOwner
		{
			get { return Owner as QuestionnaireCommentsAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireCommentsAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireCommentsAuditOwner.SelectMethod; }
			set { QuestionnaireCommentsAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireCommentsAuditService QuestionnaireCommentsAuditProvider
		{
			get { return Provider as QuestionnaireCommentsAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireCommentsAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireCommentsAudit> results = null;
			QuestionnaireCommentsAudit item;
			count = 0;
			
			System.Int32 _questionnaireCommenAudittId;

			switch ( SelectMethod )
			{
				case QuestionnaireCommentsAuditSelectMethod.Get:
					QuestionnaireCommentsAuditKey entityKey  = new QuestionnaireCommentsAuditKey();
					entityKey.Load(values);
					item = QuestionnaireCommentsAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireCommentsAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireCommentsAuditSelectMethod.GetAll:
                    results = QuestionnaireCommentsAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireCommentsAuditSelectMethod.GetPaged:
					results = QuestionnaireCommentsAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireCommentsAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireCommentsAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireCommentsAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireCommentsAuditSelectMethod.GetByQuestionnaireCommenAudittId:
					_questionnaireCommenAudittId = ( values["QuestionnaireCommenAudittId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireCommenAudittId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireCommentsAuditProvider.GetByQuestionnaireCommenAudittId(_questionnaireCommenAudittId);
					results = new TList<QuestionnaireCommentsAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireCommentsAuditSelectMethod.Get || SelectMethod == QuestionnaireCommentsAuditSelectMethod.GetByQuestionnaireCommenAudittId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireCommentsAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireCommentsAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireCommentsAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireCommentsAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireCommentsAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireCommentsAuditDataSource class.
	/// </summary>
	public class QuestionnaireCommentsAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireCommentsAudit, QuestionnaireCommentsAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireCommentsAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireCommentsAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireCommentsAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireCommentsAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireCommentsAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireCommentsAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireCommentsAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireCommentsAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireCommentsAuditDataSourceActionList(QuestionnaireCommentsAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireCommentsAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireCommentsAuditDataSourceActionList
	
	#endregion QuestionnaireCommentsAuditDataSourceDesigner
	
	#region QuestionnaireCommentsAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireCommentsAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireCommentsAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireCommenAudittId method.
		/// </summary>
		GetByQuestionnaireCommenAudittId
	}
	
	#endregion QuestionnaireCommentsAuditSelectMethod

	#region QuestionnaireCommentsAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditFilter : SqlFilter<QuestionnaireCommentsAuditColumn>
	{
	}
	
	#endregion QuestionnaireCommentsAuditFilter

	#region QuestionnaireCommentsAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireCommentsAuditColumn>
	{
	}
	
	#endregion QuestionnaireCommentsAuditExpressionBuilder	

	#region QuestionnaireCommentsAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireCommentsAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireCommentsAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsAuditProperty : ChildEntityProperty<QuestionnaireCommentsAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireCommentsAuditProperty
}

