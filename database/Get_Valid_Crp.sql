
/****** Object:  StoredProcedure [dbo].[Get_Valid_Crp]    Script Date: 3/08/2015 4:54:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Get_Valid_Crp]                      
(                      
 @CompanyId int      
)                      
AS       
select A.Person_id,A.Full_Name,DATEADD(yy,2, A.COURSE_END_DATE) As Valid_Until      
,D.COMPANYID,E.SITEID from HR..XXHR_ARP_CRP_ALL_V AS A      
LEFT OUTER JOIN HR..XXHR_PEOPLE_DETAILS_V AS B      
ON A.PERSON_ID = B.PERSON_ID      
LEFT OUTER JOIN CompanyVendor AS C      
ON C.VENDOR_NUMBER = B.CWK_VENDOR_NO     
LEFT OUTER JOIN Companies AS D      
ON D.CompanyAbn = C.TAX_REGISTRATION_NUMBER      
LEFT OUTER JOIN SITES AS E      
ON E.SITENAMEHR=B.LOCATION     
WHERE A.PER_TYPE_ID = 'C'      
AND DATEADD(yy,2, A.COURSE_END_DATE)> getdate()      
AND D.COMPANYID =@CompanyId
----------------------------------------

GO


