﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskEmailTemplateService
    {
        AdminTaskEmailTemplate Get(Expression<Func<AdminTaskEmailTemplate, bool>> where, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes);
        List<AdminTaskEmailTemplate> GetMany(int? top, Expression<Func<AdminTaskEmailTemplate, bool>> where, Func<IQueryable<AdminTaskEmailTemplate>, IOrderedQueryable<AdminTaskEmailTemplate>> orderBy, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes);
        List<AdminTaskEmailTemplate> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailTemplate, bool>> where, Func<IQueryable<AdminTaskEmailTemplate>, IOrderedQueryable<AdminTaskEmailTemplate>> orderBy, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskEmailTemplate, bool>> where, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes);
        void Insert(AdminTaskEmailTemplate item, bool saveChanges = true);
        void Update(AdminTaskEmailTemplate item, bool saveChanges = true);
        void Delete(AdminTaskEmailTemplate item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskEmailTemplateService : IAdminTaskEmailTemplateService
    {
        private readonly IRepository<AdminTaskEmailTemplate> _repository;

        public AdminTaskEmailTemplateService(IRepository<AdminTaskEmailTemplate> repository)
        {
            this._repository = repository;
        }

        public AdminTaskEmailTemplate Get(Expression<Func<AdminTaskEmailTemplate, bool>> where, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskEmailTemplate> GetMany(int? top, Expression<Func<AdminTaskEmailTemplate, bool>> where, Func<IQueryable<AdminTaskEmailTemplate>, IOrderedQueryable<AdminTaskEmailTemplate>> orderBy, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskEmailTemplate> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailTemplate, bool>> where, Func<IQueryable<AdminTaskEmailTemplate>, IOrderedQueryable<AdminTaskEmailTemplate>> orderBy, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskEmailTemplate, bool>> where, List<Expression<Func<AdminTaskEmailTemplate, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskEmailTemplate item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplate");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskEmailTemplate item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplate");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskEmailTemplate item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplate");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
