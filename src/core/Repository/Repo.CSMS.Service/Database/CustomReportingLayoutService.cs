﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICustomReportingLayoutService
    {
        CustomReportingLayout Get(Expression<Func<CustomReportingLayout, bool>> where, List<Expression<Func<CustomReportingLayout, object>>> includes);
        List<CustomReportingLayout> GetMany(int? top, Expression<Func<CustomReportingLayout, bool>> where, Func<IQueryable<CustomReportingLayout>, IOrderedQueryable<CustomReportingLayout>> orderBy, List<Expression<Func<CustomReportingLayout, object>>> includes);
        List<CustomReportingLayout> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CustomReportingLayout, bool>> where, Func<IQueryable<CustomReportingLayout>, IOrderedQueryable<CustomReportingLayout>> orderBy, List<Expression<Func<CustomReportingLayout, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CustomReportingLayout, bool>> where, List<Expression<Func<CustomReportingLayout, object>>> includes);
        void Insert(CustomReportingLayout item, bool saveChanges = true);
        void Update(CustomReportingLayout item, bool saveChanges = true);
        void Delete(CustomReportingLayout item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CustomReportingLayoutService : ICustomReportingLayoutService
    {
        private readonly IRepository<CustomReportingLayout> _repository;

        public CustomReportingLayoutService(IRepository<CustomReportingLayout> repository)
        {
            this._repository = repository;
        }

        public CustomReportingLayout Get(Expression<Func<CustomReportingLayout, bool>> where, List<Expression<Func<CustomReportingLayout, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CustomReportingLayout> GetMany(int? top, Expression<Func<CustomReportingLayout, bool>> where, Func<IQueryable<CustomReportingLayout>, IOrderedQueryable<CustomReportingLayout>> orderBy, List<Expression<Func<CustomReportingLayout, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CustomReportingLayout> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CustomReportingLayout, bool>> where, Func<IQueryable<CustomReportingLayout>, IOrderedQueryable<CustomReportingLayout>> orderBy, List<Expression<Func<CustomReportingLayout, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CustomReportingLayout, bool>> where, List<Expression<Func<CustomReportingLayout, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CustomReportingLayout item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CustomReportingLayout");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CustomReportingLayout item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CustomReportingLayout");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CustomReportingLayout item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CustomReportingLayout");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
