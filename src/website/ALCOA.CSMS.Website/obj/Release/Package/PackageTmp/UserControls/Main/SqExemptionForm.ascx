﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SqExemptionForm.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Main.SqExemptionForm" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<table border="0" cellpadding="2" cellspacing="0" width="900">
    <tr>
        <td class="pageName" colspan="7">
            <span class="title">Safety Qualification Exemption Form<asp:Button ID="btnEdit" runat="server" BackColor="White"
                BorderColor="White" BorderStyle="None" Font-Names="Verdana" Font-Size="XX-Small"
                Font-Underline="True" ForeColor="Red" OnClick="btnEdit_Click" Text="(Edit)" /></span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
        </td>
        <td class="pageName" colspan="1">
        </td>
    </tr>
</table>
<br />
<dx:ASPxHtmlEditor ID="aheTemplate" runat="server" Height="600px" Width="900px" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue">
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <ViewArea>
            <Border BorderColor="#002D96" />
        </ViewArea>
    </Styles>
<Settings AllowHtmlView="False" />

<SettingsImageUpload>
<ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></ValidationSettings>
</SettingsImageUpload>

<SettingsImageSelector>
<CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png"></CommonSettings>
</SettingsImageSelector>

<SettingsDocumentSelector>
<CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp"></CommonSettings>
</SettingsDocumentSelector>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFileManager>
        <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
        </FolderContainerNodeLoadingPanel>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanel>
    </ImagesFileManager>
</dx:ASPxHtmlEditor>
<br />
<dx:ASPxLabel ID="lblSave" runat="server" Text="" ForeColor="Red" Visible="false">
</dx:ASPxLabel>
<br />
<dx:ASPxButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="false">
</dx:ASPxButton>
<br />