using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;

public partial class Adminv2_UserControls_UsersHsAssessor : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
        }
    }

    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        bool active = true;
        if (e.NewValues["Enabled"] == null) { e.NewValues["Enabled"] = active; }
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
        Label label = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "noCompanies") as Label;
        int EhsConsultantId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "EhsConsultantId");

        CompaniesService cService = new CompaniesService();
        TList<Companies> c = cService.GetByEhsConsultantId(EhsConsultantId);
        label.Text = c.Count.ToString();
    }
}
