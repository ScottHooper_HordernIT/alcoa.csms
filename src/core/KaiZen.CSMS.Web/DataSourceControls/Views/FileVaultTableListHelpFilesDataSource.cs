﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultTableListHelpFilesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(FileVaultTableListHelpFilesDataSourceDesigner))]
	public class FileVaultTableListHelpFilesDataSource : ReadOnlyDataSource<FileVaultTableListHelpFiles>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesDataSource class.
		/// </summary>
		public FileVaultTableListHelpFilesDataSource() : base(new FileVaultTableListHelpFilesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultTableListHelpFilesDataSourceView used by the FileVaultTableListHelpFilesDataSource.
		/// </summary>
		protected FileVaultTableListHelpFilesDataSourceView FileVaultTableListHelpFilesView
		{
			get { return ( View as FileVaultTableListHelpFilesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultTableListHelpFilesDataSourceView class that is to be
		/// used by the FileVaultTableListHelpFilesDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultTableListHelpFilesDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultTableListHelpFiles, Object> GetNewDataSourceView()
		{
			return new FileVaultTableListHelpFilesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultTableListHelpFilesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultTableListHelpFilesDataSourceView : ReadOnlyDataSourceView<FileVaultTableListHelpFiles>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableListHelpFilesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultTableListHelpFilesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultTableListHelpFilesDataSourceView(FileVaultTableListHelpFilesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultTableListHelpFilesDataSource FileVaultTableListHelpFilesOwner
		{
			get { return Owner as FileVaultTableListHelpFilesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultTableListHelpFilesService FileVaultTableListHelpFilesProvider
		{
			get { return Provider as FileVaultTableListHelpFilesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region FileVaultTableListHelpFilesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultTableListHelpFilesDataSource class.
	/// </summary>
	public class FileVaultTableListHelpFilesDataSourceDesigner : ReadOnlyDataSourceDesigner<FileVaultTableListHelpFiles>
	{
	}

	#endregion FileVaultTableListHelpFilesDataSourceDesigner

	#region FileVaultTableListHelpFilesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListHelpFilesFilter : SqlFilter<FileVaultTableListHelpFilesColumn>
	{
	}

	#endregion FileVaultTableListHelpFilesFilter

	#region FileVaultTableListHelpFilesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTableListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableListHelpFilesExpressionBuilder : SqlExpressionBuilder<FileVaultTableListHelpFilesColumn>
	{
	}
	
	#endregion FileVaultTableListHelpFilesExpressionBuilder		
}

