﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireActionLogProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireActionLogDataSourceDesigner))]
	public class QuestionnaireActionLogDataSource : ProviderDataSource<QuestionnaireActionLog, QuestionnaireActionLogKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogDataSource class.
		/// </summary>
		public QuestionnaireActionLogDataSource() : base(new QuestionnaireActionLogService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireActionLogDataSourceView used by the QuestionnaireActionLogDataSource.
		/// </summary>
		protected QuestionnaireActionLogDataSourceView QuestionnaireActionLogView
		{
			get { return ( View as QuestionnaireActionLogDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireActionLogDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireActionLogSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireActionLogSelectMethod selectMethod = QuestionnaireActionLogSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireActionLogSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireActionLogDataSourceView class that is to be
		/// used by the QuestionnaireActionLogDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireActionLogDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireActionLog, QuestionnaireActionLogKey> GetNewDataSourceView()
		{
			return new QuestionnaireActionLogDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireActionLogDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireActionLogDataSourceView : ProviderDataSourceView<QuestionnaireActionLog, QuestionnaireActionLogKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireActionLogDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireActionLogDataSourceView(QuestionnaireActionLogDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireActionLogDataSource QuestionnaireActionLogOwner
		{
			get { return Owner as QuestionnaireActionLogDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireActionLogSelectMethod SelectMethod
		{
			get { return QuestionnaireActionLogOwner.SelectMethod; }
			set { QuestionnaireActionLogOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireActionLogService QuestionnaireActionLogProvider
		{
			get { return Provider as QuestionnaireActionLogService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireActionLog> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireActionLog> results = null;
			QuestionnaireActionLog item;
			count = 0;
			
			System.Int32 _questionnaireActionLogId;
			System.Int32 _questionnaireId;
			System.Int32 _createdByRoleId;
			System.Int32 _questionnaireActionId;
			System.Int32 _createdByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireActionLogSelectMethod.Get:
					QuestionnaireActionLogKey entityKey  = new QuestionnaireActionLogKey();
					entityKey.Load(values);
					item = QuestionnaireActionLogProvider.Get(entityKey);
					results = new TList<QuestionnaireActionLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireActionLogSelectMethod.GetAll:
                    results = QuestionnaireActionLogProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireActionLogSelectMethod.GetPaged:
					results = QuestionnaireActionLogProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireActionLogSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireActionLogProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireActionLogProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireActionLogSelectMethod.GetByQuestionnaireActionLogId:
					_questionnaireActionLogId = ( values["QuestionnaireActionLogId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireActionLogId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireActionLogProvider.GetByQuestionnaireActionLogId(_questionnaireActionLogId);
					results = new TList<QuestionnaireActionLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireActionLogSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireActionLogProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireActionLogSelectMethod.GetByCreatedByRoleId:
					_createdByRoleId = ( values["CreatedByRoleId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByRoleId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireActionLogProvider.GetByCreatedByRoleId(_createdByRoleId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireActionLogSelectMethod.GetByQuestionnaireActionId:
					_questionnaireActionId = ( values["QuestionnaireActionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireActionId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireActionLogProvider.GetByQuestionnaireActionId(_questionnaireActionId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireActionLogSelectMethod.GetByCreatedByUserId:
					_createdByUserId = ( values["CreatedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CreatedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireActionLogProvider.GetByCreatedByUserId(_createdByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireActionLogSelectMethod.Get || SelectMethod == QuestionnaireActionLogSelectMethod.GetByQuestionnaireActionLogId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireActionLog entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireActionLogProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireActionLog> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireActionLogProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireActionLogDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireActionLogDataSource class.
	/// </summary>
	public class QuestionnaireActionLogDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireActionLog, QuestionnaireActionLogKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogDataSourceDesigner class.
		/// </summary>
		public QuestionnaireActionLogDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireActionLogSelectMethod SelectMethod
		{
			get { return ((QuestionnaireActionLogDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireActionLogDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireActionLogDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireActionLogDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireActionLogDataSourceActionList : DesignerActionList
	{
		private QuestionnaireActionLogDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireActionLogDataSourceActionList(QuestionnaireActionLogDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireActionLogSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireActionLogDataSourceActionList
	
	#endregion QuestionnaireActionLogDataSourceDesigner
	
	#region QuestionnaireActionLogSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireActionLogDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireActionLogSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireActionLogId method.
		/// </summary>
		GetByQuestionnaireActionLogId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByCreatedByRoleId method.
		/// </summary>
		GetByCreatedByRoleId,
		/// <summary>
		/// Represents the GetByQuestionnaireActionId method.
		/// </summary>
		GetByQuestionnaireActionId,
		/// <summary>
		/// Represents the GetByCreatedByUserId method.
		/// </summary>
		GetByCreatedByUserId
	}
	
	#endregion QuestionnaireActionLogSelectMethod

	#region QuestionnaireActionLogFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFilter : SqlFilter<QuestionnaireActionLogColumn>
	{
	}
	
	#endregion QuestionnaireActionLogFilter

	#region QuestionnaireActionLogExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogExpressionBuilder : SqlExpressionBuilder<QuestionnaireActionLogColumn>
	{
	}
	
	#endregion QuestionnaireActionLogExpressionBuilder	

	#region QuestionnaireActionLogProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireActionLogChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogProperty : ChildEntityProperty<QuestionnaireActionLogChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireActionLogProperty
}

