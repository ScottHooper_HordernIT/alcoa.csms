﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminPlanOperatorService
    {
        AdminPlanOperator Get(Expression<Func<AdminPlanOperator, bool>> where, List<Expression<Func<AdminPlanOperator, object>>> includes);
        List<AdminPlanOperator> GetMany(int? top, Expression<Func<AdminPlanOperator, bool>> where, Func<IQueryable<AdminPlanOperator>, IOrderedQueryable<AdminPlanOperator>> orderBy, List<Expression<Func<AdminPlanOperator, object>>> includes);
        List<AdminPlanOperator> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminPlanOperator, bool>> where, Func<IQueryable<AdminPlanOperator>, IOrderedQueryable<AdminPlanOperator>> orderBy, List<Expression<Func<AdminPlanOperator, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminPlanOperator, bool>> where, List<Expression<Func<AdminPlanOperator, object>>> includes);
        void Insert(AdminPlanOperator item, bool saveChanges = true);
        void Update(AdminPlanOperator item, bool saveChanges = true);
        void Delete(AdminPlanOperator item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminPlanOperatorService : IAdminPlanOperatorService
    {
        private readonly IRepository<AdminPlanOperator> _repository;

        public AdminPlanOperatorService(IRepository<AdminPlanOperator> repository)
        {
            this._repository = repository;
        }

        public AdminPlanOperator Get(Expression<Func<AdminPlanOperator, bool>> where, List<Expression<Func<AdminPlanOperator, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminPlanOperator> GetMany(int? top, Expression<Func<AdminPlanOperator, bool>> where, Func<IQueryable<AdminPlanOperator>, IOrderedQueryable<AdminPlanOperator>> orderBy, List<Expression<Func<AdminPlanOperator, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminPlanOperator> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminPlanOperator, bool>> where, Func<IQueryable<AdminPlanOperator>, IOrderedQueryable<AdminPlanOperator>> orderBy, List<Expression<Func<AdminPlanOperator, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminPlanOperator, bool>> where, List<Expression<Func<AdminPlanOperator, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminPlanOperator item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminPlanOperator");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminPlanOperator item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminPlanOperator");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminPlanOperator item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminPlanOperator");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
