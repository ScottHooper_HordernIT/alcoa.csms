﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireReportOverview_SubQuery1' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireReportOverviewSubQuery1 : QuestionnaireReportOverviewSubQuery1Base
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireReportOverviewSubQuery1"/> instance.
		///</summary>
		public QuestionnaireReportOverviewSubQuery1():base(){}	
		
		#endregion
	}
}
