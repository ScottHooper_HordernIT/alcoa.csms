﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.CSMS.Common.Helpers
{
    public class AppSettings
    {
        #region Pagination Settings

        public static int DefaultGridPageSize
        {
            get
            {
                int pageSize = 0;

                //If no page size in database then get it from app settings
                if (pageSize == 0)
                {
                    int.TryParse(ConfigurationManager.AppSettings["Default.Grid.PageSize"], out pageSize);
                }

                return pageSize;
            }
        }

        #endregion

        #region Website Mode Settings

        public static Mode WebsiteMode
        {
            get
            {
                Mode websiteMode = (Mode)Enum.Parse(typeof(Mode), ConfigurationManager.AppSettings["Website.Mode"]);

                if (!Enum.IsDefined(typeof(Mode), websiteMode))
                {
                    websiteMode = Mode.Unknown;
                }

                return websiteMode;
            }
        }

        #endregion

        #region Fitbit Settings

        public static string FitbitConsumerKey
        {
            get
            {
                return ConfigurationManager.AppSettings["FitbitConsumerKey"];
            }
        }

        public static string FitbitConsumerSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["FitbitConsumerSecret"];
            }
        }

        public static string FitbitRequestTokenUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["FitbitRequestTokenUrl"];
            }
        }

        public static string FitbitAccessTokenUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["FitbitAccessTokenUrl"];
            }
        }

        public static string FitbitAuthoriseUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["FitbitAuthoriseUrl"];
            }
        }

        public static bool FitbitCodeIsTest
        {
            get
            {
                bool fitbitCodeIsTest = false;
                bool.TryParse(ConfigurationManager.AppSettings["FitbitCodeIsTest"], out fitbitCodeIsTest);
                return fitbitCodeIsTest;
            }
        }

        public static int FitbitSyncWarningHours
        {
            get
            {
                int fitbitSyncWarningHours = 0;
                int.TryParse(ConfigurationManager.AppSettings["FitbitSyncWarningHours"], out fitbitSyncWarningHours);
                return fitbitSyncWarningHours;
            }
        }

        #endregion

        #region General Settings

        public static int MinTeamMemberCount
        {
            get
            {
                int minTeamMemberCount = 0;
                int.TryParse(ConfigurationManager.AppSettings["MinTeamMemberCount"], out minTeamMemberCount);
                return minTeamMemberCount;
            }
        }

        public static int MaxTeamMemberCount
        {
            get
            {
                int maxTeamMemberCount = 0;
                int.TryParse(ConfigurationManager.AppSettings["MaxTeamMemberCount"], out maxTeamMemberCount);
                return maxTeamMemberCount;
            }
        }

        public static int TeamCreationReminderIntervalDays
        {
            get
            {
                int teamCreationReminderIntervalDays = 0;
                int.TryParse(ConfigurationManager.AppSettings["TeamCreationReminderIntervalDays"], out teamCreationReminderIntervalDays);
                return teamCreationReminderIntervalDays;
            }
        }

        public static int LinkDeviceReminderIntervalDays
        {
            get
            {
                int linkDeviceReminderIntervalDays = 0;
                int.TryParse(ConfigurationManager.AppSettings["LinkDeviceReminderIntervalDays"], out linkDeviceReminderIntervalDays);
                return linkDeviceReminderIntervalDays;
            }
        }

        public static int MailMessageQueueLimit
        {
            get
            {
                int mailMessageQueueLimit = 0;
                int.TryParse(ConfigurationManager.AppSettings["MailMessage.QueueLimit"], out mailMessageQueueLimit);
                return mailMessageQueueLimit;
            }
        }

        public static bool EmailServiceIsTest
        {
            get
            {
                bool emailServiceIsTest = false;
                bool.TryParse(ConfigurationManager.AppSettings["EmailService.IsTest"], out emailServiceIsTest);
                return emailServiceIsTest;
            }
        }

        public static string EmailServiceTestToEmailAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailService.TestToEmailAddress"];
            }
        }

        public static bool RegisterProcessMailMessageQueueTask
        {
            get
            {
                bool registerProcessMailMessageQueueTask = false;
                bool.TryParse(ConfigurationManager.AppSettings["RegisterProcessMailMessageQueueTask"], out registerProcessMailMessageQueueTask);
                return registerProcessMailMessageQueueTask;
            }
        }

        public static bool RegisterCompetitionTaskSchedulerTask
        {
            get
            {
                bool registerCompetitionTaskSchedulerTask = false;
                bool.TryParse(ConfigurationManager.AppSettings["RegisterCompetitionTaskSchedulerTask"], out registerCompetitionTaskSchedulerTask);
                return registerCompetitionTaskSchedulerTask;
            }
        }

        public static bool RegisterRewardEarnedTask
        {
            get
            {
                bool registerRewardEarnedTask = false;
                bool.TryParse(ConfigurationManager.AppSettings["RegisterRewardEarnedTask"], out registerRewardEarnedTask);
                return registerRewardEarnedTask;
            }
        }

        #endregion

        #region Caching

        public static Caching Caching
        {
            get
            {
                Caching caching = Caching.Disabled;

                try
                {
                    caching = (Caching)Enum.Parse(typeof(Caching), ConfigurationManager.AppSettings["Caching"]);
                }
                catch { }

                if (!Enum.IsDefined(typeof(Caching), caching))
                {
                    caching = Caching.Disabled;
                }

                return caching;
            }
        }

        #endregion
    }

    public enum Caching
    {
        Enabled,
        Disabled
    }

    public enum Mode
    {
        Unknown,
        Live,
        Test,
        Demo,
    }
}
