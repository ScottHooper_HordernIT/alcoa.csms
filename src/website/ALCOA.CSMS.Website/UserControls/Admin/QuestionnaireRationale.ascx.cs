﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.Text;

public partial class UserControls_Admin_QuestionnaireRationale : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            FillQuestionCombo("Supplier");
            cbQuestion.SelectedItem = null;
            cbQuestion.Text = "<Select Question>";
        }
    }
    protected void cbArea_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillQuestionCombo(cbArea.SelectedItem.Value.ToString());
    }
    protected void FillQuestionCombo(string Area)
    {
        cbQuestion.Items.Clear();
        if (Area == "Supplier")
        {
            QuestionnaireMainRationaleService qmrService = new QuestionnaireMainRationaleService();
            TList<QuestionnaireMainRationale> qmrList = qmrService.GetAll();
            foreach (QuestionnaireMainRationale q in qmrList)
            {
                string questionText = "";
                QuestionnaireMainQuestionService qmqService = new QuestionnaireMainQuestionService();
                QuestionnaireMainQuestion qmq = qmqService.GetByQuestionNo(q.QuestionNo.ToString());
                if (qmq != null)
                {
                    questionText = StringUtilities.TruncateString(qmq.QuestionText, 100, true);
                    cbQuestion.Items.Add(String.Format("Question: {0} - {1}", q.QuestionNo, questionText), q.QuestionNo.ToString());
                }
                else
                {
                    cbQuestion.Items.Add(String.Format("Question: {0}", q.QuestionNo), q.QuestionNo.ToString());
                }
            }
        }
        else if (Area == "Verification")
        {
            QuestionnaireVerificationRationaleService qvrService = new QuestionnaireVerificationRationaleService();
            TList<QuestionnaireVerificationRationale> qvrList = qvrService.GetAll();
            foreach (QuestionnaireVerificationRationale q in qvrList)
            {
                string text = String.Format("Section: {0} Question: {1}", q.SectionId, q.QuestionId);
                if(q.QuestionId == "Summary") text.Replace(" Question: ", " - ");
                cbQuestion.Items.Add(text, String.Format("{0}_{1}", q.SectionId, q.QuestionId));
            }
        }
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        Load2();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Save();
    }
    protected void btnSaveClose_Click(object sender, EventArgs e)
    {
        Save();
        Close();
        ASPxLabel1.Text = "Saved & Closed.";
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Close();
    }

    protected void Load2()
    {
        cbArea.Enabled = false;
        cbQuestion.Enabled = false;
        btnEdit.Enabled = false;
        ASPxHtmlEditor1.Html = "";
        ASPxHtmlEditor2.Html = "";


        string Area = cbArea.SelectedItem.Value.ToString();
        string Question = cbQuestion.SelectedItem.Value.ToString();
        if (Area == "Supplier")
        {
            QuestionnaireMainRationaleService qmrService = new QuestionnaireMainRationaleService();
            QuestionnaireMainRationale q = qmrService.GetByQuestionNo(Question);
            if (q != null)
            {
                if (q.Rationale != null)
                {
                    if (q.Rationale.Length != 0)
                        ASPxHtmlEditor1.Html = Encoding.ASCII.GetString(q.Rationale);
                }
                if (q.Assistance != null)
                {
                    if (q.Assistance.Length != 0)
                        ASPxHtmlEditor2.Html = Encoding.ASCII.GetString(q.Assistance);
                }
            }

        }
        else if (Area == "Verification")
        {
            string[] _question = Question.Split('_');
            int SectionId = -1;
            string QuestionId = _question[1];
            int.TryParse(_question[0], out SectionId);

            if (SectionId == -1 || String.IsNullOrEmpty(QuestionId)) { throw new Exception("WTF"); };

            QuestionnaireVerificationRationaleService qvrService = new QuestionnaireVerificationRationaleService();
            QuestionnaireVerificationRationale q = qvrService.GetBySectionIdQuestionId(SectionId, QuestionId);
            if (q != null)
            {
                if (q.Rationale != null)
                {
                    if (q.Rationale.Length != 0)
                        ASPxHtmlEditor1.Html = Encoding.ASCII.GetString(q.Rationale);
                }
                if (q.Assistance != null)
                {
                    if (q.Assistance.Length != 0)
                        ASPxHtmlEditor2.Html = Encoding.ASCII.GetString(q.Assistance);
                }
            }
        }

        ASPxPageControl1.Visible = true;
        ASPxHtmlEditor1.Visible = true;
        ASPxHtmlEditor2.Visible = true;
        btnSave.Visible = true;
        btnSaveClose.Visible = true;
        btnClose.Visible = true;
        ASPxLabel1.Text = "Loaded.";
    }
    protected void Save()
    {
        ASCIIEncoding encoding = new ASCIIEncoding();
        string Area = cbArea.SelectedItem.Value.ToString();
        string Question = cbQuestion.SelectedItem.Value.ToString();
        if (Area == "Supplier")
        {
            QuestionnaireMainRationaleService qmrService = new QuestionnaireMainRationaleService();
            QuestionnaireMainRationale q = qmrService.GetByQuestionNo(Question);
            q.Rationale = (byte[])encoding.GetBytes(ASPxHtmlEditor1.Html);
            q.Assistance = (byte[])encoding.GetBytes(ASPxHtmlEditor2.Html);
            if (q.EntityState != EntityState.Unchanged)
                qmrService.Save(q);
        }
        else if (Area == "Verification")
        {
            string[] _question = Question.Split('_');
            int SectionId = -1;
            string QuestionId = _question[1];
            int.TryParse(_question[0], out SectionId);

            if (SectionId == -1 || String.IsNullOrEmpty(QuestionId)) { throw new Exception("WTF"); };

            QuestionnaireVerificationRationaleService qvrService = new QuestionnaireVerificationRationaleService();
            QuestionnaireVerificationRationale q = qvrService.GetBySectionIdQuestionId(SectionId, QuestionId);
            q.Rationale = (byte[])encoding.GetBytes(ASPxHtmlEditor1.Html);
            q.Assistance = (byte[])encoding.GetBytes(ASPxHtmlEditor2.Html);
            if (q.EntityState != EntityState.Unchanged)
                qvrService.Save(q);
        }
        ASPxLabel1.Text = "Saved.";
    }
    protected void Close()
    {
        ASPxLabel1.Text = "Closed.";
        ASPxHtmlEditor1.Html = "";
        ASPxHtmlEditor2.Html = "";
        ASPxPageControl1.Visible = false;
        ASPxHtmlEditor1.Visible = false;
        ASPxHtmlEditor2.Visible = false;
        btnSave.Visible = false;
        btnSaveClose.Visible = false;
        btnClose.Visible = false;

        cbArea.Enabled = true;
        cbQuestion.Enabled = true;
        btnEdit.Enabled = true;

        cbQuestion.SelectedItem = null;
        cbQuestion.Text = "<Select Question>";
    }
}
