﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialContactService
    {
        QuestionnaireInitialContact Get(Expression<Func<QuestionnaireInitialContact, bool>> where, List<Expression<Func<QuestionnaireInitialContact, object>>> includes);
        List<QuestionnaireInitialContact> GetMany(int? top, Expression<Func<QuestionnaireInitialContact, bool>> where, Func<IQueryable<QuestionnaireInitialContact>, IOrderedQueryable<QuestionnaireInitialContact>> orderBy, List<Expression<Func<QuestionnaireInitialContact, object>>> includes);
        List<QuestionnaireInitialContact> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContact, bool>> where, Func<IQueryable<QuestionnaireInitialContact>, IOrderedQueryable<QuestionnaireInitialContact>> orderBy, List<Expression<Func<QuestionnaireInitialContact, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialContact, bool>> where, List<Expression<Func<QuestionnaireInitialContact, object>>> includes);
        void Insert(QuestionnaireInitialContact item, bool saveChanges = true);
        void Update(QuestionnaireInitialContact item, bool saveChanges = true);
        void Delete(QuestionnaireInitialContact item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialContactService : IQuestionnaireInitialContactService
    {
        private readonly IRepository<QuestionnaireInitialContact> _repository;

        public QuestionnaireInitialContactService(IRepository<QuestionnaireInitialContact> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialContact Get(Expression<Func<QuestionnaireInitialContact, bool>> where, List<Expression<Func<QuestionnaireInitialContact, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialContact> GetMany(int? top, Expression<Func<QuestionnaireInitialContact, bool>> where, Func<IQueryable<QuestionnaireInitialContact>, IOrderedQueryable<QuestionnaireInitialContact>> orderBy, List<Expression<Func<QuestionnaireInitialContact, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialContact> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContact, bool>> where, Func<IQueryable<QuestionnaireInitialContact>, IOrderedQueryable<QuestionnaireInitialContact>> orderBy, List<Expression<Func<QuestionnaireInitialContact, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialContact, bool>> where, List<Expression<Func<QuestionnaireInitialContact, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialContact item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContact");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialContact item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContact");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialContact item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContact");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
