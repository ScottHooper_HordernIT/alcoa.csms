﻿using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyContractorReviewService
    {
        CompanyContractorReview Get(Expression<Func<CompanyContractorReview, bool>> where, List<Expression<Func<CompanyContractorReview, object>>> includes);
        List<CompanyContractorReview> GetMany(int? top, Expression<Func<CompanyContractorReview, bool>> where, Func<IQueryable<CompanyContractorReview>, IOrderedQueryable<CompanyContractorReview>> orderBy, List<Expression<Func<CompanyContractorReview, object>>> includes);
        List<CompanyContractorReview> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyContractorReview, bool>> where, Func<IQueryable<CompanyContractorReview>, IOrderedQueryable<CompanyContractorReview>> orderBy, List<Expression<Func<CompanyContractorReview, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyContractorReview, bool>> where, List<Expression<Func<CompanyContractorReview, object>>> includes);
        void Insert(CompanyContractorReview item, bool saveChanges = true);
        void Update(CompanyContractorReview item, bool saveChanges = true);
        void Delete(CompanyContractorReview item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyContractorReviewService : ICompanyContractorReviewService
    {
        private readonly IRepository<CompanyContractorReview> _repository;

        public CompanyContractorReviewService(IRepository<CompanyContractorReview> repository)
        {
            this._repository = repository;
        }

        public CompanyContractorReview Get(Expression<Func<CompanyContractorReview, bool>> where, List<Expression<Func<CompanyContractorReview, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyContractorReview> GetMany(int? top, Expression<Func<CompanyContractorReview, bool>> where, Func<IQueryable<CompanyContractorReview>, IOrderedQueryable<CompanyContractorReview>> orderBy, List<Expression<Func<CompanyContractorReview, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyContractorReview> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyContractorReview, bool>> where, Func<IQueryable<CompanyContractorReview>, IOrderedQueryable<CompanyContractorReview>> orderBy, List<Expression<Func<CompanyContractorReview, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyContractorReview, bool>> where, List<Expression<Func<CompanyContractorReview, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyContractorReview item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyContractorReview");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyContractorReview item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyContractorReview");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyContractorReview item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyContractorReview");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
