﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompaniesAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompaniesAudit, KaiZen.CSMS.Entities.CompaniesAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompaniesAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompaniesAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompaniesAudit index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompaniesAudit&gt;"/> class.</returns>
		public TList<CompaniesAudit> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(null,_companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesAudit index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompaniesAudit&gt;"/> class.</returns>
		public TList<CompaniesAudit> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompaniesAudit&gt;"/> class.</returns>
		public TList<CompaniesAudit> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompaniesAudit&gt;"/> class.</returns>
		public TList<CompaniesAudit> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesAudit index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompaniesAudit&gt;"/> class.</returns>
		public TList<CompaniesAudit> GetByCompanyId(System.Int32 _companyId, int start, int pageLength, out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompaniesAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompaniesAudit&gt;"/> class.</returns>
		public abstract TList<CompaniesAudit> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompaniesAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompaniesAudit&gt;"/></returns>
		public static TList<CompaniesAudit> Fill(IDataReader reader, TList<CompaniesAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompaniesAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompaniesAudit")
					.Append("|").Append((System.Int32)reader[((int)CompaniesAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompaniesAudit>(
					key.ToString(), // EntityTrackingKey
					"CompaniesAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompaniesAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)CompaniesAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)CompaniesAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)CompaniesAuditColumn.AuditEventId - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompaniesAuditColumn.CompanyId - 1)];
					c.CompanyName = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyName - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.CompanyName - 1)];
					c.CompanyNameEbi = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyNameEbi - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.CompanyNameEbi - 1)];
					c.CompanyAbn = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyAbn - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.CompanyAbn - 1)];
					c.PhoneNo = (reader.IsDBNull(((int)CompaniesAuditColumn.PhoneNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.PhoneNo - 1)];
					c.FaxNo = (reader.IsDBNull(((int)CompaniesAuditColumn.FaxNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.FaxNo - 1)];
					c.MobNo = (reader.IsDBNull(((int)CompaniesAuditColumn.MobNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.MobNo - 1)];
					c.AddressBusiness = (reader.IsDBNull(((int)CompaniesAuditColumn.AddressBusiness - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.AddressBusiness - 1)];
					c.AddressPostal = (reader.IsDBNull(((int)CompaniesAuditColumn.AddressPostal - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.AddressPostal - 1)];
					c.ProcurementSupplierNo = (reader.IsDBNull(((int)CompaniesAuditColumn.ProcurementSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.ProcurementSupplierNo - 1)];
					c.EhsConsultantId = (reader.IsDBNull(((int)CompaniesAuditColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.EhsConsultantId - 1)];
					c.ModifiedbyUserId = (reader.IsDBNull(((int)CompaniesAuditColumn.ModifiedbyUserId - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.ModifiedbyUserId - 1)];
					c.IprocSupplierNo = (reader.IsDBNull(((int)CompaniesAuditColumn.IprocSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.IprocSupplierNo - 1)];
					c.CompanyStatusId = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyStatusId - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.CompanyStatusId - 1)];
					c.StartDate = (reader.IsDBNull(((int)CompaniesAuditColumn.StartDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesAuditColumn.StartDate - 1)];
					c.EndDate = (reader.IsDBNull(((int)CompaniesAuditColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesAuditColumn.EndDate - 1)];
					c.CompanyStatus2Id = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyStatus2Id - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.CompanyStatus2Id - 1)];
					c.ContactsLastUpdated = (reader.IsDBNull(((int)CompaniesAuditColumn.ContactsLastUpdated - 1)))?null:(System.DateTime?)reader[((int)CompaniesAuditColumn.ContactsLastUpdated - 1)];
					c.Deactivated = (reader.IsDBNull(((int)CompaniesAuditColumn.Deactivated - 1)))?null:(System.Boolean?)reader[((int)CompaniesAuditColumn.Deactivated - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompaniesAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)CompaniesAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)CompaniesAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)CompaniesAuditColumn.AuditEventId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompaniesAuditColumn.CompanyId - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyName - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.CompanyName - 1)];
			entity.CompanyNameEbi = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyNameEbi - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.CompanyNameEbi - 1)];
			entity.CompanyAbn = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyAbn - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.CompanyAbn - 1)];
			entity.PhoneNo = (reader.IsDBNull(((int)CompaniesAuditColumn.PhoneNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.PhoneNo - 1)];
			entity.FaxNo = (reader.IsDBNull(((int)CompaniesAuditColumn.FaxNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.FaxNo - 1)];
			entity.MobNo = (reader.IsDBNull(((int)CompaniesAuditColumn.MobNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.MobNo - 1)];
			entity.AddressBusiness = (reader.IsDBNull(((int)CompaniesAuditColumn.AddressBusiness - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.AddressBusiness - 1)];
			entity.AddressPostal = (reader.IsDBNull(((int)CompaniesAuditColumn.AddressPostal - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.AddressPostal - 1)];
			entity.ProcurementSupplierNo = (reader.IsDBNull(((int)CompaniesAuditColumn.ProcurementSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.ProcurementSupplierNo - 1)];
			entity.EhsConsultantId = (reader.IsDBNull(((int)CompaniesAuditColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.EhsConsultantId - 1)];
			entity.ModifiedbyUserId = (reader.IsDBNull(((int)CompaniesAuditColumn.ModifiedbyUserId - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.ModifiedbyUserId - 1)];
			entity.IprocSupplierNo = (reader.IsDBNull(((int)CompaniesAuditColumn.IprocSupplierNo - 1)))?null:(System.String)reader[((int)CompaniesAuditColumn.IprocSupplierNo - 1)];
			entity.CompanyStatusId = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyStatusId - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.CompanyStatusId - 1)];
			entity.StartDate = (reader.IsDBNull(((int)CompaniesAuditColumn.StartDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesAuditColumn.StartDate - 1)];
			entity.EndDate = (reader.IsDBNull(((int)CompaniesAuditColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)CompaniesAuditColumn.EndDate - 1)];
			entity.CompanyStatus2Id = (reader.IsDBNull(((int)CompaniesAuditColumn.CompanyStatus2Id - 1)))?null:(System.Int32?)reader[((int)CompaniesAuditColumn.CompanyStatus2Id - 1)];
			entity.ContactsLastUpdated = (reader.IsDBNull(((int)CompaniesAuditColumn.ContactsLastUpdated - 1)))?null:(System.DateTime?)reader[((int)CompaniesAuditColumn.ContactsLastUpdated - 1)];
			entity.Deactivated = (reader.IsDBNull(((int)CompaniesAuditColumn.Deactivated - 1)))?null:(System.Boolean?)reader[((int)CompaniesAuditColumn.Deactivated - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompaniesAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.CompanyNameEbi = Convert.IsDBNull(dataRow["CompanyNameEbi"]) ? null : (System.String)dataRow["CompanyNameEbi"];
			entity.CompanyAbn = Convert.IsDBNull(dataRow["CompanyAbn"]) ? null : (System.String)dataRow["CompanyAbn"];
			entity.PhoneNo = Convert.IsDBNull(dataRow["PhoneNo"]) ? null : (System.String)dataRow["PhoneNo"];
			entity.FaxNo = Convert.IsDBNull(dataRow["FaxNo"]) ? null : (System.String)dataRow["FaxNo"];
			entity.MobNo = Convert.IsDBNull(dataRow["MobNo"]) ? null : (System.String)dataRow["MobNo"];
			entity.AddressBusiness = Convert.IsDBNull(dataRow["AddressBusiness"]) ? null : (System.String)dataRow["AddressBusiness"];
			entity.AddressPostal = Convert.IsDBNull(dataRow["AddressPostal"]) ? null : (System.String)dataRow["AddressPostal"];
			entity.ProcurementSupplierNo = Convert.IsDBNull(dataRow["ProcurementSupplierNo"]) ? null : (System.String)dataRow["ProcurementSupplierNo"];
			entity.EhsConsultantId = Convert.IsDBNull(dataRow["EHSConsultantId"]) ? null : (System.Int32?)dataRow["EHSConsultantId"];
			entity.ModifiedbyUserId = Convert.IsDBNull(dataRow["ModifiedbyUserId"]) ? null : (System.Int32?)dataRow["ModifiedbyUserId"];
			entity.IprocSupplierNo = Convert.IsDBNull(dataRow["IprocSupplierNo"]) ? null : (System.String)dataRow["IprocSupplierNo"];
			entity.CompanyStatusId = Convert.IsDBNull(dataRow["CompanyStatusId"]) ? null : (System.Int32?)dataRow["CompanyStatusId"];
			entity.StartDate = Convert.IsDBNull(dataRow["StartDate"]) ? null : (System.DateTime?)dataRow["StartDate"];
			entity.EndDate = Convert.IsDBNull(dataRow["EndDate"]) ? null : (System.DateTime?)dataRow["EndDate"];
			entity.CompanyStatus2Id = Convert.IsDBNull(dataRow["CompanyStatus2Id"]) ? null : (System.Int32?)dataRow["CompanyStatus2Id"];
			entity.ContactsLastUpdated = Convert.IsDBNull(dataRow["ContactsLastUpdated"]) ? null : (System.DateTime?)dataRow["ContactsLastUpdated"];
			entity.Deactivated = Convert.IsDBNull(dataRow["Deactivated"]) ? null : (System.Boolean?)dataRow["Deactivated"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompaniesAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompaniesAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompaniesAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompaniesAudit</c>
	///</summary>
	public enum CompaniesAuditChildEntityTypes
	{
	}
	
	#endregion CompaniesAuditChildEntityTypes
	
	#region CompaniesAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompaniesAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditFilterBuilder : SqlFilterBuilder<CompaniesAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditFilterBuilder class.
		/// </summary>
		public CompaniesAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesAuditFilterBuilder
	
	#region CompaniesAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompaniesAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesAuditParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditParameterBuilder class.
		/// </summary>
		public CompaniesAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesAuditParameterBuilder
	
	#region CompaniesAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompaniesAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesAuditSortBuilder : SqlSortBuilder<CompaniesAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesAuditSqlSortBuilder class.
		/// </summary>
		public CompaniesAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesAuditSortBuilder
	
} // end namespace
