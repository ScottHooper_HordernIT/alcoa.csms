﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiProjectListOpenProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(KpiProjectListOpenDataSourceDesigner))]
	public class KpiProjectListOpenDataSource : ReadOnlyDataSource<KpiProjectListOpen>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenDataSource class.
		/// </summary>
		public KpiProjectListOpenDataSource() : base(new KpiProjectListOpenService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiProjectListOpenDataSourceView used by the KpiProjectListOpenDataSource.
		/// </summary>
		protected KpiProjectListOpenDataSourceView KpiProjectListOpenView
		{
			get { return ( View as KpiProjectListOpenDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiProjectListOpenDataSourceView class that is to be
		/// used by the KpiProjectListOpenDataSource.
		/// </summary>
		/// <returns>An instance of the KpiProjectListOpenDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiProjectListOpen, Object> GetNewDataSourceView()
		{
			return new KpiProjectListOpenDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiProjectListOpenDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiProjectListOpenDataSourceView : ReadOnlyDataSourceView<KpiProjectListOpen>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiProjectListOpenDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiProjectListOpenDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiProjectListOpenDataSourceView(KpiProjectListOpenDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiProjectListOpenDataSource KpiProjectListOpenOwner
		{
			get { return Owner as KpiProjectListOpenDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiProjectListOpenService KpiProjectListOpenProvider
		{
			get { return Provider as KpiProjectListOpenService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region KpiProjectListOpenDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiProjectListOpenDataSource class.
	/// </summary>
	public class KpiProjectListOpenDataSourceDesigner : ReadOnlyDataSourceDesigner<KpiProjectListOpen>
	{
	}

	#endregion KpiProjectListOpenDataSourceDesigner

	#region KpiProjectListOpenFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListOpenFilter : SqlFilter<KpiProjectListOpenColumn>
	{
	}

	#endregion KpiProjectListOpenFilter

	#region KpiProjectListOpenExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiProjectListOpen"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiProjectListOpenExpressionBuilder : SqlExpressionBuilder<KpiProjectListOpenColumn>
	{
	}
	
	#endregion KpiProjectListOpenExpressionBuilder		
}

