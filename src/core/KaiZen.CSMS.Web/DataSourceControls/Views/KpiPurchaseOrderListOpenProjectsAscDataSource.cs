﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiPurchaseOrderListOpenProjectsAscProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(KpiPurchaseOrderListOpenProjectsAscDataSourceDesigner))]
	public class KpiPurchaseOrderListOpenProjectsAscDataSource : ReadOnlyDataSource<KpiPurchaseOrderListOpenProjectsAsc>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscDataSource class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsAscDataSource() : base(new KpiPurchaseOrderListOpenProjectsAscService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiPurchaseOrderListOpenProjectsAscDataSourceView used by the KpiPurchaseOrderListOpenProjectsAscDataSource.
		/// </summary>
		protected KpiPurchaseOrderListOpenProjectsAscDataSourceView KpiPurchaseOrderListOpenProjectsAscView
		{
			get { return ( View as KpiPurchaseOrderListOpenProjectsAscDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiPurchaseOrderListOpenProjectsAscDataSourceView class that is to be
		/// used by the KpiPurchaseOrderListOpenProjectsAscDataSource.
		/// </summary>
		/// <returns>An instance of the KpiPurchaseOrderListOpenProjectsAscDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiPurchaseOrderListOpenProjectsAsc, Object> GetNewDataSourceView()
		{
			return new KpiPurchaseOrderListOpenProjectsAscDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiPurchaseOrderListOpenProjectsAscDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiPurchaseOrderListOpenProjectsAscDataSourceView : ReadOnlyDataSourceView<KpiPurchaseOrderListOpenProjectsAsc>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsAscDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiPurchaseOrderListOpenProjectsAscDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiPurchaseOrderListOpenProjectsAscDataSourceView(KpiPurchaseOrderListOpenProjectsAscDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiPurchaseOrderListOpenProjectsAscDataSource KpiPurchaseOrderListOpenProjectsAscOwner
		{
			get { return Owner as KpiPurchaseOrderListOpenProjectsAscDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiPurchaseOrderListOpenProjectsAscService KpiPurchaseOrderListOpenProjectsAscProvider
		{
			get { return Provider as KpiPurchaseOrderListOpenProjectsAscService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region KpiPurchaseOrderListOpenProjectsAscDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiPurchaseOrderListOpenProjectsAscDataSource class.
	/// </summary>
	public class KpiPurchaseOrderListOpenProjectsAscDataSourceDesigner : ReadOnlyDataSourceDesigner<KpiPurchaseOrderListOpenProjectsAsc>
	{
	}

	#endregion KpiPurchaseOrderListOpenProjectsAscDataSourceDesigner

	#region KpiPurchaseOrderListOpenProjectsAscFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsAscFilter : SqlFilter<KpiPurchaseOrderListOpenProjectsAscColumn>
	{
	}

	#endregion KpiPurchaseOrderListOpenProjectsAscFilter

	#region KpiPurchaseOrderListOpenProjectsAscExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsAscExpressionBuilder : SqlExpressionBuilder<KpiPurchaseOrderListOpenProjectsAscColumn>
	{
	}
	
	#endregion KpiPurchaseOrderListOpenProjectsAscExpressionBuilder		
}

