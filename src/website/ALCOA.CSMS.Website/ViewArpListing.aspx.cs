﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.XtraPrinting;
using System.Collections.Generic;

using KaiZen.Library;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Reflection;
using DevExpress.Web.ASPxPopupControl;


namespace ALCOA.CSMS.Website
{
    public partial class ViewArpListing : System.Web.UI.Page
    {
        TList<Sites> sitesList;
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
        }


        private void moduleInit(bool postBack)
        {

            if (!postBack)
            {
                int _i = 0;

                cbRegionSite.Items.Clear();
                SitesFilters sitesFilters = new SitesFilters();
                sitesFilters.Append(SitesColumn.IsVisible, "1");
                sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                Session["SiteList"] = sitesList;
                foreach (Sites s in sitesList)
                {
                    cbRegionSite.Items.Add(s.SiteName, s.SiteId);
                }
                cbRegionSite.Items.Add("----------------", 0);

                RegionsFilters regionsFilters = new RegionsFilters();
                regionsFilters.Append(RegionsColumn.IsVisible, "1");
                TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                foreach (Regions r in regionsList)
                {
                    int NegRegionId = -1 * r.RegionId;

                    cbRegionSite.Items.Add(r.RegionName, NegRegionId);

                }
                int indexAus = cbRegionSite.Items.IndexOfText("Australia");

                cbRegionSite.SelectedIndex = indexAus;

                //ddlCompanies.DataSourceID = "dsCompanies";
                //ddlCompanies.TextField = "CompanyName";
                //ddlCompanies.ValueField = "CompanyId";
                //ddlCompanies.DataBind();

                //ddlCompanies.Items.Add(" < Select Company... >", -1);
                //ddlCompanies.SelectedIndex = -1;
                //ddlCompanies.Value = -1;
                //ddlCompanies.Text = " < Select Company... >";
                Session["session_siteId"] = -2;
                
            }

            int SiteId = Convert.ToInt32(Session["session_siteId"].ToString());

            //getArpByCompanySite(SiteId);
            DataSet dsARP = DataRepository.KpiProvider.ViewARPListing(SiteId);


            gridARP.DataSource = dsARP.Tables[0];
            gridARP.DataBind();
        }



        protected void cbRegionSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int CompanyId = Convert.ToInt32(ddlCompanies.SelectedItem.Value);
            int SiteId = Convert.ToInt32(cbRegionSite.SelectedItem.Value);
            Session["session_siteId"] = SiteId;
            getArpByCompanySite(SiteId);
        }

        public void getArpByCompanySite(int SiteId)
        {
            DataSet dsARP = DataRepository.KpiProvider.ViewARPListing( SiteId);


            gridARP.DataSource = dsARP.Tables[0];
            gridARP.DataBind();

            if (dsARP.Tables[0].Rows.Count > 0)
            {
            }
            else
            {
                PopupWindow pcWindow = new PopupWindow("No ARP’s were found.");
                pcWindow.FooterText = "";
                pcWindow.ShowOnPageLoad = true;
                pcWindow.Modal = true;
                ASPxPopupControl1.Windows.Add(pcWindow);

            }
        }
    }
}