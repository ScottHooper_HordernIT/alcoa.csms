﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailTemplateProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskEmailTemplateProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskEmailTemplate, KaiZen.CSMS.Entities.AdminTaskEmailTemplateKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateKey key)
		{
			return Delete(transactionManager, key.AdminTaskEmailTemplateId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskEmailTemplateId)
		{
			return Delete(null, _adminTaskEmailTemplateId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskEmailTemplate Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplateKey key, int start, int pageLength)
		{
			return GetByAdminTaskEmailTemplateId(transactionManager, key.AdminTaskEmailTemplateId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskEmail index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(null,_adminTaskEmailTemplateId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmail index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(null, _adminTaskEmailTemplateId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(transactionManager, _adminTaskEmailTemplateId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailTemplateId(transactionManager, _adminTaskEmailTemplateId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmail index.
		/// </summary>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskEmailTemplateId(System.Int32 _adminTaskEmailTemplateId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailTemplateId(null, _adminTaskEmailTemplateId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmail index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailTemplateId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskEmailTemplateId(TransactionManager transactionManager, System.Int32 _adminTaskEmailTemplateId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_AdminTaskEmailTemplate_AdminTaskTypeId index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId)
		{
			int count = -1;
			return GetByAdminTaskTypeId(null,_adminTaskTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplate_AdminTaskTypeId index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeId(null, _adminTaskTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplate_AdminTaskTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId)
		{
			int count = -1;
			return GetByAdminTaskTypeId(transactionManager, _adminTaskTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplate_AdminTaskTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskTypeId(transactionManager, _adminTaskTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplate_AdminTaskTypeId index.
		/// </summary>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskTypeId(System.Int32 _adminTaskTypeId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskTypeId(null, _adminTaskTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailTemplate_AdminTaskTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailTemplate GetByAdminTaskTypeId(TransactionManager transactionManager, System.Int32 _adminTaskTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskEmailTemplate&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskEmailTemplate&gt;"/></returns>
		public static TList<AdminTaskEmailTemplate> Fill(IDataReader reader, TList<AdminTaskEmailTemplate> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskEmailTemplate c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskEmailTemplate")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskEmailTemplateColumn.AdminTaskEmailTemplateId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskEmailTemplate>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskEmailTemplate",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskEmailTemplate();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateColumn.AdminTaskEmailTemplateId - 1)];
					c.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateColumn.AdminTaskTypeId - 1)];
					c.EmailSubject = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailSubject - 1)))?null:(System.String)reader[((int)AdminTaskEmailTemplateColumn.EmailSubject - 1)];
					c.EmailBodyAlcoaDirectExistingUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectExistingUser - 1)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectExistingUser - 1)];
					c.EmailBodyAlcoaLan = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaLan - 1)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaLan - 1)];
					c.EmailBodyAlcoaDirectNewUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectNewUser - 1)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectNewUser - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskEmailTemplate entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskEmailTemplateId = (System.Int32)reader[((int)AdminTaskEmailTemplateColumn.AdminTaskEmailTemplateId - 1)];
			entity.AdminTaskTypeId = (System.Int32)reader[((int)AdminTaskEmailTemplateColumn.AdminTaskTypeId - 1)];
			entity.EmailSubject = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailSubject - 1)))?null:(System.String)reader[((int)AdminTaskEmailTemplateColumn.EmailSubject - 1)];
			entity.EmailBodyAlcoaDirectExistingUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectExistingUser - 1)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectExistingUser - 1)];
			entity.EmailBodyAlcoaLan = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaLan - 1)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaLan - 1)];
			entity.EmailBodyAlcoaDirectNewUser = (reader.IsDBNull(((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectNewUser - 1)))?null:(System.Byte[])reader[((int)AdminTaskEmailTemplateColumn.EmailBodyAlcoaDirectNewUser - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskEmailTemplate entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailTemplateId = (System.Int32)dataRow["AdminTaskEmailTemplateId"];
			entity.AdminTaskTypeId = (System.Int32)dataRow["AdminTaskTypeId"];
			entity.EmailSubject = Convert.IsDBNull(dataRow["EmailSubject"]) ? null : (System.String)dataRow["EmailSubject"];
			entity.EmailBodyAlcoaDirectExistingUser = Convert.IsDBNull(dataRow["EmailBodyAlcoaDirectExistingUser"]) ? null : (System.Byte[])dataRow["EmailBodyAlcoaDirectExistingUser"];
			entity.EmailBodyAlcoaLan = Convert.IsDBNull(dataRow["EmailBodyAlcoaLan"]) ? null : (System.Byte[])dataRow["EmailBodyAlcoaLan"];
			entity.EmailBodyAlcoaDirectNewUser = Convert.IsDBNull(dataRow["EmailBodyAlcoaDirectNewUser"]) ? null : (System.Byte[])dataRow["EmailBodyAlcoaDirectNewUser"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailTemplate"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailTemplate Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplate entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AdminTaskTypeIdSource	
			if (CanDeepLoad(entity, "AdminTaskType|AdminTaskTypeIdSource", deepLoadType, innerList) 
				&& entity.AdminTaskTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AdminTaskTypeId;
				AdminTaskType tmpEntity = EntityManager.LocateEntity<AdminTaskType>(EntityLocator.ConstructKeyFromPkItems(typeof(AdminTaskType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AdminTaskTypeIdSource = tmpEntity;
				else
					entity.AdminTaskTypeIdSource = DataRepository.AdminTaskTypeProvider.GetByAdminTaskTypeId(transactionManager, entity.AdminTaskTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AdminTaskTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AdminTaskTypeProvider.DeepLoad(transactionManager, entity.AdminTaskTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AdminTaskTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdminTaskEmailTemplateId methods when available
			
			#region AdminTaskEmailTemplateAttachmentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailTemplateAttachment>|AdminTaskEmailTemplateAttachmentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateAttachmentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailTemplateAttachmentCollection = DataRepository.AdminTaskEmailTemplateAttachmentProvider.GetByAdminTaskEmailTemplateId(transactionManager, entity.AdminTaskEmailTemplateId);

				if (deep && entity.AdminTaskEmailTemplateAttachmentCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailTemplateAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailTemplateAttachment>) DataRepository.AdminTaskEmailTemplateAttachmentProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailTemplateAttachmentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdminTaskEmailTemplateRecipientCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailTemplateRecipient>|AdminTaskEmailTemplateRecipientCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateRecipientCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailTemplateRecipientCollection = DataRepository.AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailTemplateId(transactionManager, entity.AdminTaskEmailTemplateId);

				if (deep && entity.AdminTaskEmailTemplateRecipientCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailTemplateRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailTemplateRecipient>) DataRepository.AdminTaskEmailTemplateRecipientProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailTemplateRecipientCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskEmailTemplate object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskEmailTemplate instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailTemplate Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailTemplate entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AdminTaskTypeIdSource
			if (CanDeepSave(entity, "AdminTaskType|AdminTaskTypeIdSource", deepSaveType, innerList) 
				&& entity.AdminTaskTypeIdSource != null)
			{
				DataRepository.AdminTaskTypeProvider.Save(transactionManager, entity.AdminTaskTypeIdSource);
				entity.AdminTaskTypeId = entity.AdminTaskTypeIdSource.AdminTaskTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTaskEmailTemplateAttachment>
				if (CanDeepSave(entity.AdminTaskEmailTemplateAttachmentCollection, "List<AdminTaskEmailTemplateAttachment>|AdminTaskEmailTemplateAttachmentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailTemplateAttachment child in entity.AdminTaskEmailTemplateAttachmentCollection)
					{
						if(child.AdminTaskEmailTemplateIdSource != null)
						{
							child.AdminTaskEmailTemplateId = child.AdminTaskEmailTemplateIdSource.AdminTaskEmailTemplateId;
						}
						else
						{
							child.AdminTaskEmailTemplateId = entity.AdminTaskEmailTemplateId;
						}

					}

					if (entity.AdminTaskEmailTemplateAttachmentCollection.Count > 0 || entity.AdminTaskEmailTemplateAttachmentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailTemplateAttachmentProvider.Save(transactionManager, entity.AdminTaskEmailTemplateAttachmentCollection);
						
						deepHandles.Add("AdminTaskEmailTemplateAttachmentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailTemplateAttachment >) DataRepository.AdminTaskEmailTemplateAttachmentProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailTemplateAttachmentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdminTaskEmailTemplateRecipient>
				if (CanDeepSave(entity.AdminTaskEmailTemplateRecipientCollection, "List<AdminTaskEmailTemplateRecipient>|AdminTaskEmailTemplateRecipientCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailTemplateRecipient child in entity.AdminTaskEmailTemplateRecipientCollection)
					{
						if(child.AdminTaskEmailTemplateIdSource != null)
						{
							child.AdminTaskEmailTemplateId = child.AdminTaskEmailTemplateIdSource.AdminTaskEmailTemplateId;
						}
						else
						{
							child.AdminTaskEmailTemplateId = entity.AdminTaskEmailTemplateId;
						}

					}

					if (entity.AdminTaskEmailTemplateRecipientCollection.Count > 0 || entity.AdminTaskEmailTemplateRecipientCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailTemplateRecipientProvider.Save(transactionManager, entity.AdminTaskEmailTemplateRecipientCollection);
						
						deepHandles.Add("AdminTaskEmailTemplateRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailTemplateRecipient >) DataRepository.AdminTaskEmailTemplateRecipientProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailTemplateRecipientCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskEmailTemplateChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskEmailTemplate</c>
	///</summary>
	public enum AdminTaskEmailTemplateChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AdminTaskType</c> at AdminTaskTypeIdSource
		///</summary>
		[ChildEntityType(typeof(AdminTaskType))]
		AdminTaskType,
	
		///<summary>
		/// Collection of <c>AdminTaskEmailTemplate</c> as OneToMany for AdminTaskEmailTemplateAttachmentCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailTemplateAttachment>))]
		AdminTaskEmailTemplateAttachmentCollection,

		///<summary>
		/// Collection of <c>AdminTaskEmailTemplate</c> as OneToMany for AdminTaskEmailTemplateRecipientCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailTemplateRecipient>))]
		AdminTaskEmailTemplateRecipientCollection,
	}
	
	#endregion AdminTaskEmailTemplateChildEntityTypes
	
	#region AdminTaskEmailTemplateFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskEmailTemplateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateFilterBuilder : SqlFilterBuilder<AdminTaskEmailTemplateColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateFilterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateFilterBuilder
	
	#region AdminTaskEmailTemplateParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskEmailTemplateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailTemplateColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateParameterBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailTemplateParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailTemplateParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailTemplateParameterBuilder
	
	#region AdminTaskEmailTemplateSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskEmailTemplateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplate"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailTemplateSortBuilder : SqlSortBuilder<AdminTaskEmailTemplateColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailTemplateSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailTemplateSortBuilder
	
} // end namespace
