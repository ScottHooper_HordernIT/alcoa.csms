﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Appender;
using log4net.Core;
using System.Configuration;

namespace ALCOA.CSMS.Batch.Common.Log
{
    public class NoBufferSmtpAppenderBatch : MySmtpAppender
    {
        //Only warning and error really used here
        private Level _TriggerSendingLevel = Level.Warn;
        private string environment;



        public NoBufferSmtpAppenderBatch()
        {
            environment = String.Empty;
            try
            {
                environment = ConfigurationManager.AppSettings["Environment"];
            }
            catch
            {
                //Do nothing
            }
        }
        
        public Level TriggerSendingLevel
        {
            get
            {
                return _TriggerSendingLevel;
            }
            set
            {
                _TriggerSendingLevel = value;
            }
        }

        protected override void SendBuffer(LoggingEvent[] events)
        {
            //Only send an email when we are >= threshold
            if (prepareSubject(events))
            {
                base.SendBuffer(events);
            }
        }

        protected virtual bool prepareSubject(ICollection<LoggingEvent> events)
        {
            Subject = null;
            Level highestLevel = null;

            foreach (LoggingEvent evt in events)
            {
                if (evt.Level >= _TriggerSendingLevel)
                {
                    if (highestLevel == null)
                        highestLevel = evt.Level;
                    else
                    {
                        if (evt.Level > highestLevel)
                        {
                            highestLevel = evt.Level;
                        }
                    }
                }
            }

            if (highestLevel != null)
            {
                Log4NetService.TriggerLevel = highestLevel;
                Subject = String.Format("CSMS {0} ({1} Batch Jobs)", highestLevel, environment);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
