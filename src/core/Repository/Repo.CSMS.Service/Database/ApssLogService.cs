﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IApssLogService
    {
        ApssLog Get(Expression<Func<ApssLog, bool>> where, List<Expression<Func<ApssLog, object>>> includes);
        List<ApssLog> GetMany(int? top, Expression<Func<ApssLog, bool>> where, Func<IQueryable<ApssLog>, IOrderedQueryable<ApssLog>> orderBy, List<Expression<Func<ApssLog, object>>> includes);
        List<ApssLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ApssLog, bool>> where, Func<IQueryable<ApssLog>, IOrderedQueryable<ApssLog>> orderBy, List<Expression<Func<ApssLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ApssLog, bool>> where, List<Expression<Func<ApssLog, object>>> includes);
        void Insert(ApssLog item, bool saveChanges = true);
        void Update(ApssLog item, bool saveChanges = true);
        void Delete(ApssLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ApssLogService : IApssLogService
    {
        private readonly IRepository<ApssLog> _repository;

        public ApssLogService(IRepository<ApssLog> repository)
        {
            this._repository = repository;
        }

        public ApssLog Get(Expression<Func<ApssLog, bool>> where, List<Expression<Func<ApssLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ApssLog> GetMany(int? top, Expression<Func<ApssLog, bool>> where, Func<IQueryable<ApssLog>, IOrderedQueryable<ApssLog>> orderBy, List<Expression<Func<ApssLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ApssLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ApssLog, bool>> where, Func<IQueryable<ApssLog>, IOrderedQueryable<ApssLog>> orderBy, List<Expression<Func<ApssLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ApssLog, bool>> where, List<Expression<Func<ApssLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ApssLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ApssLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ApssLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ApssLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ApssLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ApssLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
