﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'KpiPurchaseOrderList_OpenProjects_Distinct_Asc' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class KpiPurchaseOrderListOpenProjectsDistinctAsc : KpiPurchaseOrderListOpenProjectsDistinctAscBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> instance.
		///</summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAsc():base(){}	
		
		#endregion
	}
}
