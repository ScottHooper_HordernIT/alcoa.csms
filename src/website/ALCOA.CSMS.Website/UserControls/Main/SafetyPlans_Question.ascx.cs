using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using System.Text;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxClasses.Internal;
using System.IO;
using DevExpress.Web.ASPxGridView;
using KaiZen.Library;
using DevExpress.Web.Data;



public partial class UserControls_SafetyPlans_Question : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    int qNo, rNo;
    bool ReadOnly = false;
    int modifiedBy = 0;
    DataTable dtAttachment = new DataTable("dtAttachment");
             

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {

        

        string isNew = string.Empty;
        if (Request.QueryString["R"] != null)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["R"]))
            {
                SessionHandler.spVar_SafetyPlans_ResponseId = Request.QueryString["R"];
                ReadOnly = true;
                btnNext.Visible = false;
                btnPrevious.Visible = false;
                //btnHome.PostBackUrl = "~/SafetyPlans_SelfAssessment.aspx?R=" + SessionHandler.spVar_SafetyPlans_ResponseId;
                btnHome.Text = "Save & Close";
                btnHome.Width = new Unit("120px");
            }
        }

        if (!postBack)
        {
            grid.Visible = false;
            Session["dtAttachment"] = null;
            dtAttachment.Columns.Add("AttachmentId", typeof(int));
            dtAttachment.Columns.Add("QuestionnaireId", typeof(int));
            dtAttachment.Columns.Add("SectionId", typeof(int));
            dtAttachment.Columns.Add("QuestionId", typeof(int));
            dtAttachment.Columns.Add("sQuestionId", typeof(string));
            dtAttachment.Columns.Add("sAnswerId", typeof(string));
            dtAttachment.Columns.Add("Type", typeof(int));
           
            //Load Navigation
            int i = Convert.ToInt32(Request.QueryString["Q"]);
            if (i == 1) btnPrevious.Enabled = false;
            else btnPrevious.Enabled = true;
            //if (i == 10) btnNext.Enabled = false;
            //else btnNext.Enabled = true;
            

            

            //Load Question
            qNo = Convert.ToInt32(Request.QueryString["Q"]);
            SafetyPlansSeQuestionsService ses = new SafetyPlansSeQuestionsService();
            SafetyPlansSeQuestions se = ses.GetByQuestionId(qNo);
            lblNo.Text = qNo.ToString();
            lblQuestion.Text = se.Question;
            lblText.Text = Encoding.ASCII.GetString(se.QuestionText);

            if (!String.IsNullOrEmpty(SessionHandler.spVar_SafetyPlans_ResponseId))
            {
                rNo = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);

                //Update or Save (New)
                SafetyPlansSeAnswersService seas = new SafetyPlansSeAnswersService();
                SafetyPlansSeAnswers sea = seas.GetByQuestionIdResponseId(qNo, rNo);

                if (sea!= null)
                {

                    DisplayAttachedFiles(Convert.ToInt32(sea.ResponseId), qNo);
                }
                
                if (sea == null)
                {
                    radioAnswer.SelectedValue = null;
                    ASPxHtmlEditor1.Html = "";
                    
                    grid.Visible = false;
                }
                else
                {
                    if (sea.Answer == false) radioAnswer.SelectedValue = "0";
                    else if (sea.Answer == true) radioAnswer.SelectedValue = "1";
                    ASPxHtmlEditor1.Html = Encoding.ASCII.GetString(sea.Comment);

                    if (sea.AssessorApproval != null && auth.RoleId == (int)RoleList.Contractor)
                    {
                        if (sea.AssessorApproval != false)
                        {
                            throw new Exception("Not Authorised.");
                        }
                    }
                    if (ReadOnly)
                    {
                        if (sea.AssessorApproval == true)
                        {
                            ASPxRoundPanel1.Visible = false;
                            ASPxRoundPanel2.Visible = false;
                            ASPxRoundPanel3.Visible = false;
                            btnHome.Visible = false;
                        }
                    }
                }
            }
            if (Request.QueryString["i"]!=null)
            {
                if(SessionHandler.spVar_SafetyPlans_ResponseId!=null && Convert.ToString(SessionHandler.spVar_SafetyPlans_ResponseId)!="")
                {
                  
                    rNo = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);
                }
                else{rNo=-1;}

                //Update or Save (New)
                SafetyPlansSeAnswersService seas = new SafetyPlansSeAnswersService();
                SafetyPlansSeAnswers sea = seas.GetByQuestionIdResponseId(qNo, rNo);


                isNew=Request.QueryString["i"];
                if (isNew == "1" && sea == null)
                {
                       QuestionnaireService quSer = new QuestionnaireService();

                        TList<Questionnaire> qu = quSer.GetByCompanyId(Convert.ToInt32(Convert.ToInt32(SessionHandler.spVar_CompanyId)));
                        //TList<Questionnaire> qu = quSer.GetByCompanyId(Convert.ToInt32(962));

                        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
                        QuestionnaireMainAssessment qma;

                        if(qu!=null && qu.Count>0)
                        {
                            int qUid = Convert.ToInt32(qu[0].QuestionnaireId);
                            grid.Visible = false;
                           // divItems.Style["Display"] = "block";
                           // divItems.Style[HtmlTextWriterStyle.Display] = "block";
                           // divItems.Visible = false;
                            QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
                             QuestionnaireMainAttachmentService qmas = new QuestionnaireMainAttachmentService();
                            if (qNo == 1)
                            {                  
                                
                                ASPxHtmlEditor1.Html=Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(qUid, 1, 1);

                                QuestionnaireVerificationAttachment qa = qas.GetByQuestionnaireIdSectionIdQuestionId(qUid, 1, 1);
                                if (qa != null)
                                {
                                    dtAttachment.Rows.Add(1, qUid, 1, 1, "", "", 1);
                                }
                                //Helper.Questionnaire.Supplier.Verification.LoadAttachment(qUid, 1, 1, f1e, f1l);
                                //f1e.Text = "";

                                //if (f1e.Visible)
                                //{
                                //    f1l.Visible = true;
                                //    f1e.Visible = true;
                                //    lnkBtn1.Visible = true;
                                //    dtAttachment.Rows.Add(1, qUid, 1, 1, "", "", 1);
                                //}
                                
                               
                            }

                            if (qNo == 4)
                            {
                                qma = qmaService.GetByQuestionnaireIdQuestionNo(qUid, "12");
                                string suplierAnswer = Helper.Questionnaire.Supplier.Main.LoadAnswer(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments"));
                                //string suplierAnswer = qma.AssessorComment;
                                string verificationComment = Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(qUid, 2, 1);
                                ASPxHtmlEditor1.Html = suplierAnswer + @"<br/>" + verificationComment;
                                //Helper.Questionnaire.Supplier.Verification.LoadAttachmentFromSupplier(qUid, "12", "AExample", f1e, f1l);

                                QuestionnaireMainAttachment qa = qmas.GetByQuestionnaireIdAnswerId(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "AExample"));
                                if (qa != null)
                                {
                                    dtAttachment.Rows.Add(1, qUid, 0, 0, "12", "AExample", 2);
                                }


                                //if (f1e.Visible)
                                //{
                                //    f1l.Visible = true;
                                //    f1e.Visible = true;
                                //    lnkBtn1.Visible = true;
                                //    dtAttachment.Rows.Add(1, qUid, 0, 0, "12", "AExample", 2);
                                //}
                                
                                
                            }
                            if (qNo == 5)
                            {
                                string strtext = string.Empty;
                               // qma = qmaService.GetByQuestionnaireIdQuestionNo(qUid, "17");
                              //  ASPxHtmlEditor1.Html = qma.AssessorComment;
                                //Helper.Questionnaire.Supplier.Verification.LoadAttachmentFromSupplier(qUid, "17", "BExample", f1e, f1l);
                                if (Helper.Questionnaire.Supplier.Main.LoadAnswer(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "A")) == "Yes")
                                {
                                    strtext =strtext + "Documentation of basic training exists in the form of internal certifications and records.";
                                }
                                else if(Helper.Questionnaire.Supplier.Main.LoadAnswer(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "A")) == "No")
                                {
                                    strtext = strtext + "No Documentation of basic training exists in the form of internal certifications and records";
                                }
                                if (Helper.Questionnaire.Supplier.Main.LoadAnswer(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "B")) == "Yes")
                                {
                                    strtext = strtext + @"<br/>"+ "Documentation of basic training exists in the form of internal External licenses and certificates issued by governments, professional organisations, trade associations or other recognised authority.";
                                }
                                else if (Helper.Questionnaire.Supplier.Main.LoadAnswer(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "B")) == "No")
                                {
                                    strtext = strtext + @"<br/>"+ "No Documentation of basic training exists in the form of External licenses and certificates issued by governments, professional organisations, trade associations or other recognised authority.";
                                }
                                ASPxHtmlEditor1.Html = strtext;
                                QuestionnaireMainAttachment qa = qmas.GetByQuestionnaireIdAnswerId(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "BExample"));
                                if (qa != null)
                                {

                                    dtAttachment.Rows.Add(1, qUid, 0, 0, "17", "BExample", 2);
                                }

                                //if (f1e.Visible)
                                //{
                                //    f1l.Visible = true;
                                //    f1e.Visible = true;
                                //    lnkBtn1.Visible = true;
                                   
                                //}
                               
                                
                            }
                            if (qNo == 8)
                            {

                                QuestionnaireMainAttachment qa = qmas.GetByQuestionnaireIdAnswerId(qUid, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Example"));
                                if (qa != null)
                                {
                                    dtAttachment.Rows.Add(1, qUid, 0, 0, "22", "Example", 2);
                                }



                                //Helper.Questionnaire.Supplier.Verification.LoadAttachmentFromSupplier(qUid, "22", "Example", f1e, f1l);
                                
                                //if (f1e.Visible)
                                //{
                                //    f1l.Visible = true;
                                //    f1e.Visible = true;
                                //    lnkBtn1.Visible = true;
                                //    dtAttachment.Rows.Add(1, qUid, 0, 0, "22", "Example", 2);
                                //}
                                
                            }
                            if (qNo == 9)
                            {
                                int Qnumber = 1;
                                string strComments = string.Empty;
                                while (Qnumber <= 4)
                                {
                                    if (strComments != "")
                                    { strComments = strComments + @"<br/>"; }
                                    strComments = strComments + Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(qUid, 6, Qnumber);
                                    Qnumber++;
                                }
                                ASPxHtmlEditor1.Html = strComments;
                                
                                QuestionnaireVerificationAttachment qa = qas.GetByQuestionnaireIdSectionIdQuestionId(qUid, 6, 1);
                                int iCount = 1;
                                if (qa != null)
                                {
                                    dtAttachment.Rows.Add(iCount, qUid, 6, 1, "", "", 1);
                                    iCount++;
                                }
                                qa = qas.GetByQuestionnaireIdSectionIdQuestionId(qUid, 6, 4);
                                if (qa != null)
                                {
                                    dtAttachment.Rows.Add(iCount, qUid, 6, 4, "", "", 1);
                                }
                                //Helper.Questionnaire.Supplier.Verification.LoadAttachment(qUid, 5, 1, f1e, f1l);
                                //Helper.Questionnaire.Supplier.Verification.LoadAttachment(qUid, 5, 4, f2e, f2l);
                                //if (f1e.Visible)
                                //{
                                //    f1l.Visible = true;
                                //    f1e.Visible = true;
                                //    lnkBtn1.Visible = true;
                                //}
                                //if (f2e.Visible)
                                //{
                                //    f2l.Visible = true;
                                //    f2e.Visible = true;
                                //    lnkBtn2.Visible = true;
                                //}

                            }
                            if (qNo == 10)
                            {
                                   int  Qnumber=1;
                                string strComments=string.Empty;
                                    while(Qnumber<=4)
                                {
                                    if (strComments != "")
                                    { strComments = strComments +@"<br/>"; }
                                    strComments =strComments + Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(qUid, 6, Qnumber);
                                    Qnumber++;
                                }
                                ASPxHtmlEditor1.Html=strComments;
                                QuestionnaireVerificationAttachment qa = qas.GetByQuestionnaireIdSectionIdQuestionId(qUid, 6, 3);
                                if (qa != null)
                                {
                                    dtAttachment.Rows.Add(1, qUid, 6, 3, "", "", 1);
                                }
                                 //Helper.Questionnaire.Supplier.Verification.LoadAttachment(qUid, 5, 3, f1e, f1l);
                                 //if (f1e.Visible)
                                 //{
                                 //    f1l.Visible = true;
                                 //    f1e.Visible = true;
                                 //    lnkBtn1.Visible = true;
                                 //}
                                 //if (f2e.Visible)
                                 //{
                                 //    f2l.Visible = true;
                                 //    f2e.Visible = true;
                                 //    lnkBtn2.Visible = true;
                                 //}
                            }
                            if (dtAttachment.Rows.Count > 0)
                            {
                                Session["dtAttachment"] = dtAttachment;
                                grid.DataSource = dtAttachment;
                                grid.DataBind();
                                grid.Visible = true;
                            }
                            else
                            {
                                grid.Visible = false;
                            }
                             
                           
                        }              

                }
                
            }

        }
        //lblNo.Focus();
        radioAnswer.Focus();
    }

    private void DisplayAttachedFiles(int reponseid, int questionId)
    {
        try
        {

            int count = 100;
            SafetyPlansSEResponsesAttachmentService ser = new SafetyPlansSEResponsesAttachmentService();
            TList<SafetyPlansSEResponsesAttachment> se = ser.GetByResponeIdQuestionId(reponseid, questionId, 0, 10, out count);
            grid.DataSource = se;
            grid.DataBind();
            if (se==null || se.Count == 0)
            {
                grid.Visible = false;
            }
            else 
            {
                grid.Visible = true;
            }
                      
        }
        catch (Exception ex)
        { 
            
        }
    }

    

    protected void SaveorUpdate()
    {
        try
        {
            if (!String.IsNullOrEmpty(radioAnswer.SelectedValue))
            {
                bool isInsertFromQuestionnaire = false;
                //Find QuestionNo
                qNo = Convert.ToInt32(Request.QueryString["Q"]);
                //Find ResponseId
                if (ReadOnly == true)
                {
                    rNo = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);
                }
                else
                {
                    
                    SafetyPlansSeResponsesService sers = new SafetyPlansSeResponsesService();

                    DataSet dsSer = sers.GetByCompanyId_Latest_NotSubmitted(Convert.ToInt32(SessionHandler.spVar_CompanyId));
                    if (dsSer.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSer.Tables[0].Rows)
                        {
                            SessionHandler.spVar_SafetyPlans_ResponseId = dr[0].ToString();
                        }
                        rNo = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);
                        //int qId = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);
                        isInsertFromQuestionnaire = false;
                    }
                    else
                    {
                        isInsertFromQuestionnaire = true;
                        //Create response!
                        using (SafetyPlansSeResponses ser = new SafetyPlansSeResponses())
                        {
                            ser.CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
                            ser.CreatedByUserId = auth.UserId;
                            ser.CreatedDate = DateTime.Now;
                            ser.ModifiedByUserId = auth.UserId;
                            ser.ModifiedDate = DateTime.Now;
                            ser.Year = DateTime.Now.Year;
                            ser.Submitted = false;
                            sers.Save(ser);
                            SessionHandler.spVar_SafetyPlans_ResponseId = ser.ResponseId.ToString();
                            int questionId = Convert.ToInt32(Request.QueryString["Q"]);
                          

                        }
                        rNo = Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId);
                    }
                }

                //Save or Update
                SafetyPlansSeAnswersService seas = new SafetyPlansSeAnswersService();
                SafetyPlansSeAnswers sea = new SafetyPlansSeAnswers();
                sea = seas.GetByQuestionIdResponseId(Convert.ToInt32(Request.QueryString["Q"]),
                                                    Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId));
                if (sea == null)
                {
                    isInsertFromQuestionnaire = true;
                    sea = new SafetyPlansSeAnswers();
                    sea.ResponseId = rNo;
                    sea.QuestionId = qNo;

                    if (radioAnswer.SelectedValue == "1") sea.Answer = true;
                    if (radioAnswer.SelectedValue == "0") sea.Answer = false;
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    sea.Comment = (byte[])encoding.GetBytes(ASPxHtmlEditor1.Html);
                }
                else
                {
                    
                    //data exists. lets see if there's any changes.
                    if (sea.Answer && radioAnswer.SelectedValue == "1")
                    {
                        //Same
                    }
                    else
                    {
                        if (radioAnswer.SelectedValue == "1") sea.Answer = true;
                    }

                    if (!sea.Answer && radioAnswer.SelectedValue == "0")
                    {
                        //Same
                    }
                    else
                    {
                        if (radioAnswer.SelectedValue == "0") sea.Answer = false;
                    }
                    string Comment = "";
                    if (sea.Comment != null)
                    {
                        Comment = Encoding.ASCII.GetString(sea.Comment);
                    }
                    if (Comment != ASPxHtmlEditor1.Html)
                    {
                        ASCIIEncoding encoding = new ASCIIEncoding();
                        sea.Comment = (byte[])encoding.GetBytes(ASPxHtmlEditor1.Html);
                    }
                }

                if (sea.EntityState != EntityState.Unchanged)
                {
                    seas.Save(sea);
                    int questionId = Convert.ToInt32(Request.QueryString["Q"]);
                    //Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSaftyPlanSEResponseAttachment(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId, f1, auth.UserId);
                }
                sea.Dispose();
                if (f1.UploadedFiles.Length > 0 && f1.UploadedFiles[0].FileName!="")
                {
                    int questionId = Convert.ToInt32(Request.QueryString["Q"]);
                   Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSaftyPlanSEResponseAttachment(Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId, f1, auth.UserId);
                }
                if (Request.QueryString["i"] != null && isInsertFromQuestionnaire)
                {
                    
                   
                    if (Convert.ToInt32(Request.QueryString["i"]) == 1)
                    {

                        if (grid.Visible==true)
                        {
                            int questionId = Convert.ToInt32(Request.QueryString["Q"]);


                            //QuestionnaireService quSer = new QuestionnaireService();
                            //TList<Questionnaire> qu = quSer.GetByCompanyId(Convert.ToInt32(SessionHandler.spVar_CompanyId));
                           // TList<Questionnaire> qu = quSer.GetByCompanyId(Convert.ToInt32(962));



                            //if (qu != null && qu.Count > 0)
                            //{
                                //int qUid = Convert.ToInt32(qu[0].QuestionnaireId);
                                //grid.Visible = false;
                                //divItems.Style["Display"] = "block";
                                bool isSuccessed = false;

                                DataTable dt = (DataTable)Session["dtAttachment"];
                                if (dt != null)
                                {
                                    if (dt.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            int type = Convert.ToInt32(dt.Rows[i]["Type"]);
                                            int questionnaireId = Convert.ToInt32(dt.Rows[i]["QuestionnaireId"]);
                                            int dquestionId = Convert.ToInt32(dt.Rows[i]["QuestionId"]);
                                            int sectionId = Convert.ToInt32(dt.Rows[i]["SectionId"]);
                                            string sQuestionId = Convert.ToString(dt.Rows[i]["sQuestionId"]);
                                            string sAnswerId = Convert.ToString(dt.Rows[i]["sAnswerId"]);
                                            if (sectionId > 0)
                                            {
                                                isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromVerification(questionnaireId, sectionId, dquestionId, auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId); 
                                            }
                                            else if (sAnswerId != "")
                                            {
                                                isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromSupplier(questionnaireId, sQuestionId, sAnswerId, auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);
                                            }
                                        }
                                    }
                                }

                                //if (qNo == 1)
                                //{
                                //    if (f1l.Visible == true)
                                //    {
                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromVerification(qUid, 1, 1, auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);
                                //    }
                                //}

                                //if (qNo == 4)
                                //{
                                //    if (f1l.Visible == true)
                                //    {

                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromSupplier(qUid, "12", "AExample", auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);
                                //    }
                                //}
                                //if (qNo == 5)
                                //{
                                //    if (f1l.Visible == true)
                                //    {

                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromSupplier(qUid, "17", "BExample", auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);

                                //    }
                                //}
                                //if (qNo == 8)
                                //{
                                //    if (f1l.Visible == true)
                                //    {
                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromSupplier(qUid, "22", "Example", auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);
                                //    }
                                //}
                                //if (qNo == 9)
                                //{
                                //    if (f1l.Visible == true)
                                //    {
                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromVerification(qUid, 5, 1, auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);

                                //    }
                                //    if (f2l.Visible == true)
                                //    {
                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromVerification(qUid, 5, 4, auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);
                                //    }
                                //}
                                //if (qNo == 10)
                                //{
                                //    if (f1l.Visible == true)
                                //    {
                                //        isSuccessed = Helper.SaftyPlanSeResponseAttachment.ActionLog.SaveSafetyPlanSeResAttachmentFromVerification(qUid, 5, 3, auth.UserId, Convert.ToInt32(SessionHandler.spVar_SafetyPlans_ResponseId), questionId);
                                //    }
                                //}
                                

                            //}
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write(ex.Message);
        }
    

    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        if (ValidateFiles() == true)
        {
            //System.Threading.Thread.Sleep(1000);
            SaveorUpdate();
            int i = Convert.ToInt32(Request.QueryString["Q"]);

            string url;
            if (ReadOnly)
                url = String.Format("{0}&R={1}", "~/SafetyPlans_Question.aspx?Q=" + (i - 1), Request.QueryString["R"]);
            else
            {
                if (Request.QueryString["R"] != null)
                {
                    url = "~/SafetyPlans_Question.aspx?Q=" + (i - 1);
                }
                else
                {
                    url = String.Format("{0}&i={1}", "~/SafetyPlans_Question.aspx?Q=" + (i - 1), 1);
                }
            }

            Response.Redirect(url, false);
        }
        else
        {
            ASPxPopupControl2.ShowOnPageLoad = true;
        }
    }
    protected void btnHome_Click(object sender, EventArgs e)
    {
        if (ValidateFiles() == true)
        {
              //System.Threading.Thread.Sleep(1000);
            SaveorUpdate();
            string url = "~/SafetyPlans_SelfAssessment.aspx?";
            if (ReadOnly)
            {
                url = String.Format("{0}R={1}", url, Request.QueryString["R"]);
            }
            else
            {
                url = url + "&i=1";
            }
            Response.Redirect(url, false);
        }
        else
        {
            ASPxPopupControl2.ShowOnPageLoad = true;
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (ValidateFiles() == true)
        {
            //System.Threading.Thread.Sleep(1000);
            SaveorUpdate();
            int i = Convert.ToInt32(Request.QueryString["Q"]);

            string url;
            if (i == 10)
                url = "~/SafetyPlans_SelfAssessment.aspx?";
            else
                url = "~/SafetyPlans_Question.aspx?Q=" + (i + 1);

            if (ReadOnly) url = String.Format("{0}&R={1}", url, Request.QueryString["R"]);

            if (Request.QueryString["R"] == null)
            {
                url = url + "&i=1";
            }

            Response.Redirect(url, false);
        }
        else
        {
            ASPxPopupControl2.ShowOnPageLoad = true;
        }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        DataTable dt = (DataTable)Session["dtAttachment"];
        if (dt!=null)
        {
            if (dt.Rows.Count > 0)
            {
                if (e.RowType != GridViewRowType.Data) return;

                int attachmentId = (int)grid.GetRowValues(e.VisibleIndex, "AttachmentId");
                int type = Convert.ToInt32(dt.Rows[attachmentId - 1]["Type"]);
                int questionnaireId = Convert.ToInt32(dt.Rows[attachmentId - 1]["QuestionnaireId"]);
                int questionId = Convert.ToInt32(dt.Rows[attachmentId - 1]["QuestionId"]);
                int sectionId = Convert.ToInt32(dt.Rows[attachmentId - 1]["SectionId"]);
                string sQuestionId = Convert.ToString(dt.Rows[attachmentId - 1]["sQuestionId"]);
                string sAnswerId = Convert.ToString(dt.Rows[attachmentId - 1]["sAnswerId"]);

                HyperLink fileLink = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "FileLink") as HyperLink;

                if (type == 1)
                {

                    QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
                    QuestionnaireVerificationAttachment qa = qas.GetByQuestionnaireIdSectionIdQuestionId(questionnaireId, sectionId, questionId);
                    if (qa != null)
                    {

                        fileLink.Text = qa.FileName;
                        fileLink.NavigateUrl = String.Format("~/Common/GetFile.aspx{0}",
                                            QueryStringModule.Encrypt(String.Format("Type=SQ&SubType=Verification&File={0}&Seed={1}", qa.AttachmentId, StringUtilities.RandomKey())));
                        fileLink.Target = "_blank";
                    }

                }
                else if (type == 2)
                {
                    QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
                    QuestionnaireMainAttachment qa = qas.GetByQuestionnaireIdAnswerId(questionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId(sQuestionId, sAnswerId));
                    if (qa != null)
                    {

                        fileLink.Text = qa.FileName;
                        fileLink.NavigateUrl = String.Format("~/Common/GetFile.aspx{0}",
                                            QueryStringModule.Encrypt(String.Format("Type=SQ&SubType=Supplier&File={0}&Seed={1}", qa.AttachmentId, StringUtilities.RandomKey())));
                        fileLink.Target = "_blank";
                    }
                }
            }
            else {

                grid.Visible = false;
            }
        }
        else
        {

            if (e.RowType != GridViewRowType.Data) return;

            HyperLink fileLink = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "FileLink") as HyperLink;
            int attachmentId = (int)grid.GetRowValues(e.VisibleIndex, "AttachmentId");
            SafetyPlansSEResponsesAttachmentService ser = new SafetyPlansSEResponsesAttachmentService();
            SafetyPlansSEResponsesAttachment se = ser.GetByAttachmentId(attachmentId);
            if (se != null)
            {

                //Text.Visible = true;
                fileLink.Text = se.FileName;
                fileLink.NavigateUrl = String.Format("~/Common/GetFile.aspx{0}",
                                    QueryStringModule.Encrypt(String.Format("Type=SMP&File={0}&Seed={1}", se.AttachmentId, StringUtilities.RandomKey())));
                fileLink.Target = "_blank";

            }
        }
        
    }



    protected void grid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        try
        {
            DataTable dt = (DataTable)Session["dtAttachment"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    int attachmentId = Convert.ToInt32(e.Keys["AttachmentId"].ToString());
                    if (attachmentId == 1)
                    {
                        dt.Rows.RemoveAt(attachmentId - 1);
                    }
                    else if (attachmentId == 2)
                    {
                        dt.Rows.RemoveAt(attachmentId - 1);
                        dt.Rows[0]["AttachmentId"] = 1;
                    }
                    Session["dtAttachment"] = dt;
                    e.Cancel = true;
                    grid.CancelEdit();
                    grid.DataSource = dt;
                    grid.DataBind();
                    if (dt.Rows.Count == 0)
                    {
                        grid.Visible = false;
                    }
                }
            }
                else
                {
                    SafetyPlansSEResponsesAttachmentService ser = new SafetyPlansSEResponsesAttachmentService();
                    SafetyPlansSEResponsesAttachment se = ser.GetByAttachmentId(Convert.ToInt32(e.Keys["AttachmentId"].ToString()));
                    int responseId = Convert.ToInt32(se.ResponseId);
                    int questionId = Convert.ToInt32(se.QuestionId);
                    if (se != null)
                    {
                        ser.Delete(se);
                    }
                    else
                    {
                        throw new Exception("Error Deleting. Please Try Again and/or contact your System Administrator.");
                    }

                    e.Cancel = true;
                    grid.CancelEdit();
                    DisplayAttachedFiles(responseId, questionId);
                }
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected bool ValidateFiles()
    {
        bool valid = true;
        if (f1.Enabled)
        {
            if (f1.UploadedFiles != null && f1.UploadedFiles.Length > 0)
            {
                for (int i = 0; i < f1.UploadedFiles.Length; i++)
                {
                    if (f1.UploadedFiles[i].IsValid == false) valid = false;
                }
            }

        }
        return valid;

    }
    


}
