﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IRoleService
    {
        Role Get(Expression<Func<Role, bool>> where, List<Expression<Func<Role, object>>> includes);
        List<Role> GetMany(int? top, Expression<Func<Role, bool>> where, Func<IQueryable<Role>, IOrderedQueryable<Role>> orderBy, List<Expression<Func<Role, object>>> includes);
        List<Role> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Role, bool>> where, Func<IQueryable<Role>, IOrderedQueryable<Role>> orderBy, List<Expression<Func<Role, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Role, bool>> where, List<Expression<Func<Role, object>>> includes);
        void Insert(Role item, bool saveChanges = true);
        void Update(Role item, bool saveChanges = true);
        void Delete(Role item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class RoleService : IRoleService
    {
        private readonly IRepository<Role> _repository;

        public RoleService(IRepository<Role> repository)
        {
            this._repository = repository;
        }

        public Role Get(Expression<Func<Role, bool>> where, List<Expression<Func<Role, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Role> GetMany(int? top, Expression<Func<Role, bool>> where, Func<IQueryable<Role>, IOrderedQueryable<Role>> orderBy, List<Expression<Func<Role, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Role> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Role, bool>> where, Func<IQueryable<Role>, IOrderedQueryable<Role>> orderBy, List<Expression<Func<Role, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Role, bool>> where, List<Expression<Func<Role, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Role item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Role");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Role item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Role");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Role item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Role");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
