﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileVaultListHelpFilesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class FileVaultListHelpFilesProviderBaseCore : EntityViewProviderBase<FileVaultListHelpFiles>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;FileVaultListHelpFiles&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;FileVaultListHelpFiles&gt;"/></returns>
		protected static VList&lt;FileVaultListHelpFiles&gt; Fill(DataSet dataSet, VList<FileVaultListHelpFiles> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<FileVaultListHelpFiles>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;FileVaultListHelpFiles&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<FileVaultListHelpFiles>"/></returns>
		protected static VList&lt;FileVaultListHelpFiles&gt; Fill(DataTable dataTable, VList<FileVaultListHelpFiles> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					FileVaultListHelpFiles c = new FileVaultListHelpFiles();
					c.FileVaultId = (Convert.IsDBNull(row["FileVaultId"]))?(int)0:(System.Int32)row["FileVaultId"];
					c.FileName = (Convert.IsDBNull(row["FileName"]))?string.Empty:(System.String)row["FileName"];
					c.FileHash = (Convert.IsDBNull(row["FileHash"]))?new byte[] {}:(System.Byte[])row["FileHash"];
					c.ContentLength = (Convert.IsDBNull(row["ContentLength"]))?(int)0:(System.Int32)row["ContentLength"];
					c.ModifiedDate = (Convert.IsDBNull(row["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)row["ModifiedDate"];
					c.ModifiedByUserId = (Convert.IsDBNull(row["ModifiedByUserId"]))?(int)0:(System.Int32)row["ModifiedByUserId"];
					c.FileVaultCategoryId = (Convert.IsDBNull(row["FileVaultCategoryId"]))?(int)0:(System.Int32)row["FileVaultCategoryId"];
					c.CategoryDesc = (Convert.IsDBNull(row["CategoryDesc"]))?string.Empty:(System.String)row["CategoryDesc"];
					c.CategoryName = (Convert.IsDBNull(row["CategoryName"]))?string.Empty:(System.String)row["CategoryName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;FileVaultListHelpFiles&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;FileVaultListHelpFiles&gt;"/></returns>
		protected VList<FileVaultListHelpFiles> Fill(IDataReader reader, VList<FileVaultListHelpFiles> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					FileVaultListHelpFiles entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<FileVaultListHelpFiles>("FileVaultListHelpFiles",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new FileVaultListHelpFiles();
					}
					
					entity.SuppressEntityEvents = true;

					entity.FileVaultId = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.FileVaultId)];
					//entity.FileVaultId = (Convert.IsDBNull(reader["FileVaultId"]))?(int)0:(System.Int32)reader["FileVaultId"];
					entity.FileName = (System.String)reader[((int)FileVaultListHelpFilesColumn.FileName)];
					//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
					entity.FileHash = (System.Byte[])reader[((int)FileVaultListHelpFilesColumn.FileHash)];
					//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
					entity.ContentLength = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.ContentLength)];
					//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
					entity.ModifiedDate = (System.DateTime)reader[((int)FileVaultListHelpFilesColumn.ModifiedDate)];
					//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
					entity.ModifiedByUserId = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.ModifiedByUserId)];
					//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
					entity.FileVaultCategoryId = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.FileVaultCategoryId)];
					//entity.FileVaultCategoryId = (Convert.IsDBNull(reader["FileVaultCategoryId"]))?(int)0:(System.Int32)reader["FileVaultCategoryId"];
					entity.CategoryDesc = (System.String)reader[((int)FileVaultListHelpFilesColumn.CategoryDesc)];
					//entity.CategoryDesc = (Convert.IsDBNull(reader["CategoryDesc"]))?string.Empty:(System.String)reader["CategoryDesc"];
					entity.CategoryName = (System.String)reader[((int)FileVaultListHelpFilesColumn.CategoryName)];
					//entity.CategoryName = (Convert.IsDBNull(reader["CategoryName"]))?string.Empty:(System.String)reader["CategoryName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="FileVaultListHelpFiles"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="FileVaultListHelpFiles"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, FileVaultListHelpFiles entity)
		{
			reader.Read();
			entity.FileVaultId = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.FileVaultId)];
			//entity.FileVaultId = (Convert.IsDBNull(reader["FileVaultId"]))?(int)0:(System.Int32)reader["FileVaultId"];
			entity.FileName = (System.String)reader[((int)FileVaultListHelpFilesColumn.FileName)];
			//entity.FileName = (Convert.IsDBNull(reader["FileName"]))?string.Empty:(System.String)reader["FileName"];
			entity.FileHash = (System.Byte[])reader[((int)FileVaultListHelpFilesColumn.FileHash)];
			//entity.FileHash = (Convert.IsDBNull(reader["FileHash"]))?new byte[] {}:(System.Byte[])reader["FileHash"];
			entity.ContentLength = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.ContentLength)];
			//entity.ContentLength = (Convert.IsDBNull(reader["ContentLength"]))?(int)0:(System.Int32)reader["ContentLength"];
			entity.ModifiedDate = (System.DateTime)reader[((int)FileVaultListHelpFilesColumn.ModifiedDate)];
			//entity.ModifiedDate = (Convert.IsDBNull(reader["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)reader["ModifiedDate"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.ModifiedByUserId)];
			//entity.ModifiedByUserId = (Convert.IsDBNull(reader["ModifiedByUserId"]))?(int)0:(System.Int32)reader["ModifiedByUserId"];
			entity.FileVaultCategoryId = (System.Int32)reader[((int)FileVaultListHelpFilesColumn.FileVaultCategoryId)];
			//entity.FileVaultCategoryId = (Convert.IsDBNull(reader["FileVaultCategoryId"]))?(int)0:(System.Int32)reader["FileVaultCategoryId"];
			entity.CategoryDesc = (System.String)reader[((int)FileVaultListHelpFilesColumn.CategoryDesc)];
			//entity.CategoryDesc = (Convert.IsDBNull(reader["CategoryDesc"]))?string.Empty:(System.String)reader["CategoryDesc"];
			entity.CategoryName = (System.String)reader[((int)FileVaultListHelpFilesColumn.CategoryName)];
			//entity.CategoryName = (Convert.IsDBNull(reader["CategoryName"]))?string.Empty:(System.String)reader["CategoryName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="FileVaultListHelpFiles"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="FileVaultListHelpFiles"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, FileVaultListHelpFiles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileVaultId = (Convert.IsDBNull(dataRow["FileVaultId"]))?(int)0:(System.Int32)dataRow["FileVaultId"];
			entity.FileName = (Convert.IsDBNull(dataRow["FileName"]))?string.Empty:(System.String)dataRow["FileName"];
			entity.FileHash = (Convert.IsDBNull(dataRow["FileHash"]))?new byte[] {}:(System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (Convert.IsDBNull(dataRow["ContentLength"]))?(int)0:(System.Int32)dataRow["ContentLength"];
			entity.ModifiedDate = (Convert.IsDBNull(dataRow["ModifiedDate"]))?DateTime.MinValue:(System.DateTime)dataRow["ModifiedDate"];
			entity.ModifiedByUserId = (Convert.IsDBNull(dataRow["ModifiedByUserId"]))?(int)0:(System.Int32)dataRow["ModifiedByUserId"];
			entity.FileVaultCategoryId = (Convert.IsDBNull(dataRow["FileVaultCategoryId"]))?(int)0:(System.Int32)dataRow["FileVaultCategoryId"];
			entity.CategoryDesc = (Convert.IsDBNull(dataRow["CategoryDesc"]))?string.Empty:(System.String)dataRow["CategoryDesc"];
			entity.CategoryName = (Convert.IsDBNull(dataRow["CategoryName"]))?string.Empty:(System.String)dataRow["CategoryName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region FileVaultListHelpFilesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListHelpFilesFilterBuilder : SqlFilterBuilder<FileVaultListHelpFilesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesFilterBuilder class.
		/// </summary>
		public FileVaultListHelpFilesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListHelpFilesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListHelpFilesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListHelpFilesFilterBuilder

	#region FileVaultListHelpFilesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListHelpFilesParameterBuilder : ParameterizedSqlFilterBuilder<FileVaultListHelpFilesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesParameterBuilder class.
		/// </summary>
		public FileVaultListHelpFilesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultListHelpFilesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultListHelpFilesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultListHelpFilesParameterBuilder
	
	#region FileVaultListHelpFilesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListHelpFiles"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileVaultListHelpFilesSortBuilder : SqlSortBuilder<FileVaultListHelpFilesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListHelpFilesSqlSortBuilder class.
		/// </summary>
		public FileVaultListHelpFilesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileVaultListHelpFilesSortBuilder

} // end namespace
