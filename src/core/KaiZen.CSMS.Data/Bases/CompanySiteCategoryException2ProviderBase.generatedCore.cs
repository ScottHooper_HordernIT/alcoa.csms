﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompanySiteCategoryException2ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompanySiteCategoryException2ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompanySiteCategoryException2, KaiZen.CSMS.Entities.CompanySiteCategoryException2Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryException2Key key)
		{
			return Delete(transactionManager, key.CompanySiteCategoryException2Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companySiteCategoryException2Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companySiteCategoryException2Id)
		{
			return Delete(null, _companySiteCategoryException2Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryException2Id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companySiteCategoryException2Id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Companies key.
		///		FK_CompanySiteCategoryException2_Companies Description: 
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(_companyId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Companies key.
		///		FK_CompanySiteCategoryException2_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryException2> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Companies key.
		///		FK_CompanySiteCategoryException2_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Companies key.
		///		fkCompanySiteCategoryException2Companies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanyId(null, _companyId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Companies key.
		///		fkCompanySiteCategoryException2Companies Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companyId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyId(System.Int32 _companyId, int start, int pageLength,out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Companies key.
		///		FK_CompanySiteCategoryException2_Companies Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public abstract TList<CompanySiteCategoryException2> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_CompanySiteCategory key.
		///		FK_CompanySiteCategoryException2_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(_companySiteCategoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_CompanySiteCategory key.
		///		FK_CompanySiteCategoryException2_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryException2> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_CompanySiteCategory key.
		///		FK_CompanySiteCategoryException2_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_CompanySiteCategory key.
		///		fkCompanySiteCategoryException2CompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_CompanySiteCategory key.
		///		fkCompanySiteCategoryException2CompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength,out int count)
		{
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_CompanySiteCategory key.
		///		FK_CompanySiteCategoryException2_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public abstract TList<CompanySiteCategoryException2> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Sites key.
		///		FK_CompanySiteCategoryException2_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Sites key.
		///		FK_CompanySiteCategoryException2_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		/// <remarks></remarks>
		public TList<CompanySiteCategoryException2> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Sites key.
		///		FK_CompanySiteCategoryException2_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Sites key.
		///		fkCompanySiteCategoryException2Sites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Sites key.
		///		fkCompanySiteCategoryException2Sites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public TList<CompanySiteCategoryException2> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompanySiteCategoryException2_Sites key.
		///		FK_CompanySiteCategoryException2_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompanySiteCategoryException2 objects.</returns>
		public abstract TList<CompanySiteCategoryException2> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompanySiteCategoryException2 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryException2Key key, int start, int pageLength)
		{
			return GetByCompanySiteCategoryException2Id(transactionManager, key.CompanySiteCategoryException2Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="_companySiteCategoryException2Id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanySiteCategoryException2Id(System.Int32 _companySiteCategoryException2Id)
		{
			int count = -1;
			return GetByCompanySiteCategoryException2Id(null,_companySiteCategoryException2Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="_companySiteCategoryException2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanySiteCategoryException2Id(System.Int32 _companySiteCategoryException2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryException2Id(null, _companySiteCategoryException2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryException2Id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanySiteCategoryException2Id(TransactionManager transactionManager, System.Int32 _companySiteCategoryException2Id)
		{
			int count = -1;
			return GetByCompanySiteCategoryException2Id(transactionManager, _companySiteCategoryException2Id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryException2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanySiteCategoryException2Id(TransactionManager transactionManager, System.Int32 _companySiteCategoryException2Id, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryException2Id(transactionManager, _companySiteCategoryException2Id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="_companySiteCategoryException2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanySiteCategoryException2Id(System.Int32 _companySiteCategoryException2Id, int start, int pageLength, out int count)
		{
			return GetByCompanySiteCategoryException2Id(null, _companySiteCategoryException2Id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryException2Id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanySiteCategoryException2Id(TransactionManager transactionManager, System.Int32 _companySiteCategoryException2Id, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtr"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanyIdSiteIdQtr(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtr(null,_companyId, _siteId, _qtr, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanyIdSiteIdQtr(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtr(null, _companyId, _siteId, _qtr, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtr"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanyIdSiteIdQtr(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtr(transactionManager, _companyId, _siteId, _qtr, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanyIdSiteIdQtr(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteIdQtr(transactionManager, _companyId, _siteId, _qtr, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanyIdSiteIdQtr(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteIdQtr(null, _companyId, _siteId, _qtr, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="_qtr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompanySiteCategoryException2 GetByCompanyIdSiteIdQtr(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _qtr, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_CompanySiteCategoryException2_1 index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryException2&gt;"/> class.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByCompanyIdSiteId(null,_companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2_1 index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryException2&gt;"/> class.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteId(null, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryException2&gt;"/> class.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId)
		{
			int count = -1;
			return GetByCompanyIdSiteId(transactionManager, _companyId, _siteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryException2&gt;"/> class.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyIdSiteId(transactionManager, _companyId, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2_1 index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryException2&gt;"/> class.</returns>
		public TList<CompanySiteCategoryException2> GetByCompanyIdSiteId(System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count)
		{
			return GetByCompanyIdSiteId(null, _companyId, _siteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_CompanySiteCategoryException2_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CompanySiteCategoryException2&gt;"/> class.</returns>
		public abstract TList<CompanySiteCategoryException2> GetByCompanyIdSiteId(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompanySiteCategoryException2&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompanySiteCategoryException2&gt;"/></returns>
		public static TList<CompanySiteCategoryException2> Fill(IDataReader reader, TList<CompanySiteCategoryException2> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompanySiteCategoryException2 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompanySiteCategoryException2")
					.Append("|").Append((System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanySiteCategoryException2Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompanySiteCategoryException2>(
					key.ToString(), // EntityTrackingKey
					"CompanySiteCategoryException2",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompanySiteCategoryException2();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompanySiteCategoryException2Id = (System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanySiteCategoryException2Id - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanyId - 1)];
					c.SiteId = (System.Int32)reader[((int)CompanySiteCategoryException2Column.SiteId - 1)];
					c.CompanySiteCategoryId = (System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanySiteCategoryId - 1)];
					c.Qtr = (System.Int32)reader[((int)CompanySiteCategoryException2Column.Qtr - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompanySiteCategoryException2 entity)
		{
			if (!reader.Read()) return;
			
			entity.CompanySiteCategoryException2Id = (System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanySiteCategoryException2Id - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanyId - 1)];
			entity.SiteId = (System.Int32)reader[((int)CompanySiteCategoryException2Column.SiteId - 1)];
			entity.CompanySiteCategoryId = (System.Int32)reader[((int)CompanySiteCategoryException2Column.CompanySiteCategoryId - 1)];
			entity.Qtr = (System.Int32)reader[((int)CompanySiteCategoryException2Column.Qtr - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompanySiteCategoryException2 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanySiteCategoryException2Id = (System.Int32)dataRow["CompanySiteCategoryException2Id"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.CompanySiteCategoryId = (System.Int32)dataRow["CompanySiteCategoryId"];
			entity.Qtr = (System.Int32)dataRow["Qtr"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompanySiteCategoryException2"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategoryException2 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryException2 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource

			#region CompanySiteCategoryIdSource	
			if (CanDeepLoad(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepLoadType, innerList) 
				&& entity.CompanySiteCategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanySiteCategoryId;
				CompanySiteCategory tmpEntity = EntityManager.LocateEntity<CompanySiteCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanySiteCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanySiteCategoryIdSource = tmpEntity;
				else
					entity.CompanySiteCategoryIdSource = DataRepository.CompanySiteCategoryProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanySiteCategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanySiteCategoryProvider.DeepLoad(transactionManager, entity.CompanySiteCategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanySiteCategoryIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompanySiteCategoryException2 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompanySiteCategoryException2 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompanySiteCategoryException2 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompanySiteCategoryException2 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			
			#region CompanySiteCategoryIdSource
			if (CanDeepSave(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepSaveType, innerList) 
				&& entity.CompanySiteCategoryIdSource != null)
			{
				DataRepository.CompanySiteCategoryProvider.Save(transactionManager, entity.CompanySiteCategoryIdSource);
				entity.CompanySiteCategoryId = entity.CompanySiteCategoryIdSource.CompanySiteCategoryId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompanySiteCategoryException2ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompanySiteCategoryException2</c>
	///</summary>
	public enum CompanySiteCategoryException2ChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>CompanySiteCategory</c> at CompanySiteCategoryIdSource
		///</summary>
		[ChildEntityType(typeof(CompanySiteCategory))]
		CompanySiteCategory,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion CompanySiteCategoryException2ChildEntityTypes
	
	#region CompanySiteCategoryException2FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompanySiteCategoryException2Column&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2FilterBuilder : SqlFilterBuilder<CompanySiteCategoryException2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2FilterBuilder class.
		/// </summary>
		public CompanySiteCategoryException2FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryException2FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryException2FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryException2FilterBuilder
	
	#region CompanySiteCategoryException2ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompanySiteCategoryException2Column&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanySiteCategoryException2ParameterBuilder : ParameterizedSqlFilterBuilder<CompanySiteCategoryException2Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2ParameterBuilder class.
		/// </summary>
		public CompanySiteCategoryException2ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompanySiteCategoryException2ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompanySiteCategoryException2ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompanySiteCategoryException2ParameterBuilder
	
	#region CompanySiteCategoryException2SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompanySiteCategoryException2Column&gt;"/> class
	/// that is used exclusively with a <see cref="CompanySiteCategoryException2"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompanySiteCategoryException2SortBuilder : SqlSortBuilder<CompanySiteCategoryException2Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanySiteCategoryException2SqlSortBuilder class.
		/// </summary>
		public CompanySiteCategoryException2SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompanySiteCategoryException2SortBuilder
	
} // end namespace
