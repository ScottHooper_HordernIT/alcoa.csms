﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.DocumentsDownloadLogProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(DocumentsDownloadLogDataSourceDesigner))]
	public class DocumentsDownloadLogDataSource : ProviderDataSource<DocumentsDownloadLog, DocumentsDownloadLogKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogDataSource class.
		/// </summary>
		public DocumentsDownloadLogDataSource() : base(new DocumentsDownloadLogService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the DocumentsDownloadLogDataSourceView used by the DocumentsDownloadLogDataSource.
		/// </summary>
		protected DocumentsDownloadLogDataSourceView DocumentsDownloadLogView
		{
			get { return ( View as DocumentsDownloadLogDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DocumentsDownloadLogDataSource control invokes to retrieve data.
		/// </summary>
		public DocumentsDownloadLogSelectMethod SelectMethod
		{
			get
			{
				DocumentsDownloadLogSelectMethod selectMethod = DocumentsDownloadLogSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (DocumentsDownloadLogSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the DocumentsDownloadLogDataSourceView class that is to be
		/// used by the DocumentsDownloadLogDataSource.
		/// </summary>
		/// <returns>An instance of the DocumentsDownloadLogDataSourceView class.</returns>
		protected override BaseDataSourceView<DocumentsDownloadLog, DocumentsDownloadLogKey> GetNewDataSourceView()
		{
			return new DocumentsDownloadLogDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the DocumentsDownloadLogDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class DocumentsDownloadLogDataSourceView : ProviderDataSourceView<DocumentsDownloadLog, DocumentsDownloadLogKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the DocumentsDownloadLogDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public DocumentsDownloadLogDataSourceView(DocumentsDownloadLogDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal DocumentsDownloadLogDataSource DocumentsDownloadLogOwner
		{
			get { return Owner as DocumentsDownloadLogDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal DocumentsDownloadLogSelectMethod SelectMethod
		{
			get { return DocumentsDownloadLogOwner.SelectMethod; }
			set { DocumentsDownloadLogOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal DocumentsDownloadLogService DocumentsDownloadLogProvider
		{
			get { return Provider as DocumentsDownloadLogService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<DocumentsDownloadLog> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<DocumentsDownloadLog> results = null;
			DocumentsDownloadLog item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32 _downloadedByUserId;

			switch ( SelectMethod )
			{
				case DocumentsDownloadLogSelectMethod.Get:
					DocumentsDownloadLogKey entityKey  = new DocumentsDownloadLogKey();
					entityKey.Load(values);
					item = DocumentsDownloadLogProvider.Get(entityKey);
					results = new TList<DocumentsDownloadLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case DocumentsDownloadLogSelectMethod.GetAll:
                    results = DocumentsDownloadLogProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case DocumentsDownloadLogSelectMethod.GetPaged:
					results = DocumentsDownloadLogProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case DocumentsDownloadLogSelectMethod.Find:
					if ( FilterParameters != null )
						results = DocumentsDownloadLogProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = DocumentsDownloadLogProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case DocumentsDownloadLogSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = DocumentsDownloadLogProvider.GetByAuditId(_auditId);
					results = new TList<DocumentsDownloadLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case DocumentsDownloadLogSelectMethod.GetByDownloadedByUserId:
					_downloadedByUserId = ( values["DownloadedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["DownloadedByUserId"], typeof(System.Int32)) : (int)0;
					results = DocumentsDownloadLogProvider.GetByDownloadedByUserId(_downloadedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == DocumentsDownloadLogSelectMethod.Get || SelectMethod == DocumentsDownloadLogSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				DocumentsDownloadLog entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					DocumentsDownloadLogProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<DocumentsDownloadLog> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			DocumentsDownloadLogProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region DocumentsDownloadLogDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the DocumentsDownloadLogDataSource class.
	/// </summary>
	public class DocumentsDownloadLogDataSourceDesigner : ProviderDataSourceDesigner<DocumentsDownloadLog, DocumentsDownloadLogKey>
	{
		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogDataSourceDesigner class.
		/// </summary>
		public DocumentsDownloadLogDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public DocumentsDownloadLogSelectMethod SelectMethod
		{
			get { return ((DocumentsDownloadLogDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new DocumentsDownloadLogDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region DocumentsDownloadLogDataSourceActionList

	/// <summary>
	/// Supports the DocumentsDownloadLogDataSourceDesigner class.
	/// </summary>
	internal class DocumentsDownloadLogDataSourceActionList : DesignerActionList
	{
		private DocumentsDownloadLogDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the DocumentsDownloadLogDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public DocumentsDownloadLogDataSourceActionList(DocumentsDownloadLogDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public DocumentsDownloadLogSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion DocumentsDownloadLogDataSourceActionList
	
	#endregion DocumentsDownloadLogDataSourceDesigner
	
	#region DocumentsDownloadLogSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the DocumentsDownloadLogDataSource.SelectMethod property.
	/// </summary>
	public enum DocumentsDownloadLogSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByDownloadedByUserId method.
		/// </summary>
		GetByDownloadedByUserId
	}
	
	#endregion DocumentsDownloadLogSelectMethod

	#region DocumentsDownloadLogFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogFilter : SqlFilter<DocumentsDownloadLogColumn>
	{
	}
	
	#endregion DocumentsDownloadLogFilter

	#region DocumentsDownloadLogExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogExpressionBuilder : SqlExpressionBuilder<DocumentsDownloadLogColumn>
	{
	}
	
	#endregion DocumentsDownloadLogExpressionBuilder	

	#region DocumentsDownloadLogProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;DocumentsDownloadLogChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="DocumentsDownloadLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsDownloadLogProperty : ChildEntityProperty<DocumentsDownloadLogChildEntityTypes>
	{
	}
	
	#endregion DocumentsDownloadLogProperty
}

