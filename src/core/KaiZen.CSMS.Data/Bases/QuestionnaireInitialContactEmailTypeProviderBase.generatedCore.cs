﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireInitialContactEmailTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireInitialContactEmailTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailTypeKey key)
		{
			return Delete(transactionManager, key.QuestionnaireInitialContactEmailTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireInitialContactEmailTypeId)
		{
			return Delete(null, _questionnaireInitialContactEmailTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailTypeKey key, int start, int pageLength)
		{
			return GetByQuestionnaireInitialContactEmailTypeId(transactionManager, key.QuestionnaireInitialContactEmailTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByQuestionnaireInitialContactEmailTypeId(System.Int32 _questionnaireInitialContactEmailTypeId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailTypeId(null,_questionnaireInitialContactEmailTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByQuestionnaireInitialContactEmailTypeId(System.Int32 _questionnaireInitialContactEmailTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailTypeId(null, _questionnaireInitialContactEmailTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByQuestionnaireInitialContactEmailTypeId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailTypeId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailTypeId(transactionManager, _questionnaireInitialContactEmailTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByQuestionnaireInitialContactEmailTypeId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactEmailTypeId(transactionManager, _questionnaireInitialContactEmailTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="_questionnaireInitialContactEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByQuestionnaireInitialContactEmailTypeId(System.Int32 _questionnaireInitialContactEmailTypeId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireInitialContactEmailTypeId(null, _questionnaireInitialContactEmailTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactEmailTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByQuestionnaireInitialContactEmailTypeId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactEmailTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="_typeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByTypeName(System.String _typeName)
		{
			int count = -1;
			return GetByTypeName(null,_typeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByTypeName(System.String _typeName, int start, int pageLength)
		{
			int count = -1;
			return GetByTypeName(null, _typeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByTypeName(TransactionManager transactionManager, System.String _typeName)
		{
			int count = -1;
			return GetByTypeName(transactionManager, _typeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByTypeName(TransactionManager transactionManager, System.String _typeName, int start, int pageLength)
		{
			int count = -1;
			return GetByTypeName(transactionManager, _typeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByTypeName(System.String _typeName, int start, int pageLength, out int count)
		{
			return GetByTypeName(null, _typeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactEmailType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_typeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType GetByTypeName(TransactionManager transactionManager, System.String _typeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireInitialContactEmailType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireInitialContactEmailType&gt;"/></returns>
		public static TList<QuestionnaireInitialContactEmailType> Fill(IDataReader reader, TList<QuestionnaireInitialContactEmailType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireInitialContactEmailType")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireInitialContactEmailTypeColumn.QuestionnaireInitialContactEmailTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireInitialContactEmailType>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireInitialContactEmailType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireInitialContactEmailTypeId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailTypeColumn.QuestionnaireInitialContactEmailTypeId - 1)];
					c.TypeName = (System.String)reader[((int)QuestionnaireInitialContactEmailTypeColumn.TypeName - 1)];
					c.TypeDesc = (System.String)reader[((int)QuestionnaireInitialContactEmailTypeColumn.TypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireInitialContactEmailTypeId = (System.Int32)reader[((int)QuestionnaireInitialContactEmailTypeColumn.QuestionnaireInitialContactEmailTypeId - 1)];
			entity.TypeName = (System.String)reader[((int)QuestionnaireInitialContactEmailTypeColumn.TypeName - 1)];
			entity.TypeDesc = (System.String)reader[((int)QuestionnaireInitialContactEmailTypeColumn.TypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireInitialContactEmailTypeId = (System.Int32)dataRow["QuestionnaireInitialContactEmailTypeId"];
			entity.TypeName = (System.String)dataRow["TypeName"];
			entity.TypeDesc = (System.String)dataRow["TypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireInitialContactEmailTypeId methods when available
			
			#region QuestionnaireInitialContactEmailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactEmailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactEmailCollection = DataRepository.QuestionnaireInitialContactEmailProvider.GetByEmailTypeId(transactionManager, entity.QuestionnaireInitialContactEmailTypeId);

				if (deep && entity.QuestionnaireInitialContactEmailCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContactEmail>) DataRepository.QuestionnaireInitialContactEmailProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireInitialContactEmail>
				if (CanDeepSave(entity.QuestionnaireInitialContactEmailCollection, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContactEmail child in entity.QuestionnaireInitialContactEmailCollection)
					{
						if(child.EmailTypeIdSource != null)
						{
							child.EmailTypeId = child.EmailTypeIdSource.QuestionnaireInitialContactEmailTypeId;
						}
						else
						{
							child.EmailTypeId = entity.QuestionnaireInitialContactEmailTypeId;
						}

					}

					if (entity.QuestionnaireInitialContactEmailCollection.Count > 0 || entity.QuestionnaireInitialContactEmailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactEmailProvider.Save(transactionManager, entity.QuestionnaireInitialContactEmailCollection);
						
						deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContactEmail >) DataRepository.QuestionnaireInitialContactEmailProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireInitialContactEmailTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireInitialContactEmailType</c>
	///</summary>
	public enum QuestionnaireInitialContactEmailTypeChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnaireInitialContactEmailType</c> as OneToMany for QuestionnaireInitialContactEmailCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContactEmail>))]
		QuestionnaireInitialContactEmailCollection,
	}
	
	#endregion QuestionnaireInitialContactEmailTypeChildEntityTypes
	
	#region QuestionnaireInitialContactEmailTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireInitialContactEmailTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeFilterBuilder : SqlFilterBuilder<QuestionnaireInitialContactEmailTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeFilterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailTypeFilterBuilder
	
	#region QuestionnaireInitialContactEmailTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireInitialContactEmailTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactEmailTypeParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireInitialContactEmailTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeParameterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactEmailTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactEmailTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactEmailTypeParameterBuilder
	
	#region QuestionnaireInitialContactEmailTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireInitialContactEmailTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactEmailType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireInitialContactEmailTypeSortBuilder : SqlSortBuilder<QuestionnaireInitialContactEmailTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactEmailTypeSqlSortBuilder class.
		/// </summary>
		public QuestionnaireInitialContactEmailTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireInitialContactEmailTypeSortBuilder
	
} // end namespace
