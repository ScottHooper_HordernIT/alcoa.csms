﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_KPI" Codebehind="KPI.ascx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td style="width: 900px; height: 13px; text-align: left">
            nb. If you are editing the "Time" field of the KPI, make sure you choose the 1st day
            of the month (i.e. 01/05/2007, 01/12/2007)</td>
    </tr>
    <tr>
        <td style="width: 900px; text-align: left">
    
<dxwgv:ASPxGridView
    ID="ASPxGridView1"
    runat="server"
    AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="KpiDataSource"
    OnInitNewRow="grid_InitNewRow"
    KeyFieldName="KpiId">
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0">
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
            <EditButton Visible="True">
            </EditButton>
            <DeleteButton Visible="True">
            </DeleteButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="KpiId" ReadOnly="True" Visible="False" VisibleIndex="1">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" Name="CompanyBox"
            VisibleIndex="1" GroupIndex="0" SortIndex="0" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                ValueField="CompanyId" ValueType="System.String" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" Name="SiteBox"
            VisibleIndex="2" GroupIndex="1" SortIndex="1" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                ValueField="SiteId" ValueType="System.String" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataDateColumn Caption="Time" FieldName="KpiDateTime" VisibleIndex="3" SortIndex="2" SortOrder="Descending">
            <PropertiesDateEdit>
                <CalendarProperties ShowTodayButton="False" ShowWeekNumbers="False">
                </CalendarProperties>
            </PropertiesDateEdit>
            <Settings FilterMode="DisplayText" GroupInterval="DateMonth" />
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataDateColumn Caption="Created Date" FieldName="DateAdded" ReadOnly="True" VisibleIndex="4">
        </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataComboBoxColumn Caption="By" FieldName="CreatedbyUserId"
            Name="CreatedByUserIdBox" VisibleIndex="5">
            <PropertiesComboBox DataSourceID="CreatedByDS" DropDownHeight="150px" TextField="UserFullNameLogon"
                ValueField="CreatedbyUserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataComboBoxColumn>

        
        <dxwgv:GridViewDataDateColumn Caption="Modified Date" FieldName="Datemodified" ReadOnly="True" VisibleIndex="6">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="By" FieldName="ModifiedbyUserId"
            Name="ModifiedByUserIdBox" VisibleIndex="7">
            <PropertiesComboBox DataSourceID="ModifiedByDS" DropDownHeight="150px" TextField="UserFullNameLogon"
                ValueField="ModifiedbyUserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            </dxwgv:GridViewDataComboBoxColumn>
    </Columns>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
            <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
            <SettingsEditing EditFormColumnCount="1" Mode="Inline" />
    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowGroupPanel="True" />
    <SettingsPager AlwaysShowPager="True" Mode="ShowPager" PageSize="20">
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
        </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr>
    </tr>
</table>
<br />
        
<data:KpiDataSource
    ID="KpiDataSource"
    runat="server"
	SelectMethod="GetPaged"
	EnablePaging="True"
	EnableSorting="True">
</data:KpiDataSource>

<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource> 

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<data:UsersFullNameDataSource ID="UsersFullNameDataSource" runat="server" Sort="UserFullNameLogon ASC"></data:UsersFullNameDataSource> 
<asp:SqlDataSource
	ID="CreatedByDS"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.CreatedByUserId,B.UserFullNameLogon from 
dbo.Kpi A LEFT OUTER JOIN dbo.UsersFullName B ON
A.CreatedByUserId=B.UserId where UserFullNameLogon Is Not Null Order By UserFullNameLogon">
</asp:SqlDataSource>
<asp:SqlDataSource
	ID="ModifiedByDS"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.ModifiedByUserId,B.UserFullNameLogon from 
dbo.Kpi A LEFT OUTER JOIN dbo.UsersFullName B ON
A.ModifiedByUserId=B.UserId where UserFullNameLogon Is Not Null Order By UserFullNameLogon">
</asp:SqlDataSource>
