USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_EBI_Yes_sqCurrent]    Script Date: 30/07/2015 4:28:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_EBI_Yes_sqCurrent]                                
(                                
 @SiteId int,                                
 @Month int,                                
 @Year int                                
)                                
AS                      
DECLARE @Companies VARCHAR(MAX)                  
IF (@SiteId > 0)                               
BEGIN             
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM     
  (SELECT DISTINCT F.CompanyName        
  FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  LEFT OUTER JOIN Kpi E           
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId         
  LEFT OUTER JOIN dbo.Companies F                    
  ON F.CompanyId = A.CompanyId        
  WHERE C.ActionName = 'ProcurementExpiring'          
  AND E.EbiOnSite = 1          
  AND D.CompanySiteCategoryId IS NOT NULL          
  AND D.Approved = 1          
  AND D.SiteId = @SiteId          
  AND YEAR(E.kpiDateTime) = @Year              
  AND MONTH(E.kpiDateTime) = @Month  
  AND YEAR(GETDATE()) = @Year          
  AND MONTH(GETDATE()) = @Month) X         
        
         
                  
END                        
ELSE                        
BEGIN     
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
  (SELECT DISTINCT F.CompanyName        
  FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN QuestionnaireReportExpiry B          
  ON A.QuestionnaireId = B.QuestionnaireId           
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON B.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  LEFT OUTER JOIN Kpi E           
  ON E.CompanyId = A.CompanyId AND E.SiteId = D.SiteId         
  LEFT OUTER JOIN dbo.Companies F                    
  ON F.CompanyId = A.CompanyId         
  WHERE C.ActionName = 'ProcurementExpiring'          
  AND E.EbiOnSite = 1          
  AND D.CompanySiteCategoryId IS NOT NULL          
  AND D.Approved = 1          
  AND ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = D.SiteId) > 0)          
  AND YEAR(E.kpiDateTime) = @Year              
  AND MONTH(E.kpiDateTime) = @Month  
  AND YEAR(GETDATE()) = @Year          
  AND MONTH(GETDATE()) = @Month) X          
            
END      
---------------------------------                  
         
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year         
 AND [Month] = @Month AND [ItemName] = 'Yes - SQ Current')        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]        
 SET [NonCompliantCompanies] = @Companies        
 WHERE [SiteId] = @SiteId        
   AND [Month] = @Month        
   AND [Year] = @Year        
   AND [ItemName] = 'Yes - SQ Current'        
ELSE        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]        
           ([SiteId]        
           ,[Month]        
           ,[Year]        
           ,[ItemName]        
           ,[NonCompliantCompanies])        
     VALUES        
           (@SiteId        
           ,@Month        
           ,@Year        
           ,'Yes - SQ Current'        
           ,@Companies)    
----------------------------------------

GO


