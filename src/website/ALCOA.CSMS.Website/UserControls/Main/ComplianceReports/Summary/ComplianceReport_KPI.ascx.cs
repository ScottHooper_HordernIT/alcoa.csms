﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;
using System.Collections.Generic;
using System.IO;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_KPI : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    int green = 0;
    int expected = 0;
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    TList<Sites> sList;

    private void moduleInit(bool postBack)
    {
        switch (auth.RoleId)
        {
            case ((int)RoleList.Reader):
                break;
            case ((int)RoleList.Contractor):
                break;
            case ((int)RoleList.Administrator):
                break;
            default:
                // do something
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                break;
        }
        
        int i = 0;
        SitesFilters sFilters = new SitesFilters();
        sFilters.AppendIsNotNull(SitesColumn.SiteNameEbi);
        sFilters.Junction = "AND";
        sFilters.AppendNotEquals(SitesColumn.SiteNameEbi, "");
        sList = DataRepository.SitesProvider.GetPaged(sFilters.ToString(), null, 0, 999999, out i);

        if (SessionHandler.spVar_Page == "ComplianceReport")
        {
            grid.Settings.ShowFilterRow = false;
            grid.Settings.ShowGroupPanel = false;
            grid.SettingsBehavior.AllowSort = false;
            grid.SettingsBehavior.AllowGroup = false;

            grid.DataSourceID = null;
            
            DataSet ds = new DataSet();
            if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
            {
                ds = DataRepository.KpiProvider.ComplianceReport_ByCompany(
                        Convert.ToInt32(SessionHandler.spVar_Year),
                        Convert.ToInt32(SessionHandler.spVar_CompanyId),
                        Convert.ToInt32(SessionHandler.spVar_SiteId),
                        SessionHandler.spVar_CategoryId2);
                grid.Columns["Company Name"].Visible = false;
            }
            else
            {
                ds = DataRepository.KpiProvider.ComplianceReport(
                        Convert.ToInt32(SessionHandler.spVar_Year),
                        Convert.ToInt32(SessionHandler.spVar_SiteId),
                        SessionHandler.spVar_CategoryId2);
                grid.Columns["Company Name"].Visible = true;
            }



            grid.DataSource = ds.Tables[0];
			//Added By Bishwajit for Item#31
            Session["ComplianceReport_KPI"] = ds.Tables[0];
			//End Added By Bishwajit for Item#31
            grid.DataBind();

            ds.Dispose();

            if (green == expected)
            {
                int a = Convert.ToInt32(SessionHandler.spVar_ComplianceCount);
                a++;
                SessionHandler.spVar_ComplianceCount = a.ToString();
            }
        }
        else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
        {
            //grid.Settings.ShowFilterRow = false;
            //grid.Settings.ShowGroupPanel = false;
            //grid.SettingsBehavior.AllowSort = false;
            //grid.SettingsBehavior.AllowGroup = false;
            //grid.Settings.ShowHeaderFilterButton = true;
            //grid.Settings.ShowHeaderFilterBlankItems = true;

            grid.DataSourceID = null;

            DataSet ds = new DataSet();
            if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
            {
                ds = DataRepository.KpiProvider.ComplianceReport_BySite(
                        Convert.ToInt32(SessionHandler.spVar_Year),
                        Convert.ToInt32(SessionHandler.spVar_SiteId),
                        SessionHandler.spVar_CategoryId2);
                grid.Columns["Site Name"].Visible = false;
            }
            else //Region
            {
                ds = DataRepository.KpiProvider.ComplianceReport_ByRegion(
                        Convert.ToInt32(SessionHandler.spVar_Year),
                        Convert.ToInt32(SessionHandler.spVar_RegionId),
                        SessionHandler.spVar_CategoryId2);
                grid.Columns["Site Name"].Visible = true;
            }

            grid.DataSource = ds.Tables[0];
            grid.DataBind();

            ds.Dispose();
        }
    }
    
    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        int month;
        string selectedYear = SessionHandler.spVar_Year;

        var monthList = (List<Repo.CSMS.DAL.EntityModels.Month>)Session["monthList"];
        if (monthList == null)
        {
            //Get all months
            var monthService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IMonthService>();
            monthList = monthService.GetMany(null, null, null, null);
            Session["monthList"] = monthList;
        }

        if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        if (selectedYear == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
            expected = month;
        }
        else
        {
            month = 12;
        }

        if (e.RowType != GridViewRowType.Data) return;

        Label lblSiteName = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblSiteName") as Label;
        if (lblSiteName != null)
        {
            lblSiteName.Text = (string)grid.GetRowValues(e.VisibleIndex, "SiteName");
            int i = 0;

            if (sList == null)
            {
                SitesFilters sFilters = new SitesFilters();
                sFilters.AppendIsNotNull(SitesColumn.SiteNameEbi);
                sFilters.Junction = "AND";
                sFilters.AppendNotEquals(SitesColumn.SiteNameEbi, "");
                sList = DataRepository.SitesProvider.GetPaged(sFilters.ToString(), null, 0, 999999, out i);
            }

            foreach (Sites s in sList)
            {
                if (lblSiteName.Text == s.SiteName)
                {
                    i++;
                }
            }
            if (i == 0) lblSiteName.Text += "*";
        }

        //hide labels if nonembedded.
        int _embedded = (int)grid.GetRowValues(e.VisibleIndex, "CompanySiteCategoryId");
        bool embedded;
        if (_embedded == 1)
            embedded = true;
        else
            embedded = false;

        Label lblYear = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted13") as Label;
        int yearCount = (int)grid.GetRowValues(e.VisibleIndex, "Year");
        // DT # 3061 change

        if (lblYear != null) lblYear.Text = yearCount.ToString();
        bool KPIexpected = false;
        // DT # 3061 change 
        //if (yearCount > 0)
        //{
        //    lblYear.ForeColor = Color.Green;
        //    lblYear.Text = yearCount.ToString();
        //}
        //else
        //{
        //    if (embedded) lblYear.ForeColor = Color.Red;
        //    lblYear.Text = yearCount.ToString();
        //}

        int monthCheck = 1;
        while (monthCheck <= month)
        {
            var m = monthList.Where(i => i.MonthId == monthCheck).FirstOrDefault();
            string monthAbbrev = Capitalize(m.MonthAbbrev.ToLower());

            Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, String.Format("noKPISubmitted{0}", monthCheck)) as Label;
            int change = (int)grid.GetRowValues(e.VisibleIndex, monthAbbrev);

            if (label != null) label.Text = " "; //NON-ASCII
            System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, String.Format("changeImage{0}", monthCheck)) as System.Web.UI.WebControls.Image;

            if (change != 0)
            {
                if (change != -1)
                {
                    if (img != null) img.Visible = true;
                    if (label != null) label.ForeColor = Color.Green;
                    if (change == 2)
                    {
                        if (label != null) label.ForeColor = Color.Black;
                        if (label != null) label.Text = "*";
                    }
                    if (change == 3)
                    { if (img != null) img.ImageUrl = "~/Images/TrafficLights/tlYellow.gif"; }
                    else
                    {
                        if (img != null)  //Braces added by AShley Goldstraw to fix green ticks.  Wasn't getting into the if statement to show green tick.  DT426.  Ashley Goldstraw 7/12/2015
                        {
                            img.ImageUrl = "~/Images/greentick.gif";
                        }
                    }
                }
                else
                {
                    if (img != null) img.Visible = false;
                }
            }
            else
            {
                if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                if (label != null) label.ForeColor = Color.Red;
                // DT # 3061 change 
                //if (!embedded)
                //    img.Visible = false;
                //else
                if (img != null) img.Visible = true;
                KPIexpected = true;
            }

            monthCheck++;
        }

        // DT # 3061 change 
        if (KPIexpected)
        {
            if (lblYear != null) lblYear.ForeColor = Color.Red;
        }
        else
        {
            if (lblYear != null) lblYear.ForeColor = Color.Green;
        }
    }


    public static string Capitalize(string value)
    {
        return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value);

    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1)}";
        }
        if (e.Column.FieldName == "SiteId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }

    public int getExpected()
    {
        if (green >= expected)
        { return 1; }
        else { return 0; }
    }

    public PrintableComponentLink ExportToPdf()
    {
        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
        pcl.Component = gridExport;
        pcl.CreateReportHeaderArea += new CreateAreaEventHandler(pcl_CreateReportHeaderArea);
        pcl.CreateDocument(false);
        
        return pcl;
    }

    public void pcl_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
    {
        TextBrick brick1 = e.Graph.DrawString("KPI", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }

    public string ExportToHtml()
    {
        var sb = new StringBuilder();

        sb.Append("<table width='100%' border='1' style='font-size:9px; color:Black;'>");
        sb.Append("<tbody>");
        sb.Append("<tr>");
        sb.Append("<td width='15%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Residential Category</td>");
        sb.Append("<td width='20%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Company Name</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Jan</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Feb</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Mar</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Apr</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>May</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Jun</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Jul</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Aug</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Sep</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Oct</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Nov</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Dec</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Year</td>");
        sb.Append("</tr>");

        var dt = (DataTable)grid.DataSource;
        if (dt != null && dt.Rows.Count > 0)
        {
            dt.DefaultView.Sort = "CategoryDesc asc, CompanyName asc";
            dt = dt.DefaultView.ToTable();

            foreach (DataRow dr in dt.Rows)
            {
                bool kpiExpected = false;

                sb.Append("<tr>");
                sb.Append(string.Format("<td width='15%' border='1' align='left' valign='top'>{0}</td>", dr["CategoryDesc"]));
                sb.Append(string.Format("<td width='20%' border='1' align='left' valign='top'>{0}</td>", dr["CompanyName"].ToString()));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Jan"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Feb"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Mar"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Apr"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["May"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Jun"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Jul"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Aug"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Sep"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Oct"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Nov"].ToString(), ref kpiExpected)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", MonthHtml(dr["Dec"].ToString(), ref kpiExpected)));

                if (kpiExpected)
                {
                    sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'><span style='color:#ff0000;'>{0}</span></td>", dr["Year"].ToString()));
                }
                else
                {
                    sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'><span style='color:#00ff00;'>{0}</span></td>", dr["Year"].ToString()));
                }

                sb.Append(string.Format("</tr>"));
            }
        }
        else
        {
            sb.Append("<tr>");
		    sb.Append("<td colspan='15'>No KPI(s) submitted</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");

        return sb.ToString();
    }

    private string MonthHtml(string month, ref bool kpiExpected)
    {
        string html = "";

        if (month != "0")
        {
            if (month != "-1")
            {
                if (month == "2")
                {
                    html = string.Format("<span style='color:#000000;'>*</span>", "");
                }
                else if (month == "3")
                    html = "<img width='16px' height='16px' src='[UrlPath]/images/trafficlights/tlYellow.gif' align='center' />";
                else
                    html = "<img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='center' />";  //Uncommented by Ashley Goldstraw DT553
            }
        }
        else
        {
            html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' />";
            kpiExpected = true;
        }

        return html;
    }
}
