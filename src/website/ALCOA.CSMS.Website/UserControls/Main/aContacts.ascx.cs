using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.XtraPrinting;

public partial class UserControls_aContacts : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            btnEdit.Visible = false;
            Configuration configuration = new Configuration();
            ASPxHyperLinkContact.NavigateUrl = String.Format("mailto:{0}", configuration.GetValue(ConfigList.ContactEmail));
            //grid.ExpandAll();
            SessionHandler.ViewMode = "View";
            
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    btnEdit.Visible = true;
                    break;
                default:
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Alcoa Contacts"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
    }

    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
    }
    protected void grid_RowValidating(object sender, ASPxDataValidationEventArgs e)
      {
          //if (e.NewValues["CompanyId"] == null) { e.Errors[grid.Columns["CompanyId"]] = "Value can't be null."; }
          //if (e.NewValues["ContactEmail"] == null) { e.Errors[grid.Columns["ContactEmail"]] = "Value can't be null."; }
          //if (e.NewValues["ContactFirstName"] == null) { e.Errors[grid.Columns["ContactFirstName"]] = "Value can't be null."; }
          //if (e.NewValues["ContactLastName"] == null) { e.Errors[grid.Columns["ContactLastName"]] = "Value can't be null."; }
          //if (e.NewValues["ContactTitlePosition"] == null) { e.Errors[grid.Columns["ContactTitlePosition"]] = "Value can't be null."; }
          //if (e.NewValues["ContactMobile"] == null) { e.Errors[grid.Columns["ContactMobile"]] = "Value can't be null."; }
          //if (e.NewValues["ContactPhone"] == null) { e.Errors[grid.Columns["ContactPhone"]] = "Value can't be null."; }

          //if (e.Errors.Count > 0) e.RowError = "Please, fill in all fields.";

          e.NewValues["ModifiedByuserId"] = auth.UserId;
     }
    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ModifiedByUserId"] = auth.UserId;
    }
    protected void grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        if (!grid.IsNewRowEditing) grid.DoRowValidation();
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (SessionHandler.ViewMode == "View")
        {
            grid.Columns[0].Visible = true;
            SessionHandler.ViewMode = "Edit";
            btnEdit.Text = "(View)";
        }
        else if (SessionHandler.ViewMode == "Edit")
        {
            grid.Columns[0].Visible = false;
            SessionHandler.ViewMode = "View";
            btnEdit.Text = "(New/Edit/Delete)";
        }
    }
}
