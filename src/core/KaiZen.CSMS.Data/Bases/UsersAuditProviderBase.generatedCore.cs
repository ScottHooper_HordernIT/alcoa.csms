﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UsersAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.UsersAudit, KaiZen.CSMS.Entities.UsersAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.UsersAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_UsersAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.UsersAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UsersAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UsersAudit&gt;"/></returns>
		public static TList<UsersAudit> Fill(IDataReader reader, TList<UsersAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.UsersAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UsersAudit")
					.Append("|").Append((System.Int32)reader[((int)UsersAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UsersAudit>(
					key.ToString(), // EntityTrackingKey
					"UsersAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.UsersAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)UsersAuditColumn.AuditId - 1)];
					c.UserId = (reader.IsDBNull(((int)UsersAuditColumn.UserId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.UserId - 1)];
					c.UserLogon = (reader.IsDBNull(((int)UsersAuditColumn.UserLogon - 1)))?null:(System.String)reader[((int)UsersAuditColumn.UserLogon - 1)];
					c.LastName = (reader.IsDBNull(((int)UsersAuditColumn.LastName - 1)))?null:(System.String)reader[((int)UsersAuditColumn.LastName - 1)];
					c.FirstName = (reader.IsDBNull(((int)UsersAuditColumn.FirstName - 1)))?null:(System.String)reader[((int)UsersAuditColumn.FirstName - 1)];
					c.CompanyId = (reader.IsDBNull(((int)UsersAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.CompanyId - 1)];
					c.Title = (reader.IsDBNull(((int)UsersAuditColumn.Title - 1)))?null:(System.String)reader[((int)UsersAuditColumn.Title - 1)];
					c.PhoneNo = (reader.IsDBNull(((int)UsersAuditColumn.PhoneNo - 1)))?null:(System.String)reader[((int)UsersAuditColumn.PhoneNo - 1)];
					c.FaxNo = (reader.IsDBNull(((int)UsersAuditColumn.FaxNo - 1)))?null:(System.String)reader[((int)UsersAuditColumn.FaxNo - 1)];
					c.MobNo = (reader.IsDBNull(((int)UsersAuditColumn.MobNo - 1)))?null:(System.String)reader[((int)UsersAuditColumn.MobNo - 1)];
					c.Email = (reader.IsDBNull(((int)UsersAuditColumn.Email - 1)))?null:(System.String)reader[((int)UsersAuditColumn.Email - 1)];
					c.RoleId = (reader.IsDBNull(((int)UsersAuditColumn.RoleId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.RoleId - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)UsersAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.ModifiedByUserId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)UsersAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)UsersAuditColumn.AuditEventId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.UsersAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)UsersAuditColumn.AuditId - 1)];
			entity.UserId = (reader.IsDBNull(((int)UsersAuditColumn.UserId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.UserId - 1)];
			entity.UserLogon = (reader.IsDBNull(((int)UsersAuditColumn.UserLogon - 1)))?null:(System.String)reader[((int)UsersAuditColumn.UserLogon - 1)];
			entity.LastName = (reader.IsDBNull(((int)UsersAuditColumn.LastName - 1)))?null:(System.String)reader[((int)UsersAuditColumn.LastName - 1)];
			entity.FirstName = (reader.IsDBNull(((int)UsersAuditColumn.FirstName - 1)))?null:(System.String)reader[((int)UsersAuditColumn.FirstName - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)UsersAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.CompanyId - 1)];
			entity.Title = (reader.IsDBNull(((int)UsersAuditColumn.Title - 1)))?null:(System.String)reader[((int)UsersAuditColumn.Title - 1)];
			entity.PhoneNo = (reader.IsDBNull(((int)UsersAuditColumn.PhoneNo - 1)))?null:(System.String)reader[((int)UsersAuditColumn.PhoneNo - 1)];
			entity.FaxNo = (reader.IsDBNull(((int)UsersAuditColumn.FaxNo - 1)))?null:(System.String)reader[((int)UsersAuditColumn.FaxNo - 1)];
			entity.MobNo = (reader.IsDBNull(((int)UsersAuditColumn.MobNo - 1)))?null:(System.String)reader[((int)UsersAuditColumn.MobNo - 1)];
			entity.Email = (reader.IsDBNull(((int)UsersAuditColumn.Email - 1)))?null:(System.String)reader[((int)UsersAuditColumn.Email - 1)];
			entity.RoleId = (reader.IsDBNull(((int)UsersAuditColumn.RoleId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.RoleId - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)UsersAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)UsersAuditColumn.ModifiedByUserId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)UsersAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)UsersAuditColumn.AuditEventId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UsersAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.UsersAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.UserId = Convert.IsDBNull(dataRow["UserId"]) ? null : (System.Int32?)dataRow["UserId"];
			entity.UserLogon = Convert.IsDBNull(dataRow["UserLogon"]) ? null : (System.String)dataRow["UserLogon"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.Title = Convert.IsDBNull(dataRow["Title"]) ? null : (System.String)dataRow["Title"];
			entity.PhoneNo = Convert.IsDBNull(dataRow["PhoneNo"]) ? null : (System.String)dataRow["PhoneNo"];
			entity.FaxNo = Convert.IsDBNull(dataRow["FaxNo"]) ? null : (System.String)dataRow["FaxNo"];
			entity.MobNo = Convert.IsDBNull(dataRow["MobNo"]) ? null : (System.String)dataRow["MobNo"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.RoleId = Convert.IsDBNull(dataRow["RoleId"]) ? null : (System.Int32?)dataRow["RoleId"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UsersAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.UsersAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.UsersAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UsersAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UsersAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.UsersAudit</c>
	///</summary>
	public enum UsersAuditChildEntityTypes
	{
	}
	
	#endregion UsersAuditChildEntityTypes
	
	#region UsersAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UsersAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditFilterBuilder : SqlFilterBuilder<UsersAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditFilterBuilder class.
		/// </summary>
		public UsersAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersAuditFilterBuilder
	
	#region UsersAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UsersAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAuditParameterBuilder : ParameterizedSqlFilterBuilder<UsersAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditParameterBuilder class.
		/// </summary>
		public UsersAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersAuditParameterBuilder
	
	#region UsersAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UsersAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersAuditSortBuilder : SqlSortBuilder<UsersAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAuditSqlSortBuilder class.
		/// </summary>
		public UsersAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersAuditSortBuilder
	
} // end namespace
