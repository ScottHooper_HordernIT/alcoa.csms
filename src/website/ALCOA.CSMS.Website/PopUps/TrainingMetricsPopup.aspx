﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" CodeBehind="TrainingMetricsPopup.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.TrainingMetricsPopup" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
    <%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager runat="server" ID="ScriptManager1">
</asp:ScriptManager>
<table border="0" cellpadding="0" cellspacing="0" style="width: 700px; padding:5px 0px 5px 0px">
  <tr>
    <td style="width:40%">
        
    </td>
    <td style="width:60%; vertical-align:top; text-align:left; padding:5px 0px 5px 0px">
    <div style="float:left; padding-left:20px"><asp:Button ID="btnClose" runat="server" OnClientClick="window.close();" Text="Close (ESC)" /></div>
        <div style="float:right"><asp:Button ID="btnPrint" runat="server" OnClientClick="window:print();" Text="Print" /></div>
    </td>
  </tr>
  <tr>
    <td colspan="2" style="width:100%; vertical-align:top; text-align:left; font-size:13px; padding:3px 0px 3px 0px">
       <b>Company: </b> <dxe:ASPxLabel ID="lblCompanyName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dxe:ASPxLabel>  
    </td>
  </tr>
  <tr>
    <td colspan="2" style="width:100%; vertical-align:top; text-align:left; padding-bottom:10px; font-size:13px;">
       <b>Site: </b> <dxe:ASPxLabel ID="lblSiteName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                 SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">                        
            </dxe:ASPxLabel>  
    </td>
  </tr>
  <tr>
    <td colspan="2" style="width:100%; vertical-align:top; text-align:left; padding:5px 0px 5px 0px">
        <dxwgv:ASPxGridView Id="grid" Width="100%"
                ClientInstanceName="grid" OnHtmlRowCreated="grid_RowCreated"
                 OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate"
            OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize" 
            OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" DataSourceForceStandardPaging="True"
            onafterperformcallback="grid_AfterPerformCallback" OnCustomFilterExpressionDisplayText="grid_CustomFilterExpressionDisplayText"         
                 
                runat="server" DataSourceID="ods"
                AutoGenerateColumns="False"                  
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue">
                <Settings ShowFilterRow="true" ShowFilterBar="Visible" />
                <SettingsPager PageSize="10" AllButton-Visible="true"></SettingsPager>
                 <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                        </LoadingPanelOnStatusBar>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                        </LoadingPanel>
                    </Images>
                    <ImagesFilterControl>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                        </LoadingPanel>
                    </ImagesFilterControl>                
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
              
                    <Columns>
                    
                     <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Training Packages" FieldName="COURSE_NAME" Name="COURSE_NAME">
                        <PropertiesComboBox DropDownHeight="150px" TextField="COURSE_NAME" ValueField="COURSE_NAME" DataSourceID="SqlDataSourceCourseName"
                             ValueType="System.String" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                   

                    <dxwgv:GridViewDataTextColumn FieldName="FULL_NAME" Name="FULL_NAME" Caption="Contractor Full Name" VisibleIndex="3">
                    <Settings AutoFilterCondition="Contains"/>
                    </dxwgv:GridViewDataTextColumn>
                    
                    
                    <dx:GridViewDataDateColumn ReadOnly="true" VisibleIndex="4" Caption="Date Training Occured" FieldName="COURSE_END_DATE" Name="COURSE_END_DATE">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dx:GridViewDataDateColumn>

                    <dx:GridViewDataDateColumn ReadOnly="true" VisibleIndex="5" Caption="Training Valid To" FieldName="EXPIRATION_DATE" Name="EXPIRATION_DATE">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                   <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="DateTime" EditFormatString="dd/MM/yyyy" /> 
                    </dx:GridViewDataDateColumn>  

                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="6"  SortIndex="0" SortOrder="Descending" Caption="Expires In" FieldName="ExpiresIn" Name="ExpiresIn">
                        <CellStyle HorizontalAlign="Left"></CellStyle>
                        
                        <PropertiesComboBox DropDownHeight="150px"
                             ValueType="System.Int32" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>
                        <DataItemTemplate>
                          <dxe:ASPxLabel ID="lblExpiresIn" Text="" runat="server"></dxe:ASPxLabel> &nbsp;
                        </DataItemTemplate>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                        
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    </Columns>
                  </dxwgv:ASPxGridView>
                  <div width="100%" align="right" style="padding-top:5px">
            <dxe:ASPxButton ID="btnShowAll" runat="server" Text="Show All" ForeColor="Black"  
                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" OnClick="btnShowAll_Click"
            csspostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ></dxe:ASPxButton>
            </div>   
                  <table width="700px">
                                <tr align="right">
                                    <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
                                        width: 100%" align="right">
                                        <div align="right">
                                            <uc1:ExportButtons ID="ucExportButtons2"  runat="server" />
                                        </div>
                                    </td>
                                </tr>
                 </table>
    </td>
  </tr>
  <tr>
 <td colspan="2" style="width:100%; vertical-align:top; display:none; text-align:left; padding:5px 0px 5px 0px">
       
                    <dxe:ASPxComboBox id="cmbYear" Visible="true" runat="server" Width="0px"
                      clientinstancename="cmbYear"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                      IncrementalFilteringMode="StartsWith"                     
                     valuetype="System.Int32" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic">
                        </ValidationSettings>
                    </dxe:ASPxComboBox>
  </td>
  </tr>

  </table>

  <data:CompaniesDataSource ID="CompaniesDataSource3" runat="server" Sort="CompanyName ASC"></data:CompaniesDataSource>



<asp:SqlDataSource ID="SqlDataSourceTrainingManagementValidity" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Training_Management_ValidityAll" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter Name="CompanyId" Type="Int32" />
        <asp:Parameter Name="SiteId" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>

<data:SitesDataSource ID="SitesDataSource1" runat="server" Sort="SiteName ASC"></data:SitesDataSource>



    <asp:SqlDataSource ID="SqlDataSourceCourseName" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct COURSE_NAME from HR.XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP order by COURSE_NAME" SelectCommandType="Text"> 
</asp:SqlDataSource>

<asp:ObjectDataSource ID="ods" runat="server" SortParameterName="sortColumns" EnablePaging="true"
                StartRowIndexParameterName="startRecord" MaximumRowsParameterName="maxRecords"
                SelectCountMethod="GetValidityCount" SelectMethod="GetValidity"
                 TypeName="ValidityData"></asp:ObjectDataSource>


</asp:Content>
