﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KpiVariance.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Main.KpiVariance" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<table cellspacing="0" cellpadding="2" width="100%" border="0">
    <tr>
        <td style="height: 43px" class="pageName" colspan="3">
            <span class="bodycopy"><span class="title">KPI Hours Variance Report</span><br />
                <span class="date">Detect Anomolies in reported monthly KPI Total Man Hours by Company, Site and Month</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
            </span>
        </td>
    </tr>
</table>
        <p>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        Month:
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cbMonth" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            ValueType="System.Int32" Width="120px">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="filter">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td style="padding-left: 10px;">
                        Year:
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cbYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            ValueType="System.Int32" Width="80px">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="filter">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        Residential Category:
                    </td>
                    <td>
                        <dx:ASPxComboBox ID="cbCategory" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" EnableSynchronization="False"
                            ValueType="System.Int32" Width="125px" DropDownHeight="300px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="filter">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td style="padding-left: 10px">
                        Region:
                    </td>
                    <td>
                    <td>
                        <dx:ASPxComboBox ID="cbRegion" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" EnableSynchronization="False"
                            ValueType="System.Int32" Width="125px" DropDownHeight="300px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                            <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="filter">
                                <RequiredField IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </td>
                    <td style="padding-left: 10px;">
                        <dx:ASPxButton ID="btnFilter" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" OnClick="btnFilter_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            Text="Filter" Height="27px">
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </p>
        <dx:ASPxGridView ID="grid" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" AutoGenerateColumns="False" Width="900px">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Company" FieldName="CompanyName" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Site" FieldName="SiteName" VisibleIndex="1" Width="150px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Hours (Selected Month)" FieldName="Hours_CurrentMonth"
                    VisibleIndex="3" Width="100px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Hours (Previous Month)" FieldName="Hours_PreviousMonth"
                    VisibleIndex="4" Width="100px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Variance (%)" FieldName="Variance_CurrentVsPrevious"
                    SortIndex="0" SortOrder="Descending" VisibleIndex="6" Width="100px">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Variance (Hours)" FieldName="Difference_CurrentVsPrevious"
                    SortIndex="1" SortOrder="Descending" VisibleIndex="5" Width="100px">
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior ColumnResizeMode="NextColumn" />
            <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" />
            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                </LoadingPanelOnStatusBar>
                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                </LoadingPanel>
            </Images>
            <ImagesFilterControl>
                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                </LoadingPanel>
            </ImagesFilterControl>
            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
        </dx:ASPxGridView>

    <table>
    <tr align="right">
        <td colspan="3" style="padding-top: 6px; text-align: right; text-align: -moz-right;
            width: 100%" align="right">
            <div align="right" style="text-align: right;">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
<asp:SqlDataSource ID="sqldsKpi_GetAllYearsSubmitted" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_GetAllYearsSubmitted" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    Sort="Ordinal ASC">
</data:CompanySiteCategoryDataSource>
