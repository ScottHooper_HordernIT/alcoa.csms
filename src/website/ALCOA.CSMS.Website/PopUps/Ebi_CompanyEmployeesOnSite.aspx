﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_Ebi_CompanyEmployeesOnSite" Codebehind="Ebi_CompanyEmployeesOnSite.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript" language="javascript">  
         function doClose(e) // note: takes the event as an arg (IE doesn't)  
         {  
             if (!e) e = window.event; // fix IE  

             if (e.keyCode) // IE  
             {  
                 if (e.keyCode == "27") window.close();  
             }  
             else if (e.charCode) // Netscape/Firefox/Opera  
             {  
                 if (e.keyCode == "27") window.close();  
             } 
         }
         document.onkeydown = doClose;  
 </script> 

    <div align="center">
        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" Text="Close (ESC)"
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        </dx:ASPxButton>
        <br />
        <dx:ASPxButton ID="ASPxButton2" runat="server" AutoPostBack="False" CausesValidation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Print" UseSubmitBehavior="False">
                        <ClientSideEvents Click="function(s, e) {
	javascript:window.print();
}" />
                    </dx:ASPxButton>
                    <br />
        <strong style="font-size: 14pt">Contractors On Site (Today - Live Data)</strong>
        <br /><br />
        Filtered by specific Company & Site<br />
        <br />

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" AutoGenerateColumns="False">
        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="10px">
            </LoadingPanel>
        </Styles>
        <SettingsPager Mode="ShowAllRecords">
        </SettingsPager>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <Columns>
            <dx:GridViewDataTextColumn FieldName="Company" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ClockID" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="FullName" VisibleIndex="6">
            <CellStyle HorizontalAlign="Left" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CardNumber" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Site" VisibleIndex="3">
            <CellStyle HorizontalAlign="Left" />
            </dx:GridViewDataTextColumn>
        </Columns>
        <StylesEditors>
            <ProgressBar Height="25px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
        </div>
</asp:Content>

