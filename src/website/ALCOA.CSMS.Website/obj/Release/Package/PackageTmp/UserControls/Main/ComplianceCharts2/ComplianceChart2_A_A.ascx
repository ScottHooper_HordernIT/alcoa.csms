﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Tiny_ComplianceCharts_ComplianceChart2_A_A" Codebehind="ComplianceChart2_A_A.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

<div style="width: 900px">
    <div style="text-align: center" align="center">
        <dx:ASPxLabel ID="lblResidential" runat="server" ForeColor="Red" Text="">
        </dx:ASPxLabel>
    </div>
    <br />
    <dx:ASPxGridView runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" AutoGenerateColumns="False" Width="100%" ID="grid_All">
        <SettingsBehavior AllowSort="False"></SettingsBehavior>
        <SettingsPager PageSize="30" Visible="False">
        </SettingsPager>
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
            <Header SortingImageSpacing="5px" ImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="10px">
            </LoadingPanel>
        </Styles>
        <StylesEditors>
            <ProgressBar Height="25px">
            </ProgressBar>
        </StylesEditors>
    </dx:ASPxGridView>
    <br />
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" Width="100%">
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <ContentStyle>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
<Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
        </ContentStyle>
        <TabPages>
            <dx:TabPage Name="tabEmbedded" Text="Embedded">
                <TabStyle Font-Bold="True">
                </TabStyle>
                <ContentCollection>
                    <dx:ContentControl runat="server">
                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                            TargetControlID="ContentPanel" ExpandControlID="TitlePanel" Collapsed="true"
                            TextLabelID="TextToControl" ExpandedText="" CollapsedText="" ImageControlID="ImageToControl"
                            ExpandedImage="~/Images/gvExpandedButton.png" CollapsedImage="~/Images/gvCollapsedButton.png"
                            SuppressPostBack="False" CollapseControlID="TitlePanel">
                        </asp:CollapsiblePanelExtender>
                        
                        
                        
                        <!-- Collapsible Panel Title Bar-->
                        <div id="Div1" class="collapsePanel">
                            <asp:Panel ID="TitlePanel" runat="server" Height="23px" Width="100%">
                                <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                    <div style="float: left; vertical-align: middle;">
                                        <asp:Image ID="ImageToControl" runat="server" ImageUrl="../Images/expand_blue.jpg" />
                                    </div>
                                    <div style="float: left; font-size: 10pt">
                                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Show/Hide Embedded - LWDFR, TRIFR, AIFR and IFE : Injury Ratio"
                                            ForeColor="Blue">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Collapsible Panel Target -->
                        <div id="Div2" class="collapseTargetPanel">
                            <asp:Panel ID="ContentPanel" runat="server" Height="50px" Width="100%">
                                <dx:ASPxGridView ID="grid_E" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false">
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                        </LoadingPanelOnStatusBar>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
                                    <StylesEditors>
                                        <ProgressBar Height="25px">
                                        </ProgressBar>
                                    </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                                    <SettingsPager PageSize="30">
                                        <AllButton Visible="True"></AllButton>
                                    </SettingsPager>
                                    <SettingsBehavior AllowSort="false" />
                                </dx:ASPxGridView>
                            </asp:Panel>
                        </div>
                        <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                            <dx:ASPxLabel ID="lblGrid2" runat="server" ForeColor="Red" Font-Bold="true" Text="The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure:">
                            </dx:ASPxLabel>
                        </div>
                        <dx:ASPxGridView ID="grid2_E" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="grid2_E_HtmlRowCreated">
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                            <SettingsPager PageSize="30">
                                <AllButton Visible="True"></AllButton>
                            </SettingsPager>
                            <SettingsBehavior AllowSort="false" />
                        </dx:ASPxGridView>
                        <div>
                            <div align="left" style="float: left; text-align: left; padding-top: 4px; padding-bottom: 6px; padding-left: 3px">
                                <dx:ASPxLabel ID="lblUpdateTime_E" runat="server" Text="The information shown above is a snapshot." ForeColor="Red" Font-Bold="true" Font-Size="12px">
                                </dx:ASPxLabel>
                            </div>
                            <div align="right" style="float: right; text-align: right; padding-top: 4px; padding-bottom: 6px;">
                                <dx:ASPxLabel ID="lblLastViewedAt_E" runat="server" Text="" Font-Size="11px">
                                </dx:ASPxLabel>
                            </div>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabNonEmbedded1" Text="Non Embedded 1">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server">
                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                            TargetControlID="ContentPanel2" ExpandControlID="TitlePanel2" Collapsed="true"
                            TextLabelID="TextToControl" ExpandedText="" CollapsedText="" ImageControlID="ImageToControl2"
                            ExpandedImage="~/Images/gvExpandedButton.png" CollapsedImage="~/Images/gvCollapsedButton.png"
                            SuppressPostBack="False" CollapseControlID="TitlePanel2">
                        </asp:CollapsiblePanelExtender>
                        
                        
                        <!-- Collapsible Panel Title Bar-->
                        <div id="Div3" class="collapsePanel">
                            <asp:Panel ID="TitlePanel2" runat="server" Height="23px" Width="100%">
                                <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                    <div style="float: left; vertical-align: middle;">
                                        <asp:Image ID="ImageToControl2" runat="server" ImageUrl="../Images/expand_blue.jpg" />
                                    </div>
                                    <div style="float: left; font-size: 10pt">
                                        <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Show/Hide Non Embedded 1 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio"
                                            ForeColor="Blue">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Collapsible Panel Target -->
                        <div id="Div4" class="collapseTargetPanel">
                            <asp:Panel ID="ContentPanel2" runat="server" Height="50px" Width="100%">
                                <dx:ASPxGridView ID="grid_NE1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false">
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                        </LoadingPanelOnStatusBar>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
                                    <StylesEditors>
                                        <ProgressBar Height="25px">
                                        </ProgressBar>
                                    </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                                    <SettingsPager PageSize="30">
                                        <AllButton Visible="True"></AllButton>
                                    </SettingsPager>
                                    <SettingsBehavior AllowSort="false" />
                                </dx:ASPxGridView>
                            </asp:Panel>
                        </div>
                        <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                            <dx:ASPxLabel ID="lblGrid2NE1" runat="server" ForeColor="Red" Font-Bold="true" Text="The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure:">
                            </dx:ASPxLabel>
                        </div>
                        <dx:ASPxGridView ID="grid2_NE1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="grid2_NE1_HtmlRowCreated">
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                            <SettingsPager PageSize="30">
                                <AllButton Visible="True"></AllButton>
                            </SettingsPager>
                            <SettingsBehavior AllowSort="false" />
                        </dx:ASPxGridView>
                        <div>
                            <div align="left" style="float: left; text-align: left; padding-top: 4px; padding-bottom: 6px; padding-left: 3px">
                                <dx:ASPxLabel ID="lblUpdateTime_NE1" runat="server" Text="The information shown above is a snapshot." ForeColor="Red" Font-Bold="true" Font-Size="12px">
                                </dx:ASPxLabel>
                            </div>
                            <div align="right" style="float: right; text-align: right; padding-top: 4px; padding-bottom: 6px;">
                                <dx:ASPxLabel ID="lblLastViewedAt_NE1" runat="server" Text="" Font-Size="11px">
                                </dx:ASPxLabel>
                            </div>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabNonEmbedded2" Text="Non Embedded 2">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server">
                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server"
                            TargetControlID="ContentPanel3" ExpandControlID="TitlePanel3" Collapsed="true"
                            TextLabelID="TextToControl" ExpandedText="" CollapsedText="" ImageControlID="ImageToControl3"
                            ExpandedImage="~/Images/gvExpandedButton.png" CollapsedImage="~/Images/gvCollapsedButton.png"
                            SuppressPostBack="False" CollapseControlID="TitlePanel3">
                        </asp:CollapsiblePanelExtender>
                        
                        
                                                
                        <!-- Collapsible Panel Title Bar-->
                        <div id="Div5" class="collapsePanel">
                            <asp:Panel ID="TitlePanel3" runat="server" Height="23px" Width="100%">
                                <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                    <div style="float: left; vertical-align: middle;">
                                        <asp:Image ID="ImageToControl3" runat="server" ImageUrl="../Images/expand_blue.jpg" />
                                    </div>
                                    <div style="float: left; font-size: 10pt">
                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Show/Hide Non Embedded 2 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio"
                                            ForeColor="Blue">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Collapsible Panel Target -->
                        <div id="Div6" class="collapseTargetPanel">
                            <asp:Panel ID="ContentPanel3" runat="server" Height="50px" Width="100%">
                                <dx:ASPxGridView ID="grid_NE2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false">
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                        </LoadingPanelOnStatusBar>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
                                    <StylesEditors>
                                        <ProgressBar Height="25px">
                                        </ProgressBar>
                                    </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                                    <SettingsPager PageSize="30">
                                        <AllButton Visible="True"></AllButton>
                                    </SettingsPager>
                                    <SettingsBehavior AllowSort="false" />
                                </dx:ASPxGridView>
                            </asp:Panel>
                        </div>
                        <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                            <dx:ASPxLabel ID="lblGrid2NE2" runat="server" ForeColor="Red" Font-Bold="true" Text="The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure:">
                            </dx:ASPxLabel>
                        </div>
                        <dx:ASPxGridView ID="grid2_NE2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="grid2_NE2_HtmlRowCreated">
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                            <SettingsPager PageSize="30">
                                <AllButton Visible="True"></AllButton>
                            </SettingsPager>
                            <SettingsBehavior AllowSort="false" />
                        </dx:ASPxGridView>
                        <div>
                            <div align="left" style="float: left; text-align: left; padding-top: 4px; padding-bottom: 6px; padding-left: 3px">
                                <dx:ASPxLabel ID="lblUpdateTime_NE2" runat="server" Text="The information shown above is a snapshot." ForeColor="Red" Font-Bold="true" Font-Size="12px">
                                </dx:ASPxLabel>
                            </div>
                            <div align="right" style="float: right; text-align: right; padding-top: 4px; padding-bottom: 6px;">
                                <dx:ASPxLabel ID="lblLastViewedAt_NE2" runat="server" Text="" Font-Size="11px">
                                </dx:ASPxLabel>
                            </div>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Name="tabNonEmbedded3" Text="Non Embedded 3">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl3" runat="server">
                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender4" runat="server"
                            TargetControlID="ContentPanel4" ExpandControlID="TitlePanel4" Collapsed="true"
                            TextLabelID="TextToControl" ExpandedText="" CollapsedText="" ImageControlID="ImageToControl4"
                            ExpandedImage="~/Images/gvExpandedButton.png" CollapsedImage="~/Images/gvCollapsedButton.png"
                            SuppressPostBack="False" CollapseControlID="TitlePanel4">
                        </asp:CollapsiblePanelExtender>
                       



                        <!-- Collapsible Panel Title Bar-->
                        <div id="Div7" class="collapsePanel">
                            <asp:Panel ID="TitlePanel4" runat="server" Height="23px" Width="100%">
                                <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                    <div style="float: left; vertical-align: middle;">
                                        <asp:Image ID="ImageToControl4" runat="server" ImageUrl="../Images/expand_blue.jpg" />
                                    </div>
                                    <div style="float: left; font-size: 10pt">
                                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Show/Hide Non-Embedded 3 - LWDFR, TRIFR, AIFR and IFE : Injury Ratio"
                                            ForeColor="Blue">
                                        </dx:ASPxLabel>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- Collapsible Panel Target -->
                        <div id="Div8" class="collapseTargetPanel">
                            <asp:Panel ID="ContentPanel4" runat="server" Height="50px" Width="100%">
                                <dx:ASPxGridView ID="grid_NE3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false">
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                        </LoadingPanelOnStatusBar>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
                                    <StylesEditors>
                                        <ProgressBar Height="25px">
                                        </ProgressBar>
                                    </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                                    <SettingsPager PageSize="30">
                                        <AllButton Visible="True"></AllButton>
                                    </SettingsPager>
                                    <SettingsBehavior AllowSort="false" />
                                </dx:ASPxGridView>
                            </asp:Panel>
                        </div>
                        <div align="center" style="text-align: center; padding-top: 8px; padding-bottom: 3px;">
                            <dx:ASPxLabel ID="lblGrid2NE3" runat="server" ForeColor="Red" Font-Bold="true" Text="The below table indicates the percentage (%) number of companies compliant per Key Performance Indicator (KPI) measure:">
                            </dx:ASPxLabel>
                        </div>
                        <dx:ASPxGridView ID="grid2_NE3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Width="100%" AutoGenerateColumns="false" OnHtmlRowCreated="grid2_NE3_HtmlRowCreated">
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
<SettingsBehavior AllowSort="False"></SettingsBehavior>

                            <SettingsPager PageSize="30">
                                <AllButton Visible="True"></AllButton>
                            </SettingsPager>
                            <SettingsBehavior AllowSort="false" />
                        </dx:ASPxGridView>
                        <div>
                            <div align="left" style="float: left; text-align: left; padding-top: 4px; padding-bottom: 6px; padding-left: 3px">
                                <dx:ASPxLabel ID="lblUpdateTime_NE3" runat="server" Text="The information shown above is a snapshot." ForeColor="Red" Font-Bold="true" Font-Size="12px">
                                </dx:ASPxLabel>
                            </div>
                            <div align="right" style="float: right; text-align: right; padding-top: 4px; padding-bottom: 6px;">
                                <dx:ASPxLabel ID="lblLastViewedAt_NE3" runat="server" Text="" Font-Size="11px">
                                </dx:ASPxLabel>
                            </div>
                        </div>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <LoadingPanelStyle ImageSpacing="6px">
        </LoadingPanelStyle>
    </dx:ASPxPageControl> 
    
    
         
</div>
