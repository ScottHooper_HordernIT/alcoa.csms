//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class KpiProject
    {
        public int KpiProjectId { get; set; }
        public int KpiId { get; set; }
        public string ProjectTitle { get; set; }
        public int ProjectHours { get; set; }
    
        public virtual Kpi Kpi { get; set; }
    }
}
