﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_EmailLogs" Codebehind="EmailLogs.ascx.cs" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
<table class="style1">
    <tr>
        <td>
            Month:</td>
        <td>
            <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" DataSourceID="MonthsDataSource1" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                TextField="MonthName" ValueField="MonthId" ValueType="System.String">
                <ButtonStyle Width="13px">
                </ButtonStyle>
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Year:</td>
        <td>
            <dx:ASPxComboBox ID="ASPxComboBox2" runat="server" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                ValueType="System.String">
                <ButtonStyle Width="13px">
                </ButtonStyle>
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <dx:ASPxButton ID="ASPxButton1" runat="server" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" onclick="ASPxButton1_Click" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                Text="Filter / Go">
            </dx:ASPxButton>
        </td>
    </tr>
</table>
<dx:ASPxGridView ID="ASPxGridView1" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue">
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>
<data:EmailLogTypeDataSource ID="EmailLogTypeDataSource1" runat="server">
</data:EmailLogTypeDataSource>
<data:MonthsDataSource ID="MonthsDataSource1" runat="server">
</data:MonthsDataSource>
