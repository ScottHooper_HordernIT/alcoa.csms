﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Residential"
    CodeBehind="residential.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    DataSourceID="CompanySiteCategoryStandardDataSource" KeyFieldName="CompanySiteCategoryStandardId"
    OnCellEditorInitialize="gridStandard_CellEditorInitialize" Width="900px" OnRowUpdating="grid_RowUpdating"
    OnInitNewRow="grid_InitNewRow" OnRowInserting="grid_RowInserting">
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="115px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="CompanySiteCategoryStandardId" VisibleIndex="0"
            ReadOnly="True" Visible="False">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" VisibleIndex="1"
            SortIndex="1" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="2"
            SortIndex="2" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="dsSitesFilter" TextField="SiteName" ValueField="SiteId"
                ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
            VisibleIndex="3" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" TextField="CategoryDesc"
                ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Location Sponsor" FieldName="LocationSponsorUserId"
            VisibleIndex="4">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersAlcoanDataSource2"
                TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn FieldName="Approved" VisibleIndex="5">
            <PropertiesComboBox ValueType="System.Int32" NullDisplayText="Tentative">
                <Items>
                    <dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem>
                    <dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem>
                    <%--<dxe:ListEditItem Text="Tentative" Value=""></dxe:ListEditItem>--%>
                </Items>
            </PropertiesComboBox>
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="ARP" FieldName="ArpUserId" VisibleIndex="6">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersCompaniesActiveAllDataSource"
                TextField="CompanyUserFullNameLogon" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <%--<dxwgv:GridViewDataComboBoxColumn Caption="CRP" FieldName="CrpUserId" VisibleIndex="7">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersCompaniesActiveContractorsDataSource"
                TextField="CompanyUserFullNameLogon" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>--%>
    </Columns>
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <SettingsEditing Mode="Inline" />
    <Settings ShowFilterBar="Visible" ShowFilterRow="True" ShowGroupPanel="True" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px">
        </CollapsedButton>
        <ExpandedButton Height="12px" Width="11px">
        </ExpandedButton>
        <DetailCollapsedButton Height="12px" Width="11px">
        </DetailCollapsedButton>
        <DetailExpandedButton Height="12px" Width="11px">
        </DetailExpandedButton>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
</dxwgv:ASPxGridView>
<asp:SqlDataSource ID="sqldsEHS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT DISTINCT(LocCode) As Id, LocCode As Name FROM Sites WHERE LocCode NOT LIKE ''">
</asp:SqlDataSource>
<data:CompanySiteCategoryStandardDataSource ID="CompanySiteCategoryStandardDataSource"
    runat="server" SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryStandardDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True"
    Sort="SiteName ASC">
</data:SitesDataSource>
<data:EntityDataSource ID="UsersAlcoanDataSource2" runat="server" ProviderName="UsersAlcoanProvider"
    EntityTypeName="KaiZen.CSMS.UsersAlcoan, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll" Sort="UserFullNameLogon ASC" />
    <data:EntityDataSource ID="UsersCompaniesActiveAllDataSource" runat="server" ProviderName="UsersCompaniesActiveAllProvider"
                EntityTypeName="KaiZen.CSMS.UsersCompaniesActiveContractors, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
                SelectMethod="GetAll" Sort="CompanyUserFullNameLogon ASC" />
    <data:EntityDataSource ID="UsersCompaniesActiveContractorsDataSource" runat="server" ProviderName="UsersCompaniesActiveContractorsProvider"
EntityTypeName="KaiZen.CSMS.UsersCompaniesActiveContractors, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
SelectMethod="GetAll" Sort="CompanyUserFullNameLogon ASC" />