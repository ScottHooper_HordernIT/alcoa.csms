﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContractorDataManagementDetail" Codebehind="ContractorDataManagementDetail.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="./ContractorDataManagementDetailPersonTab.ascx" TagName="PersonTab" TagPrefix="uc1" %>
<%@ Register Src="./ContractorDataManagementDetailTrainingTab.ascx" TagName="TrainingTab" TagPrefix="uc1" %>

<asp:PlaceHolder ID="ContentPlaceHolder" runat="server" Visible="false">
    <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
        <thead>
            <tr>
                <th width="33%"></th>
                <th width="33%"></th>
                <th width="33%"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2">
                    <span class="bodycopy"><span class="title">Contractor Person Maintenance</span></span><br>
                    <span><a>Contact Details</a></span><span> &gt; </span>
                    <span><a href="/ContractorDataManagement.aspx">Contractor Persons</a></span><span> &gt; </span>
                    <span><a style="font-weight:bold;"><asp:Literal ID="BreadcrumbLiteral" runat="server" Text="Add Contractor"></asp:Literal></a></span><br>
                    <img height="1" src="images/grfc_dottedline.gif" width="24">&nbsp;<br>
                </td>
                <td align="right">
                    <asp:Button ID="CancelButton" runat="server" Text="Close" PostBackUrl="~/ContractorDataManagement.aspx" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; vertical-align:top;">
                    <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" EnableHierarchyRecreation="true"
                        Width="900px">
                        <TabPages>
                            <dxtc:TabPage Text="Personal and Assignment Details">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                                        <uc1:PersonTab ID="ucPersonTab" runat="server" />
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage Text="Insert Induction Details">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                                        <uc1:TrainingTab ID="TrainingTab" runat="server" />
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                        </TabPages>
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif"></LoadingPanelImage>
                        <LoadingPanelStyle ImageSpacing="6px"></LoadingPanelStyle>
                        <ContentStyle>
                            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                        </ContentStyle>
                    </dxtc:ASPxPageControl>
                </td>
            </tr>
        </tbody>
    </table>
</asp:PlaceHolder>
<asp:PlaceHolder ID="NoContentPlaceHolder" runat="server" Visible="false">
    <asp:Literal ID="NoContentLiteral" runat="server"></asp:Literal>
</asp:PlaceHolder>
