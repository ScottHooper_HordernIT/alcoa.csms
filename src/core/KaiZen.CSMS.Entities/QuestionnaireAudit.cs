﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireAudit' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireAudit : QuestionnaireAuditBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireAudit"/> instance.
		///</summary>
		public QuestionnaireAudit():base(){}	
		
		#endregion
	}
}
