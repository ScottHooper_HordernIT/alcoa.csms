<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_Regions" Codebehind="Regions.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<dxwgv:aspxgridview id="grid"
    runat="server" autogeneratecolumns="False"
    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
    datasourceid="dsRegionsFilter" keyfieldname="RegionId"
    OnRowUpdating="grid_RowUpdating" OnRowInserting="grid_RowInserting"
    >
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Height="12px" Width="11px"></CollapsedButton>

<ExpandedButton Height="12px" Width="11px"></ExpandedButton>

<DetailCollapsedButton Height="12px" Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Height="12px" Width="11px"></DetailExpandedButton>

<FilterRowButton Height="13px" Width="13px"></FilterRowButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanel>
</Images>

<SettingsEditing Mode="Inline"></SettingsEditing>
<Columns>
    <dxwgv:GridViewCommandColumn VisibleIndex="0">
        <EditButton Visible="True">
        </EditButton>
        <NewButton Visible="True">
        </NewButton>
        <DeleteButton Visible="True">
        </DeleteButton>
    </dxwgv:GridViewCommandColumn>
    <dxwgv:GridViewDataTextColumn FieldName="RegionId" ReadOnly="True" Visible="False"
        VisibleIndex="0">
        <EditFormSettings Visible="False" />
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataTextColumn FieldName="RegionName" VisibleIndex="1">
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataTextColumn FieldName="RegionNameAbbrev" VisibleIndex="2">
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataTextColumn FieldName="RegionDescription" VisibleIndex="3">
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataTextColumn FieldName="RegionValue" Visible="True" VisibleIndex="4">
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataTextColumn FieldName="IprocCode" VisibleIndex="4">
    </dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataCheckColumn Caption="Visible" FieldName="IsVisible" VisibleIndex="5">
    </dxwgv:GridViewDataCheckColumn>
    <dxwgv:GridViewDataTextColumn FieldName="Ordinal" VisibleIndex="6">
    </dxwgv:GridViewDataTextColumn>
</Columns>
<Settings ShowHeaderFilterButton="true" />
    <SettingsPager NumericButtonCount="25" PageSize="25">
        <AllButton Visible="True"></AllButton>
    </SettingsPager>
    <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
</dxwgv:aspxgridview>

<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Sort="Ordinal ASC"></data:RegionsDataSource>