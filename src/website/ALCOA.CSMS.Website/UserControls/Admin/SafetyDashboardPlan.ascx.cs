﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using DevExpress.Web.ASPxGridView;



    public partial class SafetyDashboardPlan : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
        }
        
        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                int Current_year = DateTime.Now.Year;
                while (Current_year >= 2012)
                {
                    ddlYear.Items.Add(Current_year.ToString(), Convert.ToInt32(Current_year));
                    Current_year--;
                } //End
                ddlYear.Value = DateTime.Today.Year;
                ddlMonth.Value = DateTime.Today.Month;

                SqlDataSource1.SelectParameters["Month"].DefaultValue = ddlMonth.Value.ToString();
                SqlDataSource1.SelectParameters["Year"].DefaultValue = ddlYear.Value.ToString();

                grid.SettingsBehavior.AllowDragDrop = false;
                grid.SettingsBehavior.AllowSort = false;

                DataTable dtTable = ((DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty)).ToTable();
                 
                Session["dtTable"] = dtTable;
                grid.DataSource = dtTable;
                grid.DataBind();
               
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            SqlDataSource1.SelectParameters["Month"].DefaultValue = ddlMonth.Value.ToString();
            SqlDataSource1.SelectParameters["Year"].DefaultValue = ddlYear.Value.ToString();

            DataTable dtTable = ((DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty)).ToTable();

            Session["dtTable"] = dtTable;
            grid.DataSource = dtTable;
            grid.DataBind();
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();

                    SqlCommand cmdDel = new SqlCommand("AdminPlan_Update", cnDel);
                    cmdDel.CommandTimeout = 200;
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.Parameters.AddWithValue("@Operator", Convert.ToString(e.NewValues["Operator"]));
                    cmdDel.Parameters.AddWithValue("@PlanValue", Convert.ToString(e.NewValues["PlanValue"]));
                    cmdDel.Parameters.AddWithValue("@PlanText", Convert.ToString(e.NewValues["PlanText"]));

                    cmdDel.Parameters.AddWithValue("@Year", Convert.ToInt32(ddlYear.SelectedItem.Value));
                    cmdDel.Parameters.AddWithValue("@Month", Convert.ToInt32(ddlMonth.SelectedItem.Value));
                    cmdDel.Parameters.AddWithValue("@DashBoardId", Convert.ToInt32(e.Keys["DashboardId"]));

                    

                    cmdDel.ExecuteNonQuery();
                    cnDel.Close();
                }


                ddlYear.Enabled = true;
                ddlMonth.Enabled = true;


                DataTable dtTable = new DataTable("dtTable");
                dtTable = (DataTable)Session["dtTable"];
                foreach (DataRow dr in dtTable.Rows)
                {
                    if (Convert.ToInt32(dr["DashboardId"]) == Convert.ToInt32(e.Keys["DashboardId"]))
                    {
                        dr["Operator"] = Convert.ToString(e.NewValues["Operator"]);
                        dr["PlanValue"] = Convert.ToString(e.NewValues["PlanValue"]);
                        dr["PlanText"] = Convert.ToString(e.NewValues["PlanText"]);


                    }
                }
                grid.CancelEdit();
                e.Cancel = true;

                grid.DataSource = dtTable;
                grid.DataBind();
                Session["dtTable"] = dtTable;
                

            }
            catch (Exception ex)
            {
                
                //throw;
            }
        }

       
        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["dtTable"];
            foreach (DataRow dr in dt.Rows)
            {
                int DashboardPlanId = 0;
                if (Convert.ToString(dr["DashboardPlanId"]) != "")
                 DashboardPlanId = Convert.ToInt32(dr["DashboardPlanId"]);
                int DashboardId = Convert.ToInt32(dr["DashboardId"]);
                string Operator = Convert.ToString(dr["Operator"]);
                string PlanValue = Convert.ToString(dr["PlanValue"]);
                string PlanText = Convert.ToString(dr["PlanText"]);
                int year = Convert.ToInt32(ddlYear.SelectedItem.Value);
                int month = Convert.ToInt32(ddlMonth.SelectedItem.Value);

                int dsAuditScoreTemp = DataRepository.KpiProvider.SetPlanValue(DashboardPlanId, DashboardId, Operator, PlanValue, PlanText, year, month);
            }
                
        }


        protected void grid_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            ddlYear.Enabled = false;
            ddlMonth.Enabled = false;
        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            if (e.CallbackName == "STARTEDIT")
            {
                ddlYear.Enabled = false;
                ddlMonth.Enabled = false;
            }
        }

        protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            foreach (GridViewColumn column in grid.Columns)
            {
                GridViewDataColumn dataColumn = column as GridViewDataColumn;
                if (dataColumn == null) continue;
                if (dataColumn.FieldName == "PlanValue")
                {
                    if (e.NewValues[dataColumn.FieldName] != null)
                    {
                        string str = Convert.ToString(e.NewValues[dataColumn.FieldName]);
                        double Num;
                        bool isNum = double.TryParse(str, out Num);
                        if (!isNum)
                        {
                            e.Errors[dataColumn] = "Invalid format! Please enter data in decimal format.";
                        }
                    }
                       
                }
            }

        }
        
    }
