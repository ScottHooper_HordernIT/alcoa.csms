using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Data.SqlClient;
using DevExpress.XtraCharts;
using System.Threading;
// excellent use of oo and programming techniques below... lol
// TODO: write properly when time allows = never? lol
public partial class UserControls_ComplianceChart2p : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        Reports_DropDownListPicker.btnHandler += new
                 UserControls_Tiny_Reports_DropDownListPicker2p.OnButtonClick(DropDownListPicker_btnHandler);

        ASPxGridView.RegisterBaseScript(this.Page);

        //Adding by Pankaj Dikshit for <DT 2730, 01-Mar-2013> - Start
        if (!String.IsNullOrEmpty(Request.QueryString["internal"]))
        {
            SessionHandler.spVar_RadarInternal = Convert.ToString(Request.QueryString["internal"]);
        }

        if (Convert.ToString(SessionHandler.spVar_RadarInternal) == "1")
        {
            lblInternal.Text = "Internal";
        }
        else
        {
            lblInternal.Text = "External";
        }
        //Adding by Pankaj Dikshit for <DT 2730, 01-Mar-2013> - End

        //Commented by Pankaj Dikshit for <DT 2730, 01-Mar-2013>
        //if (!String.IsNullOrEmpty(Request.QueryString["internal"]))
        //{
        //    lblInternal.Text = "External";
        //    SessionHandler.spVar_RadarInternal = "0";
        //    if (Request.QueryString["internal"].ToString() == "1")
        //    {
        //        lblInternal.Text = "Internal";
        //        SessionHandler.spVar_RadarInternal = "1";
        //    }
        //}

        if (Request.QueryString["id"] != null)
        {
            if (auth.RoleId == (int)RoleList.Contractor &&
                Request.QueryString["id"] == "ComplianceChart2_S_A")
            {
                SessionHandler.spVar_Page = "ComplianceChart2_S_A";
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId))
                    { SessionHandler.spVar_SiteId = null; };
                
                if (String.IsNullOrEmpty(SessionHandler.spVar_Year))
                {
                    DataTable dt = ((DataView)sqldsKpi_GetAllYearsSubmitted.Select(DataSourceSelectArguments.Empty)).ToTable();
                    int r = dt.Rows.Count - 1;
                    int latestYear = 0;
                    Int32.TryParse(dt.Rows[r].ItemArray[0].ToString(), out latestYear);

                    if (latestYear == 0)
                    {
                        latestYear = DateTime.Now.Year;
                    }

                    if (DateTime.Now.Year == latestYear)
                    {
                        if (DateTime.Now.Month == 1)
                        {
                            latestYear = DateTime.Now.Year - 1;
                        }
                    }

                    SessionHandler.spVar_Year = latestYear.ToString();
                }
            }
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                int i = 0;
                if (String.IsNullOrEmpty(SessionHandler.spVar_CompanyId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_SiteId)) { i++; };
                if (String.IsNullOrEmpty(SessionHandler.spVar_Year)) { i++; };
                
                if (i == 0)
                {
                    Control uc = LoadControl(String.Format("~/UserControls/Main/ComplianceCharts2/{0}.ascx", SessionHandler.spVar_Page));
                    PlaceHolder1.Controls.Add(uc);
                }
            }
        }
        else
        {
            //Select COmpany
        }

        if (!postBack)
        {
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader): //Reader

                    break;
                case ((int)RoleList.Contractor): //Contributor
                    //SessionHandler.spVar_Page = "ComplianceChart_S_A";
                    //SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                    //SessionHandler.spVar_SiteId = "-5";
                    //SessionHandler.spVar_ResidentialStatus = "3";
                    //SessionHandler.spVar_Year = DateTime.Now.Year.ToString();
                    ////Response.Redirect("ComplianceChart.aspx?id=" + SessionHandler.spVar_Page, true);
                    break;
                case ((int)RoleList.Administrator): //Administrator
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    void DropDownListPicker_btnHandler(String strvalue)
    {
        try
        {

            PlaceHolder1.Controls.Clear();

            ASPxComboBox ddlCompanies = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlCompanies");
            ASPxComboBox ddlSites = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlSites");
            ASPxComboBox ddlYear = (ASPxComboBox)Reports_DropDownListPicker.FindControl("ddlYear");

            string sCompany = ddlCompanies.SelectedItem.Value.ToString();
            string sSite = ddlSites.SelectedItem.Value.ToString();
            string sYear = ddlYear.SelectedItem.Value.ToString();

            int i = 0;
            if (String.IsNullOrEmpty(sCompany)) { i++; };
            if (String.IsNullOrEmpty(sSite)) { i++; };
            if (String.IsNullOrEmpty(sYear)) { i++; };
            if ((sCompany) == "0") i++;
            if ((sSite) == "0") i++;
            if (i == 0)
            {
                SessionHandler.spVar_CompanyId = sCompany;
                SessionHandler.spVar_SiteId = sSite;

                if (sSite.Contains("-"))
                {
                    SessionHandler.spVar_RegionId = sSite.Replace("-", "");
                }
                else
                {
                    SessionHandler.spVar_RegionId = null;
                }

                SessionHandler.spVar_SiteName = ddlSites.SelectedItem.Text;
                SessionHandler.spVar_Year = sYear;
                SessionHandler.spVar_MonthId = "0"; //YTD

                SessionHandler.spVar_Page = "ComplianceChart2_A_A";
                if (Convert.ToInt32(sCompany) > 0)
                {
                    if (!String.IsNullOrEmpty(SessionHandler.spVar_RegionId))
                    {
                        SessionHandler.spVar_Page = "ComplianceChart2_S_A";
                    }
                    else
                    {
                        SessionHandler.spVar_Page = "ComplianceChart2_S_S";
                    }
                }

                Response.Redirect("ComplianceChart2p.aspx?id=" + SessionHandler.spVar_Page, false);
            }
            else
            {
                PopUpErrorMessage("Please Select All Search Fields.");
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            if (ex.Message == "Object reference not set to an instance of an object.")
            {
                PopUpErrorMessage("Please Select All Search Fields.");
            }
            else
            {
                PopUpErrorMessage("Error Occured: " + ex.Message);
            }
        }
    }

    protected void PopUpErrorMessage(string errormsg)
    {
        //ASPxGridView1.Visible = false;
        PopupWindow pcWindow = new PopupWindow(errormsg);
        pcWindow.FooterText = "";
        pcWindow.ShowOnPageLoad = true;
        pcWindow.Modal = true;
        //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
        ASPxPopupControl1.Windows.Add(pcWindow);
        //WebChartControl1.Visible = false;
    }

    
}

