﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireVerificationAssessmentAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireVerificationAssessmentAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireVerificationAssessmentAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessmentAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessmentAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationAssessmentAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireVerificationAssessmentAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireVerificationAssessmentAudit&gt;"/></returns>
		public static TList<QuestionnaireVerificationAssessmentAudit> Fill(IDataReader reader, TList<QuestionnaireVerificationAssessmentAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireVerificationAssessmentAudit")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireVerificationAssessmentAudit>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireVerificationAssessmentAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditEventId - 1)];
					c.ResponseId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.ResponseId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.ResponseId - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionnaireId - 1)];
					c.SectionId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.SectionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.SectionId - 1)];
					c.QuestionId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionId - 1)];
					c.AssessorApproval = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorApproval - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorApproval - 1)];
					c.AssessorComment = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorComment - 1)))?null:(System.String)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorComment - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AuditEventId - 1)];
			entity.ResponseId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.ResponseId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.ResponseId - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionnaireId - 1)];
			entity.SectionId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.SectionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.SectionId - 1)];
			entity.QuestionId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.QuestionId - 1)];
			entity.AssessorApproval = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorApproval - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorApproval - 1)];
			entity.AssessorComment = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorComment - 1)))?null:(System.String)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.AssessorComment - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireVerificationAssessmentAuditColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.ResponseId = Convert.IsDBNull(dataRow["ResponseId"]) ? null : (System.Int32?)dataRow["ResponseId"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.Int32?)dataRow["QuestionnaireId"];
			entity.SectionId = Convert.IsDBNull(dataRow["SectionId"]) ? null : (System.Int32?)dataRow["SectionId"];
			entity.QuestionId = Convert.IsDBNull(dataRow["QuestionId"]) ? null : (System.Int32?)dataRow["QuestionId"];
			entity.AssessorApproval = Convert.IsDBNull(dataRow["AssessorApproval"]) ? null : (System.Boolean?)dataRow["AssessorApproval"];
			entity.AssessorComment = Convert.IsDBNull(dataRow["AssessorComment"]) ? null : (System.String)dataRow["AssessorComment"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireVerificationAssessmentAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireVerificationAssessmentAudit</c>
	///</summary>
	public enum QuestionnaireVerificationAssessmentAuditChildEntityTypes
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentAuditChildEntityTypes
	
	#region QuestionnaireVerificationAssessmentAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireVerificationAssessmentAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditFilterBuilder : SqlFilterBuilder<QuestionnaireVerificationAssessmentAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditFilterBuilder class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentAuditFilterBuilder
	
	#region QuestionnaireVerificationAssessmentAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireVerificationAssessmentAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireVerificationAssessmentAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditParameterBuilder class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationAssessmentAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationAssessmentAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationAssessmentAuditParameterBuilder
	
	#region QuestionnaireVerificationAssessmentAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireVerificationAssessmentAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireVerificationAssessmentAuditSortBuilder : SqlSortBuilder<QuestionnaireVerificationAssessmentAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditSqlSortBuilder class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireVerificationAssessmentAuditSortBuilder
	
} // end namespace
