﻿	

#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	/// <summary>
	/// An component type implementation of the 'TwentyOnePointAudit_18' table.
	/// </summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class TwentyOnePointAudit18Service : KaiZen.CSMS.Services.TwentyOnePointAudit18ServiceBase
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit18Service class.
		/// </summary>
		public TwentyOnePointAudit18Service() : base()
		{
		}
		#endregion Constructors
		
	}//End Class

} // end namespace
