﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" CodeBehind="ContractorDataManagementDetail.aspx.cs" Inherits="ALCOA.CSMS.Website.ContractorDataManagementDetail" %>

<%@ Register src="UserControls/Main/ContractorDataManagementDetail.ascx" tagname="ContractorDataManagementDetail" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <uc1:ContractorDataManagementDetail ID="ContractorDataManagementDetail1" runat="server" />
</asp:Content>
