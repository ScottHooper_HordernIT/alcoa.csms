﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Validity2.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.PeopleManagementTraining.Validity2" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>


<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
    <%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">People Management - Training</span><br />
                <span class="date">Validity</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1"><br />
            </span></td>
    </tr>

    <tr>
        <td style="width:100%; padding-top:15px; text-align:left; vertical-align:top">
             <div style="font-weight:bold; float:left;  padding:5px 0px 0px 15px">Company:</div>
            <div style="float:left;  padding:0px 0px 0px 3px">
               <dxe:ASPxComboBox ID="cmbCompanies" IncrementalFilteringMode="StartsWith"
                       ClientInstanceName="cmbCompanies" runat="server"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" EnableSynchronization="False" 
                         SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                          OnSelectedIndexChanged="cmbCompanies_SelectedIndexChanged"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic" ValidationGroup="ShowData"  ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a Company!" ErrorImage-AlternateText="Please select a Company!">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a Company!" />
                                                    </ValidationSettings>
                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	                                                                    cmbSites.PerformCallback(s.GetValue());
                                                                    }" />
                    </dxe:ASPxComboBox>
            </div>

            <div style="font-weight:bold; float:left; padding:5px 0px 0px 15px">Sites/Regions:</div>
            <div style="float:left; padding:0px 0px 0px 3px">
                <dxe:ASPxComboBox ID="cmbSites"  ClientInstanceName="cmbSites"
                       runat="server" EnableSynchronization="False" OnCallback="cmbSites_Callback"
                       CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" IncrementalFilteringMode="StartsWith"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        ValueType="System.Int32">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic" ValidationGroup="ShowData"  ErrorDisplayMode="ImageWithTooltip" ErrorText="Please select a Company!" ErrorImage-AlternateText="Please select a Site!">
                                                        <RequiredField IsRequired="True" ErrorText="Please select a Site!" />
                         </ValidationSettings>
                    </dxe:ASPxComboBox>
            </div>

            <div style="float:left; padding:0px 0px 0px 10px">
                <dxe:ASPxButton ID="btnSearchGo" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" OnClick="btnGoFilter_click" Text="Go / Refresh" ValidationGroup="ShowData"
                            Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        </dxe:ASPxButton>

                <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Text="Add Data" Visible="false"
                            Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        </dxe:ASPxButton>
            </div>
        </td>
    </tr>

    <tr>
        
        <td style="width:100%; text-align:left; vertical-align:top">
             <dxwgv:ASPxGridView Id="grid" Width="100%" Visible="true"
                ClientInstanceName="grid" OnHtmlRowCreated="grid_RowCreated"
                 OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate"
            OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize" 
            OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter" DataSourceForceStandardPaging="True"
            onafterperformcallback="grid_AfterPerformCallback" OnCustomFilterExpressionDisplayText="grid_CustomFilterExpressionDisplayText"      
                 
                runat="server" DataSourceID="ods"
                AutoGenerateColumns="False"                  
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue">
                <Settings ShowFilterRow="true" ShowFilterBar="Visible" />
                <SettingsPager PageSize="100" AllButton-Visible="false"></SettingsPager>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
              
                    <Columns>
                    
                        <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="0" Caption="Company" FieldName="CompanyId" Name="CompanyId">
                         <HeaderStyle Wrap="True" />
                        <PropertiesComboBox DropDownHeight="150px" TextField="CompanyName" DataSourceID="SqlCompanyDataSource"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True"/>
                        <Settings SortMode="DisplayText"/>
                    </dxwgv:GridViewDataComboBoxColumn>

                        
                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="1" Caption="Site" FieldName="SiteId" Name="SiteId">
                         <HeaderStyle Wrap="True" />
                        <PropertiesComboBox DropDownHeight="150px" DataSourceID="SitesDataSource2" TextField="SiteName" ValueField="SiteId"
                             ValueType="System.Int32" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                   

                     
                     <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Training Package" FieldName="COURSE_NAME" Name="COURSE_NAME">
                        <HeaderStyle Wrap="True" /> 
                        <PropertiesComboBox DropDownHeight="150px" TextField="COURSE_NAME" ValueField="COURSE_NAME" DataSourceID="sqldaCourseName"
                             ValueType="System.String" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>                       
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                   

                    <dxwgv:GridViewDataTextColumn FieldName="FULL_NAME" Name="FULL_NAME" Caption="Contractor Full Name" VisibleIndex="3">
                    <HeaderStyle Wrap="True" />
                    <Settings AutoFilterCondition="Contains"/>
                    </dxwgv:GridViewDataTextColumn>
                    
                    
                    <dxwgv:GridViewDataDateColumn ReadOnly="true" VisibleIndex="4" Caption="Date Training Occured" FieldName="COURSE_END_DATE" Name="COURSE_END_DATE">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dxwgv:GridViewDataDateColumn>

                    <dxwgv:GridViewDataDateColumn ReadOnly="true" VisibleIndex="5" Caption="Training Valid To" FieldName="EXPIRATION_DATE" Name="EXPIRATION_DATE">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                   <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="DateTime" EditFormatString="dd/MM/yyyy" /> 
                    </dxwgv:GridViewDataDateColumn>  

                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="6" SortIndex="0" SortOrder="Descending" Caption="Expires In" FieldName="ExpiresIn" Name="ExpiresIn">
                        <CellStyle HorizontalAlign="Left"></CellStyle>
                        
                        <PropertiesComboBox DropDownHeight="150px"
                             ValueType="System.String" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>
                        <DataItemTemplate>
                          <dxe:ASPxLabel ID="lblExpiresIn" Text="" runat="server"></dxe:ASPxLabel> &nbsp;
                        </DataItemTemplate>                        
                        <Settings SortMode="DisplayText" />
                        
                    </dxwgv:GridViewDataComboBoxColumn>

                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="7" Caption="Expired" FieldName="Expired" Name="Expired">
                       <CellStyle HorizontalAlign="Center"></CellStyle>
                        <PropertiesComboBox DropDownHeight="150px"
                             ValueType="System.String" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true">
                             <Items>
                                <dxe:ListEditItem Text="Yes" Value="Yes" />
                                <dxe:ListEditItem Text="No" Value="No" />
                                <dxe:ListEditItem Text="-" Value="-" />
                            </Items>
                        </PropertiesComboBox>                       
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>

                    </Columns>
                  </dxwgv:ASPxGridView>




               <div width="100%" align="right" style="padding-top:5px">
            <dxe:ASPxButton ID="btnShowAll" runat="server" Text="Show All" ForeColor="Black"  
                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" OnClick="btnShowAll_Click"
            csspostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ></dxe:ASPxButton>
            </div>   
        </td>
    </tr>
</table>
 <table width="900px" style="padding-bottom:5px">
                                                        <tr align="right">
                                                        <td style="padding-top: 6px; text-align: left; text-align: -moz-left;
                                                                width: 80%" align="left">
                                                            nb. Export feature only supports above grid having a maximum of 65,000 rows/items being displayed at once.<br />
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If there is over 65,000 rows currently being displayed simply use appropriate filters to reduce this before exporting.
                                                        </td>
                                                            <td style="padding-top: 6px; text-align: right; text-align: -moz-right;
                                                                width: 20%" align="right">
                                                                <div align="right">
                                                                    <uc1:ExportButtons ID="ucExportButtons2" Visible="true" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

<asp:SqlDataSource ID="SqlCompanyDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select CompanyId,CompanyName from Companies order by CompanyName ASC
">
</asp:SqlDataSource>

<asp:SqlDataSource ID="SqlDataSourceTrainingManagementValidity" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Training_Management_ValidityAll2" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter Name="CompanyId" Type="Int32" DefaultValue="-1" />
        <asp:Parameter Name="SiteId" Type="Int32" DefaultValue="-2" />
    </SelectParameters>
</asp:SqlDataSource>


<asp:SqlDataSource ID="SitesDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select SiteId,SiteName from sites where siteAbbrev not like '%-%'
union 
select (-1*RegionId) as SiteId,RegionName as SiteName from regions where IsVisible='1' order by SiteName
">
</asp:SqlDataSource>



<asp:SqlDataSource ID="sqldaCourseName" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct Course_Name from hr.XXHR_ENR_TRNG_V order by Course_Name asc" 
     SelectCommandType="Text">    
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqlDataSourceFullName" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select distinct Full_Name from hr.XXHR_ENR_TRNG_V order by Full_Name" SelectCommandType="Text"> 
</asp:SqlDataSource>

<asp:ObjectDataSource ID="ods" runat="server" SortParameterName="sortColumns" EnablePaging="true"
                StartRowIndexParameterName="startRecord" MaximumRowsParameterName="maxRecords"
                SelectCountMethod="GetValidityCount" SelectMethod="GetValidity"
                 TypeName="ValidityData"></asp:ObjectDataSource>





           
