<%@ Page Language="C#" AutoEventWireup="true" Inherits="login" Codebehind="login.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Alcoa Direct > Login</title>
      <link rel="shortcut icon" href="~/Images/favicon.ico" type="image/x-icon" />
    <script language="javascript" type="text/javascript">
        function prevent_previous_page_return() {
            window.history.forward();
        }
    </script>
    <asp:PlaceHolder ID="phBundling" runat="server">
        <%: System.Web.Optimization.Styles.Render("~/bundles/user_master_styles") %>
    </asp:PlaceHolder>
    <style type="text/css">
        p.MsoListParagraph
        {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 0cm;
            margin-left: 36.0pt;
            margin-bottom: .0001pt;
            font-size: 11.0pt;
            font-family: "Calibri" , "sans-serif";
        }
    </style>
</head>
<body style="text-align: center">
    <form id="form1" runat="server">
        <table align="center" width="900px">
        <tr>
            <td align="center" style="text-align: center; margin-left: auto; margin-right: auto;" width="900px">
                <div style="text-align: center" align="center">
                    <table cellpadding="0" cellspacing="0" width="740px">
                        <tr>
                            <td align="left" style="text-align: left">
                                <img border="0" height="33" src="Images/top_alcoa_logo_wide.gif" width="153" alt="Alcoa"/>
                            </td>
                            <td align="right" style="text-align: right">
                                <span style="color: #003399">Alcoa World Alumina - Australian Operations<br />
                                    <span class="sitetitleblue"><a class="sitetitleblue" href="default.aspx" onfocus="blur(this)">
                                        Contractor Services Management System</a></span> </span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:100%">
                                <br />
        <dx:ASPxLabel EncodeHtml="false" ID="lblStatus" runat="server" Text=""></dx:ASPxLabel>
        <br />
       
        <table id="tblAuth" runat="server" align="center" style="width:740px; text-align:center; padding:10px 10px 10px 10px; border:1px solid gray">
            <tr>
                <td style="width:40%; text-align:right; padding:5px"><span style="font-size: 14px; font-weight:bold; color: #003399; font-family: Arial">Please select your Company:</span></td>
                 <td style="width:30%; text-align:left; padding:5px">
                 <dx:ASPxComboBox ID="ddlCompanies" runat="server" ClientInstanceName="ddlCompanies" ForeColor="Green" Font-Bold="true"
                            Enabled="true" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            IncrementalFilteringMode="StartsWith" EnableSynchronization="False"
                            ValueType="System.Int32" Width="300px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>                            
                            <%--<ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="true"
                             Display="Dynamic" ValidationGroup="i">
                               <RequiredField IsRequired="true" ErrorText="Please Select a Company" />
                            </ValidationSettings>--%>
                        </dx:ASPxComboBox>
                 </td>
                 <td style="padding-left:5px; width:30%">
                      <dx:ASPxButton ID="btnClick" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" OnClick="btnClick_click" Text="Login"
                            Width="99px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        </dx:ASPxButton>
                 </td>
            </tr>
            <tr>
                <td colspan="3">
                <dx:ASPxLabel EncodeHtml="false" ID="lblMessage" Visible="false" Font-Bold="true" ForeColor="Red" runat="server" Text=""></dx:ASPxLabel>
                    
                </td>
            </tr>
        </table>
                            </td>
                        </tr>
                    </table>                    
                </div>
            </td>
        </tr>
        
    </table>
    </form>

    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery_scripts") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/user_master_scripts") %>
</body>
</html>

