﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireActionLogService
    {
        QuestionnaireActionLog Get(Expression<Func<QuestionnaireActionLog, bool>> where, List<Expression<Func<QuestionnaireActionLog, object>>> includes);
        List<QuestionnaireActionLog> GetMany(int? top, Expression<Func<QuestionnaireActionLog, bool>> where, Func<IQueryable<QuestionnaireActionLog>, IOrderedQueryable<QuestionnaireActionLog>> orderBy, List<Expression<Func<QuestionnaireActionLog, object>>> includes);
        List<QuestionnaireActionLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireActionLog, bool>> where, Func<IQueryable<QuestionnaireActionLog>, IOrderedQueryable<QuestionnaireActionLog>> orderBy, List<Expression<Func<QuestionnaireActionLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireActionLog, bool>> where, List<Expression<Func<QuestionnaireActionLog, object>>> includes);
        void Insert(QuestionnaireActionLog item, bool saveChanges = true);
        void Update(QuestionnaireActionLog item, bool saveChanges = true);
        void Delete(QuestionnaireActionLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireActionLogService : IQuestionnaireActionLogService
    {
        private readonly IRepository<QuestionnaireActionLog> _repository;

        public QuestionnaireActionLogService(IRepository<QuestionnaireActionLog> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireActionLog Get(Expression<Func<QuestionnaireActionLog, bool>> where, List<Expression<Func<QuestionnaireActionLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireActionLog> GetMany(int? top, Expression<Func<QuestionnaireActionLog, bool>> where, Func<IQueryable<QuestionnaireActionLog>, IOrderedQueryable<QuestionnaireActionLog>> orderBy, List<Expression<Func<QuestionnaireActionLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireActionLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireActionLog, bool>> where, Func<IQueryable<QuestionnaireActionLog>, IOrderedQueryable<QuestionnaireActionLog>> orderBy, List<Expression<Func<QuestionnaireActionLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireActionLog, bool>> where, List<Expression<Func<QuestionnaireActionLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireActionLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireActionLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireActionLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireActionLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireActionLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireActionLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
