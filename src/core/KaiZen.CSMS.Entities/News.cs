﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'News' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class News : NewsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="News"/> instance.
		///</summary>
		public News():base(){}	
		
		#endregion
	}
}
