using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.IO;
using System.Collections.Generic;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_CSA : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    int green = 0;
    int expected = 0;
    DataSet dsNonEmbeded = new DataSet();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor): 
                    break;
                case ((int)RoleList.Administrator): 
                    break;
                default:
                    // do something
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }
        }
		//Added By Bishwajit for Item#31
        //Added by Bishwajit Sahoo to get Session
        Session["ComplianceReportCSA1"] = null;
        Session["ComplianceReportCSA2"] = null;
		//End Added By Bishwajit for Item#31
            if (SessionHandler.spVar_Page == "ComplianceReport")
            {
                //Added By Sayani Sil for DT2416
                lblEmbeded.Visible = true;
                lblNonEmbeded1.Visible = true;
                grid1.Visible = true;
                //End of change

                grid.Settings.ShowFilterRow = false;
                grid.Settings.ShowGroupPanel = false;
                grid.SettingsBehavior.AllowSort = false;
                grid.SettingsBehavior.AllowGroup = false;

                grid1.SettingsBehavior.AllowSort = false;
                grid1.SettingsBehavior.AllowGroup = false;
                //residentialstatus = SessionHandler.spVar_ResidentialStatus2;

                grid.DataSourceID = null;

                DataSet ds = new DataSet();
//                DataSet dsNonEmbeded = new DataSet();
                
                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                {
                    if (Convert.ToInt32(SessionHandler.spVar_CategoryId) == 1 || Convert.ToInt32(SessionHandler.spVar_CategoryId) == -1)
                    {
                        ds = DataRepository.CsaProvider.ComplianceReport_ByCompany_Embeded(
                                Convert.ToInt32(SessionHandler.spVar_CompanyId),
                                Convert.ToInt32(SessionHandler.spVar_Year),
                                Convert.ToInt32(SessionHandler.spVar_SiteId));

                        grid.Columns["Company Name"].Visible = false;

                        //Added by Sayani Sil

                        grid.DataSource = ds.Tables[0];
						//Added By Bishwajit for Item#31
                        //Get Datatable in the Session added By Bishwajit Sahoo
                        Session["ComplianceReportCSA1"] = ds.Tables[0];
						//End Added By Bishwajit for Item#31
                        grid.DataBind();
                    }

                    //Added By Sayani Sil

                    if (Convert.ToInt32(SessionHandler.spVar_CategoryId) == 2 || Convert.ToInt32(SessionHandler.spVar_CategoryId) == -1)
                    {
                        dsNonEmbeded = DataRepository.CsaProvider.ComplianceReport_ByCompany_NonEmbeded(
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Convert.ToInt32(SessionHandler.spVar_SiteId));
                        grid1.DataSource = dsNonEmbeded;
						//Added By Bishwajit for Item#31
                        Session["ComplianceReportCSA2"] = dsNonEmbeded.Tables[0];
                        //End Added By Bishwajit for Item#31
						grid1.Columns[2].Visible = false;
                        grid1.Columns[3].Visible = false;
                        grid1.Columns[4].Visible = false;
                        grid1.Columns[5].Visible = false;
                        grid1.DataBind();
                    }

                }
                else
                {
                    
                    ds = DataRepository.CsaProvider.ComplianceReport(
                                            Convert.ToInt32(SessionHandler.spVar_Year),
                                            Convert.ToInt32(SessionHandler.spVar_SiteId));
                    grid.Columns["Company Name"].Visible = true;

                    //Added By Sayani
                    grid.DataSource = ds.Tables[0];
					//Added By Bishwajit for Item#31
                    Session["ComplianceReportCSA1"] = ds.Tables[0];
                    //End Added By Bishwajit for Item#31
					grid.DataBind();
                }

                //Commented by Sayani Sil
                /*grid.DataSource = ds.Tables[0];
                grid.DataBind();

                //Added By Sayani
                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                {
                    if (Convert.ToInt32(SessionHandler.spVar_CategoryId) == 2 || Convert.ToInt32(SessionHandler.spVar_CategoryId) == -1)
                    {
                        dsNonEmbeded = DataRepository.CsaProvider.ComplianceReport_ByCompany_NonEmbeded(
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Convert.ToInt32(SessionHandler.spVar_SiteId));
                    }
                }
                else
                {
                }

                grid1.DataSource = dsNonEmbeded;
                grid1.DataBind();*/

                if (green == expected)
                {
                    int a = Convert.ToInt32(SessionHandler.spVar_ComplianceCount);
                    a++;
                    SessionHandler.spVar_ComplianceCount = a.ToString();
                }
            }
            else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
            {
                //grid.Settings.ShowFilterRow = false;
                //grid.Settings.ShowGroupPanel = false;
                //grid.SettingsBehavior.AllowSort = false;
                //grid.SettingsBehavior.AllowGroup = false;


                grid.DataSourceID = null;

                DataSet ds = new DataSet();
                if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                {
                    ds = DataRepository.CsaProvider.ComplianceReport_BySite(
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                            SessionHandler.spVar_CategoryId2);

                    grid.Columns["Site Name"].Visible = false;
                }
                else //Region
                {
                    ds = DataRepository.CsaProvider.ComplianceReport_BySite(
                                            Convert.ToInt32(SessionHandler.spVar_Year),
                                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                                            SessionHandler.spVar_CategoryId2);
                    grid.Columns["Site Name"].Visible = true;
                }

                grid.DataSource = ds.Tables[0];
				//Added By Bishwajit for Item#31
                Session["ComplianceReportCSA1"] = ds.Tables[0];
                //End Added By Bishwajit for Item#31
				grid.DataBind();
            }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        #region "Columns"
        int month;
        if (SessionHandler.spVar_Year == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
        }
        else
        {
            month = 12;
            expected = 4;
        }

       
        
            if (month >= 0)
            {
                #region Qtr 1
                expected = 1;
                if (e.RowType != GridViewRowType.Data) return;
                Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted1") as Label;
                int change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 1");
                if (label != null) label.Text = "";
                System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage1")
                                            as System.Web.UI.WebControls.Image;
                if (img != null) img.Visible = false;
                if (change != 0)
                {
                    if (img != null) img.Visible = true;
                    if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                    if (label != null) label.ForeColor = Color.Green;
                    if (label != null) label.Text = String.Format("({0})", change);
                    green++;
                }
                else
                {
                    if (img != null) img.Visible = true;
                    if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                    if (label != null) label.ForeColor = Color.Red;
                }
                #endregion
                if (month >= 4)
                {
                    #region Qtr 2
                    expected = 2;
                    label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted2") as Label;
                    change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 2");
                    if (label != null) label.Text = "";
                    img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
                                                as System.Web.UI.WebControls.Image;
                    if (img != null) img.Visible = false;
                    if (change != 0)
                    {
                        if (img != null) img.Visible = true;
                        if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                        if (label != null) label.ForeColor = Color.Green;
                        if (label != null) label.Text = String.Format("({0})", change);
                        green++;
                    }
                    else
                    {
                        if (img != null) img.Visible = true;
                        if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                        if (label != null) label.ForeColor = Color.Red;
                    }
                    #endregion
                    if (month >= 7)
                    {
                        #region Qtr 3
                        expected = 3;
                        label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted3") as Label;
                        change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 3");
                        if (label != null) label.Text = "";
                        img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage3")
                                                    as System.Web.UI.WebControls.Image;
                        if (img != null) img.Visible = false;
                        if (change != 0)
                        {
                            if (img != null) img.Visible = true;
                            if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                            if (label != null) label.ForeColor = Color.Green;
                            if (label != null) label.Text = String.Format("({0})", change);
                            green++;
                        }
                        else
                        {
                            if (img != null) img.Visible = true;
                            if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                            if (label != null) label.ForeColor = Color.Red;
                        }
                        #endregion
                        if (month >= 10)
                        {
                            #region Qtr 4
                            expected = 4;
                            label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted4") as Label;
                            change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 4");
                            if (label != null) label.Text = "";
                            img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage4")
                                                        as System.Web.UI.WebControls.Image;
                            if (img != null) img.Visible = false;
                            if (change != 0)
                            {
                                if (img != null) img.Visible = true;
                                if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                                if (label != null) label.ForeColor = Color.Green;
                                if (label != null) label.Text = String.Format("({0})", change);
                                green++;
                            }
                            else
                            {
                                if (img != null) img.Visible = true;
                                if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                                if (label != null) label.ForeColor = Color.Red;
                            }
                            #endregion
                        }
                    }
                }
            }
       // }

        

        #region Alcoa Annual Audit
        Label _label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted5") as Label;
        int _change = (int)grid.GetRowValues(e.VisibleIndex, "Alcoa Audit");
        if (_label != null) _label.Text = "";
        System.Web.UI.WebControls.Image _img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage5")
                                    as System.Web.UI.WebControls.Image;
        if (_img != null) _img.Visible = false;
        if (_change != 0)
        {
            if (_img != null) _img.Visible = true;
            if (_img != null) _img.ImageUrl = "~/Images/greentick.gif";
            if (_label != null) _label.ForeColor = Color.Green;
            if (_label != null) _label.Text = String.Format("({0})", _change);
        }
        else
        {
            if (_img != null) _img.Visible = true;
            if (_img != null) _img.ImageUrl = "~/Images/redcross.gif";
            if (_label != null) _label.ForeColor = Color.Red;
        }
        #endregion
        #endregion
    }


    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
        if (e.Column.FieldName == "SiteId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }

    public int getExpected()
    {
        if (green >= expected)
        { return 1; }
        else { return 0; }
    }

    protected void grid1_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {

        int month;
        if (SessionHandler.spVar_Year == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
        }
        else
        {
            month = 12;
            expected = 4;
        }

        if (month >= 0 || month >= 4)
        {
            #region Half 1

            if (e.RowType != GridViewRowType.Data) return;
            Label label = grid1.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmittedHalf1") as Label;
            int change = 0;
            if (((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 1")) > 0)
            {
                change = ((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 1"));
            }
            else if (((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 2")) > 0)
            {
                change = ((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 2"));
            }
            if (label != null) label.Text = "";
            System.Web.UI.WebControls.Image img = grid1.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage6")
                                        as System.Web.UI.WebControls.Image;
            //img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
            //                                as System.Web.UI.WebControls.Image;
            if (img != null) img.Visible = false;
            if (change != 0)
            {
                if (img != null) img.Visible = true;
                if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                if (label != null) label.ForeColor = Color.Green;
                if (label != null) label.Text = String.Format("({0})", change);
                green++;
            }
            else
            {
                if (img != null) img.Visible = true;
                if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                if (label != null) label.ForeColor = Color.Red;
            }

            #endregion

            if (month >= 7 || month >= 10)
            #region Half 2
            {
                label = grid1.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmittedHalf2") as Label;
                //change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 3") + (int)grid.GetRowValues(e.VisibleIndex, "Qtr 4");

                if (((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 3")) > 0)
                {
                    change = ((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 3"));
                }
                else if (((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 4")) > 0)
                {
                    change = ((int)grid1.GetRowValues(e.VisibleIndex, "Qtr 4"));
                }

                if (label != null) label.Text = "";
                img = grid1.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage7")
                                            as System.Web.UI.WebControls.Image;
                if (img != null) img.Visible = false;
                if (change != 0)
                {
                    if (img != null) img.Visible = true;
                    if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                    if (label != null) label.ForeColor = Color.Green;
                    if (label != null) label.Text = String.Format("({0})", change);
                    green++;
                }
                else
                {
                    if (img != null) img.Visible = true;
                    if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                    if (label != null) label.ForeColor = Color.Red;
                }
            }

            #endregion
        }

       
    }

    public PrintableComponentLink ExportEmbeddedToPdf()
    {
        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
        pcl.Component = gridExport;
        pcl.CreateReportHeaderArea += new CreateAreaEventHandler(pcl_CreateEmbeddedReportHeaderArea);
        pcl.CreateDocument(false);
        return pcl;
    }

    public void pcl_CreateEmbeddedReportHeaderArea(object sender, CreateAreaEventArgs e)
    {
        TextBrick brick1 = e.Graph.DrawString("Contractor Services Audit - Shows Embedded and Non Embedded 1 Sites Only", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        //TextBrick brick1 = e.Graph.DrawString("Contractor Services Audit - Embedded", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }

    public PrintableComponentLink ExportNonEmbedded1ToPdf(PrintingSystem ps)
    {
        PrintableComponentLink pcl = new PrintableComponentLink(ps);
        pcl.Component = grid1Export;
        pcl.CreateReportHeaderArea += new CreateAreaEventHandler(pcl_CreateNonEmbedded1ReportHeaderArea);
        pcl.CreateDocument(false);
        return pcl;
    }

    public void pcl_CreateNonEmbedded1ReportHeaderArea(object sender, CreateAreaEventArgs e)
    {
        TextBrick brick1 = e.Graph.DrawString("Contractor Services Audit - Non Embedded 1", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }

    public string ExportToHtml()
    {
        var sb = new StringBuilder();

        sb.Append("<table width='100%' border='1' style='font-size:9px; color:Black;'>");
        sb.Append("<tbody>");
        sb.Append("<tr>");
        sb.Append("<td width='20%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Residential Category</td>");
        sb.Append("<td width='30%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Company Name</td>");
        sb.Append("<td width='10%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 1</td>");
        sb.Append("<td width='10%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 2</td>");
        sb.Append("<td width='10%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 3</td>");
        sb.Append("<td width='10%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 4</td>");
        sb.Append("<td width='10%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Alcoa Audit</td>");
        sb.Append("</tr>");

        var dt = (DataTable)grid.DataSource;
        if (dt != null && dt.Rows.Count > 0)
        {
            dt.DefaultView.Sort = "CategoryDesc asc, CompanyName asc";
            dt = dt.DefaultView.ToTable();

            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<tr>");
                sb.Append(string.Format("<td width='20%' border='1' align='left' valign='top'>{0}</td>", dr["CategoryDesc"]));
                sb.Append(string.Format("<td width='30%' border='1' align='left' valign='top'>{0}</td>", dr["CompanyName"].ToString()));
                sb.Append(string.Format("<td width='10%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr 1"].ToString(), 1)));
                sb.Append(string.Format("<td width='10%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr 2"].ToString(), 2)));
                sb.Append(string.Format("<td width='10%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr 3"].ToString(), 3)));
                sb.Append(string.Format("<td width='10%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr 4"].ToString(), 4)));
                sb.Append(string.Format("<td width='10%' border='1' align='center' valign='top'>{0}</td>", AlcoaAuditHtml(dr["Alcoa Audit"].ToString())));
            }
        }
        else
        {
            sb.Append("<tr>");
            sb.Append("<td colspan='4'>No Contractor Services Audit - Non Embedded 1 submitted</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");

        return sb.ToString();
    }

    private string QtrHtml(string qtrData, int qtrNumber)
    {
        string html = "";

        int month;
        int qtr = 4;
        string selectedYear = SessionHandler.spVar_Year;

        if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        if (selectedYear == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
            if (month <= 12) qtr = 4;
            if (month <= 9) qtr = 3;
            if (month <= 6) qtr = 2;
            if (month <= 3) qtr = 1;
            expected = qtr;
        }
        else
        {
            month = 12;
            qtr = 4;
        }

        if (qtr >= 1)
        {
            if (qtrNumber == 1)
            {
                if (qtrData != "0")
                {
                    html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", qtrData);
                }
                else
                {
                    html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                }
            }
            else
            {
                if (qtr >= 2)
                {
                    if (qtrNumber == 2)
                    {
                        if (qtrData != "0")
                        {
                            html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", qtrData);
                        }
                        else
                        {
                            html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                        }
                    }
                    else
                    {
                        if (qtr >= 3)
                        {
                            if (qtrNumber == 3)
                            {
                                if (qtrData != "0")
                                {
                                    html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", qtrData);
                                }
                                else
                                {
                                    html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                                }
                            }
                            else
                            {
                                if (qtr >= 4)
                                {
                                    if (qtrNumber == 4)
                                    {
                                        if (qtrData != "0")
                                        {
                                            html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", qtrData);
                                        }
                                        else
                                        {
                                            html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return html;
    }

    private string AlcoaAuditHtml(string audit)
    {
        string html = "";

        if (audit != "0")
        {
            html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", audit);
        }
        else
        {
            html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
        }

        return html;
    }
}
