﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EscalationChainSafetyRolesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EscalationChainSafetyRolesDataSourceDesigner))]
	public class EscalationChainSafetyRolesDataSource : ProviderDataSource<EscalationChainSafetyRoles, EscalationChainSafetyRolesKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesDataSource class.
		/// </summary>
		public EscalationChainSafetyRolesDataSource() : base(new EscalationChainSafetyRolesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EscalationChainSafetyRolesDataSourceView used by the EscalationChainSafetyRolesDataSource.
		/// </summary>
		protected EscalationChainSafetyRolesDataSourceView EscalationChainSafetyRolesView
		{
			get { return ( View as EscalationChainSafetyRolesDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EscalationChainSafetyRolesDataSource control invokes to retrieve data.
		/// </summary>
		public EscalationChainSafetyRolesSelectMethod SelectMethod
		{
			get
			{
				EscalationChainSafetyRolesSelectMethod selectMethod = EscalationChainSafetyRolesSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EscalationChainSafetyRolesSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EscalationChainSafetyRolesDataSourceView class that is to be
		/// used by the EscalationChainSafetyRolesDataSource.
		/// </summary>
		/// <returns>An instance of the EscalationChainSafetyRolesDataSourceView class.</returns>
		protected override BaseDataSourceView<EscalationChainSafetyRoles, EscalationChainSafetyRolesKey> GetNewDataSourceView()
		{
			return new EscalationChainSafetyRolesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EscalationChainSafetyRolesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EscalationChainSafetyRolesDataSourceView : ProviderDataSourceView<EscalationChainSafetyRoles, EscalationChainSafetyRolesKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EscalationChainSafetyRolesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EscalationChainSafetyRolesDataSourceView(EscalationChainSafetyRolesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EscalationChainSafetyRolesDataSource EscalationChainSafetyRolesOwner
		{
			get { return Owner as EscalationChainSafetyRolesDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EscalationChainSafetyRolesSelectMethod SelectMethod
		{
			get { return EscalationChainSafetyRolesOwner.SelectMethod; }
			set { EscalationChainSafetyRolesOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EscalationChainSafetyRolesService EscalationChainSafetyRolesProvider
		{
			get { return Provider as EscalationChainSafetyRolesService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EscalationChainSafetyRoles> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EscalationChainSafetyRoles> results = null;
			EscalationChainSafetyRoles item;
			count = 0;
			
			System.Int32 _escalationChainSafetyRoleId;
			System.Int32 _level;

			switch ( SelectMethod )
			{
				case EscalationChainSafetyRolesSelectMethod.Get:
					EscalationChainSafetyRolesKey entityKey  = new EscalationChainSafetyRolesKey();
					entityKey.Load(values);
					item = EscalationChainSafetyRolesProvider.Get(entityKey);
					results = new TList<EscalationChainSafetyRoles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EscalationChainSafetyRolesSelectMethod.GetAll:
                    results = EscalationChainSafetyRolesProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EscalationChainSafetyRolesSelectMethod.GetPaged:
					results = EscalationChainSafetyRolesProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EscalationChainSafetyRolesSelectMethod.Find:
					if ( FilterParameters != null )
						results = EscalationChainSafetyRolesProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EscalationChainSafetyRolesProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EscalationChainSafetyRolesSelectMethod.GetByEscalationChainSafetyRoleId:
					_escalationChainSafetyRoleId = ( values["EscalationChainSafetyRoleId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EscalationChainSafetyRoleId"], typeof(System.Int32)) : (int)0;
					item = EscalationChainSafetyRolesProvider.GetByEscalationChainSafetyRoleId(_escalationChainSafetyRoleId);
					results = new TList<EscalationChainSafetyRoles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EscalationChainSafetyRolesSelectMethod.GetByLevel:
					_level = ( values["Level"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Level"], typeof(System.Int32)) : (int)0;
					item = EscalationChainSafetyRolesProvider.GetByLevel(_level);
					results = new TList<EscalationChainSafetyRoles>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EscalationChainSafetyRolesSelectMethod.Get || SelectMethod == EscalationChainSafetyRolesSelectMethod.GetByEscalationChainSafetyRoleId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EscalationChainSafetyRoles entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EscalationChainSafetyRolesProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EscalationChainSafetyRoles> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EscalationChainSafetyRolesProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EscalationChainSafetyRolesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EscalationChainSafetyRolesDataSource class.
	/// </summary>
	public class EscalationChainSafetyRolesDataSourceDesigner : ProviderDataSourceDesigner<EscalationChainSafetyRoles, EscalationChainSafetyRolesKey>
	{
		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesDataSourceDesigner class.
		/// </summary>
		public EscalationChainSafetyRolesDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainSafetyRolesSelectMethod SelectMethod
		{
			get { return ((EscalationChainSafetyRolesDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EscalationChainSafetyRolesDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EscalationChainSafetyRolesDataSourceActionList

	/// <summary>
	/// Supports the EscalationChainSafetyRolesDataSourceDesigner class.
	/// </summary>
	internal class EscalationChainSafetyRolesDataSourceActionList : DesignerActionList
	{
		private EscalationChainSafetyRolesDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EscalationChainSafetyRolesDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EscalationChainSafetyRolesDataSourceActionList(EscalationChainSafetyRolesDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EscalationChainSafetyRolesSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EscalationChainSafetyRolesDataSourceActionList
	
	#endregion EscalationChainSafetyRolesDataSourceDesigner
	
	#region EscalationChainSafetyRolesSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EscalationChainSafetyRolesDataSource.SelectMethod property.
	/// </summary>
	public enum EscalationChainSafetyRolesSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEscalationChainSafetyRoleId method.
		/// </summary>
		GetByEscalationChainSafetyRoleId,
		/// <summary>
		/// Represents the GetByLevel method.
		/// </summary>
		GetByLevel
	}
	
	#endregion EscalationChainSafetyRolesSelectMethod

	#region EscalationChainSafetyRolesFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafetyRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyRolesFilter : SqlFilter<EscalationChainSafetyRolesColumn>
	{
	}
	
	#endregion EscalationChainSafetyRolesFilter

	#region EscalationChainSafetyRolesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafetyRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyRolesExpressionBuilder : SqlExpressionBuilder<EscalationChainSafetyRolesColumn>
	{
	}
	
	#endregion EscalationChainSafetyRolesExpressionBuilder	

	#region EscalationChainSafetyRolesProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EscalationChainSafetyRolesChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainSafetyRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainSafetyRolesProperty : ChildEntityProperty<EscalationChainSafetyRolesChildEntityTypes>
	{
	}
	
	#endregion EscalationChainSafetyRolesProperty
}

