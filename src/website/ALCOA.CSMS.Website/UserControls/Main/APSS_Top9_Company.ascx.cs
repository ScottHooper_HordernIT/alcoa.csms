using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class Library_APSS_Top9_Company : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    Hashtable htReadFiles = new Hashtable();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            SessionHandler.ViewMode = "View";
            Session["MR"] = "MR";
        }

        if (Request.QueryString["CompanyId"] != null)
        {
            SessionHandler.spVar_CompanyId = Request.QueryString["CompanyId"].ToString();
            DataView dvReadCD = (DataView)sqldsReadCD.Select(DataSourceSelectArguments.Empty);
            foreach (DataRow dr in dvReadCD.Table.Rows)
            {
                htReadFiles.Add(dr["MustReadDownloaded"].ToString(), "1");
            }

            if(Request.QueryString["CompanyId"] != null)
            {
                ddlCompany.SelectedValue = Request.QueryString["CompanyId"].ToString();
            }

            int _CompanyId = Convert.ToInt32(SessionHandler.spVar_CompanyId);
            RegionsService rService = new RegionsService();
            Regions rList_WAO = rService.GetByRegionInternalName("WAO");
            Regions rList_VICOPS = rService.GetByRegionInternalName("VICOPS");
            int? _RegionId_WAO = rList_WAO.RegionId;
            int? _RegionId_VICOPS = rList_VICOPS.RegionId;
            CompaniesService cService = new CompaniesService();
            DataSet dsCount_v = cService.GetCountSites_ByRegion(_CompanyId, _RegionId_VICOPS);

            int vCount = 0;
            foreach (DataRow dr in dsCount_v.Tables[0].Rows) vCount = Convert.ToInt32(dr[0].ToString());
            DataSet dsCount_w = cService.GetCountSites_ByRegion(_CompanyId, _RegionId_WAO);

            int wCount = 0;
            foreach (DataRow dr in dsCount_w.Tables[0].Rows) wCount = Convert.ToInt32(dr[0].ToString());

            if (wCount == 0 || vCount == 0)
            {
                if (wCount == 0) grid.FilterExpression = "RegionId = " + rList_VICOPS.RegionId.ToString();
                if (vCount == 0) grid.FilterExpression = "RegionId = " + rList_WAO.RegionId.ToString();
            }
            grid.DataBind();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):

                    break;
                case ((int)RoleList.Administrator):
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
        else
        {
            Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Invalid Company."));
        }
    }


    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ApssDocumentType"] = "MR";
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["ApssDocumentType"] = "MR";
    }
    protected void grid_OnRowInserting(object sender, ASPxDataInsertingEventArgs e) //Default Values 
    {
        e.NewValues["ApssDocumentType"] = "MR";
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        

        if (e.RowType != GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblRead") as Label;
        string fileName = (string)grid.GetRowValues(e.VisibleIndex, "DocumentFileName");

        if (htReadFiles.ContainsKey(fileName))
        {
            label.Text = "Read";
            label.ForeColor = System.Drawing.Color.Green;
        }
        else
        {
            label.Text = "UnRead";
            label.ForeColor = System.Drawing.Color.Red;
        }
    }
}
