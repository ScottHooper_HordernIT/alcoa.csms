﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersCompaniesActiveContractorsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersCompaniesActiveContractorsDataSourceDesigner))]
	public class UsersCompaniesActiveContractorsDataSource : ReadOnlyDataSource<UsersCompaniesActiveContractors>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsDataSource class.
		/// </summary>
		public UsersCompaniesActiveContractorsDataSource() : base(new UsersCompaniesActiveContractorsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersCompaniesActiveContractorsDataSourceView used by the UsersCompaniesActiveContractorsDataSource.
		/// </summary>
		protected UsersCompaniesActiveContractorsDataSourceView UsersCompaniesActiveContractorsView
		{
			get { return ( View as UsersCompaniesActiveContractorsDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersCompaniesActiveContractorsDataSourceView class that is to be
		/// used by the UsersCompaniesActiveContractorsDataSource.
		/// </summary>
		/// <returns>An instance of the UsersCompaniesActiveContractorsDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersCompaniesActiveContractors, Object> GetNewDataSourceView()
		{
			return new UsersCompaniesActiveContractorsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersCompaniesActiveContractorsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersCompaniesActiveContractorsDataSourceView : ReadOnlyDataSourceView<UsersCompaniesActiveContractors>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersCompaniesActiveContractorsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersCompaniesActiveContractorsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersCompaniesActiveContractorsDataSourceView(UsersCompaniesActiveContractorsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersCompaniesActiveContractorsDataSource UsersCompaniesActiveContractorsOwner
		{
			get { return Owner as UsersCompaniesActiveContractorsDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersCompaniesActiveContractorsService UsersCompaniesActiveContractorsProvider
		{
			get { return Provider as UsersCompaniesActiveContractorsService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersCompaniesActiveContractorsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersCompaniesActiveContractorsDataSource class.
	/// </summary>
	public class UsersCompaniesActiveContractorsDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersCompaniesActiveContractors>
	{
	}

	#endregion UsersCompaniesActiveContractorsDataSourceDesigner

	#region UsersCompaniesActiveContractorsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveContractorsFilter : SqlFilter<UsersCompaniesActiveContractorsColumn>
	{
	}

	#endregion UsersCompaniesActiveContractorsFilter

	#region UsersCompaniesActiveContractorsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersCompaniesActiveContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersCompaniesActiveContractorsExpressionBuilder : SqlExpressionBuilder<UsersCompaniesActiveContractorsColumn>
	{
	}
	
	#endregion UsersCompaniesActiveContractorsExpressionBuilder		
}

