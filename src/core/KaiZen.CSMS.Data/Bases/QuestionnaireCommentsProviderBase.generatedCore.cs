﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireCommentsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireCommentsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireComments, KaiZen.CSMS.Entities.QuestionnaireCommentsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireCommentsKey key)
		{
			return Delete(transactionManager, key.QuestionnaireCommentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireCommentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireCommentId)
		{
			return Delete(null, _questionnaireCommentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireCommentId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_CreatedByUserId key.
		///		FK_QuestionnaireComments_CreatedByUserId Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_CreatedByUserId key.
		///		FK_QuestionnaireComments_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireComments> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_CreatedByUserId key.
		///		FK_QuestionnaireComments_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_CreatedByUserId key.
		///		fkQuestionnaireCommentsCreatedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_CreatedByUserId key.
		///		fkQuestionnaireCommentsCreatedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_CreatedByUserId key.
		///		FK_QuestionnaireComments_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public abstract TList<QuestionnaireComments> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_ModifedByUserId key.
		///		FK_QuestionnaireComments_ModifedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByModifiedByUserId(System.Int32? _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_ModifedByUserId key.
		///		FK_QuestionnaireComments_ModifedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireComments> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_ModifedByUserId key.
		///		FK_QuestionnaireComments_ModifedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_ModifedByUserId key.
		///		fkQuestionnaireCommentsModifedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByModifiedByUserId(System.Int32? _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_ModifedByUserId key.
		///		fkQuestionnaireCommentsModifedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public TList<QuestionnaireComments> GetByModifiedByUserId(System.Int32? _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireComments_ModifedByUserId key.
		///		FK_QuestionnaireComments_ModifedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireComments objects.</returns>
		public abstract TList<QuestionnaireComments> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32? _modifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireComments Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireCommentsKey key, int start, int pageLength)
		{
			return GetByQuestionnaireCommentId(transactionManager, key.QuestionnaireCommentId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireComments index.
		/// </summary>
		/// <param name="_questionnaireCommentId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireComments GetByQuestionnaireCommentId(System.Int32 _questionnaireCommentId)
		{
			int count = -1;
			return GetByQuestionnaireCommentId(null,_questionnaireCommentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireComments index.
		/// </summary>
		/// <param name="_questionnaireCommentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireComments GetByQuestionnaireCommentId(System.Int32 _questionnaireCommentId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireCommentId(null, _questionnaireCommentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireComments index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireComments GetByQuestionnaireCommentId(TransactionManager transactionManager, System.Int32 _questionnaireCommentId)
		{
			int count = -1;
			return GetByQuestionnaireCommentId(transactionManager, _questionnaireCommentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireComments index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireComments GetByQuestionnaireCommentId(TransactionManager transactionManager, System.Int32 _questionnaireCommentId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireCommentId(transactionManager, _questionnaireCommentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireComments index.
		/// </summary>
		/// <param name="_questionnaireCommentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireComments GetByQuestionnaireCommentId(System.Int32 _questionnaireCommentId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireCommentId(null, _questionnaireCommentId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireComments index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireCommentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireComments GetByQuestionnaireCommentId(TransactionManager transactionManager, System.Int32 _questionnaireCommentId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireComments index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireComments&gt;"/> class.</returns>
		public TList<QuestionnaireComments> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireComments index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireComments&gt;"/> class.</returns>
		public TList<QuestionnaireComments> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireComments index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireComments&gt;"/> class.</returns>
		public TList<QuestionnaireComments> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireComments index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireComments&gt;"/> class.</returns>
		public TList<QuestionnaireComments> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireComments index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireComments&gt;"/> class.</returns>
		public TList<QuestionnaireComments> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireComments index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireComments&gt;"/> class.</returns>
		public abstract TList<QuestionnaireComments> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireComments&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireComments&gt;"/></returns>
		public static TList<QuestionnaireComments> Fill(IDataReader reader, TList<QuestionnaireComments> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireComments c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireComments")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireCommentsColumn.QuestionnaireCommentId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireComments>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireComments",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireComments();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireCommentId = (System.Int32)reader[((int)QuestionnaireCommentsColumn.QuestionnaireCommentId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireCommentsColumn.QuestionnaireId - 1)];
					c.Comment = (System.String)reader[((int)QuestionnaireCommentsColumn.Comment - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireCommentsColumn.CreatedByUserId - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireCommentsColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsColumn.ModifiedByUserId - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)QuestionnaireCommentsColumn.CreatedDate - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireCommentsColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireCommentsColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireComments entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireCommentId = (System.Int32)reader[((int)QuestionnaireCommentsColumn.QuestionnaireCommentId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireCommentsColumn.QuestionnaireId - 1)];
			entity.Comment = (System.String)reader[((int)QuestionnaireCommentsColumn.Comment - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireCommentsColumn.CreatedByUserId - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireCommentsColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireCommentsColumn.ModifiedByUserId - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)QuestionnaireCommentsColumn.CreatedDate - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireCommentsColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireCommentsColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireComments entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireCommentId = (System.Int32)dataRow["QuestionnaireCommentId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.Comment = (System.String)dataRow["Comment"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireComments"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireComments Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireComments entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ModifiedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.ModifiedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireComments object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireComments instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireComments Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireComments entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireCommentsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireComments</c>
	///</summary>
	public enum QuestionnaireCommentsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
		}
	
	#endregion QuestionnaireCommentsChildEntityTypes
	
	#region QuestionnaireCommentsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireCommentsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsFilterBuilder : SqlFilterBuilder<QuestionnaireCommentsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsFilterBuilder class.
		/// </summary>
		public QuestionnaireCommentsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsFilterBuilder
	
	#region QuestionnaireCommentsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireCommentsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireCommentsParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireCommentsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsParameterBuilder class.
		/// </summary>
		public QuestionnaireCommentsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireCommentsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireCommentsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireCommentsParameterBuilder
	
	#region QuestionnaireCommentsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireCommentsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireComments"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireCommentsSortBuilder : SqlSortBuilder<QuestionnaireCommentsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireCommentsSqlSortBuilder class.
		/// </summary>
		public QuestionnaireCommentsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireCommentsSortBuilder
	
} // end namespace
