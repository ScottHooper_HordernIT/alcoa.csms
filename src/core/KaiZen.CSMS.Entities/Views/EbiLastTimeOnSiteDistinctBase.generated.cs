﻿/*
	File generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file EbiLastTimeOnSiteDistinct.cs instead.
*/
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Runtime.Serialization;
using System.Xml.Serialization;
#endregion

namespace KaiZen.CSMS.Entities
{
	///<summary>
	/// An object representation of the 'EbiLastTimeOnSiteDistinct' view. [No description found in the database]	
	///</summary>
	[Serializable]
	[CLSCompliant(true)]
	[ToolboxItem("EbiLastTimeOnSiteDistinctBase")]
	public abstract partial class EbiLastTimeOnSiteDistinctBase : System.IComparable, System.ICloneable, INotifyPropertyChanged
	{
		
		#region Variable Declarations
		
		/// <summary>
		/// CompanyName : 
		/// </summary>
		private System.String		  _companyName = null;
		
		/// <summary>
		/// SwipeSite : 
		/// </summary>
		private System.String		  _swipeSite = string.Empty;
		
		/// <summary>
		/// SwipeDateTime : 
		/// </summary>
		private System.DateTime		  _swipeDateTime = DateTime.MinValue;
		
		/// <summary>
		/// Object that contains data to associate with this object
		/// </summary>
		private object _tag;
		
		/// <summary>
		/// Suppresses Entity Events from Firing, 
		/// useful when loading the entities from the database.
		/// </summary>
	    [NonSerialized] 
		private bool suppressEntityEvents = false;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="EbiLastTimeOnSiteDistinctBase"/> instance.
		///</summary>
		public EbiLastTimeOnSiteDistinctBase()
		{
		}		
		
		///<summary>
		/// Creates a new <see cref="EbiLastTimeOnSiteDistinctBase"/> instance.
		///</summary>
		///<param name="_companyName"></param>
		///<param name="_swipeSite"></param>
		///<param name="_swipeDateTime"></param>
		public EbiLastTimeOnSiteDistinctBase(System.String _companyName, System.String _swipeSite, System.DateTime _swipeDateTime)
		{
			this._companyName = _companyName;
			this._swipeSite = _swipeSite;
			this._swipeDateTime = _swipeDateTime;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="EbiLastTimeOnSiteDistinct"/> instance.
		///</summary>
		///<param name="_companyName"></param>
		///<param name="_swipeSite"></param>
		///<param name="_swipeDateTime"></param>
		public static EbiLastTimeOnSiteDistinct CreateEbiLastTimeOnSiteDistinct(System.String _companyName, System.String _swipeSite, System.DateTime _swipeDateTime)
		{
			EbiLastTimeOnSiteDistinct newEbiLastTimeOnSiteDistinct = new EbiLastTimeOnSiteDistinct();
			newEbiLastTimeOnSiteDistinct.CompanyName = _companyName;
			newEbiLastTimeOnSiteDistinct.SwipeSite = _swipeSite;
			newEbiLastTimeOnSiteDistinct.SwipeDateTime = _swipeDateTime;
			return newEbiLastTimeOnSiteDistinct;
		}
				
		#endregion Constructors
		
		#region Properties	
		/// <summary>
		/// 	Gets or Sets the CompanyName property. 
		///		
		/// </summary>
		/// <value>This type is varchar</value>
		/// <remarks>
		/// This property can be set to null. 
		/// </remarks>
		[XmlElement(IsNullable=true)]
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.String CompanyName
		{
			get
			{
				return this._companyName; 
			}
			set
			{
				if (_companyName == value)
					return;
					
				this._companyName = value;
				this._isDirty = true;
				
				OnPropertyChanged("CompanyName");
			}
		}
		
		/// <summary>
		/// 	Gets or Sets the SwipeSite property. 
		///		
		/// </summary>
		/// <value>This type is varchar</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		/// <exception cref="ArgumentNullException">If you attempt to set to null.</exception>
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.String SwipeSite
		{
			get
			{
				return this._swipeSite; 
			}
			set
			{
				if ( value == null )
					throw new ArgumentNullException("value", "SwipeSite does not allow null values.");
				if (_swipeSite == value)
					return;
					
				this._swipeSite = value;
				this._isDirty = true;
				
				OnPropertyChanged("SwipeSite");
			}
		}
		
		/// <summary>
		/// 	Gets or Sets the SwipeDateTime property. 
		///		
		/// </summary>
		/// <value>This type is datetime</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		[DescriptionAttribute(""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		public virtual System.DateTime SwipeDateTime
		{
			get
			{
				return this._swipeDateTime; 
			}
			set
			{
				if (_swipeDateTime == value)
					return;
					
				this._swipeDateTime = value;
				this._isDirty = true;
				
				OnPropertyChanged("SwipeDateTime");
			}
		}
		
		
		/// <summary>
		///     Gets or sets the object that contains supplemental data about this object.
		/// </summary>
		/// <value>Object</value>
		[System.ComponentModel.Bindable(false)]
		[LocalizableAttribute(false)]
		[DescriptionAttribute("Object containing data to be associated with this object")]
		public virtual object Tag
		{
			get
			{
				return this._tag;
			}
			set
			{
				if (this._tag == value)
					return;
		
				this._tag = value;
			}
		}
	
		/// <summary>
		/// Determines whether this entity is to suppress events while set to true.
		/// </summary>
		[System.ComponentModel.Bindable(false)]
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public bool SuppressEntityEvents
		{	
			get
			{
				return suppressEntityEvents;
			}
			set
			{
				suppressEntityEvents = value;
			}	
		}

		private bool _isDeleted = false;
		/// <summary>
		/// Gets a value indicating if object has been <see cref="MarkToDelete"/>. ReadOnly.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsDeleted
		{
			get { return this._isDeleted; }
		}


		private bool _isDirty = false;
		/// <summary>
		///	Gets a value indicating  if the object has been modified from its original state.
		/// </summary>
		///<value>True if object has been modified from its original state; otherwise False;</value>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsDirty
		{
			get { return this._isDirty; }
		}
		

		private bool _isNew = true;
		/// <summary>
		///	Gets a value indicating if the object is new.
		/// </summary>
		///<value>True if objectis new; otherwise False;</value>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public virtual bool IsNew
		{
			get { return this._isNew; }
			set { this._isNew = value; }
		}

		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public string ViewName
		{
			get { return "EbiLastTimeOnSiteDistinct"; }
		}

		
		#endregion
		
		#region Methods	
		
		/// <summary>
		/// Accepts the changes made to this object by setting each flags to false.
		/// </summary>
		public virtual void AcceptChanges()
		{
			this._isDeleted = false;
			this._isDirty = false;
			this._isNew = false;
			OnPropertyChanged(string.Empty);
		}
		
		
		///<summary>
		///  Revert all changes and restore original values.
		///  Currently not supported.
		///</summary>
		/// <exception cref="NotSupportedException">This method is not currently supported and always throws this exception.</exception>
		public virtual void CancelChanges()
		{
			throw new NotSupportedException("Method currently not Supported.");
		}
		
		///<summary>
		///   Marks entity to be deleted.
		///</summary>
		public virtual void MarkToDelete()
		{
			this._isDeleted = true;
		}
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed EbiLastTimeOnSiteDistinctBase Entity 
		///</summary>
		public virtual EbiLastTimeOnSiteDistinctBase Copy()
		{
			//shallow copy entity
			EbiLastTimeOnSiteDistinct copy = new EbiLastTimeOnSiteDistinct();
				copy.CompanyName = this.CompanyName;
				copy.SwipeSite = this.SwipeSite;
				copy.SwipeDateTime = this.SwipeDateTime;
			copy.AcceptChanges();
			return (EbiLastTimeOnSiteDistinct)copy;
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Deep Copy of this entity.
		///</summary>
		public object Clone(){
			return this.Copy();
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		#endregion
		
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="EbiLastTimeOnSiteDistinctBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(EbiLastTimeOnSiteDistinctBase toObject)
		{
			if (toObject == null)
				return false;
			return Equals(this, toObject);
		}
		
		
		///<summary>
		/// Determines whether the specified <see cref="EbiLastTimeOnSiteDistinctBase"/> instances are considered equal.
		///</summary>
		///<param name="Object1">The first <see cref="EbiLastTimeOnSiteDistinctBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="EbiLastTimeOnSiteDistinctBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool Equals(EbiLastTimeOnSiteDistinctBase Object1, EbiLastTimeOnSiteDistinctBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;

			bool equal = true;
			if (Object1.CompanyName != null && Object2.CompanyName != null )
			{
				if (Object1.CompanyName != Object2.CompanyName)
					equal = false;
			}
			else if (Object1.CompanyName == null ^ Object1.CompanyName == null )
			{
				equal = false;
			}
			if (Object1.SwipeSite != Object2.SwipeSite)
				equal = false;
			if (Object1.SwipeDateTime != Object2.SwipeDateTime)
				equal = false;
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
		}
	
		#endregion
		
		#region INotifyPropertyChanged Members
		
		/// <summary>
      /// Event to indicate that a property has changed.
      /// </summary>
		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
      /// Called when a property is changed
      /// </summary>
      /// <param name="propertyName">The name of the property that has changed.</param>
		protected virtual void OnPropertyChanged(string propertyName)
		{ 
			OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
		}
		
		/// <summary>
      /// Called when a property is changed
      /// </summary>
      /// <param name="e">PropertyChangedEventArgs</param>
		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			if (!SuppressEntityEvents)
			{
				if (null != PropertyChanged)
				{
					PropertyChanged(this, e);
				}
			}
		}
		
		#endregion
				
		/// <summary>
		/// Gets the property value by name.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetPropertyValueByName(EbiLastTimeOnSiteDistinct entity, string propertyName)
		{
			switch (propertyName)
			{
				case "CompanyName":
					return entity.CompanyName;
				case "SwipeSite":
					return entity.SwipeSite;
				case "SwipeDateTime":
					return entity.SwipeDateTime;
			}
			return null;
		}
				
		/// <summary>
		/// Gets the property value by name.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public object GetPropertyValueByName(string propertyName)
		{			
			return GetPropertyValueByName(this as EbiLastTimeOnSiteDistinct, propertyName);
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{4}{3}- CompanyName: {0}{3}- SwipeSite: {1}{3}- SwipeDateTime: {2}{3}", 
				(this.CompanyName == null) ? string.Empty : this.CompanyName.ToString(),
			     
				this.SwipeSite,
				this.SwipeDateTime,
				System.Environment.NewLine, 
				this.GetType());
		}
	
	}//End Class
	
	
	/// <summary>
	/// Enumerate the EbiLastTimeOnSiteDistinct columns.
	/// </summary>
	[Serializable]
	public enum EbiLastTimeOnSiteDistinctColumn
	{
		/// <summary>
		/// CompanyName : 
		/// </summary>
		[EnumTextValue("CompanyName")]
		[ColumnEnum("CompanyName", typeof(System.String), System.Data.DbType.AnsiString, false, false, true, 255)]
		CompanyName,
		/// <summary>
		/// SwipeSite : 
		/// </summary>
		[EnumTextValue("SwipeSite")]
		[ColumnEnum("SwipeSite", typeof(System.String), System.Data.DbType.AnsiString, false, false, false, 255)]
		SwipeSite,
		/// <summary>
		/// SwipeDateTime : 
		/// </summary>
		[EnumTextValue("SwipeDateTime")]
		[ColumnEnum("SwipeDateTime", typeof(System.DateTime), System.Data.DbType.DateTime, false, false, false)]
		SwipeDateTime
	}//End enum

} // end namespace
