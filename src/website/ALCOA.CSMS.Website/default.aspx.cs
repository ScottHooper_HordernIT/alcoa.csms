﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _default : System.Web.UI.Page
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!auth.isAlcoaDirect)
        {
            if (SessionHandler.LoggedIn != "Yes")
            {
                Response.Redirect("login.aspx");
            }
        }
        else
        {
            if (SessionHandler.LoggedIn != "Yes")
            {
                Response.StatusCode = 401;
            }
        }
    }
}
