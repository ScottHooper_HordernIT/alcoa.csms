﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.RegionsSitesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(RegionsSitesDataSourceDesigner))]
	public class RegionsSitesDataSource : ProviderDataSource<RegionsSites, RegionsSitesKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesDataSource class.
		/// </summary>
		public RegionsSitesDataSource() : base(new RegionsSitesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the RegionsSitesDataSourceView used by the RegionsSitesDataSource.
		/// </summary>
		protected RegionsSitesDataSourceView RegionsSitesView
		{
			get { return ( View as RegionsSitesDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the RegionsSitesDataSource control invokes to retrieve data.
		/// </summary>
		public RegionsSitesSelectMethod SelectMethod
		{
			get
			{
				RegionsSitesSelectMethod selectMethod = RegionsSitesSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (RegionsSitesSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the RegionsSitesDataSourceView class that is to be
		/// used by the RegionsSitesDataSource.
		/// </summary>
		/// <returns>An instance of the RegionsSitesDataSourceView class.</returns>
		protected override BaseDataSourceView<RegionsSites, RegionsSitesKey> GetNewDataSourceView()
		{
			return new RegionsSitesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the RegionsSitesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class RegionsSitesDataSourceView : ProviderDataSourceView<RegionsSites, RegionsSitesKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the RegionsSitesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public RegionsSitesDataSourceView(RegionsSitesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal RegionsSitesDataSource RegionsSitesOwner
		{
			get { return Owner as RegionsSitesDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal RegionsSitesSelectMethod SelectMethod
		{
			get { return RegionsSitesOwner.SelectMethod; }
			set { RegionsSitesOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal RegionsSitesService RegionsSitesProvider
		{
			get { return Provider as RegionsSitesService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<RegionsSites> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<RegionsSites> results = null;
			RegionsSites item;
			count = 0;
			
			System.Int32 _regionSiteId;
			System.Int32 _siteId;
			System.Int32 _regionId;

			switch ( SelectMethod )
			{
				case RegionsSitesSelectMethod.Get:
					RegionsSitesKey entityKey  = new RegionsSitesKey();
					entityKey.Load(values);
					item = RegionsSitesProvider.Get(entityKey);
					results = new TList<RegionsSites>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case RegionsSitesSelectMethod.GetAll:
                    results = RegionsSitesProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case RegionsSitesSelectMethod.GetPaged:
					results = RegionsSitesProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case RegionsSitesSelectMethod.Find:
					if ( FilterParameters != null )
						results = RegionsSitesProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = RegionsSitesProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case RegionsSitesSelectMethod.GetByRegionSiteId:
					_regionSiteId = ( values["RegionSiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionSiteId"], typeof(System.Int32)) : (int)0;
					item = RegionsSitesProvider.GetByRegionSiteId(_regionSiteId);
					results = new TList<RegionsSites>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case RegionsSitesSelectMethod.GetBySiteIdRegionId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					_regionId = ( values["RegionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionId"], typeof(System.Int32)) : (int)0;
					item = RegionsSitesProvider.GetBySiteIdRegionId(_siteId, _regionId);
					results = new TList<RegionsSites>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case RegionsSitesSelectMethod.GetByRegionId:
					_regionId = ( values["RegionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionId"], typeof(System.Int32)) : (int)0;
					results = RegionsSitesProvider.GetByRegionId(_regionId, this.StartIndex, this.PageSize, out count);
					break;
				case RegionsSitesSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = RegionsSitesProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == RegionsSitesSelectMethod.Get || SelectMethod == RegionsSitesSelectMethod.GetByRegionSiteId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				RegionsSites entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					RegionsSitesProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<RegionsSites> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			RegionsSitesProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region RegionsSitesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the RegionsSitesDataSource class.
	/// </summary>
	public class RegionsSitesDataSourceDesigner : ProviderDataSourceDesigner<RegionsSites, RegionsSitesKey>
	{
		/// <summary>
		/// Initializes a new instance of the RegionsSitesDataSourceDesigner class.
		/// </summary>
		public RegionsSitesDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public RegionsSitesSelectMethod SelectMethod
		{
			get { return ((RegionsSitesDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new RegionsSitesDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region RegionsSitesDataSourceActionList

	/// <summary>
	/// Supports the RegionsSitesDataSourceDesigner class.
	/// </summary>
	internal class RegionsSitesDataSourceActionList : DesignerActionList
	{
		private RegionsSitesDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the RegionsSitesDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public RegionsSitesDataSourceActionList(RegionsSitesDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public RegionsSitesSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion RegionsSitesDataSourceActionList
	
	#endregion RegionsSitesDataSourceDesigner
	
	#region RegionsSitesSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the RegionsSitesDataSource.SelectMethod property.
	/// </summary>
	public enum RegionsSitesSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByRegionSiteId method.
		/// </summary>
		GetByRegionSiteId,
		/// <summary>
		/// Represents the GetBySiteIdRegionId method.
		/// </summary>
		GetBySiteIdRegionId,
		/// <summary>
		/// Represents the GetByRegionId method.
		/// </summary>
		GetByRegionId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion RegionsSitesSelectMethod

	#region RegionsSitesFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesFilter : SqlFilter<RegionsSitesColumn>
	{
	}
	
	#endregion RegionsSitesFilter

	#region RegionsSitesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesExpressionBuilder : SqlExpressionBuilder<RegionsSitesColumn>
	{
	}
	
	#endregion RegionsSitesExpressionBuilder	

	#region RegionsSitesProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;RegionsSitesChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesProperty : ChildEntityProperty<RegionsSitesChildEntityTypes>
	{
	}
	
	#endregion RegionsSitesProperty
}

