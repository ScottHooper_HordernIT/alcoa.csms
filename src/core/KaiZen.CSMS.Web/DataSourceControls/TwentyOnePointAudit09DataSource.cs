﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit09Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit09DataSourceDesigner))]
	public class TwentyOnePointAudit09DataSource : ProviderDataSource<TwentyOnePointAudit09, TwentyOnePointAudit09Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09DataSource class.
		/// </summary>
		public TwentyOnePointAudit09DataSource() : base(new TwentyOnePointAudit09Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit09DataSourceView used by the TwentyOnePointAudit09DataSource.
		/// </summary>
		protected TwentyOnePointAudit09DataSourceView TwentyOnePointAudit09View
		{
			get { return ( View as TwentyOnePointAudit09DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit09DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit09SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit09SelectMethod selectMethod = TwentyOnePointAudit09SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit09SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit09DataSourceView class that is to be
		/// used by the TwentyOnePointAudit09DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit09DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit09, TwentyOnePointAudit09Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit09DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit09DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit09DataSourceView : ProviderDataSourceView<TwentyOnePointAudit09, TwentyOnePointAudit09Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit09DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit09DataSourceView(TwentyOnePointAudit09DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit09DataSource TwentyOnePointAudit09Owner
		{
			get { return Owner as TwentyOnePointAudit09DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit09SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit09Owner.SelectMethod; }
			set { TwentyOnePointAudit09Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit09Service TwentyOnePointAudit09Provider
		{
			get { return Provider as TwentyOnePointAudit09Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit09> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit09> results = null;
			TwentyOnePointAudit09 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit09SelectMethod.Get:
					TwentyOnePointAudit09Key entityKey  = new TwentyOnePointAudit09Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit09Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit09>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit09SelectMethod.GetAll:
                    results = TwentyOnePointAudit09Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit09SelectMethod.GetPaged:
					results = TwentyOnePointAudit09Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit09SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit09Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit09Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit09SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit09Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit09>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit09SelectMethod.Get || SelectMethod == TwentyOnePointAudit09SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit09 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit09Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit09> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit09Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit09DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit09DataSource class.
	/// </summary>
	public class TwentyOnePointAudit09DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit09, TwentyOnePointAudit09Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit09DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit09SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit09DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit09DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit09DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit09DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit09DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit09DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit09DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit09DataSourceActionList(TwentyOnePointAudit09DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit09SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit09DataSourceActionList
	
	#endregion TwentyOnePointAudit09DataSourceDesigner
	
	#region TwentyOnePointAudit09SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit09DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit09SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit09SelectMethod

	#region TwentyOnePointAudit09Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit09"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit09Filter : SqlFilter<TwentyOnePointAudit09Column>
	{
	}
	
	#endregion TwentyOnePointAudit09Filter

	#region TwentyOnePointAudit09ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit09"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit09ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit09Column>
	{
	}
	
	#endregion TwentyOnePointAudit09ExpressionBuilder	

	#region TwentyOnePointAudit09Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit09ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit09"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit09Property : ChildEntityProperty<TwentyOnePointAudit09ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit09Property
}

