﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Configuration;
using System.Net.Mail;

namespace ALCOA.CSMS.Batch.Common.Log
{
    public static class ExtensionMethods
    {
        public static DateTime RemoveDST(this DateTime value)
        {
            //This should only ever happen on workstations outside of the ANG network
            if (DateTime.Now.IsDaylightSavingTime())
                return value.AddHours(-1);
            else //No Daylight Saving
                return value;
        }

        public static DateTime EndOfDay(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day, 23, 59, 59, 999);
        }

        public static DateTime StartOfDay(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day, 0, 0, 0, 0);
        }

        public static DateTime GetAestTime(this DateTime tradingDate, int interval)
        {
            TimeSpan result = new TimeSpan(4, 30, 0);
            TimeSpan timeStep = new TimeSpan(0, 30, 0);

            for (int i = 0; i < interval - 1; i++)
            {
                result = result.Add(timeStep);
            }

            //The interval date can roll over into tomorrow, but we still submit for today's "trading date". Fucking stupid bloody system.
            DateTime intervalDate = tradingDate + result;

            return new DateTime(intervalDate.Year, intervalDate.Month, intervalDate.Day, intervalDate.Hour, intervalDate.Minute, intervalDate.Second);
        }

        public static DateTime GetTradingDate(this DateTime value)
        {
            TimeSpan startOfTrading = new TimeSpan(4, 0, 0);
            TimeSpan midnight = new TimeSpan(0, 0, 0);
            if (value.TimeOfDay < startOfTrading && value.TimeOfDay >= midnight)
            {
                value = value.AddDays(-1);
            }
            return new DateTime(value.Year, value.Month, value.Day);
        }

        //Because LINQ does not support comparison of the DateTime.Date property bloody stupid thing
        public static DateTime DateOnly(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day);
        }

        //Every 5 minutes
        public static DateTime GetNextDispatch(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, 0).AddMinutes(5 - value.Minute % 5); //Strip out seconds and milliseconds and add remainder of 5 minutes
        }

        public static int? ToNullableInt32(this string value)
        {
            if (value.Length == 0)
                return null;
            else
                return Convert.ToInt32(value);
        }

        public static DateTime? ToNullableDateTime(this string value)
        {
            if (value.Length == 0)
                return null;
            else
                return Convert.ToDateTime(value);
        }

        public static long? ToNullableLong(this string value)
        {
            if (value.Length == 0)
                return null;
            else
                return Convert.ToInt64(value);
        }

        public static double? ToNullableDouble(this string value)
        {
            if (value.Length == 0)
                return null;
            else
                return Convert.ToDouble(value);
        }
    }
}
