﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit10ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit10ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit10, KaiZen.CSMS.Entities.TwentyOnePointAudit10Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit10Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit10 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit10Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_10 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit10 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_10 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit10 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_10 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit10 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_10 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit10 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_10 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit10 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_10 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit10 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit10&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit10&gt;"/></returns>
		public static TList<TwentyOnePointAudit10> Fill(IDataReader reader, TList<TwentyOnePointAudit10> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit10 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit10")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit10Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit10>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit10",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit10();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit10Column.Id - 1)];
					c.Achieved10a = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10a - 1)];
					c.Achieved10b = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10b - 1)];
					c.Achieved10c = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10c - 1)];
					c.Achieved10d = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10d - 1)];
					c.Achieved10e = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10e - 1)];
					c.Achieved10f = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10f - 1)];
					c.Achieved10g = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10g - 1)];
					c.Achieved10h = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10h - 1)];
					c.Observation10a = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10a - 1)];
					c.Observation10b = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10b - 1)];
					c.Observation10c = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10c - 1)];
					c.Observation10d = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10d - 1)];
					c.Observation10e = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10e - 1)];
					c.Observation10f = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10f - 1)];
					c.Observation10g = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10g - 1)];
					c.Observation10h = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10h - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit10 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit10Column.Id - 1)];
			entity.Achieved10a = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10a - 1)];
			entity.Achieved10b = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10b - 1)];
			entity.Achieved10c = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10c - 1)];
			entity.Achieved10d = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10d - 1)];
			entity.Achieved10e = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10e - 1)];
			entity.Achieved10f = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10f - 1)];
			entity.Achieved10g = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10g - 1)];
			entity.Achieved10h = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Achieved10h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.Achieved10h - 1)];
			entity.Observation10a = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10a - 1)];
			entity.Observation10b = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10b - 1)];
			entity.Observation10c = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10c - 1)];
			entity.Observation10d = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10d - 1)];
			entity.Observation10e = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10e - 1)];
			entity.Observation10f = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10f - 1)];
			entity.Observation10g = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10g - 1)];
			entity.Observation10h = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.Observation10h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit10Column.Observation10h - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit10Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit10Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit10 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved10a = Convert.IsDBNull(dataRow["Achieved10a"]) ? null : (System.Int32?)dataRow["Achieved10a"];
			entity.Achieved10b = Convert.IsDBNull(dataRow["Achieved10b"]) ? null : (System.Int32?)dataRow["Achieved10b"];
			entity.Achieved10c = Convert.IsDBNull(dataRow["Achieved10c"]) ? null : (System.Int32?)dataRow["Achieved10c"];
			entity.Achieved10d = Convert.IsDBNull(dataRow["Achieved10d"]) ? null : (System.Int32?)dataRow["Achieved10d"];
			entity.Achieved10e = Convert.IsDBNull(dataRow["Achieved10e"]) ? null : (System.Int32?)dataRow["Achieved10e"];
			entity.Achieved10f = Convert.IsDBNull(dataRow["Achieved10f"]) ? null : (System.Int32?)dataRow["Achieved10f"];
			entity.Achieved10g = Convert.IsDBNull(dataRow["Achieved10g"]) ? null : (System.Int32?)dataRow["Achieved10g"];
			entity.Achieved10h = Convert.IsDBNull(dataRow["Achieved10h"]) ? null : (System.Int32?)dataRow["Achieved10h"];
			entity.Observation10a = Convert.IsDBNull(dataRow["Observation10a"]) ? null : (System.String)dataRow["Observation10a"];
			entity.Observation10b = Convert.IsDBNull(dataRow["Observation10b"]) ? null : (System.String)dataRow["Observation10b"];
			entity.Observation10c = Convert.IsDBNull(dataRow["Observation10c"]) ? null : (System.String)dataRow["Observation10c"];
			entity.Observation10d = Convert.IsDBNull(dataRow["Observation10d"]) ? null : (System.String)dataRow["Observation10d"];
			entity.Observation10e = Convert.IsDBNull(dataRow["Observation10e"]) ? null : (System.String)dataRow["Observation10e"];
			entity.Observation10f = Convert.IsDBNull(dataRow["Observation10f"]) ? null : (System.String)dataRow["Observation10f"];
			entity.Observation10g = Convert.IsDBNull(dataRow["Observation10g"]) ? null : (System.String)dataRow["Observation10g"];
			entity.Observation10h = Convert.IsDBNull(dataRow["Observation10h"]) ? null : (System.String)dataRow["Observation10h"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit10"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit10 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit10 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint10Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit10 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit10 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit10 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit10 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point10IdSource != null)
						{
							child.Point10Id = child.Point10IdSource.Id;
						}
						else
						{
							child.Point10Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit10ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit10</c>
	///</summary>
	public enum TwentyOnePointAudit10ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit10</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit10ChildEntityTypes
	
	#region TwentyOnePointAudit10FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit10Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit10Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit10FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit10FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit10FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit10FilterBuilder
	
	#region TwentyOnePointAudit10ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit10Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit10ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit10Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit10ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit10ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit10ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit10ParameterBuilder
	
	#region TwentyOnePointAudit10SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit10Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit10"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit10SortBuilder : SqlSortBuilder<TwentyOnePointAudit10Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit10SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit10SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit10SortBuilder
	
} // end namespace
