﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersProcurementListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersProcurementListDataSourceDesigner))]
	public class UsersProcurementListDataSource : ReadOnlyDataSource<UsersProcurementList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListDataSource class.
		/// </summary>
		public UsersProcurementListDataSource() : base(new UsersProcurementListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersProcurementListDataSourceView used by the UsersProcurementListDataSource.
		/// </summary>
		protected UsersProcurementListDataSourceView UsersProcurementListView
		{
			get { return ( View as UsersProcurementListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersProcurementListDataSourceView class that is to be
		/// used by the UsersProcurementListDataSource.
		/// </summary>
		/// <returns>An instance of the UsersProcurementListDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersProcurementList, Object> GetNewDataSourceView()
		{
			return new UsersProcurementListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersProcurementListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersProcurementListDataSourceView : ReadOnlyDataSourceView<UsersProcurementList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersProcurementListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersProcurementListDataSourceView(UsersProcurementListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersProcurementListDataSource UsersProcurementListOwner
		{
			get { return Owner as UsersProcurementListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersProcurementListService UsersProcurementListProvider
		{
			get { return Provider as UsersProcurementListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersProcurementListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersProcurementListDataSource class.
	/// </summary>
	public class UsersProcurementListDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersProcurementList>
	{
	}

	#endregion UsersProcurementListDataSourceDesigner

	#region UsersProcurementListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListFilter : SqlFilter<UsersProcurementListColumn>
	{
	}

	#endregion UsersProcurementListFilter

	#region UsersProcurementListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListExpressionBuilder : SqlExpressionBuilder<UsersProcurementListColumn>
	{
	}
	
	#endregion UsersProcurementListExpressionBuilder		
}

