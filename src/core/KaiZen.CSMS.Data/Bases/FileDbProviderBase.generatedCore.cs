﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileDbProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FileDbProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.FileDb, KaiZen.CSMS.Entities.FileDbKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbKey key)
		{
			return Delete(transactionManager, key.FileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_fileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _fileId)
		{
			return Delete(null, _fileId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _fileId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_User_ModifiedBy key.
		///		FK_FileDb_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_User_ModifiedBy key.
		///		FK_FileDb_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		/// <remarks></remarks>
		public TList<FileDb> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_User_ModifiedBy key.
		///		FK_FileDb_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_User_ModifiedBy key.
		///		fkFileDbUserModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_User_ModifiedBy key.
		///		fkFileDbUserModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_User_ModifiedBy key.
		///		FK_FileDb_User_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public abstract TList<FileDb> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_Users_StatusModifiedBy key.
		///		FK_FileDb_Users_StatusModifiedBy Description: 
		/// </summary>
		/// <param name="_statusModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByStatusModifiedByUserId(System.Int32? _statusModifiedByUserId)
		{
			int count = -1;
			return GetByStatusModifiedByUserId(_statusModifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_Users_StatusModifiedBy key.
		///		FK_FileDb_Users_StatusModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusModifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		/// <remarks></remarks>
		public TList<FileDb> GetByStatusModifiedByUserId(TransactionManager transactionManager, System.Int32? _statusModifiedByUserId)
		{
			int count = -1;
			return GetByStatusModifiedByUserId(transactionManager, _statusModifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_Users_StatusModifiedBy key.
		///		FK_FileDb_Users_StatusModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByStatusModifiedByUserId(TransactionManager transactionManager, System.Int32? _statusModifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusModifiedByUserId(transactionManager, _statusModifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_Users_StatusModifiedBy key.
		///		fkFileDbUsersStatusModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_statusModifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByStatusModifiedByUserId(System.Int32? _statusModifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByStatusModifiedByUserId(null, _statusModifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_Users_StatusModifiedBy key.
		///		fkFileDbUsersStatusModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_statusModifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public TList<FileDb> GetByStatusModifiedByUserId(System.Int32? _statusModifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByStatusModifiedByUserId(null, _statusModifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileDb_Users_StatusModifiedBy key.
		///		FK_FileDb_Users_StatusModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusModifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileDb objects.</returns>
		public abstract TList<FileDb> GetByStatusModifiedByUserId(TransactionManager transactionManager, System.Int32? _statusModifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.FileDb Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbKey key, int start, int pageLength)
		{
			return GetByFileId(transactionManager, key.FileId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileDB index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDb"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDb GetByFileId(System.Int32 _fileId)
		{
			int count = -1;
			return GetByFileId(null,_fileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDB index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDb"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDb GetByFileId(System.Int32 _fileId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileId(null, _fileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDB index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDb"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDb GetByFileId(TransactionManager transactionManager, System.Int32 _fileId)
		{
			int count = -1;
			return GetByFileId(transactionManager, _fileId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDB index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDb"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDb GetByFileId(TransactionManager transactionManager, System.Int32 _fileId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileId(transactionManager, _fileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDB index.
		/// </summary>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDb"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDb GetByFileId(System.Int32 _fileId, int start, int pageLength, out int count)
		{
			return GetByFileId(null, _fileId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDB index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDb"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileDb GetByFileId(TransactionManager transactionManager, System.Int32 _fileId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_FileDB index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(null,_companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileDB index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileDB index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileDB index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileDB index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetByCompanyId(System.Int32 _companyId, int start, int pageLength, out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FileDB index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public abstract TList<FileDb> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SelfAssessment_ResponseId index.
		/// </summary>
		/// <param name="_selfAssessmentResponseId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetBySelfAssessmentResponseId(System.Int32? _selfAssessmentResponseId)
		{
			int count = -1;
			return GetBySelfAssessmentResponseId(null,_selfAssessmentResponseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SelfAssessment_ResponseId index.
		/// </summary>
		/// <param name="_selfAssessmentResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetBySelfAssessmentResponseId(System.Int32? _selfAssessmentResponseId, int start, int pageLength)
		{
			int count = -1;
			return GetBySelfAssessmentResponseId(null, _selfAssessmentResponseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SelfAssessment_ResponseId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_selfAssessmentResponseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetBySelfAssessmentResponseId(TransactionManager transactionManager, System.Int32? _selfAssessmentResponseId)
		{
			int count = -1;
			return GetBySelfAssessmentResponseId(transactionManager, _selfAssessmentResponseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SelfAssessment_ResponseId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_selfAssessmentResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetBySelfAssessmentResponseId(TransactionManager transactionManager, System.Int32? _selfAssessmentResponseId, int start, int pageLength)
		{
			int count = -1;
			return GetBySelfAssessmentResponseId(transactionManager, _selfAssessmentResponseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SelfAssessment_ResponseId index.
		/// </summary>
		/// <param name="_selfAssessmentResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public TList<FileDb> GetBySelfAssessmentResponseId(System.Int32? _selfAssessmentResponseId, int start, int pageLength, out int count)
		{
			return GetBySelfAssessmentResponseId(null, _selfAssessmentResponseId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SelfAssessment_ResponseId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_selfAssessmentResponseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FileDb&gt;"/> class.</returns>
		public abstract TList<FileDb> GetBySelfAssessmentResponseId(TransactionManager transactionManager, System.Int32? _selfAssessmentResponseId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _FileDb_GetPaged_Brief 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetPaged_Brief' stored procedure. 
		/// </summary>
		/// <param name="whereClause"> A <c>System.String</c> instance.</param>
		/// <param name="orderBy"> A <c>System.String</c> instance.</param>
		/// <param name="pageIndex"> A <c>System.Int32?</c> instance.</param>
		/// <param name="pageSize"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetPaged_Brief(System.String whereClause, System.String orderBy, System.Int32? pageIndex, System.Int32? pageSize)
		{
			return GetPaged_Brief(null, 0, int.MaxValue , whereClause, orderBy, pageIndex, pageSize);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetPaged_Brief' stored procedure. 
		/// </summary>
		/// <param name="whereClause"> A <c>System.String</c> instance.</param>
		/// <param name="orderBy"> A <c>System.String</c> instance.</param>
		/// <param name="pageIndex"> A <c>System.Int32?</c> instance.</param>
		/// <param name="pageSize"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetPaged_Brief(int start, int pageLength, System.String whereClause, System.String orderBy, System.Int32? pageIndex, System.Int32? pageSize)
		{
			return GetPaged_Brief(null, start, pageLength , whereClause, orderBy, pageIndex, pageSize);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetPaged_Brief' stored procedure. 
		/// </summary>
		/// <param name="whereClause"> A <c>System.String</c> instance.</param>
		/// <param name="orderBy"> A <c>System.String</c> instance.</param>
		/// <param name="pageIndex"> A <c>System.Int32?</c> instance.</param>
		/// <param name="pageSize"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetPaged_Brief(TransactionManager transactionManager, System.String whereClause, System.String orderBy, System.Int32? pageIndex, System.Int32? pageSize)
		{
			return GetPaged_Brief(transactionManager, 0, int.MaxValue , whereClause, orderBy, pageIndex, pageSize);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetPaged_Brief' stored procedure. 
		/// </summary>
		/// <param name="whereClause"> A <c>System.String</c> instance.</param>
		/// <param name="orderBy"> A <c>System.String</c> instance.</param>
		/// <param name="pageIndex"> A <c>System.Int32?</c> instance.</param>
		/// <param name="pageSize"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetPaged_Brief(TransactionManager transactionManager, int start, int pageLength , System.String whereClause, System.String orderBy, System.Int32? pageIndex, System.Int32? pageSize);
		
		#endregion
		
		#region _FileDb_GetStatusLastSubmittedFile 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetStatusLastSubmittedFile(System.String description, System.Int32? companyId)
		{
			return GetStatusLastSubmittedFile(null, 0, int.MaxValue , description, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetStatusLastSubmittedFile(int start, int pageLength, System.String description, System.Int32? companyId)
		{
			return GetStatusLastSubmittedFile(null, start, pageLength , description, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetStatusLastSubmittedFile(TransactionManager transactionManager, System.String description, System.Int32? companyId)
		{
			return GetStatusLastSubmittedFile(transactionManager, 0, int.MaxValue , description, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetStatusLastSubmittedFile(TransactionManager transactionManager, int start, int pageLength , System.String description, System.Int32? companyId);
		
		#endregion


        //added by sayani

        public DataSet GetVisiblePlanStatus(System.String description, System.Int32? companyId)
        {
            return GetVisiblePlanStatus(null, 0, int.MaxValue, description, companyId);
        }

        public DataSet GetVisiblePlanStatus(int start, int pageLength, System.String description, System.Int32? companyId)
        {
            return GetVisiblePlanStatus(null, start, pageLength, description, companyId);
        }

        public DataSet GetVisiblePlanStatus(TransactionManager transactionManager, System.String description, System.Int32? companyId)
        {
            return GetVisiblePlanStatus(transactionManager, 0, int.MaxValue, description, companyId);
        }


        public abstract DataSet GetVisiblePlanStatus(TransactionManager transactionManager, int start, int pageLength, System.String description, System.Int32? companyId);
		
		
        //added by sayani for task#5

        //public DataSet GetStatus(System.String description, System.Int32? companyId)
        //{
        //    return GetStatus(null, 0, int.MaxValue, description, companyId);
        //}

        //public abstract DataSet GetStatus(TransactionManager transactionManager, int start, int pageLength, System.String description, System.Int32? companyId);


		#region _FileDb_GetResponseIdLastSubmittedFile 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetResponseIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetResponseIdLastSubmittedFile(System.String description, System.Int32? companyId)
		{
			return GetResponseIdLastSubmittedFile(null, 0, int.MaxValue , description, companyId);
		}

		/// <summary>
		///	This method wrap the '_FileDb_GetResponseIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetResponseIdLastSubmittedFile(int start, int pageLength, System.String description, System.Int32? companyId)
		{
			return GetResponseIdLastSubmittedFile(null, start, pageLength , description, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetResponseIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetResponseIdLastSubmittedFile(TransactionManager transactionManager, System.String description, System.Int32? companyId)
		{
			return GetResponseIdLastSubmittedFile(transactionManager, 0, int.MaxValue , description, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetResponseIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetResponseIdLastSubmittedFile(TransactionManager transactionManager, int start, int pageLength , System.String description, System.Int32? companyId);
//added by sayani
        //public abstract DataSet GetVisiblePlanStatus(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId);
		
		#endregion
		
		#region _FileDb_BulkUpdateEhsConsultantId 
		
		/// <summary>
		///	This method wrap the '_FileDb_BulkUpdateEhsConsultantId' stored procedure. 
		/// </summary>
		/// <param name="from"> A <c>System.Int32?</c> instance.</param>
		/// <param name="to"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void BulkUpdateEhsConsultantId(System.Int32? from, System.Int32? to)
		{
			 BulkUpdateEhsConsultantId(null, 0, int.MaxValue , from, to);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_BulkUpdateEhsConsultantId' stored procedure. 
		/// </summary>
		/// <param name="from"> A <c>System.Int32?</c> instance.</param>
		/// <param name="to"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void BulkUpdateEhsConsultantId(int start, int pageLength, System.Int32? from, System.Int32? to)
		{
			 BulkUpdateEhsConsultantId(null, start, pageLength , from, to);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_BulkUpdateEhsConsultantId' stored procedure. 
		/// </summary>
		/// <param name="from"> A <c>System.Int32?</c> instance.</param>
		/// <param name="to"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public void BulkUpdateEhsConsultantId(TransactionManager transactionManager, System.Int32? from, System.Int32? to)
		{
			 BulkUpdateEhsConsultantId(transactionManager, 0, int.MaxValue , from, to);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_BulkUpdateEhsConsultantId' stored procedure. 
		/// </summary>
		/// <param name="from"> A <c>System.Int32?</c> instance.</param>
		/// <param name="to"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		public abstract void BulkUpdateEhsConsultantId(TransactionManager transactionManager, int start, int pageLength , System.Int32? from, System.Int32? to);
		
		#endregion
		
		#region _FileDb_GetByCompanyId_Custom2 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom2(System.Int32? companyId)
		{
			return GetByCompanyId_Custom2(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom2(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId_Custom2(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom2(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId_Custom2(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom2' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId_Custom2(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _FileDb_GetByCompanyId_Custom 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom(System.Int32? companyId)
		{
			return GetByCompanyId_Custom(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom(int start, int pageLength, System.Int32? companyId)
		{
			return GetByCompanyId_Custom(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByCompanyId_Custom(TransactionManager transactionManager, System.Int32? companyId)
		{
			return GetByCompanyId_Custom(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetByCompanyId_Custom' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByCompanyId_Custom(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _FileDb_GetDistinctYears 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears()
		{
			return GetDistinctYears(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears(int start, int pageLength)
		{
			return GetDistinctYears(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetDistinctYears(TransactionManager transactionManager)
		{
			return GetDistinctYears(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetDistinctYears' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetDistinctYears(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _FileDb_CountEhsConsultantsUnallocated 
		
		/// <summary>
		///	This method wrap the '_FileDb_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CountEhsConsultantsUnallocated(System.String currentYear)
		{
			return CountEhsConsultantsUnallocated(null, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CountEhsConsultantsUnallocated(int start, int pageLength, System.String currentYear)
		{
			return CountEhsConsultantsUnallocated(null, start, pageLength , currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CountEhsConsultantsUnallocated(TransactionManager transactionManager, System.String currentYear)
		{
			return CountEhsConsultantsUnallocated(transactionManager, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_CountEhsConsultantsUnallocated' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CountEhsConsultantsUnallocated(TransactionManager transactionManager, int start, int pageLength , System.String currentYear);
		
		#endregion
		
		#region _FileDb_Get_List_Brief 
		
		/// <summary>
		///	This method wrap the '_FileDb_Get_List_Brief' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Brief()
		{
			return Get_List_Brief(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_Get_List_Brief' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Brief(int start, int pageLength)
		{
			return Get_List_Brief(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_Get_List_Brief' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Get_List_Brief(TransactionManager transactionManager)
		{
			return Get_List_Brief(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_Get_List_Brief' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Get_List_Brief(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _FileDb_GetSEStatus 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetSEStatus' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSEStatus(System.Int32? responseId)
		{
			return GetSEStatus(null, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetSEStatus' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSEStatus(int start, int pageLength, System.Int32? responseId)
		{
			return GetSEStatus(null, start, pageLength , responseId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetSEStatus' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetSEStatus(TransactionManager transactionManager, System.Int32? responseId)
		{
			return GetSEStatus(transactionManager, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetSEStatus' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetSEStatus(TransactionManager transactionManager, int start, int pageLength , System.Int32? responseId);
		
		#endregion
		
		#region _FileDb_GetReport_Compliance_SP 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetReport_Compliance_SP' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance_SP(System.Int32? year)
		{
			return GetReport_Compliance_SP(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetReport_Compliance_SP' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance_SP(int start, int pageLength, System.Int32? year)
		{
			return GetReport_Compliance_SP(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetReport_Compliance_SP' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetReport_Compliance_SP(TransactionManager transactionManager, System.Int32? year)
		{
			return GetReport_Compliance_SP(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetReport_Compliance_SP' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetReport_Compliance_SP(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#region _FileDb_GetCompanyIdByQuestionnaireResponseId 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetCompanyIdByQuestionnaireResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyIdByQuestionnaireResponseId(System.Int32? responseId)
		{
			return GetCompanyIdByQuestionnaireResponseId(null, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetCompanyIdByQuestionnaireResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyIdByQuestionnaireResponseId(int start, int pageLength, System.Int32? responseId)
		{
			return GetCompanyIdByQuestionnaireResponseId(null, start, pageLength , responseId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetCompanyIdByQuestionnaireResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetCompanyIdByQuestionnaireResponseId(TransactionManager transactionManager, System.Int32? responseId)
		{
			return GetCompanyIdByQuestionnaireResponseId(transactionManager, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetCompanyIdByQuestionnaireResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetCompanyIdByQuestionnaireResponseId(TransactionManager transactionManager, int start, int pageLength , System.Int32? responseId);
		
		#endregion
		
		#region _FileDb_ComplianceReport_ByCompany 
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_ByCompany(System.Int32? companyId, System.Int32? year, System.Int32? SiteId)
		{
            return ComplianceReport_ByCompany(null, 0, int.MaxValue, companyId, year, SiteId);
		}
		

        ///Added by Sayani Sil
        public DataSet ComplianceReport_ByCompany_Site(System.Int32? companyId, System.Int32? year, System.Int32? siteId)
        {
            return ComplianceReport_ByCompany_Site(null, 0, int.MaxValue, companyId, year, siteId);
        }

        public abstract DataSet ComplianceReport_ByCompany_Site(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? year, System.Int32? siteId);

        //End of Additon by Sayani Sil

        //Added by Sayani Sil for DT2416
        public DataSet ComplianceReport_BySite(System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId)
        {
            return ComplianceReport_BySite(null, 0, int.MaxValue, sYear, siteId, companySiteCategoryId);
        }

        public abstract DataSet ComplianceReport_BySite(TransactionManager transactionManager, int start, int pageLength, System.Int32? sYear, System.Int32? siteId, System.String companySiteCategoryId);
		

        //End of addition

		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet ComplianceReport_ByCompany(int start, int pageLength, System.Int32? companyId, System.Int32? year, System.Int32? SiteId)
		{
			return ComplianceReport_ByCompany(null, start, pageLength , companyId, year, SiteId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
        public DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, System.Int32? companyId, System.Int32? year, System.Int32? SiteId)
		{
			return ComplianceReport_ByCompany(transactionManager, 0, int.MaxValue , companyId, year, SiteId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_ByCompany' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
        public abstract DataSet ComplianceReport_ByCompany(TransactionManager transactionManager, int start, int pageLength, System.Int32? companyId, System.Int32? year, System.Int32? SiteId);
		
		#endregion
		
		#region _FileDb_ComplianceReport 
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(System.Int32? year)
		{
			return ComplianceReport(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(int start, int pageLength, System.Int32? year)
		{
			return ComplianceReport(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport(TransactionManager transactionManager, System.Int32? year)
		{
			return ComplianceReport(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#region _FileDb_GetStatusFromResponseId 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusFromResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetStatusFromResponseId(System.Int32? responseId)
		{
			return GetStatusFromResponseId(null, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusFromResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetStatusFromResponseId(int start, int pageLength, System.Int32? responseId)
		{
			return GetStatusFromResponseId(null, start, pageLength , responseId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusFromResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetStatusFromResponseId(TransactionManager transactionManager, System.Int32? responseId)
		{
			return GetStatusFromResponseId(transactionManager, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetStatusFromResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetStatusFromResponseId(TransactionManager transactionManager, int start, int pageLength , System.Int32? responseId);
		
		#endregion
		
		#region _FileDb_GetEhsConsultantsWorkLoad 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetEhsConsultantsWorkLoad(System.String currentYear)
		{
			return GetEhsConsultantsWorkLoad(null, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetEhsConsultantsWorkLoad(int start, int pageLength, System.String currentYear)
		{
			return GetEhsConsultantsWorkLoad(null, start, pageLength , currentYear);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetEhsConsultantsWorkLoad(TransactionManager transactionManager, System.String currentYear)
		{
			return GetEhsConsultantsWorkLoad(transactionManager, 0, int.MaxValue , currentYear);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetEhsConsultantsWorkLoad' stored procedure. 
		/// </summary>
		/// <param name="currentYear"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetEhsConsultantsWorkLoad(TransactionManager transactionManager, int start, int pageLength , System.String currentYear);
		
		#endregion
		
		#region _FileDb_GetFileIdByResponseId 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetFileIdByResponseId(System.Int32? responseId)
		{
			return GetFileIdByResponseId(null, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetFileIdByResponseId(int start, int pageLength, System.Int32? responseId)
		{
			return GetFileIdByResponseId(null, start, pageLength , responseId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetFileIdByResponseId(TransactionManager transactionManager, System.Int32? responseId)
		{
			return GetFileIdByResponseId(transactionManager, 0, int.MaxValue , responseId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdByResponseId' stored procedure. 
		/// </summary>
		/// <param name="responseId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetFileIdByResponseId(TransactionManager transactionManager, int start, int pageLength , System.Int32? responseId);
		
		#endregion
		
		#region _FileDb_ComplianceReport_GetByByCompanyIdYear 
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_GetByByCompanyIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_GetByByCompanyIdYear(System.Int32? companyId, System.Int32? year)
		{
			return ComplianceReport_GetByByCompanyIdYear(null, 0, int.MaxValue , companyId, year);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_GetByByCompanyIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_GetByByCompanyIdYear(int start, int pageLength, System.Int32? companyId, System.Int32? year)
		{
			return ComplianceReport_GetByByCompanyIdYear(null, start, pageLength , companyId, year);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_GetByByCompanyIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ComplianceReport_GetByByCompanyIdYear(TransactionManager transactionManager, System.Int32? companyId, System.Int32? year)
		{
			return ComplianceReport_GetByByCompanyIdYear(transactionManager, 0, int.MaxValue , companyId, year);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_ComplianceReport_GetByByCompanyIdYear' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ComplianceReport_GetByByCompanyIdYear(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId, System.Int32? year);
		
		#endregion
		
		#region _FileDb_GetFileIdLastSubmittedFile 
		
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetFileIdLastSubmittedFile(System.String description, System.Int32? companyId)
		{
			return GetFileIdLastSubmittedFile(null, 0, int.MaxValue , description, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetFileIdLastSubmittedFile(int start, int pageLength, System.String description, System.Int32? companyId)
		{
			return GetFileIdLastSubmittedFile(null, start, pageLength , description, companyId);
		}
				
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetFileIdLastSubmittedFile(TransactionManager transactionManager, System.String description, System.Int32? companyId)
		{
			return GetFileIdLastSubmittedFile(transactionManager, 0, int.MaxValue , description, companyId);
		}
		
		/// <summary>
		///	This method wrap the '_FileDb_GetFileIdLastSubmittedFile' stored procedure. 
		/// </summary>
		/// <param name="description"> A <c>System.String</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetFileIdLastSubmittedFile(TransactionManager transactionManager, int start, int pageLength , System.String description, System.Int32? companyId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileDb&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileDb&gt;"/></returns>
		public static TList<FileDb> Fill(IDataReader reader, TList<FileDb> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.FileDb c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FileDb")
					.Append("|").Append((System.Int32)reader[((int)FileDbColumn.FileId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FileDb>(
					key.ToString(), // EntityTrackingKey
					"FileDb",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.FileDb();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FileId = (System.Int32)reader[((int)FileDbColumn.FileId - 1)];
					c.CompanyId = (System.Int32)reader[((int)FileDbColumn.CompanyId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)FileDbColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)FileDbColumn.ModifiedDate - 1)];
					c.Description = (System.String)reader[((int)FileDbColumn.Description - 1)];
					c.FileName = (System.String)reader[((int)FileDbColumn.FileName - 1)];
					c.FileHash = (System.Byte[])reader[((int)FileDbColumn.FileHash - 1)];
					c.ContentLength = (System.Int32)reader[((int)FileDbColumn.ContentLength - 1)];
					c.Content = (System.Byte[])reader[((int)FileDbColumn.Content - 1)];
					c.StatusId = (System.Int32)reader[((int)FileDbColumn.StatusId - 1)];
					c.StatusComments = (reader.IsDBNull(((int)FileDbColumn.StatusComments - 1)))?null:(System.String)reader[((int)FileDbColumn.StatusComments - 1)];
					c.StatusModifiedByUserId = (reader.IsDBNull(((int)FileDbColumn.StatusModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)FileDbColumn.StatusModifiedByUserId - 1)];
					c.EhsConsultantId = (reader.IsDBNull(((int)FileDbColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)FileDbColumn.EhsConsultantId - 1)];
					c.SelfAssessmentResponseId = (reader.IsDBNull(((int)FileDbColumn.SelfAssessmentResponseId - 1)))?null:(System.Int32?)reader[((int)FileDbColumn.SelfAssessmentResponseId - 1)];
					c.StatusModifiedDate = (reader.IsDBNull(((int)FileDbColumn.StatusModifiedDate - 1)))?null:(System.DateTime?)reader[((int)FileDbColumn.StatusModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDb"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDb"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.FileDb entity)
		{
			if (!reader.Read()) return;
			
			entity.FileId = (System.Int32)reader[((int)FileDbColumn.FileId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)FileDbColumn.CompanyId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileDbColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)FileDbColumn.ModifiedDate - 1)];
			entity.Description = (System.String)reader[((int)FileDbColumn.Description - 1)];
			entity.FileName = (System.String)reader[((int)FileDbColumn.FileName - 1)];
			entity.FileHash = (System.Byte[])reader[((int)FileDbColumn.FileHash - 1)];
			entity.ContentLength = (System.Int32)reader[((int)FileDbColumn.ContentLength - 1)];
			entity.Content = (System.Byte[])reader[((int)FileDbColumn.Content - 1)];
			entity.StatusId = (System.Int32)reader[((int)FileDbColumn.StatusId - 1)];
			entity.StatusComments = (reader.IsDBNull(((int)FileDbColumn.StatusComments - 1)))?null:(System.String)reader[((int)FileDbColumn.StatusComments - 1)];
			entity.StatusModifiedByUserId = (reader.IsDBNull(((int)FileDbColumn.StatusModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)FileDbColumn.StatusModifiedByUserId - 1)];
			entity.EhsConsultantId = (reader.IsDBNull(((int)FileDbColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)FileDbColumn.EhsConsultantId - 1)];
			entity.SelfAssessmentResponseId = (reader.IsDBNull(((int)FileDbColumn.SelfAssessmentResponseId - 1)))?null:(System.Int32?)reader[((int)FileDbColumn.SelfAssessmentResponseId - 1)];
			entity.StatusModifiedDate = (reader.IsDBNull(((int)FileDbColumn.StatusModifiedDate - 1)))?null:(System.DateTime?)reader[((int)FileDbColumn.StatusModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDb"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDb"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.FileDb entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileId = (System.Int32)dataRow["FileId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.Description = (System.String)dataRow["Description"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.FileHash = (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (System.Int32)dataRow["ContentLength"];
			entity.Content = (System.Byte[])dataRow["Content"];
			entity.StatusId = (System.Int32)dataRow["StatusId"];
			entity.StatusComments = Convert.IsDBNull(dataRow["StatusComments"]) ? null : (System.String)dataRow["StatusComments"];
			entity.StatusModifiedByUserId = Convert.IsDBNull(dataRow["StatusModifiedByUserId"]) ? null : (System.Int32?)dataRow["StatusModifiedByUserId"];
			entity.EhsConsultantId = Convert.IsDBNull(dataRow["EHSConsultantId"]) ? null : (System.Int32?)dataRow["EHSConsultantId"];
			entity.SelfAssessmentResponseId = Convert.IsDBNull(dataRow["SelfAssessment_ResponseId"]) ? null : (System.Int32?)dataRow["SelfAssessment_ResponseId"];
			entity.StatusModifiedDate = Convert.IsDBNull(dataRow["StatusModifiedDate"]) ? null : (System.DateTime?)dataRow["StatusModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDb"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDb Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDb entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region StatusModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|StatusModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.StatusModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.StatusModifiedByUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.StatusModifiedByUserIdSource = tmpEntity;
				else
					entity.StatusModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.StatusModifiedByUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StatusModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.StatusModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.StatusModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion StatusModifiedByUserIdSource

			#region CompanyIdSource	
			if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList) 
				&& entity.CompanyIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanyId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanyIdSource = tmpEntity;
				else
					entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanyIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanyIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanyIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileDb object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileDb instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDb Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDb entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region StatusModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|StatusModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.StatusModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.StatusModifiedByUserIdSource);
				entity.StatusModifiedByUserId = entity.StatusModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region CompanyIdSource
			if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList) 
				&& entity.CompanyIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
				entity.CompanyId = entity.CompanyIdSource.CompanyId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileDbChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileDb</c>
	///</summary>
	public enum FileDbChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Companies</c> at CompanyIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
		}
	
	#endregion FileDbChildEntityTypes
	
	#region FileDbFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileDbColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbFilterBuilder : SqlFilterBuilder<FileDbColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbFilterBuilder class.
		/// </summary>
		public FileDbFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbFilterBuilder
	
	#region FileDbParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileDbColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbParameterBuilder : ParameterizedSqlFilterBuilder<FileDbColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbParameterBuilder class.
		/// </summary>
		public FileDbParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbParameterBuilder
	
	#region FileDbSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileDbColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDb"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileDbSortBuilder : SqlSortBuilder<FileDbColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbSqlSortBuilder class.
		/// </summary>
		public FileDbSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileDbSortBuilder
	
} // end namespace
