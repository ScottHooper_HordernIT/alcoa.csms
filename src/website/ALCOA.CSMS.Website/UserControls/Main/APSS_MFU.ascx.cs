using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using KaiZen.CSMS.Entities;

using DevExpress.Web.ASPxEditors;

public partial class Library_APSS_MFU : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            btnEdit.Visible = false;
            SessionHandler.ViewMode = "View";

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor): 
                    break;
                case ((int)RoleList.Administrator): 
                    btnEdit.Visible = true;
                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
            if (SessionHandler.ViewMode == "View")
            {
                grid.Columns[0].Visible = true;
                SessionHandler.ViewMode = "Edit";
                btnEdit.Text = "(View)";
            }
            else if (SessionHandler.ViewMode == "Edit")
            {
                grid.Columns[0].Visible = false;
                SessionHandler.ViewMode = "View";
                btnEdit.Text = "(Edit)";
            }
    }

    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ApssDocumentType"] = "MFU";
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        e.NewValues["ApssDocumentType"] = "MFU";
    }

    protected void grid_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        e.NewValues["ApssDocumentType"] = "MFU";
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFileDownload") as ASPxHyperLink;
        if (hl != null && grid.GetRowValues(e.VisibleIndex, "DocumentFileName") != null)
        {
            string fileName = (string)grid.GetRowValues(e.VisibleIndex, "DocumentFileName");
            hl.Text = fileName;
            hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MFU&File=" + fileName));
        }
    }
}
