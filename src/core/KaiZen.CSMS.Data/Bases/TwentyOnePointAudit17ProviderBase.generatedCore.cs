﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TwentyOnePointAudit17ProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TwentyOnePointAudit17ProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.TwentyOnePointAudit17, KaiZen.CSMS.Entities.TwentyOnePointAudit17Key>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit17Key key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.TwentyOnePointAudit17 Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit17Key key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_TwentyOnePointAudit_17 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit17 GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_17 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit17 GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_17 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit17 GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_17 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit17 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_17 index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> class.</returns>
		public KaiZen.CSMS.Entities.TwentyOnePointAudit17 GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_TwentyOnePointAudit_17 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.TwentyOnePointAudit17 GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TwentyOnePointAudit17&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TwentyOnePointAudit17&gt;"/></returns>
		public static TList<TwentyOnePointAudit17> Fill(IDataReader reader, TList<TwentyOnePointAudit17> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.TwentyOnePointAudit17 c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TwentyOnePointAudit17")
					.Append("|").Append((System.Int32)reader[((int)TwentyOnePointAudit17Column.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TwentyOnePointAudit17>(
					key.ToString(), // EntityTrackingKey
					"TwentyOnePointAudit17",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.TwentyOnePointAudit17();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)TwentyOnePointAudit17Column.Id - 1)];
					c.Achieved17a = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17a - 1)];
					c.Achieved17b = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17b - 1)];
					c.Achieved17c = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17c - 1)];
					c.Achieved17d = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17d - 1)];
					c.Achieved17e = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17e - 1)];
					c.Achieved17f = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17f - 1)];
					c.Achieved17g = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17g - 1)];
					c.Achieved17h = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17h - 1)];
					c.Achieved17i = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17i - 1)];
					c.Achieved17j = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17j - 1)];
					c.Achieved17k = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17k - 1)];
					c.Achieved17l = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17l - 1)];
					c.Achieved17m = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17m - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17m - 1)];
					c.Observation17a = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17a - 1)];
					c.Observation17b = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17b - 1)];
					c.Observation17c = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17c - 1)];
					c.Observation17d = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17d - 1)];
					c.Observation17e = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17e - 1)];
					c.Observation17f = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17f - 1)];
					c.Observation17g = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17g - 1)];
					c.Observation17h = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17h - 1)];
					c.Observation17i = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17i - 1)];
					c.Observation17j = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17j - 1)];
					c.Observation17k = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17k - 1)];
					c.Observation17l = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17l - 1)];
					c.Observation17m = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17m - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17m - 1)];
					c.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.TotalScore - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.TwentyOnePointAudit17 entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)TwentyOnePointAudit17Column.Id - 1)];
			entity.Achieved17a = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17a - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17a - 1)];
			entity.Achieved17b = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17b - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17b - 1)];
			entity.Achieved17c = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17c - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17c - 1)];
			entity.Achieved17d = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17d - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17d - 1)];
			entity.Achieved17e = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17e - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17e - 1)];
			entity.Achieved17f = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17f - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17f - 1)];
			entity.Achieved17g = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17g - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17g - 1)];
			entity.Achieved17h = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17h - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17h - 1)];
			entity.Achieved17i = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17i - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17i - 1)];
			entity.Achieved17j = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17j - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17j - 1)];
			entity.Achieved17k = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17k - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17k - 1)];
			entity.Achieved17l = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17l - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17l - 1)];
			entity.Achieved17m = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Achieved17m - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.Achieved17m - 1)];
			entity.Observation17a = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17a - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17a - 1)];
			entity.Observation17b = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17b - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17b - 1)];
			entity.Observation17c = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17c - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17c - 1)];
			entity.Observation17d = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17d - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17d - 1)];
			entity.Observation17e = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17e - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17e - 1)];
			entity.Observation17f = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17f - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17f - 1)];
			entity.Observation17g = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17g - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17g - 1)];
			entity.Observation17h = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17h - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17h - 1)];
			entity.Observation17i = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17i - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17i - 1)];
			entity.Observation17j = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17j - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17j - 1)];
			entity.Observation17k = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17k - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17k - 1)];
			entity.Observation17l = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17l - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17l - 1)];
			entity.Observation17m = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.Observation17m - 1)))?null:(System.String)reader[((int)TwentyOnePointAudit17Column.Observation17m - 1)];
			entity.TotalScore = (reader.IsDBNull(((int)TwentyOnePointAudit17Column.TotalScore - 1)))?null:(System.Int32?)reader[((int)TwentyOnePointAudit17Column.TotalScore - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.TwentyOnePointAudit17 entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["ID"];
			entity.Achieved17a = Convert.IsDBNull(dataRow["Achieved17a"]) ? null : (System.Int32?)dataRow["Achieved17a"];
			entity.Achieved17b = Convert.IsDBNull(dataRow["Achieved17b"]) ? null : (System.Int32?)dataRow["Achieved17b"];
			entity.Achieved17c = Convert.IsDBNull(dataRow["Achieved17c"]) ? null : (System.Int32?)dataRow["Achieved17c"];
			entity.Achieved17d = Convert.IsDBNull(dataRow["Achieved17d"]) ? null : (System.Int32?)dataRow["Achieved17d"];
			entity.Achieved17e = Convert.IsDBNull(dataRow["Achieved17e"]) ? null : (System.Int32?)dataRow["Achieved17e"];
			entity.Achieved17f = Convert.IsDBNull(dataRow["Achieved17f"]) ? null : (System.Int32?)dataRow["Achieved17f"];
			entity.Achieved17g = Convert.IsDBNull(dataRow["Achieved17g"]) ? null : (System.Int32?)dataRow["Achieved17g"];
			entity.Achieved17h = Convert.IsDBNull(dataRow["Achieved17h"]) ? null : (System.Int32?)dataRow["Achieved17h"];
			entity.Achieved17i = Convert.IsDBNull(dataRow["Achieved17i"]) ? null : (System.Int32?)dataRow["Achieved17i"];
			entity.Achieved17j = Convert.IsDBNull(dataRow["Achieved17j"]) ? null : (System.Int32?)dataRow["Achieved17j"];
			entity.Achieved17k = Convert.IsDBNull(dataRow["Achieved17k"]) ? null : (System.Int32?)dataRow["Achieved17k"];
			entity.Achieved17l = Convert.IsDBNull(dataRow["Achieved17l"]) ? null : (System.Int32?)dataRow["Achieved17l"];
			entity.Achieved17m = Convert.IsDBNull(dataRow["Achieved17m"]) ? null : (System.Int32?)dataRow["Achieved17m"];
			entity.Observation17a = Convert.IsDBNull(dataRow["Observation17a"]) ? null : (System.String)dataRow["Observation17a"];
			entity.Observation17b = Convert.IsDBNull(dataRow["Observation17b"]) ? null : (System.String)dataRow["Observation17b"];
			entity.Observation17c = Convert.IsDBNull(dataRow["Observation17c"]) ? null : (System.String)dataRow["Observation17c"];
			entity.Observation17d = Convert.IsDBNull(dataRow["Observation17d"]) ? null : (System.String)dataRow["Observation17d"];
			entity.Observation17e = Convert.IsDBNull(dataRow["Observation17e"]) ? null : (System.String)dataRow["Observation17e"];
			entity.Observation17f = Convert.IsDBNull(dataRow["Observation17f"]) ? null : (System.String)dataRow["Observation17f"];
			entity.Observation17g = Convert.IsDBNull(dataRow["Observation17g"]) ? null : (System.String)dataRow["Observation17g"];
			entity.Observation17h = Convert.IsDBNull(dataRow["Observation17h"]) ? null : (System.String)dataRow["Observation17h"];
			entity.Observation17i = Convert.IsDBNull(dataRow["Observation17i"]) ? null : (System.String)dataRow["Observation17i"];
			entity.Observation17j = Convert.IsDBNull(dataRow["Observation17j"]) ? null : (System.String)dataRow["Observation17j"];
			entity.Observation17k = Convert.IsDBNull(dataRow["Observation17k"]) ? null : (System.String)dataRow["Observation17k"];
			entity.Observation17l = Convert.IsDBNull(dataRow["Observation17l"]) ? null : (System.String)dataRow["Observation17l"];
			entity.Observation17m = Convert.IsDBNull(dataRow["Observation17m"]) ? null : (System.String)dataRow["Observation17m"];
			entity.TotalScore = Convert.IsDBNull(dataRow["TotalScore"]) ? null : (System.Int32?)dataRow["TotalScore"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.TwentyOnePointAudit17"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit17 Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit17 entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region TwentyOnePointAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TwentyOnePointAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TwentyOnePointAuditCollection = DataRepository.TwentyOnePointAuditProvider.GetByPoint17Id(transactionManager, entity.Id);

				if (deep && entity.TwentyOnePointAuditCollection.Count > 0)
				{
					deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TwentyOnePointAudit>) DataRepository.TwentyOnePointAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.TwentyOnePointAudit17 object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.TwentyOnePointAudit17 instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.TwentyOnePointAudit17 Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.TwentyOnePointAudit17 entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TwentyOnePointAudit>
				if (CanDeepSave(entity.TwentyOnePointAuditCollection, "List<TwentyOnePointAudit>|TwentyOnePointAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TwentyOnePointAudit child in entity.TwentyOnePointAuditCollection)
					{
						if(child.Point17IdSource != null)
						{
							child.Point17Id = child.Point17IdSource.Id;
						}
						else
						{
							child.Point17Id = entity.Id;
						}

					}

					if (entity.TwentyOnePointAuditCollection.Count > 0 || entity.TwentyOnePointAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TwentyOnePointAuditProvider.Save(transactionManager, entity.TwentyOnePointAuditCollection);
						
						deepHandles.Add("TwentyOnePointAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TwentyOnePointAudit >) DataRepository.TwentyOnePointAuditProvider.DeepSave,
							new object[] { transactionManager, entity.TwentyOnePointAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TwentyOnePointAudit17ChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.TwentyOnePointAudit17</c>
	///</summary>
	public enum TwentyOnePointAudit17ChildEntityTypes
	{

		///<summary>
		/// Collection of <c>TwentyOnePointAudit17</c> as OneToMany for TwentyOnePointAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<TwentyOnePointAudit>))]
		TwentyOnePointAuditCollection,
	}
	
	#endregion TwentyOnePointAudit17ChildEntityTypes
	
	#region TwentyOnePointAudit17FilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TwentyOnePointAudit17Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17FilterBuilder : SqlFilterBuilder<TwentyOnePointAudit17Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17FilterBuilder class.
		/// </summary>
		public TwentyOnePointAudit17FilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit17FilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17FilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit17FilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit17FilterBuilder
	
	#region TwentyOnePointAudit17ParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TwentyOnePointAudit17Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17ParameterBuilder : ParameterizedSqlFilterBuilder<TwentyOnePointAudit17Column>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17ParameterBuilder class.
		/// </summary>
		public TwentyOnePointAudit17ParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TwentyOnePointAudit17ParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17ParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TwentyOnePointAudit17ParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TwentyOnePointAudit17ParameterBuilder
	
	#region TwentyOnePointAudit17SortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TwentyOnePointAudit17Column&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TwentyOnePointAudit17SortBuilder : SqlSortBuilder<TwentyOnePointAudit17Column>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17SqlSortBuilder class.
		/// </summary>
		public TwentyOnePointAudit17SortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TwentyOnePointAudit17SortBuilder
	
} // end namespace
