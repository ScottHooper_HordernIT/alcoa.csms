﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Companies_Admin" Codebehind="companiesAdmin.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>








<div style="width: 100%; text-align: right; padding-bottom: 2px">
<a href="javascript:ShowHideCustomizationWindow();"><span style="color: #0000ff; text-decoration: none">
                Customize</span></a>
</div>
<dxwgv:ASPxGridView
    ID="grid"
    runat="server" ClientInstanceName="grid"
    AutoGenerateColumns="False"
    Width="100%"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" 
    DataSourceID="CompaniesDataSource" OnCellEditorInitialize="grid_onEdit" onrowvalidating="grid_RowValidating"
    OnRowUpdating="grid_RowUpdating" OnInitNewRow="grid_InitNewRow" OnRowInserting="grid_RowInserting"
    KeyFieldName="CompanyId" OnRowUpdated="grid_RowUpdated">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
       
        <dxwgv:GridViewCommandColumn AllowDragDrop="False" Caption="Action" VisibleIndex="2">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="CompanyId" ReadOnly="true" ShowInCustomizationForm="False"
            Visible="False" VisibleIndex="5">
            <EditFormSettings Visible="false" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId"
            SortIndex="1" SortOrder="Ascending" VisibleIndex="6">
            <PropertiesComboBox DataSourceID="CompaniesDataSource1" TextField="CompanyName" ValueField="CompanyId"
                ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn FieldName="CompanyName" SortIndex="0" SortOrder="Ascending"
            Visible="False" VisibleIndex="7">
           
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="ABN" FieldName="CompanyAbn" Name="ABN ComboBox"
            VisibleIndex="10" Width="90px">
            <PropertiesComboBox DataSourceID="CompaniesDataSource2" IncrementalFilteringMode="StartsWith"
                TextField="CompanyAbn" ValueField="CompanyAbn" ValueType="System.String">
            </PropertiesComboBox>
            <Settings SortMode="Value" />
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn Caption="ABN" FieldName="CompanyAbn" UnboundType="Integer"
            VisibleIndex="8" Width="90px" Visible="False">
            <PropertiesTextEdit>
                <ValidationSettings Display="Dynamic">
                </ValidationSettings>
            </PropertiesTextEdit>
            <EditFormSettings Visible="True" VisibleIndex="2" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="PhoneNo" Visible="False" VisibleIndex="11">
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="FaxNo" Visible="False" VisibleIndex="12">
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="MobNo" Visible="False" VisibleIndex="13">
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="AddressBusiness" Visible="False" VisibleIndex="14">
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="AddressPostal" Visible="False" VisibleIndex="15">
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Default EHS Consultant" FieldName="EhsConsultantId"
            VisibleIndex="9">
            <PropertiesComboBox DataSourceID="UsersEhsConsultantsGetActiveAllDataSource" DropDownHeight="150px"
                IncrementalFilteringMode="StartsWith" TextField="UserFullNameLogon" ValueField="EhsConsultantId"
                ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Safety Qualification Process Status" FieldName="CompanyStatusId"
            VisibleIndex="17">
            <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" DropDownHeight="150px"
                IncrementalFilteringMode="StartsWith" TextField="CompanyStatusName" ValueField="CompanyStatusId"
                ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Safety Qualification Process Status" FieldName="CompanyStatusId"
            Visible="False" VisibleIndex="16">
            <PropertiesComboBox DataSourceID="CompanyStatusDataSource1" DropDownHeight="150px"
                IncrementalFilteringMode="StartsWith" TextField="CompanyStatusDesc" ValueField="CompanyStatusId"
                ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn Caption="iProc Supplier No" FieldName="IprocSupplierNo"
            VisibleIndex="18" Visible="false" EditFormSettings-Visible="true" ShowInCustomizationForm="false">
            <PropertiesTextEdit MaxLength="10">
            </PropertiesTextEdit>
           
<EditFormSettings Visible="True"></EditFormSettings>
           
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Company Status" FieldName="CompanyStatus2Id"
            Visible="False" VisibleIndex="20">
            <PropertiesComboBox DataSourceID="CompanyStatus2DataSource1" DropDownHeight="150px"
                IncrementalFilteringMode="StartsWith" TextField="CompanyStatusDesc" ValueField="CompanyStatusId"
                ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
            <EditFormSettings Visible="True" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn FieldName="ModifiedbyUserId" ShowInCustomizationForm="False"
            Visible="False" VisibleIndex="19">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataCheckColumn Caption="De-Activated?" FieldName="Deactivated" Visible="True"
            VisibleIndex="1003">
            <EditFormSettings Visible="True" VisibleIndex="999" />
        </dxwgv:GridViewDataCheckColumn>
        
       
        
    </Columns>
   <%-- <SettingsEditing EditFormColumnCount="3" Mode="PopupEditForm">

    </SettingsEditing>--%>
    <Settings ShowGroupPanel="True"  ShowFilterRow="True" ShowFilterBar="Visible" />
    <SettingsCustomizationWindow Enabled="True" PopupVerticalAlign="Above"></SettingsCustomizationWindow>
    <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
</dxwgv:ASPxGridView>




<div align="right" style="padding-top: 2px;">
    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
</div>


<data:CompaniesDataSource ID="CompaniesDataSource" runat="server"
	SelectMethod="GetPaged"
	EnablePaging="True"
	EnableDeepLoad="True"
	Sort="CompanyName ASC"
	>
	<DeepLoadProperties Method="IncludeChildren" Recursive="False">
        <Types>
			<data:CompaniesProperty Name="Users"/> 
		</Types>
	</DeepLoadProperties>
</data:CompaniesDataSource>
		
<data:UsersEhsConsultantsDataSource runat="server" ID="UsersEhsConsultantsDataSource2" Filter="Enabled = True" SelectMethod="GetAll" Sort="UserFullNameLogon ASC" EnableSorting="true">
</data:UsersEhsConsultantsDataSource>

<data:CompanyStatusDataSource runat="server" ID="CompanyStatusDataSource1" SelectMethod="GetAll" EnableCaching="True" CacheDuration="60" Sort="CompanyStatusDesc ASC">
</data:CompanyStatusDataSource>

<data:CompanyStatus2DataSource runat="server" ID="CompanyStatus2DataSource1" SelectMethod="GetAll" EnableCaching="True" CacheDuration="60">
</data:CompanyStatus2DataSource>

<asp:SqlDataSource ID="UsersEhsConsultantsGetActiveAllDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_UsersEhsConsultants_GetAll_Active" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

<data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" Sort="CompanyName ASC">
</data:CompaniesDataSource>

<data:CompaniesDataSource ID="CompaniesDataSource2" runat="server" Sort="CompanyAbn ASC">
</data:CompaniesDataSource>

