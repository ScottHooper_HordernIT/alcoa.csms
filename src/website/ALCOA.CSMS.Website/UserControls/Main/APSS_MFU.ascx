﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Library_APSS_MFU" Codebehind="APSS_MFU.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="2" cellspacing="0" width="729">
    <tr>
        <td class="pageName" colspan="7">
            <span class="title">Library Documents
                <asp:Button ID="btnEdit" runat="server" BackColor="White" BorderColor="White" BorderStyle="None"
                        Font-Names="Verdana" Font-Size="XX-Small" Font-Underline="True" ForeColor="Red"
                        OnClick="Button2_Click" Text="(Edit)" />
            </span>
            <br />
            <span class="date">Categories:</span>
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/APSS_Top9.aspx">Must Read These...</asp:LinkButton>,
                Most Frequently Used, 
                <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/APSS_Search.aspx">Search</asp:LinkButton>.<br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" />
            <br />
        </td>
        <td class="pageName" colspan="1"></td>
    </tr>
    <tr>
        <td height="110" colspan="8">
            <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" OnRowInserting="grid_OnRowInserting"
                OnHtmlRowCreated="grid_RowCreated" 
                OnInitNewRow="grid_InitNewRow" OnRowUpdating="grid_RowUpdating" 
                DataSourceID="DocumentsDataSource1" KeyFieldName="DocumentId">
                <Columns>
                <dxwgv:GridViewCommandColumn Caption="Action" Visible="False" VisibleIndex="0" Width="50px">
                            <EditButton Visible="True">
                            </EditButton>
                            <NewButton Visible="True">
                            </NewButton>
                            <DeleteButton Visible="True">
                            </DeleteButton>
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="DocumentId" ReadOnly="True" Visible="False"
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn ReadOnly="True" VisibleIndex="0" Width="8px">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <Paddings Padding="5px" />
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentName" Caption="Document Name" VisibleIndex="2" Width="700px" SortIndex="0" SortOrder="Ascending">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileName" Caption="File Name" VisibleIndex="1" Width="100px" ToolTip="Click to download file.">
                            <DataItemTemplate>
                             <table cellpadding="0" cellspacing="0"><tr>
                             <td style="width:120px">
                                 <dxe:ASPxHyperLink ID="hlFileDownload" runat="server" Text="">
                                 </dxe:ASPxHyperLink>
                             </td>
                             </tr></table>
                        </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentFileDescription" Visible="False" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="DocumentType" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px"
                        Width="11px" />
                    <ExpandedButton Height="12px"
                        Width="11px" />
                    <DetailCollapsedButton Height="12px"
                        Width="11px" />
                    <DetailExpandedButton Height="12px"
                        Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsEditing Mode="Inline" />
                <SettingsPager PageSize="100">
                    <AllButton Visible="True"></AllButton>
                </SettingsPager>
                <SettingsBehavior ConfirmDelete="True" />
            </dxwgv:ASPxGridView>
            
            <%--<data:DocumentsDataSource
            ID="DocumentsDataSource"
            runat="server"
            EnableDeepLoad="False"
            EnableSorting="true"
            SelectMethod="GetPaged">
                <Parameters>
                      <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
                         <Filters>
                            <data:DocumentsFilter Column="DocumentType" DefaultValue="MFU" SessionField="MFU" />
                         </Filters>
                      </data:SqlParameter>
                </Parameters>
            </data:DocumentsDataSource>--%>
            <data:DocumentsDataSource
                    ID="DocumentsDataSource1"
                    runat="server"
                    EnableDeepLoad="False"
                    EnableSorting="true"
                    SelectMethod="GetPaged">
                    <Parameters>
                      <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
                         <Filters>
                            <data:DocumentsFilter Column="DocumentType" DefaultValue="MFU"/>
                           
                         </Filters>
                      </data:SqlParameter>
                   </Parameters>
                </data:DocumentsDataSource>
        </td>
    </tr>
</table>

<br />