﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits=" ALCOA.CSMS.Website.UserControls.Admin.MaintainLabourClassifications" Codebehind="MaintainLabourClassifications.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<dxwgv:ASPxGridView ID="grid" runat="server" ClientInstanceName="grid" AutoGenerateColumns="False" Width="100%"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    OnCommandButtonInitialize="grid_CommandButtonInitialize" OnCellEditorInitialize="grid_CellEditorInitialize"
    OnRowValidating="grid_RowValidating" OnRowUpdating="grid_RowUpdating" OnRowInserting="grid_RowInserting"
    KeyFieldName="LabourClassTypeId">
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue">
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
    </Styles>
    <Styles Header-Wrap="True" />
    <Columns>
        <dxwgv:GridViewCommandColumn AllowDragDrop="False" Caption="Action" VisibleIndex="0" Width="100">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="LabourClassTypeId" VisibleIndex="1" Width="100">
            <Settings SortMode="Value" />
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="LabourClassTypeDesc" VisibleIndex="2">
            <Settings SortMode="DisplayText" />
            <EditFormSettings Visible="True" VisibleIndex="0" ColumnSpan="2" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataCheckColumn Caption="Is Active?" FieldName="LabourClassTypeIsActive" VisibleIndex="3" Width="100">
            <EditFormSettings Visible="True" VisibleIndex="1" ColumnSpan="1" />
        </dxwgv:GridViewDataCheckColumn>
    </Columns>
    <SettingsEditing EditFormColumnCount="3" Mode="EditFormAndDisplayRow">
    </SettingsEditing>
    <Settings ShowGroupPanel="False"  ShowFilterRow="False" ShowFilterBar="Hidden" />
    <SettingsBehavior ColumnResizeMode="Control" />
    <SettingsPager Mode="ShowAllRecords">
    </SettingsPager>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>
<div align="right" style="padding-top: 2px;">
    <uc1:ExportButtons ID="ucExportButtons" runat="server"/>
</div>
