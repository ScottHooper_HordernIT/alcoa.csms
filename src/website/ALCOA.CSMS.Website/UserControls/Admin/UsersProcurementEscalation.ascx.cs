﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.Library;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.Data;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class ProcurementEscalation : System.Web.UI.UserControl
    {
        public Repo.CSMS.Service.Database.IUsersProcurementService UsersProcurementService { get; set; }
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                if (!postBack)
                { //first time load
                }
            }
            Helper.ExportGrid.Settings(ucExportButtons, "Procurement", "gridProcurement");
        }

        protected void grid_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e) //Default Values 
        {
            if(e.NewValues["Enabled"] == null)
            { 
                e.NewValues["Enabled"] = true; 
            }
        }

        protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
            Label lbl1 = gridProcurement.FindRowCellTemplateControl(e.VisibleIndex, null, "noSQProcContact") as Label;
            Label lbl2 = gridProcurement.FindRowCellTemplateControl(e.VisibleIndex, null, "noSQFuncProcMgr") as Label;
            int UserId = (int)gridProcurement.GetRowValues(e.VisibleIndex, "UserId");

            //noSQFuncProcMgr
            string c1 = "-";
            string c2 = "-";

            QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
            DataSet ds1 = qirService.GetCountByQuestionIdUserId("2", UserId.ToString());
            DataSet ds2 = qirService.GetCountByQuestionIdUserId("2_1", UserId.ToString());

            if (ds1.Tables[0] != null)
            {
                foreach (DataRow dr in ds1.Tables[0].Rows)
                {
                    c1 = dr[0].ToString();
                }
            }

            if (ds2.Tables[0] != null)
            {
                foreach (DataRow dr in ds2.Tables[0].Rows)
                {
                    c2 = dr[0].ToString();
                }
            }

            if (c1 == "0") c1 = "-";
            if (c2 == "0") c2 = "-";

            lbl1.Text = c1;
            lbl2.Text = c2;
        }

        protected void grid_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if(e.Column.FieldName == "UserId")
            {
                if(gridProcurement.IsNewRowEditing)
                {
                    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
                    comboBox.DataSourceID = "NewUsersDataSource";
                    comboBox.DataBind();
                }
                else
                {
                    e.Editor.ReadOnly = true;
                    e.Editor.ClientEnabled = false;
                }
            }
        }

        protected void dsUsersProcurement_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IUsersProcurementService>();
        }
    }
}