using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using KaiZen.Library;

public partial class UserControls_SqQuestionnaire_Questionnaire3Verification_8 : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    int sNo = 8;
    string qNo = "";
    bool ReadOnly = false;
    bool AssessmentMode = false;

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            bool print = false;
            try
            {
                rpAssess1.Visible = false;
                rpAssess2.Visible = false;
                rpAssess3.Visible = false;

                qNo = Request.QueryString["q"];
                if (Request.QueryString["printlayout"] != null)
                {
                    if (Request.QueryString["printlayout"] == "yes")
                    {
                        print = true;
                    }
                }

                if(!String.IsNullOrEmpty(qNo))
                {
                    int _qNo = Convert.ToInt32(qNo);
                    QuestionnaireService _qs = new QuestionnaireService();
                    Questionnaire _q =  _qs.GetByQuestionnaireId(_qNo);

                    if (_q == null)
                    {
                        throw new Exception("Invalid Questionnaire Id");
                    }
                    else
                    {
                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(_q.CompanyId);
                        if (c.Deactivated == true
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                        {
                            ReadOnlyQuestionnaire();
                            if (c.Deactivated == true)
                            {

                                Label1.Text += ". Company De-Activated.";
                                Label2.Text += ". Company De-Activated.";
                            }
                        }

                        QuestionnaireVerificationSectionService qvsService = new QuestionnaireVerificationSectionService();
                        QuestionnaireVerificationSection qvs = qvsService.GetByQuestionnaireId(_qNo);

                        bool Assessed = false;
                        QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
                        TList<QuestionnaireVerificationAssessment> qvaTlist_all;
                        qvaTlist_all = qvaService.GetByQuestionnaireIdSectionId(_q.QuestionnaireId, 8);
                        if (qvaTlist_all != null)
                        {
                            if (qvaTlist_all.Count == 3)
                            {
                                Assessed = true;
                            }
                        }

                        if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
                        {
                            if (_q.CompanyId != auth.CompanyId)
                            {
                                throw new Exception("You are not authorised to view this page.");
                            }

                        }

                        if (qvs != null)
                        {
                            if (qvs.Section8Complete.HasValue)
                            {
                                LoadAnswers(_qNo);
                            }
                        }

                        if ((_q.VerificationStatus == (int)QuestionnaireStatusList.Incomplete && Assessed) || _q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete)
                        {
                            rpAssess1.Visible = true;
                            rpAssess2.Visible = true;
                            rpAssess3.Visible = true;
                            assess1Approval.ReadOnly = true;
                            assess2Approval.ReadOnly = true;
                            assess3Approval.ReadOnly = true;
                            assess1Comments.ReadOnly = true;
                            assess2Comments.ReadOnly = true;
                            assess3Comments.ReadOnly = true;
                            assess1Comments.NullText = "(No Comments Available)";
                            assess2Comments.NullText = "(No Comments Available)";
                            assess3Comments.NullText = "(No Comments Available)";
                        }

                        if (_q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        {
                            ReadOnlyQuestionnaire();
                            if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed)
                            {
                                Label1.Text += ". Questionnaire Being Assessed.";
                                Label2.Text += ". Questionnaire Being Assessed.";
                                if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
                                {
                                    AssessmentMode = true;
                                    rpAssess1.Visible = true;
                                    rpAssess2.Visible = true;
                                    rpAssess3.Visible = true;
                                    SetButtonsSaveText(true);
                                }
                            }
                            if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete && Assessed)
                            {
                                if (Assessed)
                                {
                                    if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
                                    {
                                        rpAssess1.Visible = true;
                                        rpAssess2.Visible = true;
                                        rpAssess3.Visible = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("No Questionnaire # Specified");
                }

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        SetInivisbleAllGetHelpOverlayDivs();
                        ReadOnly = true;
                        break;
                    case ((int)RoleList.Contractor):
                        //
                        break;
                    case ((int)RoleList.Administrator):
                        SetInivisbleAllGetHelpOverlayDivs();
                        break;
                    case((int)RoleList.PreQual):
                        break;
                    default:
                        // RoleList.Disabled or No Role
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                        break;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (print)
            {
                //disable save options etc.
                btnHome.Enabled = false;
                btnHome2.Enabled = false;
                btnPrevious.Enabled = false;
                btnPrevious2.Enabled = false;
                btnNext.Enabled = false;
                btnNext2.Enabled = false;
                pnlTime.Visible = false;
                Label1.Text = "";
                Label2.Text = "";
            }
        }
    }
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            if (ValidateFiles() == true)
            {
                SaveAnswers();
                qNo = Request.QueryString["q"];
                string url = String.Format("~/SafetyPQ_QuestionnaireVerificationSection.aspx{0}", QueryStringModule.Encrypt(String.Format("s={0}&q={1}", (sNo - 1), qNo)));
                Response.Redirect(url, false);
            }
            else
            {
                ASPxPopupControl2.ShowOnPageLoad = true;
            }
        }
    }
    protected void btnHome_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            if (ValidateFiles() == true)
            {
                SaveAnswers();
                qNo = Request.QueryString["q"];
                string url = String.Format("~/SafetyPQ_QuestionnaireVerification.aspx{0}", QueryStringModule.Encrypt(String.Format("q={0}", qNo)));
                Response.Redirect(url, false);
            }
            else
            {
                ASPxPopupControl2.ShowOnPageLoad = true;
            }
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            if (ValidateFiles() == true)
            {
                SaveAnswers();
                qNo = Request.QueryString["q"];
                string url = String.Format("~/SafetyPQ_QuestionnaireVerificationSection.aspx{0}", QueryStringModule.Encrypt(String.Format("s={0}&q={1}", (sNo + 1), qNo)));
                Response.Redirect(url, false);
            }
            else
            {
                ASPxPopupControl2.ShowOnPageLoad = true;
            }
        }
    }

    protected void SaveAnswers()
    {
        try
        {
            qNo = Request.QueryString["q"];
            if (!String.IsNullOrEmpty(qNo))
            {
                int _qNo = Convert.ToInt32(qNo);
                QuestionnaireService _qs = new QuestionnaireService();
                Questionnaire _q = _qs.GetByQuestionnaireId(_qNo);

                //if (_q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                CompaniesService cService = new CompaniesService();
                Companies c = cService.GetByCompanyId(_q.CompanyId);
                if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete
                    || c.Deactivated == true
                    || c.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete
                    || c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required)
                {
                    ReadOnly = true;
                }
                if (_q.VerificationStatus == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
                    {
                        AssessmentMode = true;
                    }
                    ReadOnly = true;
                }
                if (!ReadOnly)
                {
                    _q.VerificationModifiedByUserId = auth.UserId;
                    _q.VerificationModifiedDate = DateTime.Now;
                    //_q.ModifiedByUserId = auth.UserId;
                    //_q.ModifiedDate = DateTime.Now;
                    _qs.Save(_q);

                    QuestionnaireVerificationSectionService qvsService = new QuestionnaireVerificationSectionService();
                    QuestionnaireVerificationSection qvs = qvsService.GetByQuestionnaireId(_qNo);
                    if (qvs == null)
                    {
                        qvs = new QuestionnaireVerificationSection();
                        qvs.QuestionnaireId = _qNo;
                    }

                    int i = 0;
                    if (String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.GetVal(rb1.Value))) i++;
                    if (String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.GetVal(rb2.Value))) i++;
                    if (String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.GetVal(rb3.Value))) i++;

                    bool f1_HasFile = false;
                    bool f2_HasFile = false;
                    bool f3_HasFile = false;

                    if (f1.UploadedFiles != null && f1.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(f1.UploadedFiles[0].FileName) && f1.UploadedFiles[0].IsValid)
                        {
                            f1_HasFile = true;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(f1l.Text)) f1_HasFile = true;
                        }
                    }
                    else { if (!String.IsNullOrEmpty(f1l.Text)) f1_HasFile = true; }

                    if (f2.UploadedFiles != null && f2.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(f2.UploadedFiles[0].FileName) && f2.UploadedFiles[0].IsValid)
                        {
                            f2_HasFile = true;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(f2l.Text)) f2_HasFile = true;
                        }
                    }
                    else { if (!String.IsNullOrEmpty(f2l.Text)) f2_HasFile = true; }

                    if (f3.UploadedFiles != null && f3.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(f3.UploadedFiles[0].FileName) && f3.UploadedFiles[0].IsValid)
                        {
                            f3_HasFile = true;
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(f3l.Text)) f3_HasFile = true;
                        }
                    }
                    else { if (!String.IsNullOrEmpty(f3l.Text)) f3_HasFile = true; }

                    if (String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.GetVal(m1.Text)) && !f1_HasFile) i++;
                    if (String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.GetVal(m2.Text)) && !f2_HasFile) i++;
                    if (String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.GetVal(m3.Text)) && !f3_HasFile) i++;

                    //if (!f1_HasFile) i++;
                    //if (!f2_HasFile) i++;
                    //if (!f3_HasFile) i++;

                    if (i > 0) { qvs.Section8Complete = false; } else { qvs.Section8Complete = true; };

                    Helper.Questionnaire.Supplier.Verification.SaveAnswer(_qNo, sNo, 1, Helper.Questionnaire.Supplier.Verification.GetVal(rb1.Value), Helper.Questionnaire.Supplier.Verification.GetByte(m1.Text), auth.UserId);
                    Helper.Questionnaire.Supplier.Verification.SaveAnswer(_qNo, sNo, 2, Helper.Questionnaire.Supplier.Verification.GetVal(rb2.Value), Helper.Questionnaire.Supplier.Verification.GetByte(m2.Text), auth.UserId);
                    Helper.Questionnaire.Supplier.Verification.SaveAnswer(_qNo, sNo, 4, Helper.Questionnaire.Supplier.Verification.GetVal(rb3.Value), Helper.Questionnaire.Supplier.Verification.GetByte(m3.Text), auth.UserId);

                    Helper.Questionnaire.Supplier.Verification.SaveAttachment(_qNo, sNo, 1, f1, auth.UserId);
                    Helper.Questionnaire.Supplier.Verification.SaveAttachment(_qNo, sNo, 2, f2, auth.UserId);
                    Helper.Questionnaire.Supplier.Verification.SaveAttachment(_qNo, sNo, 4, f3, auth.UserId);

                    qvsService.Save(qvs);
                }
                if (AssessmentMode)
                {
                    //Assessment
                    Helper.Questionnaire.Supplier.Verification.SaveAssessment(_qNo, sNo, 1, assess1Approval.Value, assess1Comments.Text, auth.UserId);
                    Helper.Questionnaire.Supplier.Verification.SaveAssessment(_qNo, sNo, 2, assess2Approval.Value, assess2Comments.Text, auth.UserId);
                    Helper.Questionnaire.Supplier.Verification.SaveAssessment(_qNo, sNo, 3, assess3Approval.Value, assess3Comments.Text, auth.UserId);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void LoadAnswers(int q)
    {
        rb1.Value = Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sNo, 1);
        rb2.Value = Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sNo, 2);
        rb3.Value = Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sNo, 4);

        if (rb1.Value != null) div1.Visible = false;
        if (rb2.Value != null) div2.Visible = false;
        if (rb3.Value != null) div3.Visible = false;

        m1.Text = Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(q, sNo, 1);
        m2.Text = Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(q, sNo, 2);
        m3.Text = Helper.Questionnaire.Supplier.Verification.LoadAnswerByte(q, sNo, 4);

        Helper.Questionnaire.Supplier.Verification.LoadAttachment(q, sNo, 1, f1e, f1l);
        Helper.Questionnaire.Supplier.Verification.LoadAttachment(q, sNo, 2, f2e, f2l);
        Helper.Questionnaire.Supplier.Verification.LoadAttachment(q, sNo, 4, f3e, f3l);

        //Assessment
        QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();

        QuestionnaireVerificationAssessment qva = qvaService.GetByQuestionnaireIdSectionIdQuestionId(q, sNo, 1);
        if (qva != null)
        {
            assess1Approval.Value = NumberUtilities.Convert.BoolToInt(qva.AssessorApproval);
            assess1Comments.Text = qva.AssessorComment;

            if (auth.RoleId != (int)RoleList.Administrator && NumberUtilities.Convert.BoolToInt(qva.AssessorApproval) == 1)
            {
                rb1.ReadOnly = true;
                m1.ReadOnly = true;
                f1.Enabled = false;
            }
        }
        qva = null;

        qva = qvaService.GetByQuestionnaireIdSectionIdQuestionId(q, sNo, 2);
        if (qva != null)
        {
            assess2Approval.Value = NumberUtilities.Convert.BoolToInt(qva.AssessorApproval);
            assess2Comments.Text = qva.AssessorComment;

            if (auth.RoleId != (int)RoleList.Administrator && NumberUtilities.Convert.BoolToInt(qva.AssessorApproval) == 1)
            {
                rb2.ReadOnly = true;
                m2.ReadOnly = true;
                f2.Enabled = false;
            }
        }
        qva = null;

        qva = qvaService.GetByQuestionnaireIdSectionIdQuestionId(q, sNo, 3);
        if (qva != null)
        {
            assess3Approval.Value = NumberUtilities.Convert.BoolToInt(qva.AssessorApproval);
            assess3Comments.Text = qva.AssessorComment;

            if (auth.RoleId != (int)RoleList.Administrator && NumberUtilities.Convert.BoolToInt(qva.AssessorApproval) == 1)
            {
                rb3.ReadOnly = true;
                m3.ReadOnly = true;
                f3.Enabled = false;
            }
        }
        qva = null;
    }
    public void SetButtonsSaveText(bool enable)
    {
        if (enable)
        {
            btnHome.Text = "[ Home (and Save) ]";
            btnPrevious.Text = "<< Previous Section (and Save)";
            btnNext.Text = "Next Section (and Save) >>";
            btnHome2.Text = btnHome.Text;
            btnPrevious2.Text = btnPrevious.Text;
            btnNext2.Text = btnNext.Text;
        }
        else
        {
            btnHome.Text = "[ Home ]";
            btnPrevious.Text = "<< Previous Section ";
            btnNext.Text = "Next Section >>";
            btnHome2.Text = btnHome.Text;
            btnPrevious2.Text = btnPrevious.Text;
            btnNext2.Text = btnNext.Text;
        }
    }

    public void ReadOnlyQuestionnaire()
    {
        SetButtonsSaveText(false);
        rb1.ReadOnly = true;
        rb2.ReadOnly = true;
        rb3.ReadOnly = true;
        m1.ReadOnly = true;
        m2.ReadOnly = true;
        m3.ReadOnly = true;
        f1.Enabled = false;
        f2.Enabled = false;
        f3.Enabled = false;
        pnlTime.Visible = false;

        Label1.Text = "Read Only";
        Label2.Text = "Read Only";
    }
    protected bool ValidateFiles()
    {
        bool valid = true;
        if (f1.Enabled)
        {
            if (f1.UploadedFiles != null && f1.UploadedFiles.Length > 0)
            {
                if (f1.UploadedFiles[0].IsValid == false) valid = false;
            }
            if (f2.UploadedFiles != null && f2.UploadedFiles.Length > 0)
            {
                if (f2.UploadedFiles[0].IsValid == false) valid = false;
            }
            if (f3.UploadedFiles != null && f3.UploadedFiles.Length > 0)
            {
                if (f3.UploadedFiles[0].IsValid == false) valid = false;
            }
        }
        return valid;
    }

    protected void SetInivisbleAllGetHelpOverlayDivs()
    {
        div1.Visible = false;
        div2.Visible = false;
        div3.Visible = false;
    }
}
