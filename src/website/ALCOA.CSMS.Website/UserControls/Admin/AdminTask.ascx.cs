﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using System.Drawing;

using System.Text;

using System.Net.Mail;

using System.IO;
using System.Data;

using DevExpress.Web.ASPxEditors;


namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class AdminTask : System.Web.UI.UserControl
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");

                ASPxRoundPanel1.Visible = false;
                ASPxRoundPanel2.Visible = false;
                lblHappy.Visible = false;
                btnAcceptChanges.Visible = false;
                Prefill();
            }
        }

        protected void btnPreviewChanges_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsPostBack)
                {
                    ASPxRoundPanel1.Visible = true;
                    ASPxRoundPanel2.Visible = true;
                    lblHappy.Visible = true;
                    btnAcceptChanges.Visible = true;

                    Validate();
                    PerformTask(true); //preview
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void Prefill()
        {
            int AdminTaskId = 0;

            Int32.TryParse(Request.QueryString["q"].ToString(), out AdminTaskId);

            lblTaskNo.Text = "(NEW)";

            if (AdminTaskId > 0)
            {
                AdminTaskService atService = new AdminTaskService();
                KaiZen.CSMS.Entities.AdminTask at = atService.GetByAdminTaskId(AdminTaskId);

                lblTaskNo.Text = at.AdminTaskId.ToString();

                //TODO: Load DateOpened, OpenedByUserId

                cbAccess.Value = at.CsmsAccessId;
                cbCompany.Value = at.CompanyId;
                cbSource.Value = at.AdminTaskSourceId;
                cbType.Value = at.AdminTaskTypeId;

                tbLogin.Text = at.Login;
                tbEmail.Text = at.EmailAddress;
                tbFirstName.Text = at.FirstName;
                tbLastName.Text = at.LastName;
                tbJobTitle.Text = at.JobTitle;
                tbMobileNumber.Text = at.MobileNo;
                tbTelephoneNo.Text = at.TelephoneNo;
                tbFaxNo.Text = at.FaxNo;

                mTaskComments.Text = at.AdminTaskComments;

                if (at.AdminTaskStatusId == (int)AdminTaskStatusList.Closed)
                {
                    cbAccess.Enabled = false;
                    cbCompany.Enabled = false;
                    cbSource.Enabled = false;
                    cbType.Enabled = false;
                    tbLogin.Enabled = false;
                    tbEmail.Enabled = false;
                    tbFirstName.Enabled = false;
                    tbLastName.Enabled = false;
                    tbJobTitle.Enabled = false;
                    tbMobileNumber.Enabled = false;
                    tbTelephoneNo.Enabled = false;
                    tbFaxNo.Enabled = false;

                    btnPreviewChanges.Text = "Preview Email";
                    btnAcceptChanges.Text = "Re-Send Email";
                }
            }
            else
            {
                //btnPreviewChanges.Enabled = false;
                //btnAcceptChanges.Enabled = false;
                throw new Exception("Invalid Admin Task");
            }
        }

        protected void PerformTask(bool Preview)
        {
            try
            {
                int i = 1;
                bool Changes = false;

                Configuration config = new Configuration();

                int AdminTaskId = 0;
                Int32.TryParse(Request.QueryString["q"].ToString(), out AdminTaskId);

                AdminTaskService atService = new AdminTaskService();
                KaiZen.CSMS.Entities.AdminTask at = atService.GetByAdminTaskId(AdminTaskId);

                bool ResendEmail = false;
                if (at.AdminTaskStatusId == (int)AdminTaskStatusList.Closed) ResendEmail = true;

                at.CompanyId = (int)cbCompany.SelectedItem.Value;

                at.Login = tbLogin.Text;
                at.EmailAddress = tbEmail.Text;
                at.FirstName = tbFirstName.Text;
                at.LastName = tbLastName.Text;
                at.JobTitle = tbJobTitle.Text;
                at.MobileNo = tbMobileNumber.Text;
                at.TelephoneNo = tbTelephoneNo.Text;
                at.FaxNo = tbFaxNo.Text;

                if (at == null) throw new Exception("Error Loading Admin Task Details");

                #region 1) Create/Update User Details

                UsersService uService = new UsersService();
                Users u = uService.GetByUserLogon(tbLogin.Text);

                if (u != null)
                {
                    if (u.Email != tbEmail.Text) u.Email = at.EmailAddress;
                    if (u.CompanyId != (int)cbCompany.SelectedItem.Value) u.CompanyId = at.CompanyId;
                    if (u.FirstName != tbFirstName.Text) u.FirstName = at.FirstName;
                    if (u.LastName != tbLastName.Text) u.LastName = at.LastName;
                    if (u.Title != tbJobTitle.Text) u.Title = at.JobTitle;
                    if (u.MobNo != tbMobileNumber.Text) u.MobNo = at.MobileNo;
                    if (u.PhoneNo != tbTelephoneNo.Text) u.PhoneNo = at.TelephoneNo;
                    if (u.FaxNo != tbFaxNo.Text) u.FaxNo = at.FaxNo;
                }
                else
                {
                    u = new Users();
                    u.ModifiedByUserId = 0;

                    u.UserLogon = at.Login;
                    u.Email = at.EmailAddress;
                    u.CompanyId = at.CompanyId;
                    u.FirstName = at.FirstName;
                    u.LastName = at.LastName;
                    u.Title = at.JobTitle;
                    u.MobNo = at.MobileNo;
                    u.PhoneNo = at.TelephoneNo;
                    u.FaxNo = at.FaxNo;
                }

                CompaniesService cService = new CompaniesService();
                Companies c = cService.GetByCompanyId(at.CompanyId);

                if (c == null) throw new Exception("Error Loading Company Details.");

                switch ((int)cbType.SelectedItem.Value)
                {
                    case (int)AdminTaskTypeList.Contractor_Request_for_Access:
                        if (IsCompanySqExpired(at.CompanyId))
                        {
                            u.RoleId = (int)RoleList.PreQual;
                        }
                        else
                        {
                            u.RoleId = (int)RoleList.Contractor;
                        }
                        break;
                    case (int)AdminTaskTypeList.Contractor_Safety_Qualification_Pre_Qualification:
                        u.RoleId = (int)RoleList.PreQual;
                        break;
                    case (int)AdminTaskTypeList.Contractor_Safety_Qualification_Re_Qualification:
                        if (IsCompanySqExpired(at.CompanyId))
                        {
                            u.RoleId = (int)RoleList.PreQual;
                        }
                        else
                        {
                            u.RoleId = (int)RoleList.Contractor;
                        }
                        break;
                    case (int)AdminTaskTypeList.Alcoa_EBI_Data_User:
                        u.RoleId = (int)RoleList.Reader;
                        break;
                    case (int)AdminTaskTypeList.Alcoa_Financial_Reports_User:
                        u.RoleId = (int)RoleList.Reader;
                        break;
                    case (int)AdminTaskTypeList.Alcoa_H_S_Assessor_Access:
                        u.RoleId = (int)RoleList.Reader;
                        break;
                    case (int)AdminTaskTypeList.Alcoa_Procurement_Access:
                        u.RoleId = (int)RoleList.Reader;
                        break;
                    case (int)AdminTaskTypeList.Alcoa_Reader_Access_Enquiry:
                        u.RoleId = (int)RoleList.Reader;
                        break;
                    case (int)AdminTaskTypeList.Alcoa_Training_Package_User:
                        u.RoleId = (int)RoleList.Reader;
                        break;
                    default:
                        break;
                }

                lblChanges.Text = "";

                if (!ResendEmail)
                {

                    if (u.EntityState == EntityState.Changed || u.EntityState == EntityState.Added)
                    {
                        if (u.EntityState == EntityState.Changed)
                        {
                            lblChanges.Text += i + ") User found. User Details differ. User Details will be updated.<br />";
                            Changes = true;
                            i++;
                        }
                        else if (u.EntityState == EntityState.Added)
                        {
                            lblChanges.Text += i + ") User not found. User will be created.<br />";
                            i++;
                            Changes = true;
                        }

                        if (!Preview) uService.Save(u);
                    }
                    else if (u.EntityState == EntityState.Unchanged)
                    {
                        lblChanges.Text += i + ") User found. User Details match. No Changes Required.<br />";
                        i++;
                    }

                    if(!Preview) UpdatePresentlyWith(u.CompanyId);

                #endregion

                #region 2) Additional Tasks (if required)
                    switch ((int)cbType.SelectedItem.Value)
                    {
                        case (int)AdminTaskTypeList.Contractor_Request_for_Access:
                            //nothing else required.
                            break;
                        case (int)AdminTaskTypeList.Contractor_Safety_Qualification_Pre_Qualification:
                            //nothing else required.
                            break;
                        case (int)AdminTaskTypeList.Contractor_Safety_Qualification_Re_Qualification:
                            //nothing else required.
                            break;
                        case (int)AdminTaskTypeList.Alcoa_EBI_Data_User:
                            UsersEbiService usersEbiService = new UsersEbiService();
                            UsersEbi usersEbi = usersEbiService.GetByUserId(u.UserId);
                            if (usersEbi == null)
                            {
                                if (!ResendEmail)
                                {
                                    lblChanges.Text += i + ") User will be added to EBI users table.<br />";
                                    i++;
                                    Changes = true;

                                    if (!Preview)
                                    {
                                        usersEbi = new UsersEbi();
                                        usersEbi.UserId = u.UserId;
                                        usersEbi.ModifiedDate = DateTime.Now;
                                        usersEbiService.Insert(usersEbi);
                                    }
                                }
                            }
                            break;
                        case (int)AdminTaskTypeList.Alcoa_Financial_Reports_User:
                            //isPrivilegedUser
                            //Added for DT2435
                            UsersPrivilegedService usersPrivilegedService = new UsersPrivilegedService();
                            UsersPrivileged usersPrivileged = usersPrivilegedService.GetByUserId(u.UserId);
                            if (usersPrivileged == null)
                            {
                                if (!ResendEmail)
                                {
                                    lblChanges.Text += i + ") User will be added to Financial users table.<br />";
                                    i++;
                                    Changes = true;

                                    if (!Preview)
                                    {
                                        usersPrivileged = new UsersPrivileged();
                                        usersPrivileged.UserId = u.UserId;
                                        usersPrivilegedService.Insert(usersPrivileged);
                                    }
                                }
                            }
                            break;
                            //End

                        case (int)AdminTaskTypeList.Alcoa_H_S_Assessor_Access:
                            EhsConsultantService hsService = new EhsConsultantService();
                            EhsConsultant hs = hsService.GetByUserId(u.UserId);
                            if (hs == null)
                            {
                                if (!ResendEmail)
                                {
                                    Changes = true;
                                    lblChanges.Text += i + ") User will be added to H&S Assessors users table.<br />";
                                    i++;

                                    if (!Preview)
                                    {
                                        hs = new EhsConsultant();
                                        hs.UserId = u.UserId;
                                        hs.Enabled = true;
                                        hsService.Insert(hs);
                                    }
                                }
                            }
                            else
                            {
                                if (!hs.Enabled)
                                {
                                    if (!ResendEmail)
                                    {
                                        Changes = true;
                                        lblChanges.Text += i + ") User exists in H&S Assessors users table but was disabled. User will be enabled.<br />";
                                        i++;

                                        if (!Preview)
                                        {
                                            hs.Enabled = true;
                                            hsService.Save(hs);
                                        }
                                    }
                                }
                            }
                            break;
                        case (int)AdminTaskTypeList.Alcoa_Procurement_Access:
                            UsersProcurementService usersProcurementService = new UsersProcurementService();
                            UsersProcurement usersProcurement = usersProcurementService.GetByUserId(u.UserId);
                            if (usersProcurement == null)
                            {
                                if (!ResendEmail)
                                {
                                    lblChanges.Text += i + ") User will be added to Procurement users table.<br />";
                                    i++;
                                    Changes = true;

                                    if (!Preview)
                                    {
                                        usersProcurement = new UsersProcurement();
                                        usersProcurement.UserId = u.UserId;
                                        usersProcurement.Enabled = true;
                                        //Added by Pankaj Dikshit for <Issue ID# 2438, 04-June-2012>
                                        if (usersProcurement.ModifiedDate == DateTime.MinValue)
                                            usersProcurement.ModifiedDate = DateTime.Now;
                                        usersProcurementService.Save(usersProcurement);
                                    }
                                }
                            }
                            else
                            {
                                if (!usersProcurement.Enabled)
                                {
                                    if (!ResendEmail)
                                    {
                                        Changes = true;
                                        lblChanges.Text += i + ") User exists in Procurement users table but was disabled. User will be enabled.<br />";
                                        i++;

                                        if (!Preview)
                                        {
                                            usersProcurement.Enabled = true;
                                            usersProcurementService.Save(usersProcurement);
                                        }
                                    }
                                }
                            }
                            break;
                        case (int)AdminTaskTypeList.Alcoa_Reader_Access_Enquiry:
                            //nothing else is required.
                            break;
                        case (int)AdminTaskTypeList.Alcoa_Training_Package_User:
                            UsersPrivileged2Service usersPrivileged2Service = new UsersPrivileged2Service();
                            UsersPrivileged2 usersPrivileged2 = usersPrivileged2Service.GetByUserId(u.UserId);
                            if (usersPrivileged2 == null)
                            {
                                if (!ResendEmail)
                                {
                                    lblChanges.Text += i + ") User will be added to Training Package users table.<br />";
                                    i++;
                                    Changes = true;

                                    if (!Preview)
                                    {
                                        usersPrivileged2 = new UsersPrivileged2();
                                        usersPrivileged2.UserId = u.UserId;
                                        usersPrivileged2Service.Insert(usersPrivileged2);
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                    #endregion

                //if (String.IsNullOrEmpty(lblChanges.Text)) lblChanges.Text = "No Changes.";

                #region 3) Send Email
                lblChanges.Text += i + ") E-Mail will be sent (see preview below):<br />";

                using (MailMessage mail = new MailMessage())
                {
                    AdminTaskEmailTemplateService atetService = new AdminTaskEmailTemplateService();
                    using (KaiZen.CSMS.Entities.AdminTaskEmailTemplate atet = atetService.GetByAdminTaskTypeId((int)cbType.SelectedItem.Value))
                    {
                        tbFrom.Text = String.Format("AUA Alcoa Contractor Services Team ({0})", config.GetValue(ConfigList.ContactEmail));
                        mail.From = new MailAddress(config.GetValue(ConfigList.ContactEmail), "AUA Alcoa Contractor Services Team");

                        AdminTaskEmailTemplateRecipientService atetrService = new AdminTaskEmailTemplateRecipientService();
                        AdminTaskEmailRecipientService aterService = new AdminTaskEmailRecipientService();

                        tbTo.Text = "";
                        using (TList<AdminTaskEmailTemplateRecipient> atetrList = atetrService.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(atet.AdminTaskEmailTemplateId,
                                                                                                                                                    (int)CsmsEmailRecipientTypeList.To))
                        {
                            int count = 1;
                            foreach (AdminTaskEmailTemplateRecipient atetr in atetrList)
                            {
                                string Recipient = GetEmail(at, c, u, atetr.AdminTaskEmailRecipientId);

                                if (!string.IsNullOrEmpty(Recipient))
                                {
                                    if (count == atetrList.Count)
                                    {
                                        tbTo.Text += Recipient;
                                    }
                                    else
                                    {
                                        tbTo.Text += Recipient + "; ";
                                    }
                                    mail.To.Add(Recipient);
                                }
                                count++;
                            }
                        }

                        tbCc.Text = "";
                        using (TList<AdminTaskEmailTemplateRecipient> atetrList = atetrService.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(atet.AdminTaskEmailTemplateId,
                                                                                                                                                    (int)CsmsEmailRecipientTypeList.Cc))
                        {
                            int count = 1;
                            foreach (AdminTaskEmailTemplateRecipient atetr in atetrList)
                            {
                                string Recipient = GetEmail(at, c, u, atetr.AdminTaskEmailRecipientId);

                                if (!string.IsNullOrEmpty(Recipient))
                                {
                                    if (count == atetrList.Count)
                                    {
                                        tbCc.Text += Recipient;
                                    }
                                    else
                                    {
                                        tbCc.Text += Recipient + "; ";
                                    }
                                    mail.CC.Add(Recipient);
                                }
                                count++;
                            }
                        }

                        tbBcc.Text = "";
                        using (TList<AdminTaskEmailTemplateRecipient> atetrList = atetrService.GetByAdminTaskEmailTemplateIdCsmsEmailRecipientTypeId(atet.AdminTaskEmailTemplateId,
                                                                                                                                                    (int)CsmsEmailRecipientTypeList.Bcc))
                        {
                            int count = 1;
                            foreach (AdminTaskEmailTemplateRecipient atetr in atetrList)
                            {
                                string Recipient = GetEmail(at, c, u, atetr.AdminTaskEmailRecipientId);

                                if (!string.IsNullOrEmpty(Recipient))
                                {
                                    if (count == atetrList.Count)
                                    {
                                        tbBcc.Text += Recipient;
                                    }
                                    else
                                    {
                                        tbBcc.Text += Recipient + "; ";
                                    }
                                    mail.Bcc.Add(Recipient);
                                }
                                count++;
                            }
                        }

                        tbSubject.Text = atet.EmailSubject;

                        tbSubject.Text = tbSubject.Text.Replace("{CompanyName}", c.CompanyName);
                        if (c.CompanyId != 0)
                        {
                            switch (c.CompanyStatus2Id)
                            {
                                case (int)CompanyStatus2List.Contractor:
                                    tbSubject.Text = tbSubject.Text.Replace("{ContractorType}", "Contractor");
                                    break;
                                case (int)CompanyStatus2List.SubContractor:
                                    tbSubject.Text = tbSubject.Text.Replace("{ContractorType}", "Sub Contractor");
                                    break;
                                default:
                                    break;
                            }
                        }

                        if ((int)cbAccess.SelectedItem.Value == (int)CsmsAccessList.AlcoaLan)
                        {
                            if (atet.EmailBodyAlcoaLan != null)
                                heBody.Html = Encoding.ASCII.GetString(atet.EmailBodyAlcoaLan);
                        }
                        else
                        {
                            if (rblNewPassword.SelectedItem.Text == "Yes")
                            {
                                if (atet.EmailBodyAlcoaDirectNewUser != null)
                                    heBody.Html = Encoding.ASCII.GetString(atet.EmailBodyAlcoaDirectNewUser);
                            }
                            else
                            {
                                if (atet.EmailBodyAlcoaDirectExistingUser != null)
                                    heBody.Html = Encoding.ASCII.GetString(atet.EmailBodyAlcoaDirectExistingUser);
                            }
                        }

                        heBody.Html = heBody.Html.Replace("{FirstName}", u.FirstName);
                        heBody.Html = heBody.Html.Replace("{CompanyName}", c.CompanyName);
                        heBody.Html = heBody.Html.Replace("{UserLogin}", u.UserLogon);

                        mail.IsBodyHtml = true;
                        mail.Subject = tbSubject.Text;
                        mail.Body = heBody.Html;
                        mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        //mail.Headers.Add("Disposition-Notification-To", config.GetValue(ConfigList.ContactEmail));

                        AdminTaskEmailTemplateAttachmentService atetaService = new AdminTaskEmailTemplateAttachmentService();
                        TList<AdminTaskEmailTemplateAttachment> atetaTlist = atetaService.GetByAdminTaskEmailTemplateId(atet.AdminTaskEmailTemplateId);

                        CsmsFileService cfService = new CsmsFileService();

                        int hlcount = 1;
                        lblNoAttachments.Visible = true;
                        foreach (AdminTaskEmailTemplateAttachment ateta in atetaTlist)
                        {
                            lblNoAttachments.Visible = false;
                            if (ateta.CsmsAccessId == (int)cbAccess.SelectedItem.Value)
                            {
                                CsmsFile cf = cfService.GetByCsmsFileId(ateta.CsmsFileId);
                                MemoryStream ms = new MemoryStream(cf.Content);

                                string AttachText = String.Format("{0} ({1})", cf.FileName, KaiZen.Library.FileUtilities.Display.FileSize(cf.ContentLength));
                                string AttachNavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')",
                                                                    QueryStringModule.Encrypt("Type=CSMSFiles&File=" + cf.CsmsFileId));

                                switch (hlcount)
                                {
                                    case 1:
                                        hlAttachment1.Text = AttachText;
                                        hlAttachment1.NavigateUrl = AttachNavigateUrl;
                                        break;
                                    case 2:
                                        hlAttachment2.Text = AttachText;
                                        hlAttachment2.NavigateUrl = AttachNavigateUrl;
                                        break;
                                    case 3:
                                        hlAttachment3.Text = AttachText;
                                        hlAttachment3.NavigateUrl = AttachNavigateUrl;
                                        break;
                                    case 4:
                                        hlAttachment4.Text = AttachText;
                                        hlAttachment4.NavigateUrl = AttachNavigateUrl;
                                        break;
                                    case 5:
                                        hlAttachment5.Text = AttachText;
                                        hlAttachment5.NavigateUrl = AttachNavigateUrl;
                                        break;
                                    default:
                                        break;
                                }
                                hlcount++;
                                mail.Attachments.Add(new Attachment(ms, cf.FileName, null));

                                //ASPxHyperLink hl;
                                //Control hl_control = FindControl(String.Format("hlAttachment{0}", hlcount));
                                //if (hl_control != null)
                                //{
                                //    hl = (ASPxHyperLink)hl_control;
                                //    hl.Text = String.Format("{0} ({1})", cf.FileName, KaiZen.Library.FileUtilities.Display.FileSize(cf.ContentLength));
                                //    hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')",
                                //                                    QueryStringModule.Encrypt("Type=CSMSFiles&File=" + cf.CsmsFileId));

                                //    hlcount++;
                                //    mail.Attachments.Add(new Attachment(ms, cf.FileName, null));
                                //}
                            }
                        }
                    }

                    if (!Preview)
                    {
                        SmtpClient smtp = new SmtpClient();
                        smtp.Send(mail);
                       

                        //Log
                        CsmsEmailLogService celService = new CsmsEmailLogService();
                        CsmsEmailLog cel = new CsmsEmailLog();
                        cel.SentByUserId = auth.UserId;
                        cel.EmailSentDateTime = DateTime.Now;
                        cel.Subject = mail.Subject;
 
                        ASCIIEncoding encoding = new ASCIIEncoding();
                        cel.Body = (byte[])encoding.GetBytes(mail.Body);

                        celService.Save(cel);

                        CsmsEmailLogRecipientService celrService = new CsmsEmailLogRecipientService();
                        AdminTaskEmailLogService atelService = new AdminTaskEmailLogService();
                        foreach(MailAddress to in mail.To)
                        {
                            CsmsEmailLogRecipient celr = new CsmsEmailLogRecipient();
                            celr.CsmsEmailLogId = cel.CsmsEmailLogId;
                            celr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.To;
                            celr.RecipientAddress = to.Address;
                            celrService.Save(celr);

                            AdminTaskEmailLog atel = new AdminTaskEmailLog();
                            atel.AdminTaskId = at.AdminTaskId;
                            atel.CsmsEmailLogId = celr.CsmsEmailLogId;
                            atelService.Save(atel);
                        }
                        foreach (MailAddress cc in mail.CC)
                        {
                            CsmsEmailLogRecipient celr = new CsmsEmailLogRecipient();
                            celr.CsmsEmailLogId = cel.CsmsEmailLogId;
                            celr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.Cc;
                            celr.RecipientAddress = cc.Address;
                            celrService.Save(celr);

                            //AdminTaskEmailLog atel = new AdminTaskEmailLog();
                            //atel.AdminTaskId = at.AdminTaskId;
                            //atel.CsmsEmailLogId = celr.CsmsEmailLogId;
                            //atelService.Save(atel);
                        }
                        foreach (MailAddress Bcc in mail.Bcc)
                        {
                            CsmsEmailLogRecipient celr = new CsmsEmailLogRecipient();
                            celr.CsmsEmailLogId = cel.CsmsEmailLogId;
                            celr.CsmsEmailRecipientTypeId = (int)CsmsEmailRecipientTypeList.Bcc;
                            celr.RecipientAddress = Bcc.Address;
                            celrService.Save(celr);

                            //AdminTaskEmailLog atel = new AdminTaskEmailLog();
                            //atel.AdminTaskId = at.AdminTaskId;
                            //atel.CsmsEmailLogId = celr.CsmsEmailLogId;
                            //atelService.Save(atel);
                        }
                    }
                }

                #endregion

                if (!Preview)
                {
                    if (!ResendEmail)
                    {
                        at.AdminTaskStatusId = (int)AdminTaskStatusList.Closed;
                        at.ClosedByUserId = auth.UserId;
                        at.DateClosed = DateTime.Now;
                        atService.Save(at);
                        Redirect("Task Completed Successfully.", "AdminTasks.aspx");
                    }
                    else
                    {
                        at.AdminTaskComments = mTaskComments.Text;
                        atService.Save(at);
                        Redirect("E-Mail Resent!", "AdminTasks.aspx");
                    }
                }
                else
                {
                    //ASPxRoundPanel1.Visible = true;
                    //ASPxRoundPanel2.Visible = true;
                    //ASPxPanel1.Visible = true;
                    //btnAcceptChanges.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured: " + ex.Message);
            }
        }

        protected string GetEmail(KaiZen.CSMS.Entities.AdminTask adminTask, Companies company, Users user, int AdminTaskEmailRecipientId)
        {
            string email = "";
            int count = 0;

            Configuration config = new Configuration();
            UsersService uService = new UsersService();
            QuestionnaireWithLocationApprovalViewLatestService qService = new QuestionnaireWithLocationApprovalViewLatestService();

            switch (AdminTaskEmailRecipientId)
            {
                case (int)AdminTaskEmailRecipientList.Admin:
                    email = config.GetValue(ConfigList.ContactEmail);
                    break;
                case (int)AdminTaskEmailRecipientList.H_S_Assessor:

                    if (company.EhsConsultantId != null)
                    {
                        EhsConsultantService eService = new EhsConsultantService();
                        using (EhsConsultant e = eService.GetByEhsConsultantId((int)company.EhsConsultantId))
                        {
                            if (e != null)
                            {
                                Users u = uService.GetByUserId(e.UserId);
                                if (u != null)
                                {
                                    email = u.Email;
                                }
                            }
                        }
                    }

                    break;
                case (int)AdminTaskEmailRecipientList.Lead_H_S_Assessor:

                    //email = config.GetValue(ConfigList.DefaultEhsConsultant_WAO);

                    break;
                case (int)AdminTaskEmailRecipientList.Procurement_Contact:

                    QuestionnaireWithLocationApprovalViewLatestFilters qFiltersPc = new QuestionnaireWithLocationApprovalViewLatestFilters();
                    qFiltersPc.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, company.CompanyId.ToString());

                    using (VList<QuestionnaireWithLocationApprovalViewLatest> qVlist = qService.GetPaged(qFiltersPc.ToString(), "QuestionnaireId DESC", 0, 100, out count))
                    {
                        if (count > 0)
                        {
                            int UserId = 0;
                            Int32.TryParse(qVlist[0].ProcurementContactUserId.ToString(), out UserId);
                            if (UserId > 0)
                            {
                                Users u = uService.GetByUserId(UserId);
                                if (u != null)
                                {
                                    email = u.Email;
                                }
                            }
                        }
                    }

                    break;
                case (int)AdminTaskEmailRecipientList.Procurement_Functional_Supervisor:

                    QuestionnaireWithLocationApprovalViewLatestFilters qFiltersPfs = new QuestionnaireWithLocationApprovalViewLatestFilters();
                    qFiltersPfs.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, company.CompanyId.ToString());

                    using (VList<QuestionnaireWithLocationApprovalViewLatest> qVlist = qService.GetPaged(qFiltersPfs.ToString(), "QuestionnaireId DESC", 0, 100, out count))
                    {
                        if (count > 0)
                        {
                            int UserId = 0;
                            Int32.TryParse(qVlist[0].ContractManagerUserId.ToString(), out UserId);
                            if (UserId > 0)
                            {
                                Users u = uService.GetByUserId(UserId);
                                if (u != null)
                                {
                                    email = u.Email;
                                }
                            }
                        }
                    }

                    break;
                case (int)AdminTaskEmailRecipientList.Requester:
                    email = user.Email;
                    break;
            }


            return email;
        }

        protected void Validate()
        {
            try
            {
                
                bool AlcoaDirect = false;
                if ((int)cbAccess.SelectedItem.Value == (int)CsmsAccessList.AlcoaDirect) AlcoaDirect = true;

                if (AlcoaDirect)
                {
                    if (tbLogin.Text != tbEmail.Text)
                    {
                        throw new Exception("Login and E-Mail Address must match if access is via AlcoaDirect");
                    }
                    if (!StringUtilities.RegEx.IsEmail(tbEmail.Text))
                    {
                        throw new Exception("E-mail Address is not valid.");
                    }
                }

                if ((int)cbType.SelectedItem.Value == (int)AdminTaskTypeList.Contractor_Request_for_Access ||
                    (int)cbType.SelectedItem.Value == (int)AdminTaskTypeList.Contractor_Safety_Qualification_Pre_Qualification ||
                    (int)cbType.SelectedItem.Value == (int)AdminTaskTypeList.Contractor_Safety_Qualification_Re_Qualification)
                {
                    if (cbCompany.SelectedItem.Value.ToString() == "0")
                    {
                        throw new Exception("Based on your 'type' selection, Alcoa can not be selected as the company.'");
                    }
                }
                else
                {
                    if (cbCompany.SelectedItem.Value.ToString() != "0")
                    {
                        throw new Exception("Based on your 'type' selection, Alcoa must be selected as the company.'");
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void btnAcceptChanges_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsPostBack)
                {
                    Validate();
                    PerformTask(false); //do it
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Redirect(string l_msg, string l_sURL)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(Page);
            if (l_oSM != null)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "wsReturnToPreviousPage", String.Format("alert('{0}');window.location.href='{1}';", l_msg, l_sURL), true);
            }
        }
        public void Redirect(string l_sURL)
        {
            ScriptManager l_oSM = ScriptManager.GetCurrent(Page);
            if (l_oSM != null)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "wsReturnToPreviousPage", String.Format("window.location.href='{0}';", l_sURL), true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "wsReturnToPreviousPage", String.Format("window.location.href='{0}';", l_sURL), true);
            }
        }

        protected void UpdatePresentlyWith(int CompanyId)
        {
            try
            {
                QuestionnaireService qService0 = new QuestionnaireService();
                DataSet qDataSet = qService0.GetLastModifiedQuestionnaireId(CompanyId);
                if (qDataSet.Tables[0] != null)
                {
                    if (qDataSet.Tables[0].Rows.Count == 1)
                    {
                        string LatestQuestionnaireId = qDataSet.Tables[0].Rows[0].ItemArray[0].ToString();

                        if (!String.IsNullOrEmpty(LatestQuestionnaireId))
                        {
                            QuestionnaireService qService = new QuestionnaireService();
                            Questionnaire q = qService.GetByQuestionnaireId(Convert.ToInt32(LatestQuestionnaireId));
                            if (q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.CsmsCreateCompanyLogins)
                            {
                                q.QuestionnairePresentlyWithActionId = (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireSupplier;
                                q.QuestionnairePresentlyWithSince = DateTime.Now;
                                qService.Save(q);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
            }
        }

        protected bool IsCompanySqExpired(int companyId)
        {
            bool expired = false;

            try
            {
                CompaniesService cService = new CompaniesService();
                Companies c = cService.GetByCompanyId(companyId);
                if (c.CompanyId != 0) //!= Alcoa
                {
                    QuestionnaireFilters qFilters = new QuestionnaireFilters();
                    qFilters.Append(QuestionnaireColumn.CompanyId, c.CompanyId.ToString());
                    int count = 0;
                    TList<Questionnaire> qList =
                        DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);

                    if (qList != null && count > 0)
                    {
                        if (c.Deactivated != false)
                        {
                            if (qList[0].MainAssessmentValidTo != null && qList[0].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                            {
                                DateTime dtExpiry = Convert.ToDateTime(qList[0].MainAssessmentValidTo);
                                TimeSpan ts = Convert.ToDateTime(dtExpiry) - DateTime.Now;
                                QuestionnaireService qService = new QuestionnaireService();
                                if (ts.Days <= 0)
                                {
                                    expired = true;
                                }
                            }
                            else
                            {
                                if (count > 1)
                                {
                                    if (qList[1] != null)
                                    {
                                        if (qList[1].MainAssessmentValidTo != null && qList[1].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                        {
                                            if (qList[1].MainAssessmentValidTo <= DateTime.Now)
                                                expired = true;
                                        }
                                        else
                                        {
                                            if (count > 2)
                                            {
                                                if (qList[2].MainAssessmentValidTo != null && qList[2].Status == (int)QuestionnaireStatusList.AssessmentComplete)
                                                {
                                                    if (qList[2].MainAssessmentValidTo <= DateTime.Now)
                                                        expired = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //UpdateHealth(false, "UpdateCompanyStatusIfQuestionnaireExpired", "Exception: " + ex.Message);
            }

            return expired;
        }
    }
}