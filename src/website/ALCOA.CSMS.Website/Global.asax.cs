﻿using ALCOA.CSMS.Website.App_Start;
using Autofac;
using Autofac.Integration.Web;
using Repo.CSMS.Common.Log;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Service.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Optimization;
using System.Web.Security;
using System.Web.SessionState;

namespace ALCOA.CSMS.Website
{
    public class Global : System.Web.HttpApplication, IContainerProviderAccessor
    {
        // Provider that holds the application container.
        static IContainerProvider _containerProvider;

        // Instance property that will be used by Autofac HttpModules
        // to resolve and inject dependencies.
        public IContainerProvider ContainerProvider
        {
            get { return _containerProvider; }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterDependencies(new ContainerBuilder());

            App_Start.BundlingConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        public void RegisterDependencies(ContainerBuilder builder)
        {
            //Register services
            builder.RegisterAssemblyTypes(typeof(UserService).Assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            //// Register controllers using assembly scanning.
            //builder.RegisterControllers(Assembly.GetExecutingAssembly())
            //    .PropertiesAutowired();

            builder.RegisterAssemblyTypes(typeof(ContextAdaptor).Assembly)
                .AsImplementedInterfaces()
                .InstancePerRequest();

            //Register entities
            builder.RegisterType<Entities>()
                .As<IDbContext>()
                .InstancePerRequest();

            //Register context
            builder.RegisterType<ContextAdaptor>()
                .As<IObjectSetFactory, IObjectContext>()
                .InstancePerRequest();

            //Register unit of work
            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerRequest();

            //Register repository
            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerRequest();

            //Register logging
            builder.RegisterType<LogService>()
                .As<ILogService>()
                .InstancePerRequest();

            ////Register emailing
            //builder.RegisterType<EmailService>()
            //    .As<IEmailService>()
            //    .InstancePerLifetimeScope();

            ////Register scheduled tasks
            //builder.RegisterAssemblyTypes(typeof(CSMS.Service.ScheduledTasks.EmptyTask).Assembly)
            //    .Where(t => t.Name.EndsWith("Task"))
            //    .As<IScheduledTask>()
            //    .InstancePerRequest();

            try
            {
                //Build autofac container
                var container = builder.Build();

                //Set the dependency resolver implementation.

                _containerProvider = new ContainerProvider(container);
            }
            catch (Exception ex)
            {
                if (ex is ReflectionTypeLoadException)
                {
                    var typeLoadException = ex as ReflectionTypeLoadException;
                    var loaderExceptions = typeLoadException.LoaderExceptions;
                    throw new AggregateException(typeLoadException.Message, loaderExceptions);
                }
                throw;
            }
        }

        private static ILifetimeScope GetContainer()
        {
            var cpa = (IContainerProviderAccessor)HttpContext.Current.ApplicationInstance;
            return cpa.ContainerProvider.RequestLifetime;
        }

        public static T GetInstance<T>()
        {
            return GetContainer().Resolve<T>();  //Example:: var kService = ALCOA.CSMS.Website.Global.GetInstance<CSMS.Service.Database.IKpiService>();
        }
    }
}