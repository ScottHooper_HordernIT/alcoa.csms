﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationAttachmentProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationAttachmentDataSourceDesigner))]
	public class QuestionnaireVerificationAttachmentDataSource : ProviderDataSource<QuestionnaireVerificationAttachment, QuestionnaireVerificationAttachmentKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentDataSource class.
		/// </summary>
		public QuestionnaireVerificationAttachmentDataSource() : base(new QuestionnaireVerificationAttachmentService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationAttachmentDataSourceView used by the QuestionnaireVerificationAttachmentDataSource.
		/// </summary>
		protected QuestionnaireVerificationAttachmentDataSourceView QuestionnaireVerificationAttachmentView
		{
			get { return ( View as QuestionnaireVerificationAttachmentDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationAttachmentDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationAttachmentSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationAttachmentSelectMethod selectMethod = QuestionnaireVerificationAttachmentSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationAttachmentSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationAttachmentDataSourceView class that is to be
		/// used by the QuestionnaireVerificationAttachmentDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationAttachmentDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationAttachment, QuestionnaireVerificationAttachmentKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationAttachmentDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationAttachmentDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationAttachmentDataSourceView : ProviderDataSourceView<QuestionnaireVerificationAttachment, QuestionnaireVerificationAttachmentKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationAttachmentDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationAttachmentDataSourceView(QuestionnaireVerificationAttachmentDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationAttachmentDataSource QuestionnaireVerificationAttachmentOwner
		{
			get { return Owner as QuestionnaireVerificationAttachmentDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationAttachmentSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationAttachmentOwner.SelectMethod; }
			set { QuestionnaireVerificationAttachmentOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationAttachmentService QuestionnaireVerificationAttachmentProvider
		{
			get { return Provider as QuestionnaireVerificationAttachmentService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationAttachment> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationAttachment> results = null;
			QuestionnaireVerificationAttachment item;
			count = 0;
			
			System.Int32 _attachmentId;
			System.Int32 _questionnaireId;
			System.Int32 _sectionId;
			System.Int32 _questionId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationAttachmentSelectMethod.Get:
					QuestionnaireVerificationAttachmentKey entityKey  = new QuestionnaireVerificationAttachmentKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationAttachmentProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationAttachment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationAttachmentSelectMethod.GetAll:
                    results = QuestionnaireVerificationAttachmentProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationAttachmentSelectMethod.GetPaged:
					results = QuestionnaireVerificationAttachmentProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationAttachmentSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationAttachmentProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationAttachmentProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationAttachmentSelectMethod.GetByAttachmentId:
					_attachmentId = ( values["AttachmentId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AttachmentId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationAttachmentProvider.GetByAttachmentId(_attachmentId);
					results = new TList<QuestionnaireVerificationAttachment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireVerificationAttachmentSelectMethod.GetByQuestionnaireIdSectionIdQuestionId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_sectionId = ( values["SectionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32)) : (int)0;
					_questionId = ( values["QuestionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationAttachmentProvider.GetByQuestionnaireIdSectionIdQuestionId(_questionnaireId, _sectionId, _questionId);
					results = new TList<QuestionnaireVerificationAttachment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case QuestionnaireVerificationAttachmentSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationAttachmentProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationAttachmentSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationAttachmentProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationAttachmentSelectMethod.Get || SelectMethod == QuestionnaireVerificationAttachmentSelectMethod.GetByAttachmentId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationAttachment entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationAttachmentProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationAttachment> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationAttachmentProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationAttachmentDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationAttachmentDataSource class.
	/// </summary>
	public class QuestionnaireVerificationAttachmentDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationAttachment, QuestionnaireVerificationAttachmentKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationAttachmentDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationAttachmentSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationAttachmentDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationAttachmentDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationAttachmentDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationAttachmentDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationAttachmentDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationAttachmentDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAttachmentDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationAttachmentDataSourceActionList(QuestionnaireVerificationAttachmentDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationAttachmentSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationAttachmentDataSourceActionList
	
	#endregion QuestionnaireVerificationAttachmentDataSourceDesigner
	
	#region QuestionnaireVerificationAttachmentSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationAttachmentDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationAttachmentSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAttachmentId method.
		/// </summary>
		GetByAttachmentId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSectionIdQuestionId method.
		/// </summary>
		GetByQuestionnaireIdSectionIdQuestionId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireVerificationAttachmentSelectMethod

	#region QuestionnaireVerificationAttachmentFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentFilter : SqlFilter<QuestionnaireVerificationAttachmentColumn>
	{
	}
	
	#endregion QuestionnaireVerificationAttachmentFilter

	#region QuestionnaireVerificationAttachmentExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationAttachmentColumn>
	{
	}
	
	#endregion QuestionnaireVerificationAttachmentExpressionBuilder	

	#region QuestionnaireVerificationAttachmentProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationAttachmentChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAttachmentProperty : ChildEntityProperty<QuestionnaireVerificationAttachmentChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationAttachmentProperty
}

