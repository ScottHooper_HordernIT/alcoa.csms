﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'CompanyStatus2' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table CompanyStatus2</remark>
	[Serializable, Flags]
	public enum CompanyStatus2List
	{
		/// <summary> 
		/// Contractor
		/// </summary>
		[EnumTextValue(@"Contractor")]
		Contractor = 1, 

		/// <summary> 
		/// Sub Contractor
		/// </summary>
		[EnumTextValue(@"Sub Contractor")]
		SubContractor = 2

	}
}
