﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{
    ///<summary>
    /// This class is the base class for any <see cref="QuestionnaireMainAttachmentProviderBase"/> implementation.
    /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
    ///</summary>
    public abstract partial class UserCompanyProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.UserCompany, KaiZen.CSMS.Entities.UserCompanyKey>
    {
        #region Get from Many To Many Relationship Functions
        #endregion

        #region Delete Methods

        /// <summary>
        /// 	Deletes a row from the DataSource.
        /// </summary>
        /// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
        /// <param name="key">The unique identifier of the row to delete.</param>
        /// <returns>Returns true if operation suceeded.</returns>
        public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserCompanyKey key)
        {
            return Delete(transactionManager, key.UserCompanyId);
        }

        /// <summary>
        /// 	Deletes a row from the DataSource.
        /// </summary>
        /// <param name="_attachmentId">. Primary Key.</param>
        /// <remarks>Deletes based on primary key(s).</remarks>
        /// <returns>Returns true if operation suceeded.</returns>
        public bool Delete(System.Int32 _UserCompanyId)
        {
            return Delete(null, _UserCompanyId);
        }

        /// <summary>
        /// 	Deletes a row from the DataSource.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId">. Primary Key.</param>
        /// <remarks>Deletes based on primary key(s).</remarks>
        /// <returns>Returns true if operation suceeded.</returns>
        public abstract bool Delete(TransactionManager transactionManager, System.Int32 _UserCompanyId);

        #endregion Delete Methods

        #region Get By Foreign Key Functions

        #region GetByUserId
        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="_userId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserId(System.Int32 _userId)
        {
            int count = -1;
            return GetByUserId(_userId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_userId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        /// <remarks></remarks>
        public TList<UserCompany> GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
        {
            int count = -1;
            return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_userId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
        {
            int count = -1;
            return GetByUserId(transactionManager, _userId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_userId"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserId(System.Int32 _userId, int start, int pageLength)
        {
            int count = -1;
            return GetByUserId(null, _userId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_userId"></param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserId(System.Int32 _userId, int start, int pageLength, out int count)
        {
            return GetByUserId(null, _userId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public abstract TList<UserCompany> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
        
        #endregion

        #region GetAllCustom
        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="_userId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetAllCustom()
        {
            int count = -1;
            return GetAllCustom(0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_userId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        /// <remarks></remarks>
        public TList<UserCompany> GetAllCustom(TransactionManager transactionManager)
        {
            int count = -1;
            return GetAllCustom(transactionManager, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_userId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetAllCustom(TransactionManager transactionManager, int start, int pageLength)
        {
            int count = -1;
            return GetAllCustom(transactionManager,start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_userId"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetAllCustom(int start, int pageLength)
        {
            int count = -1;
            return GetAllCustom(null, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_userId"></param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetAllCustom(int start, int pageLength, out int count)
        {
            return GetAllCustom(null, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public abstract TList<UserCompany> GetAllCustom(TransactionManager transactionManager, int start, int pageLength, out int count);

        #endregion

        #region GetByCompanyId
        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="_userId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByCompanyId(System.Int32 _companyId)
        {
            int count = -1;
            return GetByCompanyId(_companyId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_companyId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        /// <remarks></remarks>
        public TList<UserCompany> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
        {
            int count = -1;
            return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_companyId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
        {
            int count = -1;
            return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_companyId"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
        {
            int count = -1;
            return GetByCompanyId(null, _companyId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_companyId"></param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByCompanyId(System.Int32 _companyId, int start, int pageLength, out int count)
        {
            return GetByCompanyId(null, _companyId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public abstract TList<UserCompany> GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);

        #endregion

        #region GetByUserIdCompanyId
        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="_userId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserIdCompanyId(System.Int32 _userId,System.Int32 _companyId,System.Boolean _active)
        {
            int count = -1;
            return GetByUserIdCompanyId(_userId,_companyId,_active, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_companyId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        /// <remarks></remarks>
        public TList<UserCompany> GetByUserIdCompanyId(TransactionManager transactionManager,System.Int32 _userId, System.Int32 _companyId,System.Boolean _active)
        {
            int count = -1;
            return GetByUserIdCompanyId(transactionManager, _userId, _companyId, _active, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_companyId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserIdCompanyId(TransactionManager transactionManager, System.Int32 _userId, System.Int32 _companyId, System.Boolean _active, int start, int pageLength)
        {
            int count = -1;
            return GetByUserIdCompanyId(transactionManager, _userId, _companyId, _active, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_companyId"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserIdCompanyId(System.Int32 _userId, System.Int32 _companyId, System.Boolean _active, int start, int pageLength)
        {
            int count = -1;
            return GetByUserIdCompanyId(null, _userId, _companyId, _active, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_companyId"></param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<UserCompany> GetByUserIdCompanyId(System.Int32 _userId, System.Int32 _companyId, System.Boolean _active, int start, int pageLength, out int count)
        {
            return GetByUserIdCompanyId(null, _userId, _companyId, _active, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public abstract TList<UserCompany> GetByUserIdCompanyId(TransactionManager transactionManager, System.Int32 _userId, System.Int32 _companyId, System.Boolean _active, int start, int pageLength, out int count);

        

        #endregion



        #region Get By Index Functions

        /// <summary>
        /// 	Gets a row from the DataSource based on its primary key.
        /// </summary>
        /// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
        /// <param name="key">The unique identifier of the row to retrieve.</param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <returns>Returns an instance of the Entity class.</returns>
        public override KaiZen.CSMS.Entities.UserCompany Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserCompanyKey key, int start, int pageLength)
        {
            return GetByUserCompanyId(transactionManager, key.UserCompanyId, start, pageLength);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the primary key PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_attachmentId"></param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.UserCompany GetByUserCompanyId(System.Int32 _UserCompanyId)
        {
            int count = -1;
            return GetByUserCompanyId(null, _UserCompanyId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.UserCompany GetByUserCompanyId(System.Int32 _UserCompanyId, int start, int pageLength)
        {
            int count = -1;
            return GetByUserCompanyId(null, _UserCompanyId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId"></param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.UserCompany GetByUserCompanyId(TransactionManager transactionManager, System.Int32 _UserCompanyId)
        {
            int count = -1;
            return GetByUserCompanyId(transactionManager, _UserCompanyId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.UserCompany GetByUserCompanyId(TransactionManager transactionManager, System.Int32 _UserCompanyId, int start, int pageLength)
        {
            int count = -1;
            return GetByUserCompanyId(transactionManager, _UserCompanyId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.UserCompany GetByUserCompanyId(System.Int32 _UserCompanyId, int start, int pageLength, out int count)
        {
            return GetByUserCompanyId(null, _UserCompanyId, start, pageLength, out count);
        }


        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public abstract KaiZen.CSMS.Entities.UserCompany GetByUserCompanyId(TransactionManager transactionManager, System.Int32 _UserCompanyId, int start, int pageLength, out int count);

    
       


        public TList<UserCompany> GetByResponeIdQuestionId(TransactionManager transactionManager, System.Int32 _responseId, System.Int32? _questionId, int start, int pageLength)
        {
            int count = -1;
            return GetByResponeIdQuestionId(transactionManager, _responseId, _questionId, start, pageLength, out count);
        }


        public TList<UserCompany> GetByResponeIdQuestionId(System.Int32 _responseId, System.Int32? _questionId, int start, int pageLength, out int count)
        {
            return GetByResponeIdQuestionId(null, _responseId, _questionId, start, pageLength, out count);
        }

        public abstract TList<UserCompany> GetByResponeIdQuestionId(TransactionManager transactionManager, System.Int32 _responseId, System.Int32? _questionId, int start, int pageLength, out int count);
        #endregion "Get By Index Functions"

        #region Custom Methods


        #endregion

        #region Helper Functions

        /// <summary>
        /// Fill a TList&lt;SafetyPlansSEResponsesAttachment&gt; From a DataReader.
        /// </summary>
        /// <param name="reader">Datareader</param>
        /// <param name="rows">The collection to fill</param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">number of rows.</param>
        /// <returns>a <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/></returns>
        public static TList<UserCompany> Fill(IDataReader reader, TList<UserCompany> rows, int start, int pageLength)
        {
            NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
            Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;

            // advance to the starting row
            for (int i = 0; i < start; i++)
            {
                if (!reader.Read())
                    return rows; // not enough rows, just return
            }
            for (int i = 0; i < pageLength; i++)
            {
                if (!reader.Read())
                    break; // we are done

                string key = null;

                KaiZen.CSMS.Entities.UserCompany c = null;
                if (useEntityFactory)
                {
                    key = new System.Text.StringBuilder("UserCompany")
                    .Append("|").Append((System.Int32)reader[((int)UserCompanyColumn.UserCompanyId - 1)]).ToString();
                    c = EntityManager.LocateOrCreate<UserCompany>(
                    key.ToString(), // EntityTrackingKey
                    "UserCompany",  //Creational Type
                    entityCreationFactoryType,  //Factory used to create entity
                    enableEntityTracking); // Track this entity?
                }
                else
                {
                    c = new KaiZen.CSMS.Entities.UserCompany();
                }

                if (!enableEntityTracking ||
                    c.EntityState == EntityState.Added ||
                    (enableEntityTracking &&

                        (
                            (currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
                            (currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
                        )
                    ))
                {
                    c.SuppressEntityEvents = true;
                    c.UserCompanyId = (System.Int32)reader[((int)UserCompanyColumn.UserCompanyId - 1)];
                    c.UserId = (System.Int32)reader[((int)UserCompanyColumn.UserId - 1)];
                    c.CompanyId = (System.Int32)reader[((int)UserCompanyColumn.CompanyId - 1)];
                    c.Active = (System.Boolean)reader[((int)UserCompanyColumn.Active - 1)];
                    
                    
                    c.EntityTrackingKey = key;
                    c.AcceptChanges();
                    c.SuppressEntityEvents = false;
                }
                rows.Add(c);
            }
            return rows;
        }
        /// <summary>
        /// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object from the <see cref="IDataReader"/>.
        /// </summary>
        /// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
        /// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object to refresh.</param>
        public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.UserCompany entity)
        {
            if (!reader.Read()) return;

           // entity.AttachmentId = (System.Int32)reader[((int)SafetyPlansSEResponsesAttachmentColumn.AttachmentId - 1)];
            entity.UserCompanyId = (System.Int32)reader[((int)UserCompanyColumn.UserCompanyId - 1)];
            entity.UserId = (System.Int32)reader[((int)UserCompanyColumn.UserId - 1)];
            entity.CompanyId = (System.Int32)reader[((int)UserCompanyColumn.CompanyId - 1)];
            entity.Active = (System.Boolean)reader[((int)UserCompanyColumn.Active - 1)];
          
            entity.AcceptChanges();
        }

        /// <summary>
        /// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object from the <see cref="DataSet"/>.
        /// </summary>
        /// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
        /// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object.</param>
        public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.UserCompany entity)
        {
            DataRow dataRow = dataSet.Tables[0].Rows[0];

            entity.UserCompanyId = (System.Int32)dataRow["UserCompanyId"];
            entity.UserId = (System.Int32)dataRow["UserId"]; 
            entity.CompanyId = (System.Int32)dataRow["CompanyId"];
            entity.Active = (System.Boolean)dataRow["Active"];         
            
           entity.AcceptChanges();
        }
        #endregion

        #region DeepLoad Methods
        /// <summary>
        /// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
        /// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
        /// </summary>
        /// <remarks>
        /// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
        /// </remarks>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object to load.</param>
        /// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
        /// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
        /// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment Property Collection Type Array To Include or Exclude from Load</param>
        /// <param name="innerList">A collection of child types for easy access.</param>
        /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
        /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
        public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserCompany entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
        {
            if (entity == null)
                return;

            #region ModifiedByUserIdSource
            if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList)
                && entity.UserIdSource == null)
            {
                object[] pkItems = new object[1];
                pkItems[0] = entity.UserId;
                Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
                if (tmpEntity != null)
                    entity.UserIdSource = tmpEntity;
                else
                    entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);

#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
#endif

                if (deep && entity.UserIdSource != null)
                {
                    innerList.SkipChildren = true;
                    DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
                    innerList.SkipChildren = false;
                }

            }
            #endregion ModifiedByUserIdSource

            #region CompanyIdSource
            if (CanDeepLoad(entity, "Companies|CompanyIdSource", deepLoadType, innerList)
                && entity.CompanyIdSource == null)
            {
                object[] pkItems = new object[1];
                pkItems[0] = entity.CompanyId;
                Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
                if (tmpEntity != null)
                    entity.CompanyIdSource = tmpEntity;
                else
                    entity.CompanyIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.CompanyId);

#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AnswerIdSource' loaded. key " + entity.EntityTrackingKey);
#endif

                if (deep && entity.CompanyIdSource != null)
                {
                    innerList.SkipChildren = true;
                    DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.CompanyIdSource, deep, deepLoadType, childTypes, innerList);
                    innerList.SkipChildren = false;
                }

            }
            #endregion AnswerIdSource

            //used to hold DeepLoad method delegates and fire after all the local children have been loaded.
            Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

            //Fire all DeepLoad Items
            foreach (KeyValuePair<Delegate, object> pair in deepHandles.Values)
            {
                pair.Key.DynamicInvoke((object[])pair.Value);
            }
            deepHandles = null;
        }

        #endregion

        #region DeepSave Methods

        /// <summary>
        /// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment object with criteria based of the child 
        /// Type property array and DeepSaveType.
        /// </summary>
        /// <param name="transactionManager">The transaction manager.</param>
        /// <param name="entity">KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment instance</param>
        /// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
        /// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment Property Collection Type Array To Include or Exclude from Save</param>
        /// <param name="innerList">A Hashtable of child types for easy access.</param>
        public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserCompany entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
        {
            if (entity == null)
                return false;

            #region Composite Parent Properties
            //Save Source Composite Properties, however, don't call deep save on them.  
            //So they only get saved a single level deep.

            #region UserIdSource
            if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList)
                && entity.UserIdSource != null)
            {
                DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
                entity.UserId = entity.UserIdSource.UserId;
            }
            #endregion

            #region AnswerIdSource
            if (CanDeepSave(entity, "Companies|CompanyIdSource", deepSaveType, innerList)
                && entity.CompanyIdSource != null)
            {
                DataRepository.CompaniesProvider.Save(transactionManager, entity.CompanyIdSource);
                entity.UserCompanyId = entity.CompanyIdSource.CompanyId;
            }
            #endregion
            #endregion Composite Parent Properties

            // Save Root Entity through Provider
            if (!entity.IsDeleted)
                this.Save(transactionManager, entity);

            //used to hold DeepSave method delegates and fire after all the local children have been saved.
            Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
            //Fire all DeepSave Items
            foreach (KeyValuePair<Delegate, object> pair in deepHandles.Values)
            {
                pair.Key.DynamicInvoke((object[])pair.Value);
            }

            // Save Root Entity through Provider, if not already saved in delete mode
            if (entity.IsDeleted)
                this.Save(transactionManager, entity);


            deepHandles = null;

            return true;
        }
        #endregion
    } // end class

    #region UserCompanyChildEntityTypes

    ///<summary>
    /// Enumeration used to expose the different child entity types 
    /// for child properties in <c>KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment</c>
    ///</summary>
    public enum UserCompanyChildEntityTypes
    {

        ///<summary>
        /// Composite Property for <c>Users</c> at ModifiedByUserIdSource
        ///</summary>
        [ChildEntityType(typeof(Users))]
        Users,

        ///<summary>
        /// Composite Property for <c>QuestionnaireMainAnswer</c> at AnswerIdSource
        ///</summary>
        [ChildEntityType(typeof(Companies))]
        Companies
    }

    #endregion UserCompanyChildEntityTypes

    #region UserCompanyFilterBuilder

    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
    /// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UserCompanyFilterBuilder : SqlFilterBuilder<UserCompanyColumn>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
        /// </summary>
        public UserCompanyFilterBuilder() : base() { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        public UserCompanyFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        /// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public UserCompanyFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

        #endregion Constructors
    }

    #endregion QuestionnaireMainAttachmentFilterBuilder

    #region UserCompanyParameterBuilder

    /// <summary>
    /// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
    /// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UserCompanyParameterBuilder : ParameterizedSqlFilterBuilder<UserCompanyColumn>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
        /// </summary>
        public UserCompanyParameterBuilder() : base() { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        public UserCompanyParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        /// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public UserCompanyParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

        #endregion Constructors
    }

    #endregion UserCompanyParameterBuilder

    #region UserCompanySortBuilder

    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
    /// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UserCompanySortBuilder : SqlSortBuilder<UserCompanyColumn>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentSqlSortBuilder class.
        /// </summary>
        public UserCompanySortBuilder() : base() { }

        #endregion Constructors

    }
    #endregion UserCompanySortBuilder

} // end namespace
#endregion