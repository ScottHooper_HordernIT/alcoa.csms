﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'Role' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table Role</remark>
	[Serializable]
	public enum RoleList
	{
		/// <summary> 
		/// Alcoan User
		/// </summary>
		[EnumTextValue(@"Alcoan User")]
		Reader = 1, 

		/// <summary> 
		/// Contractor User
		/// </summary>
		[EnumTextValue(@"Contractor User")]
		Contractor = 2, 

		/// <summary> 
		/// Full Control of Site
		/// </summary>
		[EnumTextValue(@"Full Control of Site")]
		Administrator = 3, 

		/// <summary> 
		/// Can not login
		/// </summary>
		[EnumTextValue(@"Can not login")]
		Disabled = 4, 

		/// <summary> 
		/// PreQual Contractors
		/// </summary>
		[EnumTextValue(@"PreQual Contractors")]
		PreQual = 5,

        /// <summary> 
        /// Contractor Data Maintainer
		/// </summary>
        [EnumTextValue(@"CWK Maintainer")]
        CWKMaintainer = 7,

        /// <summary> 
        /// Contractor Data Enquiry
        /// </summary>
        [EnumTextValue(@"CWK Enquiry")]
        CWKEnquiry = 8
	}
}
