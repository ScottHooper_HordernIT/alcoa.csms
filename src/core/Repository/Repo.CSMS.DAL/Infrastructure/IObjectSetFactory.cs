﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Repo.CSMS.DAL.EntityModels;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.DAL.Infrastructure
{
    public interface IObjectSetFactory : IDisposable
    {
        IObjectSet<T> CreateObjectSet<T>() where T : class;
        void ChangeObjectState(object entity, EntityState state);
        bool IsObjectAttached(System.Data.Entity.Core.EntityKey key);
        ObjectContext Context { get; }
    }
}
