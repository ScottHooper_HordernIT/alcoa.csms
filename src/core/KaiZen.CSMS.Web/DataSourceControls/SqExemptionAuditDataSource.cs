﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SqExemptionAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(SqExemptionAuditDataSourceDesigner))]
	public class SqExemptionAuditDataSource : ProviderDataSource<SqExemptionAudit, SqExemptionAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditDataSource class.
		/// </summary>
		public SqExemptionAuditDataSource() : base(new SqExemptionAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SqExemptionAuditDataSourceView used by the SqExemptionAuditDataSource.
		/// </summary>
		protected SqExemptionAuditDataSourceView SqExemptionAuditView
		{
			get { return ( View as SqExemptionAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the SqExemptionAuditDataSource control invokes to retrieve data.
		/// </summary>
		public SqExemptionAuditSelectMethod SelectMethod
		{
			get
			{
				SqExemptionAuditSelectMethod selectMethod = SqExemptionAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (SqExemptionAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SqExemptionAuditDataSourceView class that is to be
		/// used by the SqExemptionAuditDataSource.
		/// </summary>
		/// <returns>An instance of the SqExemptionAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<SqExemptionAudit, SqExemptionAuditKey> GetNewDataSourceView()
		{
			return new SqExemptionAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SqExemptionAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SqExemptionAuditDataSourceView : ProviderDataSourceView<SqExemptionAudit, SqExemptionAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SqExemptionAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SqExemptionAuditDataSourceView(SqExemptionAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SqExemptionAuditDataSource SqExemptionAuditOwner
		{
			get { return Owner as SqExemptionAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal SqExemptionAuditSelectMethod SelectMethod
		{
			get { return SqExemptionAuditOwner.SelectMethod; }
			set { SqExemptionAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SqExemptionAuditService SqExemptionAuditProvider
		{
			get { return Provider as SqExemptionAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<SqExemptionAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<SqExemptionAudit> results = null;
			SqExemptionAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case SqExemptionAuditSelectMethod.Get:
					SqExemptionAuditKey entityKey  = new SqExemptionAuditKey();
					entityKey.Load(values);
					item = SqExemptionAuditProvider.Get(entityKey);
					results = new TList<SqExemptionAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case SqExemptionAuditSelectMethod.GetAll:
                    results = SqExemptionAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case SqExemptionAuditSelectMethod.GetPaged:
					results = SqExemptionAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case SqExemptionAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = SqExemptionAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = SqExemptionAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case SqExemptionAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = SqExemptionAuditProvider.GetByAuditId(_auditId);
					results = new TList<SqExemptionAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == SqExemptionAuditSelectMethod.Get || SelectMethod == SqExemptionAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				SqExemptionAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					SqExemptionAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<SqExemptionAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			SqExemptionAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region SqExemptionAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SqExemptionAuditDataSource class.
	/// </summary>
	public class SqExemptionAuditDataSourceDesigner : ProviderDataSourceDesigner<SqExemptionAudit, SqExemptionAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditDataSourceDesigner class.
		/// </summary>
		public SqExemptionAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SqExemptionAuditSelectMethod SelectMethod
		{
			get { return ((SqExemptionAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new SqExemptionAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region SqExemptionAuditDataSourceActionList

	/// <summary>
	/// Supports the SqExemptionAuditDataSourceDesigner class.
	/// </summary>
	internal class SqExemptionAuditDataSourceActionList : DesignerActionList
	{
		private SqExemptionAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the SqExemptionAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public SqExemptionAuditDataSourceActionList(SqExemptionAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SqExemptionAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion SqExemptionAuditDataSourceActionList
	
	#endregion SqExemptionAuditDataSourceDesigner
	
	#region SqExemptionAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the SqExemptionAuditDataSource.SelectMethod property.
	/// </summary>
	public enum SqExemptionAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion SqExemptionAuditSelectMethod

	#region SqExemptionAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditFilter : SqlFilter<SqExemptionAuditColumn>
	{
	}
	
	#endregion SqExemptionAuditFilter

	#region SqExemptionAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditExpressionBuilder : SqlExpressionBuilder<SqExemptionAuditColumn>
	{
	}
	
	#endregion SqExemptionAuditExpressionBuilder	

	#region SqExemptionAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;SqExemptionAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionAuditProperty : ChildEntityProperty<SqExemptionAuditChildEntityTypes>
	{
	}
	
	#endregion SqExemptionAuditProperty
}

