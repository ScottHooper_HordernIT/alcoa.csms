﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'Config' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table Config</remark>
	[Serializable]
	public enum ConfigList
	{
		/// <summary> 
		/// \\wao_services\infoshare\control\pss\waocsm\
		/// </summary>
		[EnumTextValue(@"\\wao_services\infoshare\control\pss\waocsm\")]
		APSSDir = 1, 

		/// <summary> 
		/// c:\temp\
		/// </summary>
		[EnumTextValue(@"c:\temp\")]
		TempDir = 12, 

		/// <summary> 
		/// \\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\CSMS Templates\
		/// </summary>
		[EnumTextValue(@"\\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\CSMS Templates\")]
		TemplatesDir = 13, 

		/// <summary> 
		/// -1
		/// </summary>
		[EnumTextValue(@"-1")]
		KpiContractorCutOff = 14, 

		/// <summary> 
		/// TrainingTemplate.xls
		/// </summary>
		[EnumTextValue(@"TrainingTemplate.xls")]
		TrainingTemplateFileName = 15, 

		/// <summary> 
		/// AUAAlcoaContractorServices@alcoa.com.au
		/// </summary>
		[EnumTextValue(@"AUAAlcoaContractorServices@alcoa.com.au")]
		ContactEmail = 16, 

		/// <summary> 
		/// MedicalTemplate.xls
		/// </summary>
		[EnumTextValue(@"MedicalTemplate.xls")]
		MedicalTemplateFileName = 17, 

		/// <summary> 
		/// 0
		/// </summary>
		[EnumTextValue(@"0")]
		SiteOfflineStatus = 18, 

		/// <summary> 
		/// Scheduled System Update
		/// </summary>
		[EnumTextValue(@"Scheduled System Update")]
		SiteOfflineReason = 19, 

		/// <summary> 
		/// 05/09/2011 17:30WST (GMT+8)
		/// </summary>
		[EnumTextValue(@"05/09/2011 17:30WST (GMT+8)")]
		SiteOfflineETA = 20, 

		/// <summary> 
		/// 40
		/// </summary>
		[EnumTextValue(@"40")]
		ContractorWeeklyFTEHours = 21, 

		/// <summary> 
		/// \\wao_central_1\alcoawa\APSS\DistribLogs\
		/// </summary>
		[EnumTextValue(@"\\wao_central_1\alcoawa\APSS\DistribLogs\")]
		RoboCopyLogsDir = 23, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-Contractor Incident Counts by Month.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-Contractor Incident Counts by Month.csv")]
		ContractorIncidentCountsFilePath = 26, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-Last Incident Date by Contractor Company.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-Last Incident Date by Contractor Company.csv")]
		ContractorLastIncidentDateFilePath = 27, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\iProc Contractor Services Report\iProc Contractor Services Report.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\iProc Contractor Services Report\iProc Contractor Services Report.csv")]
		ContractorIprocExtractFilePath = 28, 

		/// <summary> 
		/// NOsmtp.aua.alcoa.com
		/// </summary>
		[EnumTextValue(@"NOsmtp.aua.alcoa.com")]
		MailServer = 29, 

		/// <summary> 
		/// 0
		/// </summary>
		[EnumTextValue(@"0")]
		LoadingScreenRandomMessages = 30, 

		/// <summary> 
		/// Karen.Racco@alcoa.com.au
		/// </summary>
		[EnumTextValue(@"Karen.Racco@alcoa.com.au")]
		DefaultEhsConsultantContactEmail = 33, 

		/// <summary> 
		/// Karen.Racco@alcoa.com.au
		/// </summary>
		[EnumTextValue(@"Karen.Racco@alcoa.com.au")]
		DefaultEhsConsultant_WAO = 36, 

		/// <summary> 
		/// elizabeth.malseed@alcoa.com.au
		/// </summary>
		[EnumTextValue(@"elizabeth.malseed@alcoa.com.au")]
		DefaultEhsConsultant_VICOPS = 37, 

		/// <summary> 
		/// \\pth_smelter_1\alcoavo\Administration\Business Management\Published\DOCS\
		/// </summary>
		[EnumTextValue(@"\\pth_smelter_1\alcoavo\Administration\Business Management\Published\DOCS\")]
		VicOpsDocsDir = 38, 

		/// <summary> 
		/// MedicalTemplate_VICOPS.xls
		/// </summary>
		[EnumTextValue(@"MedicalTemplate_VICOPS.xls")]
		MedicalTemplateFileName_VICOPS = 39, 

		/// <summary> 
		/// \\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\Archive\
		/// </summary>
		[EnumTextValue(@"\\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\Archive\")]
		EbiArchiveDir = 40, 

		/// <summary> 
		/// \\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\Yesterday\
		/// </summary>
		[EnumTextValue(@"\\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\Yesterday\")]
		EbiYesterdayDir = 41, 

		/// <summary> 
		/// 0
		/// </summary>
		[EnumTextValue(@"0")]
		DisableAudit = 42, 

		/// <summary> 
		/// \\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\Today\
		/// </summary>
		[EnumTextValue(@"\\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\Today\")]
		EbiTodayDir = 43, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-Contractor Incident Counts by Month.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-Contractor Incident Counts by Month.csv")]
		AI_ContractorIncidentCountsFilePath = 44, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\ContractorHoursCounts.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\ContractorHoursCounts.csv")]
		ContractorHoursCountsFilePath = 45, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-ContractorHoursCounts-NEW.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI-ContractorHoursCounts-NEW.csv")]
		AI_ContractorHoursCountsFilePath = 46, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\ESP\projectslist.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\ESP\projectslist.csv")]
		KpiProjectsListFilePath = 47, 

		/// <summary> 
		/// \\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\All\
		/// </summary>
		[EnumTextValue(@"\\wao_services\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EBI\New\All\")]
		EbiAllFilePath = 51, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\CSMS Help Files\
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\CSMS Help Files\")]
		HelpFilesFilePath = 54, 

		/// <summary> 
		/// WAOCSM_Help_QuickReferenceGuide.pdf
		/// </summary>
		[EnumTextValue(@"WAOCSM_Help_QuickReferenceGuide.pdf")]
		HelpFileQuickReferenceKpi = 55, 

		/// <summary> 
		/// WAOCSM_Help_QuickReferenceGuide_AlcoaDirect_Login.pdf
		/// </summary>
		[EnumTextValue(@"WAOCSM_Help_QuickReferenceGuide_AlcoaDirect_Login.pdf")]
		HelpFileLoginAlcoaDirect = 56, 

		/// <summary> 
		/// WAOCSM_Help_QuickReferenceGuide_SMP.pdf
		/// </summary>
		[EnumTextValue(@"WAOCSM_Help_QuickReferenceGuide_SMP.pdf")]
		HelpFileSubmitSmp = 57, 

		/// <summary> 
		/// WAOCSM_Help_QuickReferenceGuide_CSA.pdf
		/// </summary>
		[EnumTextValue(@"WAOCSM_Help_QuickReferenceGuide_CSA.pdf")]
		HelpFileCompleteCsa = 58, 

		/// <summary> 
		/// WAOCSM_Help_QuickReferenceGuide_SRQ.pdf
		/// </summary>
		[EnumTextValue(@"WAOCSM_Help_QuickReferenceGuide_SRQ.pdf")]
		HelpFileCompleteSq = 59, 

		/// <summary> 
		/// KPI for Embedded and Non Embedded 1 2 3.pdf
		/// </summary>
		[EnumTextValue(@"KPI for Embedded and Non Embedded 1 2 3.pdf")]
		HelpFileKpiEmbedded = 60, 

		/// <summary> 
		/// S8.12 Evidence Folder Document.doc
		/// </summary>
		[EnumTextValue(@"S8.12 Evidence Folder Document.doc")]
		HelpFileS812Evidence = 61, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI Import Files\
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\AI Import Files\")]
		AiImportFilesFilePath = 62, 

		/// <summary> 
        /// The information shown above is a snapshot.
		/// </summary>
        [EnumTextValue(@"The information shown above is a snapshot.")]
		UpdateRadarTime = 63, 

		/// <summary> 
		/// \\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\iProc\P660_export.csv
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\iProc\P660_export.csv")]
		KpiPurchaseOrderListFilePath = 64, 

		/// <summary> 
		/// 0
		/// </summary>
		[EnumTextValue(@"0")]
		ContactEmailReceivesEmails = 66, 

		/// <summary> 
		/// 5
		/// </summary>
		[EnumTextValue(@"5")]
		MaxAllowedOpenContractorRequestsByUser = 68, 

		/// <summary> 
		/// adrian.jones@alcoa.com.au
		/// </summary>
		[EnumTextValue(@"adrian.jones@alcoa.com.au")]
		ProcurementRegionalManager = 69, 

		/// <summary> 
		/// 7
		/// </summary>
		[EnumTextValue(@"7")]
		EscalationChainDaysWithProcurement = 70, 

		/// <summary> 
		/// 7
		/// </summary>
		[EnumTextValue(@"7")]
		EscalationChainDaysWithHsAssessor = 71, 

		/// <summary> 
		/// 28
		/// </summary>
		[EnumTextValue(@"28")]
		EscalationChainDaysWithSupplier = 72, 

		/// <summary> 
		/// d:\ARP-CRP 09 Mar 12.csv
		/// </summary>
		[EnumTextValue(@"d:\ARP-CRP 09 Mar 12.csv")]
		CrpContactsFilePath = 73, 

		/// <summary> 
		/// 5.0
		/// </summary>
		[EnumTextValue(@"5.0")]
		IfeInjuryRatioYtd = 76, 

		/// <summary> 
		/// 5
		/// </summary>
		[EnumTextValue(@"5")]
		IfeInjuryRatioMonth = 77, 

		/// <summary> 
		/// 
		/// </summary>
		[EnumTextValue(@"")]
		SqExemptionFormApssNumber = 78, 

		/// <summary> 
		/// 0
		/// </summary>
		[EnumTextValue(@"0")]
		CpmsLinkVisibleToReaders = 79,

        //Adding by Pankaj Dikshit for <DT 2579, 21-Aug-2012> - Start
        /// <summary> 
		/// 3
		/// </summary>
		[EnumTextValue(@"3")]
		EscalationChainDaysWithProcurementEmailDaysFrequency = 80, 

		/// <summary> 
		/// 3
		/// </summary>
		[EnumTextValue(@"3")]
		EscalationChainDaysWithHsAssessorEmailDaysFrequency = 81, 

		/// <summary> 
		/// 3
		/// </summary>
		[EnumTextValue(@"3")]
		EscalationChainDaysWithSupplierEmailDaysFrequency = 82,

        /// <summary> 
		/// 3
		/// </summary>
		[EnumTextValue(@"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\EHSIMS Data\")]
        AIContractorListFilePath = 83,
        //Adding by Pankaj Dikshit for <DT 2579, 21-Aug-2012> - End

           //Adding By Bishwajit Sahoo for Item#34, 21-Dec-2012> - Start
        /// <summary> 
        /// C:\Bishwajit Sahoo\csv\IhsRnCaOpenClosedMetrics.csv
		/// </summary>
        [EnumTextValue(@"IhsRnCaOpenClosedMetricsFilePath")]
        IhsRnCaOpenClosedMetricsFilePath = 84,
        //Adding By Bishwajit Sahoo for Item#34, 21-Dec-2012> - End

        //Added by Jolly Roy for Spec # 72 - Start
        /// <summary> 
		/// \\aua.alcoa.com\dfs\BGN\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\CSMS Templates\CSM_Contractor_Contact_Information.xls
		/// </summary>
        [EnumTextValue(@"ContractorContactTemplateFilePath")]
        ContractorContactTemplateFilePath = 89
        //Added by Jolly Roy for Spec # 72 - End
	}
}
