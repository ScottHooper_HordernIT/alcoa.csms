﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Web;
using Repo.CSMS.DAL.Infrastructure;
using System.IO;

namespace Repo.CSMS.DAL.EntityModels
{
    #region Metadata

    public partial class User
    {
        [DisplayName("Full Name")]
        public String FullName
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName).Trim();
            }
        }
    }

    public partial class AdHoc_Radar_Items
    {
        //[DisplayName("Full Name")]
        //public decimal? KpiScoreYtdDecimal
        //{
        //    get
        //    {
        //        decimal? score = null;
        //        decimal scoreDecimal = -1;
        //        if (decimal.TryParse(this.KpiScoreYtd, out scoreDecimal))
        //        {
        //            score = scoreDecimal;
        //        }
        //        return score;
        //    }
        //}

        public decimal? KpiScore(int monthId = 0)
        {
            decimal? score = null;

            if (monthId == 0)
            {
                //Ytd score
                decimal scoreDecimal = -1;
                if (decimal.TryParse(this.KpiScoreYtd, out scoreDecimal))
                {
                    score = scoreDecimal;
                }
            }
            else
            {
                //Monthly score
                string monthScore = "";

                switch (monthId)
                {
                    case 1:
                        monthScore = this.KpiScoreJan;
                        break;
                    case 2:
                        monthScore = this.KpiScoreFeb;
                        break;
                    case 3:
                        monthScore = this.KpiScoreMar;
                        break;
                    case 4:
                        monthScore = this.KpiScoreApr;
                        break;
                    case 5:
                        monthScore = this.KpiScoreMay;
                        break;
                    case 6:
                        monthScore = this.KpiScoreJun;
                        break;
                    case 7:
                        monthScore = this.KpiScoreJul;
                        break;
                    case 8:
                        monthScore = this.KpiScoreAug;
                        break;
                    case 9:
                        monthScore = this.KpiScoreSep;
                        break;
                    case 10:
                        monthScore = this.KpiScoreOct;
                        break;
                    case 11:
                        monthScore = this.KpiScoreNov;
                        break;
                    case 12:
                        monthScore = this.KpiScoreDec;
                        break;
                    default:
                        monthScore = this.KpiScoreYtd;
                        break;
                }

                decimal scoreDecimal = -1;
                if (decimal.TryParse(monthScore, out scoreDecimal))
                {
                    score = scoreDecimal;
                }
            }

            return score;
        }

        public void SetKpiScore(string kpiScore, int monthId = 0)
        {
            if (monthId == 0)
            {
                //Ytd score
                this.KpiScoreYtd = kpiScore;
            }
            else
            {
                //Monthly score
                switch (monthId)
                {
                    case 1:
                        this.KpiScoreJan = kpiScore;
                        break;
                    case 2:
                        this.KpiScoreFeb = kpiScore;
                        break;
                    case 3:
                        this.KpiScoreMar = kpiScore;
                        break;
                    case 4:
                        this.KpiScoreApr = kpiScore;
                        break;
                    case 5:
                        this.KpiScoreMay = kpiScore;
                        break;
                    case 6:
                        this.KpiScoreJun = kpiScore;
                        break;
                    case 7:
                        this.KpiScoreJul = kpiScore;
                        break;
                    case 8:
                        this.KpiScoreAug = kpiScore;
                        break;
                    case 9:
                        this.KpiScoreSep = kpiScore;
                        break;
                    case 10:
                        this.KpiScoreOct = kpiScore;
                        break;
                    case 11:
                        this.KpiScoreNov = kpiScore;
                        break;
                    case 12:
                        this.KpiScoreDec = kpiScore;
                        break;
                    default:
                        this.KpiScoreYtd = kpiScore;
                        break;
                }
            }
        }
        public string CompaniesNotCompliantMonth(int monthId = 0)  // Added by Ashley Goldstraw to return month score if that was what was selected.  8/2/2016
        {
            string monthScore = "";
            if (monthId == 0)
            {
                    monthScore = this.CompaniesNotCompliant;
            }
            else
            {
                //Monthly score
                switch (monthId)
                {
                    case 1:
                        monthScore = this.CompaniesNotCompliantJan;
                        break;
                    case 2:
                        monthScore = this.CompaniesNotCompliantFeb;
                        break;
                    case 3:
                        monthScore = this.CompaniesNotCompliantMar;
                        break;
                    case 4:
                        monthScore = this.CompaniesNotCompliantApr;
                        break;
                    case 5:
                        monthScore = this.CompaniesNotCompliantMay;
                        break;
                    case 6:
                        monthScore = this.CompaniesNotCompliantJun;
                        break;
                    case 7:
                        monthScore = this.CompaniesNotCompliantJul;
                        break;
                    case 8:
                        monthScore = this.CompaniesNotCompliantAug;
                        break;
                    case 9:
                        monthScore = this.CompaniesNotCompliantSep;
                        break;
                    case 10:
                        monthScore = this.CompaniesNotCompliantOct;
                        break;
                    case 11:
                        monthScore = this.CompaniesNotCompliantNov;
                        break;
                    case 12:
                        monthScore = this.CompaniesNotCompliantDec;
                        break;
                    default:
                        monthScore = this.CompaniesNotCompliant;
                        break;
                }

            }
            return monthScore;
        }
    }

    #endregion

    #region Custom Metadata


    #endregion
}
