﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'QuestionnaireType' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnaireType 
	{
		/// <summary>			
		/// QuestionnaireTypeId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "QuestionnaireType"</remarks>
		System.Int32 QuestionnaireTypeId { get; set; }
				
		
		
		/// <summary>
		/// QuestionnaireTypeName : 
		/// </summary>
		System.String  QuestionnaireTypeName  { get; set; }
		
		/// <summary>
		/// QuestionnaireTypeDesc : 
		/// </summary>
		System.String  QuestionnaireTypeDesc  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _questionnaireServicesSelectedQuestionnaireTypeId
		/// </summary>	
		TList<QuestionnaireServicesSelected> QuestionnaireServicesSelectedCollection {  get;  set;}	

		#endregion Data Properties

	}
}


