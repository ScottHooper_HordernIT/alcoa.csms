﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskService
    {
        AdminTask Get(Expression<Func<AdminTask, bool>> where, List<Expression<Func<AdminTask, object>>> includes);
        List<AdminTask> GetMany(int? top, Expression<Func<AdminTask, bool>> where, Func<IQueryable<AdminTask>, IOrderedQueryable<AdminTask>> orderBy, List<Expression<Func<AdminTask, object>>> includes);
        List<AdminTask> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTask, bool>> where, Func<IQueryable<AdminTask>, IOrderedQueryable<AdminTask>> orderBy, List<Expression<Func<AdminTask, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTask, bool>> where, List<Expression<Func<AdminTask, object>>> includes);
        void Insert(AdminTask item, bool saveChanges = true);
        void Update(AdminTask item, bool saveChanges = true);
        void Delete(AdminTask item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskService : IAdminTaskService
    {
        private readonly IRepository<AdminTask> _repository;

        public AdminTaskService(IRepository<AdminTask> repository)
        {
            this._repository = repository;
        }

        public AdminTask Get(Expression<Func<AdminTask, bool>> where, List<Expression<Func<AdminTask, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTask> GetMany(int? top, Expression<Func<AdminTask, bool>> where, Func<IQueryable<AdminTask>, IOrderedQueryable<AdminTask>> orderBy, List<Expression<Func<AdminTask, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTask> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTask, bool>> where, Func<IQueryable<AdminTask>, IOrderedQueryable<AdminTask>> orderBy, List<Expression<Func<AdminTask, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTask, bool>> where, List<Expression<Func<AdminTask, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTask item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTask");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTask item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTask");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTask item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTask");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
