﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Other_userLine : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }


    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            if (auth.Role != null)
            {
                hlUser.Text = String.Format("{0} from {1} as {2} {3}", auth.Role, auth.CompanyName, auth.FirstName, auth.LastName);
                hlUser.ToolTip = auth.UserLogon;
            }
            else { Response.Redirect("~/AccessDenied.aspx?error=" + Server.UrlEncode("Unauthorized user")); }
        }
    }
}
