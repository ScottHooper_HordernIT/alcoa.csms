﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Linq;
using KaiZen.CSMS.Entities;

public partial class MasterPages_user : System.Web.UI.MasterPage
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e)
    {
        Helper._Auth.PageLoad(auth);

        if ((auth.RoleId == (int)RoleList.CWKMaintainer || auth.RoleId == (int)RoleList.CWKEnquiry) &&
            !(this.Request.Url.AbsolutePath == "/ContractorDataManagement.aspx" || this.Request.Url.AbsolutePath == "/ContractorDataManagementDetail.aspx" || this.Request.Url.AbsolutePath == "/CompanyContractorReview.aspx"))
        {
            Response.Redirect("~/ContractorDataManagement.aspx");
        }
        else
        {
            RegisterSessionKeepAlive();
        }
    }
    private void RegisterSessionKeepAlive()
    {
        try
        {		// NOTE: Everything needs converted to milliseconds		
            int timeoutWindow = 600; // Number of seconds before session will timeout to fire a simulated PostBack to renew Session		
            int timeout = (this.Session.Timeout * 60000) - (timeoutWindow * 1000);
            // if (System.Diagnostics.Debugger.IsAttached) timeout = 10000;	 // For Testing, comment if not needed		
            string reconnectUrl = VirtualPathUtility.ToAbsolute("~/reconnect.aspx");
            string startupScript = string.Format("window.setInterval('reconnectSession()', {0});\n", timeout); //Set to length required		
            string scriptKey = "SessionReconnectScript";
            string startupScriptKey = "SessionReconnectStartupScript";
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            if (sm != null && sm.IsInAsyncPostBack)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), startupScriptKey, startupScript, true);
            else
            {
                if (!Page.ClientScript.IsClientScriptBlockRegistered(this.Page.GetType(), scriptKey))
                {
                    System.Text.StringBuilder script = new System.Text.StringBuilder();
                    script.Append("var reconnectCount = 0;\n");	// Number of Reconnects				
                    script.Append("var reconnectMax = 50;\n");	// Maximum Reconnects				
                    script.Append("function reconnectSession(){\n");
                    script.Append("\treconnectCount++;\n");
                    script.Append("\tif (reconnectCount < reconnectMax){\n");
                    script.Append("\t\twindow.status = 'Session Refreshed ' + reconnectCount.toString() + ' time(s)';\n");
                    script.Append("\t\tvar img = new Image(1,1);\n");
                    script.AppendFormat("\t\timg.src = '{0}';\n", reconnectUrl);
                    script.Append("\t}\n");
                    script.Append("}\n");
                    Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), scriptKey, script.ToString(), true);
                }
                if (!Page.ClientScript.IsStartupScriptRegistered(this.Page.GetType(), startupScriptKey))
                    Page.ClientScript.RegisterStartupScript(this.Page.GetType(), startupScriptKey, startupScript, true);
            }
        }
        catch (Exception) { throw; }
    }

    //implemented for pop-up exception message
    protected void scriptmanager1_AsyncPostBackError(object sender,
     AsyncPostBackErrorEventArgs e)
    {
        scriptmanager1.AsyncPostBackErrorMessage =
            "An error occurred during the request: " +
            e.Exception.Message;
    }
    

   
}
