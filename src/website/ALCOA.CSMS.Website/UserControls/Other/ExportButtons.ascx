﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ExportButtons" Codebehind="ExportButtons.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional" onunload ="UpdatePanel_Unload" EnableHierarchyRecreation ="false">
    <contenttemplate>
    <div style="padding-top: 2px">
        <TABLE style="HEIGHT: 20px" cellSpacing=0 cellPadding=0 border=0>
        <TBODY><TR>
        <TD align="right" style="VERTICAL-ALIGN: middle; padding-right: 3px; text-align:-moz-right; text-align:right;" vAlign=middle><STRONG>Export:</STRONG></TD>
        <TD align="right" style="VERTICAL-ALIGN: middle; padding-right: 2px; text-align:-moz-right; text-align:right;" vAlign=middle>
            <asp:ImageButton id="imgBtnExcel" onclick="imgBtnExcel_Click" runat="server" ToolTip="Export table to Microsoft Excel (XLS)" ImageUrl="~/Images/ExportLogos/excel.gif"></asp:ImageButton>
            <asp:ImageButton id="imgBtnWord" onclick="imgBtnWord_Click" runat="server" ToolTip="Export table to Microsoft Word Document (RTF)" ImageUrl="~/Images/ExportLogos/word.gif"></asp:ImageButton>
            <asp:ImageButton id="imgBtnPdf" onclick="imgBtnPdf_Click" runat="server" ToolTip="Export table to Adobe Acrobat (PDF)" ImageUrl="~/Images/ExportLogos/pdf.gif"></asp:ImageButton>
            </TD>
        </TR></TBODY>
        </TABLE></div>
</contenttemplate>
<Triggers>
    <asp:PostBackTrigger ControlID="imgBtnExcel"></asp:PostBackTrigger>
    <asp:PostBackTrigger ControlID="imgBtnWord"></asp:PostBackTrigger>
    <asp:PostBackTrigger ControlID="imgBtnPdf"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

<cc1:ASPxGridViewExporter ID="gridExporter" runat="server"
    FileName="CSM" GridViewID="grid" landscape="true"   >
    
</cc1:ASPxGridViewExporter>

<table style="width:100%; text-align:left; vertical-align:top">
<tr>
<td style="width:100%; text-align:left; vertical-align:top">
<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Warning" Height="111px"
    Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    Width="439px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <SizeGripImage Height="16px" Width="16px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px">
    </CloseButtonImage>
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" Height="30px" Font-Size="14px" ID="ASPxLabel1">
                <Border BorderColor="White" BorderWidth="10px"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
</td>
</tr>
</table>