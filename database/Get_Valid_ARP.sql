

/****** Object:  StoredProcedure [dbo].[Get_Valid_ARP]    Script Date: 3/08/2015 4:48:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Get_Valid_ARP]                
(                                        
 @CompanyId INT,                          
 @SiteId INT               
)                         
AS               
DECLARE @PersonIds varchar (MAX)        
DECLARE @vCompanyId INT, @vSiteId INT        
          
CREATE TABLE #ARPTbl (CompanyId INT, SiteId INT, PersonId INT)        
            
IF (@SiteId > 0)                                     
 BEGIN                
  SELECT @PersonIds = B.ARPPersonId                        
  FROM [dbo].[CompanySiteCategoryStandard] O                    
  LEFT OUTER JOIN ARPNameMaster AS B                          
  ON O.ArpUserId = B.ArpUserId                  
  WHERE O.CompanyId = @CompanyId AND O.SiteId= @SiteId        
          
  INSERT INTO #ARPTbl(CompanyId, SiteId, PersonId)         
  SELECT @CompanyId,@SiteId,[VALUE] FROM [dbo].[SPLITSITE](',',@PersonIds)        
 END         
ELSE         
 BEGIN         
  BEGIN TRY        
   DECLARE curARP CURSOR FOR        
   SELECT B.ARPPersonId, O.CompanyId, O.SiteId                         
   FROM [dbo].[CompanySiteCategoryStandard] O                    
   LEFT OUTER JOIN ARPNameMaster AS B                          
   ON O.ArpUserId = B.ArpUserId                  
   WHERE O.CompanyId = @CompanyId AND O.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * RegionId) = @SiteId)              
   AND B.ARPPersonId IS NOT NULL AND RTRIM(LTRIM(B.ARPPersonId)) <> ''        
   OPEN curARP         
   FETCH NEXT FROM curARP INTO @PersonIds, @vCompanyId, @vSiteId        
    WHILE @@FETCH_STATUS = 0        
    BEGIN        
    INSERT INTO #ARPTbl(CompanyId, SiteId, PersonId)         
    SELECT @vCompanyId,@vSiteId,[VALUE] FROM [dbo].[SPLITSITE](',',@PersonIds)        
    FETCH NEXT FROM curARP INTO @PersonIds, @vCompanyId, @vSiteId        
    END        
            
    CLOSE curARP        
    DEALLOCATE curARP        
   END TRY        
   BEGIN CATCH        
    CLOSE curARP        
    DEALLOCATE curARP        
   END CATCH        
 END        
        
  
 Select inner_sql.CompanyId,inner_sql.SiteId,inner_sql.PersonId,inner_sql.SiteName,inner_sql.Full_Name,max(inner_sql.Valid_Until) as Valid_Until,inner_sql.op_centre  
from  
(  
 SELECT B.CompanyId, B.SiteId, B.PersonId, E.SiteName,        
 A.Full_Name,DATEADD(yy,2, A.COURSE_END_DATE) As Valid_Until, A.op_centre        
 FROM #ARPTbl B         
 LEFT OUTER JOIN HR..XXHR_ARP_CRP_ALL_V AS A         
 ON B.PersonId = A.Person_Id        
 LEFT OUTER JOIN SITES AS E     
 ON E.SiteId = B.SiteId) as inner_sql  
 group by CompanyId,SiteId,PersonId,SiteName,Full_Name,op_centre  
           
 DROP  TABLE #ARPTbl 
----------------------------------------

GO


