using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxEditors;

using System.Collections.Specialized;
using DevExpress.Web.ASPxTabControl;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;

using System.Data.SqlClient;

public partial class UserControls_Training_Packages : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            btnEdit.Visible = false;
            SessionHandler.ViewMode = "View";
            //grid.ExpandAll();

            grid.ExpandRow(0);
            grid.ExpandRow(4);
            grid.ExpandRow(3);
            grid.ExpandRow(2);
            grid.ExpandRow(1);

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):

                    break;
                case ((int)RoleList.Contractor):
                    if (auth.RoleId == 2)
                    {
                        if (!Helper.General.isPrivileged2User(auth.UserId))
                        {
                            grid.Columns["Answers"].Visible = false;
                        }
                    }
                    break;
                case ((int)RoleList.Administrator):
                    btnEdit.Visible = true;
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        if (!(String.IsNullOrEmpty(SessionHandler.ViewMode)))
        {
            if (SessionHandler.ViewMode == "View")
            {
                grid.Columns[0].Visible = true;
                SessionHandler.ViewMode = "Edit";
                btnEdit.Text = "(View)";
            }
            else
            {
                grid.Columns[0].Visible = false;
                SessionHandler.ViewMode = "View";
                btnEdit.Text = "(Edit)";
            }
        }
    }

    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        e.NewValues["DocumentId"] = e.Keys["DocumentId"];
        e.NewValues["DocumentType"] = "TP";

        e.NewValues["RootNode"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbRootNode")).Value;
        e.NewValues["ParentNode"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbParentNode")).Value;
        e.NewValues["ChildNode"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbChildNode")).Value;
        e.NewValues["DocumentName"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbDocumentName")).Value;
        e.NewValues["DocumentFileName"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbDocumentFileName")).Value;

        if (Session["dataFN"] != null)
        {
            e.NewValues["UploadedFile"] = Session["data"];
            e.NewValues["UploadedFileName"] = Session["dataFN"];
        }

        if (Session["dataFN2"] != null)
        {
            e.NewValues["UploadedFile2"] = Session["data2"];
            e.NewValues["UploadedFileName2"] = Session["dataFN2"];
        }

        Session.Remove("data");
        Session.Remove("data2");
        Session.Remove("dataFN");
        Session.Remove("dataFN2");
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["DocumentType"] = "TP";
    }

    protected void grid_OnRowInserting(object sender, ASPxDataInsertingEventArgs e) //Default Values 
    {
        e.NewValues["DocumentType"] = "TP";

        e.NewValues["RootNode"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbRootNode")).Value;
        e.NewValues["ParentNode"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbParentNode")).Value;
        e.NewValues["ChildNode"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbChildNode")).Value;
        e.NewValues["DocumentName"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbDocumentName")).Value;
        e.NewValues["DocumentFileName"] = ((ASPxTextBox)grid.FindEditFormTemplateControl("tbDocumentFileName")).Value;

        if (Session["dataFN"] != null)
        {
            e.NewValues["UploadedFile"] = Session["data"];
            e.NewValues["UploadedFileName"] = Session["dataFN"];
        }

        if (Session["dataFN2"] != null)
        {
            e.NewValues["UploadedFile2"] = Session["data2"];
            e.NewValues["UploadedFileName2"] = Session["dataFN2"];
        }

        Session.Remove("data");
        Session.Remove("data2");
        Session.Remove("dataFN");
        Session.Remove("dataFN2");
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        ASPxUploadControl uc = ((ASPxUploadControl)grid.FindEditFormTemplateControl("FileUpload1"));

        if (uc.UploadedFiles != null && uc.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(uc.UploadedFiles[0].FileName) && uc.UploadedFiles[0].IsValid)
            {
                string fileName = uc.UploadedFiles[0].FileName;
                uc.UploadedFiles[0].SaveAs(MapPath("~/Temp/" + fileName));
            }
        }
    }
    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        ASPxUploadControl uc = ((ASPxUploadControl)grid.FindEditFormTemplateControl("FileUpload1"));

        if (uc.UploadedFiles != null && uc.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(uc.UploadedFiles[0].FileName) && uc.UploadedFiles[0].IsValid)
            {
                string fileName = uc.UploadedFiles[0].FileName;
                uc.UploadedFiles[0].SaveAs(MapPath("~/Temp/" + fileName));
            }
        }
    }

    protected void ASPxUploadControl1_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        if ((sender as ASPxUploadControl).UploadedFiles[0].IsValid)
        {
            Session["data"] = (sender as ASPxUploadControl).UploadedFiles[0].FileBytes;
            Session["dataFN"] = (sender as ASPxUploadControl).UploadedFiles[0].FileName;
        }
    }
    protected void ASPxUploadControl2_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        if ((sender as ASPxUploadControl).UploadedFiles[0].IsValid)
        {
            Session["data2"] = (sender as ASPxUploadControl).UploadedFiles[0].FileBytes;
            Session["dataFN2"] = (sender as ASPxUploadControl).UploadedFiles[0].FileName;
        }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        int i = 1;
        if (auth.RoleId == (int)RoleList.Administrator) { i++; };
        if (Helper.General.isPrivileged2User(auth.UserId)) { i++; };

        if (i > 0)
        {
            if (e.RowType != GridViewRowType.Data) return;
            int DocumentId = (int)grid.GetRowValues(e.VisibleIndex, "DocumentId");

            HyperLink hlF = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlF") as HyperLink;
            if (hlF != null)
            {
                if (grid.GetRowValues(e.VisibleIndex, "DocumentFileName") != null)
                {
                    string DocumentFileName = (string)grid.GetRowValues(e.VisibleIndex, "DocumentFileName");
                    hlF.Text = DocumentFileName;
                    hlF.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt(String.Format("Type=APSS&SubType=TP&File={0}", DocumentFileName)));
                }
            }

            string UploadedFileName = (string)grid.GetRowValues(e.VisibleIndex, "UploadedFileName");
            string UploadedFileName2 = (string)grid.GetRowValues(e.VisibleIndex, "UploadedFileName2");

            HyperLink hlQ = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlQ") as HyperLink;
            HyperLink hlA = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlA") as HyperLink;
            
            if (hlQ != null)
            {
                if (!String.IsNullOrEmpty(UploadedFileName))
                {
                    hlQ.Text = "Questionnaire";
                    hlQ.ToolTip = String.Format("Download '{0}'", UploadedFileName);
                    hlQ.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt(String.Format("Type=CSMS&SubType=TrainingQuestionnaire&File={0}", DocumentId)));
                }
                else
                {
                    hlQ.Text = "";
                }
            }

            if (hlA != null)
            {
                if (i > 1)
                {
                    if (!String.IsNullOrEmpty(UploadedFileName2))
                    {
                        hlA.Text = "Answers";
                        hlA.ToolTip = String.Format("Download '{0}'", UploadedFileName2);
                        hlA.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt(String.Format("Type=CSMS&SubType=TrainingQuestionnaireAnswers&File={0}", DocumentId)));
                    }
                    else
                    {
                        hlA.Text = "";
                    }
                }
                else
                {
                    hlA.NavigateUrl = "";
                    hlA.Text = "Not Available";
                }
            }
        }
    }
}
