﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileVaultTableProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FileVaultTableProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.FileVaultTable, KaiZen.CSMS.Entities.FileVaultTableKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultTableKey key)
		{
			return Delete(transactionManager, key.FileVaultTableId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_fileVaultTableId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _fileVaultTableId)
		{
			return Delete(null, _fileVaultTableId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultTableId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _fileVaultTableId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_SubCategory key.
		///		FK_FileVaultTable_SubCategory Description: 
		/// </summary>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultSubCategoryId(System.Int32 _fileVaultSubCategoryId)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(_fileVaultSubCategoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_SubCategory key.
		///		FK_FileVaultTable_SubCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		/// <remarks></remarks>
		public TList<FileVaultTable> GetByFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(transactionManager, _fileVaultSubCategoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_SubCategory key.
		///		FK_FileVaultTable_SubCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultSubCategoryId(transactionManager, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_SubCategory key.
		///		fkFileVaultTableSubCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultSubCategoryId(System.Int32 _fileVaultSubCategoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByFileVaultSubCategoryId(null, _fileVaultSubCategoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_SubCategory key.
		///		fkFileVaultTableSubCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultSubCategoryId(System.Int32 _fileVaultSubCategoryId, int start, int pageLength,out int count)
		{
			return GetByFileVaultSubCategoryId(null, _fileVaultSubCategoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_SubCategory key.
		///		FK_FileVaultTable_SubCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public abstract TList<FileVaultTable> GetByFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultSubCategoryId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_Users key.
		///		FK_FileVaultTable_Users Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_Users key.
		///		FK_FileVaultTable_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		/// <remarks></remarks>
		public TList<FileVaultTable> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_Users key.
		///		FK_FileVaultTable_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_Users key.
		///		fkFileVaultTableUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_Users key.
		///		fkFileVaultTableUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_Users key.
		///		FK_FileVaultTable_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public abstract TList<FileVaultTable> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_FileVault key.
		///		FK_FileVaultTable_FileVault Description: 
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultId(System.Int32 _fileVaultId)
		{
			int count = -1;
			return GetByFileVaultId(_fileVaultId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_FileVault key.
		///		FK_FileVaultTable_FileVault Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		/// <remarks></remarks>
		public TList<FileVaultTable> GetByFileVaultId(TransactionManager transactionManager, System.Int32 _fileVaultId)
		{
			int count = -1;
			return GetByFileVaultId(transactionManager, _fileVaultId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_FileVault key.
		///		FK_FileVaultTable_FileVault Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultId(TransactionManager transactionManager, System.Int32 _fileVaultId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultId(transactionManager, _fileVaultId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_FileVault key.
		///		fkFileVaultTableFileVault Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_fileVaultId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultId(System.Int32 _fileVaultId, int start, int pageLength)
		{
			int count =  -1;
			return GetByFileVaultId(null, _fileVaultId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_FileVault key.
		///		fkFileVaultTableFileVault Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public TList<FileVaultTable> GetByFileVaultId(System.Int32 _fileVaultId, int start, int pageLength,out int count)
		{
			return GetByFileVaultId(null, _fileVaultId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_FileVaultTable_FileVault key.
		///		FK_FileVaultTable_FileVault Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.FileVaultTable objects.</returns>
		public abstract TList<FileVaultTable> GetByFileVaultId(TransactionManager transactionManager, System.Int32 _fileVaultId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.FileVaultTable Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultTableKey key, int start, int pageLength)
		{
			return GetByFileVaultTableId(transactionManager, key.FileVaultTableId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileVaultTable index.
		/// </summary>
		/// <param name="_fileVaultTableId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultTableId(System.Int32 _fileVaultTableId)
		{
			int count = -1;
			return GetByFileVaultTableId(null,_fileVaultTableId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultTable index.
		/// </summary>
		/// <param name="_fileVaultTableId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultTableId(System.Int32 _fileVaultTableId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultTableId(null, _fileVaultTableId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultTable index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultTableId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultTableId(TransactionManager transactionManager, System.Int32 _fileVaultTableId)
		{
			int count = -1;
			return GetByFileVaultTableId(transactionManager, _fileVaultTableId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultTable index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultTableId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultTableId(TransactionManager transactionManager, System.Int32 _fileVaultTableId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultTableId(transactionManager, _fileVaultTableId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultTable index.
		/// </summary>
		/// <param name="_fileVaultTableId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultTableId(System.Int32 _fileVaultTableId, int start, int pageLength, out int count)
		{
			return GetByFileVaultTableId(null, _fileVaultTableId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileVaultTable index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultTableId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultTableId(TransactionManager transactionManager, System.Int32 _fileVaultTableId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_FileVaultTable_FileVaultId_FileVaultSubCategoryId index.
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultIdFileVaultSubCategoryId(System.Int32 _fileVaultId, System.Int32 _fileVaultSubCategoryId)
		{
			int count = -1;
			return GetByFileVaultIdFileVaultSubCategoryId(null,_fileVaultId, _fileVaultSubCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultTable_FileVaultId_FileVaultSubCategoryId index.
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultIdFileVaultSubCategoryId(System.Int32 _fileVaultId, System.Int32 _fileVaultSubCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultIdFileVaultSubCategoryId(null, _fileVaultId, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultTable_FileVaultId_FileVaultSubCategoryId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultIdFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultId, System.Int32 _fileVaultSubCategoryId)
		{
			int count = -1;
			return GetByFileVaultIdFileVaultSubCategoryId(transactionManager, _fileVaultId, _fileVaultSubCategoryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultTable_FileVaultId_FileVaultSubCategoryId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultIdFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultId, System.Int32 _fileVaultSubCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByFileVaultIdFileVaultSubCategoryId(transactionManager, _fileVaultId, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultTable_FileVaultId_FileVaultSubCategoryId index.
		/// </summary>
		/// <param name="_fileVaultId"></param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultIdFileVaultSubCategoryId(System.Int32 _fileVaultId, System.Int32 _fileVaultSubCategoryId, int start, int pageLength, out int count)
		{
			return GetByFileVaultIdFileVaultSubCategoryId(null, _fileVaultId, _fileVaultSubCategoryId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_FileVaultTable_FileVaultId_FileVaultSubCategoryId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_fileVaultId"></param>
		/// <param name="_fileVaultSubCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileVaultTable GetByFileVaultIdFileVaultSubCategoryId(TransactionManager transactionManager, System.Int32 _fileVaultId, System.Int32 _fileVaultSubCategoryId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileVaultTable&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileVaultTable&gt;"/></returns>
		public static TList<FileVaultTable> Fill(IDataReader reader, TList<FileVaultTable> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.FileVaultTable c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FileVaultTable")
					.Append("|").Append((System.Int32)reader[((int)FileVaultTableColumn.FileVaultTableId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FileVaultTable>(
					key.ToString(), // EntityTrackingKey
					"FileVaultTable",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.FileVaultTable();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FileVaultTableId = (System.Int32)reader[((int)FileVaultTableColumn.FileVaultTableId - 1)];
					c.FileVaultId = (System.Int32)reader[((int)FileVaultTableColumn.FileVaultId - 1)];
					c.FileVaultSubCategoryId = (System.Int32)reader[((int)FileVaultTableColumn.FileVaultSubCategoryId - 1)];
					c.FileNameCustom = (reader.IsDBNull(((int)FileVaultTableColumn.FileNameCustom - 1)))?null:(System.String)reader[((int)FileVaultTableColumn.FileNameCustom - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)FileVaultTableColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)FileVaultTableColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.FileVaultTable entity)
		{
			if (!reader.Read()) return;
			
			entity.FileVaultTableId = (System.Int32)reader[((int)FileVaultTableColumn.FileVaultTableId - 1)];
			entity.FileVaultId = (System.Int32)reader[((int)FileVaultTableColumn.FileVaultId - 1)];
			entity.FileVaultSubCategoryId = (System.Int32)reader[((int)FileVaultTableColumn.FileVaultSubCategoryId - 1)];
			entity.FileNameCustom = (reader.IsDBNull(((int)FileVaultTableColumn.FileNameCustom - 1)))?null:(System.String)reader[((int)FileVaultTableColumn.FileNameCustom - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)FileVaultTableColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)FileVaultTableColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.FileVaultTable entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FileVaultTableId = (System.Int32)dataRow["FileVaultTableId"];
			entity.FileVaultId = (System.Int32)dataRow["FileVaultId"];
			entity.FileVaultSubCategoryId = (System.Int32)dataRow["FileVaultSubCategoryId"];
			entity.FileNameCustom = Convert.IsDBNull(dataRow["FileNameCustom"]) ? null : (System.String)dataRow["FileNameCustom"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileVaultTable"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileVaultTable Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultTable entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region FileVaultSubCategoryIdSource	
			if (CanDeepLoad(entity, "FileVaultSubCategory|FileVaultSubCategoryIdSource", deepLoadType, innerList) 
				&& entity.FileVaultSubCategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.FileVaultSubCategoryId;
				FileVaultSubCategory tmpEntity = EntityManager.LocateEntity<FileVaultSubCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(FileVaultSubCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FileVaultSubCategoryIdSource = tmpEntity;
				else
					entity.FileVaultSubCategoryIdSource = DataRepository.FileVaultSubCategoryProvider.GetByFileVaultSubCategoryId(transactionManager, entity.FileVaultSubCategoryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileVaultSubCategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FileVaultSubCategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FileVaultSubCategoryProvider.DeepLoad(transactionManager, entity.FileVaultSubCategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FileVaultSubCategoryIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region FileVaultIdSource	
			if (CanDeepLoad(entity, "FileVault|FileVaultIdSource", deepLoadType, innerList) 
				&& entity.FileVaultIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.FileVaultId;
				FileVault tmpEntity = EntityManager.LocateEntity<FileVault>(EntityLocator.ConstructKeyFromPkItems(typeof(FileVault), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FileVaultIdSource = tmpEntity;
				else
					entity.FileVaultIdSource = DataRepository.FileVaultProvider.GetByFileVaultId(transactionManager, entity.FileVaultId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FileVaultIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FileVaultIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FileVaultProvider.DeepLoad(transactionManager, entity.FileVaultIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FileVaultIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileVaultTable object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileVaultTable instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileVaultTable Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileVaultTable entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region FileVaultSubCategoryIdSource
			if (CanDeepSave(entity, "FileVaultSubCategory|FileVaultSubCategoryIdSource", deepSaveType, innerList) 
				&& entity.FileVaultSubCategoryIdSource != null)
			{
				DataRepository.FileVaultSubCategoryProvider.Save(transactionManager, entity.FileVaultSubCategoryIdSource);
				entity.FileVaultSubCategoryId = entity.FileVaultSubCategoryIdSource.FileVaultSubCategoryId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region FileVaultIdSource
			if (CanDeepSave(entity, "FileVault|FileVaultIdSource", deepSaveType, innerList) 
				&& entity.FileVaultIdSource != null)
			{
				DataRepository.FileVaultProvider.Save(transactionManager, entity.FileVaultIdSource);
				entity.FileVaultId = entity.FileVaultIdSource.FileVaultId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileVaultTableChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileVaultTable</c>
	///</summary>
	public enum FileVaultTableChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>FileVaultSubCategory</c> at FileVaultSubCategoryIdSource
		///</summary>
		[ChildEntityType(typeof(FileVaultSubCategory))]
		FileVaultSubCategory,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>FileVault</c> at FileVaultIdSource
		///</summary>
		[ChildEntityType(typeof(FileVault))]
		FileVault,
		}
	
	#endregion FileVaultTableChildEntityTypes
	
	#region FileVaultTableFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileVaultTableColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableFilterBuilder : SqlFilterBuilder<FileVaultTableColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableFilterBuilder class.
		/// </summary>
		public FileVaultTableFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableFilterBuilder
	
	#region FileVaultTableParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileVaultTableColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultTableParameterBuilder : ParameterizedSqlFilterBuilder<FileVaultTableColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableParameterBuilder class.
		/// </summary>
		public FileVaultTableParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileVaultTableParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileVaultTableParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileVaultTableParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileVaultTableParameterBuilder
	
	#region FileVaultTableSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileVaultTableColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultTable"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileVaultTableSortBuilder : SqlSortBuilder<FileVaultTableColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultTableSqlSortBuilder class.
		/// </summary>
		public FileVaultTableSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileVaultTableSortBuilder
	
} // end namespace
