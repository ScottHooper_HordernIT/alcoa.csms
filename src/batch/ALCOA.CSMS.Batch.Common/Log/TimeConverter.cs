﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Core;
using log4net.Layout.Pattern;

namespace ALCOA.CSMS.Batch.Common.Log
{
    public class TimeConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, LoggingEvent loggingEvent)
        {
            writer.Write(DateTime.Now.RemoveDST().ToString("dd/MM/yyyy HH:mm:ss"));
        }
    }
}
