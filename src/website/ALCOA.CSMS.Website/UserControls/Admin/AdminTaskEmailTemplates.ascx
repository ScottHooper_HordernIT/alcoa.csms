﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminTaskEmailTemplates.ascx.cs"
    Inherits="ALCOA.CSMS.Website.UserControls.Admin.AdminTaskEmailTemplates" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<style type="text/css">
    .style1
    {
        color: #FF0000;
    }
    .style2
    {
        width: 900px;
    }
    .style3
    {
        width: 47px;
        text-align: right;
    }
</style>
<script type="text/javascript">
    // <![CDATA[

    //to
    var textSeparator = ";";

    function UnselectAll_To() {
        clbTo.UnselectIndices([0]);
        ccbTo.SetText('');
    }

    function UpdateSelectAllItemState_To() {
        IsAllSelected_To() ? clbTo.SelectIndices([0]) : clbTo.UnselectIndices([0]);
    }
    function IsAllSelected_To() {
        for (var i = 1; i < clbTo.GetItemCount(); i++)
            if (!clbTo.GetItem(i).selected)
                return false;
        return true;
    }
    function UpdateText_To() {
        var selectedItems = clbTo.GetSelectedItems();
        ccbTo.SetText(GetSelectedItemsText(selectedItems));
    }
    function SynchronizeListBoxValues_To(dropDown, args) {
        clbTo.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts_To(texts);
        clbTo.SelectValues(values);
        UpdateSelectAllItemState_To();
        UpdateText_To(); // for remove non-existing texts
    }
    function GetValuesByTexts_To(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = clbTo.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    function OnListBoxSelectionChanged_To(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState_To();
        UpdateText_To();
    }

    //cc
    function UnselectAll_Cc() {
        clbCc.UnselectIndices([0]);
        ccbCc.SetText('');
    }

    function UpdateSelectAllItemState_Cc() {
        IsAllSelected_Cc() ? clbCc.SelectIndices([0]) : clbCc.UnselectIndices([0]);
    }
    function IsAllSelected_Cc() {
        for (var i = 1; i < clbCc.GetItemCount(); i++)
            if (!clbCc.GetItem(i).selected)
                return false;
        return true;
    }
    function UpdateText_Cc() {
        var selectedItems = clbCc.GetSelectedItems();
        ccbCc.SetText(GetSelectedItemsText(selectedItems));
    }
    function SynchronizeListBoxValues_Cc(dropDown, args) {
        clbCc.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts_Cc(texts);
        clbCc.SelectValues(values);
        UpdateSelectAllItemState_Cc();
        UpdateText_Cc(); // for remove non-existing texts
    }
    function GetValuesByTexts_Cc(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = clbCc.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    function OnListBoxSelectionChanged_Cc(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState_Cc();
        UpdateText_Cc();
    }

    //bcc
    function UnselectAll_Bcc() {
        clbBcc.UnselectIndices([0]);
        ccbBcc.SetText('');
    }

    function UpdateSelectAllItemState_Bcc() {
        IsAllSelected_Bcc() ? clbBcc.SelectIndices([0]) : clbBcc.UnselectIndices([0]);
    }
    function IsAllSelected_Bcc() {
        for (var i = 1; i < clbBcc.GetItemCount(); i++)
            if (!clbBcc.GetItem(i).selected)
                return false;
        return true;
    }
    function UpdateText_Bcc() {
        var selectedItems = clbBcc.GetSelectedItems();
        ccbBcc.SetText(GetSelectedItemsText(selectedItems));
    }
    function SynchronizeListBoxValues_Bcc(dropDown, args) {
        clbBcc.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts_Bcc(texts);
        clbBcc.SelectValues(values);
        UpdateSelectAllItemState_Bcc();
        UpdateText_Bcc(); // for remove non-existing texts
    }
    function GetValuesByTexts_Bcc(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = clbBcc.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    function OnListBoxSelectionChanged_Bcc(listBox, args) {
        if (args.index == 0)
            args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState_Bcc();
        UpdateText_Bcc();
    }

    function GetSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
            if (items[i].index != 0)
                texts.push(items[i].text);
        return texts.join(textSeparator);
    }
        
    // ]]>
</script>
<table>
    <tr>
        <td>
            <dx:ASPxComboBox ID="cbTemplate" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" DataSourceID="AdminTaskEmailTemplateListDataSource1"
                TextField="AdminTaskTypeName" ValueField="AdminTaskEmailTemplateId" Width="400px" IncrementalFilteringMode="Contains">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings Display="Dynamic" ValidationGroup="Load">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
        <td>
            <dx:ASPxButton ID="btnLoad" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                Text="Load" OnClick="btnLoad_Click" ValidationGroup="Load">
            </dx:ASPxButton>
        </td>
    </tr>
</table>
<asp:Panel ID="Panel1" runat="server">
    <table>
        <tr>
            <td>
                <div align="left" style="float: left;">
                    <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Save" OnClick="btnSave_Click">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
                <div align="left" style="float: left;">
                    <dx:ASPxButton ID="btnSaveClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Save & Close" OnClick="btnSaveAndClose_Click">
                    </dx:ASPxButton>
                </div>
            </td>
            <td>
                <div align="left" style="float: left;">
                    <dx:ASPxButton ID="btnClose" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Text="Close" OnClick="btnClose_Click">
                    </dx:ASPxButton>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <strong>Subject: </strong>
    <dx:ASPxTextBox ID="tbSubject" runat="server" Width="800px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    </dx:ASPxTextBox>
    <br />
   
    <strong>Recipients:</strong><table cellpadding="0" cellspacing="0" class="style2">
        <tr>
            <td class="style3">
                <dx:ASPxLabel ID="lblTo" Text="To:" runat="server"/>
            </td>
            <td>
                <dx:ASPxDropDownEdit ClientInstanceName="ccbTo" ID="ddeTo" SkinID="CheckComboBox"
                    Width="800px" runat="server" EnableAnimation="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <DropDownWindowStyle BackColor="#EDEDED" />
                    <DropDownWindowTemplate>
                        <dx:ASPxListBox Width="100%" ID="lbTo" ClientInstanceName="clbTo" SelectionMode="CheckColumn"
                            runat="server" SkinID="CheckComboBoxListBox"
                           >
                            <Border BorderStyle="None" />
                            <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                            <Items>
                            </Items>
                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged_To" />
                        </dx:ASPxListBox>
                        <table style="width: 100%" cellspacing="0" cellpadding="4">
                            <tr>
                                <td align="right">
                                    <dx:aspxbutton id="btnToClose" autopostback="False" runat="server" text="Close"
                                        cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
                                        spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                                        <ClientSideEvents Click="function(s, e){ ccbTo.HideDropDown(); }" />
                                                                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </DropDownWindowTemplate>
                    <ClientSideEvents TextChanged="SynchronizeListBoxValues_To" DropDown="SynchronizeListBoxValues_To" />
                </dx:ASPxDropDownEdit>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <dx:ASPxLabel ID="lblCC" Text="CC:" runat="server"/>
            </td>
            <td>
                <dx:ASPxDropDownEdit ClientInstanceName="ccbCc" ID="ddeCc" SkinID="CheckComboBox"
                    Width="800px" runat="server" EnableAnimation="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <DropDownWindowStyle BackColor="#EDEDED" />
                    <DropDownWindowTemplate>
                        <dx:ASPxListBox Width="100%" ID="lbCc" ClientInstanceName="clbCc" SelectionMode="CheckColumn"
                            runat="server" SkinID="CheckComboBoxListBox"
                           >
                            <Border BorderStyle="None" />
                            <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                            <Items>
                            </Items>
                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged_Cc" />
                        </dx:ASPxListBox>
                        <table style="width: 100%" cellspacing="0" cellpadding="4">
                            <tr>
                                <td align="right">
                                    <dx:aspxbutton id="btnCcClose" autopostback="False" runat="server" text="Close"
                                        cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
                                        spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                                        <ClientSideEvents Click="function(s, e){ ccbCc.HideDropDown(); }" />
                                                                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </DropDownWindowTemplate>
                    <ClientSideEvents TextChanged="SynchronizeListBoxValues_Cc" DropDown="SynchronizeListBoxValues_Cc" />
                </dx:ASPxDropDownEdit>
            </td>
        </tr>
        <tr>
            <td class="style3">
               <dx:ASPxLabel ID="lblBcc" Text="BCC:" runat="server"/>
            </td>
            <td>
                <dx:ASPxDropDownEdit ClientInstanceName="ccbBcc" ID="ddeBcc" SkinID="CheckComboBox"
                    Width="800px" runat="server" EnableAnimation="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <DropDownWindowStyle BackColor="#EDEDED" />
                    <DropDownWindowTemplate>
                        <dx:ASPxListBox Width="100%" ID="lbBcc" ClientInstanceName="clbBcc" SelectionMode="CheckColumn"
                            runat="server" SkinID="CheckComboBoxListBox"
                           >
                            <Border BorderStyle="None" />
                            <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                            <Items>
                            </Items>
                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged_Bcc" />
                        </dx:ASPxListBox>
                        <table style="width: 100%" cellspacing="0" cellpadding="4">
                            <tr>
                                <td align="right">
                                    <dx:aspxbutton id="btnBccClose" autopostback="False" runat="server" text="Close"
                                        cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" csspostfix="Office2003Blue"
                                        spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                                        <ClientSideEvents Click="function(s, e){ ccbBcc.HideDropDown(); }" />
                                                                                    </dx:aspxbutton>
                                </td>
                            </tr>
                        </table>
                    </DropDownWindowTemplate>
                    <ClientSideEvents TextChanged="SynchronizeListBoxValues_Bcc" DropDown="SynchronizeListBoxValues_Bcc" />
                </dx:ASPxDropDownEdit>
            </td>
        </tr>
    </table>
    <br />
    
&nbsp;<br />
    <br />
    <strong>Body:</strong>
    <br />
    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
        Width="900px">
        <TabPages>
            <dx:TabPage Text="Alcoa (Internal)">
                <ContentCollection>
                    <dx:ContentControl ID="ccBodyAlcoaLan" runat="server" SupportsDisabledAttribute="True">
                        <span class="style1"><dx:ASPxLabel ID="lblBodyAlcoaLan" runat="server" text="This is the email message which is shown to Internal Alcoan Requests." /></span><br />
                        <dx:ASPxHtmlEditor ID="heBodyAlcoaLan" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Height="500px" Visible="False" Width="876px">
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <ViewArea>
                                    <Border BorderColor="#002D96" />
                                </ViewArea>
                            </Styles>
                            <SettingsImageUpload>
                                <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                                </ValidationSettings>
                            </SettingsImageUpload>
                            <SettingsImageSelector>
                                <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                            </SettingsImageSelector>
                            <SettingsDocumentSelector>
                                <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                            </SettingsDocumentSelector>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <ImagesFileManager>
                                <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
                                </FolderContainerNodeLoadingPanel>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanel>
                            </ImagesFileManager>
                        </dx:ASPxHtmlEditor>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="AlcoaDirect (External) - New User">
                <ContentCollection>
                    <dx:ContentControl ID="ccBodyAlcoaDirectNewUser" runat="server" SupportsDisabledAttribute="True">
                        <span class="style1"><dx:ASPxLabel ID="lblBodyAlcoaDirectNewUser" runat="server" text="This is the email message which is shown to AlcoaDirect (External)
                            Requests for a new user." /></span><br />
                        <dx:ASPxHtmlEditor ID="heBodyAlcoaDirectNewUser" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Height="500px" Visible="False" Width="876px">
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <ViewArea>
                                    <Border BorderColor="#002D96" />
                                </ViewArea>
                            </Styles>
                            <SettingsImageUpload>
                                <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                                </ValidationSettings>
                            </SettingsImageUpload>
                            <SettingsImageSelector>
                                <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                            </SettingsImageSelector>
                            <SettingsDocumentSelector>
                                <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                            </SettingsDocumentSelector>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <ImagesFileManager>
                                <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
                                </FolderContainerNodeLoadingPanel>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanel>
                            </ImagesFileManager>
                        </dx:ASPxHtmlEditor>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="AlcoaDirect (External) - Existing User">
                <ContentCollection>
                    <dx:ContentControl ID="ccBodyAlcoaDirectExistingUser" runat="server" SupportsDisabledAttribute="True">
                        <span class="style1"><dx:ASPxLabel runat="server" ID="lblBodyAlcoaDirectExistingUser" Text="This is the email message which is shown to AlcoaDirect (External)
                            Requests for a existing user." /></span><br />
                        <dx:ASPxHtmlEditor ID="heBodyAlcoaDirectExistingUser" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Height="500px" Visible="False" Width="876px">
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <ViewArea>
                                    <Border BorderColor="#002D96" />
                                </ViewArea>
                            </Styles>
                            <SettingsImageUpload>
                                <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                                </ValidationSettings>
                            </SettingsImageUpload>
                            <SettingsImageSelector>
                                <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                            </SettingsImageSelector>
                            <SettingsDocumentSelector>
                                <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                            </SettingsDocumentSelector>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/HtmlEditor/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <ImagesFileManager>
                                <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
                                </FolderContainerNodeLoadingPanel>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanel>
                            </ImagesFileManager>
                        </dx:ASPxHtmlEditor>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            
        </TabPages>
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <LoadingPanelStyle ImageSpacing="6px">
        </LoadingPanelStyle>
        <ContentStyle>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </ContentStyle>
    </dx:ASPxPageControl>
    <br />
    <br />
    <strong>Attachments:</strong><br />
    <dx:ASPxPageControl ID="ASPxPageControl2" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
        Width="900px">
        <TabPages>
            <dx:TabPage Text="Alcoa (Internal)">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                        <strong>Attachments Available</strong> (Select files to attach by clicking the checkbox
                        next to a File):<dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            DataSourceID="CsmsFileDataSource1" KeyFieldName="CsmsFileId" Width="870px" OnDataBound="grid_DataBound">
                            <Columns>
                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" ShowSelectCheckbox="True"
                                    VisibleIndex="0">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="CsmsFileId" ShowInCustomizationForm="False"
                                    Visible="False">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataComboBoxColumn Caption="File Type" FieldName="CsmsFileTypeId" ShowInCustomizationForm="True"
                                    VisibleIndex="1">
                                    <PropertiesComboBox DataSourceID="CsmsFileTypeDataSource1" TextField="CsmsFileTypeName"
                                        ValueField="CsmsFileTypeId" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                </dx:GridViewDataComboBoxColumn>
                                <dx:GridViewDataTextColumn FieldName="FileName" ShowInCustomizationForm="True" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Description" ShowInCustomizationForm="True"
                                    VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager>
                                <AllButton Visible="True">
                                </AllButton>
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="AlcoaDirect (External)">
                <ContentCollection>
                    <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                        <strong>Attachments Available</strong> (Select files to attach by clicking the checkbox
                        next to a File):<dx:ASPxGridView ID="grid2" runat="server" AutoGenerateColumns="False"
                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                            DataSourceID="CsmsFileDataSource1" KeyFieldName="CsmsFileId" Width="870px" OnDataBound="grid2_DataBound">
                            <Columns>
                                <dx:GridViewCommandColumn ShowInCustomizationForm="True" ShowSelectCheckbox="True"
                                    VisibleIndex="0">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn FieldName="CsmsFileId" ShowInCustomizationForm="False"
                                    Visible="False">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataComboBoxColumn Caption="File Type" FieldName="CsmsFileTypeId" ShowInCustomizationForm="True"
                                    VisibleIndex="1">
                                    <PropertiesComboBox DataSourceID="CsmsFileTypeDataSource1" TextField="CsmsFileTypeName"
                                        ValueField="CsmsFileTypeId" ValueType="System.Int32">
                                    </PropertiesComboBox>
                                </dx:GridViewDataComboBoxColumn>
                                <dx:GridViewDataTextColumn FieldName="FileName" ShowInCustomizationForm="True" VisibleIndex="2">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="Description" ShowInCustomizationForm="True"
                                    VisibleIndex="3">
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager>
                                <AllButton Visible="True">
                                </AllButton>
                            </SettingsPager>
                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                </LoadingPanelOnStatusBar>
                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                </LoadingPanel>
                            </Images>
                            <ImagesFilterControl>
                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                </LoadingPanel>
                            </ImagesFilterControl>
                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                </Header>
                                <LoadingPanel ImageSpacing="10px">
                                </LoadingPanel>
                            </Styles>
                            <StylesEditors>
                                <ProgressBar Height="25px">
                                </ProgressBar>
                            </StylesEditors>
                        </dx:ASPxGridView>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </LoadingPanelImage>
        <LoadingPanelStyle ImageSpacing="6px">
        </LoadingPanelStyle>
        <ContentStyle>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </ContentStyle>
    </dx:ASPxPageControl>
</asp:Panel>
<data:AdminTaskEmailRecipientDataSource ID="AdminTaskEmailRecipientDataSource1" runat="server">
</data:AdminTaskEmailRecipientDataSource>
<data:CsmsFileDataSource ID="CsmsFileDataSource1" runat="server" EnableCaching="false">
</data:CsmsFileDataSource>
<data:CsmsFileTypeDataSource ID="CsmsFileTypeDataSource1" runat="server">
</data:CsmsFileTypeDataSource>
<data:AdminTaskEmailTemplateListDataSource ID="AdminTaskEmailTemplateListDataSource1"
    runat="server">
</data:AdminTaskEmailTemplateListDataSource>
