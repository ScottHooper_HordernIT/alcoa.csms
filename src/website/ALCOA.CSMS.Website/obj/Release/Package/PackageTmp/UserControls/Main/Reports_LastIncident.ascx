<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Reports_LastIncident" Codebehind="Reports_LastIncident.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" style="text-align: left;">
                        <span class="title">Reports</span><br />
                            <span class="date">Safety > Injury Free Days Report</span><br />
                            <img height="1" src="images/grfc_dottedline.gif" width="24" />
            </td>
    </tr>
    </table>
    <br />
<dxwgv:aspxgridview id="grid" runat="server" autogeneratecolumns="False" ClientInstanceName="grid"
    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    csspostfix="Office2003Blue" Width="900px"
    >
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
<Styles CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
    </LoadingPanelOnStatusBar>
<CollapsedButton Width="11px"></CollapsedButton>

<ExpandedButton Width="11px"></ExpandedButton>

<DetailCollapsedButton Width="11px"></DetailCollapsedButton>

<DetailExpandedButton Width="11px"></DetailExpandedButton>

<FilterRowButton Width="13px"></FilterRowButton>
    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"> <%--//Change by Debashis--%>
    </LoadingPanel>
</Images>


<Columns>
<dxwgv:GridViewDataTextColumn FieldName="CompanyName" SortIndex="0" VisibleIndex="0"></dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="SiteName" Caption="Site Name" SortIndex="1" VisibleIndex="1"></dxwgv:GridViewDataTextColumn>
    <dxwgv:GridViewDataDateColumn Caption="Last Incident Date" FieldName="LastIncidentDate"
        SortIndex="2" VisibleIndex="2" Width="200px">
        <PropertiesDateEdit DisplayFormatString="dd-MMM-yy">
        </PropertiesDateEdit>
    </dxwgv:GridViewDataDateColumn>
<dxwgv:GridViewDataTextColumn FieldName="InjuryFreeDays" Caption="Injury Free Days" VisibleIndex="3" Width="200px">
</dxwgv:GridViewDataTextColumn>
</Columns>
<Settings ShowHeaderFilterButton="True" ShowGroupPanel="True" ShowPreview="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="215"></Settings>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <ClientSideEvents ContextMenu="function(s, e) {
                if (expanded == true)
                {
                    grid.CollapseAll();
                    expanded = false;
                }
                else
                {
	                grid.ExpandAll();
	                expanded = true;
	            }
}" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:aspxgridview>
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
    </tr>
</table>
<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True" Sort="Ordinal ASC"></data:RegionsDataSource>