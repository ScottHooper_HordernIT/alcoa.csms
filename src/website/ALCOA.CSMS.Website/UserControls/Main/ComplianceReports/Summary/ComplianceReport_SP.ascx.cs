using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.IO;
using System.Collections.Generic;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_SP : System.Web.UI.UserControl
{
    Auth auth = new Auth();
  
    int expected = 1;
    int green = 0;
    //private bool dataLoaded = false;

  
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time loa
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    break;
                default:
                    // do something
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }
        }
        //Adding Session to get values for PDF
        Session["ComplianceReportSP"] = null;
            if (SessionHandler.spVar_Page == "ComplianceReport")
            {
                grid.Settings.ShowFilterRow = false;
                grid.Settings.ShowGroupPanel = false;
                grid.SettingsBehavior.AllowSort = false;
                grid.SettingsBehavior.AllowGroup = false;
                //residentialstatus = SessionHandler.spVar_ResidentialStatus2;

                grid.DataSourceID = null;

                DataSet ds = new DataSet();
                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                {
                    
                    ds = DataRepository.FileDbProvider.ComplianceReport_ByCompany(
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Convert.ToInt32(SessionHandler.spVar_SiteId)
                            );
                    grid.Columns["Company Name"].Visible = false;
                }
                else
                {
                    ds = DataRepository.FileDbProvider.ComplianceReport(
                            Convert.ToInt32(SessionHandler.spVar_Year));
                    grid.Columns["Company Name"].Visible = true;
                }

                grid.DataSource = ds.Tables[0];

                //Adding Session to get values for PDF
                Session["ComplianceReportSP"] = ds.Tables[0];

                grid.DataBind();

                ds.Dispose();

               DataSet dsSite = DataRepository.FileDbProvider.ComplianceReport_ByCompany_Site(
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                          Convert.ToInt32(SessionHandler.spVar_Year),
                          Convert.ToInt32(SessionHandler.spVar_SiteId));
                //need to add  Convert.ToInt32(SessionHandler.spVar_SiteId) by Sayani
               string CompSafetyCat = "";
               if (dsSite.Tables[0].Rows.Count > 0)
               {
                    CompSafetyCat = dsSite.Tables[0].Rows[0][0].ToString();
               }
                if (CompSafetyCat == "Embedded" || CompSafetyCat == "Non-Embedded 1")
                {
                    grid.Visible = true;
                    Caption.Visible = true;
                    this.Parent.FindControl("SafetyPlan").Visible = true;
                }
                else 
                {
                    grid.Visible = false;
                    Caption.Visible = false;
                    this.Parent.FindControl("SafetyPlan").Visible = false;
                }

                if (green >= expected)
                {
                    lblGood.Text = "1";
                    int a = Convert.ToInt32(SessionHandler.spVar_ComplianceCount);
                    a++;
                    SessionHandler.spVar_ComplianceCount = a.ToString();
                }
            }

            //Added By Sayani for task DT2416

            else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
            {
                grid.Settings.ShowFilterRow = true;
                grid.Settings.ShowGroupPanel = false;
                grid.SettingsBehavior.AllowSort = true;
                grid.SettingsBehavior.AllowGroup = false;
                
               // grid.Columns["Site Name"].Visible = true;
                //residentialstatus = SessionHandler.spVar_ResidentialStatus2;

                grid.DataSourceID = null;

                DataSet ds = new DataSet();

                if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                {

                    ds = DataRepository.FileDbProvider.ComplianceReport_BySite(
                                Convert.ToInt32(SessionHandler.spVar_Year),
                                Convert.ToInt32(SessionHandler.spVar_SiteId),
                                SessionHandler.spVar_CategoryId2);

                    //grid.Columns["Site Name"].Visible = false;
                }
                else //Region
                {
                    ds = DataRepository.FileDbProvider.ComplianceReport_BySite(
                                            Convert.ToInt32(SessionHandler.spVar_Year),
                                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                                            SessionHandler.spVar_CategoryId2);
                  //  grid.Columns["Site Name"].Visible = true;
                }

               

                grid.Columns["Company Name"].Visible = true;
               

                grid.DataSource = ds.Tables[0];

                //Adding Session to get values for PDF
                Session["ComplianceReportSP"] = ds.Tables[0];

                grid.DataBind();

                ds.Dispose();
                
                //if (green >= expected)
                //{
                //    lblGood.Text = "1";
                //    int a = Convert.ToInt32(SessionHandler.spVar_ComplianceCount);
                //    a++;
                //    SessionHandler.spVar_ComplianceCount = a.ToString();
                //}
            }
        
    }

    public int getExpected()
    {
        if (green >= expected)
        { return 1; }
        else { return 0; }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noSPSubmitted") as Label;
        int change = (int)grid.GetRowValues(e.VisibleIndex, "Submitted");

        string status;
        if (grid.GetRowValues(e.VisibleIndex, "StatusName") != DBNull.Value)
            status = (string)grid.GetRowValues(e.VisibleIndex, "StatusName");
        else
            status = "";


        if (label != null) label.Text = "";
        System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage")
                                    as System.Web.UI.WebControls.Image;
        if (img != null) img.Visible = false;
        if (change != 0)
        {
            if (img != null) img.Visible = true;

            if (label != null) label.ForeColor = Color.Green;
            if (label != null) label.Text = String.Format("({0})", change);

            switch (status)
            {
                case "Being Assessed":
                    green++;
                    if (img != null) img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                    break;
                case "Not Approved":
                    if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                    break;
                case "Approved":
                    green++;
                    if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                    break;
                default:
                    green++;
                    if (img != null) img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                    break;
            }
        }
        else
        {
            if (img != null) img.Visible = true;
            if (img != null) img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
            if (label != null) label.ForeColor = Color.Red;
        }

    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        //if (e.Column.FieldName == "CompanyId")
        //{
        //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
        //    //comboBox.Items.Clear();
        //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); }";
        //}
        //if (e.Column.FieldName == "StatusName")
        //{
        //    ASPxComboBox comboBox = e.Editor as ASPxComboBox;
        //    //comboBox.Items.Clear();
        //    comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); }";
        //}

        if (e.Column.FieldName == "CompanyID" || e.Column.FieldName == "StatusName" || e.Column.FieldName == "CategoryId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', '');}";
        }
    }

    public PrintableComponentLink ExportToPdf()
    {
        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
        pcl.Component = gridExport;
        pcl.CreateReportHeaderArea += new CreateAreaEventHandler(pcl_CreateReportHeaderArea);
        pcl.CreateDocument(false);
        return pcl;
    }

    public void pcl_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
    {
        TextBrick brick1 = e.Graph.DrawString("Safety Management Plans", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }

    public string ExportToHtml()
    {
        var sb = new StringBuilder();

        sb.Append("<table width='100%' border='1' style='font-size:9px; color:Black;'>");
        sb.Append("<tbody>");
        sb.Append("<tr>");
        sb.Append("<td width='35%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Company Name</td>");
        sb.Append("<td width='35%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Residential Category</td>"); //Changed by AG from Safety Compliance Category
        sb.Append("<td width='10%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Submitted</td>");
        sb.Append("<td width='20%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Last Plan Current Status</td>");
        sb.Append("</tr>");

        var dt = (DataTable)grid.DataSource;
        if (dt != null && dt.Rows.Count > 0)
        {
            dt.DefaultView.Sort = "CategoryDesc asc, CompanyName asc";
            dt = dt.DefaultView.ToTable();

            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<tr>");
                sb.Append(string.Format("<td width='35%' border='1' align='left' valign='top'>{0}</td>", dr["CompanyName"].ToString()));
                sb.Append(string.Format("<td width='35%' border='1' align='left' valign='top'>{0}</td>", dr["CategoryDesc"]));
                sb.Append(string.Format("<td width='10%' border='1' align='center' valign='top'>{0}</td>", SubmittedHtml(dr["Submitted"].ToString(), dr["StatusName"].ToString())));
                sb.Append(string.Format("<td width='20%' border='1' align='left' valign='top'>{0}</td>", dr["StatusName"].ToString()));
            }
        }
        else
        {
            sb.Append("<tr>");
            sb.Append("<td colspan='4'>No Safety Plan(s) submitted</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");

        return sb.ToString();
    }

    private string SubmittedHtml(string submitted, string status)
    {
        string html = "";

        if (submitted != "0")
        {
            switch (status)
            {
                case "Being Assessed":
                    html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/trafficlights/tlGreen.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", submitted);
                    break;
                case "Not Approved":
                    html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", submitted);
                    break;
                case "Approved":
                    html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", submitted);
                    break;
                default:
                    html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/trafficlights/tlGreen.gif' align='right valign='middle'' /></td><td width='50%' align='left'><span style='color:#00ff00;' valign='middle'>({0})</span></td></tr></table>", submitted);
                    break;
            }
        }
        else
        {
            html = string.Format("<table width='100%' border='0'><tr><td width='50%' align='right'><img width='16px' height='16px' src='[UrlPath]/images/trafficlights/tlRed.gif' align='right' valign='middle' /></td><td width='50%' align='left'><span style='color:#ff0000;' valign='middle'>({0})</span></td></tr></table>", submitted);
        }

        return html;
    }
}
