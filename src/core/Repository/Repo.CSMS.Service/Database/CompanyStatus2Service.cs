﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyStatus2Service
    {
        CompanyStatus2 Get(Expression<Func<CompanyStatus2, bool>> where, List<Expression<Func<CompanyStatus2, object>>> includes);
        List<CompanyStatus2> GetMany(int? top, Expression<Func<CompanyStatus2, bool>> where, Func<IQueryable<CompanyStatus2>, IOrderedQueryable<CompanyStatus2>> orderBy, List<Expression<Func<CompanyStatus2, object>>> includes);
        List<CompanyStatus2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyStatus2, bool>> where, Func<IQueryable<CompanyStatus2>, IOrderedQueryable<CompanyStatus2>> orderBy, List<Expression<Func<CompanyStatus2, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyStatus2, bool>> where, List<Expression<Func<CompanyStatus2, object>>> includes);
        void Insert(CompanyStatus2 item, bool saveChanges = true);
        void Update(CompanyStatus2 item, bool saveChanges = true);
        void Delete(CompanyStatus2 item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyStatus2Service : ICompanyStatus2Service
    {
        private readonly IRepository<CompanyStatus2> _repository;

        public CompanyStatus2Service(IRepository<CompanyStatus2> repository)
        {
            this._repository = repository;
        }

        public CompanyStatus2 Get(Expression<Func<CompanyStatus2, bool>> where, List<Expression<Func<CompanyStatus2, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyStatus2> GetMany(int? top, Expression<Func<CompanyStatus2, bool>> where, Func<IQueryable<CompanyStatus2>, IOrderedQueryable<CompanyStatus2>> orderBy, List<Expression<Func<CompanyStatus2, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyStatus2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyStatus2, bool>> where, Func<IQueryable<CompanyStatus2>, IOrderedQueryable<CompanyStatus2>> orderBy, List<Expression<Func<CompanyStatus2, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyStatus2, bool>> where, List<Expression<Func<CompanyStatus2, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyStatus2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatus2");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyStatus2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatus2");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyStatus2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatus2");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
