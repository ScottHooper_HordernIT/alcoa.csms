﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SystemLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SystemLogProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.SystemLog, KaiZen.CSMS.Entities.SystemLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SystemLogKey key)
		{
			return Delete(transactionManager, key.SystemLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_systemLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _systemLogId)
		{
			return Delete(null, _systemLogId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_systemLogId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _systemLogId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.SystemLog Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SystemLogKey key, int start, int pageLength)
		{
			return GetBySystemLogId(transactionManager, key.SystemLogId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SystemLog index.
		/// </summary>
		/// <param name="_systemLogId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SystemLog"/> class.</returns>
		public KaiZen.CSMS.Entities.SystemLog GetBySystemLogId(System.Int32 _systemLogId)
		{
			int count = -1;
			return GetBySystemLogId(null,_systemLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SystemLog index.
		/// </summary>
		/// <param name="_systemLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SystemLog"/> class.</returns>
		public KaiZen.CSMS.Entities.SystemLog GetBySystemLogId(System.Int32 _systemLogId, int start, int pageLength)
		{
			int count = -1;
			return GetBySystemLogId(null, _systemLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SystemLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_systemLogId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SystemLog"/> class.</returns>
		public KaiZen.CSMS.Entities.SystemLog GetBySystemLogId(TransactionManager transactionManager, System.Int32 _systemLogId)
		{
			int count = -1;
			return GetBySystemLogId(transactionManager, _systemLogId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SystemLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_systemLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SystemLog"/> class.</returns>
		public KaiZen.CSMS.Entities.SystemLog GetBySystemLogId(TransactionManager transactionManager, System.Int32 _systemLogId, int start, int pageLength)
		{
			int count = -1;
			return GetBySystemLogId(transactionManager, _systemLogId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SystemLog index.
		/// </summary>
		/// <param name="_systemLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SystemLog"/> class.</returns>
		public KaiZen.CSMS.Entities.SystemLog GetBySystemLogId(System.Int32 _systemLogId, int start, int pageLength, out int count)
		{
			return GetBySystemLogId(null, _systemLogId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SystemLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_systemLogId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SystemLog"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SystemLog GetBySystemLogId(TransactionManager transactionManager, System.Int32 _systemLogId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SystemLog_Activity index.
		/// </summary>
		/// <param name="_activity"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByActivity(System.String _activity)
		{
			int count = -1;
			return GetByActivity(null,_activity, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Activity index.
		/// </summary>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByActivity(System.String _activity, int start, int pageLength)
		{
			int count = -1;
			return GetByActivity(null, _activity, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Activity index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activity"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByActivity(TransactionManager transactionManager, System.String _activity)
		{
			int count = -1;
			return GetByActivity(transactionManager, _activity, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Activity index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByActivity(TransactionManager transactionManager, System.String _activity, int start, int pageLength)
		{
			int count = -1;
			return GetByActivity(transactionManager, _activity, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Activity index.
		/// </summary>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByActivity(System.String _activity, int start, int pageLength, out int count)
		{
			return GetByActivity(null, _activity, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Activity index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public abstract TList<SystemLog> GetByActivity(TransactionManager transactionManager, System.String _activity, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SystemLog_Date_Activity index.
		/// </summary>
		/// <param name="_date"></param>
		/// <param name="_activity"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByDateActivity(System.DateTime _date, System.String _activity)
		{
			int count = -1;
			return GetByDateActivity(null,_date, _activity, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Date_Activity index.
		/// </summary>
		/// <param name="_date"></param>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByDateActivity(System.DateTime _date, System.String _activity, int start, int pageLength)
		{
			int count = -1;
			return GetByDateActivity(null, _date, _activity, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Date_Activity index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_date"></param>
		/// <param name="_activity"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByDateActivity(TransactionManager transactionManager, System.DateTime _date, System.String _activity)
		{
			int count = -1;
			return GetByDateActivity(transactionManager, _date, _activity, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Date_Activity index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_date"></param>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByDateActivity(TransactionManager transactionManager, System.DateTime _date, System.String _activity, int start, int pageLength)
		{
			int count = -1;
			return GetByDateActivity(transactionManager, _date, _activity, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Date_Activity index.
		/// </summary>
		/// <param name="_date"></param>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public TList<SystemLog> GetByDateActivity(System.DateTime _date, System.String _activity, int start, int pageLength, out int count)
		{
			return GetByDateActivity(null, _date, _activity, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SystemLog_Date_Activity index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_date"></param>
		/// <param name="_activity"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SystemLog&gt;"/> class.</returns>
		public abstract TList<SystemLog> GetByDateActivity(TransactionManager transactionManager, System.DateTime _date, System.String _activity, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SystemLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SystemLog&gt;"/></returns>
		public static TList<SystemLog> Fill(IDataReader reader, TList<SystemLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.SystemLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SystemLog")
					.Append("|").Append((System.Int32)reader[((int)SystemLogColumn.SystemLogId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SystemLog>(
					key.ToString(), // EntityTrackingKey
					"SystemLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.SystemLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SystemLogId = (System.Int32)reader[((int)SystemLogColumn.SystemLogId - 1)];
					c.DateTime = (System.DateTime)reader[((int)SystemLogColumn.DateTime - 1)];
					c.Date = (System.DateTime)reader[((int)SystemLogColumn.Date - 1)];
					c.Activity = (System.String)reader[((int)SystemLogColumn.Activity - 1)];
					c.Success = (System.Boolean)reader[((int)SystemLogColumn.Success - 1)];
					c.Notes = (reader.IsDBNull(((int)SystemLogColumn.Notes - 1)))?null:(System.String)reader[((int)SystemLogColumn.Notes - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SystemLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SystemLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.SystemLog entity)
		{
			if (!reader.Read()) return;
			
			entity.SystemLogId = (System.Int32)reader[((int)SystemLogColumn.SystemLogId - 1)];
			entity.DateTime = (System.DateTime)reader[((int)SystemLogColumn.DateTime - 1)];
			entity.Date = (System.DateTime)reader[((int)SystemLogColumn.Date - 1)];
			entity.Activity = (System.String)reader[((int)SystemLogColumn.Activity - 1)];
			entity.Success = (System.Boolean)reader[((int)SystemLogColumn.Success - 1)];
			entity.Notes = (reader.IsDBNull(((int)SystemLogColumn.Notes - 1)))?null:(System.String)reader[((int)SystemLogColumn.Notes - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SystemLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SystemLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.SystemLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SystemLogId = (System.Int32)dataRow["SystemLogId"];
			entity.DateTime = (System.DateTime)dataRow["DateTime"];
			entity.Date = (System.DateTime)dataRow["Date"];
			entity.Activity = (System.String)dataRow["Activity"];
			entity.Success = (System.Boolean)dataRow["Success"];
			entity.Notes = Convert.IsDBNull(dataRow["Notes"]) ? null : (System.String)dataRow["Notes"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SystemLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SystemLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.SystemLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SystemLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.SystemLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SystemLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.SystemLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SystemLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.SystemLog</c>
	///</summary>
	public enum SystemLogChildEntityTypes
	{
	}
	
	#endregion SystemLogChildEntityTypes
	
	#region SystemLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SystemLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogFilterBuilder : SqlFilterBuilder<SystemLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogFilterBuilder class.
		/// </summary>
		public SystemLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SystemLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SystemLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SystemLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SystemLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SystemLogFilterBuilder
	
	#region SystemLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SystemLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogParameterBuilder : ParameterizedSqlFilterBuilder<SystemLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogParameterBuilder class.
		/// </summary>
		public SystemLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SystemLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SystemLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SystemLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SystemLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SystemLogParameterBuilder
	
	#region SystemLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SystemLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SystemLogSortBuilder : SqlSortBuilder<SystemLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogSqlSortBuilder class.
		/// </summary>
		public SystemLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SystemLogSortBuilder
	
} // end namespace
