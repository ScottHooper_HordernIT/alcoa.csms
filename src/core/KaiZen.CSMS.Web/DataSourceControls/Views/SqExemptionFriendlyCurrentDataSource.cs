﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SqExemptionFriendlyCurrentProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(SqExemptionFriendlyCurrentDataSourceDesigner))]
	public class SqExemptionFriendlyCurrentDataSource : ReadOnlyDataSource<SqExemptionFriendlyCurrent>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentDataSource class.
		/// </summary>
		public SqExemptionFriendlyCurrentDataSource() : base(new SqExemptionFriendlyCurrentService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SqExemptionFriendlyCurrentDataSourceView used by the SqExemptionFriendlyCurrentDataSource.
		/// </summary>
		protected SqExemptionFriendlyCurrentDataSourceView SqExemptionFriendlyCurrentView
		{
			get { return ( View as SqExemptionFriendlyCurrentDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SqExemptionFriendlyCurrentDataSourceView class that is to be
		/// used by the SqExemptionFriendlyCurrentDataSource.
		/// </summary>
		/// <returns>An instance of the SqExemptionFriendlyCurrentDataSourceView class.</returns>
		protected override BaseDataSourceView<SqExemptionFriendlyCurrent, Object> GetNewDataSourceView()
		{
			return new SqExemptionFriendlyCurrentDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SqExemptionFriendlyCurrentDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SqExemptionFriendlyCurrentDataSourceView : ReadOnlyDataSourceView<SqExemptionFriendlyCurrent>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SqExemptionFriendlyCurrentDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SqExemptionFriendlyCurrentDataSourceView(SqExemptionFriendlyCurrentDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SqExemptionFriendlyCurrentDataSource SqExemptionFriendlyCurrentOwner
		{
			get { return Owner as SqExemptionFriendlyCurrentDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SqExemptionFriendlyCurrentService SqExemptionFriendlyCurrentProvider
		{
			get { return Provider as SqExemptionFriendlyCurrentService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region SqExemptionFriendlyCurrentDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SqExemptionFriendlyCurrentDataSource class.
	/// </summary>
	public class SqExemptionFriendlyCurrentDataSourceDesigner : ReadOnlyDataSourceDesigner<SqExemptionFriendlyCurrent>
	{
	}

	#endregion SqExemptionFriendlyCurrentDataSourceDesigner

	#region SqExemptionFriendlyCurrentFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyCurrentFilter : SqlFilter<SqExemptionFriendlyCurrentColumn>
	{
	}

	#endregion SqExemptionFriendlyCurrentFilter

	#region SqExemptionFriendlyCurrentExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemptionFriendlyCurrent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFriendlyCurrentExpressionBuilder : SqlExpressionBuilder<SqExemptionFriendlyCurrentColumn>
	{
	}
	
	#endregion SqExemptionFriendlyCurrentExpressionBuilder		
}

