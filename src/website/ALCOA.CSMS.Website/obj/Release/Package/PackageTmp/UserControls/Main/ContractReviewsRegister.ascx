﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractReviewsRegister.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContractReviewsRegister" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>

    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

    <table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
        <span class="bodycopy"><span class="title">Contract Reviews Register</span><br />
                <span class="date"></span><br />
            </span></td>
    </tr>

    <tr>
       <td style="height: 28px; padding-top:10px; width: 1170px;">
          <table style="width: 522px">
            <tr>
                <td style="width: 15px; text-align: right">
                    <strong>Year:&nbsp;</strong>
                </td>
                <td style="width: 22px; text-align: left">
                    <dxe:ASPxComboBox id="cmbYear" runat="server"
                     autopostback="True" clientinstancename="cmbYear"
                     cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                     csspostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"                     
                     valuetype="System.String" width="60px" 
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings Display="Dynamic">
                        </ValidationSettings>
                    </dxe:ASPxComboBox>
                </td>
                <td style="width: 485px; text-align: left">
                    <dxe:ASPxButton ID="btnRefresh" runat="server" Text="Go/Refresh"  onclick="btnRefresh_Click" 
                    cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            csspostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        ForeColor="Navy"></dxe:ASPxButton>
        </td>
        </tr>
        
        </table>
        </td>
        </tr>
        <tr>
            <td style="padding-top:10px; width:1170px;">
                <dxwgv:ASPxGridView Id="grid" Width="100%"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False" OnHtmlRowCreated="grid_RowCreated"
                  OnRowUpdating="grid_RowUpdating" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                  KeyFieldName="CompanySiteCategoryStandardId">
                <Settings ShowFilterRow="true" ShowFilterBar="Visible" ShowGroupPanel="True" />
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
              
                 
                 

                    <Columns>

                   
                    
                   

                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Caption="Action">
                        <EditButton Visible="true"></EditButton>
                    </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="1" Caption="Company Name" FieldName="CompanyId" Name="CompanyBox" SortIndex="2" SortOrder="Ascending">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True"/>
                        <Settings SortMode="DisplayText"/>
                    </dxwgv:GridViewDataComboBoxColumn>

                        
                    <dxwgv:GridViewDataComboBoxColumn ReadOnly="true" VisibleIndex="2" Caption="Site Name" FieldName="SiteId" Name="SiteBox">
                        <PropertiesComboBox DataSourceID="SitesDataSource2" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith" DropDownButton-Enabled="true" >
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                   
                    <dx:GridViewBandColumn Caption="Month">
                    <HeaderStyle HorizontalAlign="Center" />
                    <Columns>
                    <dxwgv:GridViewDataCheckColumn FieldName="January" Caption="Jan" VisibleIndex="3">
                        <DataItemTemplate>
                     <asp:Image ID="imgJan" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                    
                        </DataItemTemplate>                      
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />                        
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="February" Caption="Feb" VisibleIndex="4">
                        <DataItemTemplate>
                     <asp:Image ID="imgFeb" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="March" Caption="Mar" VisibleIndex="5">
                        <DataItemTemplate>
                     <asp:Image ID="imgMar" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                    
                        </DataItemTemplate>

                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="April" Caption="Apr" VisibleIndex="6">
                        <DataItemTemplate>
                     <asp:Image ID="imgApr" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>

                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="May" Caption="May" VisibleIndex="7">
                        <DataItemTemplate>
                     <asp:Image ID="imgMay" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                       

                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="June" Caption="Jun" VisibleIndex="8">
                        <DataItemTemplate>
                     <asp:Image ID="imgJun" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>
                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="July" Caption="Jul" VisibleIndex="9">
                        <DataItemTemplate>
                     <asp:Image ID="imgJul" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="August" Caption="Aug" VisibleIndex="10">
                        <DataItemTemplate>
                     <asp:Image ID="imgAug" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="September" Caption="Sep" VisibleIndex="11">
                        <DataItemTemplate>
                     <asp:Image ID="imgSep" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                       
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="October" Caption="Oct" VisibleIndex="12">
                        <DataItemTemplate>
                     <asp:Image ID="imgOct" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                       
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="November" Caption="Nov" VisibleIndex="13">
                  
                        <DataItemTemplate>
                     <asp:Image ID="imgNov" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>                        
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    <dxwgv:GridViewDataCheckColumn FieldName="December" Caption="Dec" VisibleIndex="14">
                        <DataItemTemplate>
                     <asp:Image ID="imgDec" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" />
                     
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                    </dxwgv:GridViewDataCheckColumn>

                    </Columns>


                    </dx:GridViewBandColumn>

                    <dxwgv:GridViewDataTextColumn FieldName="Year" Name="Year" Caption="Year" VisibleIndex="15">
                        <DataItemTemplate>
                     
                     <asp:Label ID="lblYear" runat="server" Text="" ></asp:Label>
                        </DataItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings AllowHeaderFilter="False" />
                        <EditFormSettings Visible="false" />
                    </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataComboBoxColumn Caption="Sponsor" FieldName="LocationSponsorUserId" 
                        VisibleIndex="16">
                        <PropertiesComboBox DropDownButton-Visible="false" DataSourceID="UsersDataSource1" DropDownHeight="150px" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="false" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="SPA(s)" FieldName="LocationSpaUserId" 
                        VisibleIndex="17">
                        <PropertiesComboBox DropDownButton-Visible="false" DataSourceID="UsersDataSource1" DropDownHeight="150px" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="false" />
                        <Settings SortMode="DisplayText" />
                         
                    </dxwgv:GridViewDataComboBoxColumn>                   

                    </Columns>
                    



                  </dxwgv:ASPxGridView>
            </td>
        </tr>
        

    </table>
 

 
                
 <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>


<asp:SqlDataSource ID="sqlSitesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Sites_GetBySiteId" SelectCommandType="StoredProcedure">
    
</asp:SqlDataSource>

<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource>    
               
   <data:UsersFullNameDataSource ID="UsersDataSource1" runat="server"></data:UsersFullNameDataSource>

   <data:UsersFullNameDataSource ID="UsersFullNameDataSource1" runat="server"></data:UsersFullNameDataSource>

   
<data:CompanySiteCategoryStandardDataSource ID="companysitecategoryData1" SelectMethod="GetPaged"
            EnablePaging="True" EnableSorting="True" runat="server">
</data:CompanySiteCategoryStandardDataSource> 
<asp:SqlDataSource ID="SitesDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT TOP 1 '99' AS SiteId,'ARP' AS SiteName FROM dbo.Sites
UNION
SELECT SiteId, SiteName FROM dbo.Sites
ORDER BY SiteName
">
</asp:SqlDataSource>

