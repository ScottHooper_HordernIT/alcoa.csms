using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using System.Text;

public partial class UserControls_Reports_AuditSection8_12 : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
        {
            //yeh... lol
        }
        else
        {
            Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        string html = "";
        ASCIIEncoding encoding = new ASCIIEncoding();
        TemplateService tService = new TemplateService();
        Template t = tService.GetByTemplateId((int)TemplateTypeList.S812Audit);
        if (t != null)
        {
            html = Encoding.ASCII.GetString(t.TemplateContent);
            html = html.Replace("{date_today}", DateTime.Now.ToString("dd/MM/yyyy"));
            html = html.Replace("{site}", cbLocation.Text);

            ConfigSa812Service csa812Service = new ConfigSa812Service();
            ConfigSa812 csa812 = csa812Service.GetBySiteId((int)cbLocation.Value);
            UsersService uService = new UsersService();
            Users u = uService.GetByUserId((int)csa812.SupervisorUserId);
            html = html.Replace("{contract_supervisor}", String.Format("{0} {1}", u.FirstName, u.LastName));

            string dateTrainingCompleted = "n/a";
            if (csa812.DateTrainingCompleted != null)
            {
                DateTime dt = (DateTime)csa812.DateTrainingCompleted;
                dateTrainingCompleted = dt.ToString("dd/MM/yyyy");
            }
            html = html.Replace("{css_33051_trained_at}", dateTrainingCompleted);

            string ehsConsultant = "n/a";
            if (csa812.EhsConsultantId != null)
            {
                EhsConsultantService eService = new EhsConsultantService();
                EhsConsultant ehs = eService.GetByEhsConsultantId((int)csa812.EhsConsultantId);
                u = null;
                u = uService.GetByUserId(ehs.UserId);
                if (u != null)
                {
                    ehsConsultant = String.Format("{0} {1}", u.FirstName, u.LastName);
                }
                else
                {
                    ehsConsultant = ehs.UserId.ToString();
                }
            }
            html = html.Replace("{ehs_consultant}", ehsConsultant);

            string dateTrainingCompleted2 = "n/a";
            if (csa812.DateTrainingCompleted2 != null)
            {
                DateTime dt = (DateTime)csa812.DateTrainingCompleted2;
                dateTrainingCompleted2 = dt.ToString("dd/MM/yyyy");
            }
            html = html.Replace("{ehsc_33051_trained_at}", dateTrainingCompleted2);

            html = html.Replace("{number_interviewed}", tbNoInterviewed.Text);

            int count = 0;
            CompanySiteCategoryStandardFilters queryC = new CompanySiteCategoryStandardFilters();
            queryC.Append(CompanySiteCategoryStandardColumn.SiteId, cbLocation.Value.ToString());
            queryC.AppendIsNotNull(CompanySiteCategoryStandardColumn.CompanySiteCategoryId);
            TList<CompanySiteCategoryStandard> cList = DataRepository.CompanySiteCategoryStandardProvider.GetPaged(queryC.ToString(), null, 0, 100, out count);
            html = html.Replace("{number_active}", count.ToString());
            count = 0;

            queryC = new CompanySiteCategoryStandardFilters();
            queryC.Append(CompanySiteCategoryStandardColumn.SiteId, cbLocation.Value.ToString());
            queryC.Append(CompanySiteCategoryStandardColumn.CompanySiteCategoryId, "1");
            cList.Dispose();
            cList = DataRepository.CompanySiteCategoryStandardProvider.GetPaged(queryC.ToString(), null, 0, 100, out count);

            html = html.Replace("{number_embedded}", count.ToString());
            
        }
        
        PlaceHolder1.Controls.Add(new LiteralControl(html));
        Panel1.Visible = true;
    }
}
