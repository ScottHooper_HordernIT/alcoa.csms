﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ApssLogsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ApssLogsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ApssLogs, KaiZen.CSMS.Entities.ApssLogsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ApssLogsKey key)
		{
			return Delete(transactionManager, key.EntryId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_entryId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _entryId)
		{
			return Delete(null, _entryId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_entryId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _entryId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.ApssLogs Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ApssLogsKey key, int start, int pageLength)
		{
			return GetByEntryId(transactionManager, key.EntryId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_RoboCopyLogs_EntryId index.
		/// </summary>
		/// <param name="_entryId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByEntryId(System.Int32 _entryId)
		{
			int count = -1;
			return GetByEntryId(null,_entryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RoboCopyLogs_EntryId index.
		/// </summary>
		/// <param name="_entryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByEntryId(System.Int32 _entryId, int start, int pageLength)
		{
			int count = -1;
			return GetByEntryId(null, _entryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RoboCopyLogs_EntryId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_entryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByEntryId(TransactionManager transactionManager, System.Int32 _entryId)
		{
			int count = -1;
			return GetByEntryId(transactionManager, _entryId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RoboCopyLogs_EntryId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_entryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByEntryId(TransactionManager transactionManager, System.Int32 _entryId, int start, int pageLength)
		{
			int count = -1;
			return GetByEntryId(transactionManager, _entryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RoboCopyLogs_EntryId index.
		/// </summary>
		/// <param name="_entryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByEntryId(System.Int32 _entryId, int start, int pageLength, out int count)
		{
			return GetByEntryId(null, _entryId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RoboCopyLogs_EntryId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_entryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ApssLogs GetByEntryId(TransactionManager transactionManager, System.Int32 _entryId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_TimeStampFileNameFileTag index.
		/// </summary>
		/// <param name="_timeStamp"></param>
		/// <param name="_fileName"></param>
		/// <param name="_fileTag"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByTimeStampFileNameFileTag(System.DateTime _timeStamp, System.String _fileName, System.String _fileTag)
		{
			int count = -1;
			return GetByTimeStampFileNameFileTag(null,_timeStamp, _fileName, _fileTag, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TimeStampFileNameFileTag index.
		/// </summary>
		/// <param name="_timeStamp"></param>
		/// <param name="_fileName"></param>
		/// <param name="_fileTag"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByTimeStampFileNameFileTag(System.DateTime _timeStamp, System.String _fileName, System.String _fileTag, int start, int pageLength)
		{
			int count = -1;
			return GetByTimeStampFileNameFileTag(null, _timeStamp, _fileName, _fileTag, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TimeStampFileNameFileTag index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_timeStamp"></param>
		/// <param name="_fileName"></param>
		/// <param name="_fileTag"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByTimeStampFileNameFileTag(TransactionManager transactionManager, System.DateTime _timeStamp, System.String _fileName, System.String _fileTag)
		{
			int count = -1;
			return GetByTimeStampFileNameFileTag(transactionManager, _timeStamp, _fileName, _fileTag, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TimeStampFileNameFileTag index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_timeStamp"></param>
		/// <param name="_fileName"></param>
		/// <param name="_fileTag"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByTimeStampFileNameFileTag(TransactionManager transactionManager, System.DateTime _timeStamp, System.String _fileName, System.String _fileTag, int start, int pageLength)
		{
			int count = -1;
			return GetByTimeStampFileNameFileTag(transactionManager, _timeStamp, _fileName, _fileTag, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TimeStampFileNameFileTag index.
		/// </summary>
		/// <param name="_timeStamp"></param>
		/// <param name="_fileName"></param>
		/// <param name="_fileTag"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public KaiZen.CSMS.Entities.ApssLogs GetByTimeStampFileNameFileTag(System.DateTime _timeStamp, System.String _fileName, System.String _fileTag, int start, int pageLength, out int count)
		{
			return GetByTimeStampFileNameFileTag(null, _timeStamp, _fileName, _fileTag, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_TimeStampFileNameFileTag index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_timeStamp"></param>
		/// <param name="_fileName"></param>
		/// <param name="_fileTag"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.ApssLogs GetByTimeStampFileNameFileTag(TransactionManager transactionManager, System.DateTime _timeStamp, System.String _fileName, System.String _fileTag, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ApssLogs&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ApssLogs&gt;"/></returns>
		public static TList<ApssLogs> Fill(IDataReader reader, TList<ApssLogs> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.ApssLogs c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ApssLogs")
					.Append("|").Append((System.Int32)reader[((int)ApssLogsColumn.EntryId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ApssLogs>(
					key.ToString(), // EntityTrackingKey
					"ApssLogs",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.ApssLogs();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EntryId = (System.Int32)reader[((int)ApssLogsColumn.EntryId - 1)];
					c.TimeStamp = (System.DateTime)reader[((int)ApssLogsColumn.TimeStamp - 1)];
					c.FileName = (System.String)reader[((int)ApssLogsColumn.FileName - 1)];
					c.FileTag = (System.String)reader[((int)ApssLogsColumn.FileTag - 1)];
					c.FileType = (System.String)reader[((int)ApssLogsColumn.FileType - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ApssLogs"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ApssLogs entity)
		{
			if (!reader.Read()) return;
			
			entity.EntryId = (System.Int32)reader[((int)ApssLogsColumn.EntryId - 1)];
			entity.TimeStamp = (System.DateTime)reader[((int)ApssLogsColumn.TimeStamp - 1)];
			entity.FileName = (System.String)reader[((int)ApssLogsColumn.FileName - 1)];
			entity.FileTag = (System.String)reader[((int)ApssLogsColumn.FileTag - 1)];
			entity.FileType = (System.String)reader[((int)ApssLogsColumn.FileType - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.ApssLogs"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ApssLogs"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ApssLogs entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EntryId = (System.Int32)dataRow["EntryId"];
			entity.TimeStamp = (System.DateTime)dataRow["TimeStamp"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.FileTag = (System.String)dataRow["FileTag"];
			entity.FileType = (System.String)dataRow["FileType"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.ApssLogs"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ApssLogs Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ApssLogs entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.ApssLogs object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.ApssLogs instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.ApssLogs Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ApssLogs entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ApssLogsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.ApssLogs</c>
	///</summary>
	public enum ApssLogsChildEntityTypes
	{
	}
	
	#endregion ApssLogsChildEntityTypes
	
	#region ApssLogsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ApssLogsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsFilterBuilder : SqlFilterBuilder<ApssLogsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsFilterBuilder class.
		/// </summary>
		public ApssLogsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ApssLogsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ApssLogsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ApssLogsFilterBuilder
	
	#region ApssLogsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ApssLogsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApssLogsParameterBuilder : ParameterizedSqlFilterBuilder<ApssLogsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsParameterBuilder class.
		/// </summary>
		public ApssLogsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ApssLogsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ApssLogsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ApssLogsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ApssLogsParameterBuilder
	
	#region ApssLogsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ApssLogsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApssLogs"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ApssLogsSortBuilder : SqlSortBuilder<ApssLogsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApssLogsSqlSortBuilder class.
		/// </summary>
		public ApssLogsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ApssLogsSortBuilder
	
} // end namespace
