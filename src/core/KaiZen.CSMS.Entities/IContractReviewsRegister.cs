﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
    ///		The data structure representation of the 'ContractReviewsRegister' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
    public interface IContractReviewsRegister
	{
		/// <summary>			
        /// ContractId : 
		/// </summary>
        /// <remarks>Member of the primary key of the underlying table "ContractReviewsRegister"</remarks>
		System.Int32 ContractId { get; set; }

        /// <summary>
        /// CompanyId : 
        /// </summary>
        System.Int32 CompanyId { get; set; }	
		
		/// <summary>
        /// SiteId : 
		/// </summary>
		System.Int32  SiteId  { get; set; }	
		
		
		/// <summary>
        /// Year : 
		/// </summary>
		System.Int32  Year  { get; set; }
		
		/// <summary>
        /// January : 
		/// </summary>
		System.Boolean  January  { get; set; }
		
		/// <summary>
        /// February : 
		/// </summary>
		System.Boolean  February  { get; set; }
		
		/// <summary>
        /// March : 
		/// </summary>
		System.Boolean  March  { get; set; }

        /// <summary>
        /// April : 
        /// </summary>
        System.Boolean April { get; set; }

        /// <summary>
        /// May : 
        /// </summary>
        System.Boolean May { get; set; }

        /// <summary>
        /// June : 
        /// </summary>
        System.Boolean June { get; set; }

        /// <summary>
        /// July : 
        /// </summary>
        System.Boolean July { get; set; }

        /// <summary>
        /// August : 
        /// </summary>
        System.Boolean August { get; set; }

        /// <summary>
        /// September : 
        /// </summary>
        System.Boolean September { get; set; }

        /// <summary>
        /// October : 
        /// </summary>
        System.Boolean October { get; set; }

        /// <summary>
        /// November : 
        /// </summary>
        System.Boolean November { get; set; }

        /// <summary>
        /// December : 
        /// </summary>
        System.Boolean December { get; set; }

        /// <summary>
        /// YearCount : 
        /// </summary>
        System.Int32 YearCount { get; set; }

        /// <summary>
        /// LocationSponsorUserId : 
        /// </summary>
        System.Int32? LocationSponsorUserId { get; set; }

        /// <summary>
        /// LocationSpaUserId : 
        /// </summary>
        System.Int32? LocationSpaUserId { get; set; }

        /// <summary>
        /// ModifiedDate : 
        /// </summary>
        System.DateTime ModifiedDate { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32  ModifiedBy  { get; set; }
		
		
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


