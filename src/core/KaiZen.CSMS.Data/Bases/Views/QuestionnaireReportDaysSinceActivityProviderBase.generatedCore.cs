﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportDaysSinceActivityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportDaysSinceActivityProviderBaseCore : EntityViewProviderBase<QuestionnaireReportDaysSinceActivity>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportDaysSinceActivity&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportDaysSinceActivity&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportDaysSinceActivity&gt; Fill(DataSet dataSet, VList<QuestionnaireReportDaysSinceActivity> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportDaysSinceActivity>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportDaysSinceActivity&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportDaysSinceActivity>"/></returns>
		protected static VList&lt;QuestionnaireReportDaysSinceActivity&gt; Fill(DataTable dataTable, VList<QuestionnaireReportDaysSinceActivity> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportDaysSinceActivity c = new QuestionnaireReportDaysSinceActivity();
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.QuestionnaireStatus = (Convert.IsDBNull(row["QuestionnaireStatus"]))?string.Empty:(System.String)row["QuestionnaireStatus"];
					c.ProcurementContact = (Convert.IsDBNull(row["ProcurementContact"]))?string.Empty:(System.String)row["ProcurementContact"];
					c.ProcurementSite = (Convert.IsDBNull(row["ProcurementSite"]))?string.Empty:(System.String)row["ProcurementSite"];
					c.ProcurementRegion = (Convert.IsDBNull(row["ProcurementRegion"]))?string.Empty:(System.String)row["ProcurementRegion"];
					c.HsAssessorSite = (Convert.IsDBNull(row["HsAssessorSite"]))?string.Empty:(System.String)row["HsAssessorSite"];
					c.HsAssessorRegion = (Convert.IsDBNull(row["HsAssessorRegion"]))?string.Empty:(System.String)row["HsAssessorRegion"];
					c.FunctionalManager = (Convert.IsDBNull(row["FunctionalManager"]))?string.Empty:(System.String)row["FunctionalManager"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.PresentlyWith = (Convert.IsDBNull(row["PresentlyWith"]))?string.Empty:(System.String)row["PresentlyWith"];
					c.IsReQualification = (Convert.IsDBNull(row["IsReQualification"]))?false:(System.Boolean)row["IsReQualification"];
					c.Validity = (Convert.IsDBNull(row["Validity"]))?string.Empty:(System.String)row["Validity"];
					c.DaysSinceActivity = (Convert.IsDBNull(row["DaysSinceActivity"]))?(int)0:(System.Int32?)row["DaysSinceActivity"];
					c.ExpiresIn = (Convert.IsDBNull(row["ExpiresIn"]))?(int)0:(System.Int32?)row["ExpiresIn"];
					c.CompanyStatusDesc = (Convert.IsDBNull(row["CompanyStatusDesc"]))?string.Empty:(System.String)row["CompanyStatusDesc"];
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(row["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithActionId"];
					c.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(row["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithUserId"];
					c.ProcessNo = (Convert.IsDBNull(row["ProcessNo"]))?(int)0:(System.Int32?)row["ProcessNo"];
					c.UserName = (Convert.IsDBNull(row["UserName"]))?string.Empty:(System.String)row["UserName"];
					c.ActionName = (Convert.IsDBNull(row["ActionName"]))?string.Empty:(System.String)row["ActionName"];
					c.UserDescription = (Convert.IsDBNull(row["UserDescription"]))?string.Empty:(System.String)row["UserDescription"];
					c.ActionDescription = (Convert.IsDBNull(row["ActionDescription"]))?string.Empty:(System.String)row["ActionDescription"];
					c.QuestionnairePresentlyWithSince = (Convert.IsDBNull(row["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)row["QuestionnairePresentlyWithSince"];
					c.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(row["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)row["QuestionnairePresentlyWithSinceDaysCount"];
					c.LastTimeOnSite = (Convert.IsDBNull(row["LastTimeOnSite"]))?DateTime.MinValue:(System.DateTime?)row["LastTimeOnSite"];
					c.TypeContractor = (Convert.IsDBNull(row["TypeContractor"]))?string.Empty:(System.String)row["TypeContractor"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportDaysSinceActivity&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportDaysSinceActivity&gt;"/></returns>
		protected VList<QuestionnaireReportDaysSinceActivity> Fill(IDataReader reader, VList<QuestionnaireReportDaysSinceActivity> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportDaysSinceActivity entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportDaysSinceActivity>("QuestionnaireReportDaysSinceActivity",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportDaysSinceActivity();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyName = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.QuestionnaireStatus = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnaireStatus)];
					//entity.QuestionnaireStatus = (Convert.IsDBNull(reader["QuestionnaireStatus"]))?string.Empty:(System.String)reader["QuestionnaireStatus"];
					entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementContact)];
					//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
					entity.ProcurementSite = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementSite)];
					//entity.ProcurementSite = (Convert.IsDBNull(reader["ProcurementSite"]))?string.Empty:(System.String)reader["ProcurementSite"];
					entity.ProcurementRegion = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementRegion)];
					//entity.ProcurementRegion = (Convert.IsDBNull(reader["ProcurementRegion"]))?string.Empty:(System.String)reader["ProcurementRegion"];
					entity.HsAssessorSite = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.HsAssessorSite)];
					//entity.HsAssessorSite = (Convert.IsDBNull(reader["HsAssessorSite"]))?string.Empty:(System.String)reader["HsAssessorSite"];
					entity.HsAssessorRegion = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.HsAssessorRegion)];
					//entity.HsAssessorRegion = (Convert.IsDBNull(reader["HsAssessorRegion"]))?string.Empty:(System.String)reader["HsAssessorRegion"];
					entity.FunctionalManager = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.FunctionalManager)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.FunctionalManager)];
					//entity.FunctionalManager = (Convert.IsDBNull(reader["FunctionalManager"]))?string.Empty:(System.String)reader["FunctionalManager"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.PresentlyWith = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.PresentlyWith)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.PresentlyWith)];
					//entity.PresentlyWith = (Convert.IsDBNull(reader["PresentlyWith"]))?string.Empty:(System.String)reader["PresentlyWith"];
					entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireReportDaysSinceActivityColumn.IsReQualification)];
					//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
					entity.Validity = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.Validity)];
					//entity.Validity = (Convert.IsDBNull(reader["Validity"]))?string.Empty:(System.String)reader["Validity"];
					entity.DaysSinceActivity = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.DaysSinceActivity)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.DaysSinceActivity)];
					//entity.DaysSinceActivity = (Convert.IsDBNull(reader["DaysSinceActivity"]))?(int)0:(System.Int32?)reader["DaysSinceActivity"];
					entity.ExpiresIn = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ExpiresIn)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ExpiresIn)];
					//entity.ExpiresIn = (Convert.IsDBNull(reader["ExpiresIn"]))?(int)0:(System.Int32?)reader["ExpiresIn"];
					entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.CompanyStatusDesc)];
					//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithActionId)];
					//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
					entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithUserId)];
					//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
					entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcessNo)];
					//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
					entity.UserName = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.UserName)];
					//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
					entity.ActionName = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ActionName)];
					//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
					entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.UserDescription)];
					//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
					entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ActionDescription)];
					//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
					entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSince)];
					//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
					entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSinceDaysCount)];
					//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
					entity.LastTimeOnSite = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.LastTimeOnSite)))?null:(System.DateTime?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.LastTimeOnSite)];
					//entity.LastTimeOnSite = (Convert.IsDBNull(reader["LastTimeOnSite"]))?DateTime.MinValue:(System.DateTime?)reader["LastTimeOnSite"];
					entity.TypeContractor = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.TypeContractor)];
					//entity.TypeContractor = (Convert.IsDBNull(reader["TypeContractor"]))?string.Empty:(System.String)reader["TypeContractor"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportDaysSinceActivity"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportDaysSinceActivity"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportDaysSinceActivity entity)
		{
			reader.Read();
			entity.CompanyName = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.QuestionnaireStatus = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnaireStatus)];
			//entity.QuestionnaireStatus = (Convert.IsDBNull(reader["QuestionnaireStatus"]))?string.Empty:(System.String)reader["QuestionnaireStatus"];
			entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementContact)];
			//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
			entity.ProcurementSite = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementSite)];
			//entity.ProcurementSite = (Convert.IsDBNull(reader["ProcurementSite"]))?string.Empty:(System.String)reader["ProcurementSite"];
			entity.ProcurementRegion = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcurementRegion)];
			//entity.ProcurementRegion = (Convert.IsDBNull(reader["ProcurementRegion"]))?string.Empty:(System.String)reader["ProcurementRegion"];
			entity.HsAssessorSite = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.HsAssessorSite)];
			//entity.HsAssessorSite = (Convert.IsDBNull(reader["HsAssessorSite"]))?string.Empty:(System.String)reader["HsAssessorSite"];
			entity.HsAssessorRegion = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.HsAssessorRegion)];
			//entity.HsAssessorRegion = (Convert.IsDBNull(reader["HsAssessorRegion"]))?string.Empty:(System.String)reader["HsAssessorRegion"];
			entity.FunctionalManager = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.FunctionalManager)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.FunctionalManager)];
			//entity.FunctionalManager = (Convert.IsDBNull(reader["FunctionalManager"]))?string.Empty:(System.String)reader["FunctionalManager"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			entity.PresentlyWith = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.PresentlyWith)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.PresentlyWith)];
			//entity.PresentlyWith = (Convert.IsDBNull(reader["PresentlyWith"]))?string.Empty:(System.String)reader["PresentlyWith"];
			entity.IsReQualification = (System.Boolean)reader[((int)QuestionnaireReportDaysSinceActivityColumn.IsReQualification)];
			//entity.IsReQualification = (Convert.IsDBNull(reader["IsReQualification"]))?false:(System.Boolean)reader["IsReQualification"];
			entity.Validity = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.Validity)];
			//entity.Validity = (Convert.IsDBNull(reader["Validity"]))?string.Empty:(System.String)reader["Validity"];
			entity.DaysSinceActivity = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.DaysSinceActivity)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.DaysSinceActivity)];
			//entity.DaysSinceActivity = (Convert.IsDBNull(reader["DaysSinceActivity"]))?(int)0:(System.Int32?)reader["DaysSinceActivity"];
			entity.ExpiresIn = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ExpiresIn)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ExpiresIn)];
			//entity.ExpiresIn = (Convert.IsDBNull(reader["ExpiresIn"]))?(int)0:(System.Int32?)reader["ExpiresIn"];
			entity.CompanyStatusDesc = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.CompanyStatusDesc)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.CompanyStatusDesc)];
			//entity.CompanyStatusDesc = (Convert.IsDBNull(reader["CompanyStatusDesc"]))?string.Empty:(System.String)reader["CompanyStatusDesc"];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithActionId)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithActionId)];
			//entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithUserId)];
			//entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(reader["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ProcessNo)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ProcessNo)];
			//entity.ProcessNo = (Convert.IsDBNull(reader["ProcessNo"]))?(int)0:(System.Int32?)reader["ProcessNo"];
			entity.UserName = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.UserName)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.UserName)];
			//entity.UserName = (Convert.IsDBNull(reader["UserName"]))?string.Empty:(System.String)reader["UserName"];
			entity.ActionName = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ActionName)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ActionName)];
			//entity.ActionName = (Convert.IsDBNull(reader["ActionName"]))?string.Empty:(System.String)reader["ActionName"];
			entity.UserDescription = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.UserDescription)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.UserDescription)];
			//entity.UserDescription = (Convert.IsDBNull(reader["UserDescription"]))?string.Empty:(System.String)reader["UserDescription"];
			entity.ActionDescription = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.ActionDescription)))?null:(System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.ActionDescription)];
			//entity.ActionDescription = (Convert.IsDBNull(reader["ActionDescription"]))?string.Empty:(System.String)reader["ActionDescription"];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSince)))?null:(System.DateTime?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSince)];
			//entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)reader["QuestionnairePresentlyWithSince"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSinceDaysCount)))?null:(System.Int32?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.QuestionnairePresentlyWithSinceDaysCount)];
			//entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(reader["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)reader["QuestionnairePresentlyWithSinceDaysCount"];
			entity.LastTimeOnSite = (reader.IsDBNull(((int)QuestionnaireReportDaysSinceActivityColumn.LastTimeOnSite)))?null:(System.DateTime?)reader[((int)QuestionnaireReportDaysSinceActivityColumn.LastTimeOnSite)];
			//entity.LastTimeOnSite = (Convert.IsDBNull(reader["LastTimeOnSite"]))?DateTime.MinValue:(System.DateTime?)reader["LastTimeOnSite"];
			entity.TypeContractor = (System.String)reader[((int)QuestionnaireReportDaysSinceActivityColumn.TypeContractor)];
			//entity.TypeContractor = (Convert.IsDBNull(reader["TypeContractor"]))?string.Empty:(System.String)reader["TypeContractor"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportDaysSinceActivity"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportDaysSinceActivity"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportDaysSinceActivity entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.QuestionnaireStatus = (Convert.IsDBNull(dataRow["QuestionnaireStatus"]))?string.Empty:(System.String)dataRow["QuestionnaireStatus"];
			entity.ProcurementContact = (Convert.IsDBNull(dataRow["ProcurementContact"]))?string.Empty:(System.String)dataRow["ProcurementContact"];
			entity.ProcurementSite = (Convert.IsDBNull(dataRow["ProcurementSite"]))?string.Empty:(System.String)dataRow["ProcurementSite"];
			entity.ProcurementRegion = (Convert.IsDBNull(dataRow["ProcurementRegion"]))?string.Empty:(System.String)dataRow["ProcurementRegion"];
			entity.HsAssessorSite = (Convert.IsDBNull(dataRow["HsAssessorSite"]))?string.Empty:(System.String)dataRow["HsAssessorSite"];
			entity.HsAssessorRegion = (Convert.IsDBNull(dataRow["HsAssessorRegion"]))?string.Empty:(System.String)dataRow["HsAssessorRegion"];
			entity.FunctionalManager = (Convert.IsDBNull(dataRow["FunctionalManager"]))?string.Empty:(System.String)dataRow["FunctionalManager"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.PresentlyWith = (Convert.IsDBNull(dataRow["PresentlyWith"]))?string.Empty:(System.String)dataRow["PresentlyWith"];
			entity.IsReQualification = (Convert.IsDBNull(dataRow["IsReQualification"]))?false:(System.Boolean)dataRow["IsReQualification"];
			entity.Validity = (Convert.IsDBNull(dataRow["Validity"]))?string.Empty:(System.String)dataRow["Validity"];
			entity.DaysSinceActivity = (Convert.IsDBNull(dataRow["DaysSinceActivity"]))?(int)0:(System.Int32?)dataRow["DaysSinceActivity"];
			entity.ExpiresIn = (Convert.IsDBNull(dataRow["ExpiresIn"]))?(int)0:(System.Int32?)dataRow["ExpiresIn"];
			entity.CompanyStatusDesc = (Convert.IsDBNull(dataRow["CompanyStatusDesc"]))?string.Empty:(System.String)dataRow["CompanyStatusDesc"];
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.QuestionnairePresentlyWithActionId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithUserId = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithUserId"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithUserId"];
			entity.ProcessNo = (Convert.IsDBNull(dataRow["ProcessNo"]))?(int)0:(System.Int32?)dataRow["ProcessNo"];
			entity.UserName = (Convert.IsDBNull(dataRow["UserName"]))?string.Empty:(System.String)dataRow["UserName"];
			entity.ActionName = (Convert.IsDBNull(dataRow["ActionName"]))?string.Empty:(System.String)dataRow["ActionName"];
			entity.UserDescription = (Convert.IsDBNull(dataRow["UserDescription"]))?string.Empty:(System.String)dataRow["UserDescription"];
			entity.ActionDescription = (Convert.IsDBNull(dataRow["ActionDescription"]))?string.Empty:(System.String)dataRow["ActionDescription"];
			entity.QuestionnairePresentlyWithSince = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]))?DateTime.MinValue:(System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.QuestionnairePresentlyWithSinceDaysCount = (Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSinceDaysCount"]))?(int)0:(System.Int32?)dataRow["QuestionnairePresentlyWithSinceDaysCount"];
			entity.LastTimeOnSite = (Convert.IsDBNull(dataRow["LastTimeOnSite"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastTimeOnSite"];
			entity.TypeContractor = (Convert.IsDBNull(dataRow["TypeContractor"]))?string.Empty:(System.String)dataRow["TypeContractor"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportDaysSinceActivityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportDaysSinceActivityFilterBuilder : SqlFilterBuilder<QuestionnaireReportDaysSinceActivityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityFilterBuilder class.
		/// </summary>
		public QuestionnaireReportDaysSinceActivityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportDaysSinceActivityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportDaysSinceActivityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportDaysSinceActivityFilterBuilder

	#region QuestionnaireReportDaysSinceActivityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportDaysSinceActivityParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportDaysSinceActivityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityParameterBuilder class.
		/// </summary>
		public QuestionnaireReportDaysSinceActivityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportDaysSinceActivityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportDaysSinceActivityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportDaysSinceActivityParameterBuilder
	
	#region QuestionnaireReportDaysSinceActivitySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportDaysSinceActivity"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportDaysSinceActivitySortBuilder : SqlSortBuilder<QuestionnaireReportDaysSinceActivityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportDaysSinceActivitySqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportDaysSinceActivitySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportDaysSinceActivitySortBuilder

} // end namespace
