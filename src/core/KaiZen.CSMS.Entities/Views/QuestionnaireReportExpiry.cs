﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireReportExpiry' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireReportExpiry : QuestionnaireReportExpiryBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireReportExpiry"/> instance.
		///</summary>
		public QuestionnaireReportExpiry():base(){}	
		
		#endregion
	}
}
