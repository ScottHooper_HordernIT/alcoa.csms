﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.IO;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class FileVault_HelpFiles : System.Web.UI.UserControl
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            { //first time load
                if (auth.RoleId == (int)RoleList.Administrator)
                {
                   //
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
        }



        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {

        }

        protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {

        }

        protected void grid_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != GridViewRowType.Data) return;
            ASPxHyperLink hlEdit = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlEdit") as ASPxHyperLink;
            if (hlEdit != null)
                hlEdit.NavigateUrl = String.Format("javascript:popUp('../PopUps/EditHelpFile.aspx?q={0}');", 
                                                        (int)grid.GetRowValues(e.VisibleIndex, "FileVaultId"));

            ASPxLabel lblFileSize = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblFileSize") as ASPxLabel;
            if(lblFileSize != null)
                lblFileSize.Text = String.Format("{0:n0}KB", KaiZen.Library.FileUtilities.Convert.BytestoKilobytes(Convert.ToInt64((int)grid.GetRowValues(e.VisibleIndex, "ContentLength"))));
        }
    }
}