using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_SQ : System.Web.UI.UserControl
{
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        switch (auth.RoleId)
        {
            case ((int)RoleList.Reader):
                break;
            case ((int)RoleList.Contractor):
                break;
            case ((int)RoleList.Administrator):
                break;
            default:
                // do something
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                break;
        }
		//Added By Bishwajit for Item#31
        //Session value to check if the data will be come to PDF or not
        Session["SQValue"] = false;
		//End Added By Bishwajit for Item#31
        if (SessionHandler.spVar_Page == "ComplianceReport")
        {
            grid.FilterExpression = "CompanyId = " + SessionHandler.spVar_CompanyId;
            grid.DataBind();
			//Added By Bishwajit for Item#31
            //Session value to check if the data will be come to PDF or not
            Session["SQValue"] = true;
			//End Added By Bishwajit for Item#31
        }

    }
    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

        string mainAssessmentStatus = "";
        string MainAssessmentValidTo = "";

        if (grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating") != null)
        {
            if (grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating") != DBNull.Value)
                mainAssessmentStatus = (string)grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating");
        }

        if (grid.GetRowValues(e.VisibleIndex, "MainAssessmentValidTo") != null)
        {
            if (grid.GetRowValues(e.VisibleIndex, "MainAssessmentValidTo") != DBNull.Value)
            {

                DateTime dtExpiry = (DateTime)grid.GetRowValues(e.VisibleIndex, "MainAssessmentValidTo");
                MainAssessmentValidTo = dtExpiry.ToString();
            }
        }

        if(String.IsNullOrEmpty(MainAssessmentValidTo))
        {
            //check previous questionnaire
            if(grid.GetRowValues(e.VisibleIndex, "CompanyId") != null)
            {
                if (grid.GetRowValues(e.VisibleIndex, "CompanyId") != DBNull.Value)
                {
                    QuestionnaireFilters qFilters = new QuestionnaireFilters();
                    qFilters.Append(QuestionnaireColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
                    qFilters.Append(QuestionnaireColumn.CompanyId, ((int)grid.GetRowValues(e.VisibleIndex, "CompanyId")).ToString());
                    int count = 0;
                    TList<Questionnaire> qList =
                        DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);
                    if (qList != null && count > 0)
                    {
                        if (qList[0].MainAssessmentValidTo != null)
                        {
                            MainAssessmentValidTo = qList[0].MainAssessmentValidTo.ToString() +" (Previous Questionnaire)";
                        }
                    }
                }
            }
        }

        Label lblExpiresAt = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblExpiresAt") as Label;
        if (lblExpiresAt != null) lblExpiresAt.Text = MainAssessmentValidTo;

        if (!String.IsNullOrEmpty(mainAssessmentStatus))
        {
            Label lblFinalRiskRating = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "FinalRiskLevel") as Label;
            lblFinalRiskRating.Text = "Incomplete"; //default value.

            int status = (int)grid.GetRowValues(e.VisibleIndex, "Status");
            if (status == (int)QuestionnaireStatusList.AssessmentComplete)
            {
                string ProcurementRiskRating = (string)grid.GetRowValues(e.VisibleIndex, "InitialRiskAssessment");
                string SupplierRiskRating = mainAssessmentStatus;
                if (!String.IsNullOrEmpty(ProcurementRiskRating))
                {
                    lblFinalRiskRating.Text = ProcurementRiskRating;
                }
                if (!String.IsNullOrEmpty(SupplierRiskRating))
                {
                    int _ProcurementRiskRating = Helper.Questionnaire.getRiskLevel(ProcurementRiskRating);
                    int _SupplierRiskRating = Helper.Questionnaire.getRiskLevel(SupplierRiskRating);

                    if (_SupplierRiskRating > _ProcurementRiskRating)
                    {
                        lblFinalRiskRating.Text = SupplierRiskRating;
                    }
                }
            }
        }
    }
}
