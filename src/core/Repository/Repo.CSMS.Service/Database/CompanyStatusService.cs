﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyStatusService
    {
        CompanyStatus Get(Expression<Func<CompanyStatus, bool>> where, List<Expression<Func<CompanyStatus, object>>> includes);
        List<CompanyStatus> GetMany(int? top, Expression<Func<CompanyStatus, bool>> where, Func<IQueryable<CompanyStatus>, IOrderedQueryable<CompanyStatus>> orderBy, List<Expression<Func<CompanyStatus, object>>> includes);
        List<CompanyStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyStatus, bool>> where, Func<IQueryable<CompanyStatus>, IOrderedQueryable<CompanyStatus>> orderBy, List<Expression<Func<CompanyStatus, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyStatus, bool>> where, List<Expression<Func<CompanyStatus, object>>> includes);
        void Insert(CompanyStatus item, bool saveChanges = true);
        void Update(CompanyStatus item, bool saveChanges = true);
        void Delete(CompanyStatus item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyStatusService : ICompanyStatusService
    {
        private readonly IRepository<CompanyStatus> _repository;

        public CompanyStatusService(IRepository<CompanyStatus> repository)
        {
            this._repository = repository;
        }

        public CompanyStatus Get(Expression<Func<CompanyStatus, bool>> where, List<Expression<Func<CompanyStatus, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyStatus> GetMany(int? top, Expression<Func<CompanyStatus, bool>> where, Func<IQueryable<CompanyStatus>, IOrderedQueryable<CompanyStatus>> orderBy, List<Expression<Func<CompanyStatus, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyStatus> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyStatus, bool>> where, Func<IQueryable<CompanyStatus>, IOrderedQueryable<CompanyStatus>> orderBy, List<Expression<Func<CompanyStatus, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyStatus, bool>> where, List<Expression<Func<CompanyStatus, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatus");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatus");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyStatus item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyStatus");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
