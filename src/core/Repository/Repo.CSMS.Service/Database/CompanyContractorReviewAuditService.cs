﻿using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanyContractorReviewAuditService
    {
        CompanyContractorReviewAudit Get(Expression<Func<CompanyContractorReviewAudit, bool>> where, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes);
        List<CompanyContractorReviewAudit> GetMany(int? top, Expression<Func<CompanyContractorReviewAudit, bool>> where, Func<IQueryable<CompanyContractorReviewAudit>, IOrderedQueryable<CompanyContractorReviewAudit>> orderBy, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes);
        List<CompanyContractorReviewAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyContractorReviewAudit, bool>> where, Func<IQueryable<CompanyContractorReviewAudit>, IOrderedQueryable<CompanyContractorReviewAudit>> orderBy, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanyContractorReviewAudit, bool>> where, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes);
        void Insert(CompanyContractorReviewAudit item, bool saveChanges = true);
        void Update(CompanyContractorReviewAudit item, bool saveChanges = true);
        void Delete(CompanyContractorReviewAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanyContractorReviewAuditService : ICompanyContractorReviewAuditService
    {
        private readonly IRepository<CompanyContractorReviewAudit> _repository;

        public CompanyContractorReviewAuditService(IRepository<CompanyContractorReviewAudit> repository)
        {
            this._repository = repository;
        }

        public CompanyContractorReviewAudit Get(Expression<Func<CompanyContractorReviewAudit, bool>> where, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanyContractorReviewAudit> GetMany(int? top, Expression<Func<CompanyContractorReviewAudit, bool>> where, Func<IQueryable<CompanyContractorReviewAudit>, IOrderedQueryable<CompanyContractorReviewAudit>> orderBy, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanyContractorReviewAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanyContractorReviewAudit, bool>> where, Func<IQueryable<CompanyContractorReviewAudit>, IOrderedQueryable<CompanyContractorReviewAudit>> orderBy, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanyContractorReviewAudit, bool>> where, List<Expression<Func<CompanyContractorReviewAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanyContractorReviewAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyContractorReviewAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanyContractorReviewAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyContractorReviewAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanyContractorReviewAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanyContractorReviewAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
