﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="SafetyPlans_SelfAssessment" Codebehind="SafetyPlans_SelfAssessment.aspx.cs" %>

<%@ Register Src="UserControls/Main/SafetyPlans_SelfAssessment.ascx" TagName="SafetyPlans_SelfAssessment"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
    <style type="text/css">
        A
        {
            font-weight: normal;
            font-size: 11px;
            color: Navy;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            text-decoration: none;
        }
        A:hover
        {
            color: Navy;
            text-decoration: underline;
        }
        A:link
        {
            color: Navy;
            text-decoration: none;
        }
        A:visited
        {
            color: Navy;
            text-decoration: none;
        }
        A:active
        {
            color: Navy;
            text-decoration: none;
        }
        NoHighlightLink
        {
            color: Navy;
            text-decoration: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <uc1:SafetyPlans_SelfAssessment ID="SafetyPlans_SelfAssessment1" runat="server" />
</asp:Content>
