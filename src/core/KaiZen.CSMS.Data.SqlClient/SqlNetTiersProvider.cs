﻿
#region Using directives

using System;
using System.Collections;
using System.Collections.Specialized;


using System.Web.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;

#endregion

namespace KaiZen.CSMS.Data.SqlClient
{
	/// <summary>
	/// This class is the Sql implementation of the NetTiersProvider.
	/// </summary>
	public sealed class SqlNetTiersProvider : KaiZen.CSMS.Data.Bases.NetTiersProvider
	{
		private static object syncRoot = new Object();
		private string _applicationName;
        private string _connectionString;
        private bool _useStoredProcedure;
        string _providerInvariantName;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SqlNetTiersProvider"/> class.
		///</summary>
		public SqlNetTiersProvider()
		{	
		}		
		
		/// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
        /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"></see> on a provider after the provider has already been initialized.</exception>
        /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		public override void Initialize(string name, NameValueCollection config)
        {
            // Verify that config isn't null
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            // Assign the provider a default name if it doesn't have one
            if (String.IsNullOrEmpty(name))
            {
                name = "SqlNetTiersProvider";
            }

            // Add a default "description" attribute to config if the
            // attribute doesn't exist or is empty
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "NetTiers Sql provider");
            }

            // Call the base class's Initialize method
            base.Initialize(name, config);

            // Initialize _applicationName
            _applicationName = config["applicationName"];

            if (string.IsNullOrEmpty(_applicationName))
            {
                _applicationName = "/";
            }
            config.Remove("applicationName");


            #region "Initialize UseStoredProcedure"
            string storedProcedure  = config["useStoredProcedure"];
           	if (string.IsNullOrEmpty(storedProcedure))
            {
                throw new ProviderException("Empty or missing useStoredProcedure");
            }
            this._useStoredProcedure = Convert.ToBoolean(config["useStoredProcedure"]);
            config.Remove("useStoredProcedure");
            #endregion

			#region ConnectionString

			// Initialize _connectionString
			_connectionString = config["connectionString"];
			config.Remove("connectionString");

			string connect = config["connectionStringName"];
			config.Remove("connectionStringName");

			if ( String.IsNullOrEmpty(_connectionString) )
			{
				if ( String.IsNullOrEmpty(connect) )
				{
					throw new ProviderException("Empty or missing connectionStringName");
				}

				if ( DataRepository.ConnectionStrings[connect] == null )
				{
					throw new ProviderException("Missing connection string");
				}

				_connectionString = DataRepository.ConnectionStrings[connect].ConnectionString;
			}

            if ( String.IsNullOrEmpty(_connectionString) )
            {
                throw new ProviderException("Empty connection string");
			}

			#endregion
            
             #region "_providerInvariantName"

            // initialize _providerInvariantName
            this._providerInvariantName = config["providerInvariantName"];

            if (String.IsNullOrEmpty(_providerInvariantName))
            {
                throw new ProviderException("Empty or missing providerInvariantName");
            }
            config.Remove("providerInvariantName");

            #endregion

        }
		
		/// <summary>
		/// Creates a new <see cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public override TransactionManager CreateTransaction()
		{
			return new TransactionManager(this._connectionString);
		}
		
		/// <summary>
		/// Gets a value indicating whether to use stored procedure or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this repository use stored procedures; otherwise, <c>false</c>.
		/// </value>
		public bool UseStoredProcedure
		{
			get {return this._useStoredProcedure;}
			set {this._useStoredProcedure = value;}
		}
		
		 /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
		public string ConnectionString
		{
			get {return this._connectionString;}
			set {this._connectionString = value;}
		}
		
		/// <summary>
	    /// Gets or sets the invariant provider name listed in the DbProviderFactories machine.config section.
	    /// </summary>
	    /// <value>The name of the provider invariant.</value>
	    public string ProviderInvariantName
	    {
	        get { return this._providerInvariantName; }
	        set { this._providerInvariantName = value; }
	    }		
		
		///<summary>
		/// Indicates if the current <see cref="NetTiersProvider"/> implementation supports Transacton.
		///</summary>
		public override bool IsTransactionSupported
		{
			get
			{
				return true;
			}
		}

		
		#region "QuestionnaireMainAnswerProvider"
			
		private SqlQuestionnaireMainAnswerProvider innerSqlQuestionnaireMainAnswerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainAnswerProviderBase QuestionnaireMainAnswerProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainAnswerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainAnswerProvider == null)
						{
							this.innerSqlQuestionnaireMainAnswerProvider = new SqlQuestionnaireMainAnswerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainAnswerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainAnswerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainAnswerProvider SqlQuestionnaireMainAnswerProvider
		{
			get {return QuestionnaireMainAnswerProvider as SqlQuestionnaireMainAnswerProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireServicesCategoryProvider"
			
		private SqlQuestionnaireServicesCategoryProvider innerSqlQuestionnaireServicesCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireServicesCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireServicesCategoryProviderBase QuestionnaireServicesCategoryProvider
		{
			get
			{
				if (innerSqlQuestionnaireServicesCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireServicesCategoryProvider == null)
						{
							this.innerSqlQuestionnaireServicesCategoryProvider = new SqlQuestionnaireServicesCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireServicesCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireServicesCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireServicesCategoryProvider SqlQuestionnaireServicesCategoryProvider
		{
			get {return QuestionnaireServicesCategoryProvider as SqlQuestionnaireServicesCategoryProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationAssessmentAuditProvider"
			
		private SqlQuestionnaireVerificationAssessmentAuditProvider innerSqlQuestionnaireVerificationAssessmentAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationAssessmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationAssessmentAuditProviderBase QuestionnaireVerificationAssessmentAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationAssessmentAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationAssessmentAuditProvider == null)
						{
							this.innerSqlQuestionnaireVerificationAssessmentAuditProvider = new SqlQuestionnaireVerificationAssessmentAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationAssessmentAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationAssessmentAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationAssessmentAuditProvider SqlQuestionnaireVerificationAssessmentAuditProvider
		{
			get {return QuestionnaireVerificationAssessmentAuditProvider as SqlQuestionnaireVerificationAssessmentAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireTypeProvider"
			
		private SqlQuestionnaireTypeProvider innerSqlQuestionnaireTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireTypeProviderBase QuestionnaireTypeProvider
		{
			get
			{
				if (innerSqlQuestionnaireTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireTypeProvider == null)
						{
							this.innerSqlQuestionnaireTypeProvider = new SqlQuestionnaireTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireTypeProvider SqlQuestionnaireTypeProvider
		{
			get {return QuestionnaireTypeProvider as SqlQuestionnaireTypeProvider;}
		}
		
		#endregion
		
		
		#region "RoleProvider"
			
		private SqlRoleProvider innerSqlRoleProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Role"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RoleProviderBase RoleProvider
		{
			get
			{
				if (innerSqlRoleProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRoleProvider == null)
						{
							this.innerSqlRoleProvider = new SqlRoleProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRoleProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRoleProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRoleProvider SqlRoleProvider
		{
			get {return RoleProvider as SqlRoleProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireStatusProvider"
			
		private SqlQuestionnaireStatusProvider innerSqlQuestionnaireStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireStatusProviderBase QuestionnaireStatusProvider
		{
			get
			{
				if (innerSqlQuestionnaireStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireStatusProvider == null)
						{
							this.innerSqlQuestionnaireStatusProvider = new SqlQuestionnaireStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireStatusProvider SqlQuestionnaireStatusProvider
		{
			get {return QuestionnaireStatusProvider as SqlQuestionnaireStatusProvider;}
		}
		
		#endregion
		
		
		#region "CompanyStatusProvider"
			
		private SqlCompanyStatusProvider innerSqlCompanyStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanyStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanyStatusProviderBase CompanyStatusProvider
		{
			get
			{
				if (innerSqlCompanyStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanyStatusProvider == null)
						{
							this.innerSqlCompanyStatusProvider = new SqlCompanyStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanyStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanyStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanyStatusProvider SqlCompanyStatusProvider
		{
			get {return CompanyStatusProvider as SqlCompanyStatusProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireServicesSelectedAuditProvider"
			
		private SqlQuestionnaireServicesSelectedAuditProvider innerSqlQuestionnaireServicesSelectedAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireServicesSelectedAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireServicesSelectedAuditProviderBase QuestionnaireServicesSelectedAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireServicesSelectedAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireServicesSelectedAuditProvider == null)
						{
							this.innerSqlQuestionnaireServicesSelectedAuditProvider = new SqlQuestionnaireServicesSelectedAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireServicesSelectedAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireServicesSelectedAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireServicesSelectedAuditProvider SqlQuestionnaireServicesSelectedAuditProvider
		{
			get {return QuestionnaireServicesSelectedAuditProvider as SqlQuestionnaireServicesSelectedAuditProvider;}
		}
		
		#endregion
		
		
		#region "CompanyStatus2Provider"
			
		private SqlCompanyStatus2Provider innerSqlCompanyStatus2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanyStatus2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanyStatus2ProviderBase CompanyStatus2Provider
		{
			get
			{
				if (innerSqlCompanyStatus2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanyStatus2Provider == null)
						{
							this.innerSqlCompanyStatus2Provider = new SqlCompanyStatus2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanyStatus2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanyStatus2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanyStatus2Provider SqlCompanyStatus2Provider
		{
			get {return CompanyStatus2Provider as SqlCompanyStatus2Provider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationAttachmentAuditProvider"
			
		private SqlQuestionnaireVerificationAttachmentAuditProvider innerSqlQuestionnaireVerificationAttachmentAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationAttachmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationAttachmentAuditProviderBase QuestionnaireVerificationAttachmentAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationAttachmentAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationAttachmentAuditProvider == null)
						{
							this.innerSqlQuestionnaireVerificationAttachmentAuditProvider = new SqlQuestionnaireVerificationAttachmentAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationAttachmentAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationAttachmentAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationAttachmentAuditProvider SqlQuestionnaireVerificationAttachmentAuditProvider
		{
			get {return QuestionnaireVerificationAttachmentAuditProvider as SqlQuestionnaireVerificationAttachmentAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainQuestionProvider"
			
		private SqlQuestionnaireMainQuestionProvider innerSqlQuestionnaireMainQuestionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainQuestionProviderBase QuestionnaireMainQuestionProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainQuestionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainQuestionProvider == null)
						{
							this.innerSqlQuestionnaireMainQuestionProvider = new SqlQuestionnaireMainQuestionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainQuestionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainQuestionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainQuestionProvider SqlQuestionnaireMainQuestionProvider
		{
			get {return QuestionnaireMainQuestionProvider as SqlQuestionnaireMainQuestionProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationRationaleProvider"
			
		private SqlQuestionnaireVerificationRationaleProvider innerSqlQuestionnaireVerificationRationaleProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationRationale"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationRationaleProviderBase QuestionnaireVerificationRationaleProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationRationaleProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationRationaleProvider == null)
						{
							this.innerSqlQuestionnaireVerificationRationaleProvider = new SqlQuestionnaireVerificationRationaleProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationRationaleProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationRationaleProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationRationaleProvider SqlQuestionnaireVerificationRationaleProvider
		{
			get {return QuestionnaireVerificationRationaleProvider as SqlQuestionnaireVerificationRationaleProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireProvider"
			
		private SqlQuestionnaireProvider innerSqlQuestionnaireProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Questionnaire"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireProviderBase QuestionnaireProvider
		{
			get
			{
				if (innerSqlQuestionnaireProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireProvider == null)
						{
							this.innerSqlQuestionnaireProvider = new SqlQuestionnaireProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireProvider SqlQuestionnaireProvider
		{
			get {return QuestionnaireProvider as SqlQuestionnaireProvider;}
		}
		
		#endregion


        #region SqlSafetyPlansSEResponsesAttachmentProvider

        private SqlSafetyPlansSEResponsesAttachmentProvider innerSafetyPlansSEResponsesAttachmentProvider;

        public override SafetyPlansSEResponsesAttachmentProviderBase SafetyPlansSEResponsesAttachmentProvider
        {
            get
            {
                if (innerSafetyPlansSEResponsesAttachmentProvider == null)
                {
                    lock (syncRoot)
                    {
                        if (innerSafetyPlansSEResponsesAttachmentProvider == null)
                        {
                            this.innerSafetyPlansSEResponsesAttachmentProvider = new SqlSafetyPlansSEResponsesAttachmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
                        }
                    }
                }
                return innerSafetyPlansSEResponsesAttachmentProvider;
            }
        }

        /// <summary>
        /// Gets the current <see cref="SqlQuestionnaireProvider"/>.
        /// </summary>
        /// <value></value>
        public SqlSafetyPlansSEResponsesAttachmentProvider SqlSqlSafetyPlansSEResponsesAttachmentProvider
        {
            get { return SqlSqlSafetyPlansSEResponsesAttachmentProvider as SqlSafetyPlansSEResponsesAttachmentProvider; }
        }

        #endregion


		
		
		#region "QuestionnaireServicesSelectedProvider"
			
		private SqlQuestionnaireServicesSelectedProvider innerSqlQuestionnaireServicesSelectedProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireServicesSelected"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireServicesSelectedProviderBase QuestionnaireServicesSelectedProvider
		{
			get
			{
				if (innerSqlQuestionnaireServicesSelectedProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireServicesSelectedProvider == null)
						{
							this.innerSqlQuestionnaireServicesSelectedProvider = new SqlQuestionnaireServicesSelectedProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireServicesSelectedProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireServicesSelectedProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireServicesSelectedProvider SqlQuestionnaireServicesSelectedProvider
		{
			get {return QuestionnaireServicesSelectedProvider as SqlQuestionnaireServicesSelectedProvider;}
		}
		
		#endregion
		
		
		#region "ResidentialProvider"
			
		private SqlResidentialProvider innerSqlResidentialProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Residential"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ResidentialProviderBase ResidentialProvider
		{
			get
			{
				if (innerSqlResidentialProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlResidentialProvider == null)
						{
							this.innerSqlResidentialProvider = new SqlResidentialProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlResidentialProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlResidentialProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlResidentialProvider SqlResidentialProvider
		{
			get {return ResidentialProvider as SqlResidentialProvider;}
		}
		
		#endregion
		
		
		#region "AccessLogsProvider"
			
		private SqlAccessLogsProvider innerSqlAccessLogsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AccessLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AccessLogsProviderBase AccessLogsProvider
		{
			get
			{
				if (innerSqlAccessLogsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAccessLogsProvider == null)
						{
							this.innerSqlAccessLogsProvider = new SqlAccessLogsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAccessLogsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAccessLogsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAccessLogsProvider SqlAccessLogsProvider
		{
			get {return AccessLogsProvider as SqlAccessLogsProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnairePresentlyWithUsersProvider"
			
		private SqlQuestionnairePresentlyWithUsersProvider innerSqlQuestionnairePresentlyWithUsersProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithUsers"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnairePresentlyWithUsersProviderBase QuestionnairePresentlyWithUsersProvider
		{
			get
			{
				if (innerSqlQuestionnairePresentlyWithUsersProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnairePresentlyWithUsersProvider == null)
						{
							this.innerSqlQuestionnairePresentlyWithUsersProvider = new SqlQuestionnairePresentlyWithUsersProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnairePresentlyWithUsersProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnairePresentlyWithUsersProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnairePresentlyWithUsersProvider SqlQuestionnairePresentlyWithUsersProvider
		{
			get {return QuestionnairePresentlyWithUsersProvider as SqlQuestionnairePresentlyWithUsersProvider;}
		}
		
		#endregion
		
		
		#region "RegionsProvider"
			
		private SqlRegionsProvider innerSqlRegionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Regions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RegionsProviderBase RegionsProvider
		{
			get
			{
				if (innerSqlRegionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRegionsProvider == null)
						{
							this.innerSqlRegionsProvider = new SqlRegionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRegionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRegionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRegionsProvider SqlRegionsProvider
		{
			get {return RegionsProvider as SqlRegionsProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesProvider"
			
		private SqlCompaniesProvider innerSqlCompaniesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Companies"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesProviderBase CompaniesProvider
		{
			get
			{
				if (innerSqlCompaniesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesProvider == null)
						{
							this.innerSqlCompaniesProvider = new SqlCompaniesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesProvider SqlCompaniesProvider
		{
			get {return CompaniesProvider as SqlCompaniesProvider;}
		}
		
		#endregion
		
		
		#region "UsersProvider"
			
		private SqlUsersProvider innerSqlUsersProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Users"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProviderBase UsersProvider
		{
			get
			{
				if (innerSqlUsersProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProvider == null)
						{
							this.innerSqlUsersProvider = new SqlUsersProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProvider SqlUsersProvider
		{
			get {return UsersProvider as SqlUsersProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationAssessmentProvider"
			
		private SqlQuestionnaireVerificationAssessmentProvider innerSqlQuestionnaireVerificationAssessmentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationAssessment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationAssessmentProviderBase QuestionnaireVerificationAssessmentProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationAssessmentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationAssessmentProvider == null)
						{
							this.innerSqlQuestionnaireVerificationAssessmentProvider = new SqlQuestionnaireVerificationAssessmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationAssessmentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationAssessmentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationAssessmentProvider SqlQuestionnaireVerificationAssessmentProvider
		{
			get {return QuestionnaireVerificationAssessmentProvider as SqlQuestionnaireVerificationAssessmentProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationResponseProvider"
			
		private SqlQuestionnaireVerificationResponseProvider innerSqlQuestionnaireVerificationResponseProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationResponseProviderBase QuestionnaireVerificationResponseProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationResponseProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationResponseProvider == null)
						{
							this.innerSqlQuestionnaireVerificationResponseProvider = new SqlQuestionnaireVerificationResponseProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationResponseProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationResponseProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationResponseProvider SqlQuestionnaireVerificationResponseProvider
		{
			get {return QuestionnaireVerificationResponseProvider as SqlQuestionnaireVerificationResponseProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationResponseAuditProvider"
			
		private SqlQuestionnaireVerificationResponseAuditProvider innerSqlQuestionnaireVerificationResponseAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationResponseAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationResponseAuditProviderBase QuestionnaireVerificationResponseAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationResponseAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationResponseAuditProvider == null)
						{
							this.innerSqlQuestionnaireVerificationResponseAuditProvider = new SqlQuestionnaireVerificationResponseAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationResponseAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationResponseAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationResponseAuditProvider SqlQuestionnaireVerificationResponseAuditProvider
		{
			get {return QuestionnaireVerificationResponseAuditProvider as SqlQuestionnaireVerificationResponseAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationAttachmentProvider"
			
		private SqlQuestionnaireVerificationAttachmentProvider innerSqlQuestionnaireVerificationAttachmentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationAttachment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationAttachmentProviderBase QuestionnaireVerificationAttachmentProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationAttachmentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationAttachmentProvider == null)
						{
							this.innerSqlQuestionnaireVerificationAttachmentProvider = new SqlQuestionnaireVerificationAttachmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationAttachmentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationAttachmentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationAttachmentProvider SqlQuestionnaireVerificationAttachmentProvider
		{
			get {return QuestionnaireVerificationAttachmentProvider as SqlQuestionnaireVerificationAttachmentProvider;}
		}
		
		#endregion
		
		
		#region "RegionsSitesProvider"
			
		private SqlRegionsSitesProvider innerSqlRegionsSitesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="RegionsSites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override RegionsSitesProviderBase RegionsSitesProvider
		{
			get
			{
				if (innerSqlRegionsSitesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlRegionsSitesProvider == null)
						{
							this.innerSqlRegionsSitesProvider = new SqlRegionsSitesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlRegionsSitesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlRegionsSitesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlRegionsSitesProvider SqlRegionsSitesProvider
		{
			get {return RegionsSitesProvider as SqlRegionsSitesProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireVerificationSectionProvider"
			
		private SqlQuestionnaireVerificationSectionProvider innerSqlQuestionnaireVerificationSectionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireVerificationSection"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireVerificationSectionProviderBase QuestionnaireVerificationSectionProvider
		{
			get
			{
				if (innerSqlQuestionnaireVerificationSectionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireVerificationSectionProvider == null)
						{
							this.innerSqlQuestionnaireVerificationSectionProvider = new SqlQuestionnaireVerificationSectionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireVerificationSectionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireVerificationSectionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireVerificationSectionProvider SqlQuestionnaireVerificationSectionProvider
		{
			get {return QuestionnaireVerificationSectionProvider as SqlQuestionnaireVerificationSectionProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnairePresentlyWithMetricProvider"
			
		private SqlQuestionnairePresentlyWithMetricProvider innerSqlQuestionnairePresentlyWithMetricProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithMetric"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnairePresentlyWithMetricProviderBase QuestionnairePresentlyWithMetricProvider
		{
			get
			{
				if (innerSqlQuestionnairePresentlyWithMetricProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnairePresentlyWithMetricProvider == null)
						{
							this.innerSqlQuestionnairePresentlyWithMetricProvider = new SqlQuestionnairePresentlyWithMetricProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnairePresentlyWithMetricProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnairePresentlyWithMetricProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnairePresentlyWithMetricProvider SqlQuestionnairePresentlyWithMetricProvider
		{
			get {return QuestionnairePresentlyWithMetricProvider as SqlQuestionnairePresentlyWithMetricProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialContactStatusProvider"
			
		private SqlQuestionnaireInitialContactStatusProvider innerSqlQuestionnaireInitialContactStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialContactStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialContactStatusProviderBase QuestionnaireInitialContactStatusProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialContactStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialContactStatusProvider == null)
						{
							this.innerSqlQuestionnaireInitialContactStatusProvider = new SqlQuestionnaireInitialContactStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialContactStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialContactStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialContactStatusProvider SqlQuestionnaireInitialContactStatusProvider
		{
			get {return QuestionnaireInitialContactStatusProvider as SqlQuestionnaireInitialContactStatusProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialLocationProvider"
			
		private SqlQuestionnaireInitialLocationProvider innerSqlQuestionnaireInitialLocationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialLocation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialLocationProviderBase QuestionnaireInitialLocationProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialLocationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialLocationProvider == null)
						{
							this.innerSqlQuestionnaireInitialLocationProvider = new SqlQuestionnaireInitialLocationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialLocationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialLocationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialLocationProvider SqlQuestionnaireInitialLocationProvider
		{
			get {return QuestionnaireInitialLocationProvider as SqlQuestionnaireInitialLocationProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialLocationAuditProvider"
			
		private SqlQuestionnaireInitialLocationAuditProvider innerSqlQuestionnaireInitialLocationAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialLocationAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialLocationAuditProviderBase QuestionnaireInitialLocationAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialLocationAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialLocationAuditProvider == null)
						{
							this.innerSqlQuestionnaireInitialLocationAuditProvider = new SqlQuestionnaireInitialLocationAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialLocationAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialLocationAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialLocationAuditProvider SqlQuestionnaireInitialLocationAuditProvider
		{
			get {return QuestionnaireInitialLocationAuditProvider as SqlQuestionnaireInitialLocationAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialContactProvider"
			
		private SqlQuestionnaireInitialContactProvider innerSqlQuestionnaireInitialContactProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialContactProviderBase QuestionnaireInitialContactProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialContactProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialContactProvider == null)
						{
							this.innerSqlQuestionnaireInitialContactProvider = new SqlQuestionnaireInitialContactProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialContactProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialContactProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialContactProvider SqlQuestionnaireInitialContactProvider
		{
			get {return QuestionnaireInitialContactProvider as SqlQuestionnaireInitialContactProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialContactEmailTypeProvider"
			
		private SqlQuestionnaireInitialContactEmailTypeProvider innerSqlQuestionnaireInitialContactEmailTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialContactEmailType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialContactEmailTypeProviderBase QuestionnaireInitialContactEmailTypeProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialContactEmailTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialContactEmailTypeProvider == null)
						{
							this.innerSqlQuestionnaireInitialContactEmailTypeProvider = new SqlQuestionnaireInitialContactEmailTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialContactEmailTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialContactEmailTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialContactEmailTypeProvider SqlQuestionnaireInitialContactEmailTypeProvider
		{
			get {return QuestionnaireInitialContactEmailTypeProvider as SqlQuestionnaireInitialContactEmailTypeProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireActionProvider"
			
		private SqlQuestionnaireActionProvider innerSqlQuestionnaireActionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireAction"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireActionProviderBase QuestionnaireActionProvider
		{
			get
			{
				if (innerSqlQuestionnaireActionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireActionProvider == null)
						{
							this.innerSqlQuestionnaireActionProvider = new SqlQuestionnaireActionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireActionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireActionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireActionProvider SqlQuestionnaireActionProvider
		{
			get {return QuestionnaireActionProvider as SqlQuestionnaireActionProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialContactEmailProvider"
			
		private SqlQuestionnaireInitialContactEmailProvider innerSqlQuestionnaireInitialContactEmailProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialContactEmail"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialContactEmailProviderBase QuestionnaireInitialContactEmailProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialContactEmailProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialContactEmailProvider == null)
						{
							this.innerSqlQuestionnaireInitialContactEmailProvider = new SqlQuestionnaireInitialContactEmailProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialContactEmailProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialContactEmailProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialContactEmailProvider SqlQuestionnaireInitialContactEmailProvider
		{
			get {return QuestionnaireInitialContactEmailProvider as SqlQuestionnaireInitialContactEmailProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialResponseProvider"
			
		private SqlQuestionnaireInitialResponseProvider innerSqlQuestionnaireInitialResponseProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialResponseProviderBase QuestionnaireInitialResponseProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialResponseProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialResponseProvider == null)
						{
							this.innerSqlQuestionnaireInitialResponseProvider = new SqlQuestionnaireInitialResponseProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialResponseProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialResponseProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialResponseProvider SqlQuestionnaireInitialResponseProvider
		{
			get {return QuestionnaireInitialResponseProvider as SqlQuestionnaireInitialResponseProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialContactAuditProvider"
			
		private SqlQuestionnaireInitialContactAuditProvider innerSqlQuestionnaireInitialContactAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialContactAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialContactAuditProviderBase QuestionnaireInitialContactAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialContactAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialContactAuditProvider == null)
						{
							this.innerSqlQuestionnaireInitialContactAuditProvider = new SqlQuestionnaireInitialContactAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialContactAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialContactAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialContactAuditProvider SqlQuestionnaireInitialContactAuditProvider
		{
			get {return QuestionnaireInitialContactAuditProvider as SqlQuestionnaireInitialContactAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainResponseProvider"
			
		private SqlQuestionnaireMainResponseProvider innerSqlQuestionnaireMainResponseProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainResponse"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainResponseProviderBase QuestionnaireMainResponseProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainResponseProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainResponseProvider == null)
						{
							this.innerSqlQuestionnaireMainResponseProvider = new SqlQuestionnaireMainResponseProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainResponseProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainResponseProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainResponseProvider SqlQuestionnaireMainResponseProvider
		{
			get {return QuestionnaireMainResponseProvider as SqlQuestionnaireMainResponseProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireInitialResponseAuditProvider"
			
		private SqlQuestionnaireInitialResponseAuditProvider innerSqlQuestionnaireInitialResponseAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireInitialResponseAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireInitialResponseAuditProviderBase QuestionnaireInitialResponseAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireInitialResponseAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireInitialResponseAuditProvider == null)
						{
							this.innerSqlQuestionnaireInitialResponseAuditProvider = new SqlQuestionnaireInitialResponseAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireInitialResponseAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireInitialResponseAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireInitialResponseAuditProvider SqlQuestionnaireInitialResponseAuditProvider
		{
			get {return QuestionnaireInitialResponseAuditProvider as SqlQuestionnaireInitialResponseAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainResponseAuditProvider"
			
		private SqlQuestionnaireMainResponseAuditProvider innerSqlQuestionnaireMainResponseAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainResponseAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainResponseAuditProviderBase QuestionnaireMainResponseAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainResponseAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainResponseAuditProvider == null)
						{
							this.innerSqlQuestionnaireMainResponseAuditProvider = new SqlQuestionnaireMainResponseAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainResponseAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainResponseAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainResponseAuditProvider SqlQuestionnaireMainResponseAuditProvider
		{
			get {return QuestionnaireMainResponseAuditProvider as SqlQuestionnaireMainResponseAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainRationaleProvider"
			
		private SqlQuestionnaireMainRationaleProvider innerSqlQuestionnaireMainRationaleProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainRationale"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainRationaleProviderBase QuestionnaireMainRationaleProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainRationaleProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainRationaleProvider == null)
						{
							this.innerSqlQuestionnaireMainRationaleProvider = new SqlQuestionnaireMainRationaleProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainRationaleProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainRationaleProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainRationaleProvider SqlQuestionnaireMainRationaleProvider
		{
			get {return QuestionnaireMainRationaleProvider as SqlQuestionnaireMainRationaleProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnairePresentlyWithActionProvider"
			
		private SqlQuestionnairePresentlyWithActionProvider innerSqlQuestionnairePresentlyWithActionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithAction"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnairePresentlyWithActionProviderBase QuestionnairePresentlyWithActionProvider
		{
			get
			{
				if (innerSqlQuestionnairePresentlyWithActionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnairePresentlyWithActionProvider == null)
						{
							this.innerSqlQuestionnairePresentlyWithActionProvider = new SqlQuestionnairePresentlyWithActionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnairePresentlyWithActionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnairePresentlyWithActionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnairePresentlyWithActionProvider SqlQuestionnairePresentlyWithActionProvider
		{
			get {return QuestionnairePresentlyWithActionProvider as SqlQuestionnairePresentlyWithActionProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainAssessmentProvider"
			
		private SqlQuestionnaireMainAssessmentProvider innerSqlQuestionnaireMainAssessmentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainAssessment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainAssessmentProviderBase QuestionnaireMainAssessmentProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainAssessmentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainAssessmentProvider == null)
						{
							this.innerSqlQuestionnaireMainAssessmentProvider = new SqlQuestionnaireMainAssessmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainAssessmentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainAssessmentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainAssessmentProvider SqlQuestionnaireMainAssessmentProvider
		{
			get {return QuestionnaireMainAssessmentProvider as SqlQuestionnaireMainAssessmentProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainAttachmentAuditProvider"
			
		private SqlQuestionnaireMainAttachmentAuditProvider innerSqlQuestionnaireMainAttachmentAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainAttachmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainAttachmentAuditProviderBase QuestionnaireMainAttachmentAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainAttachmentAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainAttachmentAuditProvider == null)
						{
							this.innerSqlQuestionnaireMainAttachmentAuditProvider = new SqlQuestionnaireMainAttachmentAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainAttachmentAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainAttachmentAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainAttachmentAuditProvider SqlQuestionnaireMainAttachmentAuditProvider
		{
			get {return QuestionnaireMainAttachmentAuditProvider as SqlQuestionnaireMainAttachmentAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainAssessmentAuditProvider"
			
		private SqlQuestionnaireMainAssessmentAuditProvider innerSqlQuestionnaireMainAssessmentAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainAssessmentAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainAssessmentAuditProviderBase QuestionnaireMainAssessmentAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainAssessmentAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainAssessmentAuditProvider == null)
						{
							this.innerSqlQuestionnaireMainAssessmentAuditProvider = new SqlQuestionnaireMainAssessmentAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainAssessmentAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainAssessmentAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainAssessmentAuditProvider SqlQuestionnaireMainAssessmentAuditProvider
		{
			get {return QuestionnaireMainAssessmentAuditProvider as SqlQuestionnaireMainAssessmentAuditProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireContractorRsProvider"
			
		private SqlQuestionnaireContractorRsProvider innerSqlQuestionnaireContractorRsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireContractorRs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireContractorRsProviderBase QuestionnaireContractorRsProvider
		{
			get
			{
				if (innerSqlQuestionnaireContractorRsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireContractorRsProvider == null)
						{
							this.innerSqlQuestionnaireContractorRsProvider = new SqlQuestionnaireContractorRsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireContractorRsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireContractorRsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireContractorRsProvider SqlQuestionnaireContractorRsProvider
		{
			get {return QuestionnaireContractorRsProvider as SqlQuestionnaireContractorRsProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainAttachmentProvider"
			
		private SqlQuestionnaireMainAttachmentProvider innerSqlQuestionnaireMainAttachmentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainAttachment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainAttachmentProviderBase QuestionnaireMainAttachmentProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainAttachmentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainAttachmentProvider == null)
						{
							this.innerSqlQuestionnaireMainAttachmentProvider = new SqlQuestionnaireMainAttachmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainAttachmentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainAttachmentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainAttachmentProvider SqlQuestionnaireMainAttachmentProvider
		{
			get {return QuestionnaireMainAttachmentProvider as SqlQuestionnaireMainAttachmentProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit18Provider"
			
		private SqlTwentyOnePointAudit18Provider innerSqlTwentyOnePointAudit18Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit18"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit18ProviderBase TwentyOnePointAudit18Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit18Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit18Provider == null)
						{
							this.innerSqlTwentyOnePointAudit18Provider = new SqlTwentyOnePointAudit18Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit18Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit18Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit18Provider SqlTwentyOnePointAudit18Provider
		{
			get {return TwentyOnePointAudit18Provider as SqlTwentyOnePointAudit18Provider;}
		}
		
		#endregion
		
		
		#region "SafetyPlansSeAnswersProvider"
			
		private SqlSafetyPlansSeAnswersProvider innerSqlSafetyPlansSeAnswersProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SafetyPlansSeAnswers"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SafetyPlansSeAnswersProviderBase SafetyPlansSeAnswersProvider
		{
			get
			{
				if (innerSqlSafetyPlansSeAnswersProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSafetyPlansSeAnswersProvider == null)
						{
							this.innerSqlSafetyPlansSeAnswersProvider = new SqlSafetyPlansSeAnswersProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSafetyPlansSeAnswersProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSafetyPlansSeAnswersProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSafetyPlansSeAnswersProvider SqlSafetyPlansSeAnswersProvider
		{
			get {return SafetyPlansSeAnswersProvider as SqlSafetyPlansSeAnswersProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit19Provider"
			
		private SqlTwentyOnePointAudit19Provider innerSqlTwentyOnePointAudit19Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit19"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit19ProviderBase TwentyOnePointAudit19Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit19Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit19Provider == null)
						{
							this.innerSqlTwentyOnePointAudit19Provider = new SqlTwentyOnePointAudit19Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit19Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit19Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit19Provider SqlTwentyOnePointAudit19Provider
		{
			get {return TwentyOnePointAudit19Provider as SqlTwentyOnePointAudit19Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit17Provider"
			
		private SqlTwentyOnePointAudit17Provider innerSqlTwentyOnePointAudit17Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit17"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit17ProviderBase TwentyOnePointAudit17Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit17Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit17Provider == null)
						{
							this.innerSqlTwentyOnePointAudit17Provider = new SqlTwentyOnePointAudit17Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit17Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit17Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit17Provider SqlTwentyOnePointAudit17Provider
		{
			get {return TwentyOnePointAudit17Provider as SqlTwentyOnePointAudit17Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit20Provider"
			
		private SqlTwentyOnePointAudit20Provider innerSqlTwentyOnePointAudit20Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit20"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit20ProviderBase TwentyOnePointAudit20Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit20Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit20Provider == null)
						{
							this.innerSqlTwentyOnePointAudit20Provider = new SqlTwentyOnePointAudit20Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit20Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit20Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit20Provider SqlTwentyOnePointAudit20Provider
		{
			get {return TwentyOnePointAudit20Provider as SqlTwentyOnePointAudit20Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit13Provider"
			
		private SqlTwentyOnePointAudit13Provider innerSqlTwentyOnePointAudit13Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit13"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit13ProviderBase TwentyOnePointAudit13Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit13Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit13Provider == null)
						{
							this.innerSqlTwentyOnePointAudit13Provider = new SqlTwentyOnePointAudit13Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit13Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit13Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit13Provider SqlTwentyOnePointAudit13Provider
		{
			get {return TwentyOnePointAudit13Provider as SqlTwentyOnePointAudit13Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit16Provider"
			
		private SqlTwentyOnePointAudit16Provider innerSqlTwentyOnePointAudit16Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit16"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit16ProviderBase TwentyOnePointAudit16Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit16Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit16Provider == null)
						{
							this.innerSqlTwentyOnePointAudit16Provider = new SqlTwentyOnePointAudit16Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit16Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit16Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit16Provider SqlTwentyOnePointAudit16Provider
		{
			get {return TwentyOnePointAudit16Provider as SqlTwentyOnePointAudit16Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit14Provider"
			
		private SqlTwentyOnePointAudit14Provider innerSqlTwentyOnePointAudit14Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit14"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit14ProviderBase TwentyOnePointAudit14Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit14Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit14Provider == null)
						{
							this.innerSqlTwentyOnePointAudit14Provider = new SqlTwentyOnePointAudit14Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit14Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit14Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit14Provider SqlTwentyOnePointAudit14Provider
		{
			get {return TwentyOnePointAudit14Provider as SqlTwentyOnePointAudit14Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit21Provider"
			
		private SqlTwentyOnePointAudit21Provider innerSqlTwentyOnePointAudit21Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit21"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit21ProviderBase TwentyOnePointAudit21Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit21Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit21Provider == null)
						{
							this.innerSqlTwentyOnePointAudit21Provider = new SqlTwentyOnePointAudit21Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit21Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit21Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit21Provider SqlTwentyOnePointAudit21Provider
		{
			get {return TwentyOnePointAudit21Provider as SqlTwentyOnePointAudit21Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit15Provider"
			
		private SqlTwentyOnePointAudit15Provider innerSqlTwentyOnePointAudit15Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit15"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit15ProviderBase TwentyOnePointAudit15Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit15Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit15Provider == null)
						{
							this.innerSqlTwentyOnePointAudit15Provider = new SqlTwentyOnePointAudit15Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit15Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit15Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit15Provider SqlTwentyOnePointAudit15Provider
		{
			get {return TwentyOnePointAudit15Provider as SqlTwentyOnePointAudit15Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAuditQtrProvider"
			
		private SqlTwentyOnePointAuditQtrProvider innerSqlTwentyOnePointAuditQtrProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAuditQtr"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAuditQtrProviderBase TwentyOnePointAuditQtrProvider
		{
			get
			{
				if (innerSqlTwentyOnePointAuditQtrProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAuditQtrProvider == null)
						{
							this.innerSqlTwentyOnePointAuditQtrProvider = new SqlTwentyOnePointAuditQtrProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAuditQtrProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAuditQtrProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAuditQtrProvider SqlTwentyOnePointAuditQtrProvider
		{
			get {return TwentyOnePointAuditQtrProvider as SqlTwentyOnePointAuditQtrProvider;}
		}
		
		#endregion
		
		
		#region "UsersPrivileged3Provider"
			
		private SqlUsersPrivileged3Provider innerSqlUsersPrivileged3Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersPrivileged3"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersPrivileged3ProviderBase UsersPrivileged3Provider
		{
			get
			{
				if (innerSqlUsersPrivileged3Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersPrivileged3Provider == null)
						{
							this.innerSqlUsersPrivileged3Provider = new SqlUsersPrivileged3Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersPrivileged3Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersPrivileged3Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersPrivileged3Provider SqlUsersPrivileged3Provider
		{
			get {return UsersPrivileged3Provider as SqlUsersPrivileged3Provider;}
		}
		
		#endregion
		
		
		#region "UserPrivilegeProvider"
			
		private SqlUserPrivilegeProvider innerSqlUserPrivilegeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UserPrivilege"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UserPrivilegeProviderBase UserPrivilegeProvider
		{
			get
			{
				if (innerSqlUserPrivilegeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUserPrivilegeProvider == null)
						{
							this.innerSqlUserPrivilegeProvider = new SqlUserPrivilegeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUserPrivilegeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUserPrivilegeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUserPrivilegeProvider SqlUserPrivilegeProvider
		{
			get {return UserPrivilegeProvider as SqlUserPrivilegeProvider;}
		}
		
		#endregion
		
		
		#region "UsersProcurementProvider"
			
		private SqlUsersProcurementProvider innerSqlUsersProcurementProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersProcurement"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProcurementProviderBase UsersProcurementProvider
		{
			get
			{
				if (innerSqlUsersProcurementProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProcurementProvider == null)
						{
							this.innerSqlUsersProcurementProvider = new SqlUsersProcurementProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProcurementProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProcurementProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProcurementProvider SqlUsersProcurementProvider
		{
			get {return UsersProcurementProvider as SqlUsersProcurementProvider;}
		}
		
		#endregion
		
		
		#region "UsersPrivileged2Provider"
			
		private SqlUsersPrivileged2Provider innerSqlUsersPrivileged2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersPrivileged2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersPrivileged2ProviderBase UsersPrivileged2Provider
		{
			get
			{
				if (innerSqlUsersPrivileged2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersPrivileged2Provider == null)
						{
							this.innerSqlUsersPrivileged2Provider = new SqlUsersPrivileged2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersPrivileged2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersPrivileged2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersPrivileged2Provider SqlUsersPrivileged2Provider
		{
			get {return UsersPrivileged2Provider as SqlUsersPrivileged2Provider;}
		}
		
		#endregion
		
		
		#region "UsersProcurementAuditProvider"
			
		private SqlUsersProcurementAuditProvider innerSqlUsersProcurementAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersProcurementAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProcurementAuditProviderBase UsersProcurementAuditProvider
		{
			get
			{
				if (innerSqlUsersProcurementAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProcurementAuditProvider == null)
						{
							this.innerSqlUsersProcurementAuditProvider = new SqlUsersProcurementAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProcurementAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProcurementAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProcurementAuditProvider SqlUsersProcurementAuditProvider
		{
			get {return UsersProcurementAuditProvider as SqlUsersProcurementAuditProvider;}
		}
		
		#endregion
		
		
		#region "UsersAuditProvider"
			
		private SqlUsersAuditProvider innerSqlUsersAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersAuditProviderBase UsersAuditProvider
		{
			get
			{
				if (innerSqlUsersAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersAuditProvider == null)
						{
							this.innerSqlUsersAuditProvider = new SqlUsersAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersAuditProvider SqlUsersAuditProvider
		{
			get {return UsersAuditProvider as SqlUsersAuditProvider;}
		}
		
		#endregion
		
		
		#region "UsersPrivilegedProvider"
			
		private SqlUsersPrivilegedProvider innerSqlUsersPrivilegedProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersPrivileged"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersPrivilegedProviderBase UsersPrivilegedProvider
		{
			get
			{
				if (innerSqlUsersPrivilegedProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersPrivilegedProvider == null)
						{
							this.innerSqlUsersPrivilegedProvider = new SqlUsersPrivilegedProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersPrivilegedProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersPrivilegedProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersPrivilegedProvider SqlUsersPrivilegedProvider
		{
			get {return UsersPrivilegedProvider as SqlUsersPrivilegedProvider;}
		}
		
		#endregion
		
		
		#region "UsersEbiProvider"
			
		private SqlUsersEbiProvider innerSqlUsersEbiProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersEbi"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersEbiProviderBase UsersEbiProvider
		{
			get
			{
				if (innerSqlUsersEbiProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersEbiProvider == null)
						{
							this.innerSqlUsersEbiProvider = new SqlUsersEbiProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersEbiProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersEbiProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersEbiProvider SqlUsersEbiProvider
		{
			get {return UsersEbiProvider as SqlUsersEbiProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit12Provider"
			
		private SqlTwentyOnePointAudit12Provider innerSqlTwentyOnePointAudit12Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit12"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit12ProviderBase TwentyOnePointAudit12Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit12Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit12Provider == null)
						{
							this.innerSqlTwentyOnePointAudit12Provider = new SqlTwentyOnePointAudit12Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit12Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit12Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit12Provider SqlTwentyOnePointAudit12Provider
		{
			get {return TwentyOnePointAudit12Provider as SqlTwentyOnePointAudit12Provider;}
		}
		
		#endregion
		
		
		#region "UsersEbiAuditProvider"
			
		private SqlUsersEbiAuditProvider innerSqlUsersEbiAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersEbiAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersEbiAuditProviderBase UsersEbiAuditProvider
		{
			get
			{
				if (innerSqlUsersEbiAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersEbiAuditProvider == null)
						{
							this.innerSqlUsersEbiAuditProvider = new SqlUsersEbiAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersEbiAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersEbiAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersEbiAuditProvider SqlUsersEbiAuditProvider
		{
			get {return UsersEbiAuditProvider as SqlUsersEbiAuditProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit11Provider"
			
		private SqlTwentyOnePointAudit11Provider innerSqlTwentyOnePointAudit11Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit11"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit11ProviderBase TwentyOnePointAudit11Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit11Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit11Provider == null)
						{
							this.innerSqlTwentyOnePointAudit11Provider = new SqlTwentyOnePointAudit11Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit11Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit11Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit11Provider SqlTwentyOnePointAudit11Provider
		{
			get {return TwentyOnePointAudit11Provider as SqlTwentyOnePointAudit11Provider;}
		}
		
		#endregion
		
		
		#region "SitesProvider"
			
		private SqlSitesProvider innerSqlSitesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Sites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SitesProviderBase SitesProvider
		{
			get
			{
				if (innerSqlSitesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSitesProvider == null)
						{
							this.innerSqlSitesProvider = new SqlSitesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSitesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSitesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSitesProvider SqlSitesProvider
		{
			get {return SitesProvider as SqlSitesProvider;}
		}
		
		#endregion
		
		
		#region "SafetyPlansSeQuestionsProvider"
			
		private SqlSafetyPlansSeQuestionsProvider innerSqlSafetyPlansSeQuestionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SafetyPlansSeQuestions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SafetyPlansSeQuestionsProviderBase SafetyPlansSeQuestionsProvider
		{
			get
			{
				if (innerSqlSafetyPlansSeQuestionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSafetyPlansSeQuestionsProvider == null)
						{
							this.innerSqlSafetyPlansSeQuestionsProvider = new SqlSafetyPlansSeQuestionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSafetyPlansSeQuestionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSafetyPlansSeQuestionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSafetyPlansSeQuestionsProvider SqlSafetyPlansSeQuestionsProvider
		{
			get {return SafetyPlansSeQuestionsProvider as SqlSafetyPlansSeQuestionsProvider;}
		}
		
		#endregion
		
		
		#region "TemplateProvider"
			
		private SqlTemplateProvider innerSqlTemplateProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Template"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TemplateProviderBase TemplateProvider
		{
			get
			{
				if (innerSqlTemplateProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTemplateProvider == null)
						{
							this.innerSqlTemplateProvider = new SqlTemplateProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTemplateProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTemplateProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTemplateProvider SqlTemplateProvider
		{
			get {return TemplateProvider as SqlTemplateProvider;}
		}
		
		#endregion
		
		
		#region "SystemLogProvider"
			
		private SqlSystemLogProvider innerSqlSystemLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SystemLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SystemLogProviderBase SystemLogProvider
		{
			get
			{
				if (innerSqlSystemLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSystemLogProvider == null)
						{
							this.innerSqlSystemLogProvider = new SqlSystemLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSystemLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSystemLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSystemLogProvider SqlSystemLogProvider
		{
			get {return SystemLogProvider as SqlSystemLogProvider;}
		}
		
		#endregion
		
		
		#region "TemplateTypeProvider"
			
		private SqlTemplateTypeProvider innerSqlTemplateTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TemplateType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TemplateTypeProviderBase TemplateTypeProvider
		{
			get
			{
				if (innerSqlTemplateTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTemplateTypeProvider == null)
						{
							this.innerSqlTemplateTypeProvider = new SqlTemplateTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTemplateTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTemplateTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTemplateTypeProvider SqlTemplateTypeProvider
		{
			get {return TemplateTypeProvider as SqlTemplateTypeProvider;}
		}
		
		#endregion
		
		
		#region "SqExemptionAuditProvider"
			
		private SqlSqExemptionAuditProvider innerSqlSqExemptionAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SqExemptionAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SqExemptionAuditProviderBase SqExemptionAuditProvider
		{
			get
			{
				if (innerSqlSqExemptionAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSqExemptionAuditProvider == null)
						{
							this.innerSqlSqExemptionAuditProvider = new SqlSqExemptionAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSqExemptionAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSqExemptionAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSqExemptionAuditProvider SqlSqExemptionAuditProvider
		{
			get {return SqExemptionAuditProvider as SqlSqExemptionAuditProvider;}
		}
		
		#endregion
		
		
		#region "SafetyPlansSeResponsesProvider"
			
		private SqlSafetyPlansSeResponsesProvider innerSqlSafetyPlansSeResponsesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SafetyPlansSeResponses"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SafetyPlansSeResponsesProviderBase SafetyPlansSeResponsesProvider
		{
			get
			{
				if (innerSqlSafetyPlansSeResponsesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSafetyPlansSeResponsesProvider == null)
						{
							this.innerSqlSafetyPlansSeResponsesProvider = new SqlSafetyPlansSeResponsesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSafetyPlansSeResponsesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSafetyPlansSeResponsesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSafetyPlansSeResponsesProvider SqlSafetyPlansSeResponsesProvider
		{
			get {return SafetyPlansSeResponsesProvider as SqlSafetyPlansSeResponsesProvider;}
		}
		
		#endregion
		
		
		#region "SqExemptionProvider"
			
		private SqlSqExemptionProvider innerSqlSqExemptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SqExemption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SqExemptionProviderBase SqExemptionProvider
		{
			get
			{
				if (innerSqlSqExemptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSqExemptionProvider == null)
						{
							this.innerSqlSqExemptionProvider = new SqlSqExemptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSqExemptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSqExemptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSqExemptionProvider SqlSqExemptionProvider
		{
			get {return SqExemptionProvider as SqlSqExemptionProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit10Provider"
			
		private SqlTwentyOnePointAudit10Provider innerSqlTwentyOnePointAudit10Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit10"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit10ProviderBase TwentyOnePointAudit10Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit10Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit10Provider == null)
						{
							this.innerSqlTwentyOnePointAudit10Provider = new SqlTwentyOnePointAudit10Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit10Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit10Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit10Provider SqlTwentyOnePointAudit10Provider
		{
			get {return TwentyOnePointAudit10Provider as SqlTwentyOnePointAudit10Provider;}
		}
		
		#endregion
		
		
		#region "SafetyPlanStatusProvider"
			
		private SqlSafetyPlanStatusProvider innerSqlSafetyPlanStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SafetyPlanStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SafetyPlanStatusProviderBase SafetyPlanStatusProvider
		{
			get
			{
				if (innerSqlSafetyPlanStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSafetyPlanStatusProvider == null)
						{
							this.innerSqlSafetyPlanStatusProvider = new SqlSafetyPlanStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSafetyPlanStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSafetyPlanStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSafetyPlanStatusProvider SqlSafetyPlanStatusProvider
		{
			get {return SafetyPlanStatusProvider as SqlSafetyPlanStatusProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit07Provider"
			
		private SqlTwentyOnePointAudit07Provider innerSqlTwentyOnePointAudit07Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit07"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit07ProviderBase TwentyOnePointAudit07Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit07Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit07Provider == null)
						{
							this.innerSqlTwentyOnePointAudit07Provider = new SqlTwentyOnePointAudit07Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit07Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit07Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit07Provider SqlTwentyOnePointAudit07Provider
		{
			get {return TwentyOnePointAudit07Provider as SqlTwentyOnePointAudit07Provider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireCommentsAuditProvider"
			
		private SqlQuestionnaireCommentsAuditProvider innerSqlQuestionnaireCommentsAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireCommentsAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireCommentsAuditProviderBase QuestionnaireCommentsAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireCommentsAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireCommentsAuditProvider == null)
						{
							this.innerSqlQuestionnaireCommentsAuditProvider = new SqlQuestionnaireCommentsAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireCommentsAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireCommentsAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireCommentsAuditProvider SqlQuestionnaireCommentsAuditProvider
		{
			get {return QuestionnaireCommentsAuditProvider as SqlQuestionnaireCommentsAuditProvider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit08Provider"
			
		private SqlTwentyOnePointAudit08Provider innerSqlTwentyOnePointAudit08Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit08"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit08ProviderBase TwentyOnePointAudit08Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit08Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit08Provider == null)
						{
							this.innerSqlTwentyOnePointAudit08Provider = new SqlTwentyOnePointAudit08Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit08Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit08Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit08Provider SqlTwentyOnePointAudit08Provider
		{
			get {return TwentyOnePointAudit08Provider as SqlTwentyOnePointAudit08Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit01Provider"
			
		private SqlTwentyOnePointAudit01Provider innerSqlTwentyOnePointAudit01Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit01"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit01ProviderBase TwentyOnePointAudit01Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit01Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit01Provider == null)
						{
							this.innerSqlTwentyOnePointAudit01Provider = new SqlTwentyOnePointAudit01Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit01Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit01Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit01Provider SqlTwentyOnePointAudit01Provider
		{
			get {return TwentyOnePointAudit01Provider as SqlTwentyOnePointAudit01Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit09Provider"
			
		private SqlTwentyOnePointAudit09Provider innerSqlTwentyOnePointAudit09Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit09"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit09ProviderBase TwentyOnePointAudit09Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit09Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit09Provider == null)
						{
							this.innerSqlTwentyOnePointAudit09Provider = new SqlTwentyOnePointAudit09Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit09Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit09Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit09Provider SqlTwentyOnePointAudit09Provider
		{
			get {return TwentyOnePointAudit09Provider as SqlTwentyOnePointAudit09Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit02Provider"
			
		private SqlTwentyOnePointAudit02Provider innerSqlTwentyOnePointAudit02Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit02"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit02ProviderBase TwentyOnePointAudit02Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit02Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit02Provider == null)
						{
							this.innerSqlTwentyOnePointAudit02Provider = new SqlTwentyOnePointAudit02Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit02Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit02Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit02Provider SqlTwentyOnePointAudit02Provider
		{
			get {return TwentyOnePointAudit02Provider as SqlTwentyOnePointAudit02Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit06Provider"
			
		private SqlTwentyOnePointAudit06Provider innerSqlTwentyOnePointAudit06Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit06"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit06ProviderBase TwentyOnePointAudit06Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit06Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit06Provider == null)
						{
							this.innerSqlTwentyOnePointAudit06Provider = new SqlTwentyOnePointAudit06Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit06Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit06Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit06Provider SqlTwentyOnePointAudit06Provider
		{
			get {return TwentyOnePointAudit06Provider as SqlTwentyOnePointAudit06Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit05Provider"
			
		private SqlTwentyOnePointAudit05Provider innerSqlTwentyOnePointAudit05Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit05"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit05ProviderBase TwentyOnePointAudit05Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit05Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit05Provider == null)
						{
							this.innerSqlTwentyOnePointAudit05Provider = new SqlTwentyOnePointAudit05Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit05Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit05Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit05Provider SqlTwentyOnePointAudit05Provider
		{
			get {return TwentyOnePointAudit05Provider as SqlTwentyOnePointAudit05Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit04Provider"
			
		private SqlTwentyOnePointAudit04Provider innerSqlTwentyOnePointAudit04Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit04"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit04ProviderBase TwentyOnePointAudit04Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit04Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit04Provider == null)
						{
							this.innerSqlTwentyOnePointAudit04Provider = new SqlTwentyOnePointAudit04Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit04Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit04Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit04Provider SqlTwentyOnePointAudit04Provider
		{
			get {return TwentyOnePointAudit04Provider as SqlTwentyOnePointAudit04Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAudit03Provider"
			
		private SqlTwentyOnePointAudit03Provider innerSqlTwentyOnePointAudit03Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit03"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAudit03ProviderBase TwentyOnePointAudit03Provider
		{
			get
			{
				if (innerSqlTwentyOnePointAudit03Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAudit03Provider == null)
						{
							this.innerSqlTwentyOnePointAudit03Provider = new SqlTwentyOnePointAudit03Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAudit03Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAudit03Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAudit03Provider SqlTwentyOnePointAudit03Provider
		{
			get {return TwentyOnePointAudit03Provider as SqlTwentyOnePointAudit03Provider;}
		}
		
		#endregion
		
		
		#region "TwentyOnePointAuditProvider"
			
		private SqlTwentyOnePointAuditProvider innerSqlTwentyOnePointAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="TwentyOnePointAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override TwentyOnePointAuditProviderBase TwentyOnePointAuditProvider
		{
			get
			{
				if (innerSqlTwentyOnePointAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlTwentyOnePointAuditProvider == null)
						{
							this.innerSqlTwentyOnePointAuditProvider = new SqlTwentyOnePointAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlTwentyOnePointAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlTwentyOnePointAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlTwentyOnePointAuditProvider SqlTwentyOnePointAuditProvider
		{
			get {return TwentyOnePointAuditProvider as SqlTwentyOnePointAuditProvider;}
		}
		
		#endregion
		
		
		#region "ConfigNavigationProvider"
			
		private SqlConfigNavigationProvider innerSqlConfigNavigationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ConfigNavigation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigNavigationProviderBase ConfigNavigationProvider
		{
			get
			{
				if (innerSqlConfigNavigationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigNavigationProvider == null)
						{
							this.innerSqlConfigNavigationProvider = new SqlConfigNavigationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigNavigationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigNavigationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigNavigationProvider SqlConfigNavigationProvider
		{
			get {return ConfigNavigationProvider as SqlConfigNavigationProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireCommentsProvider"
			
		private SqlQuestionnaireCommentsProvider innerSqlQuestionnaireCommentsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireComments"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireCommentsProviderBase QuestionnaireCommentsProvider
		{
			get
			{
				if (innerSqlQuestionnaireCommentsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireCommentsProvider == null)
						{
							this.innerSqlQuestionnaireCommentsProvider = new SqlQuestionnaireCommentsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireCommentsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireCommentsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireCommentsProvider SqlQuestionnaireCommentsProvider
		{
			get {return QuestionnaireCommentsProvider as SqlQuestionnaireCommentsProvider;}
		}
		
		#endregion
		
		
		#region "ConfigSa812Provider"
			
		private SqlConfigSa812Provider innerSqlConfigSa812Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ConfigSa812"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigSa812ProviderBase ConfigSa812Provider
		{
			get
			{
				if (innerSqlConfigSa812Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigSa812Provider == null)
						{
							this.innerSqlConfigSa812Provider = new SqlConfigSa812Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigSa812Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigSa812Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigSa812Provider SqlConfigSa812Provider
		{
			get {return ConfigSa812Provider as SqlConfigSa812Provider;}
		}
		
		#endregion
		
		
		#region "ConfigProvider"
			
		private SqlConfigProvider innerSqlConfigProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Config"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigProviderBase ConfigProvider
		{
			get
			{
				if (innerSqlConfigProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigProvider == null)
						{
							this.innerSqlConfigProvider = new SqlConfigProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigProvider SqlConfigProvider
		{
			get {return ConfigProvider as SqlConfigProvider;}
		}
		
		#endregion
		
		
		#region "ConfigTextTypeProvider"
			
		private SqlConfigTextTypeProvider innerSqlConfigTextTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ConfigTextType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigTextTypeProviderBase ConfigTextTypeProvider
		{
			get
			{
				if (innerSqlConfigTextTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigTextTypeProvider == null)
						{
							this.innerSqlConfigTextTypeProvider = new SqlConfigTextTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigTextTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigTextTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigTextTypeProvider SqlConfigTextTypeProvider
		{
			get {return ConfigTextTypeProvider as SqlConfigTextTypeProvider;}
		}
		
		#endregion
		
		
		#region "CompanyStatusChangeApprovalProvider"
			
		private SqlCompanyStatusChangeApprovalProvider innerSqlCompanyStatusChangeApprovalProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanyStatusChangeApproval"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanyStatusChangeApprovalProviderBase CompanyStatusChangeApprovalProvider
		{
			get
			{
				if (innerSqlCompanyStatusChangeApprovalProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanyStatusChangeApprovalProvider == null)
						{
							this.innerSqlCompanyStatusChangeApprovalProvider = new SqlCompanyStatusChangeApprovalProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanyStatusChangeApprovalProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanyStatusChangeApprovalProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanyStatusChangeApprovalProvider SqlCompanyStatusChangeApprovalProvider
		{
			get {return CompanyStatusChangeApprovalProvider as SqlCompanyStatusChangeApprovalProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryExceptionProvider"
			
		private SqlCompanySiteCategoryExceptionProvider innerSqlCompanySiteCategoryExceptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryException"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryExceptionProviderBase CompanySiteCategoryExceptionProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryExceptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryExceptionProvider == null)
						{
							this.innerSqlCompanySiteCategoryExceptionProvider = new SqlCompanySiteCategoryExceptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryExceptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryExceptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryExceptionProvider SqlCompanySiteCategoryExceptionProvider
		{
			get {return CompanySiteCategoryExceptionProvider as SqlCompanySiteCategoryExceptionProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryStandardAuditProvider"
			
		private SqlCompanySiteCategoryStandardAuditProvider innerSqlCompanySiteCategoryStandardAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryStandardAuditProviderBase CompanySiteCategoryStandardAuditProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryStandardAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryStandardAuditProvider == null)
						{
							this.innerSqlCompanySiteCategoryStandardAuditProvider = new SqlCompanySiteCategoryStandardAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryStandardAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryStandardAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryStandardAuditProvider SqlCompanySiteCategoryStandardAuditProvider
		{
			get {return CompanySiteCategoryStandardAuditProvider as SqlCompanySiteCategoryStandardAuditProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryException2Provider"
			
		private SqlCompanySiteCategoryException2Provider innerSqlCompanySiteCategoryException2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryException2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryException2ProviderBase CompanySiteCategoryException2Provider
		{
			get
			{
				if (innerSqlCompanySiteCategoryException2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryException2Provider == null)
						{
							this.innerSqlCompanySiteCategoryException2Provider = new SqlCompanySiteCategoryException2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryException2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryException2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryException2Provider SqlCompanySiteCategoryException2Provider
		{
			get {return CompanySiteCategoryException2Provider as SqlCompanySiteCategoryException2Provider;}
		}
		
		#endregion
		
		
		#region "ConfigTextProvider"
			
		private SqlConfigTextProvider innerSqlConfigTextProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ConfigText"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigTextProviderBase ConfigTextProvider
		{
			get
			{
				if (innerSqlConfigTextProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigTextProvider == null)
						{
							this.innerSqlConfigTextProvider = new SqlConfigTextProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigTextProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigTextProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigTextProvider SqlConfigTextProvider
		{
			get {return ConfigTextProvider as SqlConfigTextProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryStandardProvider"
			
		private SqlCompanySiteCategoryStandardProvider innerSqlCompanySiteCategoryStandardProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryStandard"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryStandardProviderBase CompanySiteCategoryStandardProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryStandardProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryStandardProvider == null)
						{
							this.innerSqlCompanySiteCategoryStandardProvider = new SqlCompanySiteCategoryStandardProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryStandardProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryStandardProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryStandardProvider SqlCompanySiteCategoryStandardProvider
		{
			get {return CompanySiteCategoryStandardProvider as SqlCompanySiteCategoryStandardProvider;}
		}
		
		#endregion
		
		
		#region "CsmsEmailLogProvider"
			
		private SqlCsmsEmailLogProvider innerSqlCsmsEmailLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsEmailLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsEmailLogProviderBase CsmsEmailLogProvider
		{
			get
			{
				if (innerSqlCsmsEmailLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsEmailLogProvider == null)
						{
							this.innerSqlCsmsEmailLogProvider = new SqlCsmsEmailLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsEmailLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsEmailLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsEmailLogProvider SqlCsmsEmailLogProvider
		{
			get {return CsmsEmailLogProvider as SqlCsmsEmailLogProvider;}
		}
		
		#endregion
		
		
		#region "ConfigText2Provider"
			
		private SqlConfigText2Provider innerSqlConfigText2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ConfigText2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigText2ProviderBase ConfigText2Provider
		{
			get
			{
				if (innerSqlConfigText2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigText2Provider == null)
						{
							this.innerSqlConfigText2Provider = new SqlConfigText2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigText2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigText2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigText2Provider SqlConfigText2Provider
		{
			get {return ConfigText2Provider as SqlConfigText2Provider;}
		}
		
		#endregion
		
		
		#region "CsaProvider"
			
		private SqlCsaProvider innerSqlCsaProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Csa"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsaProviderBase CsaProvider
		{
			get
			{
				if (innerSqlCsaProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsaProvider == null)
						{
							this.innerSqlCsaProvider = new SqlCsaProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsaProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsaProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsaProvider SqlCsaProvider
		{
			get {return CsaProvider as SqlCsaProvider;}
		}
		
		#endregion
		
		
		#region "CsmsFileTypeProvider"
			
		private SqlCsmsFileTypeProvider innerSqlCsmsFileTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsFileType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsFileTypeProviderBase CsmsFileTypeProvider
		{
			get
			{
				if (innerSqlCsmsFileTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsFileTypeProvider == null)
						{
							this.innerSqlCsmsFileTypeProvider = new SqlCsmsFileTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsFileTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsFileTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsFileTypeProvider SqlCsmsFileTypeProvider
		{
			get {return CsmsFileTypeProvider as SqlCsmsFileTypeProvider;}
		}
		
		#endregion
		
		
		#region "CsmsEmailLogRecipientProvider"
			
		private SqlCsmsEmailLogRecipientProvider innerSqlCsmsEmailLogRecipientProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsEmailLogRecipient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsEmailLogRecipientProviderBase CsmsEmailLogRecipientProvider
		{
			get
			{
				if (innerSqlCsmsEmailLogRecipientProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsEmailLogRecipientProvider == null)
						{
							this.innerSqlCsmsEmailLogRecipientProvider = new SqlCsmsEmailLogRecipientProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsEmailLogRecipientProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsEmailLogRecipientProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsEmailLogRecipientProvider SqlCsmsEmailLogRecipientProvider
		{
			get {return CsmsEmailLogRecipientProvider as SqlCsmsEmailLogRecipientProvider;}
		}
		
		#endregion
		
		
		#region "CsmsAccessProvider"
			
		private SqlCsmsAccessProvider innerSqlCsmsAccessProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsAccess"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsAccessProviderBase CsmsAccessProvider
		{
			get
			{
				if (innerSqlCsmsAccessProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsAccessProvider == null)
						{
							this.innerSqlCsmsAccessProvider = new SqlCsmsAccessProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsAccessProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsAccessProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsAccessProvider SqlCsmsAccessProvider
		{
			get {return CsmsAccessProvider as SqlCsmsAccessProvider;}
		}
		
		#endregion
		
		
		#region "ContactsAlcoaProvider"
			
		private SqlContactsAlcoaProvider innerSqlContactsAlcoaProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ContactsAlcoa"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ContactsAlcoaProviderBase ContactsAlcoaProvider
		{
			get
			{
				if (innerSqlContactsAlcoaProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlContactsAlcoaProvider == null)
						{
							this.innerSqlContactsAlcoaProvider = new SqlContactsAlcoaProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlContactsAlcoaProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlContactsAlcoaProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlContactsAlcoaProvider SqlContactsAlcoaProvider
		{
			get {return ContactsAlcoaProvider as SqlContactsAlcoaProvider;}
		}
		
		#endregion
		
		
		#region "CsaAnswersProvider"
			
		private SqlCsaAnswersProvider innerSqlCsaAnswersProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsaAnswers"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsaAnswersProviderBase CsaAnswersProvider
		{
			get
			{
				if (innerSqlCsaAnswersProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsaAnswersProvider == null)
						{
							this.innerSqlCsaAnswersProvider = new SqlCsaAnswersProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsaAnswersProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsaAnswersProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsaAnswersProvider SqlCsaAnswersProvider
		{
			get {return CsaAnswersProvider as SqlCsaAnswersProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryProvider"
			
		private SqlCompanySiteCategoryProvider innerSqlCompanySiteCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryProviderBase CompanySiteCategoryProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryProvider == null)
						{
							this.innerSqlCompanySiteCategoryProvider = new SqlCompanySiteCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryProvider SqlCompanySiteCategoryProvider
		{
			get {return CompanySiteCategoryProvider as SqlCompanySiteCategoryProvider;}
		}
		
		#endregion
		
		
		#region "ContactsContractorsProvider"
			
		private SqlContactsContractorsProvider innerSqlContactsContractorsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ContactsContractors"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ContactsContractorsProviderBase ContactsContractorsProvider
		{
			get
			{
				if (innerSqlContactsContractorsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlContactsContractorsProvider == null)
						{
							this.innerSqlContactsContractorsProvider = new SqlContactsContractorsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlContactsContractorsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlContactsContractorsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlContactsContractorsProvider SqlContactsContractorsProvider
		{
			get {return ContactsContractorsProvider as SqlContactsContractorsProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailTemplateProvider"
			
		private SqlAdminTaskEmailTemplateProvider innerSqlAdminTaskEmailTemplateProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailTemplate"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailTemplateProviderBase AdminTaskEmailTemplateProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailTemplateProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailTemplateProvider == null)
						{
							this.innerSqlAdminTaskEmailTemplateProvider = new SqlAdminTaskEmailTemplateProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailTemplateProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailTemplateProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailTemplateProvider SqlAdminTaskEmailTemplateProvider
		{
			get {return AdminTaskEmailTemplateProvider as SqlAdminTaskEmailTemplateProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesRelationshipProvider"
			
		private SqlCompaniesRelationshipProvider innerSqlCompaniesRelationshipProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesRelationship"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesRelationshipProviderBase CompaniesRelationshipProvider
		{
			get
			{
				if (innerSqlCompaniesRelationshipProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesRelationshipProvider == null)
						{
							this.innerSqlCompaniesRelationshipProvider = new SqlCompaniesRelationshipProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesRelationshipProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesRelationshipProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesRelationshipProvider SqlCompaniesRelationshipProvider
		{
			get {return CompaniesRelationshipProvider as SqlCompaniesRelationshipProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailRecipientProvider"
			
		private SqlAdminTaskEmailRecipientProvider innerSqlAdminTaskEmailRecipientProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailRecipient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailRecipientProviderBase AdminTaskEmailRecipientProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailRecipientProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailRecipientProvider == null)
						{
							this.innerSqlAdminTaskEmailRecipientProvider = new SqlAdminTaskEmailRecipientProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailRecipientProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailRecipientProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailRecipientProvider SqlAdminTaskEmailRecipientProvider
		{
			get {return AdminTaskEmailRecipientProvider as SqlAdminTaskEmailRecipientProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskSourceProvider"
			
		private SqlAdminTaskSourceProvider innerSqlAdminTaskSourceProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskSource"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskSourceProviderBase AdminTaskSourceProvider
		{
			get
			{
				if (innerSqlAdminTaskSourceProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskSourceProvider == null)
						{
							this.innerSqlAdminTaskSourceProvider = new SqlAdminTaskSourceProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskSourceProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskSourceProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskSourceProvider SqlAdminTaskSourceProvider
		{
			get {return AdminTaskSourceProvider as SqlAdminTaskSourceProvider;}
		}
		
		#endregion
		
		
		#region "AdHocRadarProvider"
			
		private SqlAdHocRadarProvider innerSqlAdHocRadarProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdHocRadar"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdHocRadarProviderBase AdHocRadarProvider
		{
			get
			{
				if (innerSqlAdHocRadarProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdHocRadarProvider == null)
						{
							this.innerSqlAdHocRadarProvider = new SqlAdHocRadarProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdHocRadarProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdHocRadarProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdHocRadarProvider SqlAdHocRadarProvider
		{
			get {return AdHocRadarProvider as SqlAdHocRadarProvider;}
		}
		
		#endregion
		
		
		#region "AdHocRadarItems2Provider"
			
		private SqlAdHocRadarItems2Provider innerSqlAdHocRadarItems2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdHocRadarItems2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdHocRadarItems2ProviderBase AdHocRadarItems2Provider
		{
			get
			{
				if (innerSqlAdHocRadarItems2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdHocRadarItems2Provider == null)
						{
							this.innerSqlAdHocRadarItems2Provider = new SqlAdHocRadarItems2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdHocRadarItems2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdHocRadarItems2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdHocRadarItems2Provider SqlAdHocRadarItems2Provider
		{
			get {return AdHocRadarItems2Provider as SqlAdHocRadarItems2Provider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailTemplateRecipientProvider"
			
		private SqlAdminTaskEmailTemplateRecipientProvider innerSqlAdminTaskEmailTemplateRecipientProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateRecipient"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailTemplateRecipientProviderBase AdminTaskEmailTemplateRecipientProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailTemplateRecipientProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailTemplateRecipientProvider == null)
						{
							this.innerSqlAdminTaskEmailTemplateRecipientProvider = new SqlAdminTaskEmailTemplateRecipientProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailTemplateRecipientProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailTemplateRecipientProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailTemplateRecipientProvider SqlAdminTaskEmailTemplateRecipientProvider
		{
			get {return AdminTaskEmailTemplateRecipientProvider as SqlAdminTaskEmailTemplateRecipientProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskStatusProvider"
			
		private SqlAdminTaskStatusProvider innerSqlAdminTaskStatusProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskStatusProviderBase AdminTaskStatusProvider
		{
			get
			{
				if (innerSqlAdminTaskStatusProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskStatusProvider == null)
						{
							this.innerSqlAdminTaskStatusProvider = new SqlAdminTaskStatusProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskStatusProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskStatusProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskStatusProvider SqlAdminTaskStatusProvider
		{
			get {return AdminTaskStatusProvider as SqlAdminTaskStatusProvider;}
		}
		
		#endregion
		
		
		#region "AdHocRadarItemsProvider"
			
		private SqlAdHocRadarItemsProvider innerSqlAdHocRadarItemsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdHocRadarItems"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdHocRadarItemsProviderBase AdHocRadarItemsProvider
		{
			get
			{
				if (innerSqlAdHocRadarItemsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdHocRadarItemsProvider == null)
						{
							this.innerSqlAdHocRadarItemsProvider = new SqlAdHocRadarItemsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdHocRadarItemsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdHocRadarItemsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdHocRadarItemsProvider SqlAdHocRadarItemsProvider
		{
			get {return AdHocRadarItemsProvider as SqlAdHocRadarItemsProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskTypeProvider"
			
		private SqlAdminTaskTypeProvider innerSqlAdminTaskTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskTypeProviderBase AdminTaskTypeProvider
		{
			get
			{
				if (innerSqlAdminTaskTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskTypeProvider == null)
						{
							this.innerSqlAdminTaskTypeProvider = new SqlAdminTaskTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskTypeProvider SqlAdminTaskTypeProvider
		{
			get {return AdminTaskTypeProvider as SqlAdminTaskTypeProvider;}
		}
		
		#endregion
		
		
		#region "AnnualTargetsProvider"
			
		private SqlAnnualTargetsProvider innerSqlAnnualTargetsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AnnualTargets"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AnnualTargetsProviderBase AnnualTargetsProvider
		{
			get
			{
				if (innerSqlAnnualTargetsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAnnualTargetsProvider == null)
						{
							this.innerSqlAnnualTargetsProvider = new SqlAnnualTargetsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAnnualTargetsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAnnualTargetsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAnnualTargetsProvider SqlAnnualTargetsProvider
		{
			get {return AnnualTargetsProvider as SqlAnnualTargetsProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesHrMapProvider"
			
		private SqlCompaniesHrMapProvider innerSqlCompaniesHrMapProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesHrMap"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesHrMapProviderBase CompaniesHrMapProvider
		{
			get
			{
				if (innerSqlCompaniesHrMapProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesHrMapProvider == null)
						{
							this.innerSqlCompaniesHrMapProvider = new SqlCompaniesHrMapProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesHrMapProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesHrMapProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesHrMapProvider SqlCompaniesHrMapProvider
		{
			get {return CompaniesHrMapProvider as SqlCompaniesHrMapProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesEhsimsMapProvider"
			
		private SqlCompaniesEhsimsMapProvider innerSqlCompaniesEhsimsMapProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesEhsimsMap"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesEhsimsMapProviderBase CompaniesEhsimsMapProvider
		{
			get
			{
				if (innerSqlCompaniesEhsimsMapProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesEhsimsMapProvider == null)
						{
							this.innerSqlCompaniesEhsimsMapProvider = new SqlCompaniesEhsimsMapProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesEhsimsMapProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesEhsimsMapProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesEhsimsMapProvider SqlCompaniesEhsimsMapProvider
		{
			get {return CompaniesEhsimsMapProvider as SqlCompaniesEhsimsMapProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskProvider"
			
		private SqlAdminTaskProvider innerSqlAdminTaskProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTask"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskProviderBase AdminTaskProvider
		{
			get
			{
				if (innerSqlAdminTaskProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskProvider == null)
						{
							this.innerSqlAdminTaskProvider = new SqlAdminTaskProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskProvider SqlAdminTaskProvider
		{
			get {return AdminTaskProvider as SqlAdminTaskProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesHrCrpDataProvider"
			
		private SqlCompaniesHrCrpDataProvider innerSqlCompaniesHrCrpDataProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesHrCrpData"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesHrCrpDataProviderBase CompaniesHrCrpDataProvider
		{
			get
			{
				if (innerSqlCompaniesHrCrpDataProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesHrCrpDataProvider == null)
						{
							this.innerSqlCompaniesHrCrpDataProvider = new SqlCompaniesHrCrpDataProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesHrCrpDataProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesHrCrpDataProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesHrCrpDataProvider SqlCompaniesHrCrpDataProvider
		{
			get {return CompaniesHrCrpDataProvider as SqlCompaniesHrCrpDataProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailLogProvider"
			
		private SqlAdminTaskEmailLogProvider innerSqlAdminTaskEmailLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailLogProviderBase AdminTaskEmailLogProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailLogProvider == null)
						{
							this.innerSqlAdminTaskEmailLogProvider = new SqlAdminTaskEmailLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailLogProvider SqlAdminTaskEmailLogProvider
		{
			get {return AdminTaskEmailLogProvider as SqlAdminTaskEmailLogProvider;}
		}
		
		#endregion
		
		
		#region "ApssLogsProvider"
			
		private SqlApssLogsProvider innerSqlApssLogsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ApssLogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ApssLogsProviderBase ApssLogsProvider
		{
			get
			{
				if (innerSqlApssLogsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlApssLogsProvider == null)
						{
							this.innerSqlApssLogsProvider = new SqlApssLogsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlApssLogsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlApssLogsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlApssLogsProvider SqlApssLogsProvider
		{
			get {return ApssLogsProvider as SqlApssLogsProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesAuditProvider"
			
		private SqlCompaniesAuditProvider innerSqlCompaniesAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesAuditProviderBase CompaniesAuditProvider
		{
			get
			{
				if (innerSqlCompaniesAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesAuditProvider == null)
						{
							this.innerSqlCompaniesAuditProvider = new SqlCompaniesAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesAuditProvider SqlCompaniesAuditProvider
		{
			get {return CompaniesAuditProvider as SqlCompaniesAuditProvider;}
		}
		
		#endregion
		
		
		#region "AuditFileVaultProvider"
			
		private SqlAuditFileVaultProvider innerSqlAuditFileVaultProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AuditFileVault"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AuditFileVaultProviderBase AuditFileVaultProvider
		{
			get
			{
				if (innerSqlAuditFileVaultProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAuditFileVaultProvider == null)
						{
							this.innerSqlAuditFileVaultProvider = new SqlAuditFileVaultProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAuditFileVaultProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAuditFileVaultProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAuditFileVaultProvider SqlAuditFileVaultProvider
		{
			get {return AuditFileVaultProvider as SqlAuditFileVaultProvider;}
		}
		
		#endregion
		
		
		#region "CsmsEmailRecipientTypeProvider"
			
		private SqlCsmsEmailRecipientTypeProvider innerSqlCsmsEmailRecipientTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsEmailRecipientType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsEmailRecipientTypeProviderBase CsmsEmailRecipientTypeProvider
		{
			get
			{
				if (innerSqlCsmsEmailRecipientTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsEmailRecipientTypeProvider == null)
						{
							this.innerSqlCsmsEmailRecipientTypeProvider = new SqlCsmsEmailRecipientTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsEmailRecipientTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsEmailRecipientTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsEmailRecipientTypeProvider SqlCsmsEmailRecipientTypeProvider
		{
			get {return CsmsEmailRecipientTypeProvider as SqlCsmsEmailRecipientTypeProvider;}
		}
		
		#endregion
		
		
		#region "AuditFileVaultTableProvider"
			
		private SqlAuditFileVaultTableProvider innerSqlAuditFileVaultTableProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AuditFileVaultTable"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AuditFileVaultTableProviderBase AuditFileVaultTableProvider
		{
			get
			{
				if (innerSqlAuditFileVaultTableProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAuditFileVaultTableProvider == null)
						{
							this.innerSqlAuditFileVaultTableProvider = new SqlAuditFileVaultTableProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAuditFileVaultTableProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAuditFileVaultTableProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAuditFileVaultTableProvider SqlAuditFileVaultTableProvider
		{
			get {return AuditFileVaultTableProvider as SqlAuditFileVaultTableProvider;}
		}
		
		#endregion
		
		
		#region "KpiProvider"
			
		private SqlKpiProvider innerSqlKpiProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Kpi"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiProviderBase KpiProvider
		{
			get
			{
				if (innerSqlKpiProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiProvider == null)
						{
							this.innerSqlKpiProvider = new SqlKpiProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiProvider SqlKpiProvider
		{
			get {return KpiProvider as SqlKpiProvider;}
		}
		
		#endregion
		
		
		#region "CsmsFileProvider"
			
		private SqlCsmsFileProvider innerSqlCsmsFileProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsFile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsFileProviderBase CsmsFileProvider
		{
			get
			{
				if (innerSqlCsmsFileProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsFileProvider == null)
						{
							this.innerSqlCsmsFileProvider = new SqlCsmsFileProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsFileProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsFileProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsFileProvider SqlCsmsFileProvider
		{
			get {return CsmsFileProvider as SqlCsmsFileProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultSubCategoryProvider"
			
		private SqlFileVaultSubCategoryProvider innerSqlFileVaultSubCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultSubCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultSubCategoryProviderBase FileVaultSubCategoryProvider
		{
			get
			{
				if (innerSqlFileVaultSubCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultSubCategoryProvider == null)
						{
							this.innerSqlFileVaultSubCategoryProvider = new SqlFileVaultSubCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultSubCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultSubCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultSubCategoryProvider SqlFileVaultSubCategoryProvider
		{
			get {return FileVaultSubCategoryProvider as SqlFileVaultSubCategoryProvider;}
		}
		
		#endregion
		
		
		#region "KpiAuditProvider"
			
		private SqlKpiAuditProvider innerSqlKpiAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiAuditProviderBase KpiAuditProvider
		{
			get
			{
				if (innerSqlKpiAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiAuditProvider == null)
						{
							this.innerSqlKpiAuditProvider = new SqlKpiAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiAuditProvider SqlKpiAuditProvider
		{
			get {return KpiAuditProvider as SqlKpiAuditProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultCategoryProvider"
			
		private SqlFileVaultCategoryProvider innerSqlFileVaultCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultCategoryProviderBase FileVaultCategoryProvider
		{
			get
			{
				if (innerSqlFileVaultCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultCategoryProvider == null)
						{
							this.innerSqlFileVaultCategoryProvider = new SqlFileVaultCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultCategoryProvider SqlFileVaultCategoryProvider
		{
			get {return FileVaultCategoryProvider as SqlFileVaultCategoryProvider;}
		}
		
		#endregion
		
		
		#region "FileDbMedicalTrainingProvider"
			
		private SqlFileDbMedicalTrainingProvider innerSqlFileDbMedicalTrainingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileDbMedicalTraining"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileDbMedicalTrainingProviderBase FileDbMedicalTrainingProvider
		{
			get
			{
				if (innerSqlFileDbMedicalTrainingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileDbMedicalTrainingProvider == null)
						{
							this.innerSqlFileDbMedicalTrainingProvider = new SqlFileDbMedicalTrainingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileDbMedicalTrainingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileDbMedicalTrainingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileDbMedicalTrainingProvider SqlFileDbMedicalTrainingProvider
		{
			get {return FileDbMedicalTrainingProvider as SqlFileDbMedicalTrainingProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultAuditProvider"
			
		private SqlFileVaultAuditProvider innerSqlFileVaultAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultAuditProviderBase FileVaultAuditProvider
		{
			get
			{
				if (innerSqlFileVaultAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultAuditProvider == null)
						{
							this.innerSqlFileVaultAuditProvider = new SqlFileVaultAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultAuditProvider SqlFileVaultAuditProvider
		{
			get {return FileVaultAuditProvider as SqlFileVaultAuditProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultProvider"
			
		private SqlFileVaultProvider innerSqlFileVaultProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVault"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultProviderBase FileVaultProvider
		{
			get
			{
				if (innerSqlFileVaultProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultProvider == null)
						{
							this.innerSqlFileVaultProvider = new SqlFileVaultProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultProvider SqlFileVaultProvider
		{
			get {return FileVaultProvider as SqlFileVaultProvider;}
		}
		
		#endregion
		
		
		#region "KpiProjectListProvider"
			
		private SqlKpiProjectListProvider innerSqlKpiProjectListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiProjectList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiProjectListProviderBase KpiProjectListProvider
		{
			get
			{
				if (innerSqlKpiProjectListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiProjectListProvider == null)
						{
							this.innerSqlKpiProjectListProvider = new SqlKpiProjectListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiProjectListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiProjectListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiProjectListProvider SqlKpiProjectListProvider
		{
			get {return KpiProjectListProvider as SqlKpiProjectListProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultTableProvider"
			
		private SqlFileVaultTableProvider innerSqlFileVaultTableProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultTable"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultTableProviderBase FileVaultTableProvider
		{
			get
			{
				if (innerSqlFileVaultTableProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultTableProvider == null)
						{
							this.innerSqlFileVaultTableProvider = new SqlFileVaultTableProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultTableProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultTableProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultTableProvider SqlFileVaultTableProvider
		{
			get {return FileVaultTableProvider as SqlFileVaultTableProvider;}
		}
		
		#endregion
		
		
		#region "KpiProjectsProvider"
			
		private SqlKpiProjectsProvider innerSqlKpiProjectsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiProjects"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiProjectsProviderBase KpiProjectsProvider
		{
			get
			{
				if (innerSqlKpiProjectsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiProjectsProvider == null)
						{
							this.innerSqlKpiProjectsProvider = new SqlKpiProjectsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiProjectsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiProjectsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiProjectsProvider SqlKpiProjectsProvider
		{
			get {return KpiProjectsProvider as SqlKpiProjectsProvider;}
		}
		
		#endregion
		
		
		#region "PrivilegeProvider"
			
		private SqlPrivilegeProvider innerSqlPrivilegeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Privilege"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PrivilegeProviderBase PrivilegeProvider
		{
			get
			{
				if (innerSqlPrivilegeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPrivilegeProvider == null)
						{
							this.innerSqlPrivilegeProvider = new SqlPrivilegeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPrivilegeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPrivilegeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPrivilegeProvider SqlPrivilegeProvider
		{
			get {return PrivilegeProvider as SqlPrivilegeProvider;}
		}
		
		#endregion
		
		
		#region "KpiPurchaseOrderListProvider"
			
		private SqlKpiPurchaseOrderListProvider innerSqlKpiPurchaseOrderListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiPurchaseOrderList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiPurchaseOrderListProviderBase KpiPurchaseOrderListProvider
		{
			get
			{
				if (innerSqlKpiPurchaseOrderListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiPurchaseOrderListProvider == null)
						{
							this.innerSqlKpiPurchaseOrderListProvider = new SqlKpiPurchaseOrderListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiPurchaseOrderListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiPurchaseOrderListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiPurchaseOrderListProvider SqlKpiPurchaseOrderListProvider
		{
			get {return KpiPurchaseOrderListProvider as SqlKpiPurchaseOrderListProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireActionLogProvider"
			
		private SqlQuestionnaireActionLogProvider innerSqlQuestionnaireActionLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireActionLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireActionLogProviderBase QuestionnaireActionLogProvider
		{
			get
			{
				if (innerSqlQuestionnaireActionLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireActionLogProvider == null)
						{
							this.innerSqlQuestionnaireActionLogProvider = new SqlQuestionnaireActionLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireActionLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireActionLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireActionLogProvider SqlQuestionnaireActionLogProvider
		{
			get {return QuestionnaireActionLogProvider as SqlQuestionnaireActionLogProvider;}
		}
		
		#endregion
		
		
		#region "PermissionProvider"
			
		private SqlPermissionProvider innerSqlPermissionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Permission"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override PermissionProviderBase PermissionProvider
		{
			get
			{
				if (innerSqlPermissionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlPermissionProvider == null)
						{
							this.innerSqlPermissionProvider = new SqlPermissionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlPermissionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlPermissionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlPermissionProvider SqlPermissionProvider
		{
			get {return PermissionProvider as SqlPermissionProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireAuditProvider"
			
		private SqlQuestionnaireAuditProvider innerSqlQuestionnaireAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireAuditProviderBase QuestionnaireAuditProvider
		{
			get
			{
				if (innerSqlQuestionnaireAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireAuditProvider == null)
						{
							this.innerSqlQuestionnaireAuditProvider = new SqlQuestionnaireAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireAuditProvider SqlQuestionnaireAuditProvider
		{
			get {return QuestionnaireAuditProvider as SqlQuestionnaireAuditProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailTemplateAttachmentProvider"
			
		private SqlAdminTaskEmailTemplateAttachmentProvider innerSqlAdminTaskEmailTemplateAttachmentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateAttachment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailTemplateAttachmentProviderBase AdminTaskEmailTemplateAttachmentProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailTemplateAttachmentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailTemplateAttachmentProvider == null)
						{
							this.innerSqlAdminTaskEmailTemplateAttachmentProvider = new SqlAdminTaskEmailTemplateAttachmentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailTemplateAttachmentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailTemplateAttachmentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailTemplateAttachmentProvider SqlAdminTaskEmailTemplateAttachmentProvider
		{
			get {return AdminTaskEmailTemplateAttachmentProvider as SqlAdminTaskEmailTemplateAttachmentProvider;}
		}
		
		#endregion
		
		
		#region "News2Provider"
			
		private SqlNews2Provider innerSqlNews2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="News2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override News2ProviderBase News2Provider
		{
			get
			{
				if (innerSqlNews2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlNews2Provider == null)
						{
							this.innerSqlNews2Provider = new SqlNews2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlNews2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlNews2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlNews2Provider SqlNews2Provider
		{
			get {return News2Provider as SqlNews2Provider;}
		}
		
		#endregion
		
		
		#region "MonthsProvider"
			
		private SqlMonthsProvider innerSqlMonthsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Months"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override MonthsProviderBase MonthsProvider
		{
			get
			{
				if (innerSqlMonthsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlMonthsProvider == null)
						{
							this.innerSqlMonthsProvider = new SqlMonthsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlMonthsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlMonthsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlMonthsProvider SqlMonthsProvider
		{
			get {return MonthsProvider as SqlMonthsProvider;}
		}
		
		#endregion
		
		
		#region "FileDbAuditProvider"
			
		private SqlFileDbAuditProvider innerSqlFileDbAuditProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileDbAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileDbAuditProviderBase FileDbAuditProvider
		{
			get
			{
				if (innerSqlFileDbAuditProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileDbAuditProvider == null)
						{
							this.innerSqlFileDbAuditProvider = new SqlFileDbAuditProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileDbAuditProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileDbAuditProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileDbAuditProvider SqlFileDbAuditProvider
		{
			get {return FileDbAuditProvider as SqlFileDbAuditProvider;}
		}
		
		#endregion
		
		
		#region "NewsProvider"
			
		private SqlNewsProvider innerSqlNewsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="News"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override NewsProviderBase NewsProvider
		{
			get
			{
				if (innerSqlNewsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlNewsProvider == null)
						{
							this.innerSqlNewsProvider = new SqlNewsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlNewsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlNewsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlNewsProvider SqlNewsProvider
		{
			get {return NewsProvider as SqlNewsProvider;}
		}
		
		#endregion
		
		
		#region "FileDbAdminProvider"
			
		private SqlFileDbAdminProvider innerSqlFileDbAdminProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileDbAdmin"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileDbAdminProviderBase FileDbAdminProvider
		{
			get
			{
				if (innerSqlFileDbAdminProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileDbAdminProvider == null)
						{
							this.innerSqlFileDbAdminProvider = new SqlFileDbAdminProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileDbAdminProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileDbAdminProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileDbAdminProvider SqlFileDbAdminProvider
		{
			get {return FileDbAdminProvider as SqlFileDbAdminProvider;}
		}
		
		#endregion
		
		
		#region "EhsConsultantProvider"
			
		private SqlEhsConsultantProvider innerSqlEhsConsultantProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EhsConsultant"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EhsConsultantProviderBase EhsConsultantProvider
		{
			get
			{
				if (innerSqlEhsConsultantProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEhsConsultantProvider == null)
						{
							this.innerSqlEhsConsultantProvider = new SqlEhsConsultantProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEhsConsultantProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEhsConsultantProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEhsConsultantProvider SqlEhsConsultantProvider
		{
			get {return EhsConsultantProvider as SqlEhsConsultantProvider;}
		}
		
		#endregion
		
		
		#region "EbiIssueProvider"
			
		private SqlEbiIssueProvider innerSqlEbiIssueProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EbiIssue"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiIssueProviderBase EbiIssueProvider
		{
			get
			{
				if (innerSqlEbiIssueProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiIssueProvider == null)
						{
							this.innerSqlEbiIssueProvider = new SqlEbiIssueProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiIssueProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiIssueProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiIssueProvider SqlEbiIssueProvider
		{
			get {return EbiIssueProvider as SqlEbiIssueProvider;}
		}
		
		#endregion
		
		
		#region "EbiDailyReportProvider"
			
		private SqlEbiDailyReportProvider innerSqlEbiDailyReportProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EbiDailyReport"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiDailyReportProviderBase EbiDailyReportProvider
		{
			get
			{
				if (innerSqlEbiDailyReportProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiDailyReportProvider == null)
						{
							this.innerSqlEbiDailyReportProvider = new SqlEbiDailyReportProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiDailyReportProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiDailyReportProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiDailyReportProvider SqlEbiDailyReportProvider
		{
			get {return EbiDailyReportProvider as SqlEbiDailyReportProvider;}
		}
		
		#endregion
		
		
		#region "EbiMetricProvider"
			
		private SqlEbiMetricProvider innerSqlEbiMetricProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EbiMetric"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiMetricProviderBase EbiMetricProvider
		{
			get
			{
				if (innerSqlEbiMetricProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiMetricProvider == null)
						{
							this.innerSqlEbiMetricProvider = new SqlEbiMetricProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiMetricProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiMetricProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiMetricProvider SqlEbiMetricProvider
		{
			get {return EbiMetricProvider as SqlEbiMetricProvider;}
		}
		
		#endregion
		
		
		#region "CustomReportingLayoutsProvider"
			
		private SqlCustomReportingLayoutsProvider innerSqlCustomReportingLayoutsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CustomReportingLayouts"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CustomReportingLayoutsProviderBase CustomReportingLayoutsProvider
		{
			get
			{
				if (innerSqlCustomReportingLayoutsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCustomReportingLayoutsProvider == null)
						{
							this.innerSqlCustomReportingLayoutsProvider = new SqlCustomReportingLayoutsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCustomReportingLayoutsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCustomReportingLayoutsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCustomReportingLayoutsProvider SqlCustomReportingLayoutsProvider
		{
			get {return CustomReportingLayoutsProvider as SqlCustomReportingLayoutsProvider;}
		}
		
		#endregion
		
		
		#region "EbiProvider"
			
		private SqlEbiProvider innerSqlEbiProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Ebi"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiProviderBase EbiProvider
		{
			get
			{
				if (innerSqlEbiProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiProvider == null)
						{
							this.innerSqlEbiProvider = new SqlEbiProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiProvider SqlEbiProvider
		{
			get {return EbiProvider as SqlEbiProvider;}
		}
		
		#endregion
		
		
		#region "DocumentsProvider"
			
		private SqlDocumentsProvider innerSqlDocumentsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="Documents"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DocumentsProviderBase DocumentsProvider
		{
			get
			{
				if (innerSqlDocumentsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDocumentsProvider == null)
						{
							this.innerSqlDocumentsProvider = new SqlDocumentsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDocumentsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDocumentsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDocumentsProvider SqlDocumentsProvider
		{
			get {return DocumentsProvider as SqlDocumentsProvider;}
		}
		
		#endregion
		
		
		#region "EhsimsExceptionsProvider"
			
		private SqlEhsimsExceptionsProvider innerSqlEhsimsExceptionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EhsimsExceptions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EhsimsExceptionsProviderBase EhsimsExceptionsProvider
		{
			get
			{
				if (innerSqlEhsimsExceptionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEhsimsExceptionsProvider == null)
						{
							this.innerSqlEhsimsExceptionsProvider = new SqlEhsimsExceptionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEhsimsExceptionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEhsimsExceptionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEhsimsExceptionsProvider SqlEhsimsExceptionsProvider
		{
			get {return EhsimsExceptionsProvider as SqlEhsimsExceptionsProvider;}
		}
		
		#endregion
		
		
		#region "DocumentsDownloadLogProvider"
			
		private SqlDocumentsDownloadLogProvider innerSqlDocumentsDownloadLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="DocumentsDownloadLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override DocumentsDownloadLogProviderBase DocumentsDownloadLogProvider
		{
			get
			{
				if (innerSqlDocumentsDownloadLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlDocumentsDownloadLogProvider == null)
						{
							this.innerSqlDocumentsDownloadLogProvider = new SqlDocumentsDownloadLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlDocumentsDownloadLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlDocumentsDownloadLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlDocumentsDownloadLogProvider SqlDocumentsDownloadLogProvider
		{
			get {return DocumentsDownloadLogProvider as SqlDocumentsDownloadLogProvider;}
		}
		
		#endregion
		
		
		#region "ElmahErrorProvider"
			
		private SqlElmahErrorProvider innerSqlElmahErrorProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ElmahError"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ElmahErrorProviderBase ElmahErrorProvider
		{
			get
			{
				if (innerSqlElmahErrorProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlElmahErrorProvider == null)
						{
							this.innerSqlElmahErrorProvider = new SqlElmahErrorProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlElmahErrorProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlElmahErrorProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlElmahErrorProvider SqlElmahErrorProvider
		{
			get {return ElmahErrorProvider as SqlElmahErrorProvider;}
		}
		
		#endregion
		
		
		#region "EscalationChainSafetyRolesProvider"
			
		private SqlEscalationChainSafetyRolesProvider innerSqlEscalationChainSafetyRolesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EscalationChainSafetyRoles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EscalationChainSafetyRolesProviderBase EscalationChainSafetyRolesProvider
		{
			get
			{
				if (innerSqlEscalationChainSafetyRolesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEscalationChainSafetyRolesProvider == null)
						{
							this.innerSqlEscalationChainSafetyRolesProvider = new SqlEscalationChainSafetyRolesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEscalationChainSafetyRolesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEscalationChainSafetyRolesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEscalationChainSafetyRolesProvider SqlEscalationChainSafetyRolesProvider
		{
			get {return EscalationChainSafetyRolesProvider as SqlEscalationChainSafetyRolesProvider;}
		}
		
		#endregion
		
		
		#region "EmailLogProvider"
			
		private SqlEmailLogProvider innerSqlEmailLogProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EmailLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EmailLogProviderBase EmailLogProvider
		{
			get
			{
				if (innerSqlEmailLogProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEmailLogProvider == null)
						{
							this.innerSqlEmailLogProvider = new SqlEmailLogProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEmailLogProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEmailLogProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEmailLogProvider SqlEmailLogProvider
		{
			get {return EmailLogProvider as SqlEmailLogProvider;}
		}
		
		#endregion
		
		
		#region "FileDbProvider"
			
		private SqlFileDbProvider innerSqlFileDbProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileDb"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileDbProviderBase FileDbProvider
		{
			get
			{
				if (innerSqlFileDbProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileDbProvider == null)
						{
							this.innerSqlFileDbProvider = new SqlFileDbProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileDbProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileDbProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileDbProvider SqlFileDbProvider
		{
			get {return FileDbProvider as SqlFileDbProvider;}
		}
		
		#endregion
		
		
		#region "EscalationChainSafetyProvider"
			
		private SqlEscalationChainSafetyProvider innerSqlEscalationChainSafetyProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EscalationChainSafety"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EscalationChainSafetyProviderBase EscalationChainSafetyProvider
		{
			get
			{
				if (innerSqlEscalationChainSafetyProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEscalationChainSafetyProvider == null)
						{
							this.innerSqlEscalationChainSafetyProvider = new SqlEscalationChainSafetyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEscalationChainSafetyProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEscalationChainSafetyProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEscalationChainSafetyProvider SqlEscalationChainSafetyProvider
		{
			get {return EscalationChainSafetyProvider as SqlEscalationChainSafetyProvider;}
		}
		
		#endregion
		
		
		#region "EscalationChainProcurementRolesProvider"
			
		private SqlEscalationChainProcurementRolesProvider innerSqlEscalationChainProcurementRolesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EscalationChainProcurementRoles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EscalationChainProcurementRolesProviderBase EscalationChainProcurementRolesProvider
		{
			get
			{
				if (innerSqlEscalationChainProcurementRolesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEscalationChainProcurementRolesProvider == null)
						{
							this.innerSqlEscalationChainProcurementRolesProvider = new SqlEscalationChainProcurementRolesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEscalationChainProcurementRolesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEscalationChainProcurementRolesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEscalationChainProcurementRolesProvider SqlEscalationChainProcurementRolesProvider
		{
			get {return EscalationChainProcurementRolesProvider as SqlEscalationChainProcurementRolesProvider;}
		}
		
		#endregion
		
		
		#region "EmailLogTypeProvider"
			
		private SqlEmailLogTypeProvider innerSqlEmailLogTypeProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EmailLogType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EmailLogTypeProviderBase EmailLogTypeProvider
		{
			get
			{
				if (innerSqlEmailLogTypeProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEmailLogTypeProvider == null)
						{
							this.innerSqlEmailLogTypeProvider = new SqlEmailLogTypeProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEmailLogTypeProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEmailLogTypeProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEmailLogTypeProvider SqlEmailLogTypeProvider
		{
			get {return EmailLogTypeProvider as SqlEmailLogTypeProvider;}
		}
		
		#endregion
		
		
		#region "EscalationChainProcurementProvider"
			
		private SqlEscalationChainProcurementProvider innerSqlEscalationChainProcurementProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EscalationChainProcurement"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EscalationChainProcurementProviderBase EscalationChainProcurementProvider
		{
			get
			{
				if (innerSqlEscalationChainProcurementProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEscalationChainProcurementProvider == null)
						{
							this.innerSqlEscalationChainProcurementProvider = new SqlEscalationChainProcurementProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEscalationChainProcurementProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEscalationChainProcurementProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEscalationChainProcurementProvider SqlEscalationChainProcurementProvider
		{
			get {return EscalationChainProcurementProvider as SqlEscalationChainProcurementProvider;}
		}
		
		#endregion
		
		
		#region "EnumApprovalProvider"
			
		private SqlEnumApprovalProvider innerSqlEnumApprovalProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EnumApproval"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EnumApprovalProviderBase EnumApprovalProvider
		{
			get
			{
				if (innerSqlEnumApprovalProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEnumApprovalProvider == null)
						{
							this.innerSqlEnumApprovalProvider = new SqlEnumApprovalProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEnumApprovalProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEnumApprovalProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEnumApprovalProvider SqlEnumApprovalProvider
		{
			get {return EnumApprovalProvider as SqlEnumApprovalProvider;}
		}
		
		#endregion
		
		
		#region "YearlyTargetsProvider"
			
		private SqlYearlyTargetsProvider innerSqlYearlyTargetsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="YearlyTargets"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override YearlyTargetsProviderBase YearlyTargetsProvider
		{
			get
			{
				if (innerSqlYearlyTargetsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlYearlyTargetsProvider == null)
						{
							this.innerSqlYearlyTargetsProvider = new SqlYearlyTargetsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlYearlyTargetsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlYearlyTargetsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlYearlyTargetsProvider SqlYearlyTargetsProvider
		{
			get {return YearlyTargetsProvider as SqlYearlyTargetsProvider;}
		}
		
		#endregion
		
		
		#region "EnumQuestionnaireRiskRatingProvider"
			
		private SqlEnumQuestionnaireRiskRatingProvider innerSqlEnumQuestionnaireRiskRatingProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EnumQuestionnaireRiskRating"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EnumQuestionnaireRiskRatingProviderBase EnumQuestionnaireRiskRatingProvider
		{
			get
			{
				if (innerSqlEnumQuestionnaireRiskRatingProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEnumQuestionnaireRiskRatingProvider == null)
						{
							this.innerSqlEnumQuestionnaireRiskRatingProvider = new SqlEnumQuestionnaireRiskRatingProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEnumQuestionnaireRiskRatingProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEnumQuestionnaireRiskRatingProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEnumQuestionnaireRiskRatingProvider SqlEnumQuestionnaireRiskRatingProvider
		{
			get {return EnumQuestionnaireRiskRatingProvider as SqlEnumQuestionnaireRiskRatingProvider;}
		}
		
		#endregion
		
		
		
		#region "CurrentCompaniesEhsConsultantIdProvider"
		
		private SqlCurrentCompaniesEhsConsultantIdProvider innerSqlCurrentCompaniesEhsConsultantIdProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CurrentCompaniesEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrentCompaniesEhsConsultantIdProviderBase CurrentCompaniesEhsConsultantIdProvider
		{
			get
			{
				if (innerSqlCurrentCompaniesEhsConsultantIdProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrentCompaniesEhsConsultantIdProvider == null)
						{
							this.innerSqlCurrentCompaniesEhsConsultantIdProvider = new SqlCurrentCompaniesEhsConsultantIdProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrentCompaniesEhsConsultantIdProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrentCompaniesEhsConsultantIdProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrentCompaniesEhsConsultantIdProvider SqlCurrentCompaniesEhsConsultantIdProvider
		{
			get {return CurrentCompaniesEhsConsultantIdProvider as SqlCurrentCompaniesEhsConsultantIdProvider;}
		}
		
		#endregion
		
		
		#region "CurrentQuestionnaireProcurementContactProvider"
		
		private SqlCurrentQuestionnaireProcurementContactProvider innerSqlCurrentQuestionnaireProcurementContactProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CurrentQuestionnaireProcurementContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrentQuestionnaireProcurementContactProviderBase CurrentQuestionnaireProcurementContactProvider
		{
			get
			{
				if (innerSqlCurrentQuestionnaireProcurementContactProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrentQuestionnaireProcurementContactProvider == null)
						{
							this.innerSqlCurrentQuestionnaireProcurementContactProvider = new SqlCurrentQuestionnaireProcurementContactProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrentQuestionnaireProcurementContactProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrentQuestionnaireProcurementContactProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrentQuestionnaireProcurementContactProvider SqlCurrentQuestionnaireProcurementContactProvider
		{
			get {return CurrentQuestionnaireProcurementContactProvider as SqlCurrentQuestionnaireProcurementContactProvider;}
		}
		
		#endregion
		
		
		#region "CurrentQuestionnaireProcurementFunctionalManagerProvider"
		
		private SqlCurrentQuestionnaireProcurementFunctionalManagerProvider innerSqlCurrentQuestionnaireProcurementFunctionalManagerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrentQuestionnaireProcurementFunctionalManagerProviderBase CurrentQuestionnaireProcurementFunctionalManagerProvider
		{
			get
			{
				if (innerSqlCurrentQuestionnaireProcurementFunctionalManagerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrentQuestionnaireProcurementFunctionalManagerProvider == null)
						{
							this.innerSqlCurrentQuestionnaireProcurementFunctionalManagerProvider = new SqlCurrentQuestionnaireProcurementFunctionalManagerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrentQuestionnaireProcurementFunctionalManagerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrentQuestionnaireProcurementFunctionalManagerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrentQuestionnaireProcurementFunctionalManagerProvider SqlCurrentQuestionnaireProcurementFunctionalManagerProvider
		{
			get {return CurrentQuestionnaireProcurementFunctionalManagerProvider as SqlCurrentQuestionnaireProcurementFunctionalManagerProvider;}
		}
		
		#endregion
		
		
		#region "CurrentSmpEhsConsultantIdProvider"
		
		private SqlCurrentSmpEhsConsultantIdProvider innerSqlCurrentSmpEhsConsultantIdProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CurrentSmpEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CurrentSmpEhsConsultantIdProviderBase CurrentSmpEhsConsultantIdProvider
		{
			get
			{
				if (innerSqlCurrentSmpEhsConsultantIdProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCurrentSmpEhsConsultantIdProvider == null)
						{
							this.innerSqlCurrentSmpEhsConsultantIdProvider = new SqlCurrentSmpEhsConsultantIdProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCurrentSmpEhsConsultantIdProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCurrentSmpEhsConsultantIdProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCurrentSmpEhsConsultantIdProvider SqlCurrentSmpEhsConsultantIdProvider
		{
			get {return CurrentSmpEhsConsultantIdProvider as SqlCurrentSmpEhsConsultantIdProvider;}
		}
		
		#endregion
		
		
		#region "OrphanCompaniesEhsConsultantIdProvider"
		
		private SqlOrphanCompaniesEhsConsultantIdProvider innerSqlOrphanCompaniesEhsConsultantIdProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrphanCompaniesEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrphanCompaniesEhsConsultantIdProviderBase OrphanCompaniesEhsConsultantIdProvider
		{
			get
			{
				if (innerSqlOrphanCompaniesEhsConsultantIdProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrphanCompaniesEhsConsultantIdProvider == null)
						{
							this.innerSqlOrphanCompaniesEhsConsultantIdProvider = new SqlOrphanCompaniesEhsConsultantIdProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrphanCompaniesEhsConsultantIdProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrphanCompaniesEhsConsultantIdProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrphanCompaniesEhsConsultantIdProvider SqlOrphanCompaniesEhsConsultantIdProvider
		{
			get {return OrphanCompaniesEhsConsultantIdProvider as SqlOrphanCompaniesEhsConsultantIdProvider;}
		}
		
		#endregion
		
		
		#region "OrphanQuestionnaireProcurementContactProvider"
		
		private SqlOrphanQuestionnaireProcurementContactProvider innerSqlOrphanQuestionnaireProcurementContactProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrphanQuestionnaireProcurementContact"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrphanQuestionnaireProcurementContactProviderBase OrphanQuestionnaireProcurementContactProvider
		{
			get
			{
				if (innerSqlOrphanQuestionnaireProcurementContactProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrphanQuestionnaireProcurementContactProvider == null)
						{
							this.innerSqlOrphanQuestionnaireProcurementContactProvider = new SqlOrphanQuestionnaireProcurementContactProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrphanQuestionnaireProcurementContactProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrphanQuestionnaireProcurementContactProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrphanQuestionnaireProcurementContactProvider SqlOrphanQuestionnaireProcurementContactProvider
		{
			get {return OrphanQuestionnaireProcurementContactProvider as SqlOrphanQuestionnaireProcurementContactProvider;}
		}
		
		#endregion
		
		
		#region "OrphanQuestionnaireProcurementFunctionalManagerProvider"
		
		private SqlOrphanQuestionnaireProcurementFunctionalManagerProvider innerSqlOrphanQuestionnaireProcurementFunctionalManagerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrphanQuestionnaireProcurementFunctionalManagerProviderBase OrphanQuestionnaireProcurementFunctionalManagerProvider
		{
			get
			{
				if (innerSqlOrphanQuestionnaireProcurementFunctionalManagerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrphanQuestionnaireProcurementFunctionalManagerProvider == null)
						{
							this.innerSqlOrphanQuestionnaireProcurementFunctionalManagerProvider = new SqlOrphanQuestionnaireProcurementFunctionalManagerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrphanQuestionnaireProcurementFunctionalManagerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrphanQuestionnaireProcurementFunctionalManagerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrphanQuestionnaireProcurementFunctionalManagerProvider SqlOrphanQuestionnaireProcurementFunctionalManagerProvider
		{
			get {return OrphanQuestionnaireProcurementFunctionalManagerProvider as SqlOrphanQuestionnaireProcurementFunctionalManagerProvider;}
		}
		
		#endregion
		
		
		#region "OrphanSmpEhsConsultantIdProvider"
		
		private SqlOrphanSmpEhsConsultantIdProvider innerSqlOrphanSmpEhsConsultantIdProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="OrphanSmpEhsConsultantId"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override OrphanSmpEhsConsultantIdProviderBase OrphanSmpEhsConsultantIdProvider
		{
			get
			{
				if (innerSqlOrphanSmpEhsConsultantIdProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlOrphanSmpEhsConsultantIdProvider == null)
						{
							this.innerSqlOrphanSmpEhsConsultantIdProvider = new SqlOrphanSmpEhsConsultantIdProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlOrphanSmpEhsConsultantIdProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlOrphanSmpEhsConsultantIdProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlOrphanSmpEhsConsultantIdProvider SqlOrphanSmpEhsConsultantIdProvider
		{
			get {return OrphanSmpEhsConsultantIdProvider as SqlOrphanSmpEhsConsultantIdProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailRecipientsListProvider"
		
		private SqlAdminTaskEmailRecipientsListProvider innerSqlAdminTaskEmailRecipientsListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailRecipientsList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailRecipientsListProviderBase AdminTaskEmailRecipientsListProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailRecipientsListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailRecipientsListProvider == null)
						{
							this.innerSqlAdminTaskEmailRecipientsListProvider = new SqlAdminTaskEmailRecipientsListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailRecipientsListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailRecipientsListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailRecipientsListProvider SqlAdminTaskEmailRecipientsListProvider
		{
			get {return AdminTaskEmailRecipientsListProvider as SqlAdminTaskEmailRecipientsListProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailTemplateListProvider"
		
		private SqlAdminTaskEmailTemplateListProvider innerSqlAdminTaskEmailTemplateListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailTemplateListProviderBase AdminTaskEmailTemplateListProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailTemplateListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailTemplateListProvider == null)
						{
							this.innerSqlAdminTaskEmailTemplateListProvider = new SqlAdminTaskEmailTemplateListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailTemplateListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailTemplateListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailTemplateListProvider SqlAdminTaskEmailTemplateListProvider
		{
			get {return AdminTaskEmailTemplateListProvider as SqlAdminTaskEmailTemplateListProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskEmailTemplateListWithAttachmentsProvider"
		
		private SqlAdminTaskEmailTemplateListWithAttachmentsProvider innerSqlAdminTaskEmailTemplateListWithAttachmentsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskEmailTemplateListWithAttachments"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskEmailTemplateListWithAttachmentsProviderBase AdminTaskEmailTemplateListWithAttachmentsProvider
		{
			get
			{
				if (innerSqlAdminTaskEmailTemplateListWithAttachmentsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskEmailTemplateListWithAttachmentsProvider == null)
						{
							this.innerSqlAdminTaskEmailTemplateListWithAttachmentsProvider = new SqlAdminTaskEmailTemplateListWithAttachmentsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskEmailTemplateListWithAttachmentsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskEmailTemplateListWithAttachmentsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskEmailTemplateListWithAttachmentsProvider SqlAdminTaskEmailTemplateListWithAttachmentsProvider
		{
			get {return AdminTaskEmailTemplateListWithAttachmentsProvider as SqlAdminTaskEmailTemplateListWithAttachmentsProvider;}
		}
		
		#endregion
		
		
		#region "AdminTaskHistoryProvider"
		
		private SqlAdminTaskHistoryProvider innerSqlAdminTaskHistoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="AdminTaskHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override AdminTaskHistoryProviderBase AdminTaskHistoryProvider
		{
			get
			{
				if (innerSqlAdminTaskHistoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlAdminTaskHistoryProvider == null)
						{
							this.innerSqlAdminTaskHistoryProvider = new SqlAdminTaskHistoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlAdminTaskHistoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlAdminTaskHistoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlAdminTaskHistoryProvider SqlAdminTaskHistoryProvider
		{
			get {return AdminTaskHistoryProvider as SqlAdminTaskHistoryProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesActiveProvider"
		
		private SqlCompaniesActiveProvider innerSqlCompaniesActiveProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesActive"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesActiveProviderBase CompaniesActiveProvider
		{
			get
			{
				if (innerSqlCompaniesActiveProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesActiveProvider == null)
						{
							this.innerSqlCompaniesActiveProvider = new SqlCompaniesActiveProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesActiveProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesActiveProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesActiveProvider SqlCompaniesActiveProvider
		{
			get {return CompaniesActiveProvider as SqlCompaniesActiveProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesEhsConsultantsProvider"
		
		private SqlCompaniesEhsConsultantsProvider innerSqlCompaniesEhsConsultantsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesEhsConsultants"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesEhsConsultantsProviderBase CompaniesEhsConsultantsProvider
		{
			get
			{
				if (innerSqlCompaniesEhsConsultantsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesEhsConsultantsProvider == null)
						{
							this.innerSqlCompaniesEhsConsultantsProvider = new SqlCompaniesEhsConsultantsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesEhsConsultantsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesEhsConsultantsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesEhsConsultantsProvider SqlCompaniesEhsConsultantsProvider
		{
			get {return CompaniesEhsConsultantsProvider as SqlCompaniesEhsConsultantsProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesEhsimsMapSitesEhsimsIhsListProvider"
		
		private SqlCompaniesEhsimsMapSitesEhsimsIhsListProvider innerSqlCompaniesEhsimsMapSitesEhsimsIhsListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesEhsimsMapSitesEhsimsIhsList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesEhsimsMapSitesEhsimsIhsListProviderBase CompaniesEhsimsMapSitesEhsimsIhsListProvider
		{
			get
			{
				if (innerSqlCompaniesEhsimsMapSitesEhsimsIhsListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesEhsimsMapSitesEhsimsIhsListProvider == null)
						{
							this.innerSqlCompaniesEhsimsMapSitesEhsimsIhsListProvider = new SqlCompaniesEhsimsMapSitesEhsimsIhsListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesEhsimsMapSitesEhsimsIhsListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesEhsimsMapSitesEhsimsIhsListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesEhsimsMapSitesEhsimsIhsListProvider SqlCompaniesEhsimsMapSitesEhsimsIhsListProvider
		{
			get {return CompaniesEhsimsMapSitesEhsimsIhsListProvider as SqlCompaniesEhsimsMapSitesEhsimsIhsListProvider;}
		}
		
		#endregion
		
		
		#region "CompaniesHrCrpDataListAllProvider"
		
		private SqlCompaniesHrCrpDataListAllProvider innerSqlCompaniesHrCrpDataListAllProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompaniesHrCrpDataListAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompaniesHrCrpDataListAllProviderBase CompaniesHrCrpDataListAllProvider
		{
			get
			{
				if (innerSqlCompaniesHrCrpDataListAllProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompaniesHrCrpDataListAllProvider == null)
						{
							this.innerSqlCompaniesHrCrpDataListAllProvider = new SqlCompaniesHrCrpDataListAllProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompaniesHrCrpDataListAllProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompaniesHrCrpDataListAllProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompaniesHrCrpDataListAllProvider SqlCompaniesHrCrpDataListAllProvider
		{
			get {return CompaniesHrCrpDataListAllProvider as SqlCompaniesHrCrpDataListAllProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryStandardDetailsProvider"
		
		private SqlCompanySiteCategoryStandardDetailsProvider innerSqlCompanySiteCategoryStandardDetailsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardDetails"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryStandardDetailsProviderBase CompanySiteCategoryStandardDetailsProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryStandardDetailsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryStandardDetailsProvider == null)
						{
							this.innerSqlCompanySiteCategoryStandardDetailsProvider = new SqlCompanySiteCategoryStandardDetailsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryStandardDetailsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryStandardDetailsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryStandardDetailsProvider SqlCompanySiteCategoryStandardDetailsProvider
		{
			get {return CompanySiteCategoryStandardDetailsProvider as SqlCompanySiteCategoryStandardDetailsProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryStandardNamesProvider"
		
		private SqlCompanySiteCategoryStandardNamesProvider innerSqlCompanySiteCategoryStandardNamesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardNames"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryStandardNamesProviderBase CompanySiteCategoryStandardNamesProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryStandardNamesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryStandardNamesProvider == null)
						{
							this.innerSqlCompanySiteCategoryStandardNamesProvider = new SqlCompanySiteCategoryStandardNamesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryStandardNamesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryStandardNamesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryStandardNamesProvider SqlCompanySiteCategoryStandardNamesProvider
		{
			get {return CompanySiteCategoryStandardNamesProvider as SqlCompanySiteCategoryStandardNamesProvider;}
		}
		
		#endregion
		
		
		#region "CompanySiteCategoryStandardRegionProvider"
		
		private SqlCompanySiteCategoryStandardRegionProvider innerSqlCompanySiteCategoryStandardRegionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CompanySiteCategoryStandardRegion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CompanySiteCategoryStandardRegionProviderBase CompanySiteCategoryStandardRegionProvider
		{
			get
			{
				if (innerSqlCompanySiteCategoryStandardRegionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCompanySiteCategoryStandardRegionProvider == null)
						{
							this.innerSqlCompanySiteCategoryStandardRegionProvider = new SqlCompanySiteCategoryStandardRegionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCompanySiteCategoryStandardRegionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCompanySiteCategoryStandardRegionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCompanySiteCategoryStandardRegionProvider SqlCompanySiteCategoryStandardRegionProvider
		{
			get {return CompanySiteCategoryStandardRegionProvider as SqlCompanySiteCategoryStandardRegionProvider;}
		}
		
		#endregion
		
		
		#region "ConfigSa812SitesProvider"
		
		private SqlConfigSa812SitesProvider innerSqlConfigSa812SitesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="ConfigSa812Sites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override ConfigSa812SitesProviderBase ConfigSa812SitesProvider
		{
			get
			{
				if (innerSqlConfigSa812SitesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlConfigSa812SitesProvider == null)
						{
							this.innerSqlConfigSa812SitesProvider = new SqlConfigSa812SitesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlConfigSa812SitesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlConfigSa812SitesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlConfigSa812SitesProvider SqlConfigSa812SitesProvider
		{
			get {return ConfigSa812SitesProvider as SqlConfigSa812SitesProvider;}
		}
		
		#endregion
		
		
		#region "CsaCompanySiteCategoryProvider"
		
		private SqlCsaCompanySiteCategoryProvider innerSqlCsaCompanySiteCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsaCompanySiteCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsaCompanySiteCategoryProviderBase CsaCompanySiteCategoryProvider
		{
			get
			{
				if (innerSqlCsaCompanySiteCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsaCompanySiteCategoryProvider == null)
						{
							this.innerSqlCsaCompanySiteCategoryProvider = new SqlCsaCompanySiteCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsaCompanySiteCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsaCompanySiteCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsaCompanySiteCategoryProvider SqlCsaCompanySiteCategoryProvider
		{
			get {return CsaCompanySiteCategoryProvider as SqlCsaCompanySiteCategoryProvider;}
		}
		
		#endregion
		
		
		#region "CsmsEmailLogListProvider"
		
		private SqlCsmsEmailLogListProvider innerSqlCsmsEmailLogListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="CsmsEmailLogList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override CsmsEmailLogListProviderBase CsmsEmailLogListProvider
		{
			get
			{
				if (innerSqlCsmsEmailLogListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlCsmsEmailLogListProvider == null)
						{
							this.innerSqlCsmsEmailLogListProvider = new SqlCsmsEmailLogListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlCsmsEmailLogListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlCsmsEmailLogListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlCsmsEmailLogListProvider SqlCsmsEmailLogListProvider
		{
			get {return CsmsEmailLogListProvider as SqlCsmsEmailLogListProvider;}
		}
		
		#endregion
		
		
		#region "EbiLastTimeOnSiteProvider"
		
		private SqlEbiLastTimeOnSiteProvider innerSqlEbiLastTimeOnSiteProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EbiLastTimeOnSite"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiLastTimeOnSiteProviderBase EbiLastTimeOnSiteProvider
		{
			get
			{
				if (innerSqlEbiLastTimeOnSiteProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiLastTimeOnSiteProvider == null)
						{
							this.innerSqlEbiLastTimeOnSiteProvider = new SqlEbiLastTimeOnSiteProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiLastTimeOnSiteProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiLastTimeOnSiteProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiLastTimeOnSiteProvider SqlEbiLastTimeOnSiteProvider
		{
			get {return EbiLastTimeOnSiteProvider as SqlEbiLastTimeOnSiteProvider;}
		}
		
		#endregion
		
		
		#region "EbiLastTimeOnSiteDistinctProvider"
		
		private SqlEbiLastTimeOnSiteDistinctProvider innerSqlEbiLastTimeOnSiteDistinctProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EbiLastTimeOnSiteDistinct"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiLastTimeOnSiteDistinctProviderBase EbiLastTimeOnSiteDistinctProvider
		{
			get
			{
				if (innerSqlEbiLastTimeOnSiteDistinctProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiLastTimeOnSiteDistinctProvider == null)
						{
							this.innerSqlEbiLastTimeOnSiteDistinctProvider = new SqlEbiLastTimeOnSiteDistinctProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiLastTimeOnSiteDistinctProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiLastTimeOnSiteDistinctProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiLastTimeOnSiteDistinctProvider SqlEbiLastTimeOnSiteDistinctProvider
		{
			get {return EbiLastTimeOnSiteDistinctProvider as SqlEbiLastTimeOnSiteDistinctProvider;}
		}
		
		#endregion
		
		
		#region "EbiLastTimeOnSiteDistinctAnySiteProvider"
		
		private SqlEbiLastTimeOnSiteDistinctAnySiteProvider innerSqlEbiLastTimeOnSiteDistinctAnySiteProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EbiLastTimeOnSiteDistinctAnySite"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EbiLastTimeOnSiteDistinctAnySiteProviderBase EbiLastTimeOnSiteDistinctAnySiteProvider
		{
			get
			{
				if (innerSqlEbiLastTimeOnSiteDistinctAnySiteProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEbiLastTimeOnSiteDistinctAnySiteProvider == null)
						{
							this.innerSqlEbiLastTimeOnSiteDistinctAnySiteProvider = new SqlEbiLastTimeOnSiteDistinctAnySiteProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEbiLastTimeOnSiteDistinctAnySiteProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEbiLastTimeOnSiteDistinctAnySiteProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEbiLastTimeOnSiteDistinctAnySiteProvider SqlEbiLastTimeOnSiteDistinctAnySiteProvider
		{
			get {return EbiLastTimeOnSiteDistinctAnySiteProvider as SqlEbiLastTimeOnSiteDistinctAnySiteProvider;}
		}
		
		#endregion
		
		
		#region "EhsimsExceptionsCompaniesSitesProvider"
		
		private SqlEhsimsExceptionsCompaniesSitesProvider innerSqlEhsimsExceptionsCompaniesSitesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EhsimsExceptionsCompaniesSites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EhsimsExceptionsCompaniesSitesProviderBase EhsimsExceptionsCompaniesSitesProvider
		{
			get
			{
				if (innerSqlEhsimsExceptionsCompaniesSitesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEhsimsExceptionsCompaniesSitesProvider == null)
						{
							this.innerSqlEhsimsExceptionsCompaniesSitesProvider = new SqlEhsimsExceptionsCompaniesSitesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEhsimsExceptionsCompaniesSitesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEhsimsExceptionsCompaniesSitesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEhsimsExceptionsCompaniesSitesProvider SqlEhsimsExceptionsCompaniesSitesProvider
		{
			get {return EhsimsExceptionsCompaniesSitesProvider as SqlEhsimsExceptionsCompaniesSitesProvider;}
		}
		
		#endregion
		
		
		#region "EmailLogAutoProvider"
		
		private SqlEmailLogAutoProvider innerSqlEmailLogAutoProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EmailLogAuto"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EmailLogAutoProviderBase EmailLogAutoProvider
		{
			get
			{
				if (innerSqlEmailLogAutoProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEmailLogAutoProvider == null)
						{
							this.innerSqlEmailLogAutoProvider = new SqlEmailLogAutoProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEmailLogAutoProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEmailLogAutoProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEmailLogAutoProvider SqlEmailLogAutoProvider
		{
			get {return EmailLogAutoProvider as SqlEmailLogAutoProvider;}
		}
		
		#endregion
		
		
		#region "EmailLogIhsProvider"
		
		private SqlEmailLogIhsProvider innerSqlEmailLogIhsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EmailLogIhs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EmailLogIhsProviderBase EmailLogIhsProvider
		{
			get
			{
				if (innerSqlEmailLogIhsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEmailLogIhsProvider == null)
						{
							this.innerSqlEmailLogIhsProvider = new SqlEmailLogIhsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEmailLogIhsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEmailLogIhsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEmailLogIhsProvider SqlEmailLogIhsProvider
		{
			get {return EmailLogIhsProvider as SqlEmailLogIhsProvider;}
		}
		
		#endregion
		
		
		#region "EnumQuestionnaireMainAnswerProvider"
		
		private SqlEnumQuestionnaireMainAnswerProvider innerSqlEnumQuestionnaireMainAnswerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EnumQuestionnaireMainAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EnumQuestionnaireMainAnswerProviderBase EnumQuestionnaireMainAnswerProvider
		{
			get
			{
				if (innerSqlEnumQuestionnaireMainAnswerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEnumQuestionnaireMainAnswerProvider == null)
						{
							this.innerSqlEnumQuestionnaireMainAnswerProvider = new SqlEnumQuestionnaireMainAnswerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEnumQuestionnaireMainAnswerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEnumQuestionnaireMainAnswerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEnumQuestionnaireMainAnswerProvider SqlEnumQuestionnaireMainAnswerProvider
		{
			get {return EnumQuestionnaireMainAnswerProvider as SqlEnumQuestionnaireMainAnswerProvider;}
		}
		
		#endregion
		
		
		#region "EnumQuestionnaireMainQuestionProvider"
		
		private SqlEnumQuestionnaireMainQuestionProvider innerSqlEnumQuestionnaireMainQuestionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EnumQuestionnaireMainQuestion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EnumQuestionnaireMainQuestionProviderBase EnumQuestionnaireMainQuestionProvider
		{
			get
			{
				if (innerSqlEnumQuestionnaireMainQuestionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEnumQuestionnaireMainQuestionProvider == null)
						{
							this.innerSqlEnumQuestionnaireMainQuestionProvider = new SqlEnumQuestionnaireMainQuestionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEnumQuestionnaireMainQuestionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEnumQuestionnaireMainQuestionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEnumQuestionnaireMainQuestionProvider SqlEnumQuestionnaireMainQuestionProvider
		{
			get {return EnumQuestionnaireMainQuestionProvider as SqlEnumQuestionnaireMainQuestionProvider;}
		}
		
		#endregion
		
		
		#region "EnumQuestionnaireMainQuestionAnswerProvider"
		
		private SqlEnumQuestionnaireMainQuestionAnswerProvider innerSqlEnumQuestionnaireMainQuestionAnswerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="EnumQuestionnaireMainQuestionAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override EnumQuestionnaireMainQuestionAnswerProviderBase EnumQuestionnaireMainQuestionAnswerProvider
		{
			get
			{
				if (innerSqlEnumQuestionnaireMainQuestionAnswerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlEnumQuestionnaireMainQuestionAnswerProvider == null)
						{
							this.innerSqlEnumQuestionnaireMainQuestionAnswerProvider = new SqlEnumQuestionnaireMainQuestionAnswerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlEnumQuestionnaireMainQuestionAnswerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlEnumQuestionnaireMainQuestionAnswerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlEnumQuestionnaireMainQuestionAnswerProvider SqlEnumQuestionnaireMainQuestionAnswerProvider
		{
			get {return EnumQuestionnaireMainQuestionAnswerProvider as SqlEnumQuestionnaireMainQuestionAnswerProvider;}
		}
		
		#endregion
		
		
		#region "FileDbUsersCompaniesSitesProvider"
		
		private SqlFileDbUsersCompaniesSitesProvider innerSqlFileDbUsersCompaniesSitesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileDbUsersCompaniesSites"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileDbUsersCompaniesSitesProviderBase FileDbUsersCompaniesSitesProvider
		{
			get
			{
				if (innerSqlFileDbUsersCompaniesSitesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileDbUsersCompaniesSitesProvider == null)
						{
							this.innerSqlFileDbUsersCompaniesSitesProvider = new SqlFileDbUsersCompaniesSitesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileDbUsersCompaniesSitesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileDbUsersCompaniesSitesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileDbUsersCompaniesSitesProvider SqlFileDbUsersCompaniesSitesProvider
		{
			get {return FileDbUsersCompaniesSitesProvider as SqlFileDbUsersCompaniesSitesProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultListProvider"
		
		private SqlFileVaultListProvider innerSqlFileVaultListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultListProviderBase FileVaultListProvider
		{
			get
			{
				if (innerSqlFileVaultListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultListProvider == null)
						{
							this.innerSqlFileVaultListProvider = new SqlFileVaultListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultListProvider SqlFileVaultListProvider
		{
			get {return FileVaultListProvider as SqlFileVaultListProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultListHelpFilesProvider"
		
		private SqlFileVaultListHelpFilesProvider innerSqlFileVaultListHelpFilesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultListHelpFiles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultListHelpFilesProviderBase FileVaultListHelpFilesProvider
		{
			get
			{
				if (innerSqlFileVaultListHelpFilesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultListHelpFilesProvider == null)
						{
							this.innerSqlFileVaultListHelpFilesProvider = new SqlFileVaultListHelpFilesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultListHelpFilesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultListHelpFilesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultListHelpFilesProvider SqlFileVaultListHelpFilesProvider
		{
			get {return FileVaultListHelpFilesProvider as SqlFileVaultListHelpFilesProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultListSqExemptionProvider"
		
		private SqlFileVaultListSqExemptionProvider innerSqlFileVaultListSqExemptionProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultListSqExemption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultListSqExemptionProviderBase FileVaultListSqExemptionProvider
		{
			get
			{
				if (innerSqlFileVaultListSqExemptionProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultListSqExemptionProvider == null)
						{
							this.innerSqlFileVaultListSqExemptionProvider = new SqlFileVaultListSqExemptionProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultListSqExemptionProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultListSqExemptionProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultListSqExemptionProvider SqlFileVaultListSqExemptionProvider
		{
			get {return FileVaultListSqExemptionProvider as SqlFileVaultListSqExemptionProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultTableListProvider"
		
		private SqlFileVaultTableListProvider innerSqlFileVaultTableListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultTableList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultTableListProviderBase FileVaultTableListProvider
		{
			get
			{
				if (innerSqlFileVaultTableListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultTableListProvider == null)
						{
							this.innerSqlFileVaultTableListProvider = new SqlFileVaultTableListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultTableListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultTableListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultTableListProvider SqlFileVaultTableListProvider
		{
			get {return FileVaultTableListProvider as SqlFileVaultTableListProvider;}
		}
		
		#endregion
		
		
		#region "FileVaultTableListHelpFilesProvider"
		
		private SqlFileVaultTableListHelpFilesProvider innerSqlFileVaultTableListHelpFilesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="FileVaultTableListHelpFiles"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override FileVaultTableListHelpFilesProviderBase FileVaultTableListHelpFilesProvider
		{
			get
			{
				if (innerSqlFileVaultTableListHelpFilesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlFileVaultTableListHelpFilesProvider == null)
						{
							this.innerSqlFileVaultTableListHelpFilesProvider = new SqlFileVaultTableListHelpFilesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlFileVaultTableListHelpFilesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlFileVaultTableListHelpFilesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlFileVaultTableListHelpFilesProvider SqlFileVaultTableListHelpFilesProvider
		{
			get {return FileVaultTableListHelpFilesProvider as SqlFileVaultTableListHelpFilesProvider;}
		}
		
		#endregion
		
		
		#region "KpiCompanySiteCategoryProvider"
		
		private SqlKpiCompanySiteCategoryProvider innerSqlKpiCompanySiteCategoryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiCompanySiteCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiCompanySiteCategoryProviderBase KpiCompanySiteCategoryProvider
		{
			get
			{
				if (innerSqlKpiCompanySiteCategoryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiCompanySiteCategoryProvider == null)
						{
							this.innerSqlKpiCompanySiteCategoryProvider = new SqlKpiCompanySiteCategoryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiCompanySiteCategoryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiCompanySiteCategoryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiCompanySiteCategoryProvider SqlKpiCompanySiteCategoryProvider
		{
			get {return KpiCompanySiteCategoryProvider as SqlKpiCompanySiteCategoryProvider;}
		}
		
		#endregion
		
		
		#region "KpiEngineeringProjectHoursProvider"
		
		private SqlKpiEngineeringProjectHoursProvider innerSqlKpiEngineeringProjectHoursProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiEngineeringProjectHours"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiEngineeringProjectHoursProviderBase KpiEngineeringProjectHoursProvider
		{
			get
			{
				if (innerSqlKpiEngineeringProjectHoursProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiEngineeringProjectHoursProvider == null)
						{
							this.innerSqlKpiEngineeringProjectHoursProvider = new SqlKpiEngineeringProjectHoursProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiEngineeringProjectHoursProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiEngineeringProjectHoursProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiEngineeringProjectHoursProvider SqlKpiEngineeringProjectHoursProvider
		{
			get {return KpiEngineeringProjectHoursProvider as SqlKpiEngineeringProjectHoursProvider;}
		}
		
		#endregion
		
		
		#region "KpiListProvider"
		
		private SqlKpiListProvider innerSqlKpiListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiListProviderBase KpiListProvider
		{
			get
			{
				if (innerSqlKpiListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiListProvider == null)
						{
							this.innerSqlKpiListProvider = new SqlKpiListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiListProvider SqlKpiListProvider
		{
			get {return KpiListProvider as SqlKpiListProvider;}
		}
		
		#endregion
		
		
		#region "KpiProjectListOpenProvider"
		
		private SqlKpiProjectListOpenProvider innerSqlKpiProjectListOpenProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiProjectListOpen"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiProjectListOpenProviderBase KpiProjectListOpenProvider
		{
			get
			{
				if (innerSqlKpiProjectListOpenProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiProjectListOpenProvider == null)
						{
							this.innerSqlKpiProjectListOpenProvider = new SqlKpiProjectListOpenProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiProjectListOpenProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiProjectListOpenProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiProjectListOpenProvider SqlKpiProjectListOpenProvider
		{
			get {return KpiProjectListOpenProvider as SqlKpiProjectListOpenProvider;}
		}
		
		#endregion
		
		
		#region "KpiPurchaseOrderListOpenProjectsAscProvider"
		
		private SqlKpiPurchaseOrderListOpenProjectsAscProvider innerSqlKpiPurchaseOrderListOpenProjectsAscProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiPurchaseOrderListOpenProjectsAsc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiPurchaseOrderListOpenProjectsAscProviderBase KpiPurchaseOrderListOpenProjectsAscProvider
		{
			get
			{
				if (innerSqlKpiPurchaseOrderListOpenProjectsAscProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiPurchaseOrderListOpenProjectsAscProvider == null)
						{
							this.innerSqlKpiPurchaseOrderListOpenProjectsAscProvider = new SqlKpiPurchaseOrderListOpenProjectsAscProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiPurchaseOrderListOpenProjectsAscProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiPurchaseOrderListOpenProjectsAscProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiPurchaseOrderListOpenProjectsAscProvider SqlKpiPurchaseOrderListOpenProjectsAscProvider
		{
			get {return KpiPurchaseOrderListOpenProjectsAscProvider as SqlKpiPurchaseOrderListOpenProjectsAscProvider;}
		}
		
		#endregion
		
		
		#region "KpiPurchaseOrderListOpenProjectsDistinctAscProvider"
		
		private SqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider innerSqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override KpiPurchaseOrderListOpenProjectsDistinctAscProviderBase KpiPurchaseOrderListOpenProjectsDistinctAscProvider
		{
			get
			{
				if (innerSqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider == null)
						{
							this.innerSqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider = new SqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider SqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider
		{
			get {return KpiPurchaseOrderListOpenProjectsDistinctAscProvider as SqlKpiPurchaseOrderListOpenProjectsDistinctAscProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireActionLogFriendlyProvider"
		
		private SqlQuestionnaireActionLogFriendlyProvider innerSqlQuestionnaireActionLogFriendlyProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireActionLogFriendly"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireActionLogFriendlyProviderBase QuestionnaireActionLogFriendlyProvider
		{
			get
			{
				if (innerSqlQuestionnaireActionLogFriendlyProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireActionLogFriendlyProvider == null)
						{
							this.innerSqlQuestionnaireActionLogFriendlyProvider = new SqlQuestionnaireActionLogFriendlyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireActionLogFriendlyProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireActionLogFriendlyProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireActionLogFriendlyProvider SqlQuestionnaireActionLogFriendlyProvider
		{
			get {return QuestionnaireActionLogFriendlyProvider as SqlQuestionnaireActionLogFriendlyProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireContactProcurementProvider"
		
		private SqlQuestionnaireContactProcurementProvider innerSqlQuestionnaireContactProcurementProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireContactProcurement"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireContactProcurementProviderBase QuestionnaireContactProcurementProvider
		{
			get
			{
				if (innerSqlQuestionnaireContactProcurementProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireContactProcurementProvider == null)
						{
							this.innerSqlQuestionnaireContactProcurementProvider = new SqlQuestionnaireContactProcurementProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireContactProcurementProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireContactProcurementProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireContactProcurementProvider SqlQuestionnaireContactProcurementProvider
		{
			get {return QuestionnaireContactProcurementProvider as SqlQuestionnaireContactProcurementProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireEscalationHsAssessorListProvider"
		
		private SqlQuestionnaireEscalationHsAssessorListProvider innerSqlQuestionnaireEscalationHsAssessorListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireEscalationHsAssessorList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireEscalationHsAssessorListProviderBase QuestionnaireEscalationHsAssessorListProvider
		{
			get
			{
				if (innerSqlQuestionnaireEscalationHsAssessorListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireEscalationHsAssessorListProvider == null)
						{
							this.innerSqlQuestionnaireEscalationHsAssessorListProvider = new SqlQuestionnaireEscalationHsAssessorListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireEscalationHsAssessorListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireEscalationHsAssessorListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireEscalationHsAssessorListProvider SqlQuestionnaireEscalationHsAssessorListProvider
		{
			get {return QuestionnaireEscalationHsAssessorListProvider as SqlQuestionnaireEscalationHsAssessorListProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireEscalationProcurementListProvider"
		
		private SqlQuestionnaireEscalationProcurementListProvider innerSqlQuestionnaireEscalationProcurementListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireEscalationProcurementList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireEscalationProcurementListProviderBase QuestionnaireEscalationProcurementListProvider
		{
			get
			{
				if (innerSqlQuestionnaireEscalationProcurementListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireEscalationProcurementListProvider == null)
						{
							this.innerSqlQuestionnaireEscalationProcurementListProvider = new SqlQuestionnaireEscalationProcurementListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireEscalationProcurementListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireEscalationProcurementListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireEscalationProcurementListProvider SqlQuestionnaireEscalationProcurementListProvider
		{
			get {return QuestionnaireEscalationProcurementListProvider as SqlQuestionnaireEscalationProcurementListProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireMainQuestionAnswerProvider"
		
		private SqlQuestionnaireMainQuestionAnswerProvider innerSqlQuestionnaireMainQuestionAnswerProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireMainQuestionAnswer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireMainQuestionAnswerProviderBase QuestionnaireMainQuestionAnswerProvider
		{
			get
			{
				if (innerSqlQuestionnaireMainQuestionAnswerProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireMainQuestionAnswerProvider == null)
						{
							this.innerSqlQuestionnaireMainQuestionAnswerProvider = new SqlQuestionnaireMainQuestionAnswerProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireMainQuestionAnswerProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireMainQuestionAnswerProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireMainQuestionAnswerProvider SqlQuestionnaireMainQuestionAnswerProvider
		{
			get {return QuestionnaireMainQuestionAnswerProvider as SqlQuestionnaireMainQuestionAnswerProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnairePresentlyWithUsersActionsProvider"
		
		private SqlQuestionnairePresentlyWithUsersActionsProvider innerSqlQuestionnairePresentlyWithUsersActionsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnairePresentlyWithUsersActions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnairePresentlyWithUsersActionsProviderBase QuestionnairePresentlyWithUsersActionsProvider
		{
			get
			{
				if (innerSqlQuestionnairePresentlyWithUsersActionsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnairePresentlyWithUsersActionsProvider == null)
						{
							this.innerSqlQuestionnairePresentlyWithUsersActionsProvider = new SqlQuestionnairePresentlyWithUsersActionsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnairePresentlyWithUsersActionsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnairePresentlyWithUsersActionsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnairePresentlyWithUsersActionsProvider SqlQuestionnairePresentlyWithUsersActionsProvider
		{
			get {return QuestionnairePresentlyWithUsersActionsProvider as SqlQuestionnairePresentlyWithUsersActionsProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportDaysSinceActivityProvider"
		
		private SqlQuestionnaireReportDaysSinceActivityProvider innerSqlQuestionnaireReportDaysSinceActivityProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportDaysSinceActivity"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportDaysSinceActivityProviderBase QuestionnaireReportDaysSinceActivityProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportDaysSinceActivityProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportDaysSinceActivityProvider == null)
						{
							this.innerSqlQuestionnaireReportDaysSinceActivityProvider = new SqlQuestionnaireReportDaysSinceActivityProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportDaysSinceActivityProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportDaysSinceActivityProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportDaysSinceActivityProvider SqlQuestionnaireReportDaysSinceActivityProvider
		{
			get {return QuestionnaireReportDaysSinceActivityProvider as SqlQuestionnaireReportDaysSinceActivityProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportExpiryProvider"
		
		private SqlQuestionnaireReportExpiryProvider innerSqlQuestionnaireReportExpiryProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportExpiry"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportExpiryProviderBase QuestionnaireReportExpiryProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportExpiryProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportExpiryProvider == null)
						{
							this.innerSqlQuestionnaireReportExpiryProvider = new SqlQuestionnaireReportExpiryProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportExpiryProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportExpiryProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportExpiryProvider SqlQuestionnaireReportExpiryProvider
		{
			get {return QuestionnaireReportExpiryProvider as SqlQuestionnaireReportExpiryProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportOverviewProvider"
		
		private SqlQuestionnaireReportOverviewProvider innerSqlQuestionnaireReportOverviewProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportOverview"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportOverviewProviderBase QuestionnaireReportOverviewProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportOverviewProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportOverviewProvider == null)
						{
							this.innerSqlQuestionnaireReportOverviewProvider = new SqlQuestionnaireReportOverviewProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportOverviewProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportOverviewProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportOverviewProvider SqlQuestionnaireReportOverviewProvider
		{
			get {return QuestionnaireReportOverviewProvider as SqlQuestionnaireReportOverviewProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportOverviewSubQuery1Provider"
		
		private SqlQuestionnaireReportOverviewSubQuery1Provider innerSqlQuestionnaireReportOverviewSubQuery1Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubQuery1"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportOverviewSubQuery1ProviderBase QuestionnaireReportOverviewSubQuery1Provider
		{
			get
			{
				if (innerSqlQuestionnaireReportOverviewSubQuery1Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportOverviewSubQuery1Provider == null)
						{
							this.innerSqlQuestionnaireReportOverviewSubQuery1Provider = new SqlQuestionnaireReportOverviewSubQuery1Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportOverviewSubQuery1Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportOverviewSubQuery1Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportOverviewSubQuery1Provider SqlQuestionnaireReportOverviewSubQuery1Provider
		{
			get {return QuestionnaireReportOverviewSubQuery1Provider as SqlQuestionnaireReportOverviewSubQuery1Provider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportOverviewSubQuery2Provider"
		
		private SqlQuestionnaireReportOverviewSubQuery2Provider innerSqlQuestionnaireReportOverviewSubQuery2Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubQuery2"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportOverviewSubQuery2ProviderBase QuestionnaireReportOverviewSubQuery2Provider
		{
			get
			{
				if (innerSqlQuestionnaireReportOverviewSubQuery2Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportOverviewSubQuery2Provider == null)
						{
							this.innerSqlQuestionnaireReportOverviewSubQuery2Provider = new SqlQuestionnaireReportOverviewSubQuery2Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportOverviewSubQuery2Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportOverviewSubQuery2Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportOverviewSubQuery2Provider SqlQuestionnaireReportOverviewSubQuery2Provider
		{
			get {return QuestionnaireReportOverviewSubQuery2Provider as SqlQuestionnaireReportOverviewSubQuery2Provider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportOverviewSubQuery3Provider"
		
		private SqlQuestionnaireReportOverviewSubQuery3Provider innerSqlQuestionnaireReportOverviewSubQuery3Provider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubQuery3"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportOverviewSubQuery3ProviderBase QuestionnaireReportOverviewSubQuery3Provider
		{
			get
			{
				if (innerSqlQuestionnaireReportOverviewSubQuery3Provider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportOverviewSubQuery3Provider == null)
						{
							this.innerSqlQuestionnaireReportOverviewSubQuery3Provider = new SqlQuestionnaireReportOverviewSubQuery3Provider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportOverviewSubQuery3Provider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportOverviewSubQuery3Provider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportOverviewSubQuery3Provider SqlQuestionnaireReportOverviewSubQuery3Provider
		{
			get {return QuestionnaireReportOverviewSubQuery3Provider as SqlQuestionnaireReportOverviewSubQuery3Provider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportOverviewSubContractorsProvider"
		
		private SqlQuestionnaireReportOverviewSubContractorsProvider innerSqlQuestionnaireReportOverviewSubContractorsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportOverviewSubContractors"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportOverviewSubContractorsProviderBase QuestionnaireReportOverviewSubContractorsProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportOverviewSubContractorsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportOverviewSubContractorsProvider == null)
						{
							this.innerSqlQuestionnaireReportOverviewSubContractorsProvider = new SqlQuestionnaireReportOverviewSubContractorsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportOverviewSubContractorsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportOverviewSubContractorsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportOverviewSubContractorsProvider SqlQuestionnaireReportOverviewSubContractorsProvider
		{
			get {return QuestionnaireReportOverviewSubContractorsProvider as SqlQuestionnaireReportOverviewSubContractorsProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportProgressProvider"
		
		private SqlQuestionnaireReportProgressProvider innerSqlQuestionnaireReportProgressProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportProgress"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportProgressProviderBase QuestionnaireReportProgressProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportProgressProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportProgressProvider == null)
						{
							this.innerSqlQuestionnaireReportProgressProvider = new SqlQuestionnaireReportProgressProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportProgressProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportProgressProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportProgressProvider SqlQuestionnaireReportProgressProvider
		{
			get {return QuestionnaireReportProgressProvider as SqlQuestionnaireReportProgressProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportServicesProvider"
		
		private SqlQuestionnaireReportServicesProvider innerSqlQuestionnaireReportServicesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportServices"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportServicesProviderBase QuestionnaireReportServicesProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportServicesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportServicesProvider == null)
						{
							this.innerSqlQuestionnaireReportServicesProvider = new SqlQuestionnaireReportServicesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportServicesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportServicesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportServicesProvider SqlQuestionnaireReportServicesProvider
		{
			get {return QuestionnaireReportServicesProvider as SqlQuestionnaireReportServicesProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportServicesAllProvider"
		
		private SqlQuestionnaireReportServicesAllProvider innerSqlQuestionnaireReportServicesAllProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportServicesAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportServicesAllProviderBase QuestionnaireReportServicesAllProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportServicesAllProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportServicesAllProvider == null)
						{
							this.innerSqlQuestionnaireReportServicesAllProvider = new SqlQuestionnaireReportServicesAllProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportServicesAllProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportServicesAllProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportServicesAllProvider SqlQuestionnaireReportServicesAllProvider
		{
			get {return QuestionnaireReportServicesAllProvider as SqlQuestionnaireReportServicesAllProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportServicesListProvider"
		
		private SqlQuestionnaireReportServicesListProvider innerSqlQuestionnaireReportServicesListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportServicesList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportServicesListProviderBase QuestionnaireReportServicesListProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportServicesListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportServicesListProvider == null)
						{
							this.innerSqlQuestionnaireReportServicesListProvider = new SqlQuestionnaireReportServicesListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportServicesListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportServicesListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportServicesListProvider SqlQuestionnaireReportServicesListProvider
		{
			get {return QuestionnaireReportServicesListProvider as SqlQuestionnaireReportServicesListProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportServicesListAllProvider"
		
		private SqlQuestionnaireReportServicesListAllProvider innerSqlQuestionnaireReportServicesListAllProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportServicesListAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportServicesListAllProviderBase QuestionnaireReportServicesListAllProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportServicesListAllProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportServicesListAllProvider == null)
						{
							this.innerSqlQuestionnaireReportServicesListAllProvider = new SqlQuestionnaireReportServicesListAllProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportServicesListAllProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportServicesListAllProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportServicesListAllProvider SqlQuestionnaireReportServicesListAllProvider
		{
			get {return QuestionnaireReportServicesListAllProvider as SqlQuestionnaireReportServicesListAllProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportServicesOtherProvider"
		
		private SqlQuestionnaireReportServicesOtherProvider innerSqlQuestionnaireReportServicesOtherProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportServicesOther"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportServicesOtherProviderBase QuestionnaireReportServicesOtherProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportServicesOtherProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportServicesOtherProvider == null)
						{
							this.innerSqlQuestionnaireReportServicesOtherProvider = new SqlQuestionnaireReportServicesOtherProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportServicesOtherProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportServicesOtherProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportServicesOtherProvider SqlQuestionnaireReportServicesOtherProvider
		{
			get {return QuestionnaireReportServicesOtherProvider as SqlQuestionnaireReportServicesOtherProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportServicesOtherAllProvider"
		
		private SqlQuestionnaireReportServicesOtherAllProvider innerSqlQuestionnaireReportServicesOtherAllProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportServicesOtherAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportServicesOtherAllProviderBase QuestionnaireReportServicesOtherAllProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportServicesOtherAllProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportServicesOtherAllProvider == null)
						{
							this.innerSqlQuestionnaireReportServicesOtherAllProvider = new SqlQuestionnaireReportServicesOtherAllProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportServicesOtherAllProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportServicesOtherAllProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportServicesOtherAllProvider SqlQuestionnaireReportServicesOtherAllProvider
		{
			get {return QuestionnaireReportServicesOtherAllProvider as SqlQuestionnaireReportServicesOtherAllProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportTimeDelayAssessmentCompleteProvider"
		
		private SqlQuestionnaireReportTimeDelayAssessmentCompleteProvider innerSqlQuestionnaireReportTimeDelayAssessmentCompleteProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportTimeDelayAssessmentComplete"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportTimeDelayAssessmentCompleteProviderBase QuestionnaireReportTimeDelayAssessmentCompleteProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportTimeDelayAssessmentCompleteProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportTimeDelayAssessmentCompleteProvider == null)
						{
							this.innerSqlQuestionnaireReportTimeDelayAssessmentCompleteProvider = new SqlQuestionnaireReportTimeDelayAssessmentCompleteProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportTimeDelayAssessmentCompleteProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportTimeDelayAssessmentCompleteProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportTimeDelayAssessmentCompleteProvider SqlQuestionnaireReportTimeDelayAssessmentCompleteProvider
		{
			get {return QuestionnaireReportTimeDelayAssessmentCompleteProvider as SqlQuestionnaireReportTimeDelayAssessmentCompleteProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportTimeDelayBeingAssessedProvider"
		
		private SqlQuestionnaireReportTimeDelayBeingAssessedProvider innerSqlQuestionnaireReportTimeDelayBeingAssessedProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportTimeDelayBeingAssessedProviderBase QuestionnaireReportTimeDelayBeingAssessedProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportTimeDelayBeingAssessedProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportTimeDelayBeingAssessedProvider == null)
						{
							this.innerSqlQuestionnaireReportTimeDelayBeingAssessedProvider = new SqlQuestionnaireReportTimeDelayBeingAssessedProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportTimeDelayBeingAssessedProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportTimeDelayBeingAssessedProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportTimeDelayBeingAssessedProvider SqlQuestionnaireReportTimeDelayBeingAssessedProvider
		{
			get {return QuestionnaireReportTimeDelayBeingAssessedProvider as SqlQuestionnaireReportTimeDelayBeingAssessedProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireReportTimeDelayWithSupplierProvider"
		
		private SqlQuestionnaireReportTimeDelayWithSupplierProvider innerSqlQuestionnaireReportTimeDelayWithSupplierProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireReportTimeDelayWithSupplier"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireReportTimeDelayWithSupplierProviderBase QuestionnaireReportTimeDelayWithSupplierProvider
		{
			get
			{
				if (innerSqlQuestionnaireReportTimeDelayWithSupplierProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireReportTimeDelayWithSupplierProvider == null)
						{
							this.innerSqlQuestionnaireReportTimeDelayWithSupplierProvider = new SqlQuestionnaireReportTimeDelayWithSupplierProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireReportTimeDelayWithSupplierProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireReportTimeDelayWithSupplierProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireReportTimeDelayWithSupplierProvider SqlQuestionnaireReportTimeDelayWithSupplierProvider
		{
			get {return QuestionnaireReportTimeDelayWithSupplierProvider as SqlQuestionnaireReportTimeDelayWithSupplierProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireWithLocationApprovalViewProvider"
		
		private SqlQuestionnaireWithLocationApprovalViewProvider innerSqlQuestionnaireWithLocationApprovalViewProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireWithLocationApprovalView"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireWithLocationApprovalViewProviderBase QuestionnaireWithLocationApprovalViewProvider
		{
			get
			{
				if (innerSqlQuestionnaireWithLocationApprovalViewProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireWithLocationApprovalViewProvider == null)
						{
							this.innerSqlQuestionnaireWithLocationApprovalViewProvider = new SqlQuestionnaireWithLocationApprovalViewProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireWithLocationApprovalViewProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireWithLocationApprovalViewProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireWithLocationApprovalViewProvider SqlQuestionnaireWithLocationApprovalViewProvider
		{
			get {return QuestionnaireWithLocationApprovalViewProvider as SqlQuestionnaireWithLocationApprovalViewProvider;}
		}
		
		#endregion
		
		
		#region "QuestionnaireWithLocationApprovalViewLatestProvider"
		
		private SqlQuestionnaireWithLocationApprovalViewLatestProvider innerSqlQuestionnaireWithLocationApprovalViewLatestProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="QuestionnaireWithLocationApprovalViewLatest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override QuestionnaireWithLocationApprovalViewLatestProviderBase QuestionnaireWithLocationApprovalViewLatestProvider
		{
			get
			{
				if (innerSqlQuestionnaireWithLocationApprovalViewLatestProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlQuestionnaireWithLocationApprovalViewLatestProvider == null)
						{
							this.innerSqlQuestionnaireWithLocationApprovalViewLatestProvider = new SqlQuestionnaireWithLocationApprovalViewLatestProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlQuestionnaireWithLocationApprovalViewLatestProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlQuestionnaireWithLocationApprovalViewLatestProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlQuestionnaireWithLocationApprovalViewLatestProvider SqlQuestionnaireWithLocationApprovalViewLatestProvider
		{
			get {return QuestionnaireWithLocationApprovalViewLatestProvider as SqlQuestionnaireWithLocationApprovalViewLatestProvider;}
		}
		
		#endregion
		
		
		#region "SitesEhsimsIhsListProvider"
		
		private SqlSitesEhsimsIhsListProvider innerSqlSitesEhsimsIhsListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SitesEhsimsIhsList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SitesEhsimsIhsListProviderBase SitesEhsimsIhsListProvider
		{
			get
			{
				if (innerSqlSitesEhsimsIhsListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSitesEhsimsIhsListProvider == null)
						{
							this.innerSqlSitesEhsimsIhsListProvider = new SqlSitesEhsimsIhsListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSitesEhsimsIhsListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSitesEhsimsIhsListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSitesEhsimsIhsListProvider SqlSitesEhsimsIhsListProvider
		{
			get {return SitesEhsimsIhsListProvider as SqlSitesEhsimsIhsListProvider;}
		}
		
		#endregion
		
		
		#region "SqExemptionFriendlyAllProvider"
		
		private SqlSqExemptionFriendlyAllProvider innerSqlSqExemptionFriendlyAllProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SqExemptionFriendlyAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SqExemptionFriendlyAllProviderBase SqExemptionFriendlyAllProvider
		{
			get
			{
				if (innerSqlSqExemptionFriendlyAllProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSqExemptionFriendlyAllProvider == null)
						{
							this.innerSqlSqExemptionFriendlyAllProvider = new SqlSqExemptionFriendlyAllProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSqExemptionFriendlyAllProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSqExemptionFriendlyAllProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSqExemptionFriendlyAllProvider SqlSqExemptionFriendlyAllProvider
		{
			get {return SqExemptionFriendlyAllProvider as SqlSqExemptionFriendlyAllProvider;}
		}
		
		#endregion
		
		
		#region "SqExemptionFriendlyCurrentProvider"
		
		private SqlSqExemptionFriendlyCurrentProvider innerSqlSqExemptionFriendlyCurrentProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SqExemptionFriendlyCurrent"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SqExemptionFriendlyCurrentProviderBase SqExemptionFriendlyCurrentProvider
		{
			get
			{
				if (innerSqlSqExemptionFriendlyCurrentProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSqExemptionFriendlyCurrentProvider == null)
						{
							this.innerSqlSqExemptionFriendlyCurrentProvider = new SqlSqExemptionFriendlyCurrentProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSqExemptionFriendlyCurrentProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSqExemptionFriendlyCurrentProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSqExemptionFriendlyCurrentProvider SqlSqExemptionFriendlyCurrentProvider
		{
			get {return SqExemptionFriendlyCurrentProvider as SqlSqExemptionFriendlyCurrentProvider;}
		}
		
		#endregion
		
		
		#region "SqExemptionFriendlyExpiredProvider"
		
		private SqlSqExemptionFriendlyExpiredProvider innerSqlSqExemptionFriendlyExpiredProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="SqExemptionFriendlyExpired"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override SqExemptionFriendlyExpiredProviderBase SqExemptionFriendlyExpiredProvider
		{
			get
			{
				if (innerSqlSqExemptionFriendlyExpiredProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlSqExemptionFriendlyExpiredProvider == null)
						{
							this.innerSqlSqExemptionFriendlyExpiredProvider = new SqlSqExemptionFriendlyExpiredProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlSqExemptionFriendlyExpiredProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlSqExemptionFriendlyExpiredProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlSqExemptionFriendlyExpiredProvider SqlSqExemptionFriendlyExpiredProvider
		{
			get {return SqExemptionFriendlyExpiredProvider as SqlSqExemptionFriendlyExpiredProvider;}
		}
		
		#endregion
		
		
		#region "UsersAlcoanProvider"
		
		private SqlUsersAlcoanProvider innerSqlUsersAlcoanProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersAlcoan"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersAlcoanProviderBase UsersAlcoanProvider
		{
			get
			{
				if (innerSqlUsersAlcoanProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersAlcoanProvider == null)
						{
							this.innerSqlUsersAlcoanProvider = new SqlUsersAlcoanProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersAlcoanProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersAlcoanProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersAlcoanProvider SqlUsersAlcoanProvider
		{
			get {return UsersAlcoanProvider as SqlUsersAlcoanProvider;}
		}
		
		#endregion
		
		
		#region "UsersCompaniesProvider"
		
		private SqlUsersCompaniesProvider innerSqlUsersCompaniesProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersCompanies"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersCompaniesProviderBase UsersCompaniesProvider
		{
			get
			{
				if (innerSqlUsersCompaniesProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersCompaniesProvider == null)
						{
							this.innerSqlUsersCompaniesProvider = new SqlUsersCompaniesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersCompaniesProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersCompaniesProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersCompaniesProvider SqlUsersCompaniesProvider
		{
			get {return UsersCompaniesProvider as SqlUsersCompaniesProvider;}
		}
		
		#endregion
		
		
		#region "UsersCompaniesActiveAllProvider"
		
		private SqlUsersCompaniesActiveAllProvider innerSqlUsersCompaniesActiveAllProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersCompaniesActiveAll"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersCompaniesActiveAllProviderBase UsersCompaniesActiveAllProvider
		{
			get
			{
				if (innerSqlUsersCompaniesActiveAllProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersCompaniesActiveAllProvider == null)
						{
							this.innerSqlUsersCompaniesActiveAllProvider = new SqlUsersCompaniesActiveAllProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersCompaniesActiveAllProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersCompaniesActiveAllProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersCompaniesActiveAllProvider SqlUsersCompaniesActiveAllProvider
		{
			get {return UsersCompaniesActiveAllProvider as SqlUsersCompaniesActiveAllProvider;}
		}
		
		#endregion
		
		
		#region "UsersCompaniesActiveContractorsProvider"
		
		private SqlUsersCompaniesActiveContractorsProvider innerSqlUsersCompaniesActiveContractorsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersCompaniesActiveContractors"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersCompaniesActiveContractorsProviderBase UsersCompaniesActiveContractorsProvider
		{
			get
			{
				if (innerSqlUsersCompaniesActiveContractorsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersCompaniesActiveContractorsProvider == null)
						{
							this.innerSqlUsersCompaniesActiveContractorsProvider = new SqlUsersCompaniesActiveContractorsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersCompaniesActiveContractorsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersCompaniesActiveContractorsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersCompaniesActiveContractorsProvider SqlUsersCompaniesActiveContractorsProvider
		{
			get {return UsersCompaniesActiveContractorsProvider as SqlUsersCompaniesActiveContractorsProvider;}
		}
		
		#endregion
		
		
		#region "UsersEhsConsultantsProvider"
		
		private SqlUsersEhsConsultantsProvider innerSqlUsersEhsConsultantsProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersEhsConsultants"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersEhsConsultantsProviderBase UsersEhsConsultantsProvider
		{
			get
			{
				if (innerSqlUsersEhsConsultantsProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersEhsConsultantsProvider == null)
						{
							this.innerSqlUsersEhsConsultantsProvider = new SqlUsersEhsConsultantsProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersEhsConsultantsProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersEhsConsultantsProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersEhsConsultantsProvider SqlUsersEhsConsultantsProvider
		{
			get {return UsersEhsConsultantsProvider as SqlUsersEhsConsultantsProvider;}
		}
		
		#endregion
		
		
		#region "UsersFullNameProvider"
		
		private SqlUsersFullNameProvider innerSqlUsersFullNameProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersFullName"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersFullNameProviderBase UsersFullNameProvider
		{
			get
			{
				if (innerSqlUsersFullNameProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersFullNameProvider == null)
						{
							this.innerSqlUsersFullNameProvider = new SqlUsersFullNameProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersFullNameProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersFullNameProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersFullNameProvider SqlUsersFullNameProvider
		{
			get {return UsersFullNameProvider as SqlUsersFullNameProvider;}
		}
		
		#endregion
		
		
		#region "UsersHsAssessorListWithLocationProvider"
		
		private SqlUsersHsAssessorListWithLocationProvider innerSqlUsersHsAssessorListWithLocationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersHsAssessorListWithLocation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersHsAssessorListWithLocationProviderBase UsersHsAssessorListWithLocationProvider
		{
			get
			{
				if (innerSqlUsersHsAssessorListWithLocationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersHsAssessorListWithLocationProvider == null)
						{
							this.innerSqlUsersHsAssessorListWithLocationProvider = new SqlUsersHsAssessorListWithLocationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersHsAssessorListWithLocationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersHsAssessorListWithLocationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersHsAssessorListWithLocationProvider SqlUsersHsAssessorListWithLocationProvider
		{
			get {return UsersHsAssessorListWithLocationProvider as SqlUsersHsAssessorListWithLocationProvider;}
		}
		
		#endregion
		
		
		#region "UsersProcurementEscalationListProvider"
		
		private SqlUsersProcurementEscalationListProvider innerSqlUsersProcurementEscalationListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersProcurementEscalationList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProcurementEscalationListProviderBase UsersProcurementEscalationListProvider
		{
			get
			{
				if (innerSqlUsersProcurementEscalationListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProcurementEscalationListProvider == null)
						{
							this.innerSqlUsersProcurementEscalationListProvider = new SqlUsersProcurementEscalationListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProcurementEscalationListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProcurementEscalationListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProcurementEscalationListProvider SqlUsersProcurementEscalationListProvider
		{
			get {return UsersProcurementEscalationListProvider as SqlUsersProcurementEscalationListProvider;}
		}
		
		#endregion
		
		
		#region "UsersProcurementListProvider"
		
		private SqlUsersProcurementListProvider innerSqlUsersProcurementListProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersProcurementList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProcurementListProviderBase UsersProcurementListProvider
		{
			get
			{
				if (innerSqlUsersProcurementListProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProcurementListProvider == null)
						{
							this.innerSqlUsersProcurementListProvider = new SqlUsersProcurementListProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProcurementListProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProcurementListProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProcurementListProvider SqlUsersProcurementListProvider
		{
			get {return UsersProcurementListProvider as SqlUsersProcurementListProvider;}
		}
		
		#endregion
		
		
		#region "UsersProcurementListWithLocationProvider"
		
		private SqlUsersProcurementListWithLocationProvider innerSqlUsersProcurementListWithLocationProvider;

		///<summary>
		/// This class is the Data Access Logic Component for the <see cref="UsersProcurementListWithLocation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		/// <value></value>
		public override UsersProcurementListWithLocationProviderBase UsersProcurementListWithLocationProvider
		{
			get
			{
				if (innerSqlUsersProcurementListWithLocationProvider == null) 
				{
					lock (syncRoot) 
					{
						if (innerSqlUsersProcurementListWithLocationProvider == null)
						{
							this.innerSqlUsersProcurementListWithLocationProvider = new SqlUsersProcurementListWithLocationProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
						}
					}
				}
				return innerSqlUsersProcurementListWithLocationProvider;
			}
		}
		
		/// <summary>
		/// Gets the current <see cref="SqlUsersProcurementListWithLocationProvider"/>.
		/// </summary>
		/// <value></value>
		public SqlUsersProcurementListWithLocationProvider SqlUsersProcurementListWithLocationProvider
		{
			get {return UsersProcurementListWithLocationProvider as SqlUsersProcurementListWithLocationProvider;}
		}
		
		#endregion
		
		
		#region "General data access methods"

		#region "ExecuteNonQuery"
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper);	
			
		}

		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		public override void ExecuteNonQuery(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			database.ExecuteNonQuery(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteNonQuery(commandType, commandText);	
		}
		/// <summary>
		/// Executes the non query.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override int ExecuteNonQuery(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteNonQuery(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataReader"
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteReader(commandWrapper);	
		}

		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteReader(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteReader(commandType, commandText);	
		}
		/// <summary>
		/// Executes the reader.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override IDataReader ExecuteReader(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteReader(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteDataSet"
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteDataSet(commandWrapper);	
		}

		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteDataSet(commandWrapper, transactionManager.TransactionObject);	
		}


		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteDataSet(commandType, commandText);	
		}
		/// <summary>
		/// Executes the data set.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override DataSet ExecuteDataSet(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteDataSet(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#region "ExecuteScalar"
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(string storedProcedureName, params object[] parameterValues)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="storedProcedureName">Name of the stored procedure.</param>
		/// <param name="parameterValues">The parameter values.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, string storedProcedureName, params object[] parameterValues)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(transactionManager.TransactionObject, storedProcedureName, parameterValues);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(DbCommand commandWrapper)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);			
			return database.ExecuteScalar(commandWrapper);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandWrapper">The command wrapper.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, DbCommand commandWrapper)
		{
			Database database = transactionManager.Database;
			return database.ExecuteScalar(commandWrapper, transactionManager.TransactionObject);	
		}

		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(CommandType commandType, string commandText)
		{
			SqlDatabase database = new SqlDatabase(this._connectionString);
			return database.ExecuteScalar(commandType, commandText);	
		}
		/// <summary>
		/// Executes the scalar.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="commandType">Type of the command.</param>
		/// <param name="commandText">The command text.</param>
		/// <returns></returns>
		public override object ExecuteScalar(TransactionManager transactionManager, CommandType commandType, string commandText)
		{
			Database database = transactionManager.Database;			
			return database.ExecuteScalar(transactionManager.TransactionObject , commandType, commandText);				
		}
		#endregion

		#endregion


        #region "KplHelpFilesProvider"

        private SqlKpiHelpFilesProvider innerSqlKpiHelpFilesProvider;

        ///<summary>
        /// This class is the Data Access Logic Component for the <see cref="FileVault"/> business entity.
        /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
        ///</summary>
        /// <value></value>
        public override KpiHelpFilesProviderBase KpiHelpFilesProvider
        {
            get
            {
                if (innerSqlKpiHelpFilesProvider == null)
                {
                    lock (syncRoot)
                    {
                        if (innerSqlKpiHelpFilesProvider == null)
                        {
                            this.innerSqlKpiHelpFilesProvider = new SqlKpiHelpFilesProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
                        }
                    }
                }
                return innerSqlKpiHelpFilesProvider;
            }
        }

        /// <summary>
        /// Gets the current <see cref="SqlFileVaultProvider"/>.
        /// </summary>
        /// <value></value>
        public SqlKpiHelpFilesProvider SqlKpiHelpFilesProvider
        {
            get { return KpiHelpFilesProvider as SqlKpiHelpFilesProvider; }
        }

        #endregion


        #region SqlContractReviewsRegisterProvider


        private SqlContractReviewsRegisterProvider innerContractReviewsRegisterProvider;

        public override ContractReviewsRegisterProviderBase ContractReviewsRegisterProvider
        {
            get
            {
                if (innerContractReviewsRegisterProvider == null)
                {
                    lock (syncRoot)
                    {
                        if (innerContractReviewsRegisterProvider == null)
                        {
                            this.innerContractReviewsRegisterProvider = new SqlContractReviewsRegisterProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
                        }
                    }
                }
                return innerContractReviewsRegisterProvider;
            }
        }

        /// <summary>
        /// Gets the current <see cref="SqlQuestionnaireProvider"/>.
        /// </summary>
        /// <value></value>
        public SqlContractReviewsRegisterProvider SqlContractReviewsRegisterProvider
        {
            get { return SqlContractReviewsRegisterProvider as SqlContractReviewsRegisterProvider; }
        }


        #endregion

        #region SqlUserCompanyProvider


        private SqlUserCompanyProvider innerUserCompanyProvider;

        public override UserCompanyProviderBase UserCompanyProvider
        {
            get
            {
                if (innerUserCompanyProvider == null)
                {
                    lock (syncRoot)
                    {
                        if (innerUserCompanyProvider == null)
                        {
                            this.innerUserCompanyProvider = new SqlUserCompanyProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
                        }
                    }
                }
                return innerUserCompanyProvider;
            }
        }

        /// <summary>
        /// Gets the current <see cref="SqlQuestionnaireProvider"/>.
        /// </summary>
        /// <value></value>
        public SqlUserCompanyProvider SqlUserCompanyProvider
        {
            get { return SqlUserCompanyProvider as SqlUserCompanyProvider; }
        }


        #endregion

        #region SqlUsersCompaniesMapProvider

        private SqlUsersCompaniesMapProvider innerSqlUsersCompaniesMapProvider;

        ///<summary>
        /// This class is the Data Access Logic Component for the <see cref="UsersCompanies"/> business entity.
        /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
        ///</summary>
        /// <value></value>
        public override UsersCompaniesMapProviderBase UsersCompaniesMapProvider
        {
            get
            {
                if (innerSqlUsersCompaniesMapProvider == null)
                {
                    lock (syncRoot)
                    {
                        if (innerSqlUsersCompaniesMapProvider == null)
                        {
                            this.innerSqlUsersCompaniesMapProvider = new SqlUsersCompaniesMapProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
                        }
                    }
                }
                return innerSqlUsersCompaniesMapProvider;
            }
        }

        /// <summary>
        /// Gets the current <see cref="SqlUsersCompaniesProvider"/>.
        /// </summary>
        /// <value></value>
        public SqlUsersCompaniesMapProvider SqlUsersCompaniesMapProvider
        {
            get { return UsersCompaniesMapProvider as SqlUsersCompaniesMapProvider; }
        }

        #endregion

        #region "CompanyNoteProvider"

        private SqlCompanyNoteProvider innerSqlCompanyNoteProvider;

        ///<summary>
        /// This class is the Data Access Logic Component for the <see cref="CompanyNote"/> business entity.
        /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
        ///</summary>
        /// <value></value>
        public override CompanyNoteProviderBase CompanyNoteProvider
        {
            get
            {
                if (innerSqlCompanyNoteProvider == null)
                {
                    lock (syncRoot)
                    {
                        if (innerSqlCompanyNoteProvider == null)
                        {
                            this.innerSqlCompanyNoteProvider = new SqlCompanyNoteProvider(_connectionString, _useStoredProcedure, _providerInvariantName);
                        }
                    }
                }
                return innerSqlCompanyNoteProvider;
            }
        }

        /// <summary>
        /// Gets the current <see cref="SqlCompanyNoteProvider"/>.
        /// </summary>
        /// <value></value>
        public SqlCompanyNoteProvider SqlCompanyNoteProvider
        {
            get { return CompanyNoteProvider as SqlCompanyNoteProvider; }
        }

        #endregion

	}
}
