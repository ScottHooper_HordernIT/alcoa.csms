using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using System.Security.Cryptography;
using System.IO;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using repo = Repo.CSMS.Service.Database;
using model = Repo.CSMS.DAL.EntityModels;

public partial class UserControls_Medical_Upload : System.Web.UI.UserControl
{
    int argCompanyId;
    FileStream fs = null;
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            grid.Visible = false;
            Label1.Text = "";

            ddlCompaniesMain.DataSourceID = "sqldsCompaniesList";
            ddlCompaniesMain.DataTextField = "CompanyName";
            ddlCompaniesMain.DataValueField = "CompanyId";
            ddlCompaniesMain.DataBind();
            ddlCompaniesMain.Items.Add(new ListItem("-----------------------------", "-2"));
            ddlCompaniesMain.Items.Add(new ListItem("All Companies", "-3"));

            ddlCompanies.Text = "< Select Company >";
            ddlCompanies.DataSourceID = "sqldsCompaniesList";
            ddlCompanies.TextField = "CompanyName";
            ddlCompanies.ValueField = "CompanyId";
            ddlCompanies.DataBind();
            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            FillSitesCombo(SessionHandler.spVar_CompanyId);

            int previousYear = DateTime.Now.Year - 1;
            ddlYear.Items.Add(DateTime.Now.Year.ToString());
            ddlYear.Items.Add(previousYear.ToString());

            if(auth.RoleId != (int)RoleList.Contractor)
            {
                GridViewColumn col = grid.Columns["File Name"] as GridViewColumn;
                col.Width = Unit.Pixel(280);
            }
            RegionsService rService = new RegionsService();
            Regions rList = rService.GetByRegionInternalName("WAO");
            

            if (Request.QueryString["VicOps"] != null)
            {
                if (Request.QueryString["VicOps"] == "true")
                {
                    lblMedical.Text = "Victorian Health > Medical";
                     rList = rService.GetByRegionInternalName("VICOPS");
                }
            }

            grid.FilterExpression = String.Format("RegionId = {0}", rList.RegionId);
            repo.IEHSConsultantService ehsConsultantService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEHSConsultantService>();
            model.EHSConsultant ehsConsultant = ehsConsultantService.Get(i => i.UserId == auth.UserId && i.Enabled == true, null);
            bool isEHSConsultant = false;
            if (ehsConsultant != null)
                isEHSConsultant = true;
            if (auth.RoleId == (int)RoleList.Reader && isEHSConsultant == false)
            {
                    ddlCompaniesMain.SelectedValue = "-3";
                    grid.Visible = true;
                    GridViewCommandColumn commandCol = grid.Columns["Action"] as GridViewCommandColumn;
                    commandCol.EditButton.Visible = false;
                    grid.Columns[0].Visible = false;
                    grid.Settings.ShowFilterRow = true;
                    //can view all.
                    GridViewDataComboBoxColumn combo3 = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo3.PropertiesComboBox.DataSourceID = "sqldsCompaniesList";

                    btn1.Visible = false;
                    btnUpload.Visible = false;
            }
            else if (auth.RoleId == (int)RoleList.Contractor)
            {
                 
                    grid.Columns["commandCol"].Visible = false; //hide action column
                    grid.Visible = true;
                    grid.Settings.ShowFilterRow = false;

                    grid.FilterExpression = String.Format("{0} AND CompanyId = {1}", grid.FilterExpression, auth.CompanyId);
                    grid.DataBind();
                    grid.Columns["Company"].Visible = false;

                    ddlCompanies.Enabled = false;
                    ddlCompanies.Value = auth.CompanyId;

                    ddlCompaniesMain.SelectedValue = auth.CompanyId.ToString();
                    ddlCompaniesMain.Enabled = false;
            }
            else if    ( auth.RoleId == (int)RoleList.Administrator || isEHSConsultant)
            {
                    ddlCompaniesMain.SelectedValue = "-3";
                    grid.Visible = true;
                    grid.Settings.ShowFilterRow = true;
                   // grid.Columns["commandCol"].Visible = true; //AG DT 274
                    GridViewDataComboBoxColumn combo2 = grid.Columns["CompanyBox"] as GridViewDataComboBoxColumn;
                    combo2.PropertiesComboBox.DataSourceID = "sqldsCompaniesList";
            }
            else
            { 
                Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
            }
            
        }
        else
        {
            argCompanyId = Convert.ToInt32(ddlCompaniesMain.SelectedValue);
            Label1.Text = "";
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Upload();
    }

    protected void Upload()
    {
        System.Threading.Thread.Sleep(1000);
        string fileName = "";
        if (Page.IsPostBack)
        {
            try
            {
                if (ddlCompanies.SelectedItem == null) throw new Exception("No Company Selected!");
                if (ddeSites.Text == null) throw new Exception("No Site Selected!");

                argCompanyId = (int)ddlCompanies.SelectedItem.Value;
                Label1.Text = "";
                Configuration configuration = new Configuration();
                //******added by Sayani for DT2461
                string tempPath = @configuration.GetValue(ConfigList.TempDir);
               
                //string tempPath = Server.MapPath(FileUpload1.UploadedFiles[0].FileName);
                //*******end of addition

                if (FileUpload1.UploadedFiles != null && FileUpload1.UploadedFiles.Length > 0)
                {
                    if (!String.IsNullOrEmpty(FileUpload1.UploadedFiles[0].FileName) && FileUpload1.UploadedFiles[0].IsValid)
                    {
                        //1. Dump file into dropbox for virus scanning...

                        //********added by Sayani for DT2461
                        fileName = tempPath + FileUpload1.UploadedFiles[0].FileName + GetCurrentDateTimeStamp();
                        //FileUpload1.UploadedFiles[0].SaveAs(tempPath + FileUpload1.UploadedFiles[0].FileName, true);
                        FileUpload1.UploadedFiles[0].SaveAs(fileName, true);


                        //fileName = tempPath;
                        //FileUpload1.UploadedFiles[0].SaveAs(tempPath, true);

                        //***************end of addition

                        Label1.Text = "File Uploaded: " + FileUpload1.UploadedFiles[0].FileName;

                        //FileUpload1.Dispose();
                        //2. Upload into database
                        //*****************Added by Sayani for DT2461
                        //fileName = tempPath + FileUpload1.UploadedFiles[0].FileName;
                        //fileName = tempPath;
                        //**************end of addition
                        fs = File.OpenRead(fileName);
                        int fileLength = Convert.ToInt32(fs.Length);
                        byte[] fileBytes = FileUtilities.Read.ReadFully(fs, 51200);
                        byte[] fileHash = FileUtilities.Calculate.HashFile(fileBytes);
                        
                        TList<Sites> sitesList = ValidateCompanySitesAndReturnSites();

                        foreach (Sites sSelected in sitesList)
                        {
                            FileDbMedicalTrainingService fileDbMedicalTrainingService = new FileDbMedicalTrainingService();
                            FileDbMedicalTraining fileDbMedicalTraining = new FileDbMedicalTraining();

                            //todo - check if hashfile of new file exists in db! i.e. prevent duplicates

                            argCompanyId = (int)ddlCompanies.SelectedItem.Value;
                            fileDbMedicalTraining.CompanyId = argCompanyId;

                            fileDbMedicalTraining.ModifiedByUserId = auth.UserId;
                            fileDbMedicalTraining.ModifiedDate = DateTime.Now;

                            fileDbMedicalTraining.FileName = FileUpload1.UploadedFiles[0].FileName;
                            fileDbMedicalTraining.Description = String.Format("{0} - {1}", ddlYear.SelectedItem.Text, ddlQtr.SelectedItem.Value);

                            fileDbMedicalTraining.Content = fileBytes;
                            fileDbMedicalTraining.ContentLength = fileLength;
                            fileDbMedicalTraining.FileHash = fileHash;
                            fileDbMedicalTraining.Type = "MS";

                            //
                            //Regions rList = rService.GetByRegionInternalName("WAO");

                            //if (Request.QueryString["VicOps"] != null)
                            //{
                            //    if (Request.QueryString["VicOps"] == "true")
                            //    {
                            //        rList = rService.GetByRegionInternalName("VICOPS");
                            //    }
                            //}

                            RegionsService rService = new RegionsService();
                            Regions rWao = rService.GetByRegionInternalName("WAO");
                            Regions rVic = rService.GetByRegionInternalName("VICOPS");

                            RegionsSitesService rsService = new RegionsSitesService();
                            TList<RegionsSites> rsList = rsService.GetBySiteId(sSelected.SiteId);
                            
                            int wao = 0;
                            int vicops = 0;
                            foreach (RegionsSites rs in rsList)
                            {
                                if (rs.RegionId == rWao.RegionId) wao++;
                                if (rs.RegionId == rVic.RegionId) vicops++;
                            }

                            fileDbMedicalTraining.RegionId = rWao.RegionId; //default
                            if (vicops > wao) fileDbMedicalTraining.RegionId = rVic.RegionId;
                            fileDbMedicalTraining.SiteId = sSelected.SiteId;

                            fileDbMedicalTrainingService.Insert(fileDbMedicalTraining);

                            fileDbMedicalTraining.Dispose();
                        }

                        fileLength = 0;
                        fileBytes = null;
                        fileHash = null;
                        fs.Close();
                        System.Threading.Thread.Sleep(1000); //file is locked for a while.
                        File.Delete(fileName);

                        //Label1.Text = "File Uploaded Successfully!";
                        //4. Refresh List
                        grid.DataBind();

                        //5.
                        Response.Write(String.Format("<script>location.href='{0}'</script>", Request.Url));
                    }
                }
                else
                {
                    Label1.Text = "No Upload file specified.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                Label1.Text = "Error: " + ex.Message;
                if (!String.IsNullOrEmpty(fileName))
                {
                    if (fs != null)
                    {
                        fs.Close();
                        File.Delete(fileName);
                    }
                }
            }
        }
    }
    //Added by Pankaj for DT 2461
    private string GetCurrentDateTimeStamp() 
    { 
        string RetVal = string.Empty; 
        RetVal = Convert.ToString(DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year); 
        RetVal = RetVal + DateTime.Now.TimeOfDay.ToString().Replace(":", "").Substring(0, 6); 
        return RetVal; 
    }
    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
        //excel
        Response.Clear();
        ASPxGridViewExporter1.WriteXlsToResponse();
        Response.Close();
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        ASPxGridViewExporter1.WriteRtfToResponse();
        Response.Close();
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        //pdf
        Response.Clear();
        ASPxGridViewExporter1.WritePdfToResponse();
        Response.Close();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Clear();
        ASPxGridViewExporter1.WritePdfToResponse();
        Response.Close();
    }


    protected void grid_CustomDataCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        e.Result = GetNotes(e.Parameters);
    }

    object GetNotes(string id)
    {
        SqlDataSource1.SelectCommand = string.Format("Select Comments FROM [FileDBMedicalTraining] WHERE [FileId]={0}", id);
        SqlDataSource1.DataBind();
        DataView view = (DataView)SqlDataSource1.Select(DataSourceSelectArguments.Empty);
        if (view.Count > 0)
        {
            return view[0]["Comments"].ToString();
        }
        return null;
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
        if (e.Column.FieldName == "SiteId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '-', '0'); s.RemoveItem(-1);}";
        }
    }

    protected void clbSites_Callback(object source, CallbackEventArgsBase e)
    {
        FillSitesCombo(e.Parameter);
        //ddlSites.SelectedIndex = 0;
    }
    protected void FillSitesCombo(string companyId)
    {
        if (string.IsNullOrEmpty(companyId)) return;
        SessionHandler.spVar_CompanyId = companyId;

        SitesService sService = new SitesService();
        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(companyId));

        ASPxListBox lbSites = (ASPxListBox)ddeSites.FindControl("lbSites");
        lbSites.Items.Clear();
        //ddeSites.Text = "";
        int _i = 0;
        int itemsAdded = 0;

        if (Convert.ToInt32(companyId) > 0)
        {
            if (cscsTlist != null)
            {
                if (cscsTlist.Count > 0)
                {
                    lbSites.Items.Add("(Select all)");
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);

                    RegionsService rService = new RegionsService();
                    Regions rList = rService.GetByRegionInternalName("WAO");

                    if (Request.QueryString["VicOps"] != null)
                    {
                        if (Request.QueryString["VicOps"] == "true")
                        {
                            rList = rService.GetByRegionInternalName("VICOPS");
                            itemsAdded++;
                        }
                    }

                    RegionsSitesService rsService = new RegionsSitesService();
                    TList<RegionsSites> rsList = rsService.GetByRegionId(rList.RegionId);

                    foreach (Sites s in sitesList)
                    {
                        RegionsSites rs = rsList.Find(RegionsSitesColumn.SiteId, s.SiteId);
                        if (rs != null)
                        {
                            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                            {
                                if (s.SiteId == cscs.SiteId)
                                {
                                    lbSites.Items.Add(s.SiteName, cscs.SiteId);
                                    itemsAdded++;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (itemsAdded == 0) lbSites.Items.Clear();

        //if (string.IsNullOrEmpty(companyId)) return;
        //SessionHandler.spVar_CompanyId = companyId;
        //ddlSites.DataSourceID = "sqldsResidential_Sites_ByCompany";
        //ddlSites.TextField = "SiteName";
        //ddlSites.ValueField = "SiteId";
        //sqldsResidential_Sites_ByCompany.SelectParameters[0].DefaultValue = companyId;
        //sqldsResidential_Sites_ByCompany.DataBind();
        //ddlSites.DataBind();
    }
    protected void ddlCompanies_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSitesCombo(ddlCompanies.SelectedItem.Value.ToString());
    }

    protected TList<Sites> ValidateCompanySitesAndReturnSites()
    {
        TList<Sites> SitesList = new TList<Sites>();

        int CompanyId = 0;
        Int32.TryParse(SessionHandler.spVar_CompanyId, out CompanyId);

        if (CompanyId == 0) throw new Exception("Company Not Selected");

        string[] sites = ddeSites.Text.Split(';');

        CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
        TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(Convert.ToInt32(CompanyId));

        SitesFilters sitesFilters = new SitesFilters();
        sitesFilters.Append(SitesColumn.IsVisible, "1");
        int _i = 0;
        TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
        if (_i == 0) throw new Exception("Error occurred whilst validating Sites. Please try again.");

        RegionsService rService = new RegionsService();
        Regions rList = rService.GetByRegionInternalName("WAO");

        if (Request.QueryString["VicOps"] != null)
        {
            if (Request.QueryString["VicOps"] == "true")
            {
                rList = rService.GetByRegionInternalName("VICOPS");
            }
        }

        RegionsSitesService rsService = new RegionsSitesService();
        TList<RegionsSites> rsList = rsService.GetByRegionId(rList.RegionId);

        foreach (Sites s in sitesList)
        {
            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
            {
                RegionsSites rs = rsList.Find(RegionsSitesColumn.SiteId, s.SiteId);
                if (rs != null)
                {
                    if (s.SiteId == cscs.SiteId)
                    {
                        foreach (string site in sites)
                        {
                            if (s.SiteName == site)
                            {
                                Label1.Text += s.SiteId + " + ";
                                SitesList.Add(s);
                            }
                        }
                    }
                }
            }
        }
        if (SitesList.Count == 0) throw new Exception("No Sites Selected!");

        return SitesList;
    }

}
