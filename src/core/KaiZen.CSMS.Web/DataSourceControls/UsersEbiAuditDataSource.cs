﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersEbiAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(UsersEbiAuditDataSourceDesigner))]
	public class UsersEbiAuditDataSource : ProviderDataSource<UsersEbiAudit, UsersEbiAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditDataSource class.
		/// </summary>
		public UsersEbiAuditDataSource() : base(new UsersEbiAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersEbiAuditDataSourceView used by the UsersEbiAuditDataSource.
		/// </summary>
		protected UsersEbiAuditDataSourceView UsersEbiAuditView
		{
			get { return ( View as UsersEbiAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the UsersEbiAuditDataSource control invokes to retrieve data.
		/// </summary>
		public UsersEbiAuditSelectMethod SelectMethod
		{
			get
			{
				UsersEbiAuditSelectMethod selectMethod = UsersEbiAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (UsersEbiAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersEbiAuditDataSourceView class that is to be
		/// used by the UsersEbiAuditDataSource.
		/// </summary>
		/// <returns>An instance of the UsersEbiAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersEbiAudit, UsersEbiAuditKey> GetNewDataSourceView()
		{
			return new UsersEbiAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersEbiAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersEbiAuditDataSourceView : ProviderDataSourceView<UsersEbiAudit, UsersEbiAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersEbiAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersEbiAuditDataSourceView(UsersEbiAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersEbiAuditDataSource UsersEbiAuditOwner
		{
			get { return Owner as UsersEbiAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal UsersEbiAuditSelectMethod SelectMethod
		{
			get { return UsersEbiAuditOwner.SelectMethod; }
			set { UsersEbiAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersEbiAuditService UsersEbiAuditProvider
		{
			get { return Provider as UsersEbiAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<UsersEbiAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<UsersEbiAudit> results = null;
			UsersEbiAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case UsersEbiAuditSelectMethod.Get:
					UsersEbiAuditKey entityKey  = new UsersEbiAuditKey();
					entityKey.Load(values);
					item = UsersEbiAuditProvider.Get(entityKey);
					results = new TList<UsersEbiAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case UsersEbiAuditSelectMethod.GetAll:
                    results = UsersEbiAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case UsersEbiAuditSelectMethod.GetPaged:
					results = UsersEbiAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case UsersEbiAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = UsersEbiAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = UsersEbiAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case UsersEbiAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = UsersEbiAuditProvider.GetByAuditId(_auditId);
					results = new TList<UsersEbiAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == UsersEbiAuditSelectMethod.Get || SelectMethod == UsersEbiAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				UsersEbiAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					UsersEbiAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<UsersEbiAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			UsersEbiAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region UsersEbiAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersEbiAuditDataSource class.
	/// </summary>
	public class UsersEbiAuditDataSourceDesigner : ProviderDataSourceDesigner<UsersEbiAudit, UsersEbiAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditDataSourceDesigner class.
		/// </summary>
		public UsersEbiAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersEbiAuditSelectMethod SelectMethod
		{
			get { return ((UsersEbiAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new UsersEbiAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region UsersEbiAuditDataSourceActionList

	/// <summary>
	/// Supports the UsersEbiAuditDataSourceDesigner class.
	/// </summary>
	internal class UsersEbiAuditDataSourceActionList : DesignerActionList
	{
		private UsersEbiAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the UsersEbiAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public UsersEbiAuditDataSourceActionList(UsersEbiAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public UsersEbiAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion UsersEbiAuditDataSourceActionList
	
	#endregion UsersEbiAuditDataSourceDesigner
	
	#region UsersEbiAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the UsersEbiAuditDataSource.SelectMethod property.
	/// </summary>
	public enum UsersEbiAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion UsersEbiAuditSelectMethod

	#region UsersEbiAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEbiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiAuditFilter : SqlFilter<UsersEbiAuditColumn>
	{
	}
	
	#endregion UsersEbiAuditFilter

	#region UsersEbiAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEbiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiAuditExpressionBuilder : SqlExpressionBuilder<UsersEbiAuditColumn>
	{
	}
	
	#endregion UsersEbiAuditExpressionBuilder	

	#region UsersEbiAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;UsersEbiAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="UsersEbiAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersEbiAuditProperty : ChildEntityProperty<UsersEbiAuditChildEntityTypes>
	{
	}
	
	#endregion UsersEbiAuditProperty
}

